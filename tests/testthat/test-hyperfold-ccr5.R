library(testthat)
library(hyperfold)

testthat::test_that("Works for CCR5 sequence example.", {

 sq <- "UUUAAAAGCCAGGACGGUCACCUUUGGGGUGGUGACAAGUGUGAUCACUUGGGUGGUGGCUGUGUUUGCGUCUCUCCCAGGAAUCAUCUUUACCAGAUCUCAA"


 result <- Hyperfold(sequences=sq)

 testthat::expect_is(result, "list")

})