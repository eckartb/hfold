
to_properties <- function(x) {
  result <- character()
  nms = names(x)
  if (is.null(nms)) {
   nms <- as.character(seq_along(x))
  }
  for (i in seq_along(x)) {
   result2 = ""
   if (is.list(x)) {
    result2 = paste0(nms[i],".",to_properties(x[[i]]))
   } else {
    result2 = paste0(nms[i],"=",x[i])
   }

   result <- append(result,result2)
  }
  result
}

test_properties <- function() {
 tbl <- head(cars)
 to_properties(tbl)
}