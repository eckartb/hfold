#' Generates list containing multiple copies of input list
#' 
#' This function generates a list containing multiple copies of input list
#' and computes unique input names.
#' 
#' @name RepeatList
#' @param times Number of repetitions
#' @param sep Character string: separator
#' @export
#' @examples 
#' ll <- list(a=3, b=5)
#' l2 <- RepeatList(ll, 2)
#' length(l2) == 2*length(ll)
#' identical(unique(names(l2)), names(l2))
RepeatList <- function(lst, times, sep=".") {
  n <- length(lst)
  nms <- names(lst)
  if (is.null(nms)) {
    nms <- as.character(1:n)
  }
  ends <- sort(rep(1:times, n))
  result <- rep(lst, times) 
  names(result) = paste(nms,ends, sep=sep)
  result
}


#' check for table of pairings
#' 
#' Returns true, if and only if data structure corresponds to a data frame 
#' with pairing information in form of columns "id1", "id2", and "bp"
#' 
#' @name is.pairtable
#' @param x Should be a data frame with columns "id1", "id2", and "bp"
#' @export
is.pairtable <- function(x, n=NULL) {
# cat("Started is.pairtable with:\n")
#  print(x)
  result <- is.data.frame(x) && (!is.null(x$id1)) && (!is.null(x$id2)) && (!is.null(x$bp))  && (!is.factor(x$id1)) && (!is.factor(x$id2))  && is.character(x$bp) && is.numeric(x$id1) && is.numeric(x$id2)
   if (result && (nrow(x) > 0)) {
    result <- result && (min(x$id1) > 0) && (min(x$id2) > 0)
    if (result && (!is.null(n))) {
        result <- result && (max(x$id1) <= n) && (max(x$id2) <= n)
    }
   }
   result
}

#' check for bead model
#' 
#' Returns true, if and only if data structure corresponds to a bead model that can be used 
#' by SimulateNABeadsFromList
#' 
#' @name is.beadmodel
#' @param x Should be a list with elements "sequences", "id1", "id2", and "bp"
#' @export
#' @examples
#' sequences <- "GGGGCCCC"
#' bv1 <- c(1,2)
#' bv2 <- c(8,7)
#' bp <- c("cWW","cWW")
#' pairsOrig <- data.frame(cbind(id1=bv1, id2=bv2, bp=bp))
#' pairsPlaced <- data.frame(cbind(id1=bv1, id2=bv2, bp=bp))
#' beadModel <- list("sequences"=sequences, pairs_orig=pairsOrig, pairs_placed=pairsPlaced)
#' is.beadmodel(beadModel)
is.beadmodel <- function(x, pairs_orig.slot="pairs_orig") {
  n <- sum(nchar(x$sequences))
  result <- is.list(x) && (is.character(x[["sequences"]]))&& (is.list(x[[pairs_orig.slot]]))
  result
}

#' Generate matrix of column-sorted ids
#'
#' This function generates for two vectors a matrix of column-sorted ids such that each row
#' consists of two column, and the first column item is less or equal to the second column item.
#'
#' @name NormalizeIdOrder
#' @param id1 numeric vector of residue ids (1)
#' @param id2 numeric vector of residue ids (2)
#' @param col.names A character vector of length 2 with column names
#' @export
#' @examples
#' v1 <- c(5,2,7)
#' v2 <- c(6,1,4)
#' x <- NormalizeIdOrder(v1,v2)
#' x[1,1] == 5
#' x[1,2] == 6
#' x[2,1] == 1
#' x[2,2] == 2
#' x[3,1] == 4
#' x[3,2] == 7
NormalizeIdOrder <- function(id1, id2, col.names=c("V1", "V2")) {
  if (is.null(id1)) {
   id1 <- rep(0,0)
  }		 
  if (is.null(id2)) {
   id2 <- rep(0,0)
  }
  result <- data.frame(cbind(id1,id2), stringsAsFactors = FALSE)
  colnames(result) <- col.names
  stopifnot(!is.factor(result[,1]))
  stopifnot(!is.factor(result[,2]))
  ids <- result[,1] > result[,2]
  r2 <- result[ids,]
  r2 <- r2[,c(2,1)] # reverse columns
  result[ids,1] <- r2[,1]
  result[ids,2] <- r2[,2]
  result
}

#' Normalize a table of interaction ids
#'
#' This functions normalizes a table of interaction ids such that the first id is smaller than the second id.
#'
#' @name NormalizePairTable
#' @param pairs A table of pairs with at least two columns
#' @param remove.trail.split If set of a character, the function modifies the column called "bp" by removing the split part
#' @param id1col Column name of number corresponding to id 1 of base pairs
#' @param id2col Column name of number corresponding to id 2 of base pairs
#' @param bpcol Column name of number corresponding to type of base pairs. Typical value of column name: "bp".
#' @param Typical value of column entries: "cWW" for RNA base pairs.
#' @export
#' @return the function returns a modified version of the input table (variable name pairs)
NormalizePairTable <- function(pairs, remove.trail.split=":", id1col="id1", id2col="id2", bpcol="bp") {
  if (is.factor(pairs[, bpcol])) {
    pairs[, bpcol] <- levels(pairs[, bpcol])[pairs[, bpcol]] # remove factor 
  }
  pairs <- pairs[pairs[,id1col] < pairs[,id2col],]
  if (is.character(remove.trail.split)) {
   pairs$bp <- unlist(lapply(strsplit(pairs$bp, remove.trail.split), head,1))
  }
  if (is.null(pairs$bp)) {
    pairs$bp <- character()
  }
  stopifnot(!is.null(pairs$bp))
  
  stopifnot(is.pairtable(pairs))
  pairs
}

#' Cyclical rotation of a vector or list
#' 
#' This function returns a cyclically rotated vector or list
#' 
#' @name CycRot
#' @param x A vector or list
#' @param n Amount pf cylicial rotation
#' @return A vector or list with cyclical rotation of input
#' @export
#' @examples 
#' CycRot(c(1,2,3),1) # c(2,3,1)
#' CycRot(c(1,2,3), -1) # c(3,1,2)
CycRot <- function(x, n = 1) {
  if (is.na(n) || (n == 0)) x else c(tail(x, -n), head(x, n))
}

#' Cyclical rotation of a string
#' 
#' This function returns a cyclically rotated vector or list
#' 
#' @name CycRotStr
#' @param s A character string
#' @param n Amount pf cylicial rotation
#' @return A string with cyclical rotation of input
#' @export
#' @examples 
#' CycRotStr("you",1) == "ouy"
#' CycRotStr("you", -1) == "uyo"
CycRotStr <- function(s, n = 1) {
  result <- s
  if (!is.na(n)) {
    x <- strsplit(s, "")[[1]]
    y <- CycRot(x, n)
    result <- paste(y, collapse="")
  }
  result
}

Which.Inverse <- function(x, ymin=min(x), ymax=max(x)) {
  y <- rep(NA, max(x))
  for (i in 1:length(x)) {
    y[x[i]] <- i
  }
  y
}

#' Cyclic rotation of bead model
#' 
#' This function performs a cyclic rotation of sequences from a bead model
#'
#' @name CycRotBeadmodel
#' @param beadModel A list as generated by GenerateBeadModel
#' @param rotations a vector with the same length as number of sequences; for each sequence indicating rotation (0 or NA if no rotation)
CycRotBeadmodel <- function(beadModel, rotations) {
  ns <- length(beadModel$sequences)
  for (i in 1:ns) {
    beadModel$sequences[i] <- CycRotStr(beadModel$sequences[i], rotations[i])
  }
  relIds <- LengthsToRelativeIds(nchar(beadModel$sequences))
  relIds2 <- relIds
  stopifnot(length(relIds2) == ns)
  for (i in 1:ns) {
    relIds2[[i]] <- CycRot(relIds2[[i]], rotations[i])
  }
  relIds2b <- unlist(relIds2)
  beadModel$pairs_orig$id1 <- relIds2b[beadModel$pairs_orig$id1]
  beadModel$pairs_orig$id2 <- relIds2b[beadModel$pairs_orig$id2]
  beadModel
}

