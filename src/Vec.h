#ifndef VEC_H
#define VEC_H

#include <iostream>
#include <iterator> // iostream_iterator
#include <vector>
#include <string>
#include "debug.h"

using namespace std;

// type alias used to hide a template parameter
template<class T>
using Vec = std::vector<T>;

/** Sends a Vec to stream.
 The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
template <class T>
inline
std::istream& operator >> (std::istream& is, Vec<T>& v) {
      ERROR_IF(!is, "Input stream is not available for reading!");
      typename Vec<T>::size_type size;
      v.clear(); // first remove all existing elements EB 02/2001
      is >> size;
      ERROR_IF(!is, "Vec: Number of elements expected as first item!");
      // maximum number of input elements:
      ERROR_IF(size > 200000, "Vec: Too large number of input elements!");
      for (typename Vec<T>::size_type i = 0; i < size; i++) {
        T element;
        is >> element;
        ERROR_IF(!is, "Vec: Error reading element!");
        v.push_back(element);
      }
  return is;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<int>& v) {
   ERROR_IF(!os.good(), "Output stream is not available for writing!");
   os << v.size() << "   ";
   copy(v.begin(), v.end(), std::ostream_iterator<int>(os, " "));
   os << std::endl;
   return os;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<unsigned int>& v) {
   ERROR_IF(!os.good(), "Output stream is not available for writing!");
   os << v.size() << "   ";
   copy(v.begin(), v.end(), std::ostream_iterator<unsigned int>(os, " "));
   os << std::endl;
   return os;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<unsigned long>& v) {
   ERROR_IF(!os.good(), "Output stream is not available for writing!");
   os << v.size() << "   ";
   copy(v.begin(), v.end(), std::ostream_iterator<unsigned long>(os, " "));
   os << std::endl;
   return os;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<char>& v) {
   ERROR_IF(!os.good(), "Output stream is not available for writing!");
   os << v.size() << "   ";
   copy(v.begin(), v.end(), std::ostream_iterator<char>(os, " "));
   os << std::endl;
   return os;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<std::string>& v) {
   ERROR_IF(!os.good(), "Output stream is not available for writing!");
   os << v.size() << " ";
   for (Vec<std::string>::size_type i = 0; i < v.size(); ++i) {
     os << " " << v[i];
   }
//   copy(v.begin(), v.end(), std::ostream_iterator<std::string>(os, " "));
   os << std::endl;
   return os;
}



/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
the vector followed by a space-seperated list of elements. */
inline
  std::ostream& operator << (std::ostream& os, const Vec<double>& v) {
    ERROR_IF(!os.good(), "Output stream is not available for writing!");
    os << v.size() << " ";
    for (Vec<std::string>::size_type i = 0; i < v.size(); ++i) {
      os << " " << v[i];
    }
    //   copy(v.begin(), v.end(), std::ostream_iterator<std::string>(os, " "));
    os << std::endl;
    return os;
  }


/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
 the vector followed by a space-seperated list of elements. */
inline
std::ostream& operator << (std::ostream& os, const Vec<float>& v) {
    ERROR_IF(!os.good(), "Output stream is not available for writing!");
    os << v.size() << " ";
    for (Vec<std::string>::size_type i = 0; i < v.size(); ++i) {
      os << " " << v[i];
    }
    //   copy(v.begin(), v.end(), std::ostream_iterator<std::string>(os, " "));
    os << std::endl;
  return os;
}

/** Writes a Vec to a stream. The data format is very simple: the first element is the length of
 the vector followed by a space-seperated list of elements. */
inline
  std::ostream& operator << (std::ostream& os, const Vec<Vec<float> >& v) {
    ERROR_IF(!os.good(), "Output stream is not available for writing!");
    os << v.size() << " ";
    for (Vec<std::string>::size_type i = 0; i < v.size(); ++i) {
      os << " " << v[i];
    }
    //   copy(v.begin(), v.end(), std::ostream_iterator<std::string>(os, " "));
    os << std::endl;
    return os;
  }
#endif
