// --*- C++ -*------x---------------------------------------------------------
// $Id: RankedSolution5.h,v 1.1.1.1 2006/07/03 14:43:21 bindewae Exp $
//
// Class:           RankedSolution5
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     Implements ranked solutions
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

#ifndef __RANKED_SOLUTION5_H__
#define __RANKED_SOLUTION5_H__

// Includes

#include <iostream>
#include "debug.h"

template<class T, class S = int>
class RankedSolution5 {
public:
  RankedSolution5<T, S>() : first(0.0) { } 

  RankedSolution5<T, S>(double _first, const T& _second) : 
    first(_first), second(_second)
  {
  }

  RankedSolution5<T, S>(double _first, const T& _second, const S& _third) : first(_first),
							    second(_second), third(_third)
  {
  }

  RankedSolution5<T,S>(const RankedSolution5<T,S>& orig) { copy(orig); }

  virtual ~RankedSolution5<T,S>() {} 

  /* OPERATORS */

  /** Assigment operator. */
  RankedSolution5<T,S>& operator = (const RankedSolution5<T,S>& orig) {
    if (&orig != this) {
      copy(orig);
    }
    return *this;
  }

  /** Assigment operator. */
  bool operator == (const RankedSolution5<T,S>& orig) {
    return first == orig.first && second == orig.second && third == orig.third;
  }

  friend ostream& operator << (ostream& os, const RankedSolution5<T,S>& orig) {
    os << orig.first << " " << orig.second << " " << orig.third; return os;
  }

  friend istream& operator >> (istream& is, RankedSolution5<T,S>& orig) {
    is >> orig.first >> orig.second >> orig.third; return is;
  }

  /* PREDICATES */

  double getFirst() const { return first; }

  const T& getSecond() const { return second; }

  const S& getThird() const { return third; }

  /* MODIFIERS */
  void copy(const RankedSolution5<T, S>& other) {
    first = other.first;
    second = other.second;
    third = other.third;
  }

  /* ATTRIBUTES */

  double first;
  T second;
  S third;

protected:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* PRIVATE ATTRIBUTES */

};

/** comparison operator. */
template<class T, class S>
inline
bool operator < (const RankedSolution5<T,S>& lval,
		 const RankedSolution5<T,S>& rval) { 
  return lval.first < rval.first;
}

/** comparison operator. */
template<class T, class S>
inline
bool operator < (double lval, const RankedSolution5<T,S>& rval) {
  return lval < rval.first;
}

/** comparison operator. */
template<class T, class S>
inline
bool operator < (const RankedSolution5<T,S>& lval, double rval)   {
  return lval.first < rval;
}
#endif /* __ACLASS_H__ */

