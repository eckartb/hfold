#ifndef _RESIDUE_MODEL_
#define _RESIDUE_MODEL_
#include <iostream>
#include <string>
#include "Vec.h"
#include "StackingEnergy.h"
#include <ctype.h>
#include <math.h>
#include "verbose.h"
#include "Vector3D.h"
#include "Stem.h"
#include "RnaGeometryConstants.h"

// #include <MatchFoldConstants.h>

#define ULTRA_SMALL 1E-30

// gas constant R in kcal/mol per Kelvin
#define GAS_CONSTANT 1.98577e-3

// maximum length of loop so that two adjacent helices have stacking interaction
#define BULGE_MAX 3

#define BP_DEFAULT 0.0

#define BP_ENERGY_AU -0.5
#define BP_ENERGY_GC -1.0

#define BP_ENERGY_AU_SINGLE 3
#define BP_ENERGY_GC_SINGLE 2

#define HELIX_PENALTY 4.0

/** Aalberts, NAR, 2005 */
// #ifndef HELIX_RISE
// #define HELIX_RISE 2.7
// #endif

/** Aalberts, NAR, 2005 */
#define BASEPAIRS_PER_TURN 11.2

/** Aalberts, NAR, 2005 */
#define HELIX_C4_HELIXRADIUS 9.9

/** Aalberts, NAR, 2005 */
#define HELIX_WATSON_CRICK_PHASE 1.6

/** Aalberts, NAR, 2005 */
#define HELIX_CRICK_ZOFFSET -4.2

// 4.0
// 2.0

// Reaction radius in Angstroem
// #define REACTION_RADIUS 10.0
// the value of 8.8 has been fitted such that the folding algorithm reproduces the melting temperatures of Freier duplexes: case 56 in Strenum9Test.h (AGAGAGAG/CUCUCUCU) , but also case 50, 52, 54, ad 58). For all but case 54, the melting temperature is found with an error of about 1 degree celsius. Aug 26, 2015
#define REACTION_RADIUS 8.8
// before Aug 25, 2015: 1.45
// #define REACTION_RADIUS 5.0
// 5.0

// Defines size of effective volume: how many strands should be simulated 
#define STRANDS_PER_EFF_VOLUME 1

// cubic volume of reaction in Angstroem cubed units
// MUST BE (8*REACTION_RADIUS*REACTION_RADIUS*REACTION_RADIUS)
// #define REACTION_VOLUME 1000
// Reaction volume in m3: multiple REACTION_VOLUME with 1e-30 (1E-10 * 1E-10 * 1E-10)
// #define REACTION_VOLUME_SI 1E-27

// convert from external concentrations (micromolar) to internal concentrations: particles per Angstroem cubed
// convert from micro mol per liter to particles per Angstroem cubed // (6.0221415e+23 * 1E-33) 
#define MICROMOL_TO_N_PER_ANG3 6.022141e-10 
// convert from particles per Angstroem cubed to micro mol per liter // (1/(6.0221415e+23 * 1E-33)) 
#define N_PER_ANG3_TO_MICROMOL (1.0/6.022141e-10)

// minimum distance between C1' atoms (unpaired)
#define RESIDUE_DIST_MIN 5.5

// maximum distance between adjacent C1' atoms (unpaired)
// #define RESIDUE_ADJ_DIST_MAX 7.0
// max distance between C4' atoms of adjacent residues: 6.52A (test.atomDistanceStats_adjC4p in package rna3d)
#define RESIDUE_ADJ_DIST_MAX 6.52

// midrange of adj residue C1' positions of 438D structure
// #define HELIX_ADJ_DIST 5.56

/** distance between C1' atoms (PDB: 438D) */
// #define HELIX_PAIRED_DIST 10.48
/** distance between C4' atoms (PDB: 157D) */
#define HELIX_PAIRED_DIST 14.98

// Minimal allowed length of loops minus one 
#ifndef LOOP_LEN_MIN
#define LOOP_LEN_MIN 4
#endif
// #define LOOP_LEN_MIN 3

// Max deviation per base pair from ideal position (was: 1.0) : 
#define HELIX_SLOP 0.5
// 0.5

// #define HELIX_RISE 2.8

#define AVOGADRO 6.0221415e+23

#define DIFFERENT_POT_DISTANCE 9.9e+20

// value of RT in kcal/mol at 37 degree Celsius
#define RT_37C 0.616

#define CELSIUS_TO_KELVIN 273.15

// Exponent of loop entropy: Wenbing Zhang and Shi-Jie Chen: Biophys J. (90), 778–787 (2006).
#define INTRA_COLLISION_EXPONENT 1.80

#define INTRA_COLLISION_EPSILON 1.0

#define STRAND_ENTROPY_PENALTY_DEFAULT 0.0

/** This class describes how an RNA secondary structure is represented: how many beads per residue, what (pseudo) atom types and what min and max distances for each (pseudo)atom pair?
 * Units: concentrations: number of single particles per Angstroem cubed.
 * 
 */
class ResidueModel {

 public:

  friend class StackingEnergy;

  //    typedef string::size_type size_t;
  typedef int index_t;
  typedef string::size_type size_t;

  enum { NO_ENTROPY = 0, SIMPLE_ENTROPY=1, STRAND_ENTROPY=2, ENTROPY_MODE_AUX=3, PARTITION_FUNCTION_ENTROPY=4,ENTROPY_MODE_MAX=5 };

  enum { NO_PSEUDO_KNOTS = 0, INTERSTRAND_PSEUDOKNOTS=1, ALL_PSEUDOKNOTS=2, H_TYPE_PSEUDOKNOTS=3, PSEUDOKNOT_MODE_MAX=4};

 private:
  int entropyMode;
  double entropyWeight;
  double helixDnaGAdjust;
  double helixRnaGAdjust;
  double helixSlop;
  string modelName;
  size_t atomsPerResidue;
  double intraCollisionProb0;
  double reactionRadius;
  double statesPerFlexibleResidue; // how many "states" can a flexible residue have? Parameter to be fitted
  Vec<string> atomTypeNames; // like rAN1, rCN 
  Vec<string> bpTypeNames; // like cWW, 
  Vec<Vec<Vec<double> > > bpMinDists; // for atom type 1, atom type 2, base pair type: what is minimum distance found, (perhaps with defined alpha)
  Vec<Vec<Vec<double> > > bpMaxDists; // for atom type 1, atom type 2, base pair type: what is maximum distance found, (perhaps with defined alpha)
  Vec<Vec<double> > adjNoHelixMinDists; // for atom type 1, atom type 2 of 3' adjacent residue, what is minimum distance found, (perhaps with defined alpha)
  Vec<Vec<double> > adjNoHelixMaxDists; // for atom type 1, atom type 2 of 3' adjacent residue, what is maximum distance found, (perhaps with defined alpha)
  Vec<Vec<double> > adjHelixMinDists; // for atom type 1, atom type 2 of 3' adjacent residue, what is minimum distance found, (perhaps with defined alpha)
  Vec<Vec<double> > adjHelixMaxDists; // for atom type 1, atom type 2 of 3' adjacent residue, what is maximum distance found, (perhaps with defined alpha)
  StackingEnergy stackingEnergy;
  double strandEntropyPenalty; // entropic penalty for bae pairing between so far not connected strands (units: kcal/mol)
  // RT (Na*k*T) for 37 degree Celsius
  double RT;
  int pseudoknotMode;
  int verbose;

 public:

  ResidueModel(size_t _atomsPerResidue) {
    ASSERT(testIsWatsonCrick()); // self-test
    atomsPerResidue = _atomsPerResidue;
    RT = RT_37C; // 0.616kcal/mol // RT (Na*k*T) for 37 degree Celsius
    stackingEnergy.setToupperMode(false);
    verbose = VERBOSE_GLOBAL;
    setDefaults();
THROWIFNOT(validate(),"Assertion violation in L173");
  }

  ResidueModel() {
    ASSERT(testIsWatsonCrick()); // self-test
    atomsPerResidue = 1;
    RT = RT_37C; // 0.616  // RT (Na*k*T) for 37 degree Celsius
    stackingEnergy.setToupperMode(false);
    verbose = VERBOSE_GLOBAL;
    setDefaults();
THROWIFNOT(validate(),"Assertion violation in L183");
  }

  virtual ~ResidueModel() { }

  const ResidueModel& operator = (const ResidueModel& other) {
    DERROR("Internal error: Assigment operator not implemnted for class ResidueModel.");
    return *this;
  }

  virtual void adjustTemperature(const ResidueModel& enthalpyModel) {
    double tempK = getTemperatureK();
    stackingEnergy.adjustTemperatureK(tempK, enthalpyModel.stackingEnergy);
  }

  virtual size_t getAtomsPerResidue() const { return atomsPerResidue; }

  virtual int getEntropyMode() const { return entropyMode; }

  virtual double getEntropyWeight() const { return entropyWeight; }

  virtual double getIntraCollisionEpsilon() const { return INTRA_COLLISION_EPSILON; }

  virtual double getIntraCollisionExponent() const { return INTRA_COLLISION_EXPONENT; }

  virtual double getIntraCollisionProb0() const {
THROWIFNOT(intraCollisionProb0 > 0.0,"Assertion violation in L207");
    return intraCollisionProb0;
  } 

  virtual int getPseudoknotMode() const { return pseudoknotMode; }

  virtual void setPseudoknotMode(int mode) { pseudoknotMode = mode; }

  virtual double getStrandEntropyPenalty() const { return strandEntropyPenalty; }

  virtual void setDefaults() {
    entropyMode = PARTITION_FUNCTION_ENTROPY; // STRAND_ENTROPY;
    modelName = "Simple_Residue_Model_0_0_1";
    atomsPerResidue = 1;
    atomTypeNames = Vec<string>(4);
    atomTypeNames[0] = "A";
    atomTypeNames[1] = "C";
    atomTypeNames[2] = "G";
    atomTypeNames[3] = "U";
    bpTypeNames = Vec<string>(1);
    bpTypeNames[0] = "cWW";
    bpMinDists.clear();
    bpMaxDists.clear();
    adjNoHelixMinDists.clear();
    adjNoHelixMaxDists.clear();
    adjHelixMinDists.clear();
    adjHelixMaxDists.clear();
    entropyWeight = 1.0;
    helixDnaGAdjust = 0.0;
    helixRnaGAdjust = 0.0;
    helixSlop = HELIX_SLOP;
    pseudoknotMode = INTERSTRAND_PSEUDOKNOTS;
    reactionRadius = REACTION_RADIUS;
    statesPerFlexibleResidue = 10.0;
    strandEntropyPenalty = STRAND_ENTROPY_PENALTY_DEFAULT;
    double tmpFactor = pow(reactionRadius, 3.0) * getIntraCollisionEpsilon();
THROWIFNOT(pow(reactionRadius, 3.0) > 0.0,"Assertion violation in L242");
THROWIFNOT(getIntraCollisionEpsilon() > 0.0,"Assertion violation in L243");
THROWIFNOT(tmpFactor > 0.0,"Assertion violation in L244");
    intraCollisionProb0 = (3.0/(4.0 * PI) ) * tmpFactor; 
THROWIFNOT(intraCollisionProb0 > 0.0,"Assertion violation in L246");
  }

  void setEntropyMode(double value) { entropyMode = value; }

  void setEntropyWeight(double value) { entropyWeight = value; }

  void setHelixDnaGAdjust(double _gAdjust) { helixDnaGAdjust = _gAdjust; }

  void setHelixRnaGAdjust(double _gAdjust) { helixRnaGAdjust = _gAdjust; }

  void setHelixSlop(double _helixSlop) { helixSlop = _helixSlop; }

  double getReactionRadius() const { return reactionRadius; }

  double getStatesPerFlexibleResidue() const { return statesPerFlexibleResidue; }

  void setStatesPerFlexibleResidue(double states) { statesPerFlexibleResidue = states; }
  
  void setReactionRadius(double _reactionRadius) { reactionRadius = _reactionRadius; }
  
  /** Example in put file:
      Simple_Residue_Model_0_0_1
      1
      4
      A
      C
      G
      U
      1
      cWW
      0
      0
      0
      0
      0
      0 */
  virtual void readModel(std::istream& is) {
    is >> modelName >> atomsPerResidue >> atomTypeNames >> bpTypeNames >> bpMinDists >> bpMaxDists >> adjNoHelixMinDists >> adjNoHelixMaxDists >> adjHelixMinDists >> adjHelixMaxDists;
  }

  virtual void readStacking(std::istream& is) {
    stackingEnergy.read(is);
    stackingEnergy.setToupperMode(false);
  }

//  virtual void writeModel(std::ostream& os) const {
//    os << modelName << atomsPerResidue << atomTypeNames << bpTypeNames << bpMinDists << bpMaxDists << adjNoHelixMinDists << adjNoHelixMaxDists << adjHelixMinDists << adjHelixMaxDists;
//  }

  virtual void writeStacking(std::ostream& os) const {
    stackingEnergy.write(os);
  }

  virtual void testHelixDist() const {
    Rcpp::Rcout << "Testing C4' atom distances in helix (same strand):" << endl;
    for (int i = 0; i < 20; ++i) {
      Rcpp::Rcout << i << " " << getHelixDist(i, true) << endl;
    }
    Rcpp::Rcout << "Testing C4' atom distances in helix (opposing strand):" << endl;
    for (int i = 0; i < 20; ++i) {
      Rcpp::Rcout << i << " " << getHelixDist(i, false) << endl;
    }
  }

  virtual double getRT() const { return RT; }

  /** Sets RT value */
  virtual void setRT(double value) { RT = value; }

  virtual void setStrandEntropyPenalty(double value) { strandEntropyPenalty = value; }

  /** Returns temperature in Celsius */
  virtual double getTemperatureC() const {
/*     double temp37K = 37.0 + CELSIUS_TO_KELVIN; */
/*     double R = RT_37C / temp37K; */
    double tempKelvin = RT/GAS_CONSTANT;
    double tempCelsius = tempKelvin - CELSIUS_TO_KELVIN;
    return tempCelsius;
  }

  /** Returns temperature in Kelvin */
  virtual double getTemperatureK() const {
/*     double temp37K = 37.0 + CELSIUS_TO_KELVIN; */
/*     double R = RT_37C / temp37K; */
    double tempKelvin = RT/GAS_CONSTANT;
    return tempKelvin;
  }

  /** Sets RT value from temperature in Celsius */
  virtual void setTemperatureC(double temperatureC) {
/*     double temp37K = 37.0 + CELSIUS_TO_KELVIN; */
/*     double R = RT_37C / temp37K; */
    double temperatureK = temperatureC + CELSIUS_TO_KELVIN;
    RT = GAS_CONSTANT * temperatureK;
  }

  /** Sets RT value from temperature in Celsius */
  virtual void setTemperatureK(double temperatureK) {
/*     double temp37K = 37.0 + CELSIUS_TO_KELVIN; */
/*     double R = RT_37C / temp37K; */
    RT = GAS_CONSTANT * temperatureK;
  }

  /** Returns helix stacking energy */
  virtual double calcStemStackingEnergy(const string& sequence, index_t start, index_t stop, index_t length) const {
    ERROR_IF(!stackingEnergy.validate(), "No energy parameter files has been read!");
    double result = stackingEnergy.calcStemStackingEnergy(sequence, start, stop, length);
    if (isDna(sequence[start]) || isDna(sequence[stop])) {
      result += helixDnaGAdjust; // only apply to DNA/DNA or RNA/DNA interactions, not to RNA/RNA interactions
    } else {
      result += helixRnaGAdjust; // only apply to RNA/RNA interactions
    }
    return result;
  }

  /** Returns helix stacking energy */
  virtual double calcStemStackingEnergy(const string& sequence, const Stem& stem) const {
    return calcStemStackingEnergy(sequence, stem.getStart(), stem.getStop(), stem.getLength());
  }

  /** Returns stacking energy, assuming two adjacent base pairs: a-b and x-y */
  virtual double getSingleBasePairEnergy(char x, char y) const {
    if (isWatsonCrick(x,y, true)) {
      return BP_DEFAULT;
    }
    return 0.0;
  }

  virtual double getHelixDistMin(int sep, bool sameStrand, char r1, char r2, const string& atom1, const string& atom2) const;

  virtual double getHelixDistMax(int sep, bool sameStrand, char r1, char r2, const string& atom1, const string& atom2) const;

  /** Position of base reference atom (here: C4') 
   *  See Aalberts, NAR, 2005
   */
  Vector3D getHelixWatsonPosition(int pos) const;

  /** Position of base reference atom (here: C4') 
   *  See Aalberts, NAR, 2005
   */
  Vector3D getHelixCrickPosition(int pos) const;
  
  /** Compute position difference between C'4 atoms */
  double getHelixDist(int sep, bool sameStrand) const;


  static bool isDna(char c) {
   return c== 'a' || c=='c' || c == 'g' || c == 't';
  }

  static bool isRna(char c) {
   return c == 'A' || c == 'C' || c == 'G' || c == 'U';
  }

  static bool isNucleotide(char c) {
    c = toupper(c);
    return c=='A' || c=='C' || c == 'G' || c == 'T' || c == 'U';
  }

  /** Returns true, if c1 and c2 describe Watson Crick pair (with GU). Even allows for hybrid pairing (lower case stands for DNA, upper case stands for RNA). TO DO: GT paring should not be allowed */
  static bool isWatsonCrickDna(char c1, char c2) {
THROWIFNOT(isDna(c1),"Assertion violation in L406");
THROWIFNOT(isDna(c2),"Assertion violation in L407");
    // char c1Orig = c1;
  //  char c2Orig = c2;
    // to upper character

    if (c1 > c2) {
      return isWatsonCrickDna(c2, c1);
    }
    
    return 
         ((c1 == 'a') && (c2 == 't')) 
      || ((c1 == 'c') && (c2 == 'g'));
  }
  
  /** Returns true, if c1 and c2 describe Watson Crick pair (with GU). Even allows for hybrid pairing (lower case stands for DNA, upper case stands for RNA). TO DO: GT paring should not be allowed */
  static bool isWatsonCrick(char c1, char c2, bool allowGu) {
    THROWIFNOT(isNucleotide(c1),"Assertion violation in ResidueModel.h :  L423: character " + string(1,c1));
    THROWIFNOT(isNucleotide(c2),"Assertion violation in ResidueModel.h :  L424: character " + string(1,c2));
    // char c1Orig = c1;
    // char c2Orig = c2;
    if (isDna(c1) && isDna(c2)) {
      return isWatsonCrickDna(c1,c2);
    }
    // to upper character
    c1 = toupper(c1);
    c2 = toupper(c2);

    if (c1 > c2) {
      return isWatsonCrick(c2, c1, allowGu);
    }
    if (c1 == 'T') {
      c1 = 'U';
      allowGu = false; // no "Gt" base pair allowed for hybrids
    }
    if (c2 == 'T') {
      c2 = 'U';
      allowGu = false;
    }
    
    return 
         ((c1 == 'A') && (c2 == 'U')) 
      || ((c1 == 'C') && (c2 == 'G'))
      || (allowGu && ((c1 == 'G') && (c2 == 'U')));  
  }

  /** Return true iff GU or UG */
  static bool isWobble(char c1, char c2) {
    return (c1 == 'G' && c2 == 'U') || (c1 == 'U' && c2 == 'G');
  }

  /** Returns helix, which the given base pair is part of */
  static int findWatsonCrickHelixLength(const string& seq, int start, int stop, bool allowGu, const Vec<size_t>& strandIds) {
    // Rcpp::Rcout << "# Starting findWatsonCrickHelixLength " << (start+1) << " " << (stop+1) << " " << seq << endl;
    if (start == stop) {
      return 0;
    }
    if (start > stop) {
      return findWatsonCrickHelixLength(seq, stop, start, allowGu, strandIds);
    }
THROWIFNOT(start < stop,"Assertion violation in L461");
THROWIFNOT(seq.size() == strandIds.size(),"Assertion violation in L462");
    int foundWc = 0;
    int n = static_cast<int>(seq.size());
THROWIFNOT(start >= 0,"Assertion violation in L465");
THROWIFNOT(start < n,"Assertion violation in L466");
THROWIFNOT(stop < n,"Assertion violation in L467");
    //    int start0 = 0;
    // int stop0 = 0;
    size_t s1id = strandIds[start];
    size_t s2id = strandIds[stop];
THROWIFNOT(stop > start,"Assertion violation in L472");
    if ((s1id == s2id) && ((stop - start) <= LOOP_LEN_MIN)) { // minimum loop length is 3
      return 0;
    }
    bool sameStrands = (s1id == s2id);
    if (isWatsonCrick(seq[start], seq[stop], allowGu)) {
      foundWc = 1;
      for (int di = 1; di < n; di++) { // check lower ids
	int nstart = start - di;
	int nstop = stop + di;
	if ((nstart < 0) || (nstart >= n) || (nstop < 0) || (nstop >= n) || (strandIds[nstart] != s1id) || (strandIds[nstop] != s2id) || (!isWatsonCrick(seq[nstart], seq[nstop], allowGu))) {
	  break;
	} else {
	  ++foundWc;
	  // start0 = start - di;
	  // stop0 = stop + di;
	  // Rcpp::Rcout << "# Found good pair in helix: " << (nstart+1) << " " << seq[nstart] << " " << (nstop+1) << " " << seq[nstop] << endl;
	} 
      }
      for (int di = 1; di < n; di++) { // check higher ids
	int nstart = start + di;
	int nstop = stop - di;
	if ((nstart < 0) || (nstart >= n) || (nstop < 0) || (nstop >= n) || (strandIds[nstart] != s1id) || (strandIds[nstop] != s2id) || (!isWatsonCrick(seq[nstart], seq[nstop], allowGu)) || (sameStrands && ((nstop - nstart) < LOOP_LEN_MIN)) ) {
	  break;
	} else {
	  ++foundWc;
	  // Rcpp::Rcout << "# Found good pair in helix: " << (nstart+1) << " " << seq[nstart] << " " << (nstop+1) << " " << seq[nstop] << endl;
	} 
      }
    }
    // Rcpp::Rcout << "# Finished findWatsonCrickHelixLength - Returning " << foundWc << endl;
    return foundWc;
  }

  /** Returns true, if c1 and c2 describe Watson Crick pair (with GU). Even allows for hybrid pairing (lower case stands for DNA, upper case stands for RNA). TO DO: GT paring should not be allowed */
  static bool testIsWatsonCrick() {
THROWIFNOT(isWatsonCrick('A','U', true),"Assertion violation in L508");
THROWIFNOT(isWatsonCrick('A','U', false),"Assertion violation in L509");
THROWIFNOT(isWatsonCrick('U','A', true),"Assertion violation in L510");
THROWIFNOT(isWatsonCrick('U','A', false),"Assertion violation in L511");
THROWIFNOT(isWatsonCrick('G','C', true),"Assertion violation in L512");
THROWIFNOT(isWatsonCrick('G','C', false),"Assertion violation in L513");
THROWIFNOT(isWatsonCrick('C','G', true),"Assertion violation in L514");
THROWIFNOT(isWatsonCrick('C','G', false),"Assertion violation in L515");
THROWIFNOT(!isWatsonCrick('C','C', true),"Assertion violation in L516");
THROWIFNOT(!isWatsonCrick('C','C', false),"Assertion violation in L517");
THROWIFNOT(isWatsonCrick('G','U', true),"Assertion violation in L518");
THROWIFNOT(!isWatsonCrick('G','U', false),"Assertion violation in L519");
THROWIFNOT(isWatsonCrick('U','G', true),"Assertion violation in L520");
THROWIFNOT(!isWatsonCrick('U','G', false),"Assertion violation in L521");
    ASSERT(isWatsonCrick('A','t', true)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('t','A', true)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('A','t', false)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('t','A', false)); // RNA/DNA hybrid

    ASSERT(isWatsonCrick('G','c', false)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('c','G', false)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('G','c', true)); // RNA/DNA hybrid
    ASSERT(isWatsonCrick('c','G', true)); // RNA/DNA hybrid

    ASSERT(!isWatsonCrick('G','t', true)); // RNA/DNA hybrid
    ASSERT(!isWatsonCrick('G','t', false)); // RNA/DNA hybrid
    ASSERT(!isWatsonCrick('t','G', true)); // RNA/DNA hybrid
    ASSERT(!isWatsonCrick('t','G', false)); // RNA/DNA hybrid

    return true;
  }

  virtual bool validate() const {
    return intraCollisionProb0 > 0;
  }

  /** Returns stacking energy, assuming two adjacent base pairs: a-b and x-y */
  virtual double getStackingEnergy(char a, char b, char x, char y) const {
THROWIFNOT(stackingEnergy.validate(),"Assertion violation in L546");
    double result = stackingEnergy.getStackingEnergy(a,b,x,y);
    return result;
    } 

  /** Returns stacking energy, assuming two adjacent base pairs: a-b and x-y */
  virtual double getStackingEnergyPermissive(char a, char b, char x, char y) const {
THROWIFNOT(stackingEnergy.validate(),"Assertion violation in L553");
    double result = stackingEnergy.getStackingEnergyPermissive(a,b,x,y);
    return result;
  } 

  /** Returns terminal stacking energy if base pair is at end of helix */
  virtual double getTerminalEnergy(char a, char b) const {
    ERROR_IF(!stackingEnergy.validate(), "No energy parameter files has been read!");
    double result = stackingEnergy.getTerminalEnergy(a,b);
    return result;
  }


};

/** Position of base reference atom (here: C4') 
 *  See Aalberts, NAR, 2005
 */
Vector3D
ResidueModel::getHelixWatsonPosition(int pos) const {
  double h = HELIX_RISE;
  double angle = (2.0 * PI) * pos / BASEPAIRS_PER_TURN;
  double r = HELIX_C4_HELIXRADIUS;
  double x = r * cos(angle);
  double y = r * sin(angle);
  double z = h * pos;
  return Vector3D(x,y,z);
}

/** Position of base reference atom (here: C4') 
 *  See Aalberts, NAR, 2005
 */
Vector3D
ResidueModel::getHelixCrickPosition(int pos) const {
  double h = HELIX_RISE;
  double angle = ((2.0 * PI) * pos / BASEPAIRS_PER_TURN) + HELIX_WATSON_CRICK_PHASE;
  double r = HELIX_C4_HELIXRADIUS;
  double x = r * cos(angle);
  double y = r * sin(angle);
  double z = h * pos + HELIX_CRICK_ZOFFSET;
  return Vector3D(x,y,z);
}

/** Compute position difference between C'4 atoms */
double
ResidueModel::getHelixDist(int pos, bool sameStrand) const {
  PRECOND(pos >= 0);
  Vector3D refPoint(HELIX_C4_HELIXRADIUS, 0.0, 0.0); // shortcut of : getHelixWatsonPosition(0);
  Vector3D result;
   if (sameStrand) {
     result = getHelixWatsonPosition(pos);
   } else {
     result = getHelixCrickPosition(pos);
   }
THROWIFNOT(result >= 0.0,"Assertion violation in L607");
   double dist = refPoint.distance(result);
   return dist;
  }

double
ResidueModel::getHelixDistMin(int sep, bool sameStrand, char r1, char r2, const string& atom1, const string& atom2) const {
  PRECOND(sep >= 0);
  double result = getHelixDist(sep, sameStrand);
  result -= sep * helixSlop; 
  if (result < 0.0) {
    result = 0.0;
  }
  return result;
  }

double
ResidueModel::getHelixDistMax(int sep, bool sameStrand, char r1, char r2, const string& atom1, const string& atom2) const {
  PRECOND(sep >= 0);
  double result = getHelixDist(sep, sameStrand);
THROWIFNOT(result >= 0.0,"Assertion violation in L627");
  result += sep * helixSlop;
   return result;
 }


#endif
