#ifndef IO_TOOLS_H
#define IO_TOOLS_H

class IOTools {

 public:

  template <class T>
  static void writeHead(ostream& os, const Vec<T>& values, const string& delim) {
    size_t n = 6;
    for (size_t i = 0; (i < n) && (i < values.size()); ++i) {
      if (i >0) {
	os << delim;
      }
      os << values[i];
    }
  }

  static void writeHead(ostream& os, const Vec<Vec<unsigned int> >& states, const string& delim) {
    // size_t n = 6;
    for (size_t i = 0; (i < states.size() ) && (i < 6); ++i) {
      if (i >0) {
	os << delim;
      }
      for (size_t j = 0; j < states[i].size(); ++j) {
	os << states[i][j] << " ";
      }
    }
  }

};

#endif
