// --*- C++ -*------x---------------------------------------------------------
// $Id: 
//
// Class:           Random
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Matthias Heiler, Eckart Bindewald
//
// Project name:    -
//
// Description:     Replaces stdlib's rand() function. The implementation as 
//                  Singelton provides the random number generator from 
//                  beeing re-seeded over and over again. 
// 
// -----------------x-------------------x-------------------x-----------------

#ifndef __RANDOM_H__
#define __RANDOM_H__

// Includes

extern "C" {
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
}
#include <math.h>


/** Random number generator.

    Replaces stdlib's rand() function. The implementation as Singleton
    provides the random number generator from beeing re-seeded over
    and over again.

    @see    Gamma et al: "Design Patterns", Singleton-Pattern */
class Random {
public:

  typedef unsigned int seed_type;

  /* OPERATORS */
  /** "functor", use for example with random_shuffle */
  long operator () (long n)  { return getRand(n); }

  /** Return reference to RNG object. */
  static Random& getInstance();

  /** How big is biggest number we can get? */
  static inline long getMax();

  /** What is next random number? */
  long getRand();

  /** return random number between 0 and n-1 : */
  long getRand(long n) { 
    if (n < 2) {
      return 0;
    }
    return getRand() % n; 
  }

  /** What is next random number?. 
      This method returns a float in [0..1[. */
  double getRandf();

  /** return normally distributed random number */
  double getGaussian();

  /** Get exponentially distributed random number using formula -gamma ln(x)
   * @see http://www.sitmo.com/eq/513
   */
  double getExponential();

  /* MODIFIERS */

  /**
   * resets random number generator and sets seed 
   */
  void resetWithSeed(seed_type seed);

protected:
  Random();

private:
  static Random* msInstance;
  int iset;
  double gset;
};

/** initialize random number generator using time and process id as seed */
inline 
Random::Random() : iset(0), gset(0.0)

{
  // srand((int)getpid() + time(0)); // should be done with R wrapper code with set.seed
}

inline 
void
Random::resetWithSeed(seed_type seed)
{
  iset = 0;
  gset = 0.0;
  // srand(seed); // should be done in R wrapper code with set.seed
}

inline 
Random&
Random::getInstance()
{
  if (msInstance == 0)
    {
      msInstance = new Random;
    }

  return *msInstance;
}

inline
long
Random::getRand()
{
  return static_cast<long>(R::runif(0,1) * RAND_MAX) ; // ::rand();
}

inline
double
Random::getRandf()
{
  return R::runif(0,1); // ::rand() / static_cast<double>(RAND_MAX);
}

inline
long
Random::getMax()
{
  return RAND_MAX;
}


Random* Random::msInstance = 0;


/** from numerical recipes 
    return normally distributed random number */
double
Random::getExponential() {
  double x = getRandf();
  if (x <= 0.0) {
    x = 1.0; // wrap around
  }
  return -log(x);
}

/** from numerical recipes 
    return normally distributed random number */
double
Random::getGaussian() 
{
  double fac,rsq,v1,v2;
  if  (iset == 0) {
    do {
      v1=2.0*getRandf()-1.0;
      v2=2.0*getRandf()-1.0;
      rsq=v1*v1+v2*v2;
    } while (rsq >= 1.0 || rsq == 0.0);
    fac=sqrt(-2.0*log(rsq)/rsq);
    gset=v1*fac;
    iset=1;
    return v2*fac;
  } 
  iset=0;
  return gset;
}


#endif /* __RADNOM_H__ */

