#ifndef SINGLE_HELIX_STRUCTURE_OPTIMIZER
#define SINGLE_HELIX_STRUCTURE_OPTIMIZER

#include "Stem.h"
#include "StemTools.h"
#include "ResidueStructure.h"
#include "ResidueStructureOptimizer.h"
#include "GraphTools.h"
#include "GraphTools.h"
#include "MultiIterator.h"

class SingleHelixStructureOptimizer : ResidueStructureOptimizer{

 private:

  double UNPLACEABLE_ENERGY;

  Vec<Stem> stems;

 public:

 SingleHelixStructureOptimizer(const Vec<Stem>& _stems) : stems(_stems) {
    UNPLACEABLE_ENERGY = 1e30;
  }

  virtual ResidueStructure optimize(const ResidueStructure& structure) const {
    ResidueStructure bestStructure = structure;
    double lowestG = structure.computeAbsoluteFreeEnergy();
    Vec<Stem> helices = structure.getHelices();
    // find helices that are overlapped by at most one other stem:
    Vec<Stem> common0, common1, candHelices;
    for (size_t i = 0; i < stems.size(); ++i) {
      size_t count = 0;
      for (size_t j = 0; j < helices.size(); ++j) {
	if (stems[i].hasCommonBases(helices[j])) {
	  ++count;
	}
      }
      if (count == 0) {
	common0.push_back(stems[i]);
      } else if (count == 1) {
	common1.push_back(stems[i]);
      }
      if (count <= 1) {
	candHelices.push_back(stems[i]);
      }
    }
    for (size_t i = 0; i < helices.size(); ++i) { // leave out helix i
      Vec<Stem> usedHelices;
      usedHelices.insert(usedHelices.end(), helices.begin(), helices.begin()+i);
      usedHelices.insert(usedHelices.end(), helices.begin()+i+1, helices.end());
THROWIFNOT(usedHelices.size() + 1 == helices.size(),"Assertion violation in SingleHelixStructureOptimizer.h :  L52");
      ResidueStructure trial(structure.getSequences(), structure.getConcentrations(), structure.getResidueModel());
      try {
	trial.setStems(usedHelices);
	double g = trial.computeAbsoluteFreeEnergy();
	if (g < lowestG) {
	  bestStructure = trial;
	  lowestG = g;
	}
	double dgBest = 0.0;
	size_t bestId = candHelices.size();
	for (size_t j = 0; j < candHelices.size(); ++j ) {
	  if (candHelices[j].hasCommonBases(helices[i]) && trial.isPlaceable(candHelices[j])) {
	    double dg = computeHelixEnergyContribution(trial, candHelices[j]);
	    double gNew = g + dg;
	    if ((dg < dgBest) && (gNew < lowestG)) {
	      dgBest = dg;
	      bestId = j;
	    }
	  }
	}
	if (dgBest < 0.0) {
	  trial.setStem(candHelices[bestId]);
	  double g2 = trial.computeAbsoluteFreeEnergy();
	  if (g2 < lowestG) {
	    lowestG = g2;
	    bestStructure = trial;
	  }
	}
      } catch (int e){

      }
    }
    return bestStructure;
  }


 private:
  
  double computeHelixEnergyContribution(ResidueStructure structure, const Stem& stem) const {
    // build structure without that helix
    double g0 = structure.computeAbsoluteFreeEnergy();
    try {
      structure.setStem(stem);
    } catch (int e) {
      return UNPLACEABLE_ENERGY;
    }
    return structure.computeAbsoluteFreeEnergy() - g0;
  }

};

#endif
