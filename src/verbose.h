#ifndef _VERBOSE_H_
#define _VERBOSE_H_


#define VERBOSE_NOTHING -1

#define VERBOSE_QUIET 0

#define VERBOSE_INFO 1

#define VERBOSE_DETAILED 2

#define VERBOSE_FINE 3

#define VERBOSE_FINEST 4

#define VERBOSE_GLOBAL 0

#endif
