#include <Rcpp.h>

#include "Strenum9.h"
#include "Vec.h"
#include "string"
#include "RcppHelp.h"

using namespace std;
using namespace Rcpp;

// // [[Rcpp::plugins(cpp11)]]
// // [[Rcpp::export]]

/*
Rcpp::DataFrame cpp_GenerateStems(const Rcpp::CharacterVector& sequences, const Rcpp::NumericVector& helixLengthMin,
				  bool guAllowed, bool guEndAllowed) {
  Vec<double> concentrations(sequences.size(), 1.0);
  Vec<string> seqs(sequences.size());
  for (size_t i = 0; i < sequences.size(); ++i) {
    seqs[i] = sequences[i];
  }
  StrandContainer strands(seqs, concentrations);
  Vec<int> dividers;
  // Vec<Stem> stems = StemTools::generateAllFlankedStems(strands, helixLengthMin, guAllowed, dividers, extendHelixMode, lengthModulo);      
  Vec<Stem> stems = StemTools::generateLongestStems(strands, helixLengthMin[0], guAllowed, guEndAllowed, dividers);
  auto n = stems.size();
  Vec<Stem::index_type> starts(n);
  Vec<Stem::index_type> stops(n);
  Vec<Stem::index_type> lengths(n);
  for (size_t i = 0; i < n; ++i) {
    starts[i] = stems[i].getStart();
    stops[i] = stems[i].getStop();
    lengths[i] = stems[i].getLength();
  }
  Rcpp::DataFrame df = Rcpp::DataFrame::create(Rcpp::Named("Start")=starts,
					       Rcpp::Named("Stop")=stops,
					       Rcpp::Named("Length")=lengths);
  return df;
}
*/

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
Rcpp::List cpp_hyperfold(const Rcpp::CharacterVector& argv, const CharacterVector& sequences, const NumericVector& concentrations, const CharacterVector& homeDirV) {
  Strenum9 resgeomApp;
  Vec<std::string> args = RcppHelp::to_vector_string(argv);
  
  string homeDir;
  if (homeDirV.size() > 0) {
    homeDir = Rcpp::as<string>(homeDirV(0));
  }
  vector<string> seqs = RcppHelp::to_vector_string(sequences);
  vector<double> concv = RcppHelp::to_vector_double(concentrations);
  Rcpp::List result = resgeomApp.main(args, seqs, concv, homeDir);
 //  result["hello"] = "world";
  if (result.size() == 0) {
    Rcpp::warning("Internal error: The structure prediction returned an empty list.");
  }
  return result;
}
