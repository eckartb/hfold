// -*-C++-*-------------------------------------------------------------------
//
// author: Eckart Bindewald, Ulrike Hoefer
//
// descriptipton: changes string to integer, double, float or long
// changes integer, double, float or long to string 
//
// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (e.g. integer expected but float given) then error
//
// ---------------------------------------------------------------------------

#ifndef STRING_TOOLS_H
#define STRING_TOOLS_H 

#include <iostream>
#include <sstream>
#include <string> // stl string class
#include <vector>
#include <cstdlib>
#include "debug.h"
#include "Vec.h"

using namespace std;

// #ifndef __MS_WINDOWS__
const char DIR_CHAR = '/';
// #else
// const char DIR_CHAR = '\\';
// #endif

const string LOWER_LETTER_STRING = "abcdefghijklmnopqrstuvwxyz";
const string UPPER_LETTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string LETTER_STRING = UPPER_LETTER_STRING + LOWER_LETTER_STRING;
const string DIGIT_STRING = "0123456789";

/** Returns content of Environment variable or empty string */
inline
string
getEnvVar( std::string const & key ) {
  char * val = getenv( key.c_str() );
  return val == NULL ? std::string("") : std::string(val);
}

// used for easier port of R functions:
inline
void cat(const string& s) { Rcpp::Rcout << s << endl; }

/** changes string into integer: */
int Stoi( const string& );
/** changes string into unsigned integer: */
unsigned int Stoui( const string& ); 
/** changes string into long: */
long Stol( const string& );
/** changes string into unsigned long: */
unsigned long Stoul( const string& );
/** changes string into float: */
float Stof( const string& );
/** changes string into double: */
double Stod( const string& );
/** changes string to vector of integer. Format: n1 n2 n3 ... */
vector<int> sToVectorOfInt( const string&);
/** changes string to vector of unsigned integer. Format: n1 n2 n3 ... */
vector<unsigned int> sToVectorOfUInt( const string&);

/** changes integer into string: */
string itos( const int& );
/** changes unsigned integer into string: */
string uitos( const unsigned int& );
/** changes long into string; */
string ltos( const long& );
/** changes float into string: */
string ftos( const float& );
/** changes double into string: */
string dtos( const double& );
/** changes double into string with given precision (digits after decimal) */
string dtos( const double&, int precision );

/** translate vector of char to string */
// string vtos( const Vec<char>& v);

/** returns true, if number in form [+|-]d*.d* with d being a digit
  exponential notation (like 1e-3) is not recognized! */
bool isSimpleDecimal(const string& sOrig);

/** tokenize text line */
vector<string> getTokens(const string& s);

// tokenize text seperated by set of delimiters
vector<string> getTokens(const string& text, const string& delim, bool allowEmpty=true);

// tokenize text seperated by set of delimiters,
// return also vector with position indices of words
vector<string> getTokens(const string& text, const string& delim,
			 vector<unsigned int>& positions);

/** parses a string of the form "-3,5-8,12" to the numbers 1,2,3,5,6,7,8,12 */
Vec<unsigned int>
parseStringToVector(const string& s);

/* auxilary function for file reading */
/* skip whitespace */
void eatWhite(istream& is);

// skips over whitespace and comments (starting with '#')
void eatWhiteAndComment(istream& is);

// skips to new line
void skipToNewLine(istream& is);

// return string with actual line. Reads last newline, but does not include it.
string getLine(istream& is);

/** Returns vector of position/length pairs corresponding to regions in string that consist of character c */
vector<pair<string::size_type, string::size_type> > getRegions(const string& s, char c);

/** return string which consists of concetenated regions represented
    by single character. Characters in removeList are removed. */
string getBlocks(const string& orig, const vector<char>& removeList);

/** return string which consists of concetenated regions represented
    by single character. Characters in removeList are removed. */
string getBlocks(const string& orig);

/** return string with removed identical neighbouring characters */
// string uniqueString(const string& sOrig);

/** remove all occurences of char c from string and return result */
string removeFromString(const string& s, char c);

/** remove all occurences of char c from string and return result */
string removeFromStringParallel(const string& s, char c,
				string other,
				string& otherresult);

/** remove all occurences of any charactor from removeSet from string and 
    return result */
string removeFromString(const string& s, const string&  removeSet);

/** remove all occurences of any charactor from removeSet from string and 
    return result */
string removeWhiteSpaceFromString(const string& s);

/** Deconed run length encoded string */
string runLengthDecode(const string& source, int maxCount);

/** Run length encoding of string */
string runLengthEncode(const string& source, int maxCount);

/** Tests functions runLengthEncode, runLengthDecode */
void testRunLengthEncode();

/** remove all occurences of char c from string and return result */
string squeezeRepeats(const string& s, char c);

/** remove all occurences of char c from string and return result */
string squeezeRepeats(const string& s, const string& letters);

/** 
 * returns true if character is white space 
 * @see http://www.jimprice.com/ascii-0-127.gif  (ASCII table)
 */
inline
bool
isWhiteSpace(char c) {
  bool b1 = ((c == ' ') || (c == '\t') || (c == '\n') || (c == '\r'));
  int ic = (int)c;

  bool b2 = (ic == 9)|| (ic == 10) || (ic == 11) || (ic == 12) || (ic == 13);
  return b1 || b2;
}

/** 
 * removes all occurences of any charactor from removeSet from string and return result 
 * @review  not yet tested!
 */
string trimWhiteSpaceFromString(const string& s);

/** 
 * removes leading or trailing ocurrences of any charactor from letters from string and return result 
 * @review  not yet tested!
 */
string trim(const string& s, const string& letters);

/** replace each occurence of c1 by c2 */
string translate(const string& s, char oldChar, char newChar);

/**
 * Reverses order of string. Not very efficient.
 */
string
reverseString(const string& s);

// replace all letters by upper case version */
void upperCase(string& s);

// replace all letters by lower case version */
void lowerCase(string& s);

// return true if no characters are lower case
bool isAllUpper(const string& s);

// return true if no characters are upper case
bool isAllLower(const string& s);

/** 
 * returns true if content of two strings identical
 */
inline 
bool isEqual(const string& s1, const string& s2) {
  return (s1.compare(s2) == 0);
}

/* print n time character c to stream */
void
printChar(ostream& os, char c, unsigned int n);

/** return vector with positions, at which character c is found in string */
vector<unsigned int> findPositions(const string& s, char c);

// read non-empty lines until end of file reached
vector<string> getLines(istream& is);


/** 
 * returns string with call command
 */
string
callString(int argc, char** argv);

/**
 * interprets environment variables of the form like "HOSTNAME=mast3"
 * would set pararameterName to HOSTNAME and parameterValue to mast3
 * in this case
 */
void
parseParameter(const string& s, string& parameterName, string& paramterValue);

/**
 * tries to find parameterValue of parameter. Entries must have form described in
 * parseparameter. Used to reading environment variables.
 */ 
string
getParameterValue(char **envp, const string& parameterNameOrig,
                  const string& defaultValue);

/** keep only subset of characters defined by subset */
string
getSubset(const string& s, const Vec<unsigned int>& subset);

/** returns slice: only n'th column */
string 
getColumn(const Vec<string>& alignment, unsigned int col);

/** finds index of word in vector of strings */
unsigned int
findWordIndex(const Vec<string>& dict, const string& word);

#include <algorithm>
#include <ctype.h>

// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (integer expected but e.g. float given) then error
// changes string into integer:
int Stoi( const string &s )
{
  return std::stoi(s);
  // int i;
  // double temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s in no number) then error:
  // if( ist == NULL )
  //   {
  //     cerr << "\"" << s << "\" is no integer!!" << endl;
  //     DERROR( "Integer expected" ); 
  //   }
  // // if temp is no integer (temp-static_cast<int>(temp)!=0) then error: 
  // if( (temp - static_cast<int>( temp )) != 0 )
  //   {
  //     cerr << "\"" << s << "\" is no integer!!" << endl;
  //     DERROR( "Wrong format" ); 
  //   }
  // i = static_cast<int>( temp );
  // return i;
}

// author: Eckart Bindewald
// description: same as Stoi, but for unsigned int
// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (integer expected but e.g. float given) then error
// changes string into integer:
unsigned int Stoui( const string &s )
{
  int x = std::stoi(s);
  return static_cast<unsigned int>(x);
  // 
  // unsigned int i;
  // double temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // // istrstream ist( pointer );
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s in no number) then error:
  // if( ist == NULL )
  //   { 
  //     cerr << "\"" << s << "\" is no unsigned integer!!" << endl;
  //     DERROR( "Integer expected" ); 
  //   }
  // // if temp is no integer (temp-static_cast<int>(temp)!=0) then error: 
  // if( (temp - static_cast<unsigned int>( temp )) != 0 )
  //   { 
  //     cerr << "\"" << s << "\" is no unsigned integer!!" << endl;
  //     DERROR( "Wrong format" ); 
  //   }
  // if( i < 0 )
  //   { DERROR( "Negative unsigned integer" ); }
  // i = static_cast<unsigned int>( temp );
  // return i;
}


// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (long expected but e.g. float given) then error
// changes string into long:
long Stol( const string &s )
{
  return std::stol(s);
  // long l;
  // long double temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // // istrstream ist( pointer );
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s in no number) then error:
  // if( ist == NULL )
  //   {
  //     cerr << "\"" << s << "\" is no long integer!!" << endl; 
  //     DERROR( "Long expected" ); 
  //   }
  // // if temp is no long (temp-static_cast<long>(temp)!=0) then error: 
  // if( (temp - static_cast<long>( temp )) != 0 )
  //   {
  //     cerr << "\"" << s << "\" is no long integer!!" << endl; 
  //     DERROR( "Wrong format" ); 
  //   }
  // l = static_cast<long>( temp );
  // return l;
}

// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (long expected but e.g. float given) then error
// changes string into long:
unsigned long Stoul( const string &s )
{
  long x = std::stol(s);
  return static_cast<unsigned long>(x);
  // unsigned long l;
  // long double temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // // istrstream ist( pointer );
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s in no number) then error:
  // if( ist == NULL )
  //   {
  //     cerr << "\"" << s << "\" is no long integer!!" << endl; 
  //     DERROR( "Long expected" ); 
  //   }
  // // if temp is no long (temp-static_cast<long>(temp)!=0) then error: 
  // if( (temp - static_cast<unsigned long>( temp )) != 0 )
  //   {
  //     cerr << "\"" << s << "\" is no long integer!!" << endl; 
  //     DERROR( "Wrong format" ); 
  //   }
  // l = static_cast<unsigned long>( temp );
  // return l;
}

// error: if the input is no number (e.g. char or string)  then error,
// if wrong input (float expected but e.g. double given) then error
// changes string into float:
float Stof( const string &s )
{
  return std::stof(s);
  // float f;
  // float temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // // istrstream ist( pointer );
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s is no number) then error:
  // if( ist == NULL )
  //   { 
  //     cerr << "\"" << s << "\" is no float!!" << endl;
  //     DERROR( "Float expected" ); 
  //   }
  // // if temp is no long (temp-static_cast<float>(temp)!=0) then error: 
  // if( (temp - static_cast<float>( temp )) != 0 )
  //   {
  //     cerr << "\"" << s << "\" is no float!!" << endl;
  //     DERROR( "Wrong format" ); 
  //   }
  // f = temp;
  // return f;
}

// error: if the input is no number (e.g. char or string)  then error
// changes string into double:
double Stod( const string &s )
{
  return std::stod(s);
  // double d;
  // double temp; // temporary integer
  // 
  // const char *pointer;
  // pointer = s.c_str();
  // // istrstream ist( pointer );
  // istringstream ist( pointer );
  // ist >> temp;
  // // if ist==NULL (s is no number) then error:
  // if( ist == NULL )
  //   {
  //     cerr << "\"" << s << "\" is no double!!" << endl;
  //     DERROR( "Wrong format" ); 
  //   }
  // d = temp;
  // return d;
}

/** returns true, if number in form [+|-]d*.d* with d being a digit
  exponential notation (like 1e-3) is not recognized! */
bool
isSimpleDecimal(const string& sOrig) {
  string s = removeWhiteSpaceFromString(sOrig);
  if (s.size() < 1) {
    return false;
  }
  if ((s[0] == '+') || (s[0] == '-')) {
    s = s.substr(1); // remove initial plus or minus
  }
  if (s.size() < 1) {
    return false;
  }
  unsigned int dotCount = 0;
  for (string::size_type i = 0; i < s.size(); ++i) {
    if (s[i] == '.') {
      ++dotCount;
      if (dotCount > 1) {
	return false;
      }
    }
    else if (!isdigit(s[i])) {
      return false;
    }
  }
  return true;
}

// -----------------------------------------------------------
// author: Eckart Bindewald
// name of function: sToVectorOfInt
// description:
// changes string to vector of integer. Format: "n1 n2 n3"
// precondition: spaces between numbers. String can also be enclosed in "..."
// postcondition: the return vector is filled with the apropriate numbers
// error behaviour: quits, if precondition is not fullfilled.
// -----------------------------------------------------------
vector<int> sToVectorOfInt( const string& s_orig) {
  string s = s_orig;
  vector<int> v;
  if (s[1] == '\'') {
    DEBUG_MSG("Removing colons!");
THROWIFNOT(s[s.size()-1]=='\'',"Assertion violation in L485");
#ifdef G_COMPILER
    s.remove( 0U, 1U ); // remove the first letter
    s.remove(s.size()-1U,1U); // remove last letter
#else
    s.erase( 0, 1 ); // remove the first letter
    s.erase(s.size()-1,1); 
#endif
  }
  // istrstream ist(s.c_str()); // see Stroustrup chapter 21.5.3
  istringstream ist(s.c_str()); // see Stroustrup chapter 21.5.3
  string w; // word
  while (ist >> w) {
    v.push_back(Stoi(w));
  }
  return v;
}

// -----------------------------------------------------------
// author: Eckart Bindewald
// name of function: sToVectorOfUInt
// description:
// changes string to vector of unsigned integer. Format: "n1 n2 n3"
// precondition: spaces between numbers. String can also be enclosed in "..."
// postcondition: the return vector is filled with the apropriate numbers
// error behaviour: quits, if precondition is not fullfilled.
// -----------------------------------------------------------
vector<unsigned int> sToVectorOfUInt( const string& s_orig) {
  string s = s_orig;
  vector<unsigned int> v;
  if (s[1] == '\'') {
    DEBUG_MSG("Removing colons!");
THROWIFNOT(s[s.size()-1]=='\'',"Assertion violation in L517");
#ifdef G_COMPILER
    s.remove( 0U, 1U ); // remove the first letter
    s.remove(s.size()-1U,1U); // remove last letter
#else
    s.erase( 0, 1 ); // remove the first letter
    s.erase(s.size()-1,1); 
#endif
  }
  // istrstream ist(s.c_str()); // see Stroustrup chapter 21.5.3
  istringstream ist(s.c_str()); // see Stroustrup chapter 21.5.3
  string w; // word
  while (ist >> w) {
    v.push_back(Stoi(w));
  }
  return v;
}


// changes integer into sting:
string itos( const int &i )
{
  string s;

  // ostrstream os; 
  ostringstream os; 
  os << i; // << '\0';
  return s = os.str();
}

// changes unsigned integer into string:
string uitos( const unsigned int &i )
{
  string s;

  // ostrstream os; 
  ostringstream os; 
  os << i;//  << '\0';
  return s = os.str();
}

// changes long into sting:
string ltos( const long &l )
{
  string s;

  // ostrstream os; 
  ostringstream os; 
  os << l; //  << '\0';
  return s = os.str();
}

// changes float into sting:
string ftos( const float &f )
{
  string s;

  // ostrstream os; 
  ostringstream os; 
  os << f;//  << '\0';
  return s = os.str();
}

// changes double into string:
string dtos( const double &d )
{
  string s;

  // ostrstream os; 
  ostringstream os; 
  os << d; // << '\0';
  return s = os.str();
}

// changes double into string for fixed precision:
string dtos( const double &d, int precision ) {
  string s;
     // ostrstream os; 
   ostringstream os; 
   os.precision(precision);
   os << d; // << '\0';
  return s = os.str();
}  


// string
// vtos(const Vec<char>& v)
// {
//   if (v.size() == 0) {
//     return string("");
//   }
//   string result(v.size(), 'X');
//   for (unsigned int i = 0; i < v.size(); ++i) {
//     result[i] = v[i];
//   }
//   POSTCOND(result.size() == v.size());
//   return result;
// }

// tokenize text
vector<string> getTokens(const string& text) 
{
  vector<string> v;
  if (text.size() == 0) {
    return v;
  }
  // istrstream ist(text.c_str());
  istringstream ist(text.c_str());
  char* charLine = new char[text.size()+1]; // size of string
  string s;
  while (!ist.eof()) {
    ist >> charLine;
    if (!ist) {
      break; // must have been bad read, ignore
    }
    s = charLine; // assignment of c-strings to string!
    if (s != "") { 
      v.push_back(s);
    }
  }
  delete[] charLine;
  return v;
}

// tokenize text seperated by set of delimiters
vector<string> getTokens(const string& text, const string& delim, bool allowEmpty) 
{
  unsigned int oldPos = 0;
  vector<string> result;
  if (text.size() == 0) {
    return result;
  }
  for (unsigned int i = 0; i < text.size(); ++i) {
    if (delim.find(text[i]) < delim.size()) {
      // delimiter found
      string newToken;
      if (i > oldPos) {
        newToken = text.substr(oldPos, i - oldPos);
      }
      if ((newToken.size() > 0) || allowEmpty) {
	result.push_back(newToken);
      }
      oldPos = i + 1;
    }
  }
  if (oldPos < text.size()) {
    string newToken = text.substr(oldPos, text.size() - oldPos);
    if ((newToken.size() > 0) || allowEmpty) {
      result.push_back(newToken);
    }
  }
  return result;
}

// tokenize text seperated by set of delimiters,
// return also vector with position indices of words
vector<string> getTokens(const string& text, const string& delim,
			 vector<unsigned int>& positions) 
{
  unsigned int oldPos = 0;
  vector<string> result;
  positions.clear();
  if (text.size() == 0) {
    return result;
  }
  for (unsigned int i = 0; i < text.size(); ++i) {
    if (delim.find(text[i]) < delim.size()) {
      // delimiter found
      string newToken;
      if (i > oldPos) {
        newToken = text.substr(oldPos, i - oldPos);
      }
      result.push_back(newToken);
      positions.push_back(oldPos);
      oldPos = i + 1;
    }
  }
  if (oldPos < text.size()) {
    string newToken = text.substr(oldPos, text.size() - oldPos);
    result.push_back(newToken);
    positions.push_back(oldPos);
  }
  return result;
}


/** parses a string of the form "5-8" to the numbers 5,6,7,8 */
// Vec<unsigned int>
// parseStringToSubVector(const string& s)
// {
//   Vec<unsigned int> result;
//   const string delimiters = "-";
//   if (s.size() == 0) {
//     return result;
//   }
//   if (s[0] == '-') {   // if like "-5"
//     unsigned int maxInd = Stoui(s.substr(1));
//     for (unsigned int i = 1; i <= maxInd; ++i) {
//       result.push_back(i);
//     }
//   }
//   else if (s.find('-') >= s.size()) { // if like "5"
//     result.push_back(Stoui(s));
//   }
//   else { // if like "5-9"
//     vector<string> words = getTokens(s, delimiters);
//     ERROR_IF(words.size() != 2,
// 	     "minimum and maximum value expected in range specifier!");
//     unsigned int minInd = Stoui(words[0]);
//     unsigned int maxInd = Stoui(words[1]);
//     for (unsigned int i = minInd; i <= maxInd; ++i) {
//       result.push_back(i);
//     }
//   }
//   return result;
// }

/** parses a string of the form "-3,5-8,12" to the numbers 1,2,3,5,6,7,8,12 */
// Vec<unsigned int>
// parseStringToVector(const string& s)
// {
//   Vec<unsigned int> result;
//   string delimiters = ",";
//   vector<string> words = getTokens(s, delimiters);
//   for (unsigned int i = 0; i < words.size(); ++i) {
//     Vec<unsigned int> subVec = parseStringToSubVector(words[i]);
//     for (unsigned int j = 0; j < subVec.size(); ++j) {
//       result.push_back(subVec[j]);
//     }
//   }
//   return result;
// }


// reads until newline is found
void 
skipToNewLine(istream& is)
{
  char c;
  do {
    is.get(c);
  } while((!is.eof()) && (c != '\n'));
}

/* auxilary function for file reading */
/* skip whitespace */
void 
eatWhite(istream& is)
{
  char c;
  while(is.get(c)) {
    if (!isspace(c)) {
      is.putback(c);
      break;
    }
  }
}


// skips over whitespace and comments (starting with '#')
void 
eatWhiteAndComment(istream& is)
{
  char c;
  do {
    eatWhite(is);
    is.get(c);
    if (c != '#') {
      is.putback(c);
      break;
    }
    skipToNewLine(is); // search next new line
  }
  while (!is.eof());
}

// return string with actual line. Reads last newline, but does not include it.
string
getLine(istream& is)
{
  char c;
  string resultString; 
  is.get(c);
  while ((is) && (!is.eof()) && (c != '\n') )
    {
      resultString = resultString + c;
      is.get(c);
    }
  return resultString;
}

vector<string>
getLines(istream& is)
{
  vector<string> result;
  while (is) {
    string line = getLine(is);
    if (line.size() > 0) {
      result.push_back(line);
    }
  }
  return result;
}



/** return string with removed identical neighbouring characters */
//  string uniqueString(const string& sOrig) 
//  {
//      string s = sOrig;
//      typename string::iterator p = unique(s.begin(),s.end());
//      // remove invalid end. uniq puts the result at the beginning
//      s.erase(p,s.end()); 
//      return s;
//  }

/** Returns vector of position/length pairs corresponding to regions in string that consist of character c */
vector<pair<string::size_type, string::size_type> >
getRegions(const string& s, char c) {
  string::size_type firstPos = s.size();
  vector<pair<string::size_type, string::size_type> > result;
  for (string::size_type i = 0; i < s.size(); ++i) {
    if (s[i] == c) {
      if (firstPos >= s.size()) {
	firstPos = i;
      }
    } else { // s[i] != c
      if (firstPos < s.size()) {
	// save:
THROWIFNOT(i > firstPos,"Assertion violation in L846");
	result.push_back(pair<string::size_type, string::size_type>(firstPos, i-firstPos));
	firstPos = s.size();
      }
    }
  } 
  if (firstPos < s.size()) { // close open interval
    result.push_back(pair<string::size_type, string::size_type>(firstPos, s.size()-firstPos));
    firstPos = s.size();
  }
  return result;
}


/** return string which consists of concetenated regions represented
    by single character */
string getBlocks(const string& orig, const vector<char>& removeList)
{
  DERROR("not readily implemented method called.");
  string result = orig; // uniqueString(orig);
  string final;
  for (unsigned int j = 0; j < result.size(); ++j) {
    // copy only if not found in removeList
    if (find(removeList.begin(), removeList.end(),result[j]) 
	== removeList.end()) {
      final = final + result[j];
    }
  }
THROWIFNOT(count(final.begin(), final.end(),removeList[0]) == 0,"Assertion violation in L874");
  return final;
}

/** return string which consists of concetenated regions represented
    by single character */
string getBlocks(const string& orig)
{
  if (orig.size() == 0) {
    return orig;
  }
  string result = orig; // uniqueString(orig);
  string final;
  final = final + result[0];
  for (unsigned int j = 1; j < result.size(); ++j) {
    // copy only if not found in removeList
    //      if (find(removeList.begin(), removeList.end(),result[j])
    //          == removeList.end()) {
    if (result[j] != result[j-1]) {
      final = final + result[j];
    }
  }
THROWIFNOT(count(final.begin(), final.end(),' ') == 0,"Assertion violation in L896");
  return final;
}


/** remove all occurences of char c from string and return result */
string removeFromString(const string& s, char c)
{
  string result = "";
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (s[i] != c) {
      result.append(1,s[i]);
    }
  }
  return result;
}

/** Run length encoding of string */
string runLengthEncode(const string& source, int maxCount) {
  if (source.size() < 4) {
    return  source;
  }
  string dest(source.size() + 1, ' '); // one longer: original length is stored in first element!
  int counter = -1;
  int nm1 = static_cast<int>(source.size()) - 1;
  int pc = 0;
  for (int i = 0; i < nm1; i++) {
    if ((source[i] != source[i + 1]) || ((i-counter) >= maxCount)) {
      if ((i - counter) > 1) {
	dest[pc++] = (i - counter);
THROWIFNOT(dest[pc-1] <= maxCount,"Assertion violation in L926");
	dest[pc++] = source[i];
       } else { // only single occurence, do not encode
 	ASSERT((i - counter) == 1); // zero or negative counts do not make sense
 	dest[pc++] = source[i];
       }
      counter = i;
    }
  }
  dest[pc++] = (source.size() - counter - 1); // special case for last element
  dest[pc++] = (source[source.length() - 1]);
  return dest.substr(0,pc);
}
 
/** Deconed run length encoded string */
string runLengthDecode(const string& source, int maxCount) {
  // first find length:
  int expanded = 0;
  for (string::size_type i = 0; i < source.size(); ++i) {
    if (source[i] <= maxCount) {
      expanded += source[i]-2;
    }
  } 
  string dest(source.size() + expanded, 'x');
  char c;
  int count;
  string::size_type i = 0;
  string::size_type pc = 0;
  while (i < source.size()) {
    if (source[i] <= maxCount) {
      count = source[i];
      ++i;
      c = source[i];
      for (int j = 0; j < count; ++j) {
	dest[pc++] = c;
      }
    } else {
      dest[pc++] = source[i];
    }
    ++i;
  } 
  if (pc != dest.size()) {
    Rcpp::Rcout << "Problem decoding string: " << pc << " " << dest.size() << " " << dest << endl;
  }
THROWIFNOT(pc == dest.size(),"Assertion violation in L970");
  return dest;
}

/** Tests functions runLengthEncode, runLengthDecode */
void testRunLengthEncode() {
  Rcpp::Rcout << "Starting testRunLengthEncode" << endl;
  string example = "WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWBBWWWWWWWWWWWWWW";
  Rcpp::Rcout << "Example" << example << endl;
  string encoded = runLengthEncode(example, 20);
  string decoded = runLengthDecode(encoded, 20);
  for (string::size_type i = 0; i < encoded.size(); ++i) {
    Rcpp::Rcout << (i+1) << " " << static_cast<int>(encoded[i]) << " " << encoded[i] << endl;
  }
  Rcpp::Rcout << "Decoded: " << decoded << endl;
  Rcpp::Rcout << "Lengths: " << example.size() << " " << encoded.size() << " " << decoded.size() << endl;
THROWIFNOT(decoded == example,"Assertion violation in L986");
  Rcpp::Rcout << runLengthDecode("1W1B1W1B1W1B1W1B1W1B1W1B1W1B", 42) << endl;
  Rcpp::Rcout << "Finished testRunLengthEncode" << endl;
}

/** remove all occurences of char c from string and return result */
string squeezeRepeats(const string& s, char c)
{
  string result = "";
  bool first = true;
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (s[i] != c) {
      result.append(1,s[i]);
      first = true;
    }
    else if (first) {
      result.append(1,s[i]);
      first = false;
    } // else ignore subsquent letters
  }
  return result;
}


/** remove all occurences of char c from string and return result */
string squeezeRepeats(const string& s, const string& letters)
{
  string result = s;
  for (unsigned int i = 0; i < letters.size(); ++i) {
    result = squeezeRepeats(result, letters[i]);
  }
  return result;
}

/** remove all occurences of char c from string and return result,
 apply same transformation to other */
string removeFromStringParallel(const string& s, char c,
				string other,
				string& otherresult)
{
  string result = "";
  otherresult = "";
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (s[i] != c) {
      result.append(1,s[i]);
      otherresult.append(1, other[i]);
    }
  }
  return result;
}

/** remove all occurences of any charactor from removeSet from string and 
    return result */
string removeFromString(const string& s, const string&  removeSet)
{
  string result = s;
  for (unsigned int j = 0; j < removeSet.size(); ++j) {
    result = removeFromString(result,removeSet[j]);
  }
  return result;
}

/** 
 * removes all occurences of whitespace from string and returns result 
 */
string removeWhiteSpaceFromString(const string& s)
{
  string whiteSpace(" \t\n\r");
  return removeFromString(s, whiteSpace);
}

/** remove all leading or trailing white space from string and return result
 * @review  not yet tested!
 */
string trimWhiteSpaceFromString(const string& s)
{
  // find first position of non-whitespace charactor
  int idxFirst = 0;
  if (s.size() == 0) {
    return s;
  }
  for (idxFirst = 0; idxFirst < static_cast<int>(s.size()); ++idxFirst) {
    if (!isWhiteSpace(s[idxFirst])) {
      break;
    }
  }
  int idxLast = s.size();
  for (idxLast = static_cast<int>(s.size() - 1); idxLast >= 0; --idxLast) {
    if (!isWhiteSpace(s[idxLast])) {
      break;
    }
  }
  if ((idxLast < 0) || (idxLast < idxFirst)) {
    return string(""); // return empty string
  }
  return s.substr(idxFirst, (idxLast - idxFirst + 1)); 
}

/** remove all leading or trailing characters contained in "letters" from string and return result
 * @review  not yet tested!
 */
string trim(const string& s, const string& letters)
{
  // find first position of non-whitespace charactor
  int idxFirst = 0;
  if (s.size() == 0) {
    return s;
  }
  for (idxFirst = 0; idxFirst < static_cast<int>(s.size()); ++idxFirst) {
    if (letters.find(s[idxFirst]) >= letters.size()) {
      break;
    }
  }
  int idxLast = s.size();
  for (idxLast = static_cast<int>(s.size() - 1); idxLast >= 0; --idxLast) {
    if (letters.find(s[idxLast]) >= letters.size()) {
      break;
    }
  }
  if ((idxLast < 0) || (idxLast < idxFirst)) {
    return string(""); // return empty string
  }
  return s.substr(idxFirst, (idxLast - idxFirst + 1)); 
}


/** replace each occurence of c1 by c2 */
string
translate(const string& s, char c1, char c2)
{
  string t = s;
  for (unsigned int i = 0; i < t.size(); ++i) {
    if (t[i] == c1) {
      t[i] = c2;
    }
  }
  return t;
}


// replace all letters by upper case version */
void
upperCase(string& s)
{
  for (unsigned int i = 0; i < s.size(); ++i) {
    s[i] = toupper(s[i]);
  }
}


// replace all letters by lower case version */
void
lowerCase(string& s)
{
  for (unsigned int i = 0; i < s.size(); ++i) {
    s[i] = tolower(s[i]);
  }
}

// return true if no characters are lower case
bool
isAllUpper(const string& s)
{
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (islower(s[i])) {
      return false;
    }
  }
  return true;
}

// return true if no characters are upper case
bool
isAllLower(const string& s)
{
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (isupper(s[i])) {
      return false;
    }
  }
  return true;
}

/* print n time character c to stream */
void
printChar(ostream& os, char c, unsigned int n)
{
  for (unsigned int i = 0; i < n; ++i) {
    os << c;
  }
}

/* return vector with positions, at which character c is found in string */
vector<unsigned int>
findPositions(const string& s, char c)
{
  vector<unsigned int> result;
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (s[i] == c) {
      result.push_back(i);
    }
  }
  return result;
}

/** 
 * returns string with call command
 */
string
callString(int argc, char** argv)
{
  string result;
  for (int i = 0; i < argc; ++i) {
    result = result + argv[i] + " ";
  }
  return result;
}

/**
 * interprets environment variables of the form like "HOSTNAME=mast3"
 * would set pararameterName to HOSTNAME and parameterValue to mast3
 * in this case
 */
void
parseParameter(const string& s, string& parameterName, string& parameterValue)
{
  parameterName = "";
  parameterValue = "";
  const char EQUAL_CHAR = '=';
  // find equal sign:
  vector<unsigned int> equalPosVec = findPositions(s, EQUAL_CHAR);
  if (equalPosVec.size() != 1) {
    return; // no equal sign found
  }
  unsigned int pos = equalPosVec[0];
  parameterName = s.substr(0, pos);
  if ((pos + 1) < s.size()) {
    parameterValue = s.substr(pos + 1);
  }
}

/**
 * tries to find parameterValue of parameter. Entries must have form described in
 * parseparameter. Used to reading environment variables.
 */ 
string
getParameterValue(char **envp, const string& parameterNameOrig, const string& defaultValue)
{
  for (int i= 0; envp[i] != 0; i++) {
    string envpString(envp[i]);
    string parameterName = "";
    string parameterValue = "";
    parseParameter(envpString, parameterName, parameterValue);
    if (parameterName.compare(parameterNameOrig) == 0) {
      return parameterValue;
    }
  }
  return defaultValue;
}

/**
 * Reverses order of string. Not very efficient.
 */
string
reverseString(const string& s)
{
  string result;
  for (unsigned int i = 0; i < s.size(); ++i) {
    result = s[i] + result;
  }
  return result;
}

string
getSubset(const string& s, const Vec<unsigned int>& subset)
{
  string result(subset.size(), 'X');
  for (unsigned int i = 0; i <  subset.size(); ++i) {
    result[i] = s[subset[i]];
  }
  return result;
}

/** returns slice: only n'th column */
string 
getColumn(const Vec<string>& alignment, unsigned int col)
{
  string result(alignment.size(), 'X');
  for (unsigned int i = 0; i < alignment.size(); ++i) {
    result[i] = alignment[i][col];
  }
  return result;
}

/** finds index of word in vector of strings. Needs complete match. */
unsigned int
findWordIndex(const Vec<string>& dict, const string& word)
{
  for (unsigned int i = 0; i < dict.size(); ++i) {
    if (dict[i].compare(word) == 0) {
      return i;
    }
  }
  return dict.size();
}



#endif // __STRING_2_NUMBER_H__



