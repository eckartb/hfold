#ifndef _CONNECTIVITY_TOOLS_
#define _CONNECTIVITY_TOOLS_

#include "PurgePriorityQueue.h"
#include "GraphTools.h"

class ConnectivityTools {

 public:

  ConnectivityTools() {
    bool verbose = true;
    ASSERT(testConnectivityToHashes(verbose)); // self test
  }


  static Vec<string> connectivityToHashes(const adjacency_matrix_t& matrix) {
    adjacency_list_t adjList = adjacency_matrix_to_adjacency_list(matrix);
    graph_id_sets_t components = adjacency_to_components(adjList);
    Vec<string> result;
    for (graph_id_sets_t::const_iterator it = components.begin(); it != components.end(); it++) {
THROWIFNOT((*it).size() > 0,"Assertion violation in L22");
      result.push_back(idsToHash(*it));
    }
    sort(result.begin(), result.end());
    return result;
  }

  static Vec<string> connectivityToHashes(const adjacency_matrix_t& matrix,
					  const map<size_t, string>& synonyms, const string& sep="") {
    adjacency_list_t adjList = adjacency_matrix_to_adjacency_list(matrix);
    graph_id_sets_t components = adjacency_to_components(adjList);
    Vec<string> result;
    for (graph_id_sets_t::const_iterator it = components.begin(); it != components.end(); it++) {
THROWIFNOT((*it).size() > 0,"Assertion violation in L35");
      result.push_back(idsToHash(*it, synonyms));
    }
    sort(result.begin(), result.end());
    return result;
  }
  
  
  static double computePartitionFunction(PurgePriorityQueue<double> connectivityEnergies, double RT) {
    double result = 0;
    while (connectivityEnergies.size() > 0) {
      double energy = - connectivityEnergies.top(); // negative energies are stored
      connectivityEnergies.pop();
THROWIFNOT(RT > 0.0,"Assertion violation in L48");
      result += exp(-energy/RT);
    }
    return result;
  }

  static map<string, double> computePartitionFunction(const map<string, PurgePriorityQueue<double> >& connectivityEnergies, double RT) {
    map<string, double> result;
    for (map<string, PurgePriorityQueue<double> >::const_iterator it = connectivityEnergies.begin(); it != connectivityEnergies.end(); it++) {
      string connectivity = it->first;
      double partitionFunction = computePartitionFunction(it->second, RT);
      result[connectivity] = partitionFunction;
    }
    return result;
  }

  /** Computes indirect adjacency from adjacency matrix (a matrix containing ones and zeros) */
  static adjacency_matrix_t connectivityToIndirectConnectivity(const adjacency_matrix_t& matrix) {
    adjacency_list_t adjList = adjacency_matrix_to_adjacency_list(matrix);
    graph_id_sets_t components = adjacency_to_components(adjList);
    adjacency_matrix_t result = matrix;
    for (graph_id_sets_t::const_iterator it = components.begin(); it != components.end(); it++) {
THROWIFNOT((*it).size() > 0,"Assertion violation in L70");
      for (graph_id_set_t::const_iterator it2 = it->begin(); it2 != (*it).end(); ++it2) {
	for (graph_id_set_t::const_iterator it3 = it->begin(); it3 != (*it).end(); ++it3) { // do not count self-connection
	  if (it2 != it3) {
	    result[*it2][*it3] = 1;
	    result[*it3][*it2] = 1;
	  }
	}
      }
    }
    return result;
  }


  /** Convert connectivity hash to vector of vectors such as  1_3|2  becomes (1,3), (2) */
  static
  graph_id_sets_t
  hashToComponents(const string& hash) {
    // divide by "|":
    vector<string> tokens = getTokens(hash, "|");
    graph_id_sets_t result;
    for (size_t i = 0; i < tokens.size(); ++i) {
      vector<string> tokens2 = getTokens(hash, "_");
      graph_id_set_t subset;
      for (size_t j = 0; j < tokens2.size(); ++j) {
	int id = stoi(tokens2[j])-1; // from one-based to zero-based
THROWIFNOT(id >= 0,"Assertion violation in L96");
	subset.insert(static_cast<graph_base_t>(id));
      }
      result.insert(subset);
    }
    return result;
  }

  /** Converts molecular connectivity to one combined hash of form 1|2_4|3 (example in which strands 2 and 4 (internal counting 1 and 3) are connected, strands 1 and 3 are uncomplexed */ 
  static string connectivityToCombinedHash(const adjacency_matrix_t& matrix) {
    Vec<string> hashes = connectivityToHashes(matrix);
THROWIFNOT(hashes.size() > 0,"Assertion violation in L107");
    string result = hashes[0];
    for (Vec<string>::size_type i = 1; i < hashes.size(); ++i) {
      result = result + "|" + hashes[i];
    }
    return result;
  }


  /** Convert sets of integers to hash string. Example: Converts set(2,7,9) into 2_7_9 */
  static string idsToHash(const Vec<size_t>& ids) {
THROWIFNOT(ids.size() > 0,"Assertion violation in L118");
    string result = "";
    string HASH_SEPERATOR = "_";
    for (Vec<size_t>::const_iterator it = ids.begin(); it != ids.end(); it++) {
      if (result.size() > 0) {
	result = result + HASH_SEPERATOR;
      }
      result = result + itos(*it + 1); // one-based instead of zero-based (should not matter except human readability)
    }
    return result;
  }
  
  /** Convert sets of integers to hash string. Example: Converts set(2,7,9) into 2_7_9 */
  static string idsToHash(const Vec<size_t>& ids, const map<size_t, string>& synonyms) {
THROWIFNOT(ids.size() > 0,"Assertion violation in L132");
    string sep = "";
    string result = "";
    string HASH_SEPERATOR = sep;
    for (Vec<size_t>::const_iterator it = ids.begin(); it != ids.end(); it++) {
      if (result.size() > 0) {
	result = result + HASH_SEPERATOR;
      }
      map<size_t, string>::const_iterator it2 = synonyms.find(*it);
THROWIFNOT(it2 != synonyms.end(),"Assertion violation in L141");
      result = result + it2->second; // one-based instead of zero-based (should not matter except human readability)
    }
    return result;
  }

  /** Convert sets of integers to hash string. Example: Converts set(2,7,9) into 2_7_9 */
  static string idsToHash(const graph_id_set_t& ids, const map<size_t, string>& synonyms) {
THROWIFNOT(ids.size() > 0,"Assertion violation in L149");
    string sep = "";
    string result = "";
    string HASH_SEPERATOR = sep;
    for (graph_id_set_t::const_iterator it = ids.begin(); it != ids.end(); it++) {
      if (result.size() > 0) {
	result = result + HASH_SEPERATOR;
      }
      map<size_t, string>::const_iterator it2 = synonyms.find(*it);
      ERROR_IF(it2 == synonyms.end(), "Internal error: undefined sequence synonym.");
      result = result + it2->second; // one-based instead of zero-based (should not matter except human readability)
    }
    return result;
  }
  
 public:

  /** Convert sets of integers to hash string. Example: Converts set(2,7,9) into 2_7_9 */
  static string idsToHash(const graph_id_set_t& ids) {
THROWIFNOT(ids.size() > 0,"Assertion violation in L168");
    string result = "";
    string HASH_SEPERATOR = "_";
    for (graph_id_set_t::const_iterator it = ids.begin(); it != ids.end(); it++) {
      if (result.size() > 0) {
	result = result + HASH_SEPERATOR;
      }
      result = result + itos(*it + 1); // one-based instead of zero-based (should not matter except human readability)
    }
    return result;
  }


  static bool testConnectivityToHashes(bool verbose) {
    return testConnectivityToHashes0(verbose)
      && testConnectivityToHashes1(verbose)
      && testConnectivityToHashes2(verbose)
      && testConnectivityToHashes3(verbose)
      && testConnectivityToHashes4(3);
  }

  static bool testConnectivityToHashes0(bool verbose) {
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    Vec<string> result = connectivityToHashes(matrix);
    // if (verbose) {
    //   Rcpp::Rcout << "Test 0 of connectivityToHashes:" << endl << result;
    //   Rcpp::Rcout << "Graph: " << matrix << endl;
    // }
THROWIFNOT(result.size() == 3,"Assertion violation in L196");
    ASSERT(result[0] == "1"); // hashes are one-based for reasons of readability
THROWIFNOT(result[1] == "2","Assertion violation in L198");
THROWIFNOT(result[2] == "3","Assertion violation in L199");
    return true;
  }

  static bool testConnectivityToHashes1(bool verbose) {
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[2][0] = 1;
    Vec<string> result = connectivityToHashes(matrix);
    // if (verbose) {
    //   Rcpp::Rcout << "Test 1 of connectivityToHashes:" << endl << result;
    //   Rcpp::Rcout << "Graph: " << matrix << endl;
    // }
THROWIFNOT(result.size() == 2,"Assertion violation in L212");
THROWIFNOT(result[0] == "1_3","Assertion violation in L213");
THROWIFNOT(result[1] == "2","Assertion violation in L214");
    return true;
  }

  /** 0 is connected with 1, 1 is connected with 2: so 0-1-2 should form one group! */
  static bool testConnectivityToHashes2(bool verbose) {
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[2][0] = 1;
    matrix[0][1] = 1;
    matrix[1][0] = 1;
    matrix[1][2] = 1;
    matrix[2][1] = 1;

    Vec<string> result = connectivityToHashes(matrix);
    // if (verbose) {
    //   Rcpp::Rcout << "Test 2 of connectivityToHashes:" << endl << result;
    //   Rcpp::Rcout << "Graph: " << matrix << endl;
    // }
THROWIFNOT(result.size() == 1,"Assertion violation in L233");
THROWIFNOT(result[0] == "1_2_3","Assertion violation in L234");
    return true;
  }

  /** 0 is connected with 1, 1 is connected with 2: so 0-1-2 should form one group! */
  static bool testConnectivityToHashes3(bool verbose) {
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[1][2] = 1;
    matrix[2][1] = 1;
    matrix[2][0] = 1;
    Vec<string> result = connectivityToHashes(matrix);
    // if (verbose) {
    //   Rcpp::Rcout << "Test 3 of connectivityToHashes:" << endl << result;
    //   Rcpp::Rcout << "Graph: " << matrix << endl;
    // }
THROWIFNOT(result.size() == 1,"Assertion violation in L250");
THROWIFNOT(result[0] == "1_2_3","Assertion violation in L251");
    return true;
  }

   static bool testConnectivityToHashes4(size_t dim) {
     adjacency_matrix_t matrix(dim, adjacency_matrix_row_t(dim, GRAPH_NO_CONNECTION));
     Vec<string> result = connectivityToHashes(matrix);
THROWIFNOT(result.size() == dim,"Assertion violation in L258");
     return result.size() == dim;
  }


};

#endif
