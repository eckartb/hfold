#ifndef _RNA_FORMATS_
#define _RNA_FORMATS_

#include <ctype.h>
#include "Vec.h"

/** Handles different file format ids and names related to RNA */
class RnaFormats {

 public:
  //  region_format: 1; bracket_2: rnafold:8 wmatch: 9; 17: rnaeval; 18: pseudoviewer; 22: fasta
  enum { UNKNOWN_FORMAT = -1, REGION_FORMAT = 1, BRACKET_FORMAT = 2, MATRIX_FORMAT = 3, CT_FORMAT = 4, SINGLESEQ_BP_FORMAT = 6, FASTA_FORMAT = 22, FASTA_BRACKET_FORMAT = 29, MATRIX_SUM_FORMAT = 30, SIMPLE_ALIGNMENT_FORMAT = 31, RNACOFOLD_FORMAT = 32, MCSOPT_FORMAT=33, FOLDING_EVENT_FORMAT=40 };
  // enum { UNKNOWN_FORMAT = -1, RNACOFOLD_FORMAT = 2, FASTA_FORMAT = 1, MATRIX_FORMAT = 3, CT_FORMAT = 4, SINGLESEQ_BP_FORMAT = 6, FASTA_ALIGNMENT_FORMAT = 5, MATRIX_SUM_FORMAT = 30, SIMPLE_ALIGNMENT_FORMAT = 31 };
  // FASTA_ALIGNMENT_FORMAT = 5, 
    /** Converts format name (as string) into the internal integer id */
    static int findFormatId(const string& format) {
       if (format == "reg") { 
 	return REGION_FORMAT; 
      } else if (format == "brk") {
	return  BRACKET_FORMAT;
      } else if (format == "matrix") {
	return MATRIX_FORMAT;
      } else if (format == "ct") {
	return CT_FORMAT;
      } else if (format == "bpl") {
	return SINGLESEQ_BP_FORMAT;
	// } else if (format == "fali") {
	// return FASTA_ALIGNMENT_FORMAT;
      } else if (format == "sum") {
	return MATRIX_SUM_FORMAT;
      } else if (format == "fasta") {
	return  FASTA_FORMAT;
      } else if (format == "sali") {
	return SIMPLE_ALIGNMENT_FORMAT;
      } else if (format == "cofold") {
	return RNACOFOLD_FORMAT;
      }
      return UNKNOWN_FORMAT;
    }

    /** Converts format name (as string) into the internal integer id */
    static string findFormatName(int format) {
      switch(format) {
      case REGION_FORMAT:
	return "reg";
      case BRACKET_FORMAT:
	return "brk";
      case  FASTA_FORMAT:
	return "fasta";
      case MATRIX_FORMAT:
	return "matrix";
      case CT_FORMAT:
	return "ct";
      case SINGLESEQ_BP_FORMAT:
	return "bpl";
/*       case FASTA_ALIGNMENT_FORMAT: */
/* 	return "fali"; */
      case MATRIX_SUM_FORMAT:
	return "sum";
      case SIMPLE_ALIGNMENT_FORMAT:
	return "sali";
      case RNACOFOLD_FORMAT:
	return "cofold";
      }
      return "unknown";
    }

    /** Converts format names (as string) into the internal integer ids */
    static Vec<int> findFormatIds(const vector<string>& formats) {
      Vec<int> result(formats.size(), UNKNOWN_FORMAT);
      for (vector<string>::size_type i = 0; i < formats.size(); ++i) {
	if (isdigit(formats[i][0])) {
	  result[i] = stoi(formats[i]);
	} else {
	  result[i] = findFormatId(formats[i]);
	}
      }
      return result;
    }

};

#endif


