#ifndef RESIDUE_PAIRING_H
#define RESIDUE_PAIRING_H

#include <string>
#include <math.h>
#include <map>
#include <cctype>
#include "debug.h"
#include "Stem.h"
#include "ResidueModel.h"
#include "StrandContainer.h"
#include "EntropyTools.h"
#include "ConnectivityTools.h"
#include "StringTools.h"
#include "stemhelp.h"
// #include "ResiduePairingTools.h"

#define NO_PAIRING_INDEX -1

class ResiduePairing : public Vec<int> {

 public:

  typedef string::size_type size_t;

  typedef Stem::index_type index_t;
  
  typedef pair<index_t, index_t> basepair_t;

  /** Are strands not connected, directly connected or indirectly connected? */
  enum { NO_CONN = 0, DIRECT_CONN=1, INDIR_CONN=2 };

 private:

  string helicesHash;

  map<string, double> properties;

  Vec<Stem> helices;

  ResidueModel * residueModel;

  StrandContainer* sequences;

  double stackingEnergy;

  string strandConnectivityHash;

  Vec<Vec<int> > strandConnectivity;

  Vec<Vec<int> > strandIndirectConnectivity;

  Vec<Vec<set<basepair_t> > > strandConnectivitySets;

  mutable double simulationTime;

 public:

 ResiduePairing() : residueModel(NULL), sequences(NULL),stackingEnergy(0.0) { }
  
 ResiduePairing(StrandContainer* _sequences, ResidueModel* _residueModel) : Vec<int>(_sequences->getTotalLength(), NO_PAIRING_INDEX), residueModel(_residueModel), sequences(_sequences) {
    DEBUG_MSG("# Starting constructor of ResiduePairing...");
    init();
    if (!validate()) {
      Rcpp::Rcout << "Structure does not validate!!!" << endl;
      Rcpp::Rcout << size() << " " << hasProperty("negenerg") << " "
    	   << countBasePairs() << " " << countBasePairs2() << " " << validateHelices() << " " << validatePairing() << endl;
    }
    ERROR_IF(!validate(), "Error creating ResiduePairing object from sequence information.");
    DEBUG_MSG("# Finished constructor of ResiduePairing!");
 }

 ResiduePairing(StrandContainer* _sequences) : Vec<int>(_sequences->getTotalLength(), NO_PAIRING_INDEX), residueModel(NULL), sequences(_sequences) {
    init();
    ERROR_IF(!validate(), "Error creating ResiduePairing object from sequence information!");
 }

 ResiduePairing(size_t n, ResidueModel* _residueModel) : Vec<int>(n, NO_PAIRING_INDEX), residueModel(_residueModel), sequences(NULL) {
    init();
    ERROR_IF(!validate(), "Error creating ResiduePairing object from size information...");
 }

  ResiduePairing(const ResiduePairing& other) {
    copy(other);
  }

  ResiduePairing& operator = (const ResiduePairing& other) {
    if (this != &other) {
      copy(other);
    }
    return (*this);
  }

  friend bool operator < (const ResiduePairing& pairing1, const ResiduePairing& pairing2) {
    if (pairing1.size() != pairing2.size()) {
      return pairing1.size() < pairing2.size();
    }
    for (ResiduePairing::size_t i = 0; i < pairing1.size(); ++i) {
      if (pairing1[i] != pairing2[i]) {
	return pairing1[i] < pairing2[i];
      }
    }

    return false; // must be equal, so pairing1 is not smaller pairing2
  }

  friend bool operator > (const ResiduePairing& pairing1, const ResiduePairing& pairing2) {
    if (pairing1.size() != pairing2.size()) {
      return pairing1.size() > pairing2.size();
    }
    for (ResiduePairing::size_t i = 0; i < pairing1.size(); ++i) {
      if (pairing1[i] != pairing2[i]) {
	return pairing1[i] > pairing2[i];
      }
    }

    return false; // must be equal, so pairing1 is not smaller pairing2
  }

  /** "Sanity check" if all helices correspond to Watson-Crick complementary base pairs or Wobble base pairs. */
  virtual bool checkComplementary() {
    const string& s = sequences->getConcatSequence();
    for (size_t i = 0; i < helices.size(); ++i) {
      if (!isComplementary(helices[i], s)) {
	return false; // should never happen!
      }
    }
    return true;
  }

  void copy(const ResiduePairing& other) {
    // Rcpp::Rcout << "Starting copy!"<< endl;
    // PRECOND(validate());
    // Rcpp::Rcout << "Validating other object..."<< endl;
    // PRECOND(other.validate());
    // Rcpp::Rcout << "Successfully Validated other object..."<< endl;
    Vec<int>::operator=(other);
    helicesHash = other.helicesHash;
    properties = other.properties;
    helices = other.helices;
    sequences = other.sequences; // shallow copy
    residueModel = other.residueModel; // shallow copy
    simulationTime = other.simulationTime;
    stackingEnergy = other.stackingEnergy;
    strandConnectivity = other.strandConnectivity;
    strandConnectivityHash = other.strandConnectivityHash;
    strandConnectivitySets = other.strandConnectivitySets;
    strandIndirectConnectivity = other.strandIndirectConnectivity;
    // Rcpp::Rcout << "validating results..."<< endl;
    // POSTCOND(validate());
    // Rcpp::Rcout << "Finished copy!"<< endl;
  }

  /** Return G and not dG ! More precicely H and not dH (Helmholtz free energy) */
  double computeAbsoluteFreeEnergy() const {
    PRECOND(hasProperty("negenergy"));
    PRECOND(hasProperty("log_multiplicity"));
    double energy  = - getProperty("negenergy"); // intra-helix energies
    double loopEnergy  = getProperty("loop_free_energy"); // entropic penalty of loop formation (positive terms)
    double freeEnergy = energy + loopEnergy - residueModel->getRT() * getProperty("log_multiplicity");
    freeEnergy += stackingEnergy; // coaxial stacking
    // setProperty("free_energy", freeEnergy); 
    return freeEnergy;
  }

  /** Return energy of folding (helix energies and stacking energies */
  double computeAbsoluteEnergy() const {
    PRECOND(hasProperty("negenergy"));
    double energy  = - getProperty("negenergy"); // intra-helix energies
    // double freeEnergy = energy - residueModel->getRT() * getProperty("log_multiplicity");
    energy += stackingEnergy; // coaxial stacking
    return energy;
  }

  /** Free energy minus free energy of reference state (typically the unfolded state */
  double computeRelativeFreeEnergy(double gibbs0) const {
    return computeAbsoluteFreeEnergy() - gibbs0;
  }

  /** Computes loop free energy contribution according to Matthews/Turner 2004 model */
  double computeLoopFreeEnergy() {
    // DEBUG_MSG("Starting computeLoopFreeEnergy");
    Vec<Vec<Vec<int> > > loops = findLoops();
THROWIFNOT(loops.size() == 2,"Assertion violation in L183");
    Vec<Vec<int> > internalLoops = loops[0]; // positions and lengths
    Vec<Vec<int> > hairpins = loops[1]; // positions and lengths
    // Rcpp::Rcout << "Internal loops: " << internalLoops[0] << " : " << internalLoops[1] << endl;
    // Rcpp::Rcout << "Hairping loops: " << hairpins[0] << " : " << hairpins[1] << endl;
    double result = 0.0;
THROWIFNOT(internalLoops.size() == 2,"Assertion violation in L189");
    for (size_t i = 0; i < internalLoops[1].size(); ++i) {
      result += EntropyTools::computeTurner04BulgeFreeEnergy(internalLoops[1][i]);
    }
THROWIFNOT(hairpins[0].size() == hairpins[1].size(),"Assertion violation in L193");
    for (size_t i = 0; i < hairpins[1].size(); ++i) {
      // Rcpp::Rcout << "starting " << hairpins[1][i] << endl;
      // result += EntropyTools::computeTurner04HairpinFreeEnergy(hairpins[1][i]);
THROWIFNOT(hairpins[0][i] > 0,"Assertion violation in L197");
THROWIFNOT(hairpins[0][i] + hairpins[1][i] < static_cast<index_t>(sequences->getTotalLength()),"Assertion violation in L198");
THROWIFNOT(!isBasePaired(hairpins[0][i]),"Assertion violation in L199");
THROWIFNOT(!isBasePaired(hairpins[0][i] + hairpins[1][i]-1),"Assertion violation in L200");
THROWIFNOT(isBasePaired(hairpins[0][i])-1,"Assertion violation in L201");
THROWIFNOT(isBasePaired(hairpins[0][i] + hairpins[1][i]),"Assertion violation in L202");
      int start = hairpins[0][i]-1;
      int len = hairpins[1][i]+2;
THROWIFNOT(len > 0,"Assertion violation in L205");
THROWIFNOT(start >= 0,"Assertion violation in L206");
      result += EntropyTools::computeTurner04HairpinFreeEnergy(sequences->getConcatSequence().substr(start, len)); // provide sequence; include last base pair
    }
    // DEBUG_MSG("Finished computeLoopFreeEnergy");
    return result;
  }
  
  /* double computeLogMolecularStates() const { */
  /*   Vec<int> loops = findLoops(); */
  /*   double result = 0.0; */
  /*   for (size_t i = 0; i < loops.size(); ++i) { */
  /*     double dS = EntropyTools::loopEntropy(loops[i]); */
  /*     ASSERT(dS > 0.0); */
  /*     ASSERT(KB == 0.0019872041); //  Boltzmann constant in kcal/mol per Kelvin, as defined in EntropyTools.h  */
  /*     double log_mult = dS / KB; */
  /*     ASSERT(dS > 0.0); */
  /*     result -= log_mult; */
  /*     // Rcpp::Rcout << "Entropy reduction corresponding to loop " << loops[i] << " : " << dS << " TdS: " << residueModel->getTemperatureK()*dS << " log of reduction in states: " << log_mult << " KB: " << KB << "dS/KB: " << dS / KB << endl; */
  /*   } */
  /*   return result; */
  /* } */

  double computeLogMultiplicity() const {
    PRECOND(validate());
    // number of components:
    double logMolecularStates = 0.0; // loop entropy is now handled differently; computeLogMolecularStates(); // countFlexibleResidues() * log(residueModel->getStatesPerFlexibleResidue());
    double logTranslationStates = 0.0;
    double logTotalStates = logMolecularStates;
    // DEBUG_MSG("# Mark ResiduePairingh 234");
    if (sequences->size() > 1) { // multiple sequences
      //      Rcpp::Rcout << "trying to get connectivity of indirect matrix: " << strandIndirectConnectivity << endl;
      Vec<string> componentHashes = ConnectivityTools::connectivityToHashes(strandIndirectConnectivity); // TODO improve efficiency of implementation
      // Rcpp::Rcout << "# Component hashes: " << componentHashes;
      size_t componentCount = componentHashes.size();
      // DEBUG_MSG("# Mark ResiduePairingh 240");
      ERROR_IF(componentCount < 1, "Internal error: there should be at least one complex!");
      // DEBUG_MSG("# Mark ResiduePairingh 242");
      ERROR_IF(componentCount > sequences->size(), "Internal error: there should be less complexes than number of sequences!");
      // DEBUG_MSG("# Mark ResiduePairingh 244");
      // Rcpp::Rcout << "# Strand concentrations: " << sequences->getConcentrations() << endl;
      double logTranslationStates0 = sequences->size() * (- 3.0 * log(residueModel->getReactionRadius()) - log(sequences->getConcentrations()[0])); 
      // DEBUG_MSG("# Mark ResiduePairingh 246");
      logTranslationStates = componentCount * (- 3.0 * log(residueModel->getReactionRadius()) - log(sequences->getConcentrations()[0])); 
      // DEBUG_MSG("# Mark ResiduePairingh 248");
      logTotalStates += (logTranslationStates - logTranslationStates0);
      // DEBUG_MSG("# Mark ResiduePairingh 250");
      // Rcpp::Rcout << "Log of total states relative of unbound state: " << logTotalStates << endl;
    } else {
      // Rcpp::Rcout << "Log-stuff (single-sequence) " << logMolecularStates << endl;
    }
    // DEBUG_MSG("# Mark ResiduePairingh 255");
    return logTotalStates;
  }

  size_t countBasePairs2() const {
    size_t result = 0;
    for (size_t i = 0; i < size(); ++i) {
      if (((*this)[i] != NO_PAIRING_INDEX) && (static_cast<index_t>(i) < (*this)[i])) {
	++result;
      }
    }
    return result;
  }

  size_t countBasePairs() const {
    size_t result = 0;
    for (size_t i = 0; i < helices.size(); ++i) {
      result += helices[i].getLength();
    }
    return result;
  }

  string getClassName() const { return "ResiduePairing"; }

  const map<string, double>& getProperties() const { return properties; }

  const Vec<Stem>& getHelices() const { return helices; }

  const string& getHelicesHash() const { return helicesHash; }

  double getProperty(const string& name) const {
    PRECOND(name.size() > 0);
    map<string, double>::const_iterator it = properties.find(name);
    if (it != properties.end()) {
      return it->second;
    }
    Rcpp::Rcout << "# Query for unknown property " << name << endl;
    // write(Rcpp::Rcout);
    DERROR("Query for unknown property encountered.");
    return 0.0;
  }
  
  bool hasProperty(const string& propertyName) const {
    return (properties.find(propertyName) != properties.end());
  }
  
  ResidueModel * getResidueModel() const { return residueModel; }

  StrandContainer * getSequences() const { return sequences; }

  double getSimulationTime() const { return simulationTime; }

  const Vec<Vec<int> >& getStrandConnectivity() const { return strandConnectivity; }

  const string& getStrandConnectivityHash() const { return strandConnectivityHash; }

  const Vec<Vec<set<basepair_t> > >& getStrandConnectivitySets() const { return strandConnectivitySets; }

  const Vec<Vec<int> >& getStrandIndirectConnectivity() const { return strandIndirectConnectivity; }

  /** Returns true, if base pair has same connectivity. Does not consider indirect connectivity. */
  bool hasSameConnectivity(int start, int stop) {
    int s1 = sequences->getStrandId(start);
    int s2 = sequences->getStrandId(stop);
    return (s1==s2) || (strandConnectivity[s1][s2] > 0);
  }

  void init() {
    DEBUG_MSG("# Starting ResiduePairing.init ...");
    simulationTime = 0.0;
    stackingEnergy = 0.0;
    double energy = updateHelixEnergies();
    setProperty("negenergy", -energy);
    DEBUG_MSG("# ResiduePairingh 320");
    if (sequences->size() > 0) {
      DEBUG_MSG("# ResiduePairingh 322");
      strandConnectivity = Vec<Vec<int> >(sequences->size(), Vec<int>(sequences->size(), 0));
      strandConnectivitySets = Vec<Vec<set<basepair_t> > >(sequences->size(), Vec<set<basepair_t> >(sequences->size()));
      strandIndirectConnectivity = Vec<Vec<int> >(sequences->size(), Vec<int>(sequences->size(), 0));
      strandConnectivityHash = ConnectivityTools::connectivityToCombinedHash(strandConnectivity);
    }
    DEBUG_MSG("# ResiduePairingh 328");
    setProperty("negenergy", 0.0);
    DEBUG_MSG("# ResiduePairingh 330");
    setProperty("log_multiplicity", computeLogMultiplicity());
    DEBUG_MSG("# ResiduePairingh 332");
    setProperty("loop_free_energy", computeLoopFreeEnergy());
    DEBUG_MSG("# ResiduePairingh 334");
    setProperty("free_energy", computeAbsoluteFreeEnergy());
    DEBUG_MSG("# ResiduePairingh 336");
    POSTCOND(validate());
    DEBUG_MSG("# Finished ResiduePairing.init!");
  }

  bool isDefinedProperty(string name) const { return properties.find(name) != properties.end(); }

  void setProperty(const string& name, double value) {
    properties[name] = value;
  }

  void setSimulationTime(double time) const {
    simulationTime = time;
  }
  
  bool isBasePaired(int start, int stop) const {
    return (*this)[start] == stop;
  }

  bool isBasePaired(int start) const {
    return (*this)[start] != NO_PAIRING_INDEX;
  }

  bool isNewBasePairOK(index_t start, index_t stop) const {
    if (stop < start) {
      return isNewBasePairOK(stop,start);
    }
    if (isBasePaired(start) || isBasePaired(stop)) {
      return false;
    }
    if (start == stop) {
      return false;
    }
    int pkMode = residueModel->getPseudoknotMode();
    size_t s1 = sequences->getStrandId(start);
    size_t s2 = sequences->getStrandId(stop);
    if ((s1 == s2) && ((stop - start) <= LOOP_LEN_MIN)) {
      DEBUG_MSG("ResiduePairing.isNewBasePairOK: too short loop in hairpin");  
      return false; // too short loop in hairpin
    }
    Stem stem(start, stop, 1);
    if (pkMode != ResidueModel::ALL_PSEUDOKNOTS) {
      // check all inter-strand helices if an undesired pseudoknot is formed:
      size_t pkCount = 0; // count crossed base pairs
      for (size_t i = 0; i < helices.size(); ++i) {
	size_t s1h = sequences->getStrandId(helices[i].getStart());
	size_t s2h = sequences->getStrandId(helices[i].getStop());
	if (pkMode == ResidueModel::NO_PSEUDO_KNOTS) {
	  if ((s1h == s1) && (s2h == s2)) { // only check if same strand connectivity
	    if (stem.isCrossing(helices[i])) {
	      return false;
	    }
	  }
	} else if (pkMode == ResidueModel::INTERSTRAND_PSEUDOKNOTS) {
	  if ((s1h == s1) && (s2h == s2) && (s1 == s2)) { // only check if same strand connectivity and on same strand
	    index_t starth = helices[i].getStart();
	    index_t stoph = helices[i].getStop();
THROWIFNOT(stoph >  starth,"Assertion violation in L402");
	    if ((stop < starth) || (start > stoph) || ((start > starth) && (stop < stoph) ) || ((starth > start) && (stoph < stop) )  ) {
	      // is ok
	    } else {
              DEBUG_MSG("ResiduePairing.isNewBasePairOK: undesired pseudoknot found.");
	      return false; // must be pseudoknot
	    }
	  }
	} else if (pkMode == ResidueModel::H_TYPE_PSEUDOKNOTS) { // check for all pseudoknots
	  index_t starth = helices[i].getStart();
	  index_t stoph = helices[i].getStop();
	  if ((s1h == s1) && (s2h == s2)) { // only check if same strand connectivity
	    if ((stop < starth) || (start > stoph) || ((start > starth) && (stop < stoph) ) || ((starth > start) && (stoph < stop) )  ) {
	      // is ok
	    } else {
	      ++pkCount; // found a crossing stem
	      if (pkCount > 1) {
                 DEBUG_MSG("ResiduePairing.isNewBasePairOK: too complex pseudoknot found.");
		return false; // more than one crossing stem for this stem found
	      }
	    }
	  }
	} else { // check for all pseudoknots
	  index_t starth = helices[i].getStart();
	  index_t stoph = helices[i].getStop();
	  if ((stop < starth) || (start > stoph) || ((start > starth) && (stop < stoph) ) || ((starth > start) && (stoph < stop) )  ) {
	    // is ok
	  } else {
            DEBUG_MSG("ResiduePairing.isNewBasePairOK: pseudoknot found.");
	    return false; // must be pseudoknot
	  }
	}
      }
    }
    return true;
  }

  bool isSameStrand(int start, int stop) const {
    const Vec<int>& strandIds = sequences->getStrandIds();
    return strandIds[start] == strandIds[stop];
  }

  int findHelix(int start, int stop) const {
    if ((start < 1) || ((stop + 1) >= static_cast<int>(size()))) {
      return -1;
    }
    int invar = Stem::computeInvariant(start, stop, true);
    for (size_t i = 0; i < helices.size(); ++i) {
      if ((helices[i].getInvariant(true) == invar) && (helices[i].uses(start,stop, true))) {
	return i;
      }
    }
    return -1;
  }

  int findUpstreamHelix(int start, int stop) const {
    if ((start < 1) || ((stop + 1) >= static_cast<int>(size()))) {
      return -1;
    }
    const Vec<int>& strandIds = sequences->getStrandIds();
    if ((strandIds[start-1] != strandIds[start])
	|| (strandIds[stop+1] != strandIds[stop]) ) {
      return  - 1;
    }
    int invar = Stem::computeInvariant(start, stop, true);
    for (size_t i = 0; i < helices.size(); ++i) {
      if ((helices[i].getInvariant(true) == invar) && (helices[i].uses(start-1,stop+1, true))) {
	return i;
      }
    }
    return -1;
  }

  int findDownstreamHelix(int start, int stop) const {
    // FIXIT verify
    const Vec<int>& strandIds = sequences->getStrandIds();
    if ((strandIds[start+1] != strandIds[start])
	|| (strandIds[stop-1] != strandIds[stop]) ) {
      return  -1;
    }
    int invar = Stem::computeInvariant(start, stop, true);
    for (size_t i = 0; i < helices.size(); ++i) {
      if ((helices[i].getInvariant(true) == invar) && (helices[i].uses(start+1,stop-1,true))) {
	return i;
      }
    }
    return -1;
  }

  bool isPlaceable(const Stem& stem) const {
    for (int i = 0; i < stem.getLength(); ++i) {
      if (isBasePaired(stem.getStart() + i) || isBasePaired(stem.getStop() - i)) {
	return false;
      }
    }
    return true;
  }

  bool setStem(const Stem& stem) {
    /* if (!isPlaceable(stem)) { */
    /*   return false; */
    /* } */
    size_t count = 0;
    for (int i = 0; i < stem.getLength(); ++i) {
THROWIFNOT(stem.getStart() + i < stem.getStop() - i,"Assertion violation in L506");
      if ((!isBasePaired(stem.getStart() + i)) && (!isBasePaired(stem.getStop() - i) ) ) {
	if (!setBasePair(stem.getStart() + i, stem.getStop() - i)) {
	  return false;
	}
	++count;
      }
    }
    return (count > 0);
  }

  bool setStem(const Stem& stem, size_t minStemLen) {
    /* if (!isPlaceable(stem)) { */
    /*   return false; */
    /* } */
    size_t count = 0;
    Vec<size_t> seqLens(stem.getLength(), 0);
    size_t seqLen = 0;
    int seqStart = -1;
    for (int i = 0; i < stem.getLength(); ++i) {
THROWIFNOT(stem.getStart() + i < stem.getStop() - i,"Assertion violation in L526");
      if ((!isBasePaired(stem.getStart() + i))
	  &&(!isBasePaired(stem.getStop() - i) ) ) {
	++seqLen;
	if (seqStart < 0) {
	  seqStart = i;
	}
	if (i+1 == stem.getLength()) { // special case: last position
	  for (int j = seqStart; j <= i; ++j) {
	    seqLens[j] = seqLen;
	  }
	}
      } else { // there is base pairing in the way
	if (seqStart >= 0) { // there is an active segment
	  for (int j = seqStart; j < i; ++j) {
	    seqLens[j] = seqLen;
	  }
	}
	seqLens[i] = 0; // nothing placeable here
	seqStart = -1;
	seqLen = 0;
      }
    }
    for (int i = 0; i < stem.getLength(); ++i) {
THROWIFNOT(stem.getStart() + i < stem.getStop() - i,"Assertion violation in L550");
      if ((seqLens[i] >= minStemLen) && (!isBasePaired(stem.getStart() + i))
	  &&(!isBasePaired(stem.getStop() - i) ) ) {
	if (!setBasePair(stem.getStart() + i, stem.getStop() - i)) {
	  return false;
	}
	++count;
      }
    }    
    return (count > 0);
  }
  
  bool isWatsonCrick(int i , int j, bool guAllowed) const {
    return sequences->isWatsonCrick(i,j,guAllowed);
  }

  bool isWatsonCrick(const Stem& stem, bool guAllowed) const {
    for (int i = 0; i < stem.getLength(); ++i) {
THROWIFNOT(stem.getStart() + i < stem.getStop() - i,"Assertion violation in L568");
      if (!isWatsonCrick(stem.getStart() + i, stem.getStop() - i, guAllowed)) {
	return false;
      }
    }    
    return true;
  }

  /** Sets a specified base pair. Also updates min and max distances and strand connectivity matrix */
  void setBasePairsFromMatrix(const Vec<Vec<double> >& mtx, double cutoff) {
    for (size_t i = 0; i< mtx.size(); ++i) {
      size_t id = maxElement(mtx[i]);
      if (mtx[i][id] >= cutoff) {
	if (i < id) {
	  setBasePair(i, id);
	}
      }
    }
  }

  /** Sets a specified base pair. Also updates min and max distances and strand connectivity matrix */
  bool setBasePair(int start, int stop) {
    // Rcpp::Rcout << "# starting setbase pair" << (start+1) << " " << (stop+1) << " for current structure " << helices << endl;
    if (start > stop) {
      return setBasePair(stop, start);
    }
    if (isBasePaired(start, stop)) {
      return false;
    }
    PRECOND(start < stop);  
    PRECOND(start >= 0);
    PRECOND(stop >= 0);
    
THROWIFNOT(validate(),"Assertion violation in L601");
    
    // ASSERT(validate());
THROWIFNOT((start >= 0) && (start < static_cast<int>(size())),"Assertion violation in L604");
THROWIFNOT((stop >= 0) && (stop < static_cast<int>(size())),"Assertion violation in L605");
THROWIFNOT(!isBasePaired(start, stop),"Assertion violation in L606");
    if (sequences != NULL) {
      const string& seq = sequences->getConcatSequence();
      if (!isComplementary(seq[start], seq[stop])) {
	Rcpp::Rcout << "Positions " << start+1 << " " << stop+1 << " are not complementary: " << seq[start] << ":" << seq[stop] << endl;
      }
THROWIFNOT(isComplementary(seq[start], seq[stop]),"Assertion violation in L612");
    }
    // int n = static_cast<int>(maxDists.size());
THROWIFNOT(start < stop,"Assertion violation in L615");
    // Rcpp::Rcout << "# Validation before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L617");
    // Rcpp::Rcout << "# Finished alidation before adjustment of distancesin setBasePairRaw." << endl;
    
    bool majorConnectivityChange = false;
    const Vec<int>& strandIds = sequences->getStrandIds();
    int s1 = strandIds[start];
    int s2 = strandIds[stop];
    // Rcpp::Rcout << "mark 1 " << endl;
    strandConnectivitySets[s1][s2].insert(basepair_t(start,stop));
    strandConnectivitySets[s2][s1].insert(basepair_t(start,stop));
    if ((s1 != s2) && (strandConnectivity[s1][s2] != DIRECT_CONN)) {
THROWIFNOT(strandConnectivity[s1][s2] == NO_CONN,"Assertion violation in L628");
      strandConnectivity[s1][s2] = DIRECT_CONN; // take into account new base pair
      strandConnectivity[s2][s1] = DIRECT_CONN;
      if (strandIndirectConnectivity[s1][s2] == NO_CONN) {
	majorConnectivityChange = true;
	strandConnectivityHash = ConnectivityTools::connectivityToCombinedHash(strandConnectivity);
#ifndef NDEBUG
	// Rcpp::Rcout << "# New strand interaction due to base pair " << (start+1) << " " << (stop+1) << endl;
	for (size_t i = 0; i < helices.size(); ++i) {
THROWIFNOT(!((strandIds[helices[i].getStart()] == s1) && (strandIds[helices[i].getStop()] == s2)),"Assertion violation in L637");
	}
#endif
      }
      // Rcpp::Rcout << "mark 2 " << endl;      
      strandIndirectConnectivity = ConnectivityTools::connectivityToIndirectConnectivity(strandConnectivity);
THROWIFNOT(strandConnectivity[s1][s2] == DIRECT_CONN,"Assertion violation in L643");
THROWIFNOT(strandConnectivity[s2][s1] == DIRECT_CONN,"Assertion violation in L644");
THROWIFNOT(strandIndirectConnectivity[s1][s2] == DIRECT_CONN,"Assertion violation in L645");
THROWIFNOT(strandIndirectConnectivity[s2][s1] == DIRECT_CONN,"Assertion violation in L646");
    }
    // Rcpp::Rcout << "mark 3 " << endl;
    int upHelixId = findUpstreamHelix(start,stop);
    int downHelixId = findDownstreamHelix(start,stop);
    
    if ((upHelixId >= 0) && (downHelixId >= 0)) {
      // Rcpp::Rcout << "mark 4 " << endl;
      // DERROR("helix fusing not yet implemented");
      Stem& helix = helices[upHelixId];
      Stem& helix2 = helices[downHelixId];
      // Rcpp::Rcout << "# case 1: helix fusing case " << (start+1) << " " << (stop+1) << ":" <<  helix << ":" << helix2 <<endl;
      if (upHelixId == downHelixId) {
	Rcpp::Rcout << "# Strange: upstream and downstram helices are identical!" << endl;
	Rcpp::Rcout << "# " << (start+1) << " " << (stop+1) << " : " << (upHelixId+1) << " : " << helix << endl;
//	write(Rcpp::Rcout);
	Rcpp::Rcout << "# Validation: " << validate() << endl;
	Rcpp::Rcout << "Helix 1 positions: " << endl;
	Rcpp::Rcout << "Position: " << (start+1) << " " << (stop+1) << endl;
	Rcpp::Rcout << "Upstream Position: " << (start+1-1) << " " << (stop+1+1) << endl;
	Rcpp::Rcout << "Downstream Position: " << (start+1+1) << " " << (stop+1-1) << endl;
	for (index_t i = 0; i < helix.getLength(); ++i) {
	  Rcpp::Rcout << helix.getStart() + 1 + i << ":" << helix.getStop() + 1 - i << endl;
THROWIFNOT(isBasePaired(helix.getStart() + i, helix.getStop()-i),"Assertion violation in L669");
	}
	Rcpp::Rcout << "Helix 2 positions: " << endl;
	for (index_t i = 0; i < helix2.getLength(); ++i) {
	  Rcpp::Rcout << helix2.getStart() + 1 + i << ":" << helix2.getStop() + 1 - i << endl;
THROWIFNOT(isBasePaired(helix2.getStart() + i, helix2.getStop()-i),"Assertion violation in L674");
	}
      }
      // Rcpp::Rcout << "mark 5 " << endl;
THROWIFNOT(helix.uses(start-1,stop+1, true),"Assertion violation in L678");
THROWIFNOT(helix2.uses(start+1,stop-1, true),"Assertion violation in L679");
THROWIFNOT(upHelixId != downHelixId,"Assertion violation in L680");
      helix.setLength(helix.getLength() + helix2.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      // size_t s1 = helices.size();
      updateHelixEnergy(upHelixId); // doe this before erase command in order to avoid index shift
      helices.erase(helices.begin() + downHelixId);
      updateHelicesHash();
      // size_t s2 = helices.size();
      // ASSERT(s2 + 1 == s1);
    } else if (upHelixId >= 0) {
      // Rcpp::Rcout << "# case 2: upstream helix case " << endl;
      Stem& helix = helices[upHelixId];
      helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(upHelixId);
THROWIFNOT(helices[upHelixId].uses(start, stop,true),"Assertion violation in L693");
    } else if (downHelixId >= 0) {
      // Rcpp::Rcout << "# case 3: downstream helix case " << endl;
      Stem& helix = helices[downHelixId];
      helix.setStart(helix.getStart()-1);
      helix.setStop(helix.getStop()+1);
      helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(downHelixId);
THROWIFNOT(helices[downHelixId].uses(start, stop,true),"Assertion violation in L701");
    } else  { // start new helix
      // Rcpp::Rcout << "# case 4: new helix case" << endl;
      // Rcpp::Rcout << "creating new stem " << (start+1) << " " << (stop + 1) << endl;
      // Rcpp::Rcout << "mark 6 " << endl;
      Stem newStem(start, stop, 1);
THROWIFNOT(residueModel != NULL,"Assertion violation in L707");
      if (residueModel->getEntropyMode() == ResidueModel::SIMPLE_ENTROPY) {
	// estimate entropic penalty: find loop
	int dBest = static_cast<int>(size());
	if (isSameStrand(start, stop)) {
	  dBest = stop - start;
	}
	for (size_t i = 0; i < helices.size(); ++i) {
	  int d = basePairDist(start,stop, helices[i]);
	  if (d == 0) {
	    Rcpp::Rcout << "Strange: " << (start+1) << " " << (stop + 1) << " : " << helices[i] << endl;
	  }
THROWIFNOT(d != 0,"Assertion violation in L719");
	  // Rcpp::Rcout << "Dist of " << (start+1) << " to " << (stop+1) << " to helix " << (i+1) << " is " << d << " : " << helices[i] << endl;
	  if ((d > 0) && ( d < dBest )) {
	     dBest = d; 
	   } 
	}
THROWIFNOT(dBest > 0,"Assertion violation in L725");
	// TODO : case of formation of first base pair between two strands
	double entropyPenalty = EntropyTools::loopEntropy(dBest-1); // dBest is shorted distance, dBest-1 is effective loop size
THROWIFNOT(entropyPenalty >= 0.0,"Assertion violation in L728");
	// Rcpp::Rcout << "creating new stem " << newStem << " " << dBest << " with entropy penalty " << entropyPenalty << endl;
	// Rcpp::Rcout << " temp " << residueModel->getTemperatureK() << " " << entropyPenalty * residueModel->getTemperatureK() << endl;
	newStem.setFormationEnergy(entropyPenalty * residueModel->getTemperatureK());
      } else if (majorConnectivityChange && (residueModel->getEntropyMode() == ResidueModel::STRAND_ENTROPY)) {
	newStem.setFormationEnergy(residueModel->getStrandEntropyPenalty());
      }
      helices.push_back(newStem);
      // Rcpp::Rcout << "updating helices " << endl;
      updateHelixEnergy(helices.size()-1);
      updateHelicesHash();
      // Rcpp::Rcout << "Finished creating new stem " << (start+1) << " " << (stop + 1) << " " << newStem.getFormationEnergy() << endl;
    }
    // Rcpp::Rcout << "mark 8 " << endl;    
  /* if ((start > 0) && ((stop+1) < n) && (helixIds[start-1][stop+1] >= 0) && (strandIds[start-1] == strandIds[start]) && (strandIds[stop+1] == strandIds[stop])) { // extend existing upstream helix */
  /*   // Rcpp::Rcout << "# case 1..." << endl; */
  /*   helixIds[start][stop] = helixIds[start-1][stop+1]; */
  /*   helixIds[stop][start] = helixIds[start][stop]; */
  /*   ASSERT(start < static_cast<index_t>(helixIds.size())); */
  /*   ASSERT(stop < static_cast<index_t>(helixIds[start].size())); */
  /*   ASSERT(helixIds[start][stop] >= 0); */
  /*   ASSERT(helixIds[start][stop] < static_cast<index_t>(helices.size())); */
  /*   Stem& helix = helices[helixIds[start][stop]]; */
  /*   helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*   updateHelixEnergy(helixIds[start][stop]); */
  /* } */
  /* if ((stop > 0) && ((start+1) < n) && (helixIds[start+1][stop-1] >= 0) && (strandIds[start+1] == strandIds[start]) && (strandIds[stop-1] == strandIds[stop])) { // case 2: extend to downstream helix? */
  /*   if (helixIds[start][stop] >= 0) { // case 2a: Fusing of helices ! */
  /*     int helixId = helixIds[start][stop]; */
  /*     Stem& helix = helices[helixIds[start][stop]]; */
  /*     int helixId2 = helixIds[start+1][stop-1]; */
  /*     ASSERT(helixId2 < static_cast<int>(helices.size())); */
  /*     Stem& helix2 = helices[helixId2]; */
  /*     for (int i = 0; i < helix2.getLength(); ++i) { */
  /* 	helixIds[helix2.getStart() + i][helix2.getStop()-i] = helixId; */
  /*     } */

  /*     helix.setLength(helix.getLength() + helix2.getLength()); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*     updateHelixEnergy(helixId); */
  /*     helix2.setLength(0); */
  /*     helix2.setEnergy(0.0); */
  /*     ASSERT(helix.getStart() < helix.getStop()); */

  /*   } else { // case 2b: extend helix start start + 1, stop - 1 */
  /*     helixIds[start][stop] = helixIds[start+1][stop-1]; */
  /*     helixIds[stop][start] = helixIds[start][stop]; */
  /*     ASSERT(helixIds[start][stop] >= 0); */
  /*     ASSERT(helixIds[start][stop] < static_cast<index_t>(helices.size())); */
  /*     Stem& helix = helices[helixIds[start][stop]]; */
  /*     helix.setStart(helix.getStart()-1); */
  /*     helix.setStop(helix.getStop()+1); */
  /*     helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*     updateHelixEnergy(helixIds[stop][start]); */
  /*   } */
  /* } */
  
    (*this)[start] = stop;
    (*this)[stop] = start;

    if (residueModel->getEntropyMode() == ResidueModel::PARTITION_FUNCTION_ENTROPY) {
      setProperty("log_multiplicity", computeLogMultiplicity());
      setProperty("loop_free_energy", computeLoopFreeEnergy());
      setProperty("free_energy", computeAbsoluteFreeEnergy());
    } else {
      DERROR("Unsupported entropy mode.");
    }

    /* strandConnectivity[strandId1][strandId2].insert(hash); */
    /* strandConnectivity[strandId2][strandId1].insert(hash); */
    // energy += computeBasePairEnergyContribution(start, stop);
    
    // Rcpp::Rcout << "Final validation before setBasePairRaw is finished..." << endl;
THROWIFNOT(validate(),"Assertion violation in L800");
    // now check if it created a single-base bulge (account for stacking energy):
THROWIFNOT(isBasePaired(start, stop),"Assertion violation in L802");
    stackingEnergy = computeStackingEnergy();

    return true;
  }

  double computeStackingEnergy() const {
    double result = 0.0;
    if (helices.size() < 2) {
      return 0.0;
    }
    for (size_t i = 0; i < helices.size(); ++i) {
      result += computeStackingEnergy(i);
    }
    return result;
  }

  // stacking energy between helices i and j
  double computeStackingEnergy(size_t hid) const;

  Vec<basepair_t> toBasePairVec() {
    Vec<basepair_t> result;
    for(size_t i = 0; i < size(); ++i) {
      if (((*this)[i] >= 0) && (static_cast<index_t>(i) < (*this)[i])) {
       result.push_back(basepair_t(i,(*this)[i]));
     }
    }
    return result;
  }
  

  /** Sets a specified base pair. Also updates min and max distances and strand connectivity matrix */
  bool unsetBasePair(int start, int stop) {
    // Rcpp::Rcout << "# starting setbase pair" << endl;
    if (start > stop) {
      return unsetBasePair(stop, start);
    }
    if (!isBasePaired(start, stop)) {
      return false;
    }
    PRECOND(start < stop);  
    PRECOND(start >= 0);
    PRECOND(stop >= 0);
    
THROWIFNOT(validate(),"Assertion violation in L846");
    
    // ASSERT(validate());
THROWIFNOT((start >= 0) && (start < static_cast<int>(size())),"Assertion violation in L849");
THROWIFNOT((stop >= 0) && (stop < static_cast<int>(size())),"Assertion violation in L850");
THROWIFNOT(!isBasePaired(start, stop),"Assertion violation in L851");
    
    // int n = static_cast<int>(maxDists.size());
THROWIFNOT(start < stop,"Assertion violation in L854");
    // Rcpp::Rcout << "# Validation before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L856");
    // Rcpp::Rcout << "# Finished alidation before adjustment of distancesin setBasePairRaw." << endl;

    int helixId = findHelix(start,stop);

    if (helixId >= 0) {
      Stem& helix = helices[helixId];
THROWIFNOT(helix.uses(start-1,stop+1, true),"Assertion violation in L863");
      if (helix.getLength() == 1) {
	helices.erase(helices.begin() + helixId);
      } else if ((start == helix.getStart() + helix.getLength() - 1) && (stop == helix.getStop() - helix.getLength() + 1)) {
	helix.setLength(helix.getLength() - 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      } else if ((start == helix.getStart() ) && (stop == helix.getStop() ) ) {
	helix.setStart(helix.getStart() + 1);
	helix.setStop(helix.getStop() - 1);
      }
      updateHelixEnergies();
    } else {
      return false; // do not remove base pairs in middle of helices
    }
    
    bool majorConnectivityChange = false;
    const Vec<int>& strandIds = sequences->getStrandIds();
    int s1 = strandIds[start];
    int s2 = strandIds[stop];
    basepair_t bp(start,stop);
    if (strandConnectivitySets[s1][s2].find(bp) != strandConnectivitySets[s1][s2].end()) {
      strandConnectivitySets[s1][s2].erase(bp);
      if (strandConnectivitySets[s1][s2].size() == 0) {
	majorConnectivityChange = true;
      }
    }
    
    if (majorConnectivityChange) {
THROWIFNOT(strandConnectivity[s1][s2] != NO_CONN,"Assertion violation in L890");
      strandConnectivity[s1][s2] = NO_CONN; // take into account new base pair
      strandConnectivity[s2][s1] = NO_CONN;
      strandIndirectConnectivity = ConnectivityTools::connectivityToIndirectConnectivity(strandConnectivity);
THROWIFNOT(strandConnectivity[s1][s2] == DIRECT_CONN,"Assertion violation in L894");
THROWIFNOT(strandConnectivity[s2][s1] == DIRECT_CONN,"Assertion violation in L895");
THROWIFNOT(strandIndirectConnectivity[s1][s2] == DIRECT_CONN,"Assertion violation in L896");
THROWIFNOT(strandIndirectConnectivity[s2][s1] == DIRECT_CONN,"Assertion violation in L897");
    }
    
  /* if ((start > 0) && ((stop+1) < n) && (helixIds[start-1][stop+1] >= 0) && (strandIds[start-1] == strandIds[start]) && (strandIds[stop+1] == strandIds[stop])) { // extend existing upstream helix */
  /*   // Rcpp::Rcout << "# case 1..." << endl; */
  /*   helixIds[start][stop] = helixIds[start-1][stop+1]; */
  /*   helixIds[stop][start] = helixIds[start][stop]; */
  /*   ASSERT(start < static_cast<index_t>(helixIds.size())); */
  /*   ASSERT(stop < static_cast<index_t>(helixIds[start].size())); */
  /*   ASSERT(helixIds[start][stop] >= 0); */
  /*   ASSERT(helixIds[start][stop] < static_cast<index_t>(helices.size())); */
  /*   Stem& helix = helices[helixIds[start][stop]]; */
  /*   helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*   updateHelixEnergy(helixIds[start][stop]); */
  /* } */
  /* if ((stop > 0) && ((start+1) < n) && (helixIds[start+1][stop-1] >= 0) && (strandIds[start+1] == strandIds[start]) && (strandIds[stop-1] == strandIds[stop])) { // case 2: extend to downstream helix? */
  /*   if (helixIds[start][stop] >= 0) { // case 2a: Fusing of helices ! */
  /*     int helixId = helixIds[start][stop]; */
  /*     Stem& helix = helices[helixIds[start][stop]]; */
  /*     int helixId2 = helixIds[start+1][stop-1]; */
  /*     ASSERT(helixId2 < static_cast<int>(helices.size())); */
  /*     Stem& helix2 = helices[helixId2]; */
  /*     for (int i = 0; i < helix2.getLength(); ++i) { */
  /* 	helixIds[helix2.getStart() + i][helix2.getStop()-i] = helixId; */
  /*     } */

  /*     helix.setLength(helix.getLength() + helix2.getLength()); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*     updateHelixEnergy(helixId); */
  /*     helix2.setLength(0); */
  /*     helix2.setEnergy(0.0); */
  /*     ASSERT(helix.getStart() < helix.getStop()); */

  /*   } else { // case 2b: extend helix start start + 1, stop - 1 */
  /*     helixIds[start][stop] = helixIds[start+1][stop-1]; */
  /*     helixIds[stop][start] = helixIds[start][stop]; */
  /*     ASSERT(helixIds[start][stop] >= 0); */
  /*     ASSERT(helixIds[start][stop] < static_cast<index_t>(helices.size())); */
  /*     Stem& helix = helices[helixIds[start][stop]]; */
  /*     helix.setStart(helix.getStart()-1); */
  /*     helix.setStop(helix.getStop()+1); */
  /*     helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted       */
  /*     updateHelixEnergy(helixIds[stop][start]); */
  /*   } */
  /* } */
  
    (*this)[start] = stop;
    (*this)[stop] = start;

    /* strandConnectivity[strandId1][strandId2].insert(hash); */
    /* strandConnectivity[strandId2][strandId1].insert(hash); */
    // energy += computeBasePairEnergyContribution(start, stop);
    
    // Rcpp::Rcout << "Final validation before setBasePairRaw is finished..." << endl;
THROWIFNOT(validate(),"Assertion violation in L950");
    return true;
  }

  virtual double updateHelixEnergy(int helixId) {
    PRECOND(helixId < static_cast<index_t>(helices.size()));
    PRECOND(residueModel != NULL);
    // Rcpp::Rcout << "Starting update helix energy" << endl;
    // write(Rcpp::Rcout);
    double result = 0.0; // HELIX_PENALTY; // 0.0;
    Stem& helix = helices[helixId];
    index_t start = helix.getStart();
    index_t stop = helix.getStop();
    double energyOrig = helix.getEnergy();
    result = residueModel->calcStemStackingEnergy(sequences->getConcatSequence(), start, stop, helix.getLength())
      + helix.getFormationEnergy(); // stacking term plus entropic penalty
    // ASSERT(result >= 0.0);
    /* for (index_t i = 1; i < helix.getLength(); ++i) { */
    /*   char a = getResidue(start+i-1); */
    /*   char b = getResidue(stop-i+1); */
    /*   char x = getResidue(start+i); */
    /*   char y = getResidue(stop-i); */
    /*   result += residueModel->getStackingEnergy(a,b,x,y); */
    /* } */
    helix.setEnergy(result);
    double energyDelta = (result - energyOrig); // update energy accordingly
THROWIFNOT(hasProperty("negenergy"),"Assertion violation in L976");
    double energy = -getProperty("negenergy");
    // ASSERT(energy <= 0.0);
    // Rcpp::Rcout << "Total old energy, old helix and new helix energy: " << energy << " " << energyOrig << " " << result << " " << energyDelta << " helix " << helix << endl;
    energy += energyDelta;

    // ASSERT(energy <= 0.0);
    setProperty("negenergy", -energy);
    /* size_t strandId1 = strandIds[start]; */
    /* size_t strandId2 = strandIds[stop]; */
    /* ASSERT(strandId1 < strandInteractionEnergies.size()); */
    /* ASSERT(strandId2 < strandInteractionEnergies.size()); */
    /* ASSERT(strandId2 < strandInteractionEnergies[strandId1].size()); */
    /* ASSERT(strandId1 < strandInteractionEnergies[strandId2].size()); */
    /* strandInteractionEnergies[strandId1][strandId2] += energyDelta; */
    /* strandInteractionEnergies[strandId2][strandId1] = strandInteractionEnergies[strandId1][strandId2]; */
    return result;
  }

  virtual double updateHelixEnergies() {  
    double result = 0.0;
    for (size_t i = 0; i < helices.size(); ++i) {
      result += updateHelixEnergy(i);
    }
    return result;
  }

  /** Returns how many base pairs a position can be extended to. Returns negative of that value
   * if one bp was found that is already set.
   */
  index_t basePairExtendability(index_t start, index_t stop, bool guAllowed) const {
    index_t a1 = sideExtendability(start, stop, guAllowed, true);
    index_t a2 = sideExtendability(start, stop, guAllowed, false);
    bool ok = true;
    if ((a1 < 0) || (a2 < 0) ) {
      ok = false;
    }
    index_t result=  abs(a1) + abs(a2) + 1;
    if (!ok) {
      result *= -1;
    }
    // Rcpp::Rcout << "Side extendability of position " << (start+1) << " " << (stop+1) << " : " <<result << " : " << a1 << " + " << a2 << endl;
    return result;
  }

  index_t sideExtendability(index_t start, index_t stop, bool guAllowed, bool upstream) const {
    PRECOND(start != stop);
    if (start > stop) {
      return sideExtendability(stop, start, guAllowed, upstream);
    }
    // Rcpp::Rcout << "# Called sideExtendability " << (start+1) << " " << (stop+1) << " " << guAllowed << " " << upstream << endl;
    const Vec<int>& strandIds = sequences->getStrandIds();
    index_t s1 = strandIds[start];
    index_t s2 = strandIds[stop];
    index_t result = 0;  
    for (index_t i = 1; i < static_cast<index_t>(size()); ++i) {
      // Rcpp::Rcout << "i: " << i << endl;
      index_t i2 = start-i;
      index_t j2 = stop+i;
      if (!upstream) {
	i2 = start+i;

	j2 = stop-i;
      }
      if (j2 <= i2) {
	break;
      }
      if ((i2 < 0) || (i2 >= static_cast<int>(size())) || (j2 < 0) || (j2 >= static_cast<int>(size()))) {
	break;
      }
      index_t s1b = strandIds[i2];
      index_t s2b = strandIds[j2];
      if ((s1 != s1b) || (s2 != s2b)) { // strands changed
	break;
      }
      if (isSameStrand(start,stop)) { // loop too short
	index_t d = j2-i2;
	if (d <= LOOP_LEN_MIN) {
	  break;
	}
      }
      if (isBasePaired(i2,j2)) {
	break;
      }
      // Rcpp::Rcout << "testing " << i2 <<" " << j2 <<endl;
      if (!sequences->isWatsonCrick(i2,j2,guAllowed)) {
	return -result;
      }
      ++result;
    }
    return result;
  }


  virtual double computeHelixEnergySum() const  {
    double result = 0.0;
    for (size_t i = 0; i < helices.size(); ++i) {
      // Rcpp::Rcout << "Helix " << (i+1) << " : " << helices[i].getEnergy() << " kcal/mol : " << helices[i] << endl;
      result += helices[i].getEnergy();
    }
    return result;
  }

  bool validateHelices() const {
    for (size_t j = 0; j < helices.size(); ++j) {
      const Stem& helix2 = helices[j];
      for (index_t i = 0; i < helix2.getLength(); ++i) {
	if(!isBasePaired(helix2.getStart() + i, helix2.getStop()-i)) {
	  return false;
	}
      }
    }
    return true;
  }

  bool validatePairing() const {
    for (size_t i = 0; i < size(); ++i) {
      index_t j = (*this)[i];
      if (j != NO_PAIRING_INDEX) {
	if ((j < 0) || (j >= static_cast<index_t>(size()))) {
	  return false;
	}
	index_t k = (*this)[j];
	if (k != static_cast<index_t>(i)) {
	  return false; // has to point back
	}
      }
    }
    return true;
  }

  void writeCt(ostream& os) const {
    DEBUG_MSG("Starting ResiduePairing::writeCt");
    ResiduePairing::size_t n = size();
    string delim = " ";
    size_t seqCount = 0;
    size_t seqRes = 0;
    os << n << delim << sequences->size();
    size_t pc = 1; // 1-based starts
    for (size_t i = 0; i < sequences->size(); ++i) {
      os << delim << pc;
      pc += (*sequences)[i].size();
    }
    map<string, double> properties = getProperties();
    for (map<string,double>::const_iterator it = properties.begin(); it != properties.end(); it++) {
      os << delim << it->first << ":" << it->second;
    }
    os << delim << "connectivity" << ":" << getStrandConnectivityHash() << endl;
    for (size_t i = 0; i < n; ++i) {
      os << (i+1) << delim << (*sequences)[seqCount][seqRes] << delim << i << delim << (i+2) << delim << (*this)[i]+1 << delim << (i+1) << endl;
      ++seqRes;
      if (seqRes >= (*sequences)[seqCount].size()) {
	seqRes = 0;
	++seqCount;
      }
    }
    DEBUG_MSG("Finished ResiduePairingTools::writeCt");
  }

  bool validateSequences() const {
    if (sequences == NULL) {
THROWIFNOT(false,"Assertion violation in L1110");
      return false;
    }
    if (sequences->getTotalLength() != size()) {
THROWIFNOT(false,"Assertion violation in L1114");
      return false;
    }
    return sequences->validate();
  }

 bool validate() const {
   // DEBUG_MSG("Starting ResiduePairing.validate...");
THROWIFNOT(size() > 0,"Assertion violation in L1122");
THROWIFNOT(hasProperty("negenergy"),"Assertion violation in L1123");
THROWIFNOT(countBasePairs() == countBasePairs2(),"Assertion violation in L1124");
THROWIFNOT(validateHelices(),"Assertion violation in L1125");
if (!validatePairing()) {
  Rcpp::Rcout << "# Error: pairing did not validate!!!" << endl;
  writeCt(Rcpp::Rcout);
}
THROWIFNOT(validatePairing(),"Assertion violation in L1126");
THROWIFNOT(validateSequences(),"Assertion violation in L1127");
THROWIFNOT(strandConnectivity.size() == sequences->size(),"Assertion violation in L1128");
THROWIFNOT(strandConnectivity.size() == strandIndirectConnectivity.size(),"Assertion violation in L1129");
THROWIFNOT((countBasePairs() == 0) || (strandConnectivityHash.size() > 0),"Assertion violation in L1130");
   bool result = (size() > 0) && hasProperty("negenergy") && (countBasePairs() == countBasePairs2()) && validateHelices() && validatePairing() && validateSequences() && (strandConnectivity.size() == sequences->size()) && (strandConnectivity.size() == strandIndirectConnectivity.size());
   // DEBUG_MSG("Finished ResiduePairing.validate!");
   return result;
 }

 // void write(ostream& os) const {
 //   operator<<(os,static_cast<Vec<int> >(*this)) << endl;
 //   os << "# Helices: " << helices << endl;
 //   if (sequences != NULL) {
 //     os << "# Defined sequence information: " << endl;
 //     sequences->write(os);
 //   }
 //   os << "# " << properties.size() << " defined properties:" << endl;
 //   for (map<string,double>::const_iterator it = properties.begin(); it != properties.end(); it++) {
 //     os << it->first << "\t" << it->second << endl;
 //   }
 // }

 void writeHelices(ostream& os) const {
   os << "helices " << helices.size() << " ";
   for (size_t i = 0; i < helices.size(); ++i) {
     os << " " << helices[i];
   }
 }

 /** Returns vector of internal loops and bulges and a second vector of hairpins (first vector of positions then vector of lengths. */
 Vec<Vec<Vec<int> > > findLoops() const {
   // DEBUG_MSG("Starting findLoops");
    Vec<int> internal; // all non-hairpins
    Vec<int> internalPos;
    Vec<int> hairpins;
    Vec<int> hairpinsPos;
    int lastPaired = -1;
    int counter = 0;
    const Vec<int>& strandIds = sequences->getStrandIds();
    for (size_t i = 0; i < size(); ++i) {
      if ((i>0) && (strandIds[i-1] != strandIds[i])) {
	counter = 0; // new strand is starting, treat last loop as dangling end
      }
      if (isBasePaired(i)) {
	if ((counter > 0) && (i>0) && (strandIds[i-1] == strandIds[i])) {
	  if (lastPaired >= 0) {
	    int partner = (*this)[i];
	    if (strandIds[partner] == strandIds[i])  {
	      if (partner == lastPaired) {
		hairpins.push_back(counter);
		hairpinsPos.push_back(lastPaired+1);
	      } else {
		internal.push_back(counter);
		internalPos.push_back(lastPaired + 1);
	      }
THROWIFNOT(isBasePaired(lastPaired),"Assertion violation in L1182");
THROWIFNOT(!isBasePaired(lastPaired+1),"Assertion violation in L1183");
	    }
	  }
	} // must have been 5' beginning of sequence ('dangling end')
	lastPaired = i;
	counter = 0;
      } else { // single stranded
	// if (counter > 0) { //  || (isBasePaired(i-1))) {
	++counter;
	  // } 
      }
    }
    Vec<Vec<Vec<int> > > finalResult;
    Vec<Vec<int> > v1;
    Vec<Vec<int> > v2;
    v1.push_back(internalPos);
    v1.push_back(internal);
    v2.push_back(hairpinsPos);
    v2.push_back(hairpins);
    finalResult.push_back(v1);
    finalResult.push_back(v2);
    // DEBUG_MSG("Finished findLoops");
    return finalResult;
  }

  /** s: states per flexible residue; f: number of flexible residues; total number of states: s^f; log of that: f*log(s) */
  int countFlexibleResidues() const {
    Vec<int> result;
    int counter = 0;
    // const Vec<int>& strandIds = sequences->getStrandIds();
    size_t pc = 0;
    for (size_t k = 0; k < sequences->size(); ++k) {
      const string& s = (*sequences)[k];
      for (size_t i = 0; i < s.size(); ++i) {
THROWIFNOT(pc < sequences->getTotalLength(),"Assertion violation in L1217");
	if (i > 0) {
	  if ((i+1) < s.size()) {
	    if ((!isBasePaired(pc)) && (!isBasePaired(pc-1)) && (!isBasePaired(pc+1))) {
	      ++counter;
	    }
	  } else { // last residue of strand
	    if ((!isBasePaired(pc)) && (!isBasePaired(pc-1))) {
	      ++counter;
	    }	    
	  }
	} else if ((!isBasePaired(pc)) && (!isBasePaired(pc+1))) { // first residue of strand
	  ++counter;
	}
	++pc;
      }
    }

    return counter;
  }

  /*
  Vec<int> findLoops() const {
    Vec<int> result;
    const Vec<int>& strandIds = sequences->getStrandIds();
    size_t n = strandIds.size();
    Vec<int> visited(strandIds.size(), 0);
    for (size_t i = 1; i < size(); ++i) {
      if ((!isBasePaired(i)) && (isBasePaired(i-1)) && (strandIds[i] == strandIds[i-1])) {
	int j = (*this)[i];
 	int counter = 0;
	int counter2 = 0;
	for (int i2 = i; i2 < static_cast<int>(n); ++i2) {
	  if ((visited[i2]) || isBasePaired(i2) || (strandIds[i2] != strandIds[i])) {
	    if (strandIds[i2] != strandIds[i]) {
	      counter = 0;
	    }
	    break;
	  }
	  ++counter;
	  visited[i2] = 0;
	}
	for (int j2 = j; j2 >= 0; --j2) {
	  if ((visited[j2]) || isBasePaired(j2) || (strandIds[j2] != strandIds[j])) {
	    if (strandIds[j2] != strandIds[j]) {
	      counter = 0;
	    }
	    break;
	  }
	  ++counter2;
	  visited[j2] = 0;
	}
	int counterSum = counter + counter2;
	if (counterSum > 0) {
	  result.push_back(counterSum);
	}
      }
    }
    return result;
  }
  */

 private:

   int basePairDist(int start1, int stop1, int start2, int stop2) const {
     PRECOND(start1 >= 0);
     PRECOND(start2 >= 0);
     PRECOND(stop1 >= 0);
     PRECOND(stop2 >= 0);
     PRECOND(start1 < static_cast<int>(size()));
     PRECOND(start2 < static_cast<int>(size()));
     PRECOND(stop1 < static_cast<int>(size()));
     PRECOND(stop2 < static_cast<int>(size()));
     const Vec<int>& strandIds = sequences->getStrandIds();
THROWIFNOT(strandIds.size() == size(),"Assertion violation in L1291");
     if ((strandIds[start1] == strandIds[start2]) && (strandIds[stop1] == strandIds[stop2])) {
       return abs(start1-start2) + abs(stop1-stop2);
     }
     return -1;
   }

  int basePairDist(int start, int stop, const Stem& helix) const {
    // Rcpp::Rcout << "Starting basePairDist " << start << " " << stop << " " << helix << endl;
    int d1 = basePairDist(start,stop, helix.getStart(), helix.getStop());
    int d2 = basePairDist(start,stop, helix.getStart() + helix.getLength()-1, helix.getStop() - helix.getLength() + 1);
    int result = 0;
    // Rcpp::Rcout << "basePairDist " << (start+1) << " " << (stop+1) << " " << helix << " : " << d1 << " " << d2 << endl;
    if (d1 < 0) {
      result = d2;
    } else if (d2 < 0) {
      result = d1;
    } else if (d1 < d2) {
      result = d1;
    } else {
      result = d2;
    }
    // Rcpp::Rcout << "Finished basePairDist " << start << " " << stop << " " << helix << " : " << result << endl;
    return result;
  }


  static string createHelixHash(const Stem& stem) {
    int invariant = stem.getStart() + stem.getStop();
    return itos(invariant);
  }

  virtual void updateHelicesHash() {
    helicesHash = "";
    Vec<string> hashes;
    for (size_t i = 0; i < helices.size(); ++i) {
      hashes.push_back(createHelixHash(helices[i]));
    }
    sort(hashes.begin(), hashes.end());
    for (size_t i = 0; i < hashes.size(); ++i) {
      if (i > 0) {
	helicesHash = helicesHash + "_";
      }
      helicesHash = helicesHash + createHelixHash(helices[i]);
    }
  }


};

class ResiduePairingComparator {

 public:
  ResiduePairingComparator() {
  }

  ResiduePairingComparator(const ResiduePairingComparator& other) {
    copy(other);
  }

  ~ResiduePairingComparator() { }

  ResiduePairingComparator& operator = (const ResiduePairingComparator& other) {
    if (&other != this) {
      copy(other);
    }
    return *this;
  }
  
  void copy(const ResiduePairingComparator& other) {
  }

  /** Implements "<" operator */
  bool operator () (const ResiduePairing& pairing1, const ResiduePairing& pairing2) {
    if (pairing1.size() != pairing2.size()) {
      return pairing1.size() < pairing2.size();
    }
    for (ResiduePairing::size_t i = 0; i < pairing1.size(); ++i) {
      if (pairing1[i] != pairing2[i]) {
	return pairing1[i] < pairing2[i];
      }
    }

    return false; // must be equal, so pairing1 is not smaller pairing2
  }
  

 private:


};

/** Needed for set operation */
/*
inline
bool
operator < (const ResiduePairing& left, const ResiduePairing& right) {
  ResiduePairingComparator comp;
  bool result = comp(left, right);
#ifndef NDEBUG
  Rcpp::Rcout << "# result of comparing two structures: " << result << " : ";
  left.writeHelices(Rcpp::Rcout);
  Rcpp::Rcout << " | ";
  right.writeHelices(Rcpp::Rcout);
  Rcpp::Rcout << endl;
#endif
  return result;
}
*/

/** Needed for set operation */
inline
bool
operator >= (const ResiduePairing& left, const ResiduePairing& right) {
  // ResiduePairingComparator comp;
  return ! (left < right);
}


/** Needed for set operation */
inline
bool
operator == (const ResiduePairing& left, const ResiduePairing& right) {
  // ResiduePairingComparator comp;
  return (!(left < right)) && (!(right > left));
}

inline
bool
operator != (const ResiduePairing& left, const ResiduePairing& right) {
  // ResiduePairingComparator comp;
  return (left < right) || (right < left);
}

class ResiduePairingAttributeComparator {
 private:
 
 string name;

 public:
  ResiduePairingAttributeComparator(const string& _name) {
    name = _name;
  }

  ResiduePairingAttributeComparator(const ResiduePairingAttributeComparator& other) {
    copy(other);
  }

  ~ResiduePairingAttributeComparator() { }

  ResiduePairingAttributeComparator& operator = (const ResiduePairingAttributeComparator& other) {
    if (&other != this) {
      copy(other);
    }
    return *this;
  }
  
  void copy(const ResiduePairingAttributeComparator& other) {
    name = other.name;
  }

  bool operator() (const ResiduePairing& pairing1, const ResiduePairing& pairing2) const {
    PRECOND(pairing1.hasProperty(name));
    PRECOND(pairing2.hasProperty(name));
    return pairing1.getProperty(name) < pairing2.getProperty(name);
  }
};


class ResiduePairingNegEnergyComparator {

 public:

  /** Functor implementation of "<" operator. Used for algorithms such as lower_bound etc */
  bool operator() (const ResiduePairing& pairing1, const ResiduePairing& pairing2) const {
    PRECOND(pairing1.hasProperty("negenergy"));
    PRECOND(pairing2.hasProperty("negenergy"));
    return (pairing1.getProperty("negenergy")) < (pairing2.getProperty("negenergy"));
  }

};

class ResiduePairingFreeEnergyComparator {

 public:

  /** Functor implementation of "<" operator. Used for algorithms such as lower_bound etc. In this case the highest free energy will be first. */
  bool operator() (const ResiduePairing& pairing1, const ResiduePairing& pairing2) const {
    return (pairing1.computeAbsoluteFreeEnergy() > pairing2.computeAbsoluteFreeEnergy()); // "worse" means higher free energy
  }

};


class ResiduePairingPropertyComparator {

 private:
 
  string propertyName;
  bool higherIsBetter;

 public:

 ResiduePairingPropertyComparator(const string& _propertyName, bool _higherIsBetter) : propertyName(_propertyName), higherIsBetter(_higherIsBetter) { }
  
  // TODO: implement copy constructor assignment operator

  /** Functor implementation of "<" operator. Used for algorithms such as lower_bound etc. In this case the highest free energy will be first. */
  bool operator() (const ResiduePairing& pairing1, const ResiduePairing& pairing2) const {
    if (higherIsBetter) {
      return (pairing1.getProperty(propertyName) > pairing2.getProperty(propertyName)); // "worse" means higher free energy
    }
    return (pairing1.getProperty(propertyName) < pairing2.getProperty(propertyName)); // "worse" means higher free energy
  }

};


class ResiduePairingHelicesHashComparator {

 public:

  /** Functor implementation of "<" operator. Used for algorithms such as lower_bound etc */
  bool operator() (const ResiduePairing& pairing1, const ResiduePairing& pairing2) const {
    return (pairing1.getHelicesHash() < pairing2.getHelicesHash());
  }

};


class ResiduePairingClassifier {
 public:
  virtual int classify(const ResiduePairing& pairing) const = 0;

};


class ResiduePairingStemClassifier : public ResiduePairingClassifier {

 public:
  
 ResiduePairingStemClassifier(const Stem& _stem) : stem(_stem) { }

  /** Returns 1 iff all base pairs of stem are present in pairig and zero otherwise. */
  virtual int classify(const ResiduePairing& pairing) const {
    ERROR_IF(stem.getLength() == 0, "Internal error: stem undefined!");
    // Rcpp::Rcout << "ResiduePairing.classify working on stem " << stem << endl;
    for (Stem::index_type i = 0; i < stem.getLength(); ++i) {
      auto start = stem.getStart() + i;
THROWIFNOT(i <= stem.getStop(),"Assertion violation in L1540");
      auto stop = stem.getStop() - i;
      if (!pairing.isBasePaired(start, stop)) {
	return 0;
      }
    }
    
    return 1;
  }

 private:

  Stem stem;
  
};


// stacking energy between helices i and j
inline
double
ResiduePairing::computeStackingEnergy(size_t hid) const {
    int start = helices[hid].getStart();
    int stop = helices[hid].getStop();
    int len = helices[hid].getLength();
    int l1 = 0;
    int l2 = 0;
    int pos1 = -1;
    int pos2 = -1;
    int startx = start + len - 1;
    int stopx  = stop - len + 1;
    const string& totseq = sequences->getConcatSequence();
    char c11 = totseq[start + len - 1];
    char c12 = totseq[stop - len + 1];

    const Vec<int>& strandIds = sequences->getStrandIds();
    int h1sstart = strandIds[helices[hid].getStart()];
    int h1sstop = strandIds[helices[hid].getStop()];
    for (int i = 0; i <= BULGE_MAX; ++i) {
      int pos = start + len +i;
      if ((pos >= static_cast<index_t>(size())) || (strandIds[pos] != h1sstart)) {
	l1 = -1; // dangling end
	break;
      }
      if (isBasePaired(pos)) {
	if ((*this)[pos] == stopx) {
	  l1 = -1; // is hairpin
	} else {
	  pos1 = pos;
	}
	break; // end of loop
      }
      ++l1;
    }
    for (int i = 0; i <= BULGE_MAX; ++i) {
      int pos = stop - len - i;
      if ((pos < 0) || ((pos - startx) <= LOOP_LEN_MIN)
	  || (strandIds[pos] != h1sstop)) {
	l2 = -1; // dangling end
	break;
      }
      if (isBasePaired(pos)) {
	if ((*this)[pos] == startx) {
	  l2 = -1; // is hairpin
	} else {
	  pos2 = pos;
	}         
	break; // end of loop
      }
      ++l2;
    }
    double e1 = 0.0;
    double e2 = 0.0;
    if (pos1 >= 0) { // try first helix
      char c21A = totseq[pos1];
      int pos1Bp = (*this)[pos1];
THROWIFNOT(isBasePaired(pos1),"Assertion violation in ResiduePairing.h :  L1614");
THROWIFNOT(pos1Bp >= 0,"Assertion violation in ResiduePairing.h :  L1615");
THROWIFNOT(pos1Bp < size(),"Assertion violation in ResiduePairing.h :  L1616");
      char c22A = totseq[pos1Bp];
      // Rcpp::Rcout << "trying first adjacent helix for " << (pos1+1) << " " << totseq[pos1] << " " << ((*this)[pos1]+1) << " " << c22A << endl;
THROWIFNOT(ResidueModel::isWatsonCrick(c21A, c22A, true),"Assertion violation in ResiduePairing.h :  L1619");
      e1 = residueModel->getStackingEnergy(c11,c12, c21A, c22A);
      e1 -= residueModel->getTerminalEnergy(c11, c12);
      e1 -= residueModel->getTerminalEnergy(c21A, c22A);
    }
    if (pos2 >= 0) { // try second helix
THROWIFNOT(pos2 >= 0,"Assertion violation in ResiduePairing.h :  L1625");
      if (pos2 != pos1) {
	char c21B = totseq[pos2];
	int pos2Bp = (*this)[pos2];
THROWIFNOT(isBasePaired(pos2),"Assertion violation in ResiduePairing.h :  L1629");
THROWIFNOT(pos2Bp >= 0,"Assertion violation in ResiduePairing.h :  L1630");
THROWIFNOT(pos2Bp < size(),"Assertion violation in ResiduePairing.h :  L1631");
	char c22B = totseq[pos2Bp];
	// Rcpp::Rcout << "trying second helix for " << (pos2+1) << " " << totseq[pos2] << " " << ((*this)[pos2]+1) << " " << c22B << endl;
THROWIFNOT(ResidueModel::isWatsonCrick(c21B, c22B, true),"Assertion violation in ResiduePairing.h :  L1634");
	e2 = residueModel->getStackingEnergy(c11,c12, c21B, c22B);
	e2 -= residueModel->getTerminalEnergy(c11, c12);
	e2 -= residueModel->getTerminalEnergy(c21B, c22B);
      }
    }
    double result = e1;
    if (e2 < result) {
      result = e2;
    }
    /* char c21 = totseq[start + len + l1]; */
    /* char c22 = totseq[stop - len - l2];     */
    /* double result = 0.0; */
    /* if ((l1 >= 0) && (l1 <= BULGE_MAX) && (l2 >= 0) && (l2 <= BULGE_MAX)) { */
    /*   EASSERT(ResidueModel::isWatsonCrick(c11,c12, true)); */
    /*   EASSERT(ResidueModel::isWatsonCrick(c21,c22, true)); */
    /*   result = residueModel->getStackingEnergy(c11,c12, c21, c22); */
    /*   // Rcpp::Rcout << "Found internal loop with lengths " << l1 << " " << l2 << c11 << " " << c12 << " " << c21 << " " << c22 << " : " << result << endl;  */
    /*   // Rcpp::Rcout << "Helices: " << helices << endl; */
    /*   // undo helix ends penalties */
    /*   result -= residueModel->getTerminalEnergy(c11, c12); */
    /*   result -= residueModel->getTerminalEnergy(c21, c22); */
    /*   // Rcpp::Rcout << "After decapping: " << result << endl; */
    /* } */
    return result;
  }


#endif
