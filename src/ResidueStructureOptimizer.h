#ifndef RESIDUE_STRUCTURE_OPTIMIZER
#define RESIDUE_STRUCTURE_OPTIMIZER

#include "ResidueStructure.h"

class ResidueStructureOptimizer{

  virtual ResidueStructure optimize(const ResidueStructure& structure) const = 0;

};

#endif
