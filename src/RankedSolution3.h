// --*- C++ -*------x---------------------------------------------------------
// $Id: RankedSolution3.h,v 1.3 2009/02/27 15:29:37 bindewae Exp $
//
// Class:           RankedSolution3
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     Implements ranked solutions
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

#ifndef __RANKED_SOLUTION3_H__
#define __RANKED_SOLUTION3_H__

// Includes

#include <iostream>
#include "debug.h"

template<class T>
class RankedSolution3 {
public:
  RankedSolution3<T>() : first(0.0) { } 

  RankedSolution3<T>(double _first, const T& _second) : first(_first), second(_second) {
  }

  RankedSolution3<T>(const RankedSolution3<T>& orig) { copy(orig); }

  virtual ~RankedSolution3<T>() {} 

  /* OPERATORS */

  /** Assigment operator. */
  RankedSolution3<T>& operator = (const RankedSolution3<T>& orig) {
    if (&orig != this) {
      copy(orig);
    }
    return *this;
  }

  friend ostream& operator << (ostream& os, const RankedSolution3<T>& orig) {
    os << orig.first << " " << orig.second; return os;
  }

  friend istream& operator >> (istream& is, RankedSolution3<T>& orig) {
    is >> orig.first >> orig.second; return is;
  }

  /* PREDICATES */

  double getFirst() const { return first; }

  const T& getSecond() const { return second; }

  /* MODIFIERS */
  void copy(const RankedSolution3<T>& other) {
    first = other.first;
    second = other.second;
  }

  void setFirst(double val) { first = val; }

  void setSecond(const T& val) { second = val; }

  /* ATTRIBUTES */

  double first;
  T second;

protected:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* PRIVATE ATTRIBUTES */

};

/** comparison operator. */
template<class T>
inline
bool operator < (const RankedSolution3<T>& lval,
		 const RankedSolution3<T>& rval) { 
  return lval.first < rval.first;
}

/** comparison operator. */
template<class T>
inline
bool operator < (double lval, const RankedSolution3<T>& rval) {
  return lval < rval.first;
}

/** comparison operator. */
template<class T>
inline
bool operator < (const RankedSolution3<T>& lval, double rval)   {
  return lval.first < rval;
}

/** generates vector consisting only of elements (not scores) */
template<class T>
inline
Vec<T>
generateElementVector(const Vec<RankedSolution3<T> >& rv) {
  Vec<T> result(rv.size());
  for (typename Vec<T>::size_type i = 0; i < rv.size(); ++i) {
    result[i] = rv[i].getSecond();
  }
  return result;
}

/** generates vector consisting only of scores (not elements) */
template<class T>
inline
Vec<double>
generateScoreVector(const Vec<RankedSolution3<T> >& rv) {
  Vec<double> result(rv.size());
  for (typename Vec<double>::size_type i = 0; i < rv.size(); ++i) {
    result[i] = rv[i].getFirst();
  }
  return result;
}


#endif /* __ACLASS_H__ */

