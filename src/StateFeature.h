#ifndef STATE_FEATURE
#define STATE_FEATURE

#include "Vec.h"

class StateFeature {

 public:

  typedef unsigned int digit_t;

  typedef Vec<digit_t> state_t;

  virtual bool hasFeature(const state_t& state) const = 0;
};

#endif
