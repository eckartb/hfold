#ifndef RESIDUE_PAIRING_STEM_SOLUTION_H
#define RESIDUE_PAIRING_STEM_SOLUTION_H

#include "state_t.h"
#include "ResiduePairing.h"

class ResiduePairingStemSolution {

 public:
  
  typedef state_t container_t;
  typedef int id_type;

  enum { INVALID_ID = -2, NO_PARENT_ID = -1 };

  // static id_type globalIdCounter;

 private:

  double energy;
  id_type id;
  id_type parentId;
  container_t stemIds;
  ResiduePairing structureG;
  ResiduePairing structureH;

 public:

  ResiduePairingStemSolution(double _energy, id_type _id, id_type _parentId, const container_t& _stemIds, const ResiduePairing& _structureG, const ResiduePairing& _structureH) : energy(_energy), id(_id), parentId(_parentId), stemIds(_stemIds) {    
    //     id = ++globalIdCounter;
    // parentId = _parentId;
    // stemIds = _stemIds;
    structureG = _structureG;
    structureH = _structureH;
    POSTCOND(id < 10000000);
    POSTCOND(parentId < 10000000);
    POSTCOND(validate());
  }

  ResiduePairingStemSolution() : id(INVALID_ID), parentId(INVALID_ID) {}

  ResiduePairingStemSolution(const ResiduePairingStemSolution& other) {
    PRECOND(other.validate());
    copy(other);
    POSTCOND(validate());
  }

  virtual ~ResiduePairingStemSolution() { }

  ResiduePairingStemSolution& operator = (const ResiduePairingStemSolution& other) {
    PRECOND(other.validate());
    if (this != &other) {
      copy(other);
    }
    POSTCOND(validate());
    return *this;
  }

  /** Add id of stem that corresponds to "stem not placed" */
  void addStemIdZero() {
    stemIds.push_back(0);
  }

  void copy(const ResiduePairingStemSolution& other) {
    id = other.id;
    parentId = other.parentId;
    stemIds = other.stemIds;
    structureG = other.structureG;
    structureH = other.structureH;
  }

  double getEnergy() const { return energy; }

  id_type getId() const { return id; }

  id_type getParentId() const { return parentId; }

  const container_t& getStemIds() const { return stemIds; }

  const ResiduePairing& getStructureG() const { return structureG; }

  const ResiduePairing& getStructureH() const { return structureH; }

  bool validate() const {
    return true;
    // return id != INVALID_ID;
  }

};

/** This reverse sorting order is good for the priority_queue (sorted by largest score/priority first). In this fashion, the lowest free energy is first. */
inline 
bool
operator < (const ResiduePairingStemSolution& left, const ResiduePairingStemSolution& right) {
  return left.getStructureG().computeAbsoluteFreeEnergy() > right.getStructureG().computeAbsoluteFreeEnergy();
}

#endif
