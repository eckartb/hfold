#ifndef _GRAPH_TOOLS_
#define _GRAPH_TOOLS_

#include <set>
#include "Vec.h"
#include "verbose.h"

using namespace std;

typedef int graph_base_t;
typedef std::set<graph_base_t> graph_id_set_t;
typedef std::set<graph_id_set_t> graph_id_sets_t;
typedef Vec<graph_base_t> adjacency_matrix_row_t;
typedef Vec<adjacency_matrix_row_t> adjacency_matrix_t;
typedef std::set<graph_base_t> adjacency_list_element_t;
typedef Vec<adjacency_list_element_t> adjacency_list_t;
typedef Vec<graph_base_t>::size_type graph_size_t;

#define GRAPH_NO_CONNECTION 0
#define GRAPH_CONNECTION 1

inline
bool
is_subset_of(const set<graph_base_t>& set1, const set<graph_base_t>& set2) {
  if (set1.size() > set2.size()) {
    return false;
  }
  for (set<graph_base_t>::const_iterator it = set1.begin(); it != set1.end(); it++) {
    if (set2.find(*it) == set2.end()) { // element not found
      return false;
    }
  }
  return true;
}

/** returns true, if and only if the two sets have at least one element in common */
inline
bool
has_intersection(const set<graph_base_t>& set1, const set<graph_base_t>& set2) {
  for (set<graph_base_t>::const_iterator it = set1.begin(); it != set1.end(); it++) {
    if (set2.find(*it) != set2.end()) { // element not found
      return true;
    }
  }
  return false;
}

/** Returns true, if two vertices in a graph are connected by an edge */
inline
bool
is_connected(const adjacency_list_t& adj, graph_base_t e1, graph_base_t e2) {
  return adj[e1].find(e2) != adj[e1].end();
}

/** Returns set of elements where every node is connected with every other node */
inline
adjacency_list_element_t
find_clique(const adjacency_list_t& adj, graph_base_t id) {
  adjacency_list_element_t result;
  // go through elements of id
  for (set<graph_base_t>::const_iterator it = adj[id].begin(); it != adj[id].end(); it++) {
    bool disconnect = false;
    for (set<graph_base_t>::const_iterator it2 = result.begin(); it2 != result.end(); it2++) {
      if (!is_connected(adj, *it, *it2)) {
	disconnect = true;
	break;
      }
    }
    if (!disconnect) {
      // Rcpp::Rcout << "adding " << (*it) << endl;
      result.insert(*it);
    }
  }
  // Rcpp::Rcout << "Inserting " << id << endl;
  result.insert(id);
  return result;
}

inline
adjacency_list_t 
adjacency_matrix_to_adjacency_list(const adjacency_matrix_t matrix) {
  adjacency_list_t result(matrix.size());
  for (graph_size_t i = 0; i < matrix.size(); ++i) {
    for (graph_size_t j = i; j < matrix.size(); ++j) {
      if (matrix[i][j] != 0) {
	result[i].insert(j);
	result[j].insert(i);
      }
    }
  }
  return result;
}

inline
void
write_adjacency_list(ostream& os, const adjacency_list_t& adj) {
  for (graph_size_t i = 0; i < adj.size(); ++i) {
    os << i << " :";
    for (adjacency_list_element_t::const_iterator it = adj[i].begin(); it!= adj[i].end(); it++) {
      os << " " << (*it);
    }
    os << endl;
  }
}

inline
adjacency_matrix_t 
adjacency_list_to_adjacenty_matrix(const adjacency_list_t adj) {
  adjacency_matrix_t result(adj.size(), adjacency_matrix_row_t(adj.size(), GRAPH_NO_CONNECTION));
  for (graph_size_t i = 0; i < adj.size(); ++i) {
    for (adjacency_list_element_t::const_iterator j = adj[i].begin(); j != adj[i].end(); j++) {
      result[i][*j] = GRAPH_CONNECTION;
      result[*j][i] = GRAPH_CONNECTION;
    }
  }
  return result;
}

// template<class T>
// inline
// ostream&
// operator << (ostream& os, const set<T>& s) {
//   os << s.size() << " ";
//   for (typename set<T>::const_iterator it = s.begin(); it != s.end(); it++) {
//     os << " " << (*it);
//   }
//   return os;
// }

/** For given graph (adjacency list representation) it finds from a start vertex a connected component */
inline
adjacency_list_element_t
adjacency_to_component(const adjacency_list_t& adj, graph_base_t startVertex, graph_id_set_t tabooIds) {
  adjacency_list_element_t result;
  if (VERBOSE_GLOBAL >= VERBOSE_FINEST) {
    Rcpp::Rcout << "Starting adjacency_to_component with start vertex " << startVertex << endl;
  //  Rcpp::Rcout << "Adjacencies: " << adj << endl << " Taboo: " << tabooIds << endl;
    write_adjacency_list(Rcpp::Rcout, adj);
  }

  if (tabooIds.find(startVertex) != tabooIds.end()) {
    return result; // start vertex was taboo
  }
  result.insert(startVertex);
  tabooIds.insert(startVertex);
  const adjacency_list_element_t& elems = adj[startVertex];
  // Rcpp::Rcout << "Vertices adjacent to start vertex " << startVertex << " : ";
  // Rcpp::Rcout << elems << endl;
  for (adjacency_list_element_t::const_iterator it = elems.begin(); it != elems.end(); it++) {
    // Rcpp::Rcout << "Considering launching recursive search from vertex " << (*it) << endl;
    if (tabooIds.find(*it) == tabooIds.end()) { // not found
      result.insert(*it);
      adjacency_list_element_t subComp = adjacency_to_component(adj, *it, tabooIds);
      // Rcpp::Rcout << "Number of found vertices as result of recursive search started from vertex " << (*it) << " : "<< subComp.size() << endl;
      for (adjacency_list_element_t::const_iterator jt = subComp.begin(); jt != subComp.end(); jt++) {
	result.insert(*jt);
	tabooIds.insert(*jt);
      }
    } else {
      // Rcpp::Rcout << "Not considering launching recursive search from verttex - it was taboo: " << (*it) << endl;
    }
  }
  // if (VERBOSE_GLOBAL >= VERBOSE_FINEST) {
  //   Rcpp::Rcout << "Result of adjacency_to_component (start vertex " << startVertex << "):" << endl;
  //   Rcpp::Rcout << result << endl;
  // }
  return result;
}

/** For given grah (adjacency list representation) it finds all connected components */
inline
graph_id_sets_t
adjacency_to_components(const adjacency_list_t& adj) {
  std::set<std::set<graph_base_t> > result;
  if (adj.size() == 0) {
    return result;
  }
THROWIFNOT(adj.size() > 0,"Assertion violation in L178");
  // if (VERBOSE_GLOBAL >= VERBOSE_FINEST) {
  //   Rcpp::Rcout << "Starting adjacency_to_components Adjacencies: " << adj << endl;
  //   write_adjacency_list(Rcpp::Rcout, adj);
  // }
  graph_base_t pc = 0;
  graph_base_t n = adj.size(); // total number of vertices
  Vec<int> usedFlags(n, 0); // check which vertex has been used
  graph_id_set_t tabooIds;
  while (pc < n) {
    if (usedFlags[pc] == 0) {
      if (VERBOSE_GLOBAL >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Starting adjacency_to_components with start vertex " << pc << " and taboo ids: ";
	for (graph_id_set_t::const_iterator ii = tabooIds.begin(); ii != tabooIds.end(); ++ii) {
	  Rcpp::Rcout << " " << (*ii);
	}
	Rcpp::Rcout << endl;
      }
      graph_id_set_t component = adjacency_to_component(adj, pc, tabooIds);  // start with first component
      result.insert(component);
      usedFlags[pc] = 1;
      // Rcpp::Rcout << "Found vertices, that are directly connected to vertex " << pc << ":" << endl;
      for (graph_id_set_t::const_iterator i = component.begin(); i != component.end(); i++) {
	// Rcpp::Rcout << (*i) << endl;
	usedFlags[*i] = 1;
	tabooIds.insert(*i);
      }
    }
    pc++;
  }
  return result;
}


class GraphTools {
 public:

  GraphTools() {
    bool verbose = true;
    testGraph(verbose);
  }

  static bool testGraph(bool verbose) {
    return testGraph1(verbose) && testGraph2a(verbose) && testGraph2b(verbose);
  }

  static bool testGraph1(bool verbose) {
    if (verbose) {
      Rcpp::Rcout << "Starting GraphTools::testGraph1" << endl;
    }
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[2][0] = 1;
    graph_id_sets_t result = adjacency_to_components(adjacency_matrix_to_adjacency_list(matrix));
    if (verbose) {
      Rcpp::Rcout << "Test 1 of connectivityToHashes:" << endl << result.size();
    }
THROWIFNOT(result.size() == 2,"Assertion violation in L235");

    return true;
  }

  static bool testGraph2a(bool verbose) {
    if (verbose) {
      Rcpp::Rcout << "Starting GraphTools::testGraph2a" << endl;
    }
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[2][0] = 1;
    matrix[1][2] = 1;
    matrix[2][1] = 1;
    // vertices 0 and 1 are only indirectly connected
    graph_id_set_t taboo;
    graph_id_set_t result = adjacency_to_component(adjacency_matrix_to_adjacency_list(matrix), 0, taboo);
    if (verbose) {
      Rcpp::Rcout << "Test 2a of connectivityToHashes:" << endl << result.size();
      for (graph_id_set_t::const_iterator i = result.begin(); i != result.end(); i++) {
	Rcpp::Rcout << (*i) << " ";
      }
      Rcpp::Rcout << endl;
    }
THROWIFNOT(result.size() == 3,"Assertion violation in L259");

    return true;
  }


  static bool testGraph2b(bool verbose) {
    if (verbose) {
      Rcpp::Rcout << "Starting GraphTools::testGraph2b" << endl;
    }
    adjacency_matrix_t matrix(3, adjacency_matrix_row_t(3, 0));
    matrix[0][2] = 1;
    matrix[1][2] = 1;
    matrix[2][1] = 1;
    matrix[2][0] = 1;
    graph_id_sets_t result = adjacency_to_components(adjacency_matrix_to_adjacency_list(matrix));
    if (verbose) {
      Rcpp::Rcout << "Test 2 of connectivityToHashes:" << endl << result.size();
    }
THROWIFNOT(result.size() == 1,"Assertion violation in L278");

    return true;
  }

};

#endif
