// --*- C++ -*------x---------------------------------------------------------
// $Id: 
//
// Class:           Limits
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     Some important numerical limits.
// 
// -----------------x-------------------x-------------------x-----------------

#ifndef __LIMITS_H__
#define __LIMITS_H__

#include <math.h>

const double REALLY_HUGE = 1.0E20;

const double EPSILON = 0.01;

const double EPSILONISSIMO = std::numeric_limits< double >::min() * 10; // 2.22507e-308 * 10 for 64 bit

const double MAX_REASONABLE = REALLY_HUGE; // 1000000000.0; // one billion

/** return true, if values are closer than EPSILON ("similar") 
    description return true, if values are closer than EPSILON ("similar") 
*/
inline 
bool 
isSimilar(double a, double b)
{
  return (fabs(a-b) < EPSILON);
}

/** is "a" close to zero ? */
inline
bool
isTiny(double a)
{
  return (fabs(a) < EPSILON);
}

/** return false if not a number (NaN) */
inline
bool
isDefined(double val) {
  return (val < 0.0) || (val >= 0.0);
}

/** return true, if not obscure number */
inline
bool
isReasonable(double val) {
  return isDefined(val) && (fabs(val) < MAX_REASONABLE);
}

#endif /* __LIMITS__ */

