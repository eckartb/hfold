#ifndef _BASEPAIR_H
#define _BASEPAIR_H

#include <utility>
#include "RankedSolution3.h"
#include "RankedSolution4.h"
#include "RankedSolution6.h"

typedef int basepair_index_t;
typedef pair<basepair_index_t, basepair_index_t> basepair_t;
typedef RankedSolution3<basepair_t> basepair_score_t;
typedef RankedSolution6<double, basepair_index_t, basepair_index_t> basepair_score_pair_t; // scores a pair of scores for each base pair
typedef RankedSolution4<char, basepair_t> folding_event_t;

#define FOLDED_BASEPAIR 'F'
#define UNFOLDED_BASEPAIR 'U'


/** Useful for output of pairs */
inline
std::ostream& operator << (std::ostream& os , const basepair_t& pair) {
  os << (pair.first+1) << " " << (pair.second+1);
  return os;
}

/** Useful for output of folding or unfolding of single base pairs */
inline
std::ostream& operator << (std::ostream& os , const folding_event_t& event) {
  os << event.first << " " << event.second;
  return os;
}


inline
basepair_t generate_basepair(basepair_index_t start, basepair_index_t stop) {
  if (start > stop) {
    return generate_basepair(stop, start);
  }
  return basepair_t(start, stop);
}

/** Returns true if start index is smaller than stop index */
inline
bool validate_basepair(const basepair_t& bp) { return bp.first < bp.second; }

#endif
