#ifndef __ABSTRACT_SECONDARY_STRUCTURE__
#define __ABSTRACT_SECONDARY_STRUCTURE__

#include <iostream>
#include "debug.h"
#include "Vec.h"
#include <string>
#include "Stem.h"

using namespace std;
 
class AbstractSecondaryStructure {

 public:

  typedef int index_type;
  typedef Vec<int> basepair_set_type;
  typedef string sequence_type;
  typedef Vec<int>::size_type size_type;

  enum { UNPAIRED_ID = -1 };

  virtual ~AbstractSecondaryStructure() { }

  virtual const basepair_set_type& getBasePairs() const = 0;

  /** returns base-paired partner, -1 if otherwise */
  virtual index_type getBasePair(index_type n) const = 0;

  /** Counts number of base pairs */
  virtual size_type getBasePairCount() const = 0;

  virtual double getEnergy() const = 0;

  virtual void setEnergy(double x) = 0;

  /** returns sequence length, but in type "index_type" (typically int instead of unsigned int) */
  virtual index_type getLength() const = 0;
  
  virtual const sequence_type& getSequence() const = 0;

  virtual const Vec<int>& getStarts() const = 0;

  virtual size_type getSegmentCount() const = 0;

  virtual string getSegmentSequence(size_type n) const = 0;

  /** returns true if nt n is involved in a base pair */
  virtual bool isBasePaired(index_type n) const = 0;

  virtual bool isConsistent() = 0;

  virtual bool isPaired(index_type i, index_type j) const = 0;

  virtual bool isPaired(index_type i) const = 0;

  virtual void setBasePairs(const basepair_set_type& bp) = 0;

  virtual void setPaired(index_type i, index_type j) = 0;

  virtual void setUnpaired(index_type i, index_type j) = 0;

  virtual void setSequence(const sequence_type& seq) = 0;

  virtual void setStarts(const Vec<int>& _starts) = 0;
  
  /** returns sequence length */
  virtual size_type size() const = 0;

  /** returns true, if two base pairs form a pseudoknot */
  static bool isPseudoKnotted(index_type startA, index_type stopA,
			      index_type startB, index_type stopB) {
    PRECOND(startA < stopA);
    PRECOND(startB < stopB);
    PRECOND(startA != startB);
    PRECOND(stopA != stopB);
    return ( ((startA < startB) && (startB < stopA) && (stopB > stopA))
	     || ((startB < startA) && (startA < stopB) && (stopA > stopB)));
  }


};


#endif
