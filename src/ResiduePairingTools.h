#ifndef RESIDUE_PAIRING_TOOLS_H
#define RESIDUE_PAIRING_TOOLS_H

#include "ResiduePairing.h"
#include "ResidueStructure.h"
#include <iostream>
#include <map>
#include <algorithm>
#include "Stem.h"
#include "stemhelp.h"
#include "ResidueModel.h" 
#include "RnaSecondaryStructure.h"

class ResiduePairingTools {

 public:

  typedef ResiduePairing::size_t size_t;
  typedef int index_t;

  static double computeBasePairDist(const ResiduePairing& p1, const ResiduePairing& p2) {
    ERROR_IF(p1.size() != p2.size(), "Structures must have same size to be compared!");
    size_t n = p1.size();
    double dist = 0.0;
    for (size_t i = 0; i < n; ++i) {
      if (p1[i] != p2[i]) {
	dist += 0.5;
      }
    } 
    return dist;
  }

  /* static ResiduePairing convertFromResidueStructure(const ResidueStructure& structure, const StrandContainer * strandContainer) { */
  /*   // Rcpp::Rcout << "ResiduePairingTootls::convertFromResidueStructure:: " << structure.getResidueCount() << " " << strandContainer->getTotalLength() << endl; */
  /*   PRECOND(structure.getResidueCount() == strandContainer->getTotalLength()); */
  /*   ResiduePairing pairing(const_cast<StrandContainer *>(strandContainer), structure.getResidueModel()); */
  /*   pairing.setProperty("negenergy", - structure.getEnergy()); */
  /*   const Vec<int>& pairv = structure.getPairv(); */
  /*   for (size_t i = 0; i < pairv.size(); ++i) { */
  /*     if (pairv[i] >= 0) { */
  /* 	pairing.setBasePair(i, pairv[i]); */
  /*     } */
  /*   } */
  /*   return pairing; */
  /* } */

  /** Convert to RnaSecondaryStructure data type from consensus2 package */
  static RnaSecondaryStructure convertToRnaSecondaryStructure(const ResiduePairing& structure) {
    const StrandContainer * strandContainer = structure.getSequences();
    string concatSequence = strandContainer->getConcatSequence();
    Vec<int> starts = strandContainer->getStarts();
    Vec<Stem> stems = structure.getHelices();
    return  RnaSecondaryStructure(stems, concatSequence, starts);
  }
  
  static size_t countPlaceableBasePairs(const ResiduePairing& structure, const Stem& stem) {
    size_t count = 0;
    for (Stem::index_type i = 0; i < stem.getLength(); ++i) {
      if ((!structure.isBasePaired(stem.getStart() + i))
	  && (!structure.isBasePaired(stem.getStop() - i))) {
	++count;
      }
    }
    return count;
  }
  
  /** Reads CT file that my contain of several strands */
  static ResiduePairing readCt(istream& is, StrandContainer& strandContainer, ResidueModel& residueModel) {
    // read stems and sequence:
    Vec<Stem> stems;
    string sequence;
    unsigned int minStemLength = 1;
    Vec<int> starts;
    readCtMulti(is, stems, sequence, minStemLength, starts);
    return fromStems(strandContainer, stems, sequence, starts, residueModel);
  }

  /** Reads CT file that my contain of several strands */
  static ResiduePairing readMyCt(istream& is, StrandContainer& strandContainer, ResidueModel& residueModel) {
    // read stems and sequence:
    Vec<Stem> stems;
    string sequence;
    unsigned int minStemLength = 1;
    Vec<int> starts;
    readMyCtMulti(is, stems, sequence, minStemLength, starts);
 //   Rcpp::Rcout << "info read so far from my version of CT file format: " << stems << endl << sequence << endl << " starts: " << starts << " that's it" << endl;
    
    return fromStems(strandContainer, stems, sequence, starts, residueModel);
  }

  /** Reads CT file that my contain of several strands */
  static ResiduePairing fromStems(StrandContainer& strandContainer, const Vec<Stem>& stems, const string& sequence, const Vec<int>& starts, ResidueModel& residueModel) {
      // read stems and sequence:
      /* Vec<Stem> stems; */
      /* string sequence; */
      /* unsigned int minStemLength = 1; */
      /* Vec<int> starts; */
      /* readCtMulti(is, stems, sequence, minStemLength, starts); */
      Vec<string> sequences;
      size_t pc = 0;
    //  Rcpp::Rcout << "Stars: " << starts << endl;
    //  Rcpp::Rcout << "Sequence: " << sequence << endl;
    //  Rcpp::Rcout << "Stems: " << stems << endl;
      for (size_t i = 0; i < starts.size(); i++) {
THROWIFNOT(starts[i] < static_cast<int>(sequence.size()),"Assertion violation in L105");
	size_t len = sequence.size() - starts[i];
	if ((i+1) < starts.size()) {
	  len = starts[i+1];
	}
	string s = sequence.substr(starts[i], len);
	sequences.push_back(s);
	pc += s.size();
      }
      Rcpp::Rcout << pc << " " << sequence.size() << endl;
THROWIFNOT(pc == sequence.size(),"Assertion violation in L115");
      Vec<double> concentrations(sequences.size(), 1.0);
      strandContainer.set(sequences, concentrations);
      ResiduePairing pairing(&strandContainer, &residueModel);
      for (size_t i = 0; i < stems.size(); ++i) {
	for (index_t j = 0; j < stems[i].getLength(); ++j) {
	  Rcpp::Rcout << "Working on stem " << (i+1) << " " << stems[i] << " pos " << (j+1) << endl;
	  pairing.setBasePair(stems[i].getStart() + j, stems[i].getStop()-j);
	}
      }
      return pairing;
    }


  static Vec<string> splitString(const string& s, Vec<int> starts) {
    PRECOND(starts.size() > 0 && starts[0] == 0);
    Vec<string> result;
    starts.push_back(s.size()); // notice how "starts" is not changed for outside world, because call by value
    for (size_t i = 0; (i+1) < starts.size(); ++i) {
      result.push_back(s.substr(starts[i], starts[i+1] - starts[i]));
    }
    return result;
  }

  static void writeBracket(ostream &os, const ResiduePairing& pairing) {
    StrandContainer * sequences = pairing.getSequences();
    ERROR_IF(sequences == NULL, "Cannot write bracket notation without sequence information.");
    const Vec<int>& starts = sequences->getStarts();
    const Vec<Stem>& helices = pairing.getHelices();
    const string& seq = sequences->getConcatSequence();
    string bracket = stemsToBracketFasta(helices, seq);
    Vec<string> seqs = splitString(seq, starts);
    Vec<string> brackets = splitString(bracket, starts);
    for (size_t i = 0; i < seqs.size(); ++i) {
      os << seqs[i] << endl << brackets[i] << endl;
    }
  }

  static void writeCt(ostream& os, const ResiduePairing& pairing, const Vec<string>& sequences) {
    DEBUG_MSG("Starting ResiduePairingTools::writeCt");
    ResiduePairing::size_t n = pairing.size();
    string delim = " ";
    size_t seqCount = 0;
    size_t seqRes = 0;
    os << n << delim << sequences.size();
    size_t pc = 1; // 1-based starts
    for (size_t i = 0; i < sequences.size(); ++i) {
      os << delim << pc;
      pc += sequences[i].size();
    }
    map<string, double> properties = pairing.getProperties();
    for (map<string,double>::const_iterator it = properties.begin(); it != properties.end(); it++) {
      os << delim << it->first << ":" << it->second;
    }
    os << delim << "connectivity" << ":" << pairing.getStrandConnectivityHash() << endl;
    for (size_t i = 0; i < n; ++i) {
      os << (i+1) << delim << sequences[seqCount][seqRes] << delim << i << delim << (i+2) << delim << (pairing[i]+1) << delim << (i+1) << endl;
      ++seqRes;
      if (seqRes >= sequences[seqCount].size()) {
	seqRes = 0;
	++seqCount;
      }
    }
    DEBUG_MSG("Finished ResiduePairingTools::writeCt");
  }

  /** Write C++ code for base pair types */
  static void writeCpp(ostream& os, const ResiduePairing& pairing, const string& containerName) {
    ResiduePairing::size_t n = pairing.size();
    for (size_t i = 0; i < n; ++i) {
      if (pairing.isBasePaired(i)) {	
	os << containerName << ".push_back(basepair_t(" << i << "," << pairing[i] << "));" << endl;
      }
    }
  }

  static void writeCt(ostream& os, const ResiduePairing& pairing) {
    writeCt(os,pairing, *(pairing.getSequences()));
  }

  static void writeMultiCt(ostream& os, const Vec<ResiduePairing>& pairings, const Vec<string>& sequences, size_t maxSize=10000) {
    for (size_t j = 0; j < pairings.size(); ++j) {
      if (j >= maxSize) {
	break;
      }
      const ResiduePairing& pairing = pairings[j];
      writeCt(os, pairing, sequences);
    }
  }

  static void writeMultiCtReverse(ostream& os, const Vec<ResiduePairing>& pairings, const Vec<string>& sequences) {
    size_t n = pairings.size();
    for (size_t j = 0; j < n; ++j) {
      const ResiduePairing& pairing = pairings[n-j-1];
      writeCt(os, pairing, sequences);
    }
  }

  static void writeMultiCt(ostream& os, const set<ResiduePairing>& pairings, const Vec<string>& sequences) {
    Vec<ResiduePairing> result;
    for (set<ResiduePairing>::const_iterator j = pairings.begin(); j != pairings.end(); ++j) {
      result.push_back(*j);
    }
    sort(result.begin(), result.end(), ResiduePairingFreeEnergyComparator()); // sort by energy 
    reverse(result.begin(), result.end()); // lowest energy (highest negative energy) first
    for (size_t i = 0; i < result.size(); ++i) {
      writeCt(os, result[i], sequences);
    }
  }

  static void writeMultiCt(ostream& os, const set<ResiduePairing,ResiduePairingHelicesHashComparator>& pairings, const Vec<string>& sequences) {
    Vec<ResiduePairing> result;
    for (set<ResiduePairing>::const_iterator j = pairings.begin(); j != pairings.end(); ++j) {
      result.push_back(*j);
    }
    sort(result.begin(), result.end(), ResiduePairingFreeEnergyComparator()); // sort by energy
    reverse(result.begin(), result.end()); // lowest energy (highest negative energy) first
    for (size_t i = 0; i < result.size(); ++i) {
      writeCt(os, result[i], sequences);
    }
  }


};

#endif
