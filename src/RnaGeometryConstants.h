#ifndef __RNA_GEOMETRY_CONSTANTS__
#define __RNA_GEOMETRY_CONSTANTS__

const double HELIX_DELTA_ANGLE = 36.0 * DEG2RAD; // rize in z-coordinate per base-pair in helix

const double HELIX_CENTER_DIST = 5.0; // distance of center of base pair from helix axis

const double HELIX_BASE_DIST = 5.0; // distance of centers of bases of base pair

const double HELIX_END_SLOP = 0.1; // 2.0; // residues adjacent to helix are this much away from ideal helix position

const double RESIDUE_DIST = 3.5;

const double C4_MIN = 3.3; // minimum distance between C4* atoms

const double C4_MAX = 8.0; // 7.5; // maximum distance between C4* atoms of neighboring residues

const double C4_MAX_OFF = 2.0; // 7.5; // offset for maximum distance between C4* atoms of neighboring residues

const double WATSON_CRICK_DIST = 14.98; // extracted from PDB:157D:2:23 (C4* atoms)

const double WC_SLOP = 0.2; // 0.1; // 0.2;

const double WATSON_CRICK_DIST_MIN = WATSON_CRICK_DIST - WC_SLOP; // 3.0;

const double WATSON_CRICK_DIST_MAX = WATSON_CRICK_DIST + WC_SLOP;

////////// values taken from Aalberts 2005, Nucleic Acids Research
// TODO move to RnaPhysicalParameters
const double HELIX_BASE_PAIRS_PER_TURN = 11.2;

const double HELIX_RADIUS = 9.0; // 9.9;  // radius of C4* atom
 
const double HELIX_OUTER_RADIUS = 9.5; // 10.0;  // cylindrical radius of outermost atoms

const double HELIX_OUTER_RADIUS_DNA = 9.5; // FIXIT

const double HELIX_OUTER_RADIUS_RDHYBRID = 9.5; // FIXIT

const double HELIX_MIN_RADIUS = 3.5;  // radius of only one base pair (smallest dimension) : TODO : optimize
const double HELIX_MIN_RADIUS_DNA = 3.5; // FIXIT // radius of only one base pair (smallest dimension) : TODO : optimize
const double HELIX_MIN_RADIUS_RDHYBRID = 3.5; // FIXIT // radius of only one base pair (smallest dimension) : TODO : optimize

// const double HELIX_SLOP = 0.2; // 0.1; // position of helix of 10 residues is unsure by 10*HELIX_SLOP Angstreom

const double HELIX_SLOP_OFF = 0.1; // 0.5; // position of helix of 10 residues is unsure by 10*HELIX_SLOP Angstreom

/** rize in z-coordinate per base-pair in helix or height per stack */
const double HELIX_RISE = 2.7; 

/** rize in z-coordinate per base-pair in helix or height per stack */
const double HELIX_RISE_DNA = 2.7;  // FIXIT

const double HELIX_RISE_RDHYBRID = 2.7;  // FIXIT

/** vertical offset between complementary strands */
const double HELIX_OFFSET = -4.2;

/** vertical offset between complementary strands */
const double HELIX_PHASE_OFFSET = 93.0*DEG2RAD;

const double SINGLE_STRAND_DIST = 6.2;

#endif
