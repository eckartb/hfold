#ifndef STRENUM_TOOLS_H
#define STRENUM_TOOLS_H

#include <math.h>
#include <map>

#include "Vec.h"
#include "Graph.h"
#include "StringTools.h"
#include "MultiIterator.h"
#include "state_t.h"

class StrenumTools {

 public:

  typedef string::size_type size_t;
  typedef state_t::digit_t digit_t;
  // typedef MultiIterator::digit_t digit_t;
  //  typedef Vec<digit_t> state_t;
  typedef Vec<state_t> state_container_t;

  /** Returns true, if and only iff structure does not contain helices */
  static bool isBaseState(const state_t& state) {
    for (auto it = state.begin(); it != state.end(); it++) {
      if ((*it) > 1) {
	return false;
      }
    }
    return true;
  }

  /* port of R code:
# determine whether a change from one state to another is legitimate
# only allowed cases are where a new helix is formed, a helix is dissolved, or two helices have branch migration
KineticStateChange <- function(v1, v2, helixMaxIds, differentMax=1, debugInfo=FALSE, connectivityChanged=FALSE) {
  stopifnot(length(v1) == length(v2))		   
  stopifnot(length(v1) == length(helixMaxIds))
  stopifnot(is.numeric(differentMax))
  stopifnot(differentMax == 1)
#  cat("Starting between")
#  print(v1)
#  print(v2)
  n <- length(v1)		   
  present1 <- (v1 > 0)
  present2 <- (v2 > 0)
  presentBoth <- present1 & present2
  differentCase <- (v1 != v2)
  largeDifference <- abs(v1-v2) > 2
  largeHelixJumps <- largeDifference & presentBoth
  helixModification <- presentBoth & differentCase 
  nHelixModification <- sum(helixModification)
  nIncrease <- sum(v2 > v1)
  nDecrease <- sum(v1 > v2)
  shortened1 <- v1 > 1
  shortened2 <- v2 > 1
  even1 <- (v1 %% 2) == 0
  even2 <- (v2 %% 2) == 0
  shortenedLeft1 <- shortened1 & even1
  shortenedLeft2 <- shortened2 & even2
  shortenedRight1 <- shortened1 & (!even1)
  shortenedRight2 <- shortened2 & (!even2)
  hasSubstems <- (helixMaxIds > 1)
  isShortest1 <- present1 & ((v1 == helixMaxIds) | ((v1 == (helixMaxIds-1)) & hasSubstems))
  isShortest2 <- present2 & ((v2 == helixMaxIds) | ((v2 == (helixMaxIds-1)) & hasSubstems))
  isScndShortest1 <- present1 & ((v1 == (helixMaxIds-2)) | ((v1 == (helixMaxIds-3)) & hasSubstems))
  isScndShortest2 <- present2 & ((v2 == (helixMaxIds-2)) | ((v2 == (helixMaxIds-3)) & hasSubstems))
  # number of helices switching from left to right shortening  		
  #  do not allow cases where "left" shortened helix turns into "right" shortened helix
  nSwitch <- sum(shortenedLeft1 & shortenedRight2) + sum(shortenedLeft2 & shortenedRight1)
  helixFormation <- (!present1) & present2
  helixDissassociation <- (present1) & (!present2)
  nHelixFormation = sum(helixFormation)
  nHelixDissassociation <- sum(helixDissassociation)
  result <- "forbidden"
  if (connectivityChanged & (nHelixFormation > 0) ) {
#    cat("Connectivity changed!\n")
    if ((sum(differentCase) == 1) && (sum(helixFormation & (isScndShortest2))==1)) { # only allow from unfolded state to second-shortest helix for change of connectivity
        result <- "helix_formation"
    } else {
#      cat("Connectivity change but not direct connection between states");
#      print(v1)
#      print(v2)
    }
  } else {
#   cat("No connectivity change.\n")
   if ((sum(differentCase) <= differentMax) && (sum(largeHelixJumps) == 0) && (nSwitch == 0)) {
#   cat("No connectivity change after sanity check.\n")
   if (nHelixModification == 0) {
     if ((nHelixFormation == 1) && (nHelixDissassociation == 0)) {
       if (sum(helixFormation & isShortest2)==1) { # only allow from unfolded state to shortest helix
        result <- "helix_formation"
       } else {
        if (debugInfo) {
	        attr(result, "why_not") <- "non-shortest association"
	}
       }
     } else if ((nHelixFormation == 0) && (nHelixDissassociation == 1)) {
       if (sum(helixDissassociation & isShortest1) == 1) { # only allow from unfolded state to shortest helix
        result <- "helix_dissassociation"
       } else if (debugInfo) {
        attr(result, "why_not") <- "non-shortest dissociation"
       }
     }
   } else if (nHelixModification == 1) { # shortening or lengthening of one helix
     if ((nHelixFormation == 0) && (nHelixDissassociation == 0)) {
       result <- "helix_modification"
     } else if (debugInfo) {
       attr(result, "why_not") <- "strange helix_modification"
     }
   } else if (nHelixModification >= 2) {
     if ((nHelixFormation == 0) && (nHelixDissassociation == 0)) { #  && (nIncrease == 1) && (nDecrease==1)) {
      result <- "branch_migration"
     } else if (debugInfo) {
       attr(result, "why_not") <- "strange branch migration"
     }
   } else if (identical(v1, v2)) {
     result <- "no_change"
   }
  } else {
 #    cat("No state change - sanity check not passed:", sum(differentCase), sum(largeHelixJumps), nSwitch, (sum(differentCase) <= differentMax),  (sum(largeHelixJumps) == 0), (nSwitch == 0), "\n")
  }
 }

 helixMaxIds: it2.getBases() : non-inclusive maximum id at each position. -2: number of shortened substems; -1: number of shortened substems including unshortened stem

  */
  static string kineticStateChange(const state_t& v1, const state_t& v2, const Vec<unsigned int>& helixMaxIds, bool debugInfo = false) {
    // determine whether a change from one state to another is legitimate
    //  only allowed cases are where a new helix is formed, a helix is dissolved, or two helices have branch migration
    // KineticStateChange <- function(v1, v2, helixMaxIds, differentMax=1, debugInfo=FALSE, connectivityChanged=FALSE) {
THROWIFNOT(v1.size() == v2.size(),"Assertion violation in L132");
THROWIFNOT(v1.size() == helixMaxIds.size(),"Assertion violation in L133");
    size_t differentMax = 4;
    size_t n = v1.size();
    auto present1 = v1;
    auto present2 = v2;
    for (size_t i = 0; i < n; ++i) {
      present1[i] = ((v1[i] > 0) ? 1 : 0);
      present2[i] = ((v2[i] > 0) ? 1 : 0);
    }
    auto presentBoth = v1;
    auto differentCase = v1;
    auto largeDifference = v1;
    auto largeHelixJumps = v1;
    auto helixModification = v1;
    auto even1 = v1;
    auto even2 = v2;
    auto shortened1 = v1; // > 1;
    auto shortened2 = v2; // > 1;
    auto shortenedLeft1 = v1; // > 1;
    auto shortenedLeft2 = v2; // > 1;
    auto shortenedRight1 = v1; // > 1;
    auto shortenedRight2 = v2; // > 1;
    auto hasSubstems = v1;
    auto isShortest1 = v1;
    auto isShortest2 = v1;
    auto helixFormation = v1;
    auto helixDissassociation = v1;
    auto shortestHelixDissociation = v1;
    auto shortestHelixFormation = v1;
    auto shortestHelixDissociationViolation = v1;
    auto shortestHelixFormationViolation = v1;
    auto switchv = v1;
    for (size_t i = 0; i < n; ++i) {
      hasSubstems[i] = (helixMaxIds[i] > 2) ? 1 : 0; // was: > 1
      presentBoth[i] = ((v1[i] > 0) && (v2[i] > 0)) ? 1 : 0;
      differentCase[i] = (v1[i] != v2[i]) ? 1 : 0;
      largeDifference[i] = (abs(static_cast<int>(v1[i])-static_cast<int>(v2[i])) > 2) ? 1 : 0;
      largeHelixJumps[i] = ((largeDifference[i] && presentBoth[i]) || (hasSubstems[i] && ((v1[i] == 0 && v2[i] == 1) || (v1[i]==1 && v2[i] == 0)))) ? 1 : 0;
      helixModification[i] = (presentBoth[i] && differentCase[i]) ? 1 : 0;
      even1[i] = ((v1[i] % 2) == 0) ? 1 : 0;
      even2[i] = ((v2[i] % 2) == 0) ? 1 : 0;
      shortened1[i] = (v1[i] > 1) ? 1 : 0;
      shortened2[i] = (v2[i] > 1) ? 1 : 0;
      shortenedLeft1[i] = shortened1[i] && even1[i] ? 1 : 0;
      shortenedLeft2[i] = shortened2[i] && even2[i] ? 1 : 0;
      shortenedRight1[i] = shortened1[i] && (!even1[i]) ? 1 : 0;
      shortenedRight2[i] = shortened2[i] && (!even2[i]) ? 1 : 0;
      isShortest1[i] = ((present1[i]>0) && ((v1[i] == (helixMaxIds[i]-1)) || ((v1[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0;
      isShortest2[i] = ((present2[i]>0) && ((v2[i] == (helixMaxIds[i]-1)) || ((v2[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0;
      //      isScndShortest1[i] = present1[i] && ((v1[i] == (helixMaxIds[i]-2)) | ((v1 == (helixMaxIds[i]-3)) && hasSubstems[i]));
      //  isScndShortest2[i] = present2[i] && ((v2[i] == (helixMaxIds[i]-2)) | ((v2 == (helixMaxIds[i]-3)) && hasSubstems[i]));
      
      helixFormation[i] = (!present1[i]) && present2[i];
      helixDissassociation[i] = present1[i] && (!present2[i]);
      switchv[i] =  (shortenedLeft1[i] && shortenedRight2[i]) ? 1 : 0;
      switchv[i] += (shortenedLeft2[i] && shortenedRight1[i]) ? 1 : 0;

      shortestHelixFormation[i] = (helixFormation[i] && isShortest2[i]) ? 1 : 0;
      shortestHelixFormationViolation[i] = (helixFormation[i] && (!isShortest2[i])) ? 1 : 0;
      shortestHelixDissociation[i] = (helixDissassociation[i] && isShortest1[i]) ? 1 : 0;
      shortestHelixDissociationViolation[i] = (helixDissassociation[i] && (!isShortest1[i])) ? 1 : 0;
    }
    auto nHelixModification = elementSum(helixModification);
    // nIncrease = elementSum(v2 > v1);
    // nDecrease = elementSum(v1 > v2);
    // shortenedRight1 = shortened1 & (!even1);
    // shortenedRight2 = shortened2 & (!even2);

    //# number of helices switching from left to right shortening  		
    // #  do not allow cases where "left" shortened helix turns into "right" shortened helix
    auto nSwitch = elementSum(switchv); // shortenedLeft1 & shortenedRight2) + elementSum(shortenedLeft2 & shortenedRight1);
    auto nHelixFormation = elementSum(helixFormation);
    auto nHelixDissassociation = elementSum(helixDissassociation);
    string result = "forbidden";
    /* if (connectivityChanged & (nHelixFormation > 0) ) { */
    /*   //    cat("Connectivity changed!\n") */
    /*   if ((elementSum(differentCase) == 1) && (elementSum(helixFormation & (isScndShortest2))==1)) { // only allow from unfolded state to second-shortest helix for change of connectivity */
    /* 	result = "helix_formation"; */
    /*   } else { */
    /*   } */
    /* } else { */
    // if (true) {
      //   cat("No connectivity change.\n")
    if (debugInfo) {
      Rcpp::Rcout << "Kinetic state change from " << v1 << " to " << v2 << ":";
      Rcpp::Rcout << elementSum(differentCase) << " " << elementSum(largeHelixJumps) << " " << nSwitch << endl;
    }
    if ((elementSum(differentCase) <= differentMax) && (elementSum(largeHelixJumps) == 0) && (nSwitch == 0) && (elementSum(shortestHelixFormationViolation) == 0) && (elementSum(shortestHelixDissociationViolation)==0)) {
      //   cat("No connectivity change after sanity check.\n")
      if (debugInfo) {
	Rcpp::Rcout << "basic check ok" << endl;
      }
      if (nHelixModification == 0) {
	if ((nHelixFormation == 1) && (nHelixDissassociation == 0)) {
	  if (elementSum(shortestHelixFormation)==1) { // only allow from unfolded state to shortest helix
	    result = "helix_formation";
	  } else {
	    if (debugInfo) {
	      Rcpp::Rcout << "non-shortest association" << endl;
	    }
	  }
	} else if ((nHelixFormation == 0) && (nHelixDissassociation == 1)) {
	  if (elementSum(shortestHelixDissociation) == 1) { // only allow from unfolded state to shortest helix
	    result = "helix_dissassociation";
	  } else if (debugInfo) {
	    Rcpp::Rcout << "non-shortest dissociation" << endl;
	  }
	}
      } else if (nHelixModification == 1) { // shortening or lengthening of one helix
	if ((nHelixFormation == 0) && (nHelixDissassociation == 0)) {
	  result = "helix_modification";
	} else if (debugInfo) {
	  Rcpp::Rcout << "strange helix_modification" << endl;
	}
      } else if (nHelixModification >= 2) { 
	// if ((nHelixFormation == 0) && (nHelixDissassociation == 0)) { #  && (nIncrease == 1) && (nDecrease==1)) {
	result = "branch_migration";
	// } else if (debugInfo) {
	  // attr(result, "why_not") = "strange branch migration" */
	// }
      } else if (v1 == v2) { 
	result = "no_change"; 
	DERROR("Internal error in kineticStateChange: L230");
      } else { // should never be here:
	DERROR("Internal error in kineticStateChange: L242");
      }
    } else {
      if (debugInfo) {
	Rcpp::Rcout << "Basic properties not fullfilled." << endl;
      }
    }
      // }
    return result;
  }

  /** Like kineticStateChange, but considering jumps between whole helices. */
  static string kineticWholeHelixStateChange(const state_t& v1, const state_t& v2, const Vec<unsigned int>& helixMaxIds, bool debugInfo = false) {
    // determine whether a change from one state to another is legitimate
    //  only allowed cases are where a new helix is formed, a helix is dissolved, or two helices have branch migration
    // KineticStateChange <- function(v1, v2, helixMaxIds, differentMax=1, debugInfo=FALSE, connectivityChanged=FALSE) {
    // Rcpp::Rcout << "# Starting kineticWholeHelixStateChange" << endl;
THROWIFNOT(v1.size() == v2.size(),"Assertion violation in L274");
THROWIFNOT(v1.size() == helixMaxIds.size(),"Assertion violation in L275");
    size_t differentMax = 4;
    size_t n = v1.size();
    auto present1 = v1;
    auto present2 = v2;
    for (size_t i = 0; i < n; ++i) {
      present1[i] = ((v1[i] > 0) ? 1 : 0);
      present2[i] = ((v2[i] > 0) ? 1 : 0);
    }
    auto presentBoth = v1;
    auto differentCase = v1;
/*     auto largeDifference = v1; */
/*     auto largeHelixJumps = v1; */
/*     auto helixModification = v1; */
/*     auto even1 = v1; */
/*     auto even2 = v2; */
/*     auto shortened1 = v1; // > 1; */
/*     auto shortened2 = v2; // > 1; */
/*     auto shortenedLeft1 = v1; // > 1; */
/*     auto shortenedLeft2 = v2; // > 1; */
/*     auto shortenedRight1 = v1; // > 1; */
/*     auto shortenedRight2 = v2; // > 1; */
/*     auto hasSubstems = v1; */
/*     auto isShortest1 = v1; */
/*     auto isShortest2 = v1; */
    auto helixFormation = v1;
    auto helixDissassociation = v1;
/*     auto shortestHelixDissociation = v1; */
/*     auto shortestHelixFormation = v1; */
/*     auto shortestHelixDissociationViolation = v1; */
/*     auto shortestHelixFormationViolation = v1; */
/*     auto switchv = v1; */
    for (size_t i = 0; i < n; ++i) {
      // hasSubstems[i] = (helixMaxIds[i] > 2) ? 1 : 0; // was: > 1
      // presentBoth[i] = ((v1[i] > 0) && (v2[i] > 0)) ? 1 : 0;
      differentCase[i] = (v1[i] != v2[i]) ? 1 : 0;
      /*       largeDifference[i] = (abs(static_cast<int>(v1[i])-static_cast<int>(v2[i])) > 2) ? 1 : 0; */
/*       largeHelixJumps[i] = ((largeDifference[i] && presentBoth[i]) || (hasSubstems[i] && ((v1[i] == 0 && v2[i] == 1) || (v1[i]==1 && v2[i] == 0)))) ? 1 : 0; */
/*       helixModification[i] = (presentBoth[i] && differentCase[i]) ? 1 : 0; */
/*       even1[i] = ((v1[i] % 2) == 0) ? 1 : 0; */
/*       even2[i] = ((v2[i] % 2) == 0) ? 1 : 0; */
/*       shortened1[i] = (v1[i] > 1) ? 1 : 0; */
/*       shortened2[i] = (v2[i] > 1) ? 1 : 0; */
/*       shortenedLeft1[i] = shortened1[i] && even1[i] ? 1 : 0; */
/*       shortenedLeft2[i] = shortened2[i] && even2[i] ? 1 : 0; */
/*       shortenedRight1[i] = shortened1[i] && (!even1[i]) ? 1 : 0; */
/*       shortenedRight2[i] = shortened2[i] && (!even2[i]) ? 1 : 0; */
/*       isShortest1[i] = ((present1[i]>0) && ((v1[i] == (helixMaxIds[i]-1)) || ((v1[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0; */
/*       isShortest2[i] = ((present2[i]>0) && ((v2[i] == (helixMaxIds[i]-1)) || ((v2[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0; */
/*       //      isScndShortest1[i] = present1[i] && ((v1[i] == (helixMaxIds[i]-2)) | ((v1 == (helixMaxIds[i]-3)) && hasSubstems[i])); */
/*       //  isScndShortest2[i] = present2[i] && ((v2[i] == (helixMaxIds[i]-2)) | ((v2 == (helixMaxIds[i]-3)) && hasSubstems[i])); */
      
      helixFormation[i] = (!present1[i]) && present2[i];
      helixDissassociation[i] = present1[i] && (!present2[i]); 
/*       switchv[i] =  (shortenedLeft1[i] && shortenedRight2[i]) ? 1 : 0; */
/*       switchv[i] += (shortenedLeft2[i] && shortenedRight1[i]) ? 1 : 0; */
/*       shortestHelixFormation[i] = (helixFormation[i] && isShortest2[i]) ? 1 : 0; */
/*       shortestHelixFormationViolation[i] = (helixFormation[i] && (!isShortest2[i])) ? 1 : 0; */
/*       shortestHelixDissociation[i] = (helixDissassociation[i] && isShortest1[i]) ? 1 : 0; */
/*       shortestHelixDissociationViolation[i] = (helixDissassociation[i] && (!isShortest1[i])) ? 1 : 0; */
    }
    // auto nHelixModification = elementSum(helixModification);
    // nIncrease = elementSum(v2 > v1);
    // nDecrease = elementSum(v1 > v2);
    // shortenedRight1 = shortened1 & (!even1);
    // shortenedRight2 = shortened2 & (!even2);

    //# number of helices switching from left to right shortening  		
    // #  do not allow cases where "left" shortened helix turns into "right" shortened helix
    // auto nSwitch = elementSum(switchv); // shortenedLeft1 & shortenedRight2) + elementSum(shortenedLeft2 & shortenedRight1);
    auto nHelixFormation = elementSum(helixFormation);
    auto nHelixDissassociation = elementSum(helixDissassociation);
    string result = "forbidden";
    /* if (connectivityChanged & (nHelixFormation > 0) ) { */
    /*   //    cat("Connectivity changed!\n") */
    /*   if ((elementSum(differentCase) == 1) && (elementSum(helixFormation & (isScndShortest2))==1)) { // only allow from unfolded state to second-shortest helix for change of connectivity */
    /* 	result = "helix_formation"; */
    /*   } else { */
    /*   } */
    /* } else { */
    // if (true) {
      //   cat("No connectivity change.\n")
    if (debugInfo) {
      Rcpp::Rcout << "Kinetic state change from " << v1 << " to " << v2 << ":";
      Rcpp::Rcout << elementSum(differentCase) << endl;
    }
    if (elementSum(differentCase) <= differentMax ) {
      //   cat("No connectivity change after sanity check.\n")
      if ((nHelixFormation <= 2) && (nHelixDissassociation <= 2)) {
	result = "helix_modification";
      } 
    }
    // Rcpp::Rcout << "# Finished kineticWholeHelixStateChange" << endl;
    return result;
  }


  /** Like kineticStateChange, but considering jumps between whole helices. */
  static string kineticHalfHelixStateChange(const state_t& v1, const state_t& v2, const Vec<unsigned int>& helixMaxIds, bool debugInfo = false) {
    // determine whether a change from one state to another is legitimate
    //  only allowed cases are where a new helix is formed, a helix is dissolved, or two helices have branch migration
    // KineticStateChange <- function(v1, v2, helixMaxIds, differentMax=1, debugInfo=FALSE, connectivityChanged=FALSE) {
    // Rcpp::Rcout << "# Starting kineticWholeHelixStateChange" << endl;
THROWIFNOT(v1.size() == v2.size(),"Assertion violation in L378");
THROWIFNOT(v1.size() == helixMaxIds.size(),"Assertion violation in L379");
    size_t differentMax = 4;
    size_t n = v1.size();
    auto present1 = v1;
    auto present2 = v2;
    for (size_t i = 0; i < n; ++i) {
      present1[i] = ((v1[i] > 0) ? 1 : 0);
      present2[i] = ((v2[i] > 0) ? 1 : 0);
    }
    auto presentBoth = v1;
    auto differentCase = v1;
/*     auto largeDifference = v1; */
/*     auto largeHelixJumps = v1; */
/*     auto helixModification = v1; */
/*     auto even1 = v1; */
/*     auto even2 = v2; */
/*     auto shortened1 = v1; // > 1; */
/*     auto shortened2 = v2; // > 1; */
/*     auto shortenedLeft1 = v1; // > 1; */
/*     auto shortenedLeft2 = v2; // > 1; */
/*     auto shortenedRight1 = v1; // > 1; */
/*     auto shortenedRight2 = v2; // > 1; */
/*     auto hasSubstems = v1; */
/*     auto isShortest1 = v1; */
/*     auto isShortest2 = v1; */
    auto helixFormation = v1;
    auto helixDissassociation = v1;
/*     auto shortestHelixDissociation = v1; */
/*     auto shortestHelixFormation = v1; */
/*     auto shortestHelixDissociationViolation = v1; */
/*     auto shortestHelixFormationViolation = v1; */
/*     auto switchv = v1; */
    string result = "forbidden";
    for (size_t i = 0; i < n; ++i) {
      // hasSubstems[i] = (helixMaxIds[i] > 2) ? 1 : 0; // was: > 1
      // presentBoth[i] = ((v1[i] > 0) && (v2[i] > 0)) ? 1 : 0;
      differentCase[i] = (v1[i] != v2[i]) ? 1 : 0;
      if (differentCase[i]) {
	if ( (helixMaxIds[i] > 2)
	     && ( ( (v1[i] == 0) && (v2[i] == 1) )
		  || ( (v1[i] == 1) && (v2[i] == 0) ) ) ) {
	  if (debugInfo) {
	    Rcpp::Rcout << "# kineticHalfHelixState: Bad jump: " << v1 << v2;
	  }
	  return result; // not allowed to jump from empty helix to full helix - bust be half state inbetween
	}	  
	if ((v1[i] > 1) && (v2[i] > 1) && ((v1[i]%2) != (v2[i]%2))) { // no change from helix that is shortened on one side to helix that is shortened from other side
	  if (debugInfo) {
	    Rcpp::Rcout << "# kineticHalfHelixState: Bad switch: " << v1 << v2;
	  }
	  return result; // cannot switch between one-half-helix to a different half-helix
	}
      }
      /*       largeDifference[i] = (abs(static_cast<int>(v1[i])-static_cast<int>(v2[i])) > 2) ? 1 : 0; */
      /*       largeHelixJumps[i] = ((largeDifference[i] && presentBoth[i]) || (hasSubstems[i] && ((v1[i] == 0 && v2[i] == 1) || (v1[i]==1 && v2[i] == 0)))) ? 1 : 0; */
      /*       helixModification[i] = (presentBoth[i] && differentCase[i]) ? 1 : 0; */
      /*       even1[i] = ((v1[i] % 2) == 0) ? 1 : 0; */
      /*       even2[i] = ((v2[i] % 2) == 0) ? 1 : 0; */
      /*       shortened1[i] = (v1[i] > 1) ? 1 : 0; */
      /*       shortened2[i] = (v2[i] > 1) ? 1 : 0; */
      /*       shortenedLeft1[i] = shortened1[i] && even1[i] ? 1 : 0; */
      /*       shortenedLeft2[i] = shortened2[i] && even2[i] ? 1 : 0; */
      /*       shortenedRight1[i] = shortened1[i] && (!even1[i]) ? 1 : 0; */
      /*       shortenedRight2[i] = shortened2[i] && (!even2[i]) ? 1 : 0; */
      /*       isShortest1[i] = ((present1[i]>0) && ((v1[i] == (helixMaxIds[i]-1)) || ((v1[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0; */
      /*       isShortest2[i] = ((present2[i]>0) && ((v2[i] == (helixMaxIds[i]-1)) || ((v2[i] == (helixMaxIds[i]-2)) && hasSubstems[i])) ) ? 1 : 0; */
      /*       //      isScndShortest1[i] = present1[i] && ((v1[i] == (helixMaxIds[i]-2)) | ((v1 == (helixMaxIds[i]-3)) && hasSubstems[i])); */
      /*       //  isScndShortest2[i] = present2[i] && ((v2[i] == (helixMaxIds[i]-2)) | ((v2 == (helixMaxIds[i]-3)) && hasSubstems[i])); */      
      helixFormation[i] = (!present1[i]) && present2[i];
      helixDissassociation[i] = present1[i] && (!present2[i]); 
      /*       switchv[i] =  (shortenedLeft1[i] && shortenedRight2[i]) ? 1 : 0; */
      /*       switchv[i] += (shortenedLeft2[i] && shortenedRight1[i]) ? 1 : 0; */
      /*       shortestHelixFormation[i] = (helixFormation[i] && isShortest2[i]) ? 1 : 0; */
      /*       shortestHelixFormationViolation[i] = (helixFormation[i] && (!isShortest2[i])) ? 1 : 0; */
      /*       shortestHelixDissociation[i] = (helixDissassociation[i] && isShortest1[i]) ? 1 : 0; */
      /*       shortestHelixDissociationViolation[i] = (helixDissassociation[i] && (!isShortest1[i])) ? 1 : 0; */
    }
    // auto nHelixModification = elementSum(helixModification);
    // nIncrease = elementSum(v2 > v1);
    // nDecrease = elementSum(v1 > v2);
    // shortenedRight1 = shortened1 & (!even1);
    // shortenedRight2 = shortened2 & (!even2);

    //# number of helices switching from left to right shortening  		
    // #  do not allow cases where "left" shortened helix turns into "right" shortened helix
    // auto nSwitch = elementSum(switchv); // shortenedLeft1 & shortenedRight2) + elementSum(shortenedLeft2 & shortenedRight1);
    auto nHelixFormation = elementSum(helixFormation);
    auto nHelixDissassociation = elementSum(helixDissassociation);
    /* if (connectivityChanged & (nHelixFormation > 0) ) { */
    /*   //    cat("Connectivity changed!\n") */
    /*   if ((elementSum(differentCase) == 1) && (elementSum(helixFormation & (isScndShortest2))==1)) { // only allow from unfolded state to second-shortest helix for change of connectivity */
    /* 	result = "helix_formation"; */
    /*   } else { */
    /*   } */
    /* } else { */
    // if (true) {
      //   cat("No connectivity change.\n")
    if (debugInfo) {
      Rcpp::Rcout << "# Kinetic state change from " << v1 << " to " << v2 << ":";
      Rcpp::Rcout << elementSum(differentCase) << endl;
    }
    if ((elementSum(differentCase) <= differentMax ) 
	&& (nHelixFormation + nHelixDissassociation <= 1)) {
      // && (nHelixFormation <= 2) && (nHelixDissassociation <= 2)) {
      if (debugInfo) {
	Rcpp::Rcout << "# kineticHalfHelixState: change counts OK" << endl;
      }
      result = "allowed";
    } 
    // Rcpp::Rcout << "# Finished kineticWholeHelixStateChange" << endl;
    return result;
  }

  /** Compute a^b */
  static size_t computeSizePow(size_t a, size_t b) {
    size_t result = 1;
    for (size_t i = 0; i < b; ++i) {
      result *= a;
    }
    return result;
  }

  static size_t computePowerSum(const state_t& state) {
    ERROR_IF(state.size() >= 63, "Too large state vector.");
    size_t id = 0;
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
	id += computeSizePow(2U, i);
	// id += computeSizePow(i, 2U);
      }
    }
    return id;
  }

  /** Convert state vector to hash string: values greater zero correspond to character '1', values equal zero correspond to character '0' */
  static string computeStateHashString(const state_t& state) {
    string id(state.size(), '0');
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
	id[i] = '1';
      }
    }
    return id;
  }

  static size_t computeMultiPowerSum(const state_t& state, const Vec<unsigned int>& helixMaxIds) {
    size_t id = 0;
    size_t prod = 1;
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
	id += state[i] * prod;
      }
      prod *= helixMaxIds[i];
    }
    return id;
  }

  static bool statesUsingSameHelices(const state_t& state1, const state_t& state2) {
    PRECOND(state1.size() == state2.size());
    bool present1, present2;
    for (size_t i = 0; i < state1.size(); ++i) {
      present1 = (state1[i] > 0);
      present2 = (state2[i] > 0);
      if (present1 != present2) {
	return false;
      }
    }
    return true;
  }

  static void findSameHelixStates(const Vec<state_t>& states, const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n) {
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }
    // Rcpp::Rcout << "# Occupancy matrix:" << endl;
    // writeMatrix(Rcpp::Rcout, occMatrix);
    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (helicesOK) {
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);
      }
    }
  }

  /** Count number of base pairs of a state. */
  static size_t countBasePairs(const state_t& state, const Vec<Vec<Stem> >& subStems) {
    size_t result = 0;
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
	const Stem& stem = subStems[i][state[i]-1];
	result += stem.getLength();
      }
    }
    return result;
  }

  /** Count number of helies of a state. */
  static size_t countHelices(const state_t& state) {
    size_t result = 0;
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
	++result;
      }
    }
    return result;
  }
  
  /** Find state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states */
  static void findHighBPSameHelixStates(const Vec<state_t>& states, const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n,
					size_t basepairSlop, size_t expandedHelicesMax, size_t helixSlop) {
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    size_t bpCountMin = 0;
    size_t helixCountMax = 0;
    size_t helixCountMin = 0;
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      size_t bps = countBasePairs(states[id], subStems);
      if ((i == 0) || (bps < bpCountMin)) {
	bpCountMin = bps; // find minimal number of base pairs in state
      }
      size_t helixCount = countHelices(states[id]);
      if (helixCount > helixCountMax) {
	helixCountMax = helixCount;
      }
      if ((i == 0) || (helixCount < helixCountMin)) {
	helixCountMin = helixCount;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }
    // Rcpp::Rcout << "# Occupancy matrix:" << endl;
    // writeMatrix(Rcpp::Rcout, occMatrix);
    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
      size_t bpCount = countBasePairs(states[i], subStems);
      if ((bpCount + basepairSlop) < bpCountMin) {
	continue; // ignore: not enought base pairs in structure
      }
      size_t helixCount = countHelices(states[i]);
      if (helixCount > (helixCountMax + helixSlop)) {
	continue;
      }
      if ((helixCount+helixSlop) < helixCountMin) {
	continue;
      }
      size_t expandedHelixCount = 0;
      for (size_t j = 0; j < states[i].size(); ++j) {
	if (states[i][j] > 1) {
	  ++expandedHelixCount;
	}
      }
      if (expandedHelixCount > expandedHelicesMax) {
/* 	if (verbose > VERBOSE_INFO) { */
/* 	  Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits; */
/* 	} */
	continue;
      } 
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (helicesOK) { // all checks were successfullly passed
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);
      }
    }
  }

  /** Find strict version  state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states. All topologies of considered states have to be the ones from compatibleStateIds */
  static Vec<size_t> findStrictWholeHelixStates(const Vec<state_t>& states, const Vec<string>& connectivities, const Vec<double>& gv, 
						const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n,
						size_t basepairSlop, size_t expandedHelicesMax, size_t helixSlop, size_t bestMax=10) {
    DEBUG_MSG( "# Starting findStrictWholeHelixStates" );
    PRECOND(states.size() == connectivities.size());
    PRECOND(states.size() == gv.size());
    PRECOND(compatibleStateIds.size() == 2); // only start and end state
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    size_t bpCountMin = 0;
    size_t helixCountMax = 0;
    size_t helixCountMin = 0;
    set<string> allowedConnectivities;
    MultiIterator::container_t subStemCounts(subStems.size(), 1);
    for (size_t i = 0; i < subStems.size(); ++i) {
      subStemCounts[i] = subStems[i].size()+1; // plus one: for iterator: 0 stands for no stem, > 0 stands for stem id -1
    }
    // filtIds = StrenumTools::createPreGraphFromStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
    string startConnectivity = connectivities[compatibleStateIds[0]];
    string endConnectivity = connectivities[compatibleStateIds[1]];
    Vec<int> filtVec = StrenumTools::createPreGraphFromWholeHelixStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
#ifndef NDEBUG
    Rcpp::Rcout << "# Returned from createPreGraphFromWholeHelixStates with " << filtVec.size() << " states permitting " << elementSum(filtVec) << " states." << endl;
#endif
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      allowedConnectivities.insert(connectivities[id]);
      size_t bps = countBasePairs(states[id], subStems);
      if ((i == 0) || (bps < bpCountMin)) {
	bpCountMin = bps; // find minimal number of base pairs in state
      }
      size_t helixCount = countHelices(states[id]);
      if (helixCount > helixCountMax) {
	helixCountMax = helixCount;
      }
      if ((i == 0) || (helixCount < helixCountMin)) {
	helixCountMin = helixCount;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  ERROR_IF(states[id][k]-1 >= subStems[k].size(), "Internal error StrenumToolsh-589");
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
THROWIFNOT(pstart >= 0,"Assertion violation in L755");
THROWIFNOT(pstart < static_cast<int>(occMatrix.size()),"Assertion violation in L756");
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }
    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
THROWIFNOT(i < filtVec.size(),"Assertion violation in L764");
      if (filtVec[i] == 0) {
	continue; // not fond in pregraph analysis
      }
      // test not necessary:
/*       if (allowedConnectivities.find(connectivities[i]) == allowedConnectivities.end()) { */
/* 	continue; // connectivity is not found in start or end state */
/*       } */
      size_t bpCount = countBasePairs(states[i], subStems);
      if ((bpCount + basepairSlop) < bpCountMin) {
	filtVec[i] = 0;
	continue; // ignore: not enought base pairs in structure
      }
      size_t helixCount = countHelices(states[i]);
      if (helixCount > (helixCountMax + helixSlop)) {
	filtVec[i] = 0;
	continue;
      }
      if ((helixCount+helixSlop) < helixCountMin) {
	filtVec[i] = 0;
	continue;
      }
      size_t expandedHelixCount = 0;
      for (size_t j = 0; j < states[i].size(); ++j) {
	if (states[i][j] > 1) {
	  ++expandedHelixCount;
	}
      }
      if (expandedHelixCount > expandedHelicesMax) {
/* 	if (verbose > VERBOSE_INFO) { */
/* 	  Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits; */
/* 	} */
	ASSERT(false); // should have been caught before
	filtVec[i] = 0;
	continue;
      } 
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (!helicesOK) {
	filtVec[i] = 0;
	continue;
      }
    }// for (size_t i = 0; ...
    PurgePriorityQueue<RankedSolution3<size_t> > resultQueue;
    /*     Rcpp::Rcout << "# sizes: " << states.size() << " " << gv.size() << " " << connectivities.size() << endl; */
    /*     Rcpp::Rcout << "# start and end connectivity: " << startConnectivity << " " << endConnectivity << endl; */
    for (size_t i = 0; i < states.size(); ++i) {
      // Rcpp::Rcout << "# now " << (i+1) << " " << gv[i] << " " << connectivities[i] << endl;
      if (filtVec[i] > 0) {
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);
	if (connectivities[i] == endConnectivity) {
	  ERROR_IF(i >= gv.size(), "Internal error 565");
	  // Rcpp::Rcout << "# setting best transition state to " << (i+1) << endl;
	  resultQueue.push(RankedSolution3<size_t>(-gv[i], i)); // highest score (lowest energy) first
	}
      }
    }
    Vec<size_t> results;
    for (size_t i = 0; i < bestMax; i++) {
      if (resultQueue.size() == 0) {
	break;
      }
      results.push_back(resultQueue.top().second);
      resultQueue.pop();
    }
    Rcpp::Rcout << "# Finished findStrictWholeHelixStates: " << results;
    return results;
  }


  /** Find strict version  state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states. All topologies of considered states have to be the ones from compatibleStateIds */
  static Vec<size_t> findStrictHalfHelixStates(const Vec<state_t>& states, const Vec<string>& connectivities, const Vec<double>& gv, 
						const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n,
						size_t basepairSlop, size_t expandedHelicesMax, size_t helixSlop, size_t bestMax=10) {
    DEBUG_MSG( "# Starting findStrictHalfHelixStates" );
    PRECOND(states.size() == connectivities.size());
    PRECOND(states.size() == gv.size());
    PRECOND(compatibleStateIds.size() == 2); // only start and end state
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    size_t bpCountMin = 0;
    size_t helixCountMax = 0;
    size_t helixCountMin = 0;
    set<string> allowedConnectivities;
    MultiIterator::container_t subStemCounts(subStems.size(), 1);
    for (size_t i = 0; i < subStems.size(); ++i) {
      subStemCounts[i] = subStems[i].size()+1; // plus one: for iterator: 0 stands for no stem, > 0 stands for stem id -1
    }
    // filtIds = StrenumTools::createPreGraphFromStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
    string startConnectivity = connectivities[compatibleStateIds[0]];
    string endConnectivity = connectivities[compatibleStateIds[1]];
    Vec<int> filtVec = StrenumTools::createPreGraphFromHalfHelixStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
#ifndef NDEBUG
    Rcpp::Rcout << "# Returned from createPreGraphFromHalfHelixStates with " << filtVec.size() << " states permitting " << elementSum(filtVec) << " states: " << filtVec;
#endif
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      allowedConnectivities.insert(connectivities[id]);
      size_t bps = countBasePairs(states[id], subStems);
      if ((i == 0) || (bps < bpCountMin)) {
	bpCountMin = bps; // find minimal number of base pairs in state
      }
      size_t helixCount = countHelices(states[id]);
      if (helixCount > helixCountMax) {
	helixCountMax = helixCount;
      }
      if ((i == 0) || (helixCount < helixCountMin)) {
	helixCountMin = helixCount;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  ERROR_IF(states[id][k]-1 >= subStems[k].size(), "Internal error StrenumToolsh-589");
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
THROWIFNOT(pstart >= 0,"Assertion violation in L900");
THROWIFNOT(pstart < static_cast<int>(occMatrix.size()),"Assertion violation in L901");
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }
    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
      // Rcpp::Rcout << "# Checking if state " << (i+1) << " is feasible..." << endl;
THROWIFNOT(i < filtVec.size(),"Assertion violation in L910");
      if (filtVec[i] == 0) {
	// Rcpp::Rcout << "# State " << (i+1) << " not found in pre-graph analysis." << endl;
	continue; // not fond in pregraph analysis
      }
      // test not necessary:
/*       if (allowedConnectivities.find(connectivities[i]) == allowedConnectivities.end()) { */
/* 	continue; // connectivity is not found in start or end state */
/*       } */
      size_t bpCount = countBasePairs(states[i], subStems);
      if ((bpCount + basepairSlop) < bpCountMin) {
	// Rcpp::Rcout << "# Too few base pairs in state " << (i+1) << endl;
	filtVec[i] = 0;
	continue; // ignore: not enought base pairs in structure
      }
      size_t helixCount = countHelices(states[i]);
      if (helixCount > (helixCountMax + helixSlop)) {
	// Rcpp::Rcout << "# Too many helices in state " << (i+1) << endl;
	filtVec[i] = 0;
	continue;
      }
      if ((helixCount+helixSlop) < helixCountMin) {
	// Rcpp::Rcout << "# Too few helices in state " << (i+1) << endl;
	filtVec[i] = 0;
	continue;
      }
      size_t expandedHelixCount = 0;
      for (size_t j = 0; j < states[i].size(); ++j) {
	if (states[i][j] > 1) {
	  ++expandedHelixCount;
	}
      }
      if (expandedHelixCount > expandedHelicesMax) {
/* 	if (verbose > VERBOSE_INFO) { */
/* 	  Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits; */
/* 	} */
	ASSERT(false); // should have been caught before
	// Rcpp::Rcout << "# Too many partially folded helices in state " << (i+1) << endl;
	filtVec[i] = 0;
	continue;
      } 
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      // Rcpp::Rcout << "# State " << (i+1) << " contains base pairs that are neither in start nor end state." << endl;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (!helicesOK) {
	filtVec[i] = 0;
	continue;
      }
    }// for (size_t i = 0; ...
    // PurgePriorityQueue<RankedSolution3<size_t> > resultQueue;
    /*     Rcpp::Rcout << "# sizes: " << states.size() << " " << gv.size() << " " << connectivities.size() << endl; */
    /*     Rcpp::Rcout << "# start and end connectivity: " << startConnectivity << " " << endConnectivity << endl; */
    // Rcpp::Rcout << "# Filter vector: " << filtVec;
    // Rcpp::Rcout << "# Connectivities: " << connectivities;

    Vec<RankedSolution3<size_t> > results; // highest score (lowest energy) first
    for (size_t i = 0; i < states.size(); ++i) {
      // Rcpp::Rcout << "# now " << (i+1) << " " << gv[i] << " " << connectivities[i] << endl;
      if (filtVec[i] > 0) {
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);
	if (connectivities[i] == endConnectivity) {
	  ERROR_IF(i >= gv.size(), "Internal error 565");
	  // Rcpp::Rcout << "# setting best transition state to " << (i+1) << endl;
	  results.push_back(RankedSolution3<size_t>(gv[i], i)); // highest score (lowest energy) first
	}
      }
    }
    sort(results.begin(), results.end()); // lowest energy first
    ASSERT(results.size() < 2 || results[0].first <= results[1].first); // must be properly sorted
    Vec<size_t> finalResults;
/*     for (size_t i = 0; i < bestMax; i++) { */
/*       if (resultQueue.size() == 0) { */
/* 	break; */
/*       } */
/*       results.push_back(resultQueue.top().second); */
/*       resultQueue.pop(); */
/*     } */    for (size_t i = 0; (i < bestMax) && (i < results.size()); ++i) {
      finalResults.push_back(results[i].second);
    }
    DEBUG_MSG("# Finished findStrictHalfHelixStates: "); //  << results;
    return finalResults;
  }

  /** Find strict version  state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states. All topologies of considered states have to be the ones from compatibleStateIds */
  static Vec<pair<size_t, size_t> > findSloppyTransitions(const Vec<state_t>& states, const Vec<string>& connectivities, 
							  const string& startConnectivity, const string& endConnectivity,
							  size_t nmax=10) {
    DEBUG_MSG( "# Starting findStrictHighBPSameHelixStates" );
    PRECOND(states.size() == connectivities.size());
    PurgePriorityQueue<RankedSolution3<pair<size_t, size_t> > > queue;

    for (size_t i = 0; i < states.size(); ++i) {
      for (size_t j = i+1; j < states.size(); ++j) {
	if (connectivities[i] == startConnectivity && connectivities[j] == endConnectivity) {
	  double dist = euclidianDistanceSquare(states[i], states[j]);
	  queue.push(RankedSolution3<pair<size_t, size_t> >(-dist, make_pair(i,j)));
	} else if (connectivities[j] == startConnectivity && connectivities[i] == endConnectivity) {
	  double dist = euclidianDistanceSquare(states[j], states[i]);
	  queue.push(RankedSolution3<pair<size_t, size_t> >(-dist, make_pair(j,i)));
	}
      }
    }
    Vec<pair<size_t, size_t> > result;
    for (size_t i = 0; i < nmax; ++i) {
      if (queue.size() == 0) {
	break;
      }
      result.push_back(queue.top().second);
      queue.pop();
    }
    return result;
  }




  /** Find strict version  state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states. All topologies of considered states have to be the ones from compatibleStateIds */
  static Vec<size_t> findStrictHighBPSameHelixStates(const Vec<state_t>& states, const Vec<string>& connectivities, const Vec<double>& gv, 
					      const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n,
						     size_t basepairSlop, size_t expandedHelicesMax, size_t helixSlop, size_t bestMax=10) {
    DEBUG_MSG( "# Starting findStrictHighBPSameHelixStates" );
    PRECOND(states.size() == connectivities.size());
    PRECOND(states.size() == gv.size());
    PRECOND(compatibleStateIds.size() == 2); // only start and end state
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    size_t bpCountMin = 0;
    size_t helixCountMax = 0;
    size_t helixCountMin = 0;
    set<string> allowedConnectivities;
    MultiIterator::container_t subStemCounts(subStems.size(), 1);
    for (size_t i = 0; i < subStems.size(); ++i) {
      subStemCounts[i] = subStems[i].size()+1; // plus one: for iterator: 0 stands for no stem, > 0 stands for stem id -1
    }
    Rcpp::Rcout << "# Starting createPreGraphFromStates..." << endl;
    // filtIds = StrenumTools::createPreGraphFromStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
    string startConnectivity = connectivities[compatibleStateIds[0]];
    string endConnectivity = connectivities[compatibleStateIds[1]];
    Vec<int> filtVec = StrenumTools::createPreGraphFromStates2(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
    Rcpp::Rcout << "# Finished createPreGraphFromStates with " << filtVec.size() << " states permitting " << elementSum(filtVec) << " states." << endl;
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      allowedConnectivities.insert(connectivities[id]);
      size_t bps = countBasePairs(states[id], subStems);
      if ((i == 0) || (bps < bpCountMin)) {
	bpCountMin = bps; // find minimal number of base pairs in state
      }
      size_t helixCount = countHelices(states[id]);
      if (helixCount > helixCountMax) {
	helixCountMax = helixCount;
      }
      if ((i == 0) || (helixCount < helixCountMin)) {
	helixCountMin = helixCount;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }

    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
THROWIFNOT(i < filtVec.size(),"Assertion violation in L1099");
      if (filtVec[i] == 0) {
	continue; // not fond in pregraph analysis
      }
      // test not necessary:
/*       if (allowedConnectivities.find(connectivities[i]) == allowedConnectivities.end()) { */
/* 	continue; // connectivity is not found in start or end state */
/*       } */
      size_t bpCount = countBasePairs(states[i], subStems);
      if ((bpCount + basepairSlop) < bpCountMin) {
	filtVec[i] = 0;
	continue; // ignore: not enought base pairs in structure
      }
      size_t helixCount = countHelices(states[i]);
      if (helixCount > (helixCountMax + helixSlop)) {
	filtVec[i] = 0;
	continue;
      }
      if ((helixCount+helixSlop) < helixCountMin) {
	filtVec[i] = 0;
	continue;
      }
      size_t expandedHelixCount = 0;
      for (size_t j = 0; j < states[i].size(); ++j) {
	if (states[i][j] > 1) {
	  ++expandedHelixCount;
	}
      }
      if (expandedHelixCount > expandedHelicesMax) {
/* 	if (verbose > VERBOSE_INFO) { */
/* 	  Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits; */
/* 	} */
	ASSERT(false); // should have been caught before
	filtVec[i] = 0;
	continue;
      } 
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (!helicesOK) {
	filtVec[i] = 0;
	continue;
      }
    }// for (size_t i = 0; ...
    
    PurgePriorityQueue<RankedSolution3<size_t> > resultQueue;
    /*     Rcpp::Rcout << "# sizes: " << states.size() << " " << gv.size() << " " << connectivities.size() << endl; */
    /*     Rcpp::Rcout << "# start and end connectivity: " << startConnectivity << " " << endConnectivity << endl; */
    for (size_t i = 0; i < states.size(); ++i) {
      // Rcpp::Rcout << "# now " << (i+1) << " " << gv[i] << " " << connectivities[i] << endl;
      if (filtVec[i] > 0) {
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);
	if (connectivities[i] == endConnectivity) {
	  ERROR_IF(i >= gv.size(), "Internal error 565");
	  // Rcpp::Rcout << "# setting best transition state to " << (i+1) << endl;
	  resultQueue.push(RankedSolution3<size_t>(-gv[i], i)); // highest score (lowest energy) first
	}
      }
    }
    Vec<size_t> results;
    for (size_t i = 0; i < bestMax; i++) {
      if (resultQueue.size() == 0) {
	break;
      }
      results.push_back(resultQueue.top().second);
      resultQueue.pop();
    }
    return results;
  }
  
  /** Find strict version  state ids, that are such that each base pair is part of at least one structure defined by compatibleStateIds. This is useful for reducing the number of states that need to be considered for 
   * Finding a path between two states. All topologies of considered states have to be the ones from compatibleStateIds */
  static size_t findStrictHighBPSameHelixStates_old(const Vec<state_t>& states, const Vec<string>& connectivities, const Vec<double>& gv, 
					      const Vec<size_t>& compatibleStateIds, Vec<size_t>& newToOrigIds, Vec<size_t>& origToNewIds, const Vec<Vec<Stem> >& subStems, size_t n,
					      size_t basepairSlop, size_t expandedHelicesMax, size_t helixSlop, bool graphCheck) {
    Rcpp::Rcout << "# Starting findStrictHighBPSameHelixStates" << endl;
    PRECOND(states.size() == connectivities.size());
    PRECOND(states.size() == gv.size());
    PRECOND(compatibleStateIds.size() == 2); // only start and end state
    newToOrigIds.clear();
    origToNewIds = Vec<size_t>(states.size(), states.size()); // from original ids to new ids
    size_t m = states[0].size();
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    size_t bpCountMin = 0;
    size_t helixCountMax = 0;
    size_t helixCountMin = 0;
    set<string> allowedConnectivities;
    MultiIterator::container_t subStemCounts(subStems.size(), 1);
    for (size_t i = 0; i < subStems.size(); ++i) {
      subStemCounts[i] = subStems[i].size()+1; // plus one: for iterator: 0 stands for no stem, > 0 stands for stem id -1
    }
    Vec<int> filtVec(states.size(), 1);
    if (graphCheck) {
      Rcpp::Rcout << "# Starting createPreGraphFromStates..." << endl;
      // filtIds = StrenumTools::createPreGraphFromStates(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
      filtVec = StrenumTools::createPreGraphFromStates2(states, connectivities, compatibleStateIds[0], compatibleStateIds[1], subStemCounts);
      Rcpp::Rcout << "# Finished createPreGraphFromStates with " << filtVec.size() << " states permitting " << elementSum(filtVec) << " states." << endl;
    }
    int bestTransitionId = -1;
    string targetConnectivity = connectivities[compatibleStateIds[1]];
    for (size_t i = 0; i < compatibleStateIds.size(); ++i) {
      size_t id = compatibleStateIds[i];
      allowedConnectivities.insert(connectivities[id]);
      size_t bps = countBasePairs(states[id], subStems);
      if ((i == 0) || (bps < bpCountMin)) {
	bpCountMin = bps; // find minimal number of base pairs in state
      }
      size_t helixCount = countHelices(states[id]);
      if (helixCount > helixCountMax) {
	helixCountMax = helixCount;
      }
      if ((i == 0) || (helixCount < helixCountMin)) {
	helixCountMin = helixCount;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[id][k] > 0) {
	  const Stem& stem = subStems[k][states[id][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    occMatrix[pstart][pstop] = 1;
	    occMatrix[pstop][pstart] = 1;
	  }
	}
      }
    }
    for (size_t i = 0; i < states.size(); ++i) { // iterate over all states helices have to be compatible with
      // Rcpp::Rcout << "# Checking state " << (i+1) << endl;
THROWIFNOT(i < filtVec.size(),"Assertion violation in L1243");
      if (filtVec[i] == 0) {
	continue; // not fond in pregraph analysis
      }
      if (allowedConnectivities.find(connectivities[i]) == allowedConnectivities.end()) {
	continue; // connectivity is not found in start or end state
      }
      size_t bpCount = countBasePairs(states[i], subStems);
      if ((bpCount + basepairSlop) < bpCountMin) {
	continue; // ignore: not enought base pairs in structure
      }
      size_t helixCount = countHelices(states[i]);
      if (helixCount > (helixCountMax + helixSlop)) {
	continue;
      }
      if ((helixCount+helixSlop) < helixCountMin) {
	continue;
      }
      size_t expandedHelixCount = 0;
      for (size_t j = 0; j < states[i].size(); ++j) {
	if (states[i][j] > 1) {
	  ++expandedHelixCount;
	}
      }
      if (expandedHelixCount > expandedHelicesMax) {
/* 	if (verbose > VERBOSE_INFO) { */
/* 	  Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits; */
/* 	} */
	continue;
      } 
      // for each helix, find at least one state, which has the same or lower non-zero helix id
      bool helicesOK = true;
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] > 0) { // check placed helices: all its base pairs have to be present in at least one structure
	  const Stem& stem = subStems[k][states[i][k]-1];
	  for (int k2 = 0; k2 <= stem.getLength(); ++k2) {
	    int pstart = stem.getStart() + k2;
	    int pstop = stem.getStop() - k2;
	    if (occMatrix[pstart][pstop] != 1) {
	      helicesOK = false;
	      break;
	    }
	  }
	}
	if (!helicesOK) {
	  break;
	}
      }
      if (helicesOK) { // all checks were successfullly passed
THROWIFNOT(i < connectivities.size(),"Assertion violation in L1292");
	if (((bestTransitionId < 0) || (gv[bestTransitionId] > gv[i])) && (connectivities[i] == targetConnectivity)) {
	  bestTransitionId = i;
#ifndef NDEBUG
	  Rcpp::Rcout << "# Setting best transition id to " << (i+1) << endl;
#endif
	}
#ifndef NDEBUG
	Rcpp::Rcout << "bestTransitionId value: " << bestTransitionId << " from " << states.size() << " states." << endl;
	Rcpp::Rcout << "# Graphstate " << (i+1) << " :" << states[i];
#endif
	origToNewIds[i] = newToOrigIds.size();
	newToOrigIds.push_back(i);

	//	for (size_t k = 0; k < states[i].size(); ++k) {
	// Rcpp::Rcout << " " << states[i][k];
	// }
	// Rcpp::Rcout << " " << gv[i] << " " << connectivities[i] << endl;
      }
    }
    Rcpp::Rcout << "# Finished findStrictHighBPSameHelixStates" << endl;
    if (bestTransitionId < 0) {
      bestTransitionId = static_cast<int>(states.size());
    }
    return static_cast<size_t>(bestTransitionId);
  }

  // same as createGraphFromStates (Keep synchronized with that method!), but only returning target states that are at the boundary 
  static set<size_t> createPreGraphFromStates(const Vec<state_t>& states, const Vec<string>& connectivities, size_t startId, size_t endId, const Vec<unsigned int>& helixMaxIds) {
    bool debugInfo = false;
    size_t n = states.size();
    set<size_t> gset;
THROWIFNOT(states.size() > 0,"Assertion violation in L1324");
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<size_t, set<size_t> > setHash;
    Vec<size_t> powerSums(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    string startConnectivity = connectivities[startId];
    string endConnectivity = connectivities[endId];
    bool endConnectivityFound = false;
    for (size_t i = 0; i < n; ++i) {
      if (connectivities[i] == startConnectivity) { // all nodes from start-connectivity are considered
	gset.insert(i); // this node is OK
      }
      // long id = computeMultiPowerSum(states[i], helixMaxIds);
      long id = computePowerSum(states[i]);
      powerSums[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      if ((connectivities[i] != startConnectivity)
	  && (connectivities[i] != endConnectivity)) {
	continue;
      }
      size_t id = powerSums[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
THROWIFNOT(connectivities[i] == startConnectivity || connectivities[i] == endConnectivity,"Assertion violation in L1364");
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if ((connectivities[j] != startConnectivity)
	    && (connectivities[j] != endConnectivity)) {
	  continue;
	}
THROWIFNOT(connectivities[j] == startConnectivity || connectivities[j] == endConnectivity,"Assertion violation in L1371");
	// for (size_t j = i+1; j < n; ++j) {
	string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  if (connectivities[i] != connectivities[j]) { // only store end state if at boundary
	    gset.insert(i);
	    gset.insert(j);
	    endConnectivityFound = true;
	  }
	}
      }
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      if ((connectivities[i] != startConnectivity)
	  && (connectivities[i] != endConnectivity)) {
	continue;
      }
      for (size_t k = 0; k < m; ++k) { // loop over helices
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L1395");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exacltly same helices (longer or shorter versions of it) present
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if ((connectivities[j] != startConnectivity)
	      && (connectivities[j] != endConnectivity)) {
	    continue;
	  }
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  if (desc != "forbidden") {
	    if (connectivities[i] != connectivities[j]) { // store end state, because it is at the boundary
	      gset.insert(i);
	      gset.insert(j);
	      endConnectivityFound = true;
	    }
	  }
	}
      }
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      if ((connectivities[i] != startConnectivity)
	  && (connectivities[i] != endConnectivity)) {
	continue;
      }
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L1434");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exactly same helices (longer or shorter versions of it) present	 
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  size_t j = *jt;
	  if ((connectivities[j] != startConnectivity)
	      && (connectivities[j] != endConnectivity)) {
	    continue;
	  }
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  if (desc != "forbidden") {
	    if (connectivities[i] != connectivities[j]) {
	      gset.insert(i);
	      gset.insert(j);
	      endConnectivityFound = true;
	    }
	  }
	}
      }
    }
    if (!endConnectivityFound) {
      Rcpp::Rcout << "# Could not find path element to end state: removing all node ids." << endl;
      gset.clear();
    }
    return gset;
  }

  static string convertStateToHash(const state_t& state) {
    string result;
    size_t lastPos = state.size();
    for (int i = static_cast<int>(state.size())-1; i >= 0; --i) {
      if (state[i] > 0) {
	lastPos = i; // highest non
	break;
      }
    }
    if (lastPos >= state.size()) {
      return ""; // unfolded state
    }
    for (size_t i = 0; i <= lastPos; ++i) {
      if (i > 0) {
	result = result + "_";
      }
      result = result + uitos(state[i]);
    }
    // Rcpp::Rcout << "# Finished convertStateToHash with result " << result << " from state: " << state;
    return result;
  }

  /** State vector now only represents number of helices and number of base pairs */
  static string convertStateToPresentHash(const state_t& state) {
    string result;
    size_t lastPos = state.size();
    for (int i = static_cast<int>(state.size())-1; i >= 0; --i) {
      if (state[i] > 0) {
	lastPos = i; // highest non
	break;
      }
    }
    if (lastPos >= state.size()) {
      return ""; // unfolded state
    }
    for (size_t i = 0; i <= lastPos; ++i) {
      if (i > 0) {
	result = result + "_";
      }
      if (state[i] >= 1) {
	result = result + "1";
      } else {
	result = result + "0";
      }
    }
    // Rcpp::Rcout << "# Finished convertStateToHash with result " << result << " from state: " << state;
    return result;
  }

  static string convertStateToHalfHelixHash(const state_t& state) {
    string result;
    size_t lastPos = state.size();
    for (int i = static_cast<int>(state.size())-1; i >= 0; --i) {
      if (state[i] > 0) {
	lastPos = i; // highest non
	break;
      }
    }
    if (lastPos >= state.size()) {
      return ""; // unfolded state
    }
    for (size_t i = 0; i <= lastPos; ++i) {
      if (i > 0) {
	result = result + "_";
      }
      if (state[i] < 2) {
	result = result + uitos(state[i]);
      } else if (state[i] % 2 == 0) {
	result = result + "e"; // even shortened stems
      } else {
	result = result + "o"; // odd shortened stems
      }
    }
    // Rcpp::Rcout << "# Finished convertStateToHash with result " << result << " from state: " << state;
    return result;
  }


  static string convertStateToParentHalfHelixHash(const state_t& state) {
    string result;
    size_t lastPos = state.size();
    size_t counter = 0;
    for (int i = static_cast<int>(state.size())-1; i >= 0; --i) {
      if (state[i] > 0) {
	++counter;
	if (counter == 2) {
	  lastPos = static_cast<size_t>(i); // highest non
	  break;
	}
      }
    }
    if (lastPos == state.size()) {
      return ""; // unfolded state
    }
    for (size_t i = 0; i <= lastPos; ++i) {
      if (i > 0) {
	result = result + "_";
      }
      if (state[i] < 2) {
	result = result + uitos(state[i]);
      } else if (state[i] % 2 == 0) {
	result = result + "e"; // even shortened stems
      } else {
	result = result + "o"; // odd shortened stems
      }
    }
    return result;
  }


  static string convertStateToParentHash(const state_t& state) {
    string result;
    size_t lastPos = state.size();
    size_t counter = 0;
    for (int i = static_cast<int>(state.size())-1; i >= 0; --i) {
      if (state[i] > 0) {
	++counter;
	if (counter == 2) {
	  lastPos = static_cast<size_t>(i); // highest non
	  break;
	}
      }
    }
    if (lastPos == state.size()) {
      return ""; // unfolded state
    }
    for (size_t i = 0; i <= lastPos; ++i) {
      if (i > 0) {
	result = result + "_";
      }
      result = result + uitos(state[i]);
    }
    return result;
  }

  // same as createGraphFromStates (Keep synchronized with that method!), but only returning target states that are at the boundary 
  static Vec<int> createPreGraphFromStates2(const Vec<state_t>& states, const Vec<string>& connectivities, size_t startId, size_t endId, const Vec<unsigned int>& helixMaxIds) {
    string methodName = "createPreGraphFromStates2";
#ifndef NDEBUG
    Rcpp::Rcout << "# Starting " << methodName << endl;
#endif
    bool debugInfo = false;
    size_t n = states.size();
    Vec<int> gset(n, 0); // zero if state not include, one if included
THROWIFNOT(states.size() > 0,"Assertion violation in L1613");
    Vec<string> hashes(n);
    Vec<string> parentHashes(n);
    map<string, size_t> hashMap;
    multimap<string, size_t> childMap;
    string startConnectivity = connectivities[startId];
    string endConnectivity = connectivities[endId];
    bool endConnectivityFound = true;
    for (size_t i = 0; i < n; ++i) {
      hashes[i] = convertStateToHash(states[i]);
      parentHashes[i] = convertStateToParentHash(states[i]);
      hashMap.insert(make_pair(hashes[i], i));
      childMap.insert(make_pair(parentHashes[i], i));
      if (connectivities[i] == startConnectivity) {
	gset[i] = 1;
      }
      // Rcpp::Rcout << "Hash " << (i+1) << " " << hashes[i] << " " << parentHashes[i] << endl;
    }
    for (size_t i = 0; i < n; ++i) {
      bool found = false;
      if ((gset[i] == 0) && (connectivities[i] == endConnectivity)) {
	// each structure must have endConnectivity (startConnectivy was check earlier)
	// it is checked, if at least one adjacent structure exists, 
	// that has the startConnectivity. 
	// check parents, siblings and children!
	string hash = hashes[i];
	string parentHash = parentHashes[i];
	auto parentHashIt = hashMap.find(parentHash);
	if (parentHashIt != hashMap.end()) {
	  size_t parentId = parentHashIt->second; // hashMap[parentHash];
THROWIFNOT(parentId < n,"Assertion violation in L1643");
THROWIFNOT(parentId != i,"Assertion violation in L1644");
	  if ((gset[parentId] == 1) && (connectivities[parentId] == startConnectivity)) {
	    string desc = kineticStateChange(states[parentId], states[i], helixMaxIds, debugInfo);
	    // kmd[i][j] = desc;
	    // Rcpp::Rcout << "State change: " << desc << endl;
	    if (desc != "forbidden") {
	      gset[i] = 1;
	      found = true;
	      endConnectivityFound = true;
	    }
	  }
	  if (!found) { // check all siblings: children of parent
	    auto itUp = childMap.upper_bound(parentHash);
	    for (auto childIt = childMap.lower_bound(parentHash); childIt != itUp; childIt++) {
	      size_t sibId = childIt->second;
	      if ((sibId != i) && (gset[sibId] == 1) && (connectivities[sibId] == startConnectivity)) {
		string desc = kineticStateChange(states[sibId], states[i], helixMaxIds, debugInfo);
		// kmd[i][j] = desc;
		// Rcpp::Rcout << "State change: " << desc << endl;
		if (desc != "forbidden") {
		  gset[i] = 1;
		  found = true;
		  endConnectivityFound = true;
		  break; // no further search among siblings needed
		}
	      }
	    }
	  }
	} // if (parentHashIt != hashMap.end
	if (!found) { // now search among child nodes
	  auto itUp = childMap.upper_bound(hash);
	  for (auto childIt = childMap.lower_bound(hash); childIt != itUp; childIt++) {
	    size_t childId = childIt->second;
	    if ((childId != i) && (gset[childId] == 1) && (connectivities[childId] == startConnectivity)) {
	      string desc = kineticStateChange(states[childId], states[i], helixMaxIds, debugInfo);
	      // kmd[i][j] = desc;
	      // Rcpp::Rcout << "State change: " << desc << endl;
	      if (desc != "forbidden") {
		gset[i] = 1;
		found = true;
		endConnectivityFound = true;
		break; // no further search among child nodes needed
	      }
	    }
	  }
	}
      }
    }
    if (!endConnectivityFound) {
      Rcpp::Rcout << "# Could not find path element to end state: removing all node ids." << endl;
      for (size_t i = 0; i < gset.size(); ++i) {
	gset[i] = 0;
      }
    }
#ifndef NDEBUG
    Rcpp::Rcout << "# Finished " << methodName << endl;
#endif
    return gset;
  }

  // same as createGraphFromStates (Keep synchronized with that method!), but only returning target states that are at the boundary 
  static Vec<int> createPreGraphFromWholeHelixStates(const Vec<state_t>& states, const Vec<string>& connectivities, size_t startId, size_t endId, const Vec<unsigned int>& helixMaxIds) {
    DEBUG_MSG("# Starting createPregraphFromWholeHelixStates");
    string startConnectivity = connectivities[startId];
    string endConnectivity = connectivities[endId];
#ifndef NDEBUG
    Rcpp::Rcout << "# Start-id: " << (startId+1) << startConnectivity << " state: " << states[startId];
    Rcpp::Rcout << "# End-id:   " << (endId+1) << endConnectivity     << " state: " << states[endId];
#endif
    bool debugInfo = false;
    size_t n = states.size();
    Vec<int> gset(n, 0); // zero if state not include, one if included
THROWIFNOT(states.size() > 0,"Assertion violation in L1716");
    Vec<string> hashes(n);
    Vec<string> parentHashes(n);
    map<string, size_t> hashMap;
    multimap<string, size_t> childMap;
    bool endConnectivityFound = true;
    for (size_t i = 0; i < n; ++i) {
      hashes[i] = convertStateToHash(states[i]);
      auto parentHash = convertStateToParentHash(states[i]);
#ifndef NDEBUG
      Rcpp::Rcout << "# id: " << (i+1) << " hash: " << hashes[i] << " parent-hash: " << parentHash << " state: " << states[i];
#endif
      if (parentHash != hashes[i]) {
	parentHashes[i] = parentHash;
	childMap.insert(make_pair(parentHashes[i], i));
      } else {
	Rcpp::Rcout << "# Warning: parent hash is equal to hash : " << i << " " << hashes[i] << endl;
      }
      hashMap.insert(make_pair(hashes[i], i));
      if (connectivities[i] == startConnectivity) {
	gset[i] = 1;
      }
      // Rcpp::Rcout << "Hash " << (i+1) << " " << hashes[i] << " " << parentHashes[i] << endl;
    }
#ifndef NDEBUG
    Rcpp::Rcout << "# Initially allowed states: " << gset;
#endif
    for (size_t i = 0; i < n; ++i) {
      bool found = false;
      if ((gset[i] == 0) && (connectivities[i] == endConnectivity)) {
	// each structure must have endConnectivity (startConnectivy was check earlier)
	// it is checked, if at least one adjacent structure exists, 
	// that has the startConnectivity. 
	// check parents, siblings and children!
	string hash = hashes[i];
	string parentHash = parentHashes[i];
	auto parentHashIt = hashMap.find(parentHash);
	if ((hash != parentHash) && (parentHashIt != hashMap.end())) {
	  size_t parentId = parentHashIt->second; // hashMap[parentHash];
#ifndef NDEBUG
	  Rcpp::Rcout << "ID: " << i << " hash : " << hash << " parent-hash: " << parentHash << " parent-id: " << parentId << endl;
#endif
THROWIFNOT(parentId < n,"Assertion violation in L1758");
THROWIFNOT(parentId != i,"Assertion violation in L1759");
	  if ((gset[parentId] == 1) && (connectivities[parentId] == startConnectivity)) {
	    string desc = kineticWholeHelixStateChange(states[parentId], states[i], helixMaxIds, debugInfo);
	    // kmd[i][j] = desc;
	    // Rcpp::Rcout << "State change: " << desc << endl;
	    if (desc != "forbidden") {
	      gset[i] = 1;
	      found = true;
	      endConnectivityFound = true;
	    }
#ifndef NDEBUG
	    else {
	      Rcpp::Rcout << "# classified state change as " << desc << ": " << states[parentId] << " " << states[i];
	    }
#endif
	  }
	  if (!found) { // check all siblings: children of parent
	    auto itUp = childMap.upper_bound(parentHash);
	    for (auto childIt = childMap.lower_bound(parentHash); childIt != itUp; childIt++) {
	      size_t sibId = childIt->second;
	      if ((sibId != i) && (gset[sibId] == 1) && (connectivities[sibId] == startConnectivity)) {
		string desc = kineticWholeHelixStateChange(states[sibId], states[i], helixMaxIds, debugInfo);
		// kmd[i][j] = desc;
		// Rcpp::Rcout << "State change: " << desc << endl;
		if (desc != "forbidden") {
		  gset[i] = 1;
		  found = true;
		  endConnectivityFound = true;
		  break; // no further search among siblings needed
		}
#ifndef NDEBUG
		else {
		  Rcpp::Rcout << "# classified state change as " << desc << ": " << states[sibId] << " " << states[i];
		}
#endif
	      }
	    }
	  }
	} // if (parentHashIt != hashMap.end
	if (!found) { // now search among child nodes
	  auto itUp = childMap.upper_bound(hash);
	  for (auto childIt = childMap.lower_bound(hash); childIt != itUp; childIt++) {
	    size_t childId = childIt->second;
	    if ((childId != i) && (gset[childId] == 1) && (connectivities[childId] == startConnectivity)) {
	      string desc = kineticWholeHelixStateChange(states[childId], states[i], helixMaxIds, debugInfo);
	      // kmd[i][j] = desc;
	      // Rcpp::Rcout << "State change: " << desc << endl;
	      if (desc != "forbidden") {
		gset[i] = 1;
		found = true;
		endConnectivityFound = true;
		break; // no further search among child nodes needed
	      }
#ifndef NDEBUG
	    else {
	      Rcpp::Rcout << "# classified state change as " << desc << ": " << states[childId] << " " << states[i];
	    }
#endif

	    }
	  }
	}
      }
    }
    if (!endConnectivityFound) {
      Rcpp::Rcout << "# Could not find path element to end state: removing all node ids." << endl;
      for (size_t i = 0; i < gset.size(); ++i) {
	gset[i] = 0;
      }
    }
    DEBUG_MSG("# Finished createPregraphFromWholeHelixStates");
    return gset;
  }

  // same as createGraphFromStates (Keep synchronized with that method!), but only returning target states that are at the boundary 
  static Vec<int> createPreGraphFromHalfHelixStates(const Vec<state_t>& states, const Vec<string>& connectivities, size_t startId, size_t endId, const Vec<unsigned int>& helixMaxIds) {
    DEBUG_MSG("# Starting createPregraphFromHalfHelixStates");
    string startConnectivity = connectivities[startId];
    string endConnectivity = connectivities[endId];
#ifndef NDEBUG
    Rcpp::Rcout << "# Start-id: " << (startId+1) << startConnectivity << " state: " << states[startId];
    Rcpp::Rcout << "# End-id:   " << (endId+1) << endConnectivity     << " state: " << states[endId];
#endif
    bool debugInfo = false;
    size_t n = states.size();
    Vec<int> gset(n, 0); // zero if state not include, one if included
    set<size_t> endSet, gsets; // set of gset
THROWIFNOT(states.size() > 0,"Assertion violation in L1846");
    size_t m = states[0].size();
    Vec<string> hashes(n);
    // Vec<string> parentHashes(n);
    // map<string, size_t> hashMap;
    multimap<string, size_t> simMmap;
    bool endConnectivityFound = true;
    for (size_t i = 0; i < n; ++i) {
      hashes[i] = convertStateToPresentHash(states[i]);
      simMmap.insert(make_pair(hashes[i], i)); 
      for (size_t j = 0; j < m; ++j) { // one helix less
	if (states[i][j] > 0) {
	  state_t sv = states[i];
	  sv[j] = 0;
	  string hash = convertStateToPresentHash(sv);
	  simMmap.insert(make_pair(hash, i));
	}
      }
      for (size_t j = 0; j < m; ++j) { // one additional helix
	if (states[i][j] == 0) {
	  state_t sv = states[i];
	  sv[j] = 1;
	  string hash = convertStateToPresentHash(sv);
	  simMmap.insert(make_pair(hash, i));
	}
      }
      // auto parentHash = convertStateToParentHalfHelixHash(states[i]);
#ifndef NDEBUG
      // Rcpp::Rcout << "# Created hash! id: " << (i+1) << " hash: " << hashes[i] << " parent-hash: " << parentHash << " state: " << states[i];
#endif
/*       if (parentHash != hashes[i]) { */
/* 	parentHashes[i] = parentHash; */
/* 	childMap.insert(make_pair(parentHashes[i], i)); */
/*       } else { */
/* 	Rcpp::Rcout << "# Warning: parent hash is equal to hash : " << i << " " << hashes[i] << endl; */
/*       } */
/*       hashMap.insert(make_pair(hashes[i], i)); */
      if (connectivities[i] == startConnectivity) {
	gset[i] = 1;
	gsets.insert(i);
      } else if (connectivities[i] == endConnectivity) {
	endSet.insert(i);
      }
      // Rcpp::Rcout << "Hash " << (i+1) << " " << hashes[i] << " " << parentHashes[i] << endl;
    }
#ifndef NDEBUG
    Rcpp::Rcout << "# Initially allowed states: " << gset;
    Rcpp::Rcout << "# Loop over states to see whether they should be added to graph..." << endl;
#endif
    // for (size_t i = 0; i < n; ++i) {
    for (auto ip = endSet.begin(); ip != endSet.end(); ip++) {
      size_t i = *ip;
      string hash = hashes[i]; // convertStateToPresentHash(states[i]);
      // bool found = false;
      // Rcpp::Rcout << "# i: " << i << " gset[i]: " << gset[i] << " connectivities[i]: " << connectivities[i] << endl;
      //       if ((gset[i] == 0) && (connectivities[i] == endConnectivity)) {
THROWIFNOT(gset[i] == 0,"Assertion violation in L1902");
THROWIFNOT(connectivities[i] == endConnectivity,"Assertion violation in L1903");
      // for (auto it = gsets.begin(); it != gsets.end(); it++) { // loop over states with start connectivity
      for (auto it = simMmap.lower_bound(hash); it != simMmap.upper_bound(hash); it++) {
	size_t j = it->second;
THROWIFNOT(it->first == hash,"Assertion violation in L1907");
	if (i != j) {
	  ASSERT(i != j); // must have different topology, hence different indices
	  string desc = kineticHalfHelixStateChange(states[j], states[i], helixMaxIds, debugInfo);
	  if (desc != "forbidden") {
	    gset[i] = 1;
	    endConnectivityFound = true;
	    break; // no further looping needed for case i
	  }
	}
      }
	// }
    }	
    if (!endConnectivityFound) {
      Rcpp::Rcout << "# Could not find path element to end state: removing all node ids." << endl;
      for (size_t i = 0; i < gset.size(); ++i) {
	gset[i] = 0;
      }
    }
    DEBUG_MSG("# Finished createPregraphFromHalfHelixStates");
    return gset;
  }

  // same as createGraphFromStates (Keep synchronized with that method!), but only returning target states that are at the boundary 
  // old version - complicated and needs verification
  static Vec<int> createPreGraphFromHalfHelixStatesOld(const Vec<state_t>& states, const Vec<string>& connectivities, size_t startId, size_t endId, const Vec<unsigned int>& helixMaxIds) {
    DEBUG_MSG("# Starting createPregraphFromHalfHelixStates");
    string startConnectivity = connectivities[startId];
    string endConnectivity = connectivities[endId];
#ifndef NDEBUG
    Rcpp::Rcout << "# Start-id: " << (startId+1) << startConnectivity << " state: " << states[startId];
    Rcpp::Rcout << "# End-id:   " << (endId+1) << endConnectivity     << " state: " << states[endId];
#endif
    bool debugInfo = false;
    size_t n = states.size();
    Vec<int> gset(n, 0); // zero if state not include, one if included
THROWIFNOT(states.size() > 0,"Assertion violation in L1943");
    Vec<string> hashes(n);
    Vec<string> parentHashes(n);
    map<string, size_t> hashMap;
    multimap<string, size_t> childMap;
    bool endConnectivityFound = true;
    for (size_t i = 0; i < n; ++i) {
      hashes[i] = convertStateToHalfHelixHash(states[i]);
      auto parentHash = convertStateToParentHalfHelixHash(states[i]);
#ifndef NDEBUG
      Rcpp::Rcout << "# Created hash! id: " << (i+1) << " hash: " << hashes[i] << " parent-hash: " << parentHash << " state: " << states[i];
#endif
      if (parentHash != hashes[i]) {
	parentHashes[i] = parentHash;
	childMap.insert(make_pair(parentHashes[i], i));
      } else {
	Rcpp::Rcout << "# Warning: parent hash is equal to hash : " << i << " " << hashes[i] << endl;
      }
      hashMap.insert(make_pair(hashes[i], i));
      if (connectivities[i] == startConnectivity) {
	gset[i] = 1;
      }
      // Rcpp::Rcout << "Hash " << (i+1) << " " << hashes[i] << " " << parentHashes[i] << endl;
    }
#ifndef NDEBUG
    Rcpp::Rcout << "# Initially allowed states: " << gset;
    Rcpp::Rcout << "# Loop over states to see whether they should be added to graph..." << endl;
#endif
    for (size_t i = 0; i < n; ++i) {
      bool found = false;
#ifndef NDEBUG
      Rcpp::Rcout << "# i: " << i << " gset[i]: " << gset[i] << " connectivities[i]: " << connectivities[i] << endl;
#endif
      if ((gset[i] == 0) && (connectivities[i] == endConnectivity)) {
	// each structure must have endConnectivity (startConnectivy was check earlier)
	// it is checked, if at least one adjacent structure exists, 
	// that has the startConnectivity. 
	// check parents, siblings and children!
#ifndef NDEBUG
	Rcpp::Rcout << "# Checking, whether state " << (i+1) << " could be a transition state: " << states[i];
#endif
	string hash = hashes[i];
	string parentHash = parentHashes[i];
	auto parentHashIt = hashMap.find(parentHash);
	if ((hash != parentHash) && (parentHashIt != hashMap.end())) {
	  size_t parentId = parentHashIt->second; // hashMap[parentHash];
#ifndef NDEBUG
	  Rcpp::Rcout << "ID: " << (i+1) << " hash : " << hash << " parent-hash: " << parentHash << " parent-id: " << parentId << " parent allowed: " << gset[parentId] << " parent connect: " << connectivities[parentId] << endl;
#endif
THROWIFNOT(parentId < n,"Assertion violation in L1992");
THROWIFNOT(parentId != i,"Assertion violation in L1993");
	  if ((gset[parentId] == 1) && (connectivities[parentId] == startConnectivity)) {
	    string desc = kineticHalfHelixStateChange(states[parentId], states[i], helixMaxIds, debugInfo);
	    // kmd[i][j] = desc;
#ifndef NDEBUG
	    Rcpp::Rcout << "# checking transition from " << states[parentId];
	    Rcpp::Rcout << "# checking transition to " << states[i];
	    Rcpp::Rcout << "# State change (parent): " << desc << endl;
#endif
	    if (desc != "forbidden") {
	      gset[i] = 1;
	      found = true;
	      endConnectivityFound = true;
	    }
#ifndef NDEBUG
	    else {
	      Rcpp::Rcout << "# classified state change as " << desc << ": " << states[parentId] << " " << states[i];
	    }
#endif
	  } else {
	    Rcpp::Rcout << "# Hmm, parent structure is not allowed: " << gset[parentId] << endl;
	    Rcpp::Rcout << "# Connectivity: " << (connectivities[parentId]) << " OK: " << (connectivities[parentId] == startConnectivity) << endl;
	  }
	  if (!found) { // check all siblings: children of parent
#ifndef NDEBUG
	    Rcpp::Rcout << "# Checking sibling nodes of parent hash " << parentHash << endl;
#endif
	    auto itUp = childMap.upper_bound(parentHash);
	    for (auto childIt = childMap.lower_bound(parentHash); childIt != itUp; childIt++) {
	      size_t sibId = childIt->second;
#ifndef NDEBUG
	      Rcpp::Rcout << "Checking sibling id " << (sibId+1) << endl;
#endif
	      if ((sibId != i) && (gset[sibId] == 1) && (connectivities[sibId] == startConnectivity)) {
		string desc = kineticHalfHelixStateChange(states[sibId], states[i], helixMaxIds, debugInfo);
#ifndef NDEBUG
		Rcpp::Rcout << "# checking transition from " << states[sibId];
		Rcpp::Rcout << "# checking transition to " << states[i];
		Rcpp::Rcout << "# State change (sibling): " << desc << endl;
#endif
		if (desc != "forbidden") {
		  gset[i] = 1;
		  found = true;
		  endConnectivityFound = true;
		  break; // no further search among siblings needed
		}
#ifndef NDEBUG
		else {
		  Rcpp::Rcout << "# classified state change as " << desc << ": " << states[sibId] << " " << states[i];
		}
#endif
	      }
	    }
	  } else {
#ifndef NDEBUG
	    Rcpp::Rcout << "# No need to check for sibling nodes..." << endl;
#endif
	  }
	} // if (parentHashIt != hashMap.end
	if (!found) { // now search among child nodes
#ifndef NDEBUG
	  Rcpp::Rcout << "# Checking child nodes of hash " << hash << endl;
#endif
	  auto itUp = childMap.upper_bound(hash);
	  for (auto childIt = childMap.lower_bound(hash); childIt != itUp; childIt++) {
	    size_t childId = childIt->second;
#ifndef NDEBUG
	    Rcpp::Rcout << "Checking child id " << (childId+1) << endl;
#endif
	    if ((childId != i) && (gset[childId] == 1) && (connectivities[childId] == startConnectivity)) {
	      string desc = kineticHalfHelixStateChange(states[childId], states[i], helixMaxIds, debugInfo);
	      // kmd[i][j] = desc;
#ifndef NDEBUG
	      Rcpp::Rcout << "# checking transition from " << states[childId];
	      Rcpp::Rcout << "# checking transition to " << states[i];	    
	      Rcpp::Rcout << "# State change (child node): " << desc << endl;
#endif
	      if (desc != "forbidden") {
		gset[i] = 1;
		found = true;
		endConnectivityFound = true;
		break; // no further search among child nodes needed
	      }
#ifndef NDEBUG
	    else {
	      Rcpp::Rcout << "# classified state change as " << desc << ": " << states[childId] << " " << states[i];
	    }
#endif

	    }
	  }
	} else {
#ifndef NDEBUG
	  Rcpp::Rcout << "# No need to check of child nodes..." << endl;
#endif
	}
      }
    }
    if (!endConnectivityFound) {
      Rcpp::Rcout << "# Could not find path element to end state: removing all node ids." << endl;
      for (size_t i = 0; i < gset.size(); ++i) {
	gset[i] = 0;
      }
    }
    DEBUG_MSG("# Finished createPregraphFromHalfHelixStates");
    return gset;
  }
  
  static Graph2 createGraphFromStates(const Vec<state_t>& states, const Vec<double>& gv, const Vec<unsigned int>& helixMaxIds, double RT, double scale) {
    PRECOND(states.size() == gv.size());
    PRECOND(states.size() > 0);
    PRECOND(states[0].size() > 0);
    PRECOND(states[0].size() == helixMaxIds.size());
    bool debugInfo = false;
    size_t n = states.size();
    Graph2 g(n);
    if (states.size() == 0) {
      return g;
    }
THROWIFNOT(states.size() > 0,"Assertion violation in L2112");
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<size_t, set<size_t> > setHash;
    Vec<size_t> powerSums(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    for (size_t i = 0; i < n; ++i) {
      // long id = computeMultiPowerSum(states[i], helixMaxIds);
      long id = computePowerSum(states[i]); // , helixMaxIds);
      powerSums[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
/*       for (size_t j = 0; j < m; ++j) { */

/* 	if (states[i][j] > 0) { */
/* 	  digit_t saved = states[i][j]; */
/* 	  states[i][j] = 0; */
/* 	  powerSumsM1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = saved; // restore to previous value */
/* 	} else { */
/* 	  states[i][j] = 1; */
/* 	  powerSumsP1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = 0; // restore to previous value */
/* 	} */

/*       } */

    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      size_t id = powerSums[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if (i == j) {
	  continue;
	}
	// for (size_t j = i+1; j < n; ++j) {
	double dG = gv[j] - gv[i];
	string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  double kf = scale * exp(-dG/RT);
	  double kb = scale;
	  if (dG < 0) {
	    kf = scale;
	    kb = scale * exp(dG/RT);
	  }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2174");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2175");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2176");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2177");
	  double timeF = 1.0/kf;
	  double timeB = 1.0/kb;
	  if (!graphIsConnected(g, i, j)) {
	    graphAddEdge(g, i,j, timeF);
	  } else if (!graphIsConnected(g, j, i)) {
	    graphAddEdge(g, j, i, timeB);
	  }
	  // g[i].push_back(make_pair(j,kf));
	  // g[j].push_back(make_pair(i,kb));
	}
      }
    }
    
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L2197");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exacltly same helices (longer or shorter versions of it) present
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2226");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2227");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2228");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2229");
	    double timeF = 1.0/kf;
	    double timeB = 1.0/kb;
	    if(!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } else if (!graphIsConnected(g, j, i)) {
	      graphAddEdge(g, j, i, timeB);;
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L2250");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exactly same helices (longer or shorter versions of it) present
	 
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2280");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2281");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2282");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2283");
	    double timeF = 1.0/kf;
	    double timeB = 1.0/kb;
	    if (!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } else if (!graphIsConnected(g, j, i)) {
	      graphAddEdge(g, j, i, timeB);
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    // Rcpp::Rcout << "Description matrix:" << endl;
    // Rcpp::Rcout << kmd;
    return g;
  }

  static Graph2 createTargetedGraphFromStates(const Vec<state_t>& states, const Vec<double>& gv, const Vec<unsigned int>& helixMaxIds,
					      const Vec<Vec<Stem> >& subStems, const state_t& stateFrom, const state_t& stateTo,
					      StrandContainer * strands, ResidueModel * model, double scale) {
    PRECOND(states.size() == gv.size());
    PRECOND(states.size() > 0);
    PRECOND(states[0].size() > 0);
    PRECOND(states[0].size() == helixMaxIds.size());
    bool debugInfo = false;
    size_t n = states.size();
    Graph2 g(n);
    if (states.size() == 0) {
      return g;
    }
    double RT = model->getRT();
THROWIFNOT(states.size() > 0,"Assertion violation in L2317");
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<size_t, set<size_t> > setHash;
    Vec<size_t> powerSums(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    ResiduePairing pairingTo = createResiduePairing(strands, model, stateTo, subStems);
    Vec<double> goPotential(n);
    Vec<ResiduePairing> pairings(n);
    for (size_t i = 0; i < n; ++i) {
      // Rcpp::Rcout << "Trying to build temporary model for state " << states[i] << endl;
      pairings[i] = createResiduePairing(strands, model, states[i], subStems);
      goPotential[i] = computeGoPotential(pairings[i], pairingTo);
    }
    for (size_t i = 0; i < n; ++i) {
      // long id = computeMultiPowerSum(states[i], helixMaxIds);
      long id = computePowerSum(states[i]);
      powerSums[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
/*       for (size_t j = 0; j < m; ++j) { */
/* 	if (states[i][j] > 0) { */
/* 	  digit_t saved = states[i][j]; */
/* 	  states[i][j] = 0; */
/* 	  powerSumsM1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = saved; // restore to previous value */
/* 	} else { */
/* 	  states[i][j] = 1; */
/* 	  powerSumsP1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = 0; // restore to previous value */
/* 	} */
/*       } */
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      size_t id = powerSums[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if (i == j) {
	  continue;
	}
	if (goPotential[i] >= goPotential[j]) { // backwards folding or unproductive folding
	  continue;
	}
	// for (size_t j = i+1; j < n; ++j) {
	double dG = gv[j] - gv[i];
	string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  bool unproductive = false;
	  for (size_t k = 0; k < pairings[0].size(); ++k) {
	    if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
	      unproductive = true;
	      break;
	    }
	  }
	  if (unproductive) {
	    continue;
	  }
	  double kf = scale * exp(-dG/RT);
	  double kb = scale;
	  if (dG < 0) {
	    kf = scale;
	    kb = scale * exp(dG/RT);
	  }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2397");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2398");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2399");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2400");
	  double timeF = 1.0/kf;
	  // double timeB = 1.0/kb;
	  if (!graphIsConnected(g, i, j)) {
	    graphAddEdge(g, i,j, timeF);
	  } // else if (!graphIsConnected(g, j, i)) {
	  // graphAddEdge(g, j, i, timeB);
	  // }
	  // g[i].push_back(make_pair(j,kf));
	  // g[j].push_back(make_pair(i,kb));
	}
      }
    }
    
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L2420");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj,helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exacltly same helices (longer or shorter versions of it) present
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding or unproductive folding
	    continue;
	  }
	  double dG = gv[j] - gv[i];
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2461");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2462");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2463");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2464");
	    double timeF = 1.0/kf;
	    // double timeB = 1.0/kb;
	    if(!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } // else if (!graphIsConnected(g, j, i)) {
	    // graphAddEdge(g, j, i, timeB);;
	    // }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L2485");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exactly same helices (longer or shorter versions of it) present
	 
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding or unproductive folding
	    continue;
	  }
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  string desc = kineticStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2528");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2529");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2530");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2531");
	    double timeF = 1.0/kf;
	    // double timeB = 1.0/kb;
	    if (!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } // else if (!graphIsConnected(g, j, i)) {
	    // graphAddEdge(g, j, i, timeB);
	    // }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    // add unchanged transition:
    for (size_t i = 0; i < g.size(); ++i) {
      graphAddEdge(g, i,i, scale); // add self-transition
    }
    
    normalizeGraphTransitionProbabilities(g, false, true);

    // Rcpp::Rcout << "Description matrix:" << endl;
    // Rcpp::Rcout << kmd;
    return g;
  }

  static Graph2 createWholeHelixGraphFromStates(const Vec<state_t>& states, const Vec<double>& gv, const Vec<unsigned int>& helixMaxIds, double RT, double scale) {
    bool debugInfo = false;
    size_t n = states.size();
    Graph2 g(n);
    if (states.size() == 0) {
      return g;
    }
THROWIFNOT(states.size() > 0,"Assertion violation in L2565");
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<size_t, set<size_t> > setHash;
    Vec<size_t> powerSums(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    for (size_t i = 0; i < n; ++i) {
      // long id = computeMultiPowerSum(states[i], helixMaxIds);
      long id = computePowerSum(states[i]);
      powerSums[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
/*       for (size_t j = 0; j < m; ++j) { */

/* 	if (states[i][j] > 0) { */
/* 	  digit_t saved = states[i][j]; */
/* 	  states[i][j] = 0; */
/* 	  powerSumsM1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = saved; // restore to previous value */
/* 	} else { */
/* 	  states[i][j] = 1; */
/* 	  powerSumsP1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = 0; // restore to previous value */
/* 	} */

/*       } */

    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      size_t id = powerSums[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if (i == j) {
	  continue;
	}
	// for (size_t j = i+1; j < n; ++j) {
	double dG = gv[j] - gv[i];
	string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  double kf = scale * exp(-dG/RT);
	  double kb = scale;
	  if (dG < 0) {
	    kf = scale;
	    kb = scale * exp(dG/RT);
	  }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2627");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2628");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2629");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2630");
	  double timeF = 1.0/kf;
	  double timeB = 1.0/kb;
	  if (!graphIsConnected(g, i, j)) {
	    graphAddEdge(g, i,j, timeF);
	  } else if (!graphIsConnected(g, j, i)) {
	    graphAddEdge(g, j, i, timeB);
	  }
	  // g[i].push_back(make_pair(j,kf));
	  // g[j].push_back(make_pair(i,kb));
	}
      }
    }
    
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L2650");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exacltly same helices (longer or shorter versions of it) present
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2679");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2680");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2681");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2682");
	    double timeF = 1.0/kf;
	    double timeB = 1.0/kb;
	    if(!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } else if (!graphIsConnected(g, j, i)) {
	      graphAddEdge(g, j, i, timeB);;
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L2703");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj,helixMaxIds);
	size_t id = computePowerSum(sj);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	// loop over sets with exactly same helices (longer or shorter versions of it) present
	 
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  // for (size_t j = i+1; j < n; ++j) {
	  if (i == j) {
	    continue;
	  }
	  double dG = gv[j] - gv[i];
	  string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2733");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2734");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2735");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2736");
	    double timeF = 1.0/kf;
	    double timeB = 1.0/kb;
	    if (!graphIsConnected(g, i, j)) {
	      graphAddEdge(g, i,j, timeF);
	    } else if (!graphIsConnected(g, j, i)) {
	      graphAddEdge(g, j, i, timeB);
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }

    // Rcpp::Rcout << "Description matrix:" << endl;
    // Rcpp::Rcout << kmd;
    return g;
  }

  static ResiduePairing createResiduePairing(StrandContainer * strands, 
					     ResidueModel * model,
					     MultiIterator::container_t state, 
					     const Vec<Vec<Stem> >& subStems) {
    // DEBUG_MSG("Starting createResiduePairing");
    ResiduePairing pairing(strands, model);
    for (size_t i = 0; i < state.size(); ++i) {
      // Rcpp::Rcout << i << " " << state[i] << " " << subStems.size() << " " << subStems[i].size() << endl;
      if (state[i] > 0) {
	const Stem& stem = subStems[i][state[i]-1];
	if (!pairing.isPlaceable(stem)) {
	  Rcpp::Rcout << "Internal error in StrenumTools::CreateResiduePairing:" << endl;
	  Rcpp::Rcout << "State: " << endl;
	  Rcpp::Rcout << state << endl;
	  Rcpp::Rcout << "Stems corresponding to this state: " << endl;
	  for (size_t j = 0; j < state.size(); ++j) {
	    Rcpp::Rcout << subStems[j][state[j]-1];
	  }
	  Rcpp::Rcout << "Structure so far: " << endl;
	  Rcpp::Rcout << pairing << endl;
	  Rcpp::Rcout << "Inconsistent stem: " << stem << endl;
	  
	}
	ERROR_IF(!pairing.isPlaceable(stem), "Internal error trying to create structure: base pairs are not consistent.");
	pairing.setStem(stem);
      }
    }
    // DEBUG_MSG("Finished createResiduePairing");
    return pairing;
  }

  /** Return Go potential */
  static double computeGoPotential(const ResiduePairing& x, const ResiduePairing& to) {
    int count = 0;
    for (size_t i = 0; i < x.size(); ++i) {
      if (x[i] == to[i]) {
	++count;
      } else if (x[i] > 0) { // not only different structure but mispaired
	--count;
      }
    }
    return count / static_cast<double>(x.size());
  }

  /** the sum of probabilities of transitions must be one */
  static void normalizeGraphTransitionProbabilities(Graph2& graph, bool isLogarithm, bool asLogarithm) {
    for (size_t i = 0; i < graph.size(); ++i) {
      double s = 0.0;
      for (auto it = graph[i].begin(); it != graph[i].end(); it++) {
	double weight = it->second;
	s += weight;
      }
      if (s > 0.0) {
	for (auto it = graph[i].begin(); it != graph[i].end(); it++) {
	  it->second = it->second / s;
	  if (asLogarithm) {
THROWIFNOT(it->second > 0.0,"Assertion violation in StrenumTools.h :  L2812");
	    it->second = - log(it->second);
	    if (it->second < 0.0) {
	      it->second = 0.0;
	      DEBUG_MSG("Encountered negative nlog probability. Set to zero.");
	    }
	  }
	}	
      }
    }
  }

  // ech helix is either not present, fully present, or two versions of half-folded */
  static Graph2 createHalfHelixGraphFromStates(const Vec<state_t>& states, const Vec<double>& gv, const Vec<unsigned int>& helixMaxIds,
					       const Vec<Vec<Stem> >& subStems, const state_t& stateFrom, const state_t& stateTo,StrandContainer * strands, ResidueModel * model, double scale) {
    Rcpp::Rcout << "# Starting createHalfHelixGraphFromStates with helixMaxIds " << helixMaxIds;
    bool debugInfo = false;
    size_t n = states.size();
    Graph2 g(n);
    if (states.size() == 0) {
      return g;
    }
THROWIFNOT(states.size() > 0,"Assertion violation in L2834");
    double RT = model->getRT();
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<size_t, set<size_t> > setHash;
    Vec<size_t> powerSums(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    ResiduePairing fromTo = createResiduePairing(strands, model, stateFrom, subStems);
    ResiduePairing pairingTo = createResiduePairing(strands, model, stateTo, subStems);
    Vec<ResiduePairing> pairings(n);
    Vec<double> goPotential(n);
    for (size_t i = 0; i < n; ++i) {
      pairings[i] = createResiduePairing(strands, model, states[i], subStems);
      goPotential[i] = computeGoPotential(pairings[i], pairingTo);
    }
    // size_t nt = pairingTo.size();
    for (size_t i = 0; i < n; ++i) {      
      // long id = computeMultiPowerSum(states[i], helixMaxIds); // , helixMaxIds);
      long id = computePowerSum(states[i]); // , helixMaxIds);
      powerSums[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
/*    for (size_t j = 0; j < m; ++j) { */
/*	if (states[i][j] > 0) { */
/*	  digit_t saved = states[i][j]; */
/*	  states[i][j] = 0; */
/* 	  powerSumsM1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = saved; // restore to previous value */
/* 	} else { */
/* 	  states[i][j] = 1; */
/* 	  powerSumsP1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = 0; // restore to previous value */
/* 	} */
/*    } */
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      size_t id = powerSums[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if (i == j) {
	  continue;
	}
	if (goPotential[i] >= goPotential[j]) { // backwards folding
	  continue;
	}
	// for (size_t j = i+1; j < n; ++j) {
	double dG = gv[j] - gv[i];
	// string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  bool unproductive = false;
	  for (size_t k = 0; k < pairings[0].size(); ++k) {
	    if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
	      unproductive = true;
	      break;
	    }
	  }
	  if (unproductive) {
	    continue;
	  }
	  bool ok = true;
	  if (ok) {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2919");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2920");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2921");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2922");
	    double timeF = 1.0/kf;
	    // double timeB = 1.0/kb;
	    if (!graphIsConnected(g, i, j)) {
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2566");
	      graphAddEdge(g, i,j, timeF);
	      ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2566");
	    } // else if (!graphIsConnected(g, j, i)) {
	    // graphAddEdge(g, j, i, timeB);
	    // }
	  }
	  // g[i].push_back(make_pair(j,kf));
	  // g[j].push_back(make_pair(i,kb));
	}
      }
    }    
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L2944");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds); // , helixMaxIds);
	size_t id = computePowerSum(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding or unproductive folding
	    continue;
	  }	  
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  // string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    bool ok = true;
	    if (ok) {
	      double kf = scale * exp(-dG/RT);
	      double kb = scale;
	      if (dG < 0) {
		kf = scale;
		kb = scale * exp(dG/RT);
	      }
THROWIFNOT(kf <= 1.0,"Assertion violation in L2988");
THROWIFNOT(kf >= 0.0,"Assertion violation in L2989");
THROWIFNOT(kb <= 1.0,"Assertion violation in L2990");
THROWIFNOT(kb >= 0.0,"Assertion violation in L2991");
	      double timeF = 1.0/kf;
	      // double timeB = 1.0/kb;
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2630");
	      if(!graphIsConnected(g, i, j)) {
		graphAddEdge(g, i,j, timeF);
		ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2634");
	      } // else if (!graphIsConnected(g, j, i) && addBackwards) {
	      // graphAddEdge(g, j, i, timeB);;
	      // } 
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L3014");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds); // , helixMaxIds);
	size_t id = computePowerSum(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  // for (size_t j = i+1; j < n; ++j) {
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding
	    continue;
	  }
	  double dG = gv[j] - gv[i];
	  // string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    bool ok = true;
	    if (ok) {
	      double kf = scale * exp(-dG/RT);
	      double kb = scale;
	      if (dG < 0) {
		kf = scale;
		kb = scale * exp(dG/RT);
	      }
THROWIFNOT(kf <= 1.0,"Assertion violation in L3058");
THROWIFNOT(kf >= 0.0,"Assertion violation in L3059");
THROWIFNOT(kb <= 1.0,"Assertion violation in L3060");
THROWIFNOT(kb >= 0.0,"Assertion violation in L3061");
	      double timeF = 1.0/kf;
	      // double timeB = 1.0/kb;
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2629");
	      if (!graphIsConnected(g, i, j)) {
		graphAddEdge(g, i,j, timeF);
		ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2701");
	      } // else if (!graphIsConnected(g, j, i)) {
	      // graphAddEdge(g, j, i, timeB);
	      // }
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }
    // add unchanged transition:
    for (size_t i = 0; i < g.size(); ++i) {
      graphAddEdge(g, i,i, scale); // add self-transition
    }    
    normalizeGraphTransitionProbabilities(g, false, true);
    // Rcpp::Rcout << "Description matrix:" << endl;
    // Rcpp::Rcout << kmd;
    return g;
  }

  /** Like createHalfHelixGraphFromStates, but can handle larger state vectors
    * each helix is either not present, fully present, or two versions of half-folded */
  static Graph2 createHalfHelixGraphFromStates2(const Vec<state_t>& states, const Vec<double>& gv, const Vec<unsigned int>& helixMaxIds,
						const Vec<Vec<Stem> >& subStems, const state_t& stateFrom, const state_t& stateTo,StrandContainer * strands,
						ResidueModel * model, double scale) {
    Rcpp::Rcout << "# Starting createHalfHelixGraphFromStates with helixMaxIds " << helixMaxIds;
    bool debugInfo = false;
    size_t n = states.size();
    Graph2 g(n);
    if (states.size() == 0) {
      return g;
    }
THROWIFNOT(states.size() > 0,"Assertion violation in L3100");
    double RT = model->getRT();
    size_t m = states[0].size();
    // Vec<Vec<string> > kmd(n, Vec<string>(n, "forbidden"));
    // state 0000 is not added to any set!
    map<string, set<size_t> > setHash;
    Vec<string> hashes(n);
    // Vec<Vec<size_t> > powerSumsM1(n, Vec<size_t>(m)); // all hashes for structures with one helix less
    // Vec<Vec<size_t> > powerSumsP1(n, Vec<size_t>(m)); // all hashes for structures with one helix more
    ResiduePairing fromTo = createResiduePairing(strands, model, stateFrom, subStems);
    ResiduePairing pairingTo = createResiduePairing(strands, model, stateTo, subStems);
    Vec<ResiduePairing> pairings(n);
    Vec<double> goPotential(n);
    for (size_t i = 0; i < n; ++i) {
      pairings[i] = createResiduePairing(strands, model, states[i], subStems);
      goPotential[i] = computeGoPotential(pairings[i], pairingTo);
    }
    // size_t nt = pairingTo.size();
    for (size_t i = 0; i < n; ++i) {      
      // long id = computeMultiPowerSum(states[i], helixMaxIds); // , helixMaxIds);
      // long id = computePowerSum(states[i]); // , helixMaxIds);
      string id = computeStateHashString(states[i]); // , helixMaxIds);
      hashes[i] = id;
      auto it = setHash.find(id);
      if (it == setHash.end()) {
	set<size_t> s;
	s.insert(i);
	setHash.insert(make_pair(id, s));
      } else {
	it->second.insert(i);
      }
/*    for (size_t j = 0; j < m; ++j) { */
/*	if (states[i][j] > 0) { */
/*	  digit_t saved = states[i][j]; */
/*	  states[i][j] = 0; */
/* 	  powerSumsM1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = saved; // restore to previous value */
/* 	} else { */
/* 	  states[i][j] = 1; */
/* 	  powerSumsP1[i][j] = computePowerSum(states[i]); */
/* 	  states[i][j] = 0; // restore to previous value */
/* 	} */
/*    } */
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states
      string id = hashes[i];
      auto setIt = setHash.find(id);
      if (setIt == setHash.end()) { // nothing found!
	continue;
      }
      // loop over sets with exactly the same helices (longer or shorter versions of it) present
      auto sBegin = setIt->second.begin();
      auto sEnd = setIt->second.end();
      for (auto jt = sBegin; jt != sEnd; jt++) {
	size_t j = *jt;
	if (i == j) {
	  continue;
	}
	if (goPotential[i] >= goPotential[j]) { // backwards folding
	  continue;
	}
	// for (size_t j = i+1; j < n; ++j) {
	double dG = gv[j] - gv[i];
	// string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	// kmd[i][j] = desc;
	// Rcpp::Rcout << "State change: " << desc << endl;
	if (desc != "forbidden") {
	  bool unproductive = false;
	  for (size_t k = 0; k < pairings[0].size(); ++k) {
	    if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
	      unproductive = true;
	      break;
	    }
	  }
	  if (unproductive) {
	    continue;
	  }
	  bool ok = true;
	  if (ok) {
	    double kf = scale * exp(-dG/RT);
	    double kb = scale;
	    if (dG < 0) {
	      kf = scale;
	      kb = scale * exp(dG/RT);
	    }
THROWIFNOT(kf <= 1.0,"Assertion violation in L3186");
THROWIFNOT(kf >= 0.0,"Assertion violation in L3187");
THROWIFNOT(kb <= 1.0,"Assertion violation in L3188");
THROWIFNOT(kb >= 0.0,"Assertion violation in L3189");
	    double timeF = 1.0/kf;
	    // double timeB = 1.0/kb;
	    if (!graphIsConnected(g, i, j)) {
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2566");
	      graphAddEdge(g, i,j, timeF);
	      ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2566");
	    } // else if (!graphIsConnected(g, j, i)) {
	    // graphAddEdge(g, j, i, timeB);
	    // }
	  }
	  // g[i].push_back(make_pair(j,kf));
	  // g[j].push_back(make_pair(i,kb));
	}
      }
    }    
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one helix less
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] < 1) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] > 0,"Assertion violation in L3211");
	sj[k] = 0;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds); // , helixMaxIds);
	string id = computeStateHashString(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding or unproductive folding
	    continue;
	  }	  
	  // for (size_t j = i+1; j < n; ++j) {
	  double dG = gv[j] - gv[i];
	  // string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    bool ok = true;
	    if (ok) {
	      double kf = scale * exp(-dG/RT);
	      double kb = scale;
	      if (dG < 0) {
		kf = scale;
		kb = scale * exp(dG/RT);
	      }
THROWIFNOT(kf <= 1.0,"Assertion violation in L3255");
THROWIFNOT(kf >= 0.0,"Assertion violation in L3256");
THROWIFNOT(kb <= 1.0,"Assertion violation in L3257");
THROWIFNOT(kb >= 0.0,"Assertion violation in L3258");
	      double timeF = 1.0/kf;
	      // double timeB = 1.0/kb;
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2630");
	      if(!graphIsConnected(g, i, j)) {
		graphAddEdge(g, i,j, timeF);
		ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2634");
	      } // else if (!graphIsConnected(g, j, i) && addBackwards) {
	      // graphAddEdge(g, j, i, timeB);;
	      // } 
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }
    for (size_t i = 0; i < n; ++i) { // main loop over states; checking for all states with one additional helix
      for (size_t k = 0; k < m; ++k) {
	if (states[i][k] > 0) {
	  continue;
	}
	state_t sj = states[i];
THROWIFNOT(sj[k] == 0,"Assertion violation in L3281");
	sj[k] = 1;
	// size_t id = computeMultiPowerSum(sj, helixMaxIds); // , helixMaxIds);
	string id = computeStateHashString(sj); // , helixMaxIds);
	auto setIt = setHash.find(id);
	if (setIt == setHash.end()) { // nothing found!
	  continue;
	}
	auto sBegin = setIt->second.begin();
	auto sEnd = setIt->second.end();
	for (auto jt = sBegin; jt != sEnd; jt++) {
	  // for (auto jt = setIt.begin(); jt != setIt.end(); jt++) {
	  size_t j = *jt;
	  // for (size_t j = i+1; j < n; ++j) {
	  if (i == j) {
	    continue;
	  }
	  if (goPotential[i] >= goPotential[j]) { // backwards folding
	    continue;
	  }
	  double dG = gv[j] - gv[i];
	  // string desc = kineticWholeHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  string desc = kineticHalfHelixStateChange(states[i], states[j], helixMaxIds, debugInfo);
	  // kmd[i][j] = desc;
	  // Rcpp::Rcout << "State change: " << desc << endl;
	  if (desc != "forbidden") {
	    bool unproductive = false;
	    for (size_t k = 0; k < pairings[0].size(); ++k) {
	      if ((pairings[i][k] != pairings[j][k]) && (pairings[i][k] == pairingTo[k])) { // folding was correct but then unproductive away from target
		unproductive = true;
		break;
	      }
	    }
	    if (unproductive) {
	      continue;
	    }
	    bool ok = true;
	    if (ok) {
	      double kf = scale * exp(-dG/RT);
	      double kb = scale;
	      if (dG < 0) {
		kf = scale;
		kb = scale * exp(dG/RT);
	      }
THROWIFNOT(kf <= 1.0,"Assertion violation in L3325");
THROWIFNOT(kf >= 0.0,"Assertion violation in L3326");
THROWIFNOT(kb <= 1.0,"Assertion violation in L3327");
THROWIFNOT(kb >= 0.0,"Assertion violation in L3328");
	      double timeF = 1.0/kf;
	      // double timeB = 1.0/kb;
	      ERROR_IF(graphIsConnected(g, i, j), "Internal error StrenumToolsh-2629");
	      if (!graphIsConnected(g, i, j)) {
		graphAddEdge(g, i,j, timeF);
		ERROR_IF(!graphIsConnected(g, i, j), "Internal error StrenumToolsh-2701");
	      } // else if (!graphIsConnected(g, j, i)) {
	      // graphAddEdge(g, j, i, timeB);
	      // }
	    }
	    // g[i].push_back(make_pair(j,kf));
	    // g[j].push_back(make_pair(i,kb));
	  }
	}
      }
    }
    // add unchanged transition:
    for (size_t i = 0; i < g.size(); ++i) {
      graphAddEdge(g, i,i, scale); // add self-transition
    }    
    normalizeGraphTransitionProbabilities(g, false, true);
    // Rcpp::Rcout << "Description matrix:" << endl;
    // Rcpp::Rcout << kmd;
    return g;
  }
  
  /** Converts '|' character to '%'; converts space to '@' character. */
  static string sanitizeFileName(const string& filename) {
    string result = filename;
    for (size_t i = 0; i < result.size(); ++i) {
      if (result[i] == '|') {
	result[i] = '%';
      } else if (result[i] == ' ') {
	result[i] = '@';
      }
    }
    return result;
  }

  static void readStrenumFile(istream& is, Vec<MultiIterator::container_t>& savedStates, Vec<double>& savedG, Vec<double>& savedH, Vec<string>& savedConnectivities) {
    vector<string> lines = getLines(is);    
    for (size_t i = 0; i < lines.size(); ++i) {
      string line = trimWhiteSpaceFromString(lines[i]);
      if (line.size() == 0 || (line[0] == '#')) {
	continue;
      }
      vector<string> words = getTokens(line);
      size_t m = words.size();
      ERROR_IF(m < 8, "Error reading data file: expected more than 8 word in line.");
      savedH.push_back(Stod(words[m-4]));
      savedG.push_back(Stod(words[m-5]));
      savedConnectivities.push_back(words[m-3]);
      MultiIterator::container_t v;
      for (size_t j = 2; j <= m-6; ++j) {
	v.push_back(Stoui(words[j]));
      }
      savedStates.push_back(v);
    }
    ERROR_IF(savedH.size() != savedStates.size(), "Internal error reading data file: 310");
    ERROR_IF(savedG.size() != savedStates.size(), "Internal error reading data file: 311");
    ERROR_IF(savedH.size() == 0, "Internal error reading data file: no states read.");
  }

  // in this version, state description lines start with the keyword "State"
  //  static void readStrenumFileV2(istream& is, Vec<MultiIterator::container_t>& savedStates, Vec<double>& savedG, Vec<double>& savedH, Vec<string>& savedConnectivities) {
  static void readStrenumFileV2(istream& is, Vec<state_t>& savedStates, Vec<double>& savedG, Vec<double>& savedH, Vec<string>& savedConnectivities) {
    vector<string> lines = getLines(is);    
    size_t others = 0; // number of other data columns
    for (size_t i = 0; i < lines.size(); ++i) {
      string line = trimWhiteSpaceFromString(lines[i]);
      if (line.size() == 0 || (line[0] == '#')) {
	continue;
      }
      vector<string> words = getTokens(line);
      size_t m = words.size();
      if ((m < 8) || (words[0] != "State") ) {
	continue;
      }
      ERROR_IF(m < 8, "Error reading data file: expected more than 8 word in line.");
      savedH.push_back(Stod(words[m-2-others]));
      savedG.push_back(Stod(words[m-3-others]));
      savedConnectivities.push_back(words[m-1-others]);
      state_t v;
      for (size_t j = 3; j <= m-6; ++j) {
	v.push_back(Stoui(words[j]));
      }
      savedStates.push_back(v);
    }
    ERROR_IF(savedH.size() != savedStates.size(), "Internal error reading data file: 310");
    ERROR_IF(savedG.size() != savedStates.size(), "Internal error reading data file: 311");
    ERROR_IF(savedH.size() == 0, "Internal error reading data file: no states read.");
  }
  
  /** Given states, a path, a reference state id, find path segment from (id-1) to result (inclusive indices), that contain a set of overlapping helices (as in branch migration */
  static size_t findRelevantStates(const state_container_t& states,
				   const Vec<int>& path,
				   size_t id,
				   const Vec<Vec<Stem> >& subStems) {
    ERROR_IF(path.size() == 0, "Internal error StrenumToolsh-1827");
    ERROR_IF(states.size() == 0, "Internal error StrenumToolsh-1828");
    ERROR_IF(states[0].size() != subStems.size(), "Internal error StrenumToolsh-1829");
    ERROR_IF(id >= path.size(), "Internal error StrenumToolsh-1830");
    ERROR_IF(id <= 0, "Internal error StrenumToolsh-1873");
    // Rcpp::Rcout << "# Starting findRelevantStates for id " << id << " and " << states.size() << " states and path " << path;
    // Rcpp::Rcout << "# Core stems: " << subStems.size() << endl;
    size_t idStart = id - 1;
    size_t result = id;
    size_t m = states[0].size(); // number of core helices
    while (result < path.size()) {
      // Rcpp::Rcout << "findRelevantStates: Current position id: " << (result+1) << endl;
      // find helices that have changed:
      bool extend = false;
      for (size_t i = 0; i < m; ++i) { // loop over helices of most extended state
	// Rcpp::Rcout << "Current helix id " << i+1 << endl;
THROWIFNOT(result < path.size(),"Assertion violation in L3443");
THROWIFNOT(path[result] < static_cast<int>(states.size()),"Assertion violation in L3444");
	ERROR_IF(result >= path.size(), "Internal error StrenumToolsh-1841");
	ERROR_IF(path[result] >= static_cast<int>(states.size()), "Internal error StrenumToolsh-1842");
THROWIFNOT(i < states[path[result]].size(),"Assertion violation in L3447");
	ERROR_IF(result == 0, "Internal error StrenumToolsh-1843");
	// Rcpp::Rcout << "# Mark StrenumToolsh-1848" << endl;
	if ((states[path[result]][i] > 0) && (states[path[result]][i] != states[path[result-1]][i])) {
	  // Rcpp::Rcout << "# Mark StrenumToolsh-1850" << endl;
	  ERROR_IF(states[path[result]][i]== 0, "Internal error StenumToolsh-1845");
	  const Stem& stem1 = subStems[i][states[path[result]][i]-1];
	  // Rcpp::Rcout << "# Mark StrenumToolsh-1853" << endl;
	  // check if overlapping with other stems from previous states:
	  for (size_t j = idStart; j < result; ++j) { // loop over previous states
	    for (size_t k = 0; k < m; ++k) { // loop over helices of previous states
	      // Rcpp::Rcout << "# Mark StrenumToolsh-1857 " << (j+1) << " " << (k+1) << endl;
THROWIFNOT(j < path.size(),"Assertion violation in L3459");
	      ERROR_IF(j >= path.size(), "Internal error StrenumToolsh-1859");
	      ERROR_IF(path[j] >= static_cast<int>(states.size()), "Internal error StrenumToolsh-1860");
THROWIFNOT(path[j] < static_cast<int>(states.size()),"Assertion violation in L3462");
	      if ((k != i) && (states[path[j]][k] > 0))  {
THROWIFNOT(k < subStems.size(),"Assertion violation in L3464");
		ERROR_IF(k >= subStems.size(), "Internal error StrenumToolsh-1864");
THROWIFNOT(path[j] < static_cast<int>(states.size()),"Assertion violation in L3466");
		ERROR_IF(path[j] >= static_cast<int>(states.size()), "Internal error StrenumToolsh-1866");
THROWIFNOT(k < states[0].size(),"Assertion violation in L3468");
		ERROR_IF(k >= states[0].size(), "Internal errror StrenumToolsh-1868");
THROWIFNOT(states[path[j]][k] > 0,"Assertion violation in L3470");
THROWIFNOT(  (states[path[j]][k]-1) < subStems[k].size(),"Assertion violation in L3471");
		ERROR_IF((states[path[j]][k]-1) >= subStems[k].size(), "Internal error StrenumToolsh-1871");
		const Stem& stem2 = subStems[k][states[path[j]][k]-1];
		if (stem2.hasCommonBases(stem1)) {
		  result++;
		  // goto LOOP_END;
		  extend = true;
		  break;
		}
	      }
	    } // for (size_t k ...
	    if (extend) {
	      break;
	    }
	  } // for (size_t j ...
	  if (extend) {
	    break;
	  }
	} // if 
	if (extend) {
	  break;
	}
      } // for (size_t i ...
      if (!extend) {
	break;
      }
    } // while (result < path.size() ...
    if (result >= path.size()) {
      result = path.size()-1;
    }
    // Rcpp::Rcout << "# Finished findRelevantStates" << endl;
    return result;
  }

  static digit_t decodeDigit(digit_t digit, size_t subStemCount, bool evenOK, bool oddOK) {
    digit_t result = digit;
    if (digit > 1) { // 0 and 1 are not encoded 	if (digit > 1) { // if 0 (unfolded) or 1 (fully folded) leave alone
      if (evenOK && (!oddOK)) { // even only
	result = (digit-1) * 2; // 2,3,4 is mapped to 2, 4, 6
      } else if ((!evenOK) && oddOK) {
	result = ((digit-1) * 2) + 1; // 2,3,4 is mapped to 3,5,7 etc
      }
    }
    // Rcpp::Rcout << "# Decoding result of " << digit << " " << subStemCount << " " << evenOK << " " << oddOK << " : " << result << endl;
    return result;
  }

  static size_t possibleDigitCount(size_t subStemCount, bool evenOK, bool oddOK) {
    digit_t result = subStemCount;
    if (evenOK != oddOK) { // even only or odd only
      result = 2 + ( (subStemCount-2) / 2 ); // complicated: first two digits 0 and 1 are special for unfolded and fully folded stem; all subsequcent digits correspond to partially folded steams from "right" or "left"
    } // case of either both odd and even digits or case of 0 and 1 (no helix full helix): in both cases, we simply search all helices // else if (!evenOK) { // both are not allowed
      // result = 2; // one 1 and 0 is allowed: full helix or no helix
    // }
    return result;    
  }
  
  /** None of the stems may use the same bases */
  static bool isConsistentState(const state_t& state,
			  const Vec<Vec<Stem> >& subStems) {
    DEBUG_MSG("# Starting isConsistentState");
    // Rcpp::Rcout << "# Substems: " << subStems;
    for (size_t j = 1; j < state.size(); ++j) {
      if (state[j] > 0) {
THROWIFNOT(j < subStems.size(),"Assertion violation in L3535");
THROWIFNOT(state[j]-1 < subStems[j].size(),"Assertion violation in L3536");
	const Stem& stem = subStems[j][state[j]-1];
	for (size_t k = 0; k < j; ++k) {
	  if (state[k] > 0) {
	    const Stem& stem2 = subStems[k][state[k]-1];
	    if (stem.hasCommonBases(stem2)) {
	      return false;
	    }
	  }
	}
      }
    }
    DEBUG_MSG("# Finished isConsistentState");
    return true;
  }

  /** For a given array of core states, generate all substates. Note that core states are also included in returned result of substates. */
  static state_container_t generateSubstates(const state_container_t& states, const Vec<Vec<Stem> >& subStems, bool includeStatesMode) {
    DEBUG_MSG("# Starting generateSubstates");
    PRECOND(states.size() > 0);
    state_t subStemCounts(subStems.size());
    for (size_t i = 0; i < subStems.size(); ++i) {
      subStemCounts[i] = subStems[i].size() + 1;
    }
    // Rcpp::Rcout << "# Generating substates for " << states;
    state_container_t result;
    set<state_t> stateMap;
    for (auto it = states.begin(); it != states.end(); it++) {
      stateMap.insert(*it);
    }
    if (includeStatesMode) {
      result = states;
    }
    size_t n = states.size(); // number of landmark states
    size_t m = states[0].size(); // number of core helices
    Vec<size_t> helixIds;
    for (size_t i = 0; i < m; ++i) { // loop over core helices
      for (size_t j = 0; j < n; ++j) { // loop over landmark states
	if (states[j][i] != states[0][i]) { // check which helices are variable
	  helixIds.push_back(i);
	  break;
	}
      }
    }
    DEBUG_MSG("StrenumToolsh-3562");
    Vec<size_t> zeroOK(helixIds.size(), 0);
    Vec<size_t> evenOK(helixIds.size(), 0);
    Vec<size_t> oddOK(helixIds.size(), 0);
    for (size_t i = 0; i < helixIds.size(); ++i) {
      size_t id = helixIds[i];
      for (size_t j = 0; j < n; ++j) { // loop over landmark states
	DEBUG_MSG("StrenumToolsh-3570");
	if (states[j][id] == 0) { // check which helices can be unfolded
	  // zeroOK[i] == 1;
	} else if (states[j][id] > 1) {
	  if (states[j][id] % 2 == 1) { // check which helices can be unfolded
	    oddOK[i] = 1;
	  } else {
	    evenOK[i] = 1; // if not zero or odd it must be even
	  }
	}
      }
    }
    DEBUG_MSG("StrenumToolsh-3581");
THROWIFNOT(states.size() > 0,"Assertion violation in L3600");
    if (helixIds.size() > 0) {
      DEBUG_MSG("StrenumToolsh-3584");
      state_t bases(helixIds.size());
      for (size_t i = 0; i < helixIds.size(); ++i) {
	DEBUG_MSG("StrenumToolsh-3587");
	size_t id = helixIds[i];
	DEBUG_MSG("StrenumToolsh-3589");
THROWIFNOT(id < subStemCounts.size(),"Assertion violation in L3608");
THROWIFNOT(i < evenOK.size(),"Assertion violation in L3609");
THROWIFNOT(i < oddOK.size(),"Assertion violation in L3610");
	size_t digit = possibleDigitCount(subStemCounts[id], evenOK[i], oddOK[i]);
THROWIFNOT(i < bases.size(),"Assertion violation in L3612");
	bases[i] = digit;
      }
      DEBUG_MSG("StrenumToolsh-3592");
      // Rcpp::Rcout << "# Creating iterator with bases " << bases;
      // Rcpp::Rcout << "# Helix ids: " << helixIds;
      MultiIterator it(bases);
      do {
	DEBUG_MSG("StrenumToolsh-3597");
	state_t state = states[0]; // some helices may remain unchanged
	const MultiIterator::container_t vdigits = it.getDigits();
	// Rcpp::Rcout << "# Iterator digits: " << vdigits;	
	/* for (size_t i = 0; i < vdigits.size(); ++i) { */
	/*   Rcpp::Rcout << i << " " << vdigits[i] << endl; */
	/* } */
	DEBUG_MSG("StrenumToolsh-3597");
	for (size_t i = 0; i < helixIds.size(); ++i) {
	  DEBUG_MSG("StrenumToolsh-3606");
	  auto id = helixIds[i];
THROWIFNOT(i < vdigits.size(),"Assertion violation in StrenumTools.h :  L3631");
	  DEBUG_MSG("StrenumToolsh-3610");
	  auto d = vdigits[i]; // actual digit being transformed
THROWIFNOT(id < state.size(),"Assertion violation in L3634");
THROWIFNOT(id < subStemCounts.size(),"Assertion violation in L3635");
THROWIFNOT(i < evenOK.size(),"Assertion violation in L3636");
THROWIFNOT(i < oddOK.size(),"Assertion violation in L3637");
	  state[id] = decodeDigit(d, subStemCounts[id], evenOK[i], oddOK[i]);
	  // Rcpp::Rcout << "# Decoding digit " << i << " helix " << id << " : " << vdigits[id] << " result: " << state[id] << endl;
THROWIFNOT(i < vdigits.size(),"Assertion violation in L3640");
	  ERROR_IF(d != vdigits[i], "Internal error StrenumToolsh 3272");
	}
	DEBUG_MSG("StrenumToolsh-3621");
	// Rcpp::Rcout << "# Generated substate: " << state;
	if (stateMap.find(state) == stateMap.end()) {
	  result.push_back(state);
	}
      } while (it.incIfPossible());
    }
    DEBUG_MSG("# Checking states for consistency...");
    state_container_t finalResult;
    for (size_t i = 0; i < result.size(); ++i) {
      if (isConsistentState(result[i], subStems)) {
	finalResult.push_back(result[i]);
      }
    }
    /* for (size_t i = 0; i < states.size(); ++i) { */
    /*   for (size_t j = 0; j < result.size(); ++j) { */
    /* 	if (states[i] == result[j]) { */
    /* 	  found = true; */
    /* 	  break; */
    /* 	} */
    /*   } */
    /*   ERROR_IF(!found, "Internal error StrenumToolsh: Could not find initial state in generateSubstates"); */
    /* } */
    DEBUG_MSG("# Finished generateSubstates");
    return finalResult;
  }

  static void writeLine(ostream& os, const Vec<int>& pairing) {
    for (size_t ii = 0; ii < pairing.size(); ++ii) {
      os << " " << (pairing[ii]+1);
    }
  }

  static void writeLine(ostream& os, const Vec<unsigned int>& pairing) {
    for (size_t ii = 0; ii < pairing.size(); ++ii) {
      os << " " << (pairing[ii]+1);
    }
  }

  static void writeLine(ostream& os, const Vec<size_t>& pairing) {
    for (size_t ii = 0; ii < pairing.size(); ++ii) {
      os << " " << (pairing[ii]+1);
    }
  }

  static void writeLine(ostream& os, ResidueStructure& pairing) {
    writeLine(os, pairing.convertFromResidueStructure());
  }

};

#endif
