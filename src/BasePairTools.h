#ifndef _BASE_PAIR_TOOLS_
#define _BASE_PAIR_TOOLS_

class BasePairTools {

 public:
  typedef int index_t;

 public:


  static string generateBasePairHash(index_t start, index_t stop) {
    if (start > stop) {
      return generateBasePairHash(stop, start);
    }
    return itos(start + 1) + "_" + itos(stop+1); // "+1" external one-based counting 
  }

};

#endif
