#ifndef SCORED_PATH_H
#define SCORED_PATH_H

#include "Vec.h"
#include "RankedSolution3.h"
#include "MultiIterator.h"

class ScoredPath : public RankedSolution3<MultiIterator::container_t> {
  
 public:

  typedef MultiIterator::container_t container_t;

  ScoredPath(double _first, const container_t& _second) : RankedSolution3<MultiIterator::container_t>(_first, _second) {
  }

};

/** For two states that do not have an intermediate state, compute score to go state1 to state2. 
    If "impossible", return very high number */
class StateChangeScorer {
  
 public:

  virtual double operator () (const ScoredPath& state1, const ScoredPath& state2) const = 0;
  
};

class StateCompatibilityChecker {
    
 public:
      
    virtual bool operator () (const ScoredPath& state, const Vec<ScoredPath>& referenceStates) const = 0;

};

// scored states: sorted list of states: best are first. Lowest scores are first and best
// Notice that scored states and scored paths are represented by the same data straucture "ScoredPath"
inline
Vec<ScoredPath>
findScoredPaths(const Vec<ScoredPath>& scoredStates, size_t startState, size_t endState, size_t nMax, double scoreLimit,
		const StateCompatibilityChecker& compatibilityChecker,
		const StateChangeScorer& stateChangeScorer,
		set<size_t> tabooIds) {
  PRECOND(startState != endState);
  Rcpp::Rcout << "# Starting findScorePath " << (startState+1) << " to " << (endState+1) << endl;
  // create set A for paths
  set<ScoredPath> finalSet;
  // find n best intermediate nodes
  if (nMax > scoredStates.size()) {
    nMax = scoredStates.size();
  }
  Vec<size_t> bestStates;
  Vec<ScoredPath> compatibleStates;
  compatibleStates.push_back(scoredStates[startState]);
  compatibleStates.push_back(scoredStates[endState]);
  for (size_t i = 0; i < scoredStates.size(); ++i) { // inefficient: improve by using list of states that are known to be interesting
    if ((i != startState) && (i != endState) && (tabooIds.find(i) == tabooIds.end())) {
      if ( compatibilityChecker(scoredStates[i], compatibleStates) ) {
	bestStates.push_back(i);
	if (bestStates.size() >= nMax) {
	  break; // list is sorted
	}
      }
    }
  }  
  tabooIds.insert(startState);
  tabooIds.insert(endState);
  // Rcpp::Rcout << "# findScorePath: best intermediate states: " << bestStates;
  if (bestStates.size() > 0) {
    for (size_t i = 0; i < bestStates.size(); ++i) {
      //      findScoredPaths from start to intermediate nodes (up to n solutions)
THROWIFNOT(startState != bestStates[i],"Assertion violation in L73");
      Vec<ScoredPath> paths1 = findScoredPaths(scoredStates, startState, bestStates[i], nMax, scoreLimit, compatibilityChecker, stateChangeScorer, tabooIds);
THROWIFNOT((paths1.size() == 0) || (paths1[0].second[0] == startState),"Assertion violation in L75");
      // ASSERT((paths1.size() == 0) || (paths1[0].second[paths1.second.size()-1] == bestStates[i]));
      //      findScoredPaths from intermediate nodes to endNode (up to n solutions)
      Vec<ScoredPath> paths2 = findScoredPaths(scoredStates, bestStates[i], endState, nMax, scoreLimit, compatibilityChecker, stateChangeScorer, tabooIds);
      //      combine all n*n paths, store in set A
      for (size_t j = 0; j < paths1.size(); ++j) {
	const ScoredPath::container_t& path1 = paths1[j].second;
	double score1 = paths1[j].first;
	for (size_t k = 0; k < paths2.size(); ++k) {
	  const ScoredPath::container_t& path2 = paths2[k].second;
	  double score2 = paths2[k].first;
	  double combinedScore = score1 + score2; // path lengths are adding
	  ScoredPath::container_t combined(path1.size()+path2.size()-1); // note that last node of paths1[j] and first node of  paths2[k] are identical - store only once
	  copy(path1.begin(),   path1.end(), combined.begin());
	  copy(path2.begin()+1, path2.end(), combined.begin()+path1.size()); // 
	  if (combinedScore < scoreLimit) {
	    finalSet.insert(ScoredPath(combinedScore, combined));
	  }
	}
      }
    }
  } else {  // special case: no intermediate state found: must be elementary "hop"
    double score = stateChangeScorer(scoredStates[startState], scoredStates[endState]); 
    ScoredPath::container_t miniPath(2);
    miniPath[0] = startState;
    miniPath[1] = endState;
    if (score < scoreLimit) {
      finalSet.insert(ScoredPath(score, miniPath));
    }
  }
  Vec<ScoredPath> finalResult;
  finalResult.reserve(finalSet.size());
  for (auto it = finalSet.begin(); it != finalSet.end(); ++it) {
    finalResult.push_back(*it);
  }
  sort(finalResult.begin(), finalResult.end());
  Rcpp::Rcout << "Finished findScorePath " << startState << " " << endState << " with " << finalResult.size() << " solutions." << endl;
  return finalResult;
}

#endif
