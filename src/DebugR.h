#ifndef ASSERT_H
#define ASSERT_H
#include <Rcpp.h>
#include <string>
#include <iostream>

void ASSERT(bool b) {
  // BEGIN_RCPP
  if (!b) {
   throw std::range_error("Assertion failed!");
  }
  // END_RCPP
  return;
}

void EASSERT(bool b) {
  // BEGIN_RCPP
  if (!b) {
   Rcpp::Rcout << "Error/Assertion failed, however program is not stopped!" << std::endl;
   throw std::range_error("Error/Assertion failed!");
  }
  // END_RCPP
  return;
}

void PRECOND(bool b) {
  // BEGIN_RCPP
  if (!b) {
    throw std::range_error("Precondition failed!");
  }
  // END_RCPP
}

void POSTCOND(bool b) {
  // BEGIN_RCPP
  if (!b) {
    throw std::range_error("Postcondition failed!");
  }
  // END_RCPP
}


void DERROR(const std::string& s) {

  throw std::range_error(s);

 return;
}

void ERROR_IF(bool b, const std::string& s) {
  if (b) {

    throw std::range_error(s);
 
  }
  return;
}

void THROWIFNOT(bool b, const std::string& s) {
  if (!b) {
    Rcpp::Rcerr << "# throwing error: " << s << std::endl;
    throw std::range_error(s);
  }
  return;
}

/** Generate R-warning if condition b is true */
void WARNIFNOT(bool b, const std::string& s) {
  if (!b) {
    Rcpp::warning(s);
  }
}

void DEBUG_MSG(const std::string& s) {
#ifndef NDEBUG
  Rcpp::Rcout << s << std::endl;
#endif
}
#endif
