#ifndef ENTROPY_TOOLS
#define ENTROPY_TOOLS

/** Boltzmann constant in kcal/mol per Kelvin */
#define KB 0.0019872041

class EntropyTools {

 public:

  // ΔG°37 bulge (n=1) = ΔG°37 bulge initiation(n) + ΔG°37 (special C bulge) + ΔG°37 (base pair stack) – RT ln(number of states)
  // DESTABILIZING ENERGIES BY SIZE OF LOOP (INTERPOLATE WHERE NEEDED)
  // hp3 ave calc no tmm;hp4 ave calc with tmm; ave all bulges
  // SIZE         INTERNAL          BULGE            HAIRPIN
  // -------------------------------------------------------
  static double computeTurner04InternalFreeEnergy(int len) {
    switch (len) {
    case 1: return 999;
    case 2: return 999;
    case 3: return 999;
    case 4: return 1.1;
    case 5: return 2.0;
    case 6: return 2.0;
    case 7: return 2.1 ;
    case 8: return 2.3; 
    case 9: return 2.4; 
    case 10: return 2.5; 
    case 11: return 2.6; 
    case 12: return 2.7; 
    case 13: return 2.8; 
    case 14: return 2.9; 
    case 15: return 2.9; 
    case 16: return 3.0; 
    case 17: return 3.1; 
    case 18: return 3.1; 
    case 19: return 3.2; 
    case 20: return 3.3; 
    case 21: return 3.3; 
    case 22: return 3.4; 
    case 23: return 3.4; 
    case 24: return 3.5; 
    case 25: return 3.5; 
    case 26: return 3.5; 
    case 27: return 3.6; 
    case 28: return 3.6; 
    case 29: return 3.7; 
    case 30: return 3.7;
    }
    // Rcpp::Rcout << "Unusually large internal loop encountered: 37" << endl;
    return 3.8;
  }

  // ΔG°37 bulge (n=1) = ΔG°37 bulge initiation(n) + ΔG°37 (special C bulge) + ΔG°37 (base pair stack) – RT ln(number of states)
  // DESTABILIZING ENERGIES BY SIZE OF LOOP (INTERPOLATE WHERE NEEDED)
  // hp3 ave calc no tmm;hp4 ave calc with tmm; ave all bulges
  // SIZE         INTERNAL          BULGE            HAIRPIN
  // -------------------------------------------------------
  static double computeTurner04BulgeFreeEnergy(int len) {
    switch (len) {
    case 1: return  3.8;//      999
    case 2: return  2.8;//      999
    case 3: return  3.2;//     5.4 
    case 4: return  3.6;//     5.6
    case 5: return  4.0;//     5.7
    case 6: return  4.4;//     5.4
    case 7: return  4.6;//     6.0
    case 8: return  4.7;//     5.5
    case 9: return  4.8;//     6.4
    case 10: return  4.9;//     6.5
    case 11: return  5.0;//     6.6
    case 12: return  5.1;//     6.7
    case 13: return  5.2;//     6.8
    case 14: return  5.3;//     6.9
    case 15: return  5.4;//     6.9
    case 16: return  5.4;//     7.0
    case 17: return  5.5;//     7.1
    case 18: return  5.5;//     7.1
    case 19: return  5.6;//     7.2
    case 20: return  5.7;//     7.2
    case 21: return  5.7;//     7.3
    case 22: return  5.8;//     7.3
    case 23: return  5.8;//     7.4
    case 24: return  5.8;//     7.4
    case 25: return  5.9;//     7.5
    case 26: return  5.9;//     7.5
    case 27: return  6.0;//     7.5
    case 28: return  6.0;//     7.6
    case 29: return  6.0;//     7.6
    case 30: return  6.1;//     7.7
    }
    // cerr << "Unusually long bulge loop encountered: " << len << endl;
    return 6.2;
  }

  // Hairin loop free energy penalty acoording to Turner 2004 model */
  // ΔG°37 bulge (n=1) = ΔG°37 bulge initiation(n) + ΔG°37 (special C bulge) + ΔG°37 (base pair stack) – RT ln(number of states)
  //    DESTABILIZING ENERGIES BY SIZE OF LOOP (INTERPOLATE WHERE NEEDED)
    // hp3 ave calc no tmm;hp4 ave calc with tmm; ave all bulges
    // SIZE         INTERNAL          BULGE            HAIRPIN
    // -------------------------------------------------------
  static double computeTurner04HairpinFreeEnergy(int len) {
    // Rcpp::Rcout << "Starting computeTurner04HairpinFreeEnergy(length)" << endl;
    switch (len) {
    case 1: return  999;
    case 2: return  999;
    case 3: return  5.4; 
    case 4: return  5.6;
    case 5: return  5.7;
    case 6: return  5.4;
    case 7: return  6.0;
    case 8: return  5.5;
    case 9: return  6.4;
    case 10: return  6.5;
    case 11: return  6.6;
    case 12: return  6.7;
    case 13: return  6.8;
    case 14: return  6.9;
    case 15: return  6.9;
    case 16: return  7.0;
    case 17: return  7.1;
    case 18: return  7.1;
    case 19: return  7.2;
    case 20: return  7.2;
    case 21: return  7.3;
    case 22: return  7.3;
    case 23: return  7.4;
    case 24: return  7.4;
    case 25: return  7.5;
    case 26: return  7.5;
    case 27: return  7.5;
    case 28: return  7.6;
    case 29: return  7.6;
    case 30: return  7.7;
    }
    // cerr << "Unusually long hairpin loop encountered: " << len << endl;
    // Rcpp::Rcout << "Finished computeTurner04HairpinFreeEnergy(length)" << endl;
    return 7.8;
  }

  /** Hairin loop free energy penalty acoording to Turner 2004 model. The sequence includes the terminal base pair of the loop */
  static double computeTurner04HairpinFreeEnergy(string sequence) {
    // Rcpp::Rcout << "Starting computeTurner04HairpinFreeEnergy" << endl;
    PRECOND(sequence.size() > 2);
    if ((sequence.size() == 6) && (sequence[0] == 'C') && (sequence[5] == 'G')) {
      if (sequence =="CUACGG") {
	return 2.8;
      } else if (sequence == "CUCCGG") {
	return 2.7;
      } else if (sequence == "CUUCGG") {
	return 3.7;
      } else if (sequence == "CUUUGG") {
	return 3.7;
      } else if (sequence == "CCAAGG") {
	return 3.3;
      } else if (sequence == "CCCAGG") {
	return 3.4;
      } else if (sequence == "CCGAGG") {
	return 3.5;
      } else if (sequence == "CCUAGG") {
	return 3.7;
      } else if (sequence == "CCACGG") {
	return 3.7;
      } else if (sequence == "CCGCGG") {
	return 3.6;
      } else if (sequence == "CCUCGG") {
	return 2.5;
      } else if (sequence == "CUAAGG") {
	return 3.6;
      } else if (sequence == "CUCAGG") {
	return 3.7;
      } else if (sequence == "CUUAGG") {
	return 3.5;
      } else if (sequence == "CUGCGG") {
	return 2.8;
      } else if (sequence == "CAACGG") {
	return 5.5;
      }
      // no special case found
    }
    double result = computeTurner04HairpinFreeEnergy(static_cast<int>(sequence.size())-2);
    // Rcpp::Rcout << "Finished computeTurner04HairpinFreeEnergy" << endl;
    return result;
  }

  /** Returns -dS for loop formations according to Zhang J Chem Phys 2008. Units: kcal/K mol (not e.u.!) */
  static double loopEntropy(int len) {
    //    Rcpp::Rcout << "starting loopEntropy" << endl;
    double result = 0.0;
    if (len < 1) {
      return 0.0;
    }
    if (len >= 10) {
      result = (9.2 * KB) + 1.85 * KB * log(static_cast<double>(len)/10); // equation 3
      // Rcpp::Rcout << " -dS0: " <<  (9.2 * KB) << " log term: " << 1.85 * log(static_cast<double>(len)/10) << " within log; " << (log(static_cast<double>(len)/10)) << " total -dS: " << result << endl; // equation 3
    } else {
      result = KB * ((5.0/9.0 ) * (len - 1) + 4.0); // simplified fig 6
    }
    //  Rcpp::Rcout << "finished loopEntropy" << endl;
    return result;
  }

 private:

  /*  static double computeLogPairConformations(double distMin, double distMax, double lBox, double lGrid) {
    double vBox = lBox * lBox * lBox;
    double vGrid = lGrid * lGrid * lGrid;
    double vMax = (4.0/3.0) * PI * distMax * distMax * distMax;
    double vMin = (4.0/3.0) * PI * distMin * distMin * distMin;
    double vShell = vMax - vMin;
    double logW = log(vBox) - (2.0 * log(vGrid)) + log(vShell); // number of different orientations (vShell/vGrid) times number of locations of first particile (vBox/vGrid)
  }

  
  static double computeLogPairConformations(double distMin, double distMax, double lBox, double lGrid) {
    double vBox = lBox * lBox * lBox;
    double vGrid = lGrid * lGrid * lGrid;
    double vMax = (4.0/3.0) * PI * distMax * distMax * distMax;
    double vMin = (4.0/3.0) * PI * distMin * distMin * distMin;
    double vShell = vMax - vMin;
    double logW = log(vBox) - (2.0 * log(vGrid)) + log(vShell); // number of different orientations (vShell/vGrid) times number of locations of first particile (vBox/vGrid)
    return logW;
  }

  static double computeLogIllegalPairConformations(double distMin, double distMax, double lBox, double lGrid) {
    double vBox = lBox * lBox * lBox;
    double vGrid = lGrid * lGrid * lGrid;
    double vMax = (4.0/3.0) * PI * distMax * distMax * distMax;
    double vMin = (4.0/3.0) * PI * distMin * distMin * distMin;
    double vAntiShell = vBox - (vMax - vMin);
    double logW = log(vBox) - (2.0 * log(vGrid)) + log(vAntiShell); // number of different orientations (vShell/vGrid) times number of locations of first particile (vBox/vGrid)
    return logW;    
  }

  static double computeLogIllegalPairConformations(double distMin, double distMax, double lBox, double lGrid) {
    double vBox = lBox * lBox * lBox;
    double vGrid = lGrid * lGrid * lGrid;
    double vMax = (4.0/3.0) * PI * distMax * distMax * distMax;
    double vMin = (4.0/3.0) * PI * distMin * distMin * distMin;
    double vAntiShell = vBox - (vMax - vMin);
    double logW = log(vBox) - (2.0 * log(vGrid)) + log(vAntiShell); // number of different orientations (vShell/vGrid) times number of locations of first particile (vBox/vGrid)
    return logW;    
  }
  */
};

#endif
