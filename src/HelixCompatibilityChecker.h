#ifndef HELIX_COMPATIBILITY_CHECKER_H
#define HELIX_COMPATIBILITY_CHECKER_H

#include "Vec.h"
#include "RankedSolution3.h"
#include "MultiIterator.h"
#include "ScoredPath.h"

/** For two states that do not have an intermediate state, compute score to go state1 to state2. 
    If "impossible", return very high number */
class HelixCompatibilityChecker : public StateCompatibilityChecker {
    
 public:

  /** Relies on certain interpretation of states as stems and substems. */
  virtual bool operator () (const ScoredPath& state, const Vec<ScoredPath>& referenceStates) const {
    const auto& path = state.second;
    // size_t n = path.size();
    for (size_t i = 0; i < path.size(); ++i) {
      if (path[i] > 0) {	  

	bool even = path[i] % 2; 
	bool found = false; // find one helix in referene states that "covers" it:
	for (size_t j = 0; j < referenceStates.size(); ++j) {
	  const auto& refPath = referenceStates[j].second;	  
	  if (refPath[i] > 0) {
	    bool even2 = refPath[i] % 2; 
	    if ((refPath[i] == 1) || ((even == even2) && (path[i] >= refPath[i]))) {
	      found = true;
	      break;
	    }
	  }
	}
	if (!found) {
	  return false;  // this helix does not appear in any of the reference states
	}
      }
    }
    return true;
  }
  
};

#endif
