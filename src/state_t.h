#ifndef STATE_T
#define STATE_T

#include "Vec.h"

using namespace std;

class state_t : public Vec<unsigned int> {

 public:

  bool smallHelices; // true, iff small helices have been placed in addition to core helices. Length limits have to be clear from context

  typedef unsigned int digit_t;

 public:

 state_t() : Vec<unsigned int>(), smallHelices(false) { }

 state_t(size_type n) : Vec<unsigned int>(n), smallHelices(false) { }

 state_t(size_type n, digit_t value) : Vec<unsigned int>(n, value), smallHelices(false) { }

 state_t(const state_t& other) : Vec<unsigned int>(other), smallHelices(other.smallHelices) { }

  state_t& operator=(const state_t& other) {
    Vec<digit_t>::operator=(other);
    smallHelices = other.smallHelices;
    return *this;
  }


 private:


};

#endif
