#ifndef __STEM_HELP__
#define __STEM_HELP__

#include <iostream>
#include <stack>
#include <ctype.h> // for function tolower
#include "Vec.h"
#include "StringTools.h"
#include "Stem.h"
#include "vectornumerics.h"
#include "RankedSolution5.h"
#include "RnaSecondaryStructure.h"
#include "stemhelp.h"

/** returns true if AU, GC, GU (or T instead of U) 
 * TODO: not hard-coded characters!
 */
bool
isComplementary(char c1, char c2);

/** Return true if all positions are Watson-Crick complementar or G-U base paired */
inline
bool
isComplementary(const Stem& stem, const string& sequence) {
  Stem::index_type start = stem.getStart();
  Stem::index_type stop = stem.getStop();
  Stem::index_type len = stem.getLength();
  for (Stem::index_type i = start; i < len; ++i ) {
    if (!isComplementary(sequence[start + i], sequence[stop-i])) {
      return false;
    }
  }
  return true;
}

/** Write all pairings of one stem. */
inline
void
writePairings(ostream& os, const Stem& stem, const string& sequence, char sep) {
  Stem::index_type start = stem.getStart();
  Stem::index_type stop = stem.getStop();
  Stem::index_type len = stem.getLength();
  for (Stem::index_type i = start; i < len; ++i ) {
    Stem::index_type i2 = start + i;
    Stem::index_type i3 = stop - i;
    os << sequence[i2] << i2 << ":" << sequence[i3] << i3 << sep;
  }
}

/** Write all pairings of stems */
inline
void
writePairings(ostream& os, const Vec<Stem>& stems, const string& sequence, char sep1, char sep2 = '\n') {
  for (size_t i = 0; i  < stems.size(); ++i) {
    writePairings(os, stems[i], sequence, sep1);
    os << sep2;
  }
}


/** list of connections of three prime end */
Vec<Stem>
convertConnectionsToStems(const Vec<int>& v, unsigned int minStemLength,
			  const string& s);


/** list of connections of three prime end */
inline
Vec<Vec<double> >
convertSecondaryStructureToMatrix(const AbstractSecondaryStructure& structure) {
  unsigned int nn = structure.size();
  Vec<Vec<double> > matrix(nn, Vec<double>(nn, 0.0));
  for (unsigned int i = 0; i < structure.size(); ++i) {
    if (structure.isBasePaired(i)) {
      int j = structure.getBasePair(i);
      matrix[i][structure.getBasePair(i)] = 1.0;
      matrix[j][i] = matrix[i][j];
    }
  }
  return matrix;
}

/** Filters out stems which have start equal to stop position */
inline
Vec<Stem>
filterNoSelfStems(const Vec<Stem>& stems) {
  Vec<Stem> result;
  for (Vec<Stem>::size_type i = 0; i < stems.size(); ++i) {
    if (stems[i].getStart() != stems[i].getStop()) {
      result.push_back(stems[i]);
    }
  }
  return result;
}

/** list of connections of three prime end */
Vec<Stem>
generateStemsFromMatrix(const Vec<Vec<double> >& mtx, unsigned int minStemLength,
			double thresh,
			const string& sequence);


/** vector of stems from matrix */
inline
Vec<Stem>
convertSecondaryStructureToStems(const RnaSecondaryStructure& structure) {
  Vec<Vec<double> > matrix = convertSecondaryStructureToMatrix(structure);
  string s;
  return generateStemsFromMatrix(matrix, 1, 0.5, s);
}

/** list of connections of three prime end */
inline
RnaSecondaryStructure
convertMatrixToSecondaryStructure(const Vec<Vec<double> >& matrix, 
				  double cutoff) {
  PRECOND(cutoff > 0.0);
  RnaSecondaryStructure result(matrix.size());
  for (unsigned int i = 0; i < matrix.size(); ++i) {
    for (unsigned int j = i+1; j < matrix.size(); ++j) {
      if (matrix[i][j] >= cutoff) {
	result.setPaired(static_cast<int>(i),
			 static_cast<int>(j));
      }
    }
  }
  return result;
}

/** generates list of connections from stems */
Vec<int>
convertStemsToConnections(const Vec<Stem>& stems,
			  unsigned int nSize);

/** reads Ct file produced by mfold */
void
readCtSingle(istream& is, 
	     Vec<Stem>& stems,
	     string& sequence, 
	     unsigned int minStemLength );

/** reads Ct file possible corresponding to multiple strands */
void
readCtMulti(istream& is, 
	    Vec<Stem>& stems,
	    string& sequence, 
	    unsigned int minStemLength,
	    Vec<int>& starts);

/** reads Ct file possible corresponding to multiple strands */
void
readMyCtMulti(istream& is, 
	      Vec<Stem>& stems,
	      string& sequence, 
	      unsigned int minStemLength,
	      Vec<int>& starts);

/** reads CT file, generates RnaSecondaryStructure object. Preferred method for reading. */
inline
RnaSecondaryStructure
readCtSecondary(istream& is) {
  Vec<Stem> stems;
  string sequence;
  readCtSingle(is, stems, sequence, 1);
  return RnaSecondaryStructure(stems, sequence);
}

/** Writes a header of a Ct file, possibly corresponding to multiple sequences. */
void
writeCtHeader(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence,
	      const Vec<int>& starts);

/** Writes a header of a Ct file, possibly corresponding to multiple sequences. */
void
writeCtHeader(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence,
	      const Vec<int>& starts,
	      const Vec<string>& keyes,
	      const Vec<string>& values);

/** Writes the of a Ct file, possibly corresponding to multiple sequences. */
void
writeCtBody(ostream& os, 
	    const Vec<Stem>& stems, 
	    const string& sequence);

/** writes header and body Ct file produced by mfold */
inline
void
writeCt(ostream& os, 
	const Vec<Stem>& stems, 
	const string& sequence,
	const Vec<int>& starts) {
  writeCtHeader(os, stems, sequence, starts);
  writeCtBody(os, stems, sequence);
}

/** writes header and body Ct file produced by mfold */
inline
void
writeCt(ostream& os, 
	const Vec<Stem>& stems, 
	const string& sequence,
	const Vec<int>& starts,
	const Vec<string>& keys,
	const Vec<string>& values) {
  writeCtHeader(os, stems, sequence, starts, keys, values);
  writeCtBody(os, stems, sequence);
}

/** writes a Ct file produced by mfold */
void
writeCtSingle(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence);

/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] */
Vec<Vec<double> >
secToMatrix(const string& sec, double val, unsigned int& pkCounter);

/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] */
Vec<Vec<double> >
secToMatrix2(const string& sec, double val, char d);

/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] or ---AAA--AAA---*/
Vec<Vec<double> >
secToMatrix2(const string& sec, double val, unsigned int& pkCounter);

Vec<Vec<double> >
readBPL(istream& is, string& sequence);

/** reads combined Ct file produced by mfold */
Vec<Vec<Stem> >
readCtCombined(istream& is, string& sequence,
	       unsigned int minStemLength);

/** reads combined Ct file produced by mfold */
Vec<Vec<Stem> >
readCtCombined(istream& is, Vec<string>& sequences,
	       unsigned int minStemLength);

/** convert stems to matrix */
Vec<Vec<double> >
placeStemValueInMatrix(Vec<Vec<double> > & matrix, const Stem& stem, double value);

/** convert stems to matrix */
Vec<Vec<double> >
generateMatrixFromStems(const Vec<Stem>& stems, unsigned int nSize);

/** convert set of set of stems to matrix. Used for computing consensus secondary structure. */
Vec<Vec<double> >
generateAverageMatrixFromStems(const Vec<Vec<Stem> >& stems, unsigned int nSize);

/**
 * returns sequence which is in FASTA forma 
 */
string
readFasta(istream& is);

/**
 * returns sequence which is in Zuker format
 */
string
readZuker(istream& is);


/**
 * returns set of sequences which is in FASTA forma 
 */
Vec<string>
readMultiFasta(istream& is);

/** reads format in format compatible with STRUCTURELAB */
Vec<Stem>
readStems(istream& is);

/** reads format in format compatible with STRUCTURELAB, several concatenated region files */
Vec<Vec<Stem> >
readMultiStems(istream& is);

/** reads BPSEQ format from Gutell homepage */
void
readBPSeq(istream& is, Vec<Stem>& stems, string& sequence,
	  unsigned int minStemLength);

/** output of region file */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems);

/** writes stems with potential position offset */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems,
	   int indexOffset);

/** write stems */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems,
	   const Vec<Vec<Vec<double> > >& matrices,
	   const Vec<unsigned int>& usedIndices,
	   int indexOffset = 0);

/** write stems in BPL format */
void
writeBPLStems(ostream& os,
	      const string& name,
	      const string& sequence,
	      const Vec<Stem>& stems);

/** write stems in format that can be "spliced" into postscript file produced by RNAalifold */
void
writeRNAalifoldPairs(ostream& os,
		     const string& name,
		     const string& sequence,
		     const Vec<Stem>& stems);

string
readSequence(istream& seqFile);

/** write stems with respect to positions of sequences without gaps */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems,
	   const string& aliSequence);

/** writes matrix readable by S-Plus/R */
/* void */
/* writeMatrix(ostream& os, */
/* 	    const Vec<Vec<double> >& m); */

/** writes matrix readable by S-Plus/R */
void
writeMatrixReverse(ostream& os,
		   const Vec<Vec<double> >& m);

/** position of relPos' character in string (skip gaps)
    counting start from zero both relative and absolute
*/
unsigned int
convert2AbsolutePositions(unsigned int relPos,
			  const string& s,
			  char gapChar);

/** input vec: n'th character before gaps and gap-length,
    output vec: absolute position of first gap character
    (or character to right of gap before insert)
*/
void
convert2AbsoluteGaps(Vec< pair<unsigned int, unsigned int> >& gaps,
		     const string& s,
		     char gapChar);

/** generates stem list from sequence */
/* Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  Vec<unsigned int>& histogram,
			  bool guAllowed);
*/

/** generates stem list from sequence */
Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  bool guAllowed);

inline
Vec<Stem>
generateSubstems(const Stem& stem, Stem::index_type lenMin, Stem::index_type minDifference) {
  // Rcpp::Rcout << "Starting generateSubstems!: " << stem << " " << lenMin << endl;
  Stem::index_type start = stem.getStart();
  Stem::index_type stop = stem.getStop();
  Stem::index_type len = stem.getLength();
  Vec<Stem> result;
  // result.push_back(Stem(start, stop, len));
  for (Stem::index_type i = 0; i < len; ++i) {
    for (Stem::index_type len2 = lenMin; len2 <= len; ++len2) {
      if (i + len2 > len) {
	break;
      }
      if ((stop-i) - (start + i) < minDifference) {
	break;
      }
THROWIFNOT(start+i < stop-i,"Assertion violation in L389");
      result.push_back(Stem(start + i, stop-i, len2));
    }
  }
 // Rcpp::Rcout << "Finished generateSubstems!: " << stem << " " << lenMin << " : " << result << endl;
  return result;
}


/** generates stem list from sequence observing sequence boundaries */
Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  bool guAllowed,
			  const Vec<unsigned int>& sequenceIds);

/** tests generation of stem list from sequence */
void
testGenerateStemsFromSequence();

void
conformSequence(string& s, 
		const string& alphabet,
		char defaultChar);

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const string& s1, const string& s2);

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const Vec<string>& s);

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const string& s1, const string& s2,
			 const Vec<string>& matchRules);

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const Vec<string>& s,
			 const Vec<string>& matchRules);

/** returns index of stem in stem list that is similar (defined by slack)
    to query stem */
unsigned int
findSimilarStem(const Stem& query,
		const Vec<Stem>& stems,
		unsigned int slack);

/** read probability matrix which is in format of output postscript file form RNAfold of Vienna package! */
Vec<RankedSolution5<unsigned int, unsigned int> >
		     readRnaFoldProbFile(istream& is, unsigned int gapChar);

/** read RNA contact probability in our format
  list of contacts (residue1, residue2, probability). Counting starts from one externally,
  but from zero internally. */
Vec<RankedSolution5<unsigned int, unsigned int> >
readProbFile(istream& is);

/** sequence corresponds to sequence without gaps! */
Vec<RankedSolution5<unsigned int, unsigned int> >
readProbFromRegionFile(istream& is, unsigned int nSize);

/** pkCounter contains number of pseudoknots.
    (1 normal, 2: one pseudoknot etc) */
Vec<Stem>
stemsFromBracketFasta(istream& is, unsigned int& pkCounter);

/** pkCounter contains number of pseudoknots.
    (1 normal, 2: one pseudoknot etc) */
Vec<Stem>
stemsFromBracketFasta(istream& is, unsigned int& pkCounter, unsigned int& length);

Vec<Vec<Stem> >
readRNAsubopt(istream& is, Vec<double>& scores, string& sequence);

void
addStemToMatrix(Vec<Vec<double> >& compMatrix,
		 const Stem& stem,
		double stemWeight);

void
addStemsToMatrix(Vec<Vec<double> >& compMatrix,
		 const Vec<Stem>& referenceStems,
		 double stemWeight);


inline
double
mathewsFromMatrices(const Vec<Vec<double> >& pred,
		    const Vec<Vec<double> >& ref,
		    double scoreLimit,
		    unsigned int& tp,
		    unsigned int& fp,
		    unsigned int& tn,
		    unsigned int& fn)
{
  tp = 0;
  fp = 0;
  tn = 0;
  fn = 0;
  unsigned int nx = pred.size();
  for (unsigned int i = 0; i < nx; ++i) {
    for (unsigned int j = 0; j < i; ++j) {
      if (ref[i][j] >= scoreLimit) {
	if (pred[i][j] >= scoreLimit) {
	  ++tp;
	}
	else {
	  ++fn;
	}
      }
      else {
	if (pred[i][j] >= scoreLimit) {
	  ++fp;
	}
	else {
	  ++tn;
	}
      }
    }
  }
  return computeMathews(tp, fp, tn, fn);
}

inline
double
compareStems(unsigned int seqLen,
	     const Vec<Stem>& predStems,
	     const Vec<Stem>& refStems,
	     unsigned int& tp,
	     unsigned int& fp, 
	     unsigned int& tn,
	     unsigned int& fn)
{
  double scoreLim = 0.5;
  ERROR_IF(seqLen == 0,
	   "No sequence length defined!");
  Vec<Vec<double> > stemMatrix(seqLen, 
			       Vec<double>(seqLen, 0.0));
  addStemsToMatrix(stemMatrix, predStems, 1.0);
  Vec<Vec<double> > refMatrix(seqLen,
			       Vec<double>(seqLen, 0.0));
  addStemsToMatrix(refMatrix, refStems, 1.0);

  return mathewsFromMatrices(stemMatrix, refMatrix, scoreLim, tp, fp, tn, fn);
}


/** givens stems and sequence, return bracket notation. Checks for gaps. */
string
stemsToBracketFasta(const Vec<Stem>& stems, const string& sequence);

/** givens stems and sequence, return bracket notation. */
string
stemsToBracketFasta(const Vec<Stem>& stems, unsigned int len);

/** write all possible stems */
void
entropyStemList(ostream& os, 
		const Vec<Vec<double> >& mat, const string& sequence,
		const Vec<string>& allowedPairs,
		double scoreLimit,
		unsigned int minStemLength,
		double scoreWeight,
		double predictionConfidence);

/** removes nucleotide, adjusts stems accordingly */
void
deletePositionInStems(Vec<Stem>& stems, 
		      string sequence, unsigned int delPos);

/** removes nucleotide, adjusts stems accordingly */
void
insertGapInStems(Vec<Stem>& stems, 
		 string& sequence,
		 unsigned int insPos);

/** returns true if position is part of stem */
bool
isPosInStem(int x, int y, const Stem& stem);

/** returns true if position is part of one of stems */
bool
isPosInStems(int x, int y, const Vec<Stem>& stems);

/** returns number of base pairs */
unsigned int
countNumBasePairs(const Vec<Stem>& stems);

bool
isPartOfStem(int n) 
{
  return (n >= 0);
}

/** returns true if AU, GC, GU (or T instead of U) 
 * TODO: not hard-coded characters!
 */
bool
isComplementary(char c1, char c2)
{
  c1 = toupper(c1);
  if (c1 == 'T') {
    c1 = 'U';
  }
  c2 = toupper(c2);
  if (c2 == 'T') {
    c2 = 'U';
  }
  switch (c1) {
  case 'A':
    return (c2 == 'U');
  case 'C':
    return (c2 == 'G');
  case 'G':
    return ((c2 == 'C')||(c2 == 'U'));
  case 'U':
    return ((c2 == 'A')||(c2 == 'G'));
  default: return false;
  }
  return false;
  
}

/** list of connections of three prime end.
 * Ignores connections that go from 3' to 5' end.
 */
Vec<Stem>
convertConnectionsToStems(const Vec<int>& v, unsigned int minStemLength,
			  const string& s)
{
  // Rcpp::Rcout << "Starting convertConnectionsToStems with "
  // << v << " sequence: " << s << endl;
  PRECOND((s.size() == 0) || (v.size() == s.size()));
  ERROR_IF(v.size() != s.size(), "Internal error in line 17!");
  // Rcpp::Rcout << "Starting convertConnectionsToStems" << endl;
  Vec<Stem> result;
  bool openFlag = false;
  Stem newStem;
  for (unsigned int i = 0; i < v.size(); ++i) {
    // ignore connections that go from 3' to 5' end:
    if (isPartOfStem(v[i]) && (static_cast<int>(i) < v[i])) {
      if (!openFlag) {
	newStem.clear();
	newStem.setStart(i);
	newStem.setStop(v[i]);
	newStem.setLength(1);
	if (s.size() > 0) {
	  ERROR_IF(i >= s.size(), "Internal error in line 29!");
	  newStem.setSequence1(s.substr(i,1));
	  ERROR_IF(v[i] >= static_cast<int>(s.size()), 
		   "Internal error in line 31!");
	  newStem.setSequence2(s.substr(v[i],1));
	}
	openFlag = true;
      }
      else if ((i > 0) && ((v[i] + 1) == v[i-1])) {
	// extension of stem:
THROWIFNOT(openFlag,"Assertion violation in L649");
	newStem.setLength(newStem.getLength() + 1);
	if (s.size() > 0) {
	  ERROR_IF(i >= s.size(), 
		   "Internal error in line 41!");
	  ERROR_IF(v[i] >= static_cast<int>(s.size()), 
		   "Internal error in line 42!");
	  newStem.setSequence1(newStem.getSequence1() + s.substr(i,1));
	  newStem.setSequence2(s.substr(v[i],1) + newStem.getSequence2());
	}
      }
      else {  // there is a break: save exisiting stem, start new one
THROWIFNOT(openFlag,"Assertion violation in L661");
	if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	  result.push_back(newStem);
	}
	newStem.clear();
	newStem.setStart(i);
	newStem.setStop(v[i]);
	newStem.setLength(1);
	if (s.size() > 0) {
	  ERROR_IF(i >= s.size(), 
		   "Internal error in line 57!");
	  ERROR_IF(v[i] >= static_cast<int>(s.size()), 
		   "Internal error in line 58!");
	  newStem.setSequence1(s.substr(i,1));
	  newStem.setSequence2(s.substr(v[i],1));
	}
      }
    }
	else { // no bond , possible store started stem
      if (openFlag) {
	if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	  result.push_back(newStem);
	}
	openFlag = false;
      }
    }
  }
  // Rcpp::Rcout << "Ending convertConnectionsToStems" << endl;
  return result;
}

/** returns vector of integer; if element is "-1", than that position has no contact,
 * otherwise if there is a contact between positions i and j,
 * the contacting position j is written at position i and i at position j 
 */
Vec<int>
convertStemsToConnections(const Vec<Stem>& stems,
			  unsigned int nSize)
{
  Vec<int> result(nSize, -1);
  for (unsigned int i = 0; i < stems.size(); ++i) {
//     int start = stems[i].getStart();
//     int stop = stems[i].getStop();
    for (int j = 0; j < stems[i].getLength(); ++j) {
      ERROR_IF((stems[i].getStart()+j >= static_cast<int>(result.size())) 
	       || (stems[i].getStop()-j < 0),
	       "Stem index out of bounds!");
      // Rcpp::Rcout << "hey" << i << " " << j << stems[i].getStart() + j << " " << stems[i].getStop()-j << endl;
      result[stems[i].getStart()+j] = stems[i].getStop()-j;
      result[stems[i].getStop()-j] = stems[i].getStart()+j;
    }
  }
  // ASSERT(false);
  return result;
}

/** list of connections of three prime end */
Vec<Stem>
generateStemsFromMatrix(const Vec<Vec<double> >& mtx, unsigned int minStemLength,
			double thresh,
			const string& s)
{
  // Rcpp::Rcout << "Starting convertConnectionsToStems" << endl;
  Vec<Stem> result;
  bool openFlag = false;
  Stem newStem;
  for (int nys = 0; nys < static_cast<int>(mtx.size()); ++nys) {
    for (int  nx = 0; nx < nys; ++nx) {
      int ny = nys - nx;
      if (nx >= ny) { // hitting diagonal
	if (openFlag) {
	  if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	    result.push_back(newStem);
	  }
	  openFlag = false;
	}
	break; // search only one half
      }
      if (mtx[nx][ny] >= thresh) {
	if (!openFlag) {
	  newStem.clear();
	  newStem.setStart(nx);
	  newStem.setStop(ny);
	  newStem.setLength(1);
	  if ((s.size() > 0) && (nx < static_cast<int>(s.size())) 
	      && (ny < static_cast<int>(s.size()))) {
	    // 	    ERROR_IF(nx >= static_cast<int>(s.size()), 
	    // 		     "Internal error in line 29!");
	    newStem.setSequence1(s.substr(nx,1));
	    // 	    ERROR_IF(ny >= static_cast<int>(s.size()), 
	    // 		     "Internal error in line 31!");
	    newStem.setSequence2(s.substr(ny,1));
	  }
	  openFlag = true;
	}
	else {
	  // extension of stem:
THROWIFNOT(openFlag,"Assertion violation in L758");
	  newStem.setLength(newStem.getLength() + 1);
	  if ((s.size() > 0) && (nx < static_cast<int>(s.size())) 
	      && (ny < static_cast<int>(s.size()))) {
// 	    ERROR_IF(nx >= static_cast<int>(s.size()), 
// 		     "Internal error in line 41!");
// 	    ERROR_IF(ny >= static_cast<int>(s.size()), 
// 		     "Internal error in line 42!");
	    newStem.setSequence1(newStem.getSequence1() + s.substr(nx,1));
	    newStem.setSequence2(s.substr(ny,1) + newStem.getSequence2());
	  }
	}
      }
      else { // save openend stem
	if (openFlag) {
	  if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	    result.push_back(newStem);
	  }
	  openFlag = false;
	}
      }
    }
  }
  if (openFlag) {
    if (newStem.getLength() >= static_cast<int>(minStemLength)) {
      result.push_back(newStem);
    }
    openFlag = false;
  }
  for (unsigned int  nxs = 1; nxs < mtx.size(); ++nxs) {
    for (int ny = mtx.size()-1; ny >= 0; --ny) {
      int nx = nxs + (static_cast<int>(mtx.size()) - ny -1);
      if (nx >= ny) { // hitting diagonal
	if (openFlag) {
	  if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	    result.push_back(newStem);
	  }
	  openFlag = false;
	}
	break;
      }
      ERROR_IF(nx >= static_cast<int>(mtx.size()), 
	       "Internal error in line 147!");
      ERROR_IF(ny >= static_cast<int>(mtx[nx].size()), 
	       "Internal error in line 147!");
      ERROR_IF(nx < 0, "Internal error in line 185!");
      ERROR_IF(ny < 0, "Internal error in line 186!");
      if (mtx[nx][ny] >= thresh) {
	if (!openFlag) {
	  newStem.clear();
	  newStem.setStart(nx);
	  newStem.setStop(ny);
	  newStem.setLength(1);
	  if ((s.size() > 0) 
	      && (nx < static_cast<int>(s.size())) 
	      && (ny < static_cast<int>(s.size()))) {
	    ERROR_IF(nx >= static_cast<int>(s.size()), 
		     "Internal error in line 195!");
	    newStem.setSequence1(s.substr(nx,1));
	    ERROR_IF(ny >= static_cast<int>(s.size()), 
		     "Internal error in line 198!");
	    newStem.setSequence2(s.substr(ny,1));
	  }
	  openFlag = true;
	}
	else {
	  // extension of stem:
THROWIFNOT(openFlag,"Assertion violation in L825");
	  newStem.setLength(newStem.getLength() + 1);
	  if ((s.size() > 0) && (nx < static_cast<int>(s.size())) && (ny < static_cast<int>(s.size()))) {
	    ERROR_IF(nx >= static_cast<int>(s.size()), 
		     "Internal error in line 41!");
	    ERROR_IF(ny >= static_cast<int>(s.size()), 
		     "Internal error in line 42!");
	    newStem.setSequence1(newStem.getSequence1() + s.substr(nx,1));
	    newStem.setSequence2(s.substr(ny,1) + newStem.getSequence2());
	  }
	}
      }
      else { // save openend stem
	if (openFlag) {
	  if (newStem.getLength() >= static_cast<int>(minStemLength)) {
	    result.push_back(newStem);
	  }
	  openFlag = false;
	}
      }
    }
  }
  sort(result.begin(), result.end());
  // Rcpp::Rcout << "Ending generateStemsFromMatrix" << endl;
  return result;
}

Vec<Vec<double> >
readBPL(istream& is, string& sequence)
{
  string line;
  line = getLine(is);
  // skip first line
  sequence.clear();
  while (is) {
    line = getLine(is);
    if (line.size() == 0) {
      continue;
    }
    if (line[0] == '$') {
      break;
    }
    sequence.append(line);
  }
  Vec<Vec<double> > matrix(sequence.size(), Vec<double>(sequence.size(), 0.0));
  while (is) {
    line = getLine(is);
    if (line.size() == 0) {
      continue;
    }
    Vec<string> words = getTokens(line);
    if (words.size() == 2) {
      unsigned int i = Stoui(words[0]) - 1; // internal representation is one smaller
      unsigned int j = Stoui(words[1]) - 1; // internal representation is one smaller
      matrix[i][j] = 1.0;
      matrix[j][i] = 1.0;
    }
  }  
  return matrix;
}

/** reads Ct file produced by mfold */
void
readCtSingle(istream& is, Vec<Stem>& stems, string& sequence,
	     unsigned int minStemLength)
{
  sequence = "";
  stems.clear();
  string line = getLine(is);
  Vec<string> words = getTokens(line);
  if (words.size() < 1) {
    return;
  }
//   ERROR_IF(!(words.size() > 0),
// 	   "Number of entries in first line of Ct file expected!",
// 	   exception);
  unsigned int numEntries = Stoui(words[0]);
  Vec<int> threePrimers(numEntries, -1);
  for (unsigned int i = 0; i < numEntries; ++i) {
    line = getLine(is);
    // Rcpp::Rcout << "reading ct line: " << line << endl;
    words = getTokens(line);
    if(words.size() < 2) {
      Rcpp::Rcout << "Strange line: " << line << endl;
      continue;
    }
    sequence = sequence + words[1];
    if (words.size() > 4) {
      threePrimers[i] = Stoi(words[4]) - 1; // internal counting starts at zero
    }
    else {
      break;
    }
  }
  upperCase(sequence);
  stems = convertConnectionsToStems(threePrimers, minStemLength, sequence);
}

/** reads multi-Ct file. This is like a regular CT file, except in the first line
 * The number of sequences and the 1-based start residue ids are specified.
 * For example if the first line reads "184 2 1 98"
 * it means that the total number of residues is 184, there are 2 sequences, and the 5' residues have the indices 1 and 98
 * The starts vector containts these numbers as zero-based start indices.
 */
void
readCtMulti(istream& is, Vec<Stem>& stems, string& sequence,
	    unsigned int minStemLength,
	    Vec<int>& starts)
{
  // Rcpp::Rcout << "# Starting readCtMulti " << endl;
  sequence = "";
  stems.clear();
  string line = getLine(is);
  Vec<string> words = getTokens(line);
  // Rcpp::Rcout << "# header line in multi-CT file: " << words << endl;
  // Rcpp::Rcout << "# line :" << line << ":" << endl;
  ERROR_IF(words.size() < 1, "Bad header in first line of readCtMult.");
  //   ERROR_IF(!(words.size() > 0),
  // 	   "Number of entries in first line of Ct file expected!",
  // 	   exception);
  // Rcpp::Rcout << "# header line words: " << endl;
  for (size_t i = 0; i < words.size(); ++i) {
    // Rcpp::Rcout << "# " << (i+1) << ":" << words << ":" << endl; 
  }
  unsigned int numEntries = Stoui(words[0]);
  starts.clear();
  starts.push_back(0);
  unsigned int numSeqs = Stoui(words[1]);
  if (words.size() > 3) {
    for (Vec<string>::size_type i = 3; i < numSeqs+2; ++i) {
      // Rcpp::Rcout << "# parsing strand start index " << (i+1) << " " << words[i] << endl;
      starts.push_back(Stoi(words[i])-1);
    }
    // Rcpp::Rcout << "Number sequences:" << numSeqs << " start ids: " << starts << endl;
    ERROR_IF(numSeqs != starts.size(),
	     "Number of start ids does not match number of specified sequences");
  }
  Vec<int> threePrimers(numEntries, -1);
  // Rcpp::Rcout << "# Starting loop over " << numEntries
  // << " nucleotides." << endl;
  for (unsigned int i = 0; i < numEntries; ++i) {
    line = getLine(is);
    // Rcpp::Rcout << "# reading line " << (i+1) << " of ct file: " << line << endl;
    words = getTokens(line);
    // Rcpp::Rcout << "# Parsed words of current line: " << words << endl;
    if(words.size() < 2) {
      Rcpp::Rcout << "Strange line: " << line << endl;
      continue;
    }
    sequence = sequence + words[1];
    // Rcpp::Rcout << "# sequence parse so far: " << sequence << endl;
    if (words.size() > 4) {
      threePrimers[i] = Stoi(words[4]) - 1; // internal counting starts at zero
    }
    else {
      break;
    }
  }
  // Rcpp::Rcout << "Finished loop over residue lines!" << endl;
  upperCase(sequence);
  // Rcpp::Rcout << "# Parsed sequence: " + sequence << endl;
  stems = convertConnectionsToStems(threePrimers, minStemLength, sequence);
}

/** reads multi-Ct file. This is like a regular CT file, except in the first line
 * The number of sequences and the 1-based start residue ids are specified.
 * For example if the first line reads "184 2 1 98"
 * it means that the total number of residues is 184, there are 2 sequences, and the 5' residues have the indices 1 and 98
 * The starts vector containts these numbers as zero-based start indices.
 */
void
readMyCtMulti(istream& is, Vec<Stem>& stems, string& sequence,
	      unsigned int minStemLength,
	      Vec<int>& starts)
{
  // Rcpp::Rcout << "# Starting readMyCtMulti " << endl;
  sequence = "";
  stems.clear();
  string line = getLine(is);
  Vec<string> words = getTokens(line);
  starts.clear();
  // Rcpp::Rcout << "# header line in multi-CT file: " << words << endl;
  // Rcpp::Rcout << "# line :" << line << ":" << endl;
  ERROR_IF(words.size() < 1, "Bad header in first line of readCtMult.");
  //   ERROR_IF(!(words.size() > 0),
  // 	   "Number of entries in first line of Ct file expected!",
  // 	   exception);
  // Rcpp::Rcout << "# header line words: " << endl;
  for (size_t i = 0; i < words.size(); ++i) {
    // Rcpp::Rcout << "# " << (i+1) << ":" << words << ":" << endl; 
    if ((words[i].size() > 7) && (words[i].substr(0,7)  == "starts=")) {
      string rest = words[i].substr(7, words[i].size());
      Vec<string> words2 = getTokens(rest, ",");
      for (size_t j = 0; j < words2.size(); ++j) {
	starts.push_back(Stoi(words2[j]));
      }
    }
  }
  unsigned int numEntries = Stoui(words[0]);
  // unsigned int numSeqs = starts.size(); 
  // if (words.size() > 3) {
  //   for (Vec<string>::size_type i = 3; i < words.size(); ++i) {
  //     // Rcpp::Rcout << " parsing word " << (i+1) << " " << words[i] << endl;
  //     starts.push_back(Stoi(words[i])-1);
  //   }
  //   // Rcpp::Rcout << "Number sequences:" << numSeqs << " start ids: " << starts << endl;
  //   ERROR_IF(numSeqs != starts.size(),
  // 	     "Number of start ids does not match number of specified sequences");
  // }
  Vec<int> threePrimers(numEntries, -1);
  for (unsigned int i = 0; i < numEntries; ++i) {
    line = getLine(is);
    // Rcpp::Rcout << "# reading line " << (i+1) << " of ct file: " << line << endl;
    words = getTokens(line);
    if(words.size() < 2) {
      // Rcpp::Rcout << "Strange line: " << line << endl;
      continue;
    }
    sequence = sequence + words[1];
    if (words.size() > 2) {
      threePrimers[i] = Stoi(words[2]) - 1; // internal counting starts at zero
    }
    else {
      break;
    }
  }
  upperCase(sequence);
  stems = convertConnectionsToStems(threePrimers, minStemLength, sequence);
}


/** reads combined Ct file produced by mfold */
Vec<Vec<Stem> >
readCtCombined(istream& is, string& sequence, 
	       unsigned int minStemLength)
{
  Vec<Vec<Stem> > result;
  sequence.clear();
  string sequenceTmp;
  while (is) {
    Vec<Stem> stems;
    readCtSingle(is, stems, sequenceTmp, minStemLength);
    if ((sequenceTmp.size() > 0) && (sequence.size() == 0)) {
      sequence = sequenceTmp;
    }
    if (stems.size() == 0) {
      break;
    }
    result.push_back(stems);
  }
  return result;
}

/** reads combined Ct file, allows for different sequences */
Vec<Vec<Stem> >
readCtCombined(istream& is, Vec<string>& sequences, 
	       unsigned int minStemLength)
{
  Vec<Vec<Stem> > result;
  sequences.clear();
  string sequenceTmp;
  while (is) {
    Vec<Stem> stems;
    readCtSingle(is, stems, sequenceTmp, minStemLength);
    if (sequenceTmp.size() > 0) {
      sequences.push_back(sequenceTmp);
      result.push_back(stems);
    }
  }
  return result;
}

/** writes n copies of char c to output stream */
void 
writeChars(ostream& os, char c, unsigned int n) {
  for (unsigned int i = 0; i < n; ++i) {
    os << c;
  }
}

/** writes header of Ct file produced by mfold */
void
writeCtHeader(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence,
	      const Vec<int>& starts) {
  unsigned int numEntries = sequence.size();
  Vec<int> connections = convertStemsToConnections(stems,
						   numEntries);
  os << sequence.size();
  if (starts.size() > 0) {
    os << "\t" << externalCounting(starts); // " ENERGY = 0.0 DROTRE4        75 bp ss-tRNA" << endl;
  } else {
    os << endl;
  }
}

/** writes header of Ct file produced by mfold */
void
writeCtHeader(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence,
	      const Vec<int>& starts,
	      const Vec<string>& keys,
	      const Vec<string>& values) {
  PRECOND(keys.size() == values.size());
  unsigned int numEntries = sequence.size();
  Vec<int> connections = convertStemsToConnections(stems,
						   numEntries);
  os << sequence.size() << "\t";
  if (starts.size() > 0) {
    Vec<int> ext = externalCounting(starts); // " ENERGY = 0.0 DROTRE4        75 bp ss-tRNA" << endl;
    os << ext.size();
    for (size_t i = 0; i < ext.size(); ++i) {
      os << " " <<  ext[i];
    }
  } else {
    os << "1 1"; // one sequence and it start at residue 1 (in external counting)
  }
  for (size_t i = 0; i < keys.size(); ++i) {
    os << "\t" << keys[i] << "=" << values[i];
  }
  os << endl;
}

/** writes body Ct file produced by mfold */
void
writeCtBody(ostream& os, 
	    const Vec<Stem>& stems, 
	    const string& sequence) {
  unsigned int numEntries = sequence.size();
  Vec<int> connections = convertStemsToConnections(stems,
						   numEntries);
  for (unsigned int i = 0; i < numEntries; ++i) {
    string s1 = itos(i + 1);
    int len1 = s1.size();
    int sp1l = 5-len1;
    ERROR_IF(sp1l < 0, "Index in CT file too large!");
    writeChars(os, ' ', sp1l); 
    char resChar = sequence[i];
    if (resChar == '.') {
      resChar = '-';
    }
    os << s1 << " " << resChar;
    string s2 = itos(i);
    int len2 = s2.size();
    int sp2l = 6-len2;
    ERROR_IF(sp2l < 0, "Index in CT file too large!");
    writeChars(os, ' ', sp2l); 
    os << s2;
    string s3 = itos(i+2);
    int len3 = s3.size();
    int sp3l = 6-len3;
    ERROR_IF(sp3l < 0, "Index in CT file too large!");
    writeChars(os, ' ', sp3l); 
    os << s3;

    string s4 = itos(connections[i]+1);
    int len4 = s4.size();
    int sp4l = 6-len4;
    ERROR_IF(sp4l < 0, "Index in CT file too large!");
    writeChars(os, ' ', sp4l); 
    os << s4;

    int sp1lb = 6-len1;
    ERROR_IF(sp1lb < 0, "Index in CT file too large!");
    writeChars(os, ' ', sp1lb); 
    os << s1 << endl;
  }
}



/** reads Ct file produced by mfold */
void
writeCtSingle(ostream& os, 
	      const Vec<Stem>& stems, 
	      const string& sequence)
{
  writeCt(os, stems, sequence, Vec<int>());
}

/**
 * returns sequence which is in FASTA forma 
 */
string
readFasta(istream& is)
{
  string result;
  string line = getLine(is);
  line = trimWhiteSpaceFromString(line);
  if (line.length() == 0) {
    return result;
  }
  if (line[0] != '>') {
    result = result + line;
  }
  while (is) {
    line = getLine(is);
    line = trimWhiteSpaceFromString(line);
    if (line.size() > 0) {
      if (line[0] == '>') {
        break;
      }
      result = result + line;
    }
  }
  return removeWhiteSpaceFromString(result);
}

/**
 * returns sequence which is in Zuker format 
 */
string
readZuker(istream& is)
{
  string result;
  string line = getLine(is);
  line = getLine(is);
  while (is) {
    line = getLine(is);
    line = trimWhiteSpaceFromString(line);
    line = removeFromString(line, ';');
    line = removeFromString(line, '1');
    result = result + line;
  }
  return removeWhiteSpaceFromString(result);
}

/**
 * returns set of sequences which is in FASTA forma 
 */
Vec<string>
readMultiFasta(istream& is)
{
  Vec<string> result;
  while (is) {
    result.push_back(readFasta(is));
  }
  return result;
}

/** removes whitespace, convert to upper case, use default char like 'X' */
void
conformSequence(string& s,
		const string& alphabet,
		char defaultChar)
{
  s = removeWhiteSpaceFromString(s);
  s = removeFromString(s, string(".-"));
  upperCase(s);
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (alphabet.find(s[i]) >= alphabet.size()) {
      s[i] = defaultChar; // replace by default char (like 'X')
    }
  }
}

/** reads format in format compatible with STRUCTURELAB */
Vec<Stem>
readStems(istream& is)
{
  Vec<Stem> result;
  int oldId = -999;
  int id = 0;
  while (is) {
    string line = getLine(is);
    Vec<string> words = getTokens(line);
    if ((words.size() > 4) && (words[0][0] != '#')) {
      Stem stem;
      if (words[1][0] == ')') {
	id = Stoi(words[0]); // index counter
	ERROR_IF(words.size() < 5, 
		 "Too few words in region file.");
	stem.setStart(abs(Stoi(words[2])) - 1);
	stem.setStop(abs(Stoi(words[3])) - 1);
	stem.setLength(Stoui(words[4]));
	if (words.size() > 5) {
	  stem.setEnergy(Stod(words[5]));
	}
	else {
	  stem.setEnergy(0.0);
	}
      }
      else {
	id = Stoi(words[0].substr(0, words[0].size()-1)); // index counter, remove trailing ")" 
	stem.setStart(abs(Stoi(words[1])) - 1); // internal counting starts at zero instead of one
	stem.setStop(abs(Stoi(words[2])) - 1);
	stem.setLength(Stoui(words[3]));
	if (words.size() > 4) {
	  stem.setEnergy(Stod(words[4]));
	}
	else {
	  stem.setEnergy(0.0);
	}
      }
      if (id > oldId) {
	result.push_back(stem);
      }
      else {
	break; // quit loop
      }
      oldId = id;
    }
    else if (words.size() == 0) {
      break; // quit if empty line
    }
  }
  return result;
}

/** reads format in format compatible with STRUCTURELAB : read several concatenated stem tables */
Vec<Vec<Stem> >
readMultiStems(istream& is)
{
  Vec<Vec<Stem> > result(1);
  int prevId = 0; // index (first column in region file) of last stem
  while (is) {
    string line = getLine(is);
    // Rcpp::Rcout << "Reading line: " << line << endl;
    Vec<string> words = getTokens(line);
    if ((words.size() > 4) && (words[0][0] != '#')) {
      Stem stem;
      if (words[0][words[0].size()-1] == ')') {
	// remove last ")" character if fused with number:
	ERROR_IF(words[0].size() < 2, 
		 "At lest two letters expected in first word of line!");
	words[0] = words[0].substr(0, words[0].size()-1); 
      }
      int id = Stoi(words[0]);
      if (id <= prevId) {
	result.push_back(Vec<Stem>()); // start new stem table
      }
      prevId = id;
      if (words[1][0] == ')') {
	ERROR_IF(words.size() < 5, 
		 "Too few words in region file.");
	stem.setStart(Stoi(words[2]) - 1);
	stem.setStop(Stoi(words[3]) - 1);
	stem.setLength(Stoui(words[4]));
	if (words.size() > 5) {
	  stem.setEnergy(Stod(words[5]));
	}
	else {
	  stem.setEnergy(0.0);
	}
      }
      else { // if there is no
	stem.setStart(Stoi(words[1]) - 1); // internal counting starts at zero instead of one
	stem.setStop(Stoi(words[2]) - 1);
	stem.setLength(Stoui(words[3]));
	if (words.size() > 4) {
	  stem.setEnergy(Stod(words[4]));
	}
	else {
	  stem.setEnergy(0.0);
	}
      }
      ERROR_IF(result.size() < 1, "Internal error in line 287!");
      result[result.size()-1].push_back(stem);
    }
  }
  return result;
}


/** reads Ct file produced by mfold */
void
readBPSeq(istream& is, Vec<Stem>& stems, string& sequence,
	  unsigned int minStemLength)
{
  sequence = "";
  stems.clear();
  string line = getLine(is);
  Vec<string> words = getTokens(line);
  if (words.size() < 1) {
    return;
  }
//   ERROR_IF(!(words.size() > 0),
// 	   "Number of entries in first line of Ct file expected!",
// 	   exception);
  // unsigned int numEntries = Stoui(words[0]);
  Vec<int> threePrimers;
  while (is) {
    line = getLine(is);
    words = getTokens(line);
    if ((words.size() != 3) || (!isdigit(words[0][0]))) {
      continue;
    }
    sequence = sequence + words[1];
    // internal counting starts at zero
    threePrimers.push_back(Stoi(words[2]) - 1); 
  }
  stems = convertConnectionsToStems(threePrimers, minStemLength, sequence);
}

/** write stems */
void
writeStem(ostream& os,
	  const Stem& stem,
	  int stemNum)
{
  os << " " << (stemNum+1) << " )\t" 
     << (stem.getStart() + 1) << "\t"
     << (stem.getStop() + 1) << "\t"
     << (stem.getLength()) << "\t" 
     << stem.getEnergy() << "\t" 
     << stem.getSequence1() << "\t"
     << stem.getSequence2();
}

/** write stems */
void
writeStem(ostream& os,
	  const Stem& stem,
	  int stemNum,
	  int offset)
{
  os << " " << (stemNum+1) << " )\t" 
     << (stem.getStart() + offset + 1) << "\t"
     << (stem.getStop() + offset + 1) << "\t"
     << (stem.getLength()) << "\t" 
     << stem.getEnergy() << "\t" 
     << stem.getSequence1() << "\t"
     << stem.getSequence2();
}


/** write stems */
void
writeStem(ostream& os,
	  const Stem& stem,
	  int stemNum,
	  const Vec<Vec<Vec<double> > >& matrices,
	  const Vec<unsigned int>& usedIndices,
	  int offset)
{
  os << " " << (stemNum+1) << " )\t" 
     << (stem.getStart() + offset + 1) << "\t"
     << (stem.getStop() + offset + 1) << "\t"
     << (stem.getLength()) << "\t" 
     << stem.getEnergy() << "\t" 
     << stem.getSequence1() << "\t"
     << stem.getSequence2();
  if (usedIndices.size() == 0) {
    return;
  }
  for (int k = 0; k < stem.getLength(); ++k) {
    int x = stem.getStart()+k;
    int y = stem.getStop()-k;
    if ((x < 0) || (y < 0) || (x >= static_cast<int>(matrices[0].size()))
	|| (y >= static_cast<int>(matrices[0].size()))) {
      break;
    }
    os << "  ";    
    for (unsigned int i = 0; i < usedIndices.size(); ++i) {
      ERROR_IF(usedIndices[i] >= matrices.size(),
	       "Undefined matrix index encountered!");
      os << matrices[usedIndices[i]][x][y] << " ";
    }
  }
}


/** write stems */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems)
{
  for (int i = 0; i < static_cast<int>(stems.size()); ++i) {
    writeStem(os, stems[i], i);
    os << endl;
  }
}

/** write stems */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems, int offset)
{
  for (int i = 0; i < static_cast<int>(stems.size()); ++i) {
    writeStem(os, stems[i], i, offset);
    os << endl;
  }
}


/** write stems */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems,
	   const Vec<Vec<Vec<double> > >& matrices,
	   const Vec<unsigned int>& usedIndices,
	   int offset)
{
  for (int i = 0; i < static_cast<int>(stems.size()); ++i) {
    writeStem(os, stems[i], i, matrices, usedIndices, offset);
    os << endl;
  }
}

/** write stems in BPL format */
void
writeBPLStems(ostream& os,
	      const string& name,
	      const string& sequence,
	      const Vec<Stem>& stems)
{
  for (int i = 0; i < static_cast<int>(stems.size()); ++i) {
    os << ">" << name << endl;
    os << sequence << endl;
    os << endl;
    os << "$ list1" << endl;
    for (unsigned int i = 0; i < stems.size(); ++i) {
      for (int j = 0; j < stems[i].getLength(); ++j) {
	os << (stems[i].getStart() + j + 1) << " " 
	   << (stems[i].getStop() - j + 1) << endl;
      }
    }
    os << endl;
  }
}

/** write stems in format that can be "spliced" into postscript file produced by RNAalifold */
void
writeRNAalifoldPairs(ostream& os,
		     const string& name,
		     const string& sequence,
		     const Vec<Stem>& stems)
{
  for (int i = 0; i < static_cast<int>(stems.size()); ++i) {
    for (unsigned int i = 0; i < stems.size(); ++i) {
      for (int j = 0; j < stems[i].getLength(); ++j) {
	os << "[" << (stems[i].getStart() + j + 1) << " " 
	   << (stems[i].getStop() - j + 1) << "]" << endl;
      }
    }
    os << endl;
  }
}


/** writes matrix readable by S-Plus/R */
// void
// writeMatrix(ostream& os,
// 	    const Vec<Vec<double> >& m)
// {
//   for (unsigned int i = 0; i < m.size(); ++i) {
//     for (unsigned int j = 0; j < m[i].size(); ++j) {
//       os << m[i][j] << " ";
//     }
//     os << endl;
//   }
// }

/** writes matrix readable by S-Plus/R */
void
writeMatrixReverse(ostream& os,
		   const Vec<Vec<double> >& m)
{
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m.size(); ++j) {
      os << m[i][m.size()-j-1] << " ";
    }
    os << endl;
  }
}




/** returns absolute position of sequence without gaps with 
    position of sequence with gaps */
unsigned int
getSeqWithoutGapPos(unsigned int pos, const string& s, char gapChar)
{
  PRECOND(pos < s.size());
  unsigned int counter = 0;
  for (unsigned int i = 0; i < pos; ++i) {
    if (s[i] == gapChar) {
      ++counter;
    }
  }
  POSTCOND(counter <= pos);
  return pos - counter;
}

/** write stems with respect to positions of sequences without gaps */
void
writeStems(ostream& os,
	   const Vec<Stem>& stems,
	   const string& aliSeq)
{
  PRECOND(aliSeq.size() > 0);
  char gapChar = '-';
  Vec<unsigned int> missingIds;
  int szOrig = aliSeq.size();
  string aliClean = removeFromString(aliSeq, gapChar);
  Vec<int> connections = convertStemsToConnections(stems, aliSeq.size());
  for (int i = szOrig; i >= 0; --i) {
    if (aliSeq[i] == gapChar) {
      for (unsigned int j = 0; j < connections.size(); ++j) {
	if (connections[j] == i) {
	  connections[j] = -1; // delete that contact
	}
	else if (connections[j] > i) {
	  --connections[j]; // reduce index by one, because shift due to delete
	}
      }
      connections.erase(connections.begin()+i); // remove that entry
    }
  }
  unsigned int minStemLength = 1;
  Vec<Stem> newStems = convertConnectionsToStems(connections,
						 minStemLength, aliClean);
  writeStems(os, newStems);
  /*
    for (unsigned int i = 0; i < stems.size(); ++i) {
    unsigned int startPos = getSeqWithoutGapPos(stems[i].getStart(), aliSeq, gapChar);
    unsigned int stopPos = getSeqWithoutGapPos(stems[i].getStop(), aliSeq, gapChar);
    if (stopPos > startPos) {
    os << (i+1) << ")\t" 
    << (startPos + 1) 
	 << "\t"
	 << ( + 1) 
	 << "\t"
	 << (stems[i].getLength()) << "\t" << stems[i].getEnergy() << "\t" 
	 << stems[i].getSequence1() << "\t"
	 << stems[i].getSequence2() << endl;
	 }
	 else {
	 missingIds.push_back(i);
	 }
	 }
	 if (missingIds.size() > 0) {
	 count << "Warning! Missing stem ids: " << externalCounting(missingIds) << endl;
	 }
  */
}

string
readSequence(istream& seqFile) 
{
  string sequence;
  string line = getLine(seqFile);
  line = getLine(seqFile);
  // allow for reading 2 different formats: FASTA and StructureLab
  if ((line.size() > 0) && (!isWhiteSpace(line[0]))
      && (line[0] != '>') && (line[0] != '#')
      && (!isdigit(line[0]))) {
    sequence = line;
  }

  while(seqFile) {
    sequence = sequence + getLine(seqFile);
  }
  
  upperCase(sequence); // transform to upper case
  
  sequence = removeWhiteSpaceFromString(sequence);
  sequence = removeFromString(sequence, '-'); // remove dash character
  return sequence;
}

bool
isRnaMatch(char c1, char c2, bool guAllowed)
{
  switch (c1) {
  case 'A': 
    if ((c2 == 'T') || (c2 == 'U')) {
      return true;
    }
    break;
  case 'C': 
    if (c2 == 'G') {
      return true;
    }
    break;
  case 'G': 
    if (c2 == 'C') {
      return true;
    }
    if (((c2 == 'U') || (c2 == 'T')) && guAllowed) {
      return true;
    }
    break;
  case 'U': 
    if (c2 == 'A') {
      return true;
    }
    if ((c2 == 'G') && guAllowed) {
      return true;
    }
    break;
  case 'T': 
    if (c2 == 'A') {
      return true;
    }
    if ((c2 == 'G') && guAllowed) {
      return true;
    }
    break;
  }
  return false;
}

bool
isRnaMatch(const string& s1, const string& s2, bool guAllowed)
{
  PRECOND(s1.size() == s2.size());
  for (unsigned int i = 0; i < s1.size(); ++i) {
    if (!isRnaMatch(s1[i], s2[i], guAllowed)) {
      return false;
    }
  }
  return true;
}

/** generates stem list from sequence */
Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  bool guAllowed) {
  Vec<unsigned int> sequenceIds(sequence.size(), 0U);
  return generateStemsFromSequence(sequence, minStemLength, guAllowed, sequenceIds);
}

/** generates stem list from sequence */
Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  bool guAllowed,
			  const Vec<unsigned int>& sequenceIds)
{
  PRECOND(sequenceIds.size() == sequence.size());
  // bool stemOpen = false;
  unsigned int stemStart = 0;
  unsigned int stemStop = 0;
  unsigned int stemLength = 0;
  double stemEnergy = 0.0;
  Vec<Stem> stems;
  for (unsigned int i = 0; i < sequence.size(); ++i) {
    for (unsigned int j = i + 1; j < sequence.size(); ++j) {
      if (isRnaMatch(sequence[i], sequence[j], guAllowed)) {
	if ((i > 0) && ((j+1) < sequence.size()) && (sequenceIds[i] == sequenceIds[i-1]) && (sequenceIds[j] == sequenceIds[j+1]) 
	    && (isRnaMatch(sequence[i-1], sequence[j+1], guAllowed))) {
	  continue; // this is not the start of the stem, so skip
	}
	// stemOpen = true;
	stemLength = 1;
	stemStart = i;
	stemStop = j;
	stemEnergy = 1.0; // dummy energy
	// ++stemLength;
	unsigned int k = 1;
	for (k = 1; ((i+k) < sequence.size()) && (k <= j)
	       && ((i+k) < (j-k)); ++k) {
	  if ((!isRnaMatch(sequence[i+k], sequence[j-k], guAllowed))
	      || (sequenceIds[i+k] != sequenceIds[i]) || (sequenceIds[j-k] != sequenceIds[j])) { // test for sequence boundaries
	    break;
	  }
	} 
	stemLength = k;
// 	if (stemLength >= histogram.size()) {
// 	  stemLength = histogram.size() - 1;
// 	}
	string seq1= sequence.substr(stemStart, stemLength);
	ERROR_IF(stemLength > stemStop, "Internal error in line 321!");
	string seq2 = sequence.substr(stemStop - stemLength + 1, stemLength);
	// ++histogram[stemLength];
 	string seq2Rev = reverseString(seq2);
 	// reverse(seq2Rev.begin(), seq2Rev.end());
	if(!isRnaMatch(seq1, seq2Rev, guAllowed)) {
	  Rcpp::Rcout << seq1 << endl << seq2 << endl << seq2Rev << endl;
	  DERROR("Internal error in line 328!");
	}
	if (stemLength >= minStemLength) {     
	  stems.push_back(Stem(stemStart, stemStop, stemLength, 
			       stemEnergy, seq1, seq2));
	}
      }
    }
  }
  return stems;
}

/** Tests generation stem list from sequence */
void
testGenerateStemsFromSequence() {
  Rcpp::Rcout << "*** Starting testGenerateStemsFromSequence ***" << endl;
  string s = "ACGUUACGU";
  bool guAllowed = false;
  unsigned int stemLengthMin = 3;
  Vec<Stem> result = generateStemsFromSequence(s, stemLengthMin, guAllowed);
  // Rcpp::Rcout << "Result: " << result << endl;
THROWIFNOT(result.size() == 1,"Assertion violation in L1820");
  string tetrahedron = "ACAUUCCUAAGUCUGAAACAUUACAGCUUGCUACACGAGAAGAGCCGCCAUAGUAUAUCACCAGGCAGUUGACAGUGUAGCAAGCUGUAAUAGAUGCGAGGGUCCAAUACUCAACUGCCUGGUGAUAAAACGACACUACGUGGGAAUCUACUAUGGCGGCUCUUCUUCAGACUUAGGAAUGUGCUUCCCACGUAGUGUCGUUUGUAUUGGACCCUCGCAU";
  Vec<Stem> result2 = generateStemsFromSequence(tetrahedron, 10, false);
//  Rcpp::Rcout << "Tetrahedron Result: " << result2 << endl;
  ASSERT(result2.size() == 6); // 6 sides of tetrahedron!  
  Rcpp::Rcout << "*** Finished testGenerateStemsFromSequence ***" << endl;
}


/** generates stem list from sequence */
/* 
Vec<Stem> 
generateStemsFromSequence(const string& sequence,
			  unsigned int minStemLength,
			  bool guAllowed)
{
  bool stemOpen = false;
  unsigned int stemStart = 0;
  unsigned int stemStop = 0;
  unsigned int stemLength = 0;
  double stemEnergy = 0.0;
  Vec<Stem> stems;
  
  for (unsigned int i = 1; i < sequence.size(); ++i) {
    for (unsigned int j = 0; j < i; ++j) {
      if (isRnaMatch(sequence[i], sequence[j], guAllowed)) {
	if ((i > 0) && (isRnaMatch(sequence[i-1], sequence[j+1], guAllowed))) {
	  continue; // this is not the start of the stem, so skip
	}
	stemOpen = true;
	stemLength = 1;
	stemStart = i;
	stemStop = j;
	stemEnergy = 1.0; // dummy energy
	++stemLength;
	unsigned int k = 1;
	for (k = 1; (i+k < sequence.size()) && (k <= j); ++k) {
	  if (!isRnaMatch(sequence[i+k], sequence[j-k], guAllowed)) {
	    break;
	  }
	} 
	stemLength = k;
	string seq1= sequence.substr(stemStart, stemLength);
	string seq2 = sequence.substr(stemStop - stemLength + 1, stemLength);
	if (stemLength >= minStemLength) {     
	  stems.push_back(Stem(stemStart, stemStop, stemLength, 
			       stemEnergy, seq1, seq2));
	}
      }
    }
  }
  return stems;
}
*/

 /** Generates stem table for one sequence that is "cut" at certain points. 
  * Even for a single sequence, the first element of the "starts" array has to be alwasy zero. 
  */
 /*
Vec<Stem>
generateAllStems(const string& s, const Vec<int>& seqId, bool guAllowed, const Vec<int>& dividers, bool longestOnly) const {
  Rcpp::Rcout << "######## Creating stems with minimum length " << minExtendability << endl;
    Vec<Stem> result;
    set<pair<int, int> > forbiddenStrandCombos = findForbiddenStrandCombinations(static_cast<int>(sequences->size()), dividers);
    if (verbose >= VERBOSE_DETAILED) {
      Rcpp::Rcout << "# The following strand combinations are not allowed due to compartementalization with dividers " << dividers << " : ";
      for (set<pair<int, int> >::const_iterator it=forbiddenStrandCombos.begin(); it != forbiddenStrandCombos.end(); it++) {
	pair<int, int> p = *it;
	Rcpp::Rcout << " " << (p.first + 1) << ":" << (p.second+1) << endl;
      }
      Rcpp::Rcout << endl;
    }
    int n = static_cast<int>(s.size());
    if (verbose > 3) {
      Rcpp::Rcout << "# Searching sequence of length " << s.size() << endl;
      Rcpp::Rcout << "# Generated seqIds: " << endl << seqId << endl;
    }
    for (int i = 0; i < n; ++i) { // start positions
      for (int j = i + 1; j < n; ++j) { // stop positions
	int s1 = seqId[i];
	int s2 = seqId[j];
THROWIFNOT(s1 <= s2,"Assertion violation in L1901");
	if ((s1 < s2) && (forbiddenStrandCombos.find(pair<int, int>(s1,s2)) != forbiddenStrandCombos.end())) {
	  continue; // forbidden strand combinations, skip
	}
	bool interStrand = (s1 != s2);
	// Rcpp::Rcout << "Position pair " << (i+1) << " " << (j+1) << " " << seqId[i] << " " << seqId[j] << " interstrand: " << interStrand << " " << s[i] << " " << s[j] << endl;	
	if (sequences->isWatsonCrick(i, j, guAllowed)) {  // must be complementary
	  if ((i == 0) || ((j+1) >= s.size()) || (seqId[i] != seqId[i-1]) || (seqId[j+1] != seqId[j]) || (!sequences->isWatsonCrick(i-1, j+1, guAllowed))) {  // must be beginning of stem
	    // if (((i == 0) || (seqId[i] != seqId[i-1]) ||  ((j+1) >= static_cast<int>(s.size()))
	    // || (!sequences->isWatsonCrick(s[i-1], s[j+1], guAllowed)))  // must be beginning of stem
	    if (interStrand || ((j - i) > LOOP_LEN_MIN)) {
	      // int k = 0;
	      int lastGoodK = -1;
	      for (int k = 0; ((i+k) < n) && (j >= k) && (i+k < j -k) && sequences->isWatsonCrick(i+k,j-k, guAllowed) && (seqId[i] == seqId[i+k]) && (seqId[j] == seqId[j-k])
		     && (interStrand || ((j-i-k-k) > LOOP_LEN_MIN)); ++k) {
		lastGoodK = k;
		if (!longestOnly) {
		  if ((k+1) >= minExtendability) {
		    Stem stem(i,j,k+1);
		    // stem.setEnergy(computeViennaStemEnergy(stem, s));
		    // if (stem.getEnergy() < HELIX_ENERGY_MAX) {
THROWIFNOT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1,"Assertion violation in L1922");
		    // Rcpp::Rcout << "Created stem: " << stem << endl;
THROWIFNOT(sequences->isWatsonCrick(stem, guAllowed),"Assertion violation in L1924");
		    if ((stem.getLength() >= static_cast<int>(minExtendability))
			&& startStructure.isPlaceable(stem)) {
		      result.push_back(stem);
		      // Rcpp::Rcout << "Added stem to stem list: " << stem << " : " << minExtendability << endl;
		    } else {
		      // Rcpp::Rcout << "Not added stem to stem list: " << stem << " : " << minExtendability << " because it is not placeable" << endl;
		    }		    
		  }
		} 
	      } // for (int k = 0; ...

	      if (longestOnly) {
		if ((lastGoodK+1) >= minExtendability) {
		  Stem stem(i,j,lastGoodK+1);
		  // stem.setEnergy(computeViennaStemEnergy(stem, s));
		  // if (stem.getEnergy() < HELIX_ENERGY_MAX) {
THROWIFNOT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1,"Assertion violation in L1941");
		  // Rcpp::Rcout << "Created stem: " << stem << endl;
THROWIFNOT(sequences->isWatsonCrick(stem, guAllowed),"Assertion violation in L1943");
		  if ((stem.getLength() >= static_cast<int>(minExtendability))
		      && startStructure.isPlaceable(stem)) {
		    result.push_back(stem);
		    // Rcpp::Rcout << "Added stem to stem list: " << stem << " : " << minExtendability << endl;
		  } else {
		    // Rcpp::Rcout << "Not added stem to stem list: " << stem << " : " << minExtendability << " because it is not placeable" << endl;
		  }		    
		}
	      } 
	      
	    }
	  }
	}
      }
    }
    return result;
}
 */

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const string& s1, const string& s2)
{
  unsigned int n = s1.size();
  if (s2.size() < s1.size()) {
    n = s2.size();
  }
  if (n == 0) {
    return 0.0;
  }
  unsigned int good = 0;
  for (unsigned int i = 0; i < n; ++i) {
    if ((s1[i] == s2[i])
	&& (s1[i] != '.')
	&& (s2[i] != '-')) {
      ++good;
    }
  }
  return good / static_cast<double>(n);
}

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const Vec<string>& s)
{
  return pairwiseSequenceIdentity(s[0], s[1]);
}

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const string& s1, const string& s2,
			 const Vec<string>& matchRules)
{
  unsigned int n = s1.size();
  if (s2.size() < s1.size()) {
    n = s2.size();
  }
  if (n == 0) {
    return 0.0;
  }
  unsigned int good = 0;
  for (unsigned int i = 0; i < n; ++i) {
    for (unsigned int j = 0; j < matchRules.size(); ++j) {
THROWIFNOT(matchRules[j].size() == 2,"Assertion violation in L2007");
      if (((s1[i] == matchRules[j][0])
	   && (s2[i] == matchRules[j][1]))
	  ||
	  ((s1[i] == matchRules[j][1])
	   && (s2[i] == matchRules[j][0]) ) ) {
	++good;
	break;
      }
    }
  }
  return good / static_cast<double>(n);
}

/** returns fraction of pairwise sequence identity */
double
pairwiseSequenceIdentity(const Vec<string>& s,
			 const Vec<string>& matchRules)
{
  return pairwiseSequenceIdentity(s[0], s[1], matchRules);
}

/** returns index of stem in stem list that is similar (defined by slack)
    to query stem */
unsigned int
findSimilarStem(const Stem& query,
		const Vec<Stem>& stems,
		unsigned int slack)
{
  for (unsigned int i = 0; i < stems.size(); ++i) {
    if (query.isSimilar(stems[i], slack)) {
      return i;
    }
  }
  return stems.size();
}

/** position of relPos' character in string (skip gaps)
    counting start from zero both relative and absolute
*/
unsigned int
convert2AbsolutePositions(unsigned int relPos,
			  const string& s,
			  char gapChar)
{
  unsigned int charCounter = 0;
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (charCounter >= relPos) {
      return i;
    }
    if (s[i] != gapChar) {
      ++charCounter;
    }
  }
  return s.size(); // weird case! no relPos characters found!
}


/** input vec: n'th character before gaps and gap-length,
    output vec: absolute position of first gap character
    (or character to right of gap before insert)
*/
void
convert2AbsoluteGaps(Vec< pair<unsigned int, unsigned int> >& gaps,
		     const string& s,
		     char gapChar)
{
  Vec<unsigned int> posVec(s.size());
  unsigned int charCounter = 0;
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (s[i] != gapChar) {
      posVec[charCounter++] = i;
    }
  }
  for (unsigned int i = 0; i < gaps.size(); ++i) {
    gaps[i].first = posVec[gaps[i].first] + 1;
  }
}


/** read probability matrix which is in format of output postscript file form RNAfold of Vienna package! */
Vec<RankedSolution5<unsigned int, unsigned int> >
readRnaFoldProbFile(istream& is, unsigned int gapChar)
{
  string line;
  // determine sequence length first:
  Vec<RankedSolution5<unsigned int, unsigned int> > result;
  while (is) {
    line = getLine(is);
    if (line.find("/sequence") == 0) {
      break; // next line must be real sequence
    }
  }
  string sequenceOrig = getLine(is);
  sequenceOrig = removeFromString(sequenceOrig, '\\');
  while (is) {
    line = getLine(is);
    if (line.find("def") < line.size()) {
      break;
    }
    line = removeFromString(line, '\\');
    sequenceOrig = sequenceOrig + line;
  }
  string sequence = sequenceOrig;
  removeFromString(sequence, gapChar);
  while (is) {
    line = getLine(is);
    // Rcpp::Rcout << "Reading line " << line << endl;
    vector<string> words = getTokens(line);
    if ((words.size() == 4) && (words[3].compare("ubox") == 0)) {
      unsigned int id1 = Stoui(words[0]) - 1; // internal counting starts at zero
      id1 = convert2AbsolutePositions(id1, sequenceOrig, gapChar);
      unsigned int id2 = Stoui(words[1]) - 1; // internal counting starts at zero
      id2 = convert2AbsolutePositions(id2, sequenceOrig, gapChar);
      double prob = Stod(words[2]);
      prob = prob * prob; // square root is stored
      ERROR_IF((id1 >= sequence.size()) || (id2 >= sequence.size()),
	       "Too large box index encountered while reading probability matrix file!");
      result.push_back(RankedSolution5<unsigned int, unsigned int>(prob, id1, id2));
    }
  }  
  return result;
}

/** read RNA contact probability in our format
  list of contacts (residue1, residue2, probability). Counting starts from one externally,
  but from zero internally. */
Vec<RankedSolution5<unsigned int, unsigned int> >
readProbFile(istream& is)
{
  // determine sequence length first:
  Vec<RankedSolution5<unsigned int, unsigned int> > result;
  unsigned int numEntries = 0;
  unsigned int id1 = 0;
  unsigned int id2 = 0;
  double prob = 0.0;
  is >> numEntries;
  for (unsigned int i = 0; i < numEntries; ++i) {
    cin >> id1 >> id2 >> prob;
    --id1;
    --id2;
    // Rcpp::Rcout << "Reading line " << line << endl;
    // prob = prob * prob; // square root is stored
    result.push_back(RankedSolution5<unsigned int, unsigned int>(prob, id1, id2));
  }  
  return result;
}

/** sequence corresponds to sequence without gaps! */
Vec<RankedSolution5<unsigned int, unsigned int> >
readProbFromRegionFile(istream& is, unsigned int nSize)
{
  Vec<Vec<Stem> > multiStems = readMultiStems(is); // read all solutions
  ERROR_IF(multiStems.size() == 0,
	   "No stems read in readProbFromRegionFile!");
  // Rcpp::Rcout << "Read multi stems: " << multiStems << endl;
  // generate one matrix for each solution file:
  double stemWeight = 1.0 / multiStems.size();
  Vec<Vec<double> > matrix(nSize, Vec<double>(nSize, 0.0));
  for (unsigned int i = 0; i < multiStems.size(); ++i) {
    if (multiStems[i].size() == 0) {
      Rcpp::Rcout << "Warning: no stems for strucure: " << i + 1 << endl;
    }
    addStemsToMatrix(matrix, multiStems[i], stemWeight);
  }
  // Rcpp::Rcout << "The intermediate contact matrix is: " << endl << matrix << endl;
  Vec<RankedSolution5<unsigned int, unsigned int> > result;
  for (unsigned int i = 0; i < matrix.size(); ++i) {
    for (unsigned int j = i+1; j < matrix[i].size(); ++j) {
      if (matrix[i][j] > 0.0) {
	// Rcpp::Rcout << "Adding " << matrix[i][j] << " " << i << " " << j << endl;
	result.push_back(RankedSolution5<unsigned int, unsigned int>(
					     matrix[i][j], i, j));
      }
    }
  }
  return result;
}

/*
Vec<Vec<double> >
secToMatrix(const string& sec, double val)
{
  stack<unsigned int> stack;
  Vec<Vec<double> > matrix(sec.size(), Vec<double>(sec.size(), 0.0));
  unsigned int j;
  for (unsigned int i = 0; i < sec.size(); ++i) {
    switch (sec[i]) {
    case '(': stack.push(i);
THROWIFNOT(stack.top() == i,"Assertion violation in L2196");
      break;
    case ')': 
      ERROR_IF(stack.size() == 0,
	       "Illegal secondary structure!");
      j = stack.top();
      stack.pop();
      matrix[i][j] = val;
      matrix[j][i] = val;
      break;
    case '.':
      break; // do nothing
    case '-':
      break; // do nothing
    default:
      Rcpp::Rcout << ":" << sec[i] << ":" << endl;
      DERROR("Illegal character in secondary structure found.");
      break;
    }
  }
  return matrix;
}
*/

/** convert bracket notation to contact matrix, d1 and d2 are a pair of delimiters like ( and ) or [ and ] */
Vec<Vec<double> >
secToMatrix(const string& sec, double val, char d1, char d2)
{
  stack<unsigned int> stack;
  Vec<Vec<double> > matrix(sec.size(), Vec<double>(sec.size(), 0.0));
  unsigned int j;
  // check if delimiter in correct order, otherwise swap:
  for (unsigned int i = 0; i < sec.size(); ++i) {
    if (sec[i] == d1) {
      break; // correct order, do nothing
    }
    else if (sec[i] == d2) { // wrong order, swap
      char help = d1;
      d1 = d2;
      d2 = help;
      break; 
    }
  }
  for (unsigned int i = 0; i < sec.size(); ++i) {
    //     switch (sec[i]) {
    if (sec[i] == d1) {
      // case '(': 
      stack.push(i);
THROWIFNOT(stack.top() == i,"Assertion violation in L2244");
    }
    else if (sec[i] == d2) {
      ERROR_IF(stack.size() == 0,
	       "Illegal secondary structure!");
      j = stack.top();
      stack.pop();
      matrix[i][j] = val;
      matrix[j][i] = val;
    }
    else if ((sec[i] == '.') || (sec[i] == '-')) {
      // do nothing
    }
    else {
      Rcpp::Rcout << ":" << sec[i] << ":" << endl;
      DERROR("Illegal character in secondary structure found.");
    }
  }
  if (stack.size() != 0) {
    Rcpp::Rcout << "Warning: not matching set of delimiters in secondary structure: "
	 << d1 << " " << d2 << " " << sec << endl;
  }
  return matrix;
}



/** convert all characters of s, that are not part of mask string into default char. 
    Variable found is true, if at least one character of mask was found in s */
string
applyMask(const string& s, const string& mask, char defaultChar, bool& found)
{
  string result = s;
  found = false;
  for (unsigned int i = 0; i < s.size(); ++i) {
    if (mask.find(s[i]) >= mask.size()) {
      result[i] = defaultChar;
    }
    else {
      found = true;
    }
  }
  return result;
}

/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] */
Vec<Vec<double> >
secToMatrix(const string& sec, double val, unsigned int& pkCounter)
{
  Vec<string> allowedPairs(8);
  allowedPairs[0] = "()";
  allowedPairs[1] = "[]";
  allowedPairs[2] = "<>";
  allowedPairs[3] = "{}";
  for (char c = 'A'; c <= 'Z'; ++c) {
    char c1 = c;
    char c2 = tolower(c1);
    string s = "AA";
    s[0] = c1;
    s[1] = c2;
THROWIFNOT(s.size() == 2,"Assertion violation in L2304");
THROWIFNOT(s[0] != s[1],"Assertion violation in L2305");
    allowedPairs.push_back(s);
  }
//   allowedPairs[3] = "aA";
//   allowedPairs[4] = "bB";
//   allowedPairs[5] = "cC";
//   allowedPairs[6] = "dD";

  char defaultChar = '-';
  bool found = false;
  pkCounter = 0;
  string maskedSec = applyMask(sec, allowedPairs[0], defaultChar, found);
  Vec<Vec<double> > result;
  if (found) {
//     Rcpp::Rcout << "Using masked secondary structure: " << allowedPairs[0] << " "
// 	 << maskedSec << endl;
    result = secToMatrix(maskedSec, val, allowedPairs[0][0], allowedPairs[0][1]);
    pkCounter++;
  }
  else {
    result = Vec<Vec<double> >(sec.size(), Vec<double>(sec.size(), 0.0));
  }
  for (unsigned int i = 1; i < allowedPairs.size(); ++i) {
    maskedSec = applyMask(sec, allowedPairs[i], defaultChar, found);
    if (found) {
      ++pkCounter;
//       Rcpp::Rcout << "Using masked secondary structure: " << allowedPairs[0] << " "
// 	   << maskedSec << endl;
      Vec<Vec<double> > tmpMatrix = secToMatrix(maskedSec, val, 
				allowedPairs[i][0], allowedPairs[i][1]);
      ERROR_IF(tmpMatrix.size() != result.size(), "Internal error in line 1128!");
      for (unsigned int i = 0; i < tmpMatrix.size(); ++i) {
	for (unsigned int j = 0; j < tmpMatrix[i].size(); ++j) {
	  if (tmpMatrix[i][j] >= val) {
	    result[i][j] = val;
	  }
	}
      }
    }
  }
//   if (pkCounter > 1) {
//     Rcpp::Rcout << "Pseudo-knot detected in reading bracket notation string: "
// 	 << sec << endl;
//   }
  return result;
}


  /** convert bracket notation to contact matrix, d1 and d2 are a pair of delimiters like ( and ) or [ and ] */
  Vec<Vec<double> > secToMatrix2Char(const string& sec, double val, char d) {
    stack<unsigned int> stack;
    Vec<Vec<double> > matrix(sec.size(), Vec<double>(sec.size(), 0.0));
    vector<pair<string::size_type, string::size_type> > regions = getRegions(sec, d);
    if (regions.size() == 2) {
      ERROR_IF(regions[0].second != regions[1].second,
	       "Region pair has different lengths!");
    }
    string::size_type len = regions[0].second;
    string::size_type start = regions[0].first;
    string::size_type stop = regions[1].first + regions[1].second - 1;
    for (string::size_type i = 0; i < len; ++i) {
      matrix[start + i][stop - i] = 1.0;
      matrix[stop-i][start + i] = 1.0;
    }
    return matrix;
  }
 
/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] or ---AAA--AAA---*/
Vec<Vec<double> > secToMatrix2(const string& sec, double val, unsigned int& pkCounter)
{
  Vec<string> allowedPairs(4);
  allowedPairs[0] = "()";
  allowedPairs[1] = "[]";
  allowedPairs[2] = "<>";
  allowedPairs[3] = "{}";
  for (char c = 'A'; c <= 'Z'; ++c) {
    char c1 = c;
    char c2 = c1;  // do not convert to lower case in secToMatrix2
    string s = "AA";
    s[0] = c1;
    s[1] = c2;
THROWIFNOT(s.size() == 2,"Assertion violation in L2386");
    allowedPairs.push_back(s);
  }
  //   allowedPairs[3] = "aA";
  //   allowedPairs[4] = "bB";
  //   allowedPairs[5] = "cC";
  //   allowedPairs[6] = "dD";
  
  char defaultChar = '-';
  bool found = false;
  pkCounter = 0;
  Vec<Vec<double> > result(sec.size(), Vec<double>(sec.size(), 0.0));
  for (unsigned int i = 0; i < allowedPairs.size(); ++i) {
    // Rcpp::Rcout << "Trying " << allowedPairs[i] << " on structure " << sec << endl;
    string maskedSec = applyMask(sec, allowedPairs[i], defaultChar, found);
    if (found) {
      ++pkCounter;
      Rcpp::Rcout << "Using masked secondary structure: " << allowedPairs[0] << " "
 	   << maskedSec << endl;
      Vec<Vec<double> > tmpMatrix;
      if (allowedPairs[i][0] != allowedPairs[i][1]) {
	tmpMatrix = secToMatrix(maskedSec, val, 
				allowedPairs[i][0], allowedPairs[i][1]);
      } else {
	tmpMatrix = secToMatrix2Char(maskedSec, val, allowedPairs[i][0]);
      }
      ERROR_IF(tmpMatrix.size() != result.size(), "Internal error in line 1128!");
      for (unsigned int i = 0; i < tmpMatrix.size(); ++i) {
	for (unsigned int j = 0; j < tmpMatrix[i].size(); ++j) {
	  if (tmpMatrix[i][j] >= val) {
	    result[i][j] = val;
	  }
	}
      }
    } else {
      // Rcpp::Rcout << "Delimiter not found: " << allowedPairs[i] << endl;
    }
  }
//   if (pkCounter > 1) {
//     Rcpp::Rcout << "Pseudo-knot detected in reading bracket notation string: "
// 	 << sec << endl;
//   }
  return result;
}


/** convert bracket notation to contact matrix, can handle many different delimiters simultaneously like ( and ) or [ and ] */
/*
Vec<Vec<double> >
secToMatrix(const string& sec, double val)
{ 
  unsigned int pkCounter = 0;
  return secToMatrix(sec, val, pkCounter);
}
*/

/** convert stems to matrix */
Vec<Vec<double> >
placeStemValueInMatrix(Vec<Vec<double> > & matrix, const Stem& stem, double value) 
{
THROWIFNOT(matrix.size() > 0,"Assertion violation in L2446");
  ASSERT(matrix.size() == matrix[0].size()); // square matrix
  for (int j = 0; j < stem.getLength(); ++j) {
    int nx = stem.getStart() + j;
    int ny = stem.getStop() - j;
THROWIFNOT(nx < static_cast<int>(matrix.size()),"Assertion violation in L2451");
THROWIFNOT(ny < static_cast<int>(matrix[0].size()),"Assertion violation in L2452");
    matrix[nx][ny] = value;
    matrix[ny][nx] = value;
  }
  return matrix;
}

/** convert stems to matrix */
Vec<Vec<double> >
generateMatrixFromStems(const Vec<Stem>& stems, unsigned int nSize)
{
THROWIFNOT(nSize > 0,"Assertion violation in L2463");
  Vec<Vec<double> > matrix(nSize, Vec<double>(nSize, 0.0));
  for (unsigned int i = 0; i < stems.size(); ++i) {
    for (int j = 0; j < stems[i].getLength(); ++j) {
      int nx = stems[i].getStart() + j;
      int ny = stems[i].getStop() - j;
      // ASSERT(nx < static_cast<int>(nSize));
      // ASSERT(ny < static_cast<int>(nSize));
      if ((nx < static_cast<int>(nSize))
	  && (ny < static_cast<int>(nSize))) { 
	matrix[nx][ny] = 1.0;
	matrix[ny][nx] = 1.0;
      } else {
	Rcpp::Rcout << "# Warning Weird stem: " << stems[i] << " for size " << nSize
	     << endl;
      }
    }
  }
  return matrix;
}

/** convert set of set of stems to matrix. Used for computing consensus secondary structure. */
Vec<Vec<double> >
generateAverageMatrixFromStems(const Vec<Vec<Stem> >& stems, unsigned int nSize)
{
  Vec<Vec<double> > matrix(nSize, Vec<double>(nSize, 0.0));
  double weight = 1.0 / static_cast<double>(stems.size());
  for (unsigned int i = 0; i < stems.size(); ++i) {
    addStemsToMatrix(matrix, stems[i], weight);
  }
  return matrix;
}

Vec<Stem>
stemsFromBracketFasta(istream& is, unsigned int& pkCounter, unsigned int& length)
{
  string sec = readFasta(is);
THROWIFNOT(sec.size() > 0,"Assertion violation in L2500");
  // Rcpp::Rcout << "Read secondary string: :" << sec << ":" << endl;
  Vec<Vec<double> > matrix = secToMatrix(sec, 1.0, pkCounter);
  length = matrix.size();
  // Rcpp::Rcout << "Matrix " << matrix << endl;
  // writeMatrix(Rcpp::Rcout, matrix);
  string dummy; // empty sequence
  Vec<Stem> result = generateStemsFromMatrix(matrix, 0, 0.9, dummy); 
  return result;
}

Vec<Stem>
stemsFromBracketFasta(istream& is, unsigned int& pkCounter)
{
  unsigned int length;
  return stemsFromBracketFasta(is, pkCounter, length);
}

Vec<Stem>
stemsFromBracketFasta(const string& sec, unsigned int& pkCounter, unsigned int& length)
{
  PRECOND(sec.size() > 0);
  // Rcpp::Rcout << "Read secondary string: " << sec << endl;
  Vec<Vec<double> > matrix = secToMatrix(sec, 1.0, pkCounter);
  length = matrix.size();
  // Rcpp::Rcout << "Matrix " << matrix << endl;
  writeMatrix(Rcpp::Rcout, matrix);
  string dummy; // empty sequence
  Vec<Stem> result = generateStemsFromMatrix(matrix, 0, 0.9, dummy); 
  return result;
}

Vec<Stem>
stemsFromBracketFasta(const string& sec, unsigned int& pkCounter)
{
  PRECOND(sec.size() > 0);
  unsigned int length;
  return stemsFromBracketFasta(sec, pkCounter, length);
}

/* checks if sequence has gaps */
string
stemToBracketFasta(const Stem& stem,
		   const string& sOrig,
		   const string& sequence)
{
  PRECOND(sOrig.size() == sequence.size());
  string s = sOrig;
  for (int i = 0; i < stem.getLength(); ++i) {
    int first = stem.getStart()+i;
    int second = stem.getStop()-i;
    if ((sequence[first] != '-') && (sequence[first] != '.')
	&& (sequence[second] != '-') && (sequence[second] != '.')) {
      s[first] = '(';
      s[second] = ')';
    }
  }
  return s;
}

/** returns true if these two stems form a pseudoknot */
bool
isStemConflicting(const Stem& stem1,
		  const Stem& stem2)
{
  bool sit1 = (stem2.getStart() > stem1.getStart()) && (stem2.getStart() < stem1.getStop()) && (stem2.getStop() > stem1.getStop());
  bool sit2 = (stem1.getStart() > stem2.getStart()) && (stem1.getStart() < stem2.getStop()) && (stem1.getStop() > stem2.getStop());
  return sit1 || sit2;
}

/** returns true if this stem corresponds to a pseudoknot 
 * with respect to stems 0 to n-1
 */
bool
isPseudoKnotStem(const Vec<Stem>& stem, unsigned int n)
{
  for (unsigned int i = 0; i < n; ++i) {
    if (isStemConflicting(stem[i],stem[n])) {
      return true;
    }
  }
  return false;
}

/* write brackets to string. do not check if sequence has gaps */
string
stemToBracketFasta(const Stem& stem, const string& sOrig)
{
  string s = sOrig;
  for (int i = 0; i < stem.getLength(); ++i) {
    int first = stem.getStart()+i;
    int second = stem.getStop()-i;
    s[first] = '(';
    s[second] = ')';
  }
  return s;
}

/* write brackets to string. do not check if sequence has gaps */
string
stemToBracketFasta(const Stem& stem, const string& sOrig, char pseudoChar)
{
  string s = sOrig;
  for (int i = 0; i < stem.getLength(); ++i) {
    int first = stem.getStart()+i;
    int second = stem.getStop()-i;
    s[first] = pseudoChar;
    s[second] = pseudoChar;
  }
  return s;
}

/** givens stems and sequence, return bracket notation. Checks for gaps. */
string
stemsToBracketFasta(const Vec<Stem>& stemsOrig, const string& sequence)
{
  PRECOND(sequence.size() > 0);
  Vec<Stem> stems = stemsOrig; // copy! use because internal sorting is done
  sort(stems.begin(), stems.end(), StemLengthComparator());
  reverse(stems.begin(), stems.end()); // longest stem first
  string result(sequence.size(), '-');
  char pseudoChar = 'A'; // use in case of pseudoknot
  for (unsigned int i = 0; i < stems.size(); ++i) {
    if (isPseudoKnotStem(stems, i)) {
      result = stemToBracketFasta(stems[i], result, pseudoChar);
      pseudoChar++;
    }
    else {
      result = stemToBracketFasta(stems[i], result);
    }
  }
  return result;
}

/** givens stems and sequence, return bracket notation.
 * Might involve pseudoknot notation .
 */
string
stemsToBracketFasta(const Vec<Stem>& stems, unsigned int len)
{
  PRECOND(len > 0);
  string sequence(len, '-');
  return stemsToBracketFasta(stems, sequence);
}

Vec<Vec<Stem> >
readRNAsubopt(istream& is, Vec<double>& scores, string& sequence)
{
  Vec<Vec<Stem> > result;
  vector<string> words;
  string line;
  unsigned int pkCounter = 0;
  unsigned int lineCounter = 0;
  while (is) {
    line = getLine(is);
    ++lineCounter;
    if ((lineCounter == 1) && (line[0] == '>')) {
      continue;
    }
    words = getTokens(line);
    if ((words.size() == 3) && (lineCounter < 3)) {
      // Rcpp::Rcout << "Skipping : " << line << endl;
      sequence = words[0];
    }
    if (words.size() != 2) {
      // Rcpp::Rcout << "Skipping : " << line << endl;
      continue;
    }
    scores.push_back(Stod(words[1]));
    result.push_back(stemsFromBracketFasta(words[0], pkCounter));
  }
  return result;
}

void
addStemToMatrix(Vec<Vec<double> >& compMatrix,
		 const Stem& stem,
		 double stemWeight)
{
  int x1 = stem.getStart();
  int y1 = stem.getStop();
  int sz = compMatrix.size();
  for (int i = 0; i < stem.getLength(); ++i) {
    int x = x1 + i;
    int y = y1 - i;
    if ( (x >= sz)
      || (y < 0) 
      || (y >= sz) 
      || (x < 0) ) {
      Rcpp::Rcout << "Warning: illegal stem position (ignoring): " << stem 
	   << " " << sz << endl;
      break;
    }
    compMatrix[x][y] += stemWeight;
    compMatrix[y][x] += stemWeight;
  }
}

void
addStemsToMatrix(Vec<Vec<double> >& compMatrix,
		 const Vec<Stem>& referenceStems,
		 double stemWeight)
{
  for (unsigned int i = 0; i < referenceStems.size(); ++i) {
    addStemToMatrix(compMatrix, referenceStems[i], stemWeight);
  }
}

void
entropyStemList(ostream& os, 
		const Vec<Vec<double> >& mat, 
		const string& sequence,
		const Vec<string>& allowedPairs,
		double scoreLimit,
		unsigned int minStemLength,
		double scoreWeight,
		double confidence)
{
  PRECOND((confidence >= 0.0) && (confidence < 1.0));
  ERROR_IF(allowedPairs.size() == 0, "No allowed pairs specified!");
  int x = 0;
  int y = 0;
  Stem stem;
  char c1, c2;
  int stemCounter = 0;
  double countCells = static_cast<double>((sequence.size()*(sequence.size()-3)))/2.0;
  double p0 = 0.7 * sequence.size() / countCells;
  double p0w = (1.0-confidence) * p0;
  double lP0w = log(p0w);
  double bestTerm, energy;
  for (int i = 0; i < static_cast<int>(mat.size()); ++i) {
    for (int j = i + 3; j < static_cast<int>(mat.size()); ++j) {
      //      double sum = 0;
      double term;
      for (int k = 0; k <= j; ++k) {
	x = i + k;
	y = j - k;
	if ((x >= static_cast<int>(mat.size())) || (y < 0) || ((x+2) >= y)) {
	  break;
	}
	bool found = false;
	c1 = sequence[x];
	c2 = sequence[y];
	for (unsigned int i2 = 0; i2 < allowedPairs.size(); ++i2) {
	  if (((allowedPairs[i2][0] == c1) && (allowedPairs[i2][1] == c2))
	    || ((allowedPairs[i2][0] == c2) && (allowedPairs[i2][1] == c1)) ) {
	    found = true;
	    break;
	  }
	}
	if (!found) {
	  break; // pair not allowed
	}
	if (mat[x][y] >= 0.0) {
	  term = - log(p0w + confidence * mat[x][y]);
	  //  sum +=  term;
	}
	else {
	  term = - lP0w;
	  // sum += term;
	}
	if ((k == 0) || (term < bestTerm)) {
	  bestTerm = term;
	}
	if ((k > 0) && (k >= static_cast<int>(minStemLength))) {
	    // && (bestTerm < lP0w)) {
	  // && 	(sum >= scoreLimit) ){
	  energy = (bestTerm + lP0w) * scoreWeight;
	  if (fabs(energy) > 1e-3) {
	    stem.setStart(i);
	    stem.setStop(j);
	    stem.setLength(k);
	    stem.setEnergy(energy);
	    writeStem(os, stem, stemCounter++);
	    os << endl;
	  }
	}
      }
    }
  }
  if (stemCounter == 0) {
    Rcpp::Rcout << "Warning: no entropy stems written!" << endl;
  }
}

/** removes nucleotide, adjusts stems accordingly */
void
deletePositionInStems(Vec<Stem>& stems, 
		      string sequence, unsigned int delPos)
{
  PRECOND(sequence.size() > 1);
  Vec<int> connections = convertStemsToConnections(stems, sequence.size());
  for (unsigned int j = 0; j < connections.size(); ++j) {
    if (connections[j] == static_cast<int>(delPos)) {
      connections[j] = -1; // delete that contact
    }
    else if (connections[j] > static_cast<int>(delPos)) {
      --connections[j]; // reduce index by one, because shift due to delete
    }
  }
  connections.erase(connections.begin()+delPos); // remove that entry
  unsigned int minStemLength = 1;
  sequence.erase(delPos, 1);
  stems = convertConnectionsToStems(connections,
				    minStemLength, sequence);
}

/** inserts gap *at position*, adjusts stems accordingly. Indices with rank equal or higher pos are increase by one */
void
insertGapInStems(Vec<Stem>& stems, 
		      string& sequence,
		      unsigned int insPos)
{
  PRECOND(sequence.size() > 1);
  // this corresponds to a CT file:
  Vec<int> connections = convertStemsToConnections(stems, sequence.size());
  for (unsigned int j = 0; j < connections.size(); ++j) {
    if ((connections[j] >= 0) && (connections[j] >= static_cast<int>(insPos))) {
      ++connections[j]; // increase index by one
    }
  }
  int noConnectionVal = -1;
  connections.insert(connections.begin() + insPos, noConnectionVal); // remove that entry
  unsigned int minStemLength = 1;
  sequence.insert(insPos, 1, '-');
  stems = convertConnectionsToStems(connections, minStemLength, sequence);
}

bool
isPosInStem(int x, int y, const Stem& stem)
{
  if (y < x) {
    int help = y;
    y = x;
    x = help;
  }
  int start = stem.getStart();
  int stop = stem.getStop();
  int len = stem.getLength();
  int px, py;
  for (int k = 0; k < len; ++k) {
    px = start + k;
    py = stop - k;
    if ((x == px) && (y == py)) {
      return true;
    }
  }
  return false;
}

bool
isPosInStems(int x, int y, const Vec<Stem>& stems)
{
  if (y < x) {
    int help = y;
    y = x;
    x = help;
  }
  for (unsigned int i = 0; i < stems.size(); ++i) {
    if (isPosInStem(x, y, stems[i])) {
      return true;
    }
  }
  return false;
}

/** returns number of base pairs */
unsigned int
countNumBasePairs(const Vec<Stem>& stems) {
  unsigned int result = 0;
  for (unsigned int i = 0; i < stems.size(); ++i) {
    result += stems[i].getLength();
  }
  return result;
}


#endif
