#ifndef STRENUM9_MAIN_H
#define STRENUM9_MAIN_H

#include <Rcpp.h> // for R binding

#include <iostream>
#include <fstream>
#include <string>
#include "Vec.h"
#include "ResiduePairing.h"
#include "ResidueStructure.h"
#include "RnaFormats.h"
#include "MultiIterator.h"
#include "ScoredPath.h"
#include "HelixCompatibilityChecker.h"
#include "ResiduePairingStemSolution.h"
#include "BasePairStateFeature.h"
#include "state_t.h"
#include "ResidueStructureRcppTools.h"

using namespace std;
using namespace Rcpp;

/** Class representing the main application of the strenum binary. Strenum stands for Structure Enumeration (of RNA structures).
 */
class Strenum9 {
  
public:
  
  typedef Stem::index_type index_t;
  typedef unsigned int digit_t;
  typedef ResiduePairingStemSolution::id_type id_type;
  // typedef Vec<digit_t> state_t;
  typedef ResiduePairing::basepair_t basepair_t;
  
  enum { HELIX_COUNT_MAX = 5000, QUEUE_SIZE_MAX_DEFAULT=10 }; // 62 };
  
  enum { HELIX_SORT_NONE=0, HELIX_SORT_ENERGY=1, HELIX_SORT_INTRA=2, HELIX_SORT_INTRA2=3, HELIX_SORT_MODE_MAX=4 };
  
public:
  
    string GARBAGE_FILE_NAME = "/dev/null";
  
private:
  
  set<string> allowedConnectivities;
  size_t bpLeeMax;
  int bpOutMode; // if not zero, output of base pairing of state
  int bpSlop;
  int branchSlop;
  double conc ;
  size_t crossingMax;
  bool distanceMode;
  double energyBarrierMax;
  double entropyWeight;
  size_t expandedHelicesMax;
  bool extendHelixMode;
  Vec<BasePairStateFeature> features;
  // double gibbsLim;
  double gCorrect;
  bool graphCheckMode;
  bool guAllowed;
  bool guEndAllowed;
  size_t helixCountSlop;
  double helixDnaGAdjust;
  double helixRnaGAdjust;
  int helixSortMode;
  double interStrandPenalty;
  int kineticsMode;
  size_t kineticStemExploreMax;
  int meltingMode;
  ResiduePairing minHStructure;
  ResiduePairing minGStructure;
  Vec<double> concentrations;
  unsigned int multiplicity;
  // string modelFile; //  = "../../prm/resgeom/bead1ResidueModel.prm";
  int forcedAutoMode;
  Vec<ResiduePairing::basepair_t> forcedPairs;
  Vec<Vec<state_t> > foundPaths;
  int helixLengthMin;
  int helixLengthMin2;
  // int helixLengthMin3;
  int helixSortIntra2Length;
  string inputFileName;
  int lengthModulo;
  ResidueModel modelG;
  ResidueModel modelH;
  int optimizationMode;
  size_t optRounds;
  string outFileBase;
  size_t pathReportMax;
  bool placeSmallHelicesMode;
  int pseudoknotMode;
  size_t queueSizeMax;
  double reactionRadius;
  int saveModeInt;
  Vec<string> sequences; // sequences
  // Vec<int> idVec;
  state_t leadDigitConstraint;
  vector<string> requiredFileNames;
  double simThreshold;
  double smallStemDGCutoff;
  bool sortStatesMode;
  string stackingFileG;// = "MatthewsSantaLuciaSugimotoGibbs.prm"; 
  string stackingFileH;// = "MatthewsSantaLuciaSugimoto.prm"; 
  // string stackingFile = "../../prm/resgeom/StackingParametersLindsay12_2012.dat"; // stack_r4.prm";
  // string stackingFile = "../../prm/resgeom/Marky1982.dat"; // stack_r4.prm";
  string strenumFileName;
  vector<string> structureFileNames;
  size_t subStateMax; // upper limit on number of sub-states that can be analyzed with shortest-path algorithm
  size_t suboptWriteMax;
  double temperatureC; // temperature in Celsius
  double temperatureC2; // temperature in Celsius
  int verbose;
  bool writeFileMode; // write to file system only if true
  string version;
  
public:
  
  Strenum9() { init(); }
  
  Rcpp::List main(const Vec<string>& args, const Vec<string>& sequences, const Vec<double>& concentrations, const string& homeDir);
  
  Rcpp::List main(int argc, char ** argv, const Vec<string>& sequences, const Vec<double>& concentrations, const string& homeDir);
  
  void init() {
    allowedConnectivities.clear();
    bpLeeMax = 2;
    bpOutMode = 1;
    bpSlop = 20;
    branchSlop = 40;
    reactionRadius = REACTION_RADIUS; // reaction radius from ResidueMoldel
    concentrations.clear();
    conc = 1.0; // default concentration in units of micromol per liter
    crossingMax = 1; // for each stem, allow this many other stem (with same involved strands) to be "crossing"
    distanceMode = true;
    energyBarrierMax = 15.0; // maximim energy barrier height that can be overcome for kinetic paths
    entropyWeight = 0.4; // previously: 1.0;
    expandedHelicesMax = 4;
    extendHelixMode = true;
    forcedAutoMode = 0;
    forcedPairs.clear();
    // gibbsLim = 150.0; // do not print structures with Gibbs free energy higher than this number
    gCorrect = 0.0;
    graphCheckMode = true;
    guAllowed = true;
    guEndAllowed = true;
    helixSortMode = HELIX_SORT_ENERGY;
    inputFileName = "";
    interStrandPenalty = 0.0;
    helixCountSlop = 2; // transition states have at most this many more helices than any of the end states
    helixDnaGAdjust = 3.3; // the value of 3.3 leads to the best reproduction of melting temperatures of duplexes
    helixRnaGAdjust = 0.0;
    helixLengthMin = 6;
    helixLengthMin2 = 3; // 4;
    helixSortIntra2Length = 16; // used for HELIX_SORT_INTRA2 mode: interstrand-helices with at least this length will not automatically be outcompetet by intra-strand helices
    kineticsMode = 0; // if zero, do not perform kinetics analysis
    kineticStemExploreMax = 100; // 1: examine only lowest-energy stem; 10; top ten energy stems for each connectivity
    //    helixLengthMin3 = 3;
    lengthModulo=1;
    meltingMode = 0;
    multiplicity = 1;
    optimizationMode = 0; // default optimization mode 1;
    optRounds = 3; // 1; // this many rounds of local optimization
    outFileBase = "hyperfold_out"; 
    pathReportMax = 2; // at most one path per topology
    placeSmallHelicesMode = true;
    pseudoknotMode = ResidueModel::ALL_PSEUDOKNOTS;
    queueSizeMax = QUEUE_SIZE_MAX_DEFAULT;
    saveModeInt = 1;
    sequences.clear();
    smallStemDGCutoff = -4.0;
    // simThreshold = ResiduePairingClusters::SIM_DEFAULT_THRESHOLD;
    sortStatesMode = false;
    stackingFileG = "MatthewsSantaLuciaSugimotoGibbs.prm"; // subdir will be added later
    stackingFileH = "MatthewsSantaLuciaSugimoto.prm"; // subdir will be added later
    structureFileNames.clear();
    suboptWriteMax = 10;
    subStateMax = 10000;
    temperatureC = 37.0;
    temperatureC2 = 37.0;
    version = "hyperfold_Nov20_2018"; 
    // Mar4_2015: added option -mod for controlling number of generated helices
    // Mar25_2015: faster version using Graph2 instead of graph
    // June10_2015: added option --hsort for different modes of helix sorting
    // November20_2018: added verbosity (internal parameter verbose, arc should be "-v")
    verbose = VERBOSE_QUIET;
    writeFileMode = true;
  }
  
  const Vec<Vec<state_t> > getFoundPaths() const { return foundPaths; }
  
  ResiduePairing getMinHStructure() const { return minHStructure; }
  
  ResiduePairing getMinGStructure() const { return minGStructure; }
  
  // bool getEvalMode() const { return getEvalMode(); }
  
  int getOptimizationMode() const { return optimizationMode; }
  
  void setOptimizationMode(int mode) { optimizationMode = mode; }
  
  const string getVersion() const { return version; }
  
  bool isStateCompatible(const Vec<Vec<size_t> > occMatrix,
                         const state_t& state,
                         const Vec<Vec<Stem> >& subStems) const {
    for (size_t i = 0; i < state.size(); ++i) {
      if (state[i] > 0) {
        const Stem& stem = subStems[i][state[i]-1];
        for (int k = 0; k < stem.getLength(); ++k) {
          if (occMatrix[stem.getStart()+k][stem.getStop()-k] == 0) { 
            return false; // occupancy matrix should be one
          }
        }
      }
    }
    return true;
  }
  
  
  void setAllowedConnectivities(const set<string>& connectivities) {
    allowedConnectivities = connectivities;
  }
  
  /** Allow GU base pairs */
  void setGuAllowed(bool mode) {
    guAllowed = mode;
  }

  /** Allow GU base pairs at end of core stems */
  void setEndGuAllowed(bool mode) {
    guAllowed = mode;
  }

  // void setGibbsLim(double value) { gibbsLim = value; }
  
  /** Sets kinetics mode: if 0, then no kinetics analyis is performed; if 1, then the kinetics analysis is performed. */
  void setKineticsMode(int mode) { kineticsMode = mode; }
  
  ResidueModel& getModelG() { return modelG; }
  
  ResidueModel& getModelH() { return modelH; }
  
  /** Sets uniform concentration values in units of micromol per liter. Note that this concentration get internally converted into strands per Angstroem cubed during the main method. The value of the conc variable itself is, however, not changed during that conversion. */
  void setConc(double _concentration) { conc = _concentration; }
  
  void setCrossingMax(size_t _crossingMax) { crossingMax = _crossingMax; }
  
  void setEntropyWeight(double value) { entropyWeight = value; }

  void setExpandedHelicesMax(size_t value) {
    expandedHelicesMax = value;
  }
  
  void setHelixDnaGAdjust(double _gAdjust) { helixDnaGAdjust = _gAdjust; }
  
  void setHelixRnaGAdjust(double _gAdjust) { helixRnaGAdjust = _gAdjust; }
  
  void setGCorrect(double _gCorrect) { gCorrect = _gCorrect; }
  
  /** Points to sequences in external counting between which there is an initial "divider. For example, a "2" would indicate a divider between the first and second sequence (whose internal countin ids are 0 and 1). During the main routine, these ids get converted to internal counting */
  void setExtendHelixMode(bool mode) { extendHelixMode = mode; }
  
  void setHelixLengthMin(int _helixLengthMin) { helixLengthMin = _helixLengthMin; }
  
  void setHelixLengthMin2(int _helixLengthMin) { helixLengthMin2 = _helixLengthMin; }
  
  void setHelixSortMode(int sortMode) {
    PRECOND(sortMode >= 0);
    PRECOND(sortMode < HELIX_SORT_MODE_MAX);
    helixSortMode = sortMode;
  }
  
  /** This values sets the step width by which helices are being reduced in size */
  // void setHelixLengthMin3(int _helixLengthMin) { helixLengthMin3 = _helixLengthMin; }
  
  void setLeadDigitConstraint(const state_t& _leadDigitConstraint) {
    leadDigitConstraint = _leadDigitConstraint;
  }
  
  void setLengthModulo(int value) { lengthModulo = value; }
  
  void setInterStrandPenalty(double penalty) { interStrandPenalty = penalty; }
  
  void setForcedAutoMode(int mode) { forcedAutoMode = mode; }
  
  void setForcedPairs(const Vec<basepair_t>& pairs) {
    forcedPairs = pairs;
  }
  
  void setOutFileBase(const string& name) {
    outFileBase = name;
  }
  
  void setPlaceSmallHelicesMode(bool mode) { placeSmallHelicesMode = mode; }
  
  void setPseudoknotMode(int mode) {
    pseudoknotMode = mode;
    modelG.setPseudoknotMode(mode);
    modelH.setPseudoknotMode(mode);
  }
  
  void setQueueSizeMax(size_t max) { queueSizeMax = max; }
  
  void setReactionRadius(double _reactionRadius) {
    modelG.setReactionRadius(_reactionRadius);
    modelH.setReactionRadius(_reactionRadius);
  }
  
  double getReactionRadius() const {
    return modelG.getReactionRadius();
  }
  
  void setSaveModeInt(int mode) {
    saveModeInt = mode;
  }
  
  void setSequences(const Vec<string>& _sequences) {
    sequences = _sequences;
  }
  
  void setSimThreshold(double _simThreshold) {
    simThreshold = _simThreshold;
  }
  
  void setSmallStemDGCutoff(double value) { smallStemDGCutoff = value; }
  
  double getSmallStemDGCutoff() const { return smallStemDGCutoff; }
  
  void setStackingFileG(const string& filename) {
    stackingFileG = filename;
    std::ifstream stackingStream(stackingFileG.c_str());
    ERROR_IF(!stackingStream, "Error opening stacking file (option -p): " + stackingFileG);
    modelG.readStacking(stackingStream);
    stackingStream.close();
  }
  
  void setStackingFileH(const string& filename) {
    stackingFileH = filename;
    std::ifstream stackingStream(stackingFileH.c_str());
    ERROR_IF(!stackingStream, "Error opening stacking file (option -p): " + stackingFileH);
    modelH.readStacking(stackingStream);
    stackingStream.close();
  }
  
  
  /** Sets temperature in Celsius */
  void setTemperatureC(double temp) { temperatureC = temp; }
  
  void setVerbose(int v) { verbose = v; }
  
  Rcpp::List processArguments(int argc, char ** argv);
  
  ResiduePairing createResiduePairing(StrandContainer * strands, 
                                      ResidueModel * model,
                                      state_t state, 
                                      const Vec<Vec<Stem> >& subStems) const;
  
private:
  
  Vec<Stem> filterStemsByOccupancyMatrix(const Vec<Stem>& stems, const Vec<Vec<int> >& occMatrix, int badValue) const;
  
  Vec<Stem> filterStemsByRequiredStructures(const Vec<Stem>& stems, const vector<string>& filenames,
                                            const StrandContainer& strands, ResidueModel& model) const;
  
  ResidueStructure localOptimizationProtocoll(const ResidueStructure& structure, const Vec<Stem>& stems);
  
  bool structureSanityCheck(const Vec<Stem>& helices) const;
  
  bool structureSanityCheck(const ResiduePairing& structure) const { return structureSanityCheck(structure.getHelices()); }
  
  bool structureSanityCheck(const ResidueStructure& structure) const { return structureSanityCheck(structure.getHelices()); }
  
};

#include "ResidueModel.h"
#include <ostream>
#include "ConnectivityTools.h"
#include "verbose.h"
#include "GetArg.h"
#include "StrandContainer.h"
#include "ResiduePairingTools.h"
#include "ResidueStructureTools.h"
#include <string.h>
#include <cstdlib>
#include "MultiIterator.h"
#include "StemTools.h"
#include "Graph.h"
#include "StrenumTools.h"
#include "IOTools.h"
#include "vectornumerics.h"
#include "ResiduePairingTools.h"
#include "ResiduePairingStemSolution.h"
#include "ScoredPath.h"
#include "BasePairChangeScorer.h"
#include "HelixCompatibilityChecker.h"
#include "Timer.h"
// #include "PriorityStemScanner.h"
#include <map>
#include <limits>
// #include "IndyFold.h"
#include "ResidueStructureOptimizer.h"
#include "HelixStructureOptimizer.h"
#include "SingleHelixStructureOptimizer.h"

/** Handling function for excepion handling */
void term_func2() {
  Rcpp::Rcout << "# term_func was called by terminate." << endl;
}


/** Returns true, if shortest helix is not shorter than helixLengthMin2 */
bool
  Strenum9::structureSanityCheck(const Vec<Stem>& helices) const {
    if (helices.size() == 0) {
      return true;
    }
    index_t len = helices[0].getLength(); 
    for (size_t i = 0; i < helices.size(); ++i) {
      if (helices[i].getLength() < len) {
        len = helices[i].getLength();
      }
    }
    return len >= helixLengthMin2;
  }

ResiduePairing
  Strenum9::createResiduePairing(StrandContainer * strands, 
                                 ResidueModel * model,
                                 state_t state, 
                                 const Vec<Vec<Stem> >& subStems) const {
    // DEBUG_MSG("Starting createResiduePairing");
    ResiduePairing pairing(strands, model);
    for (size_t i = 0; i < state.size(); ++i) {
      // Rcpp::Rcout << i << " " << state[i] << " " << subStems.size() << " " << subStems[i].size() << endl;
      if (state[i] > 0) {
        const Stem& stem = subStems[i][state[i]-1];
        //       if (verbose > VERBOSE_DETAILED) {
        // 	Rcpp::Rcout << "# should be able to place stem " << stem << endl;
        //       }
        ERROR_IF(!pairing.isPlaceable(stem), "Internal error: base pairs are not consistent.");
        pairing.setStem(stem);
      }
    }
    // DEBUG_MSG("Finished createResiduePairing");
    return pairing;
  }

Vec<Stem>
  Strenum9::filterStemsByOccupancyMatrix(const Vec<Stem>& stems, const Vec<Vec<int> >& occMatrix, int badValue) const {
    Vec<Stem> result;
    for (size_t i = 0; i < stems.size(); ++i) {
      bool ok = true;
      for (int k = 0; k < stems[i].getLength(); ++k) {
        int x = stems[i].getStart() + k;
        int y = stems[i].getStop() - k;
        if (occMatrix[x][y] == badValue) {
          ok = false;
          break;
        }
      }
      if (ok) {
        result.push_back(stems[i]);
      }
    }
    return result;
  }

Vec<Stem>
  Strenum9::filterStemsByRequiredStructures(const Vec<Stem>& stems, const vector<string>& filenames, 
                                            const StrandContainer& strandsOrig,
                                            ResidueModel& dummyModel) const {
    size_t n = strandsOrig.getConcatSequence().size();
    StrandContainer strands;
    Vec<Vec<int> > occMatrix(n, Vec<int>(n, 0));
    for (size_t i = 0; i < filenames.size(); ++i) {
      ifstream file(filenames[i].c_str());
      ERROR_IF(!file, "Error opening file " + filenames[i]);
      ResiduePairing pairing = ResiduePairingTools::readCt(file, strands, dummyModel);
      for (size_t j = 0; j < pairing.size(); ++j) {
        if (pairing[j] != NO_PAIRING_INDEX) {
          occMatrix[j][pairing[j]] = 1;
          occMatrix[pairing[j]][j] = 1;
        }
      }
      file.close();
    }
    return filterStemsByOccupancyMatrix(stems, occMatrix, 0);
  }

ResidueStructure
  Strenum9::localOptimizationProtocoll(const ResidueStructure& structure, const Vec<Stem>& stems) {
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Starting local optimzation ...";
    }
    if (optimizationMode == 0) {
      return structure;
    }
    double tolerance = 0.1;
    double gOrig = structure.computeAbsoluteFreeEnergy();
    string connectivityOrig = structure.getStrandConnectivityHash();
    HelixStructureOptimizer optimizer;
    SingleHelixStructureOptimizer optimizer2(stems);
    optimizer.setAllowMelting(meltingMode > 0); // if true, helices can be melted away during local optimation
    optimizer.setVerbose(verbose-1);
    ResidueStructure result = optimizer.optimize(structure);
    for (size_t i = 0; i < optRounds; ++i) {
      double g1 = result.computeAbsoluteFreeEnergy();
      result = optimizer2.optimize(result);
      double g2 = result.computeAbsoluteFreeEnergy();
      if (verbose >= VERBOSE_INFO) {
	Rcpp::Rcout << "# Local optimization: single-helix optimization round " << (i+1) << " free energy change from " << g1 << " to " << g2 << endl;
      }
      if ((g2 + tolerance) >= g1) { // quite loop: no improvement possible
        break; 
      }
    }
    // result = optimizer.optimize(result);
    double gFinal = result.computeAbsoluteFreeEnergy();
    if (gFinal > gOrig) {
      if (verbose > VERBOSE_QUIET) {
        Rcpp::Rcout << "# Local optimization did not lower free energy. Reverting to original structure: " << gOrig << " " << gFinal << endl;
      }
      result = structure;
    }
    string connectivityFinal = result.getStrandConnectivityHash();
    bool connectivityChanged = connectivityOrig.compare(connectivityFinal) != 0;
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Finished local optimzation: Free energy changed from " << structure.computeAbsoluteFreeEnergy() << " to " << result.computeAbsoluteFreeEnergy() << " . Connectivity ";
      if (connectivityChanged) {
        Rcpp::Rcout << "changed : ";
      } else {
        Rcpp::Rcout << "unchanged : ";
      }
      Rcpp::Rcout << connectivityOrig << " " << connectivityFinal << endl;
    }
    if (structure.computeAbsoluteFreeEnergy() < result.computeAbsoluteFreeEnergy()) {
      Rcpp::warning("Optimization could not identify lower-energy structures.");
    }
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Finished local optimzation.";
    }
    return result;
  }

Rcpp::List Strenum9::main(const Vec<string>& args, const Vec<string>& sequences, const Vec<double>& concentrations, const string& homeDir) {
  int argc = static_cast<int>(args.size());
  char ** argv = new char*[argc];
  for (int i= 0; i < argc; ++i) {
    argv[i] = new char[args[i].size() + 1]; // plus one for termination character
    strcpy(argv[i],args[i].c_str());
  }
  Rcpp::List result =  main(argc, argv,sequences, concentrations,homeDir);
  if (result.size() == 0) {
    Rcpp::warning("Strange: secondary structure prediction returned an empty list!");
  }
  return result;
}

Rcpp::List
Strenum9::processArguments(int argc, char ** argv) {
/*   Rcpp::Rcout << "# Arguments: "; */
/*   for (int i = 0; i < argc; i++) { */
/*     Rcpp::Rcout << " " << argv[i]; */
/*   } */
//   Rcpp::Rcout << endl;
  Rcpp::List params;
  vector<string> allowedConnectivitiesVec;
  getArg("-allowed", allowedConnectivitiesVec, argc, argv); 
  for (size_t i = 0; i < allowedConnectivitiesVec.size(); ++i) { 
    allowedConnectivities.insert(allowedConnectivitiesVec[i]); 
  } 
  getArg("-analyze", saveModeInt, argc, argv, saveModeInt);        
  params["analyze"] = saveModeInt;
  getArg("-bpslop", bpSlop, argc, argv, bpSlop); 
  params["bpslop"] = bpSlop;
  getArg("c", conc, argc, argv, conc); // scale for uniform concentrations (1.0: one micromol per liter) 
  params["c"] = conc;
  getArg("-cl", reactionRadius, argc, argv, reactionRadius); 
  params["cl"] = reactionRadius;
  getArg("-cross", crossingMax, argc, argv, crossingMax); 
  params["cross"] = crossingMax;
  // @TODO: not parsing vectors anymore at the moment
  //   getArg("-data", strenumFileName, argc, argv, strenumFileName); 
  // params["data"] = strenumFileName;
  getArg("-dc", gCorrect, argc, argv, gCorrect); // degenerary correction 
  params["dc"] = gCorrect;
  // getArg("-digits", leadDigitConstraint, argc, argv);  // @TODO implement
  // params["digits"] = leadDigitConstraint;
  int distanceModeInt = distanceMode ? 1 : 0; 
  getArg("-dist", distanceModeInt, argc, argv, distanceModeInt); 
  params["dist"] = distanceModeInt;
  distanceMode = distanceModeInt ? true : false; 
  getArg("e", structureFileNames, argc, argv); 
  params["e"] = structureFileNames;
  getArg("-ehm", expandedHelicesMax, argc, argv, expandedHelicesMax); 
  params["ehm"] = expandedHelicesMax;
  getArg("-ew", entropyWeight, argc, argv, entropyWeight);
  params["ew"] = entropyWeight;
  getArg("-ga", helixDnaGAdjust, argc, argv, helixDnaGAdjust); // deprecated, use instead option --gd for adjusting DNA  
  getArg("-gd", helixDnaGAdjust, argc, argv, helixDnaGAdjust); 
  params["gd"] = helixDnaGAdjust;
  getArg("-gr", helixRnaGAdjust, argc, argv, helixRnaGAdjust); 
  params["gr"] = helixRnaGAdjust;
  getArg("-g2", smallStemDGCutoff, argc, argv, smallStemDGCutoff); 
  params["g2"] = smallStemDGCutoff;
  int guAllowedInt = 2; // allow GU inside helix but not at ends
  if (!guAllowed) {
    guAllowedInt = 0;
  }
  getArg("-gu", guAllowedInt, argc, argv, guAllowedInt);
  if (guAllowedInt == 0) {
    guAllowed = false;
    guEndAllowed = false;
  } else if (guAllowedInt == 1){ 
    guAllowed = true;
    guEndAllowed = true;
  } else if (guAllowedInt == 2){ 
    guAllowed = true;
    guEndAllowed = false;
  } else {
    DERROR("Value of option --gu must be either 1 (for true) or 0 (for false).");
  }

  if (isPresent("-nogu", argc, argv)) {
    guAllowed = false;
  }
  // Rcpp::Rcout << "# final version of guAllowed: " << guAllowed << endl;
  getArg("h", helixLengthMin, argc, argv, helixLengthMin); 
  params["h"] = helixLengthMin;
  getArg("-h2", helixLengthMin2, argc, argv, helixLengthMin2); 
  params["h2"] = helixLengthMin2;
  getArg("-hsort", helixSortMode, argc, argv, helixSortMode); 
  params["hsort"] = helixSortMode;
  // getArg("m", modelFile, argc, argv, modelFile); 
  // getArg("i", inputFileName, argc, argv, inputFileName);  // not used anymore, sequences are provided separately
  // params["i"] = inputFileName;
  // getArg("-id", idVec, argc, argv);  // for utilizing only a subset of sequences - not used anymore
  // params["id"] = idVec;
  // convert2InternalCounting(idVec); 
  getArg("-isp", interStrandPenalty, argc, argv, interStrandPenalty); 
  params["isp"] = interStrandPenalty;
  getArg("k", kineticsMode, argc, argv, kineticsMode); 
  params["k"] = kineticsMode;
  getArg("n", multiplicity, argc, argv, multiplicity); 
  params["n"] = multiplicity;
  ERROR_IF(multiplicity < 1, "Multiplicity (option -n)  of sequences cannot be less than one."); 
  int nExtendHelixMode = 1; 
  getArg("-extend", nExtendHelixMode, argc, argv, nExtendHelixMode); 
  params["extend"] = nExtendHelixMode;
  if (isPresent("-extend", argc,argv)) { 
    extendHelixMode = (nExtendHelixMode > 0); 
  } 
  getArg("-mod", lengthModulo, argc, argv, lengthModulo); // length steps of helices 
  params["mod"] = lengthModulo;
  getArg("o", outFileBase, argc, argv, outFileBase); 
  params["o"] = outFileBase;
  getArg("-opt", optimizationMode, argc, argv, optimizationMode); 
  params["opt"] = optimizationMode;
  getArg("-pg", stackingFileG, argc, argv, stackingFileG); 
  params["pg"] = stackingFileG;
  getArg("-ph", stackingFileH, argc, argv, stackingFileH); 
  params["ph"] = stackingFileH;
  getArg("-pseudo", pseudoknotMode, argc, argv, pseudoknotMode); 
  params["pseudo"] = pseudoknotMode;
  getArg("q", queueSizeMax, argc, argv, queueSizeMax); 
  params["q"] = queueSizeMax;
  getArg("-req", requiredFileNames, argc, argv); 
  params["req"] = requiredFileNames;

    getArg("-shc", smallStemDGCutoff,argc, argv, smallStemDGCutoff);  
    params["shc"] = smallStemDGCutoff; 
/*    getArg("-sim", simThreshold, argc, argv, simThreshold);  */
/*    params["sim"] = simThreshold;  */
/*   int placeSmallHelicesModeInt = placeSmallHelicesMode ? 1 : 0;  */
/*   getArg("-sh", placeSmallHelicesModeInt, argc, argv, placeSmallHelicesModeInt); // if set , also place small helices  */
/*   params["sh"] = placeSmallHelicesModeInt; */
/*   if (placeSmallHelicesModeInt > 0) {  */
/*     placeSmallHelicesMode = true;  */
/*   } else {  */
/*     placeSmallHelicesMode = false;  */
/*   }  */
   getArg("T", temperatureC, argc, argv, temperatureC); 
   params["T"] = temperatureC;
  return params;
}

Rcpp::List
  Strenum9::main(int argc, char ** argv, const Vec<string>& sequences, const Vec<double>& concentrations, const string& homeDir) {
    Rcpp::List resultList;
    string connDelim = "@"; // delimiter for different topologies
    getArg("v", verbose, argc, argv, verbose);
    getArg("-verbose", verbose, argc, argv, verbose);
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Program hyperfold version " << version << " called with parameters:" << endl << "# ";
        for (int i = 0; i < argc; ++i) {
          Rcpp::Rcout << (argv[i]) << " ";
        }
        Rcpp::Rcout << endl;
    }
    set_terminate(term_func2); // set handling function for uncaught exceptions
    // Random& random = Random::getInstance();
    // GraphTools gTools;
    // ConnectivityTools conTools;
    // ResidueModel model(1); 
    // string modelFile; //  = "../../prm/resgeom/bead1ResidueModel.prm";
    // string stackingFile; //  = "../../prm/resgeom/MatthewsSantaLuciaSugimoto.prm"; // stack_r4.prm";
    // // string stackingFile = "../../prm/resgeom/StackingParametersLindsay12_2012.dat"; // stack_r4.prm";
    // // string stackingFile = "../../prm/resgeom/Marky1982.dat"; // stack_r4.prm";
    // int allowUnfolding = 1;
    // double conc = 1.0;
    // string outFileBase = "resgeom_out";
    if ((homeDir.size() > 0) && (stackingFileG.size() > 0) && (stackingFileG[0] != '/')) {
      stackingFileG = homeDir + "/prm/resgeom/" + stackingFileG;
    }
    if ((homeDir.size() > 0) && (stackingFileH.size() > 0) && (stackingFileH[0] != '/')) {
      stackingFileH = homeDir + "/prm/resgeom/" + stackingFileH;
    }
    resultList["params"] = processArguments(argc, argv); // return actually used parameters
    ResidueStructure bestStructure;
    double bestStructureFreeEnergy = 1e10;
    bool bestStructureSmallHelices = false;
    std::ifstream stackingStreamG(stackingFileG.c_str());
    ERROR_IF(!stackingStreamG, "Error opening stacking file (option --pg): " + stackingFileG);
    modelG.readStacking(stackingStreamG);
    stackingStreamG.close();
    modelG.setEntropyWeight(entropyWeight);
    modelG.setTemperatureC(temperatureC);
    THROWIFNOT(temperatureC > -273.0, "internal error in Strenum9: L646");
    std::ifstream stackingStreamH(stackingFileH.c_str());
    ERROR_IF(!stackingStreamH, "Error opening stacking file (option --ph): " + stackingFileH);
    modelH.readStacking(stackingStreamH);
    stackingStreamH.close();
    
    modelG.adjustTemperature(modelH);   // adjust energy terms according to temperature:
    modelH.setTemperatureK(0); // no entropic contribution, just enthalpy
    modelG.setHelixDnaGAdjust(helixDnaGAdjust);
    modelG.setHelixRnaGAdjust(helixRnaGAdjust);
    modelG.setPseudoknotMode(pseudoknotMode);
    modelH.setPseudoknotMode(pseudoknotMode);
    modelG.setReactionRadius(reactionRadius);
    modelH.setReactionRadius(reactionRadius);
    string structureSeq; // only used if predefined structure given
    Vec<int> structureStarts; 
    if (sequences.size() == 0) { // (inputFileName.size() > 0) || (isPresent("-si", argc, argv)) || (structureSeq.size() > 0)) {
      vector<string> sequencesOrig;
      if (inputFileName.size() > 0) {
        if (verbose >= VERBOSE_QUIET) {
          Rcpp::Rcout << "# Reading sequence information from file " << inputFileName << endl;
        }
        ifstream inputFile(inputFileName.c_str());
        ERROR_IF(!inputFile, "Error opening input file: " + inputFileName);
        sequencesOrig = getLines(inputFile);
        inputFile.close();
      } else { // option --si not necessary anymore if (isPresent("-si", argc, argv)) { // read from standard input
        if (verbose >= VERBOSE_QUIET) {
          Rcpp::Rcout << "# Reading sequence information from standard input..." << inputFileName << endl;
        }
        sequencesOrig = getLines(cin); // read from standard input
      }
      // else if (structureSeq.size() > 0) {
      //      Rcpp::Rcout << "#### Using sequence information from predefined structure!" << endl;
      //      // use sequence from structure defined with -e option
      //      structureStarts.push_back(structureSeq.size()); // workaround
      //      for (size_t i = 0; (i+1) < structureStarts.size(); ++i) {
      // 	sequencesOrig.push_back(structureSeq.substr(structureStarts[i], structureStarts[i+1]-structureStarts[i]));
      //      }
      //    }
      // Rcpp::Rcout << "# Raw sequence data: " << endl << sequencesOrig << endl;
      //     for (vector<string>::size_type i = 0; i < sequencesOrig.size(); ++i) {
      //       sequencesOrig[i] = trimWhiteSpaceFromString(sequencesOrig[i]);
      //       if ((sequencesOrig[i].size() > 0) && (sequencesOrig[i][0] != '>')) {
      // 	     for (size_t j = 0; j < multiplicity; ++j) {
      // 	      sequences.push_back(sequencesOrig[i]); // insert this many copies
      // 	     }
      //       }
      //     } 
    }
    
    // if (idVec.size() > 0) { // use only subset of sequences
    //   Rcpp::Rcout << "# Using only subset of " << idVec.size() << " sequences!" << endl;
    //   sequences = getSubset(sequences, idVec);
    // }
    if (sequences.size() == 0) {
      DERROR("No sequences defined via standard input.");
    }
    // concentrations = Vec<double>(sequences.size(), conc); // concentrations in micromol per liter
    if (concentrations.size() > 0) {
      conc = concentrations[0]; // currently only the first value is used TODO
    }
    size_t residueCount = 0;
    string concatSequence;
    for (vector<string>::size_type i = 0; i < sequences.size(); ++i) {
      residueCount += sequences[i].size(); // insert this many copies
      concatSequence = concatSequence + sequences[i];
    } 
    //  sequences.push_back(s2);
    
    THROWIFNOT(concentrations.size() == sequences.size(), "Internal error: the number of concentrations is not equal to the number of sequences.");
    
    // potentially adjust pseudoknot mode:
    if (pseudoknotMode == 3) {
      if (sequences.size() < 2) {
        pseudoknotMode = ResidueModel::ALL_PSEUDOKNOTS;
      } else {
        pseudoknotMode = ResidueModel::INTERSTRAND_PSEUDOKNOTS;
      }
    }
    if (verbose > VERBOSE_QUIET) {
    Rcpp::Rcout << "# Concentrations (micromol per liter): " << concentrations;
    Rcpp::Rcout << "# Crossing-mode: " << crossingMax << endl;
    Rcpp::Rcout << "# Helix free energy adjustment (DNA): " << helixDnaGAdjust << endl;
    Rcpp::Rcout << "# Helix free energy adjustment (RNA): " << helixRnaGAdjust << endl;
    Rcpp::Rcout << "# Helix length restrictions: " << helixLengthMin << " " << helixLengthMin2 << endl;
    Rcpp::Rcout << "# Helix sort mode: " << helixSortMode << endl;
    Rcpp::Rcout << "# Placing small helices: " << placeSmallHelicesMode << endl;
    Rcpp::Rcout << "# Parameter file for Gibbs free energy: " << stackingFileG << endl;
    Rcpp::Rcout << "# Parameter file for enthalpy: " << stackingFileH << endl;
    Rcpp::Rcout << "# Maximum crossing number: " << crossingMax << endl;
    Rcpp::Rcout << "# Number of residues: " << residueCount << endl;
    Rcpp::Rcout << "# Output file base: " << outFileBase << endl;
    Rcpp::Rcout << "# Pseudoknot-mode: " << pseudoknotMode << endl;
    Rcpp::Rcout << "# Queue size maximum: " << queueSizeMax << endl;
    Rcpp::Rcout << "# Temperature: " << temperatureC << endl;
    Rcpp::Rcout << "# Verbosity: " << verbose << endl;
    Rcpp::Rcout << "# Version: " << version << endl;
    Rcpp::Rcout << "# Sequences: " << sequences;
    }
    //   for (size_t i = 0; i < concentrations.size(); ++i) {
    // concentrations[i] *= MICROMOL_TO_N_PER_ANG3; // AVOGADRO * 1E-33; // convert from micro mol per liter to particles per Angstroem cubed
    // }
    // Rcpp::Rcout << "# Concentrations (strands per Angstroem cubed): " << concentrations;
    StrandContainer strands(sequences, concentrations);
    Vec<int> dividers;
    // Vec<Stem> stems = StemTools::generateAllFlankedStems(strands, helixLengthMin, guAllowed, dividers, extendHelixMode, lengthModulo);
    GENERATE_HELICES:
    Vec<Stem> stems = StemTools::generateLongestStems(strands, helixLengthMin, guAllowed, guEndAllowed, dividers); // core helices
    //  Vec<Stem> stemsTiny = IndyFold::generateAllStems(&strands, strands.getConcatSequence(), strands.getStrandIds(), guAllowed, verbose); // min stem length 2, no energetic cutoff criterion
    Vec<Stem> stemsTiny = StemTools::generateAllStems(&strands, strands.getConcatSequence(), strands.getStrandIds(), guAllowed, verbose); // min stem length 2, no energetic cutoff criterion
    for (size_t i = 0; i < stemsTiny.size(); ++i) {
      THROWIFNOT(strands.isWatsonCrick(stemsTiny[i], guAllowed), "internal error in Strenum9: L754");
      double energy = modelG.calcStemStackingEnergy(strands.getConcatSequence(), stemsTiny[i].getStart(), stemsTiny[i].getStop(), stemsTiny[i].getLength());
      if (temperatureC != 37.0) {
        double enthalpy = modelH.calcStemStackingEnergy(strands.getConcatSequence(), stemsTiny[i].getStart(), stemsTiny[i].getStop(), stemsTiny[i].getLength());
        double entropy = (enthalpy - energy) / (37.0 + CELSIUS_TO_KELVIN); // all at 37 degree C
        double dT = temperatureC - 37.0; // deviation from norm temperature
        energy = energy - dT * entropy;
      }
      stemsTiny[i].setEnergy(energy);
    }
    if (requiredFileNames.size() > 0) {
      if (verbose > VERBOSE_QUIET) {
        Rcpp::Rcout << "# Number of core stems before filtering: " << stems.size() << endl;
      }
      stems = filterStemsByRequiredStructures(stems, requiredFileNames, strands, modelG);
      if (verbose > VERBOSE_QUIET) {
        Rcpp::Rcout << "# Number of core stems after filtering: " << stems.size() << endl;
      }
    }
    if (stems.size() > HELIX_COUNT_MAX) {
      ++helixLengthMin;
      Rcpp::Rcout << "# Warning: too large number of helices: " << stems.size() << ". Increasing minimum length to " << helixLengthMin << endl;
      goto GENERATE_HELICES;
    }
    double workaroundEnergy = -100000.0; // no stem will have energy smaller than this value
    for (size_t i = 0; i < stems.size(); ++i) {
      double energy = modelG.calcStemStackingEnergy(strands.getConcatSequence(), stems[i].getStart(), stems[i].getStop(), stems[i].getLength());
      if (temperatureC != 37.0) {
        double enthalpy = modelH.calcStemStackingEnergy(strands.getConcatSequence(), stems[i].getStart(), stems[i].getStop(), stems[i].getLength());
        double entropy = (enthalpy - energy) / (37.0 + CELSIUS_TO_KELVIN); // all  at 37 degree C                                                               
        double dT = temperatureC - 37.0; // deviation from norm temperature       
        energy = energy - dT * entropy;
      }

      if (helixSortMode == HELIX_SORT_INTRA) {      
 	if (strands.isIntraStrand(stems[i])) { // intra strand helices are folded first
          ERROR_IF(energy < workaroundEnergy, "Encountered unusually low helix energy term.");
          energy += 2*workaroundEnergy; // "mistake" is workaround for sorting; it is corrected in loop below
        } else {
	  energy += interStrandPenalty; // typically 0.0
	}
      } else if (helixSortMode == HELIX_SORT_INTRA2) {
	  ERROR_IF(energy < workaroundEnergy, "Encountered unusually low helix energy term.");
	  if (strands.isIntraStrand(stems[i])) { // intra strand helices are folded first
	    ERROR_IF(energy < workaroundEnergy, "Encountered unusually low helix energy term.");
	    energy += 2*workaroundEnergy; // "mistake" is workaround for sorting; it is corrected in loop below
	  } 
	  if (stems[i].getLength() >= helixSortIntra2Length) {
	    energy += 3*workaroundEnergy;
	  }
      }
      stems[i].setEnergy(energy);
    } // for (size_t i = 0 ...
    if (helixSortMode != HELIX_SORT_NONE) { // sort helices by energy
      sort(stems.begin(), stems.end(), StemEnergyComparator());
    }
    if (helixSortMode == HELIX_SORT_INTRA) {// reverse energy term that was added as workaround
      for (size_t i = 0; i < stems.size(); ++i) {
        double energy = stems[i].getEnergy();
        if (energy < workaroundEnergy) {
          energy -= 2*workaroundEnergy;
        }
        stems[i].setEnergy(energy);
      }
    } else if (helixSortMode == HELIX_SORT_INTRA2) {
      for (size_t i = 0; i < stems.size(); ++i) {
	double energy = stems[i].getEnergy();
	if (strands.isIntraStrand(stems[i])) { // intra strand helices are folded first
	  energy -= 2*workaroundEnergy; // "mistake" is workaround for sorting; it is corrected in loop below
	} 
	if (stems[i].getLength() >= helixSortIntra2Length) {
	  energy -= 3*workaroundEnergy;
	}
        stems[i].setEnergy(energy);
      }
    }
    for (size_t i = 0; i < stems.size(); ++i) { // verify that now helix has energy below sanity check level:
      ERROR_IF(fabs(stems[i].getEnergy()) > fabs(workaroundEnergy), "Encountered unusually low helix energy term.");
    }
    set<Stem> coreStemSet;
    for (size_t i = 0; i < stems.size(); ++i) {
      coreStemSet.insert(stems[i]);
    }
    Vec<Vec<Stem> > subStems(stems.size());
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Generated " << stems.size() << " core stems." << endl;
    }
    ERROR_IF(stems.size() > HELIX_COUNT_MAX, "Too large number of stems. Increase minimum helix length with option -h");
    if (verbose > VERBOSE_INFO) {
      Rcpp::Rcout << "# Concatenated sequence: " << strands.getConcatSequence() << endl;
    }
    Vec<Vec<int> > stemOverlapMatrix(stems.size(), Vec<int>(stems.size(), 0));
    Vec<Vec<size_t> > stemPseudoknotMatrix(stems.size(), Vec<size_t>(stems.size(), 0));
    Vec<Vec<Vec<Stem> > > strandLongestHelixMatrix(sequences.size(), Vec<Vec<Stem> >(sequences.size(), Vec<Stem>()));
    for (size_t i = 0; i < stems.size(); ++i) {
      int strandI1 = strands.getStrandId(stems[i].getStart());
      int strandI2 = strands.getStrandId(stems[i].getStop());
      strandLongestHelixMatrix[strandI1][strandI2].push_back(stems[i]);
      strandLongestHelixMatrix[strandI2][strandI1].push_back(stems[i]);
      for (size_t j = i+1; j < stems.size(); ++j) {
        int strandJ1 = strands.getStrandId(stems[j].getStart());
        int strandJ2 = strands.getStrandId(stems[j].getStop());
        stemOverlapMatrix[i][j] = stems[i].countCommonBases(stems[j]);
        stemOverlapMatrix[j][i] = stemOverlapMatrix[i][j];
        if ( ( ( (strandI1 == strandJ1) && (strandI2 == strandJ2)) || ( (strandI1 == strandJ2) && (strandI2 == strandJ1) ) )
               && ( (!stems[i].hasCommonBases(stems[j])) && stems[i].isCrossing(stems[j]) ) ) {
          stemPseudoknotMatrix[i][j] = 1;
          stemPseudoknotMatrix[j][i] = 1;
        }
      }
    }
    if (verbose > 1) {
      Rcpp::Rcout << "# Stem pseudoknot matrix:" << endl;
      writeMatrix(Rcpp::Rcout, stemPseudoknotMatrix, "#");
    }
    if (verbose > 1) {
      Rcpp::Rcout << "# Long inter-strand helices:" << endl;
      for (size_t i = 0; i < strandLongestHelixMatrix.size(); ++i) {
	for (size_t j = (i+1); j < strandLongestHelixMatrix[i].size(); ++j) {
	  Rcpp::Rcout << "# @strand_helices" << (i+1) << "," << (j+1) << ": ";
	  for (size_t k = 0; k < strandLongestHelixMatrix[i][j].size(); ++k) {
	    const Stem& stem = strandLongestHelixMatrix[i][j][k];
	    if (k > 0) {
	      Rcpp::Rcout << "; ";
	    }
	    Rcpp::Rcout << stem.getStart()+1 << "," << stem.getStop()+1 << "," << stem.getLength() << "," << stem.getEnergy();
	  }
	}
      }
    }
    Vec<Stem> stemsSmall;
    Vec<int> strandIds = strands.getStrandIds();
    bool sameStrandMode = false; // if false, also consider inter-strand helices even for short helices
    if (placeSmallHelicesMode) {
      ERROR_IF(helixLengthMin2 > helixLengthMin, "The minimum helix length must be smaller or equal the maximum helix length.");
      if (helixLengthMin >= 3 && helixLengthMin > helixLengthMin2) { // only generate small "extra" stems if their minimum length is smaller than the minimum length of the core stems
        // stemsSmall = StemTools::generateAllStems(strands, helixLengthMin2, guAllowed, dividers, 1, helixLengthMin-1, true);
        // Vec<Stem> stemsSmallOrig = StemTools::generateLongestStems(strands, helixLengthMin2, guAllowed, dividers);
        // Vec<Stem> stemsSmallOrig = IndyFold::generateAllStems(&strands, strands.getConcatSequence(), strands.getStrandIds(), guAllowed, 3, verbose); // , dividers, 1, 0); // helixLengthMin-1); // lengthModulo
        Vec<Stem> stemsSmallOrig = StemTools::generateAllStems(&strands, strands.getConcatSequence(), strands.getStrandIds(), guAllowed, verbose); //       // previously: IndyFold::generateAllStems
        for (size_t i = 0; i < stemsSmallOrig.size(); ++i) {
          // 	if (stemsSmallOrig[i].getLength() < helixLengthMin) {
          Stem stem = stemsSmallOrig[i];
          if (coreStemSet.find(stem) == coreStemSet.end() && stem.getLength() >= helixLengthMin2) { // only store helices that are not already core stems; length criterion
            if ((!sameStrandMode) || (strandIds[stem.getStart()] == strandIds[stem.getStop()])) { // same strand
              double energy = modelG.calcStemStackingEnergy(strands.getConcatSequence(), stem.getStart(), stem.getStop(), stem.getLength());	    
              if (energy <= smallStemDGCutoff) { // energy criterion
                stem.setEnergy(energy);
                stemsSmall.push_back(stem);
              }
            }
          } else {
            if (verbose > VERBOSE_FINE) {
              Rcpp::Rcout << "# This stem is already a core stem: " << stemsSmallOrig[i] << endl;
            }
          }
        }
        sort(stemsSmall.begin(), stemsSmall.end(), StemEnergyComparator()); // sort - most negative energy first
      }
      sort(stemsSmall.begin(), stemsSmall.end(), StemEnergyComparator()); // sort - most negative energy first
    }
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Number of defined short helices: " << stemsSmall.size() << endl;
      if (verbose > VERBOSE_QUIET) {
	Rcpp::Rcout << "# Generated core helices by position: " << endl;
	StemTools::printStemsByPosition(Rcpp::Rcout, stems, strands);
	if (verbose > VERBOSE_INFO) {
	  Rcpp::Rcout << "# Generated short helices by position: " << endl;
	  StemTools::printStemsByPosition(Rcpp::Rcout, stemsSmall, strands);
	}
      }
    }    
    if (stems.size() == 0) {
      if (verbose > VERBOSE_QUIET) {
	Rcpp::Rcout << "No core helices consisting of at least " + std::to_string(helixLengthMin) + " base pairs found." << endl;
      }
      ResiduePairing unfoldedStructure(& strands, & modelG);
      ResidueStructure structureG = ResidueStructureTools::buildStructure(unfoldedStructure);
      THROWIFNOT(structureSanityCheck(structureG), "Internal error in Strenum9: L891");
      // double newStructureGEnergyInitial =  structureG.computeAbsoluteFreeEnergy();
      // size_t helixCount1 = structureG.getHelixCount();
      if (placeSmallHelicesMode) {	
	structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
      }
      THROWIFNOT(structureSanityCheck(structureG), "Internal error in Strenum9: L896");
      // double newStructureGEnergy2 =  structureG.computeAbsoluteFreeEnergy();
      // size_t helixCount2 = structureG.getHelixCount();
      resultList["best_structure"] = ResidueStructureRcppTools::residueStructureToList(structureG);
      if (outFileBase.size() > 0 && writeFileMode) {
	string bestStructureFileName = outFileBase + "_best.ct";
	if (verbose > VERBOSE_DETAILED) {
	  Rcpp::Rcout << "# Writing structure to " << bestStructureFileName << endl;
	}
	ofstream bestStructureFile(bestStructureFileName.c_str());
	ERROR_IF(!bestStructureFile, "Error opening best structure file.");
	structureG.writeCT(bestStructureFile);
	bestStructureFile.close();
      }
      //  else {
      //	Rcpp::Rcout << " # Writing unfolded structure to " << bestStructureFileName << endl;
      //	ResiduePairingTools::writeCt(bestStructureFile, unfoldedStructure);
      //	Rcpp::warning("Currently the completely unfolded structure is not returned as list.");
      // }
      // resultList["status"] = 0;
      if (resultList.size() == 0) {
        Rcpp::warning("Strange case for unfolded structure: structure prediction returning empty list.");
      }
      return resultList; // special case of unfolded structure
    }
    
    if (structureFileNames.size() > 0) {
      Vec<ResiduePairing> referenceStructures;
      for (size_t i = 0; i < structureFileNames.size(); ++i) {
	if (i > 0) {
	  Rcpp::warning("Use of reference structure (option -e): Currently only one reference structure is utilized.");
	}
        ifstream structureFile(structureFileNames[i].c_str());
        referenceStructures.push_back(ResiduePairingTools::readCt(structureFile, strands, modelG));
        structureFile.close();
      }
      Vec<unsigned int> stemSubset;
      for (size_t j = 0; j < stems.size(); ++j) {
        bool foundbad = false;
	for (int k = 0; k < stems[j].getLength(); ++k) {
	  // for (size_t i = 0; i < 1; ++i) { // only one structure for now referenceStructures.size(); ++i) {
	  if (!referenceStructures[0].isBasePaired(stems[j].getStart()+k, stems[j].getStop()-k)) {
	    foundbad = true;
	    break;
	  }
	  // }
        }
	if (!foundbad) {
	  stemSubset.push_back(j);
	}
      }
      Rcpp::Rcout << "# filtering " << stems.size() << " helices obtained from " << structureFileNames[0] << " : ";
      stems = getSubset(stems, stemSubset);
      Rcpp::Rcout << stems.size() << " helices remain." << endl;
    }
    
    // Vec<Vec<Stem> > subStems(stems.size());
    // Vec<Vec<size_t> > subStemIds;
    size_t stemTotalCount = 0;
    state_t subStemCounts(stems.size(), 1);
    // Vec<MultiIterator::container_t> savedStates;
    Vec<state_t> savedStates;
    Vec<string> savedConnectivities;
    Vec<double> savedG, savedG2, savedS, savedBp;
    Vec<double> savedH;
    set<state_t> savedStatesSet;
    set<ResiduePairing> savedPairingSet;
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Generating flanked stems with minimum length " << helixLengthMin2 << " in length-steps of " << lengthModulo << endl;
    }
    for (size_t i = 0; i < stems.size(); ++i) {
      subStems[i].push_back(stems[i]);
      Vec<Stem> sv =  StemTools::generateFlankedStems(stems[i], helixLengthMin2, lengthModulo);
      for (size_t j = 0; j < sv.size(); ++j) {
        THROWIFNOT(sv[j].getLength() >= helixLengthMin2, "Internal error in Strenum9: L963");
        THROWIFNOT(strands.isWatsonCrick(sv[j], guAllowed), "Internal error in Strenum9: L964");
        sv[j].setEnergy(modelG.calcStemStackingEnergy(strands.getConcatSequence(), sv[j].getStart(), sv[j].getStop(), sv[j].getLength()));
        subStems[i].push_back(sv[j]);
      }
      subStemCounts[i] = subStems[i].size()+1; // plus one: for iterator: 0 stands for no stem, > 0 stands for stem id -1
      stemTotalCount += subStems[i].size();
    }
    string delim = " ";
    ERROR_IF(stems.size() == 0, "No helices defined!");
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Created overall " << stemTotalCount << " helices and " << stems.size() << " core helices. and " << stemsSmall.size() << " short helices." << endl;
      Rcpp::Rcout << "# @temperature\t" << temperatureC << endl;
      Rcpp::Rcout << "# @sequences\t" << strands;
      Rcpp::Rcout << "# @helices\t" << stemTotalCount;
      for (size_t i = 0; i < stems.size(); ++i) {
	// Rcpp::Rcout << "\t" << stems[i];
	for (size_t j = 0; j < subStems[i].size(); ++j) {
	  Rcpp::Rcout << " " << subStems[i][j];
	}
      }
      Rcpp::Rcout << endl;
      Rcpp::Rcout << "# lead digits: " << leadDigitConstraint;
      Rcpp::Rcout << "# @helix_substems " << stems.size();
      for (size_t i = 0; i < stems.size(); ++i) {
	Rcpp::Rcout << " " << subStems[i].size();
      }
      Rcpp::Rcout << endl;
    }
    map<string, size_t> bestConnectivityIds;
    map<string, double> bestConnectivityScores;
    map<string, ResidueStructure> bestConnectivityStructures;
    
    // do {
    //   skipBadRegion = false;
    //   MultiIterator::container_t digits = it.getDigits();
    //   Rcpp::Rcout << (++count) << " : ";
    //   for (size_t i = 0; i < digits.size(); ++i) {
    //     Rcpp::Rcout << digits[i] << " ";
    //   }
    //   Rcpp::Rcout << endl;
    // } while (it.incIfPossible());
    // DERROR("This is what we have tried so far.");
    Vec<size_t> smallHelixIds;
    // MultiIterator it2(subStemCounts); // stems.size(), 2));
    //   Vec<Vec<double> > stemLengthScores(subStemCounts.size());
    //   for (size_t i = 0; i < stemLengthScores.size(); ++i) {
    //     stemLengthScores[i] = Vec<double>(subStemCounts[i].size(), 0.0);
    //     for (size_t j = 0; j < stemLengthScores[i].size(); ++j) {
    //       if (j > 0) {
    // 	Stem& stem = subStems[i][j-1];
    // 	stemLengthScores[i][j] = (double)(stem.getLength());
    //       }
    //     }
    //   }
    DEBUG_MSG("Mark 395");
    // bool skipBadRegion = false;
    // multimap<string, size_t> transitionStates; // child as key, can have multiple parent structures
    set<string> transitionStatesSet; // store if transition state has been already saved
    map<string, size_t> transitionStatesIds; // stores ids of transition states corresponding to transitionStatesSet
    map<string, ResidueStructure> transitionStatesFull;
    double newStructureGBp = 0.0; // number of base pairs of new structure
    double newStructureGEnergy = 0.0; // free energy of distance based structure
    double newStructureGEntropy = 0.0; // free energy of distance based structure
    if (strenumFileName.size() > 0) {
      ifstream strenumFile(strenumFileName.c_str());
      ERROR_IF(!strenumFile, "Error opening data file: " + strenumFileName);
      StrenumTools::readStrenumFileV2(strenumFile, savedStates, savedG, savedH, savedConnectivities);
      Rcpp::Rcout << "# Read " << savedStates.size() << " states from file " << strenumFileName << endl;
      Rcpp::Rcout << "# Read states:" << endl;
      IOTools::writeHead(Rcpp::Rcout, savedStates, delim="\n");
      Rcpp::Rcout << endl << "# Read dG values:" << endl;
      IOTools::writeHead(Rcpp::Rcout, savedG, delim="\n");
      Rcpp::Rcout << endl << "# Read dH values:" << endl;
      IOTools::writeHead(Rcpp::Rcout, savedH, delim="\n");
      Rcpp::Rcout << endl << "# Read connectivity values:" << endl;
      IOTools::writeHead(Rcpp::Rcout, savedConnectivities, delim="\n");
      THROWIFNOT(savedG.size()>0, "Internal error in Strenum9: L1047");
      size_t minId = minElement(savedG);
      Rcpp::Rcout << "Minimum free energy state: " << savedG[minId] << " kcal/mol " << savedConnectivities[minId] << " state: " << savedStates[minId] << endl;
    } else { // no input file with states
      // do {
      PurgePriorityQueue<ResiduePairingStemSolution> queue1; // best structures so far
      PurgePriorityQueue<ResiduePairingStemSolution> queue2; // best structures so far
      queue1.setMaxSize(queueSizeMax);
      queue2.setMaxSize(queueSizeMax);
      bool addUnfolded = true;
      bool placeSmallHelicesModeInitial = false; // special case to ensure completely unfolded structure is part of search
      if (addUnfolded) { // special case at very beginning of search
        DEBUG_MSG("Mark 422");
        ResiduePairing newStructureG(&strands, &modelG);
        ResiduePairing newStructureH(&strands, &modelH);
        ResidueStructure rs = ResidueStructureTools::buildStructure(newStructureG);
        newStructureGBp = rs.countBasePairs();
        newStructureGEnergy = rs.computeAbsoluteFreeEnergy();
        newStructureGEntropy = rs.getEntropy();
        THROWIFNOT(newStructureG.getSequences()->getConcentrations()[0] == conc, "Internal error in Strenum9: L1066");
        THROWIFNOT(newStructureH.getSequences()->getConcentrations()[0] == conc, "Internal error in Strenum9: L1067");
        state_t state;
        state.smallHelices = placeSmallHelicesMode;
        // if (leadDigitConstraint.size() == 0) {
        ResiduePairingStemSolution::container_t firstSol;
        firstSol.smallHelices = placeSmallHelicesMode;
        if (verbose > VERBOSE_INFO) {
	  Rcpp::Rcout << "# creating solution with id " << savedG.size() << " " << placeSmallHelicesMode << endl;
	}
        // ResidueStructure structureG = ResidueStructureTools::buildStructure(newStructureG);
        
        // add completely unfolded state?
        // savedG.push_back(rs.computeAbsoluteFreeEnergy()); // newStructureG.computeAbsoluteFreeEnergy());
        // savedG2.push_back(rs.computeAbsoluteFreeEnergy()); // newStructureG.computeAbsoluteFreeEnergy());
        // savedConnectivities.push_back(rs.getStrandConnectivityHash());
        // savedS.push_back(rs.getEntropy());
        // savedH.push_back(0.0); // no single base pair formed
        
        if (placeSmallHelicesModeInitial) {
          double newStructureGEnergyInitial = rs.computeAbsoluteFreeEnergy(); //   structureG.computeAbsoluteFreeEnergy();
          size_t helixCount1 = rs.getHelixCount(); // structureG.getHelixCount();
          // structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
          rs.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
          double newStructureGEnergy2 = rs.computeAbsoluteFreeEnergy(); // structureG.computeAbsoluteFreeEnergy();
          size_t helixCount2 = rs.getHelixCount(); // structureG.getHelixCount();
          state.smallHelices = (helixCount2 > helixCount1) ? 1 : 0;
          if (verbose > VERBOSE_DETAILED) {
            Rcpp::Rcout << "# Placed " << (helixCount2-helixCount1) << " helices leading to change in free energy from " 
                 << newStructureGEnergyInitial << " to " << newStructureGEnergy2 << " !" << endl;
          }
          
        }
        newStructureGBp = rs.countBasePairs();
        newStructureGEnergy = rs.computeAbsoluteFreeEnergy();// structureG.computeAbsoluteFreeEnergy();
        newStructureGEntropy = rs.getEntropy(); // structureG.getEntropy();
        
        string connectivity = rs.getStrandConnectivityHash(); // newStructureG.getStrandConnectivityHash();
        ResiduePairingStemSolution first(newStructureGEnergy, savedG.size(), ResiduePairingStemSolution::NO_PARENT_ID, firstSol, newStructureG, newStructureH);
        queue1.push(first); // start with unfolded structure
        // } 
        if (savedStatesSet.find(state) == savedStatesSet.end()) { // if new structure
          savedStatesSet.insert(state);
          savedG.push_back(newStructureGEnergy);
          savedS.push_back(newStructureGEntropy);
          savedBp.push_back(newStructureGBp);
          if (distanceMode) {
            try {
              savedG2.push_back(newStructureGEnergy);
            } catch (int e) {
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# Warning: could not build structure " << state;
              }
              savedG.push_back(999.9); // structure could not be built
              savedS.push_back(999.9);
            }
          }
          // savedH.push_back(newStructureH.computeAbsoluteFreeEnergy());
          while (state.size() < stems.size()) {
            state.push_back(0); // add unplaced stem id "0"
          }
          savedStates.push_back(state);
          // savedConnectivities.push_back(structureG.getStrandConnectivityHash());
          savedConnectivities.push_back(rs.getStrandConnectivityHash());
          
          if (newStructureGEnergy < bestStructureFreeEnergy) {
            bestStructure = rs; // structureG;
            bestStructureFreeEnergy = newStructureGEnergy;
            bestStructureSmallHelices = false;
            THROWIFNOT(savedG.size() > 0, "Internal error in Strenum9: L1133");
            THROWIFNOT(savedG[minElement(savedG)] == bestStructureFreeEnergy, "Internal error in Strenum9: L1134");
            // Rcpp::Rcout << "# Found low free energy structure with " << bestStructureGEnergy << " kcal/mol" << endl;
          }
          if ((bestConnectivityScores.find(connectivity) == bestConnectivityScores.end()) || (newStructureGEnergy < bestConnectivityScores[connectivity])) {
            bestConnectivityStructures[connectivity] = rs; // structureG;
            bestConnectivityScores[connectivity] = newStructureGEnergy;
            bestConnectivityIds[connectivity] = savedG.size(); // not sure it id is OK
          }
          
        } 
        
        if (verbose > VERBOSE_INFO) {
          Rcpp::Rcout << "# Stored seed structure with constraint helices: " << state;
        }
      }
      /******************* MAIN LOOP OVER COMPLETING STRUCTURE *******************/
      for (size_t digId = leadDigitConstraint.size(); digId < stems.size(); ++digId) { // loop over digits
        if (verbose >= VERBOSE_INFO) {
          Rcpp::Rcout << "# Working on helix " << (digId+1) << " . Analyzed structures: " << savedG.size();
          if (verbose > VERBOSE_DETAILED) {
            Rcpp::Rcout << " " << " queue size: " << maximum(queue1.size(), queue2.size());
          }
          Rcpp::Rcout << endl;
          if (verbose > VERBOSE_FINE) {
            PurgePriorityQueue<ResiduePairingStemSolution> queue1Copy = queue1; // best structures so far
            PurgePriorityQueue<ResiduePairingStemSolution> queue2Copy = queue2; // best structures so far
            int pc1 = 0;
            while (queue1Copy.size() > 0) {
              if (verbose > VERBOSE_DETAILED) {
                Rcpp::Rcout << "# queue 1 Pos " << (++pc1) << " : " << queue1Copy.top().getStemIds() << " " << queue1Copy.top().getStructureG().computeAbsoluteFreeEnergy();
              }
              queue1Copy.pop();
            }
            int pc2 = 0;
            while (queue2Copy.size() > 0) {
              if (verbose > VERBOSE_DETAILED) {
                Rcpp::Rcout << "# queue 2 Pos " << (++pc2) << " : " << queue2Copy.top().getStemIds() << " " << queue2Copy.top().getStructureG().computeAbsoluteFreeEnergy();
              }
              queue2Copy.pop();
            }
          }
        }
        // ERROR_IF((&newQueue) == (&currQueue), "Internal error 391");
        DEBUG_MSG("Mark Strenum9cc-498");
        ResiduePairingStemSolution dummySolution;
        DEBUG_MSG("Mark Strenum9cc-500");
        if (verbose > VERBOSE_FINE) {
          Rcpp::Rcout << "# Internal: " << digId << " " << queue1.size() << " " << queue2.size() << endl;
        }
        while (((digId % 2 == 0) && (queue1.size() > 0)) || ((digId % 2 == 1) && (queue2.size() > 0))) {
          if (verbose >= VERBOSE_FINE) {
            Rcpp::Rcout << "While loop: " << digId << " " << queue1.size() << " " << queue2.size() << endl;
          }
          ResiduePairingStemSolution& topSol = dummySolution;
          if (digId % 2 == 0) {
            topSol = queue1.top();
            ResiduePairingStemSolution topSolCopy = topSol;
            if (verbose >= VERBOSE_FINE) {
              Rcpp::Rcout << "# Stem ids before adding zero: " << topSolCopy.getStemIds();
            }
            topSolCopy.addStemIdZero();
            ERROR_IF(topSolCopy.getStemIds().size() != (digId+1), "Internal error 440"); // this many stems must be defined (including unplaced stem)
            queue2.push(topSolCopy); // pass on to next gen
          } else {
            topSol = queue2.top();
            ResiduePairingStemSolution topSolCopy = topSol;
            if (verbose >= VERBOSE_FINE) {
              Rcpp::Rcout << "# Stem ids before adding zero: " << topSolCopy.getStemIds();
            }
            topSolCopy.addStemIdZero();
            ERROR_IF(topSolCopy.getStemIds().size() != (digId+1), "Internal error 445"); // this many stems must be defined (including unplaced stem)
            queue1.push(topSolCopy); // pass on to next gen
          }
          ResiduePairingStemSolution::container_t sofarStems = topSol.getStemIds();
          size_t prevId = topSol.getId(); // id of structure before new stem is placed
          // Rcpp::Rcout < "# So far placed stems: " << sofarStems;
          //  1: full stem, 2: half stem (even id), 3: half stem (odd id)   digit 0 (the unplaced stem) does not have to be searched
          for (size_t digitRaw = 1; digitRaw <= 3; ++digitRaw) { 
            size_t digit = digitRaw;
            // 	  if (digitRaw == 2 || digitRaw == 3) {
            // 	    EASSERT(subStems[digId].size() % 2 == 1); // odd number of helices: fully extended helix and two versions of shortened helices
            // 	    digit = ((subStems[digId].size()) / 3) + 2;
            // 	  } 
            // 	  if (digitRaw == 2 || digitRaw == 4) {
            // 	    EASSERT(subStems[digId].size() % 2 == 1); // odd number of helices: fully extended helix and two versions of shortened helices
            // 	    digit = (2*(subStems[digId].size()) / 3) + 2;
            // 	  } 
            // 	  if (digit < 2) {
            // 	    continue; // helix is too short
            // 	  }	  
            // 	  if ((((digitRaw % 2) == 0) && (digit % 2 == 1))  // must be even
            // 	      || (((digitRaw % 2) == 1) && (digit % 2 == 0)) ) {  // must be odd
            // 	    ++digit;
            // 	  }
            
            // previous version of dividing each stem in half
            
            if (digitRaw >= 2) {
              THROWIFNOT(subStems[digId].size() % 2 == 1, "internal error in Strenum9: L1232"); // odd number of helices: fully extended helix and two versions of shortened helices
              digit = ((subStems[digId].size()-1) / 2) + 1;
              if (digit < 2) {
                continue; // helix is too short
              }
              if (digitRaw == 2) {
                if (digit % 2 == 1) {
                  ++digit; // must be even
                }
                THROWIFNOT(digit % 2 == 0, "Internal error in Strenum9: L1241");
              } else if (digitRaw == 3) {
                if (digit % 2 == 0) {
                  ++digit; // must be odd
                }
                THROWIFNOT(digit % 2 == 1, "Internal error in Strenum9: L1246");
              }
              THROWIFNOT(digit > 1, "Internal error in Strenum9: L1248");
            }
            
            // MultiIterator::container_t digits = it.getDigits();
            // Rcpp::Rcout << "# Working on digits " << digits; // DEBUG output
            // if ((chosenStemIds.size() > 1) && (!StemTools::isMutuallyOverlappingHelixSet(chosenStemIds, stems))) {
            // 	if (verbose > 1) {
            // 	  Rcpp::Rcout << "# Helices " << chosenStemIds << " are not mutually overlapping - skipping case." << endl;
            // 	}
            // 	continue;
            // }
            // bool violation = false;
            if ((digId < leadDigitConstraint.size()) && (digit != leadDigitConstraint[digId])) {
              Rcpp::Rcout << "# Skipping structure that is incompatible with constraints: " << " " << digit << " " << (digId+1) << " : " << leadDigitConstraint;
              continue; // FIXIT more efficient
            }
            state_t digits = sofarStems;	  
            digits.push_back(digit);
            bool violation = false;
            size_t expandedHelixCount = 0;
            for (size_t i = 0; i < digits.size(); ++i) {
              if (digits[i] > 1) {
                ++expandedHelixCount;
              }
            }
            if (expandedHelixCount > expandedHelicesMax) {
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# number of helices chosen for expansion is greater than " << expandedHelicesMax << " : " << digits;
              }
              continue;
            } 
            
            const Stem& stemI = subStems[digId][digit-1];
            size_t digitPeel = digit; // check for peel-back
            THROWIFNOT(digits[digId] == digit, "Internal error in Strenum9: L1282");
            size_t crossingCounter = 0;
            for (size_t j = 0; j < digId; ++j) { // check if any of existing stems is overlapping or crossing
              if (digits[j] > 0) {
                const Stem& stemJ = subStems[j][digits[j]-1];
THROWIFNOT(&stemI != &stemJ,"Assertion violation in Strenum9.h :  L1324");
                crossingCounter += stemPseudoknotMatrix[digId][j];
                if (crossingCounter > crossingMax) { // the new stem crosses too many existing stems with same strand connectivity
                  violation = true;
                  break;
                }
                if (stemI.hasCommonBases(stemJ)) { // shorten stemI: use 
                  if (verbose > VERBOSE_INFO) {
                    Rcpp::Rcout << "# Stem-overlap detected. Attempting peel-back..." << endl;
                  }
                  for (size_t k = j+1; k < digId; ++k) { // ensure it is overlapping with at most one stem
THROWIFNOT(k < subStems.size(),"Assertion violation in Strenum9.h :  L1335");
THROWIFNOT(k < digits.size(),"Assertion violation in Strenum9.h :  L1336");
                    if (digits[k] > 0) {
THROWIFNOT(digits[k] > 0,"Assertion violation in Strenum9.h :  L1338");
THROWIFNOT(digits[k]-1 < subStems[k].size(),"Assertion violation in Strenum9.h :  L1339");
                      const Stem& stemK = subStems[k][digits[k]-1];
                      if (stemI.hasCommonBases(stemK)) {
                        if (verbose > VERBOSE_DETAILED) {
                          Rcpp::Rcout << "# Found multiple overlaps. Ignoring stem alltogether." << endl;
                        }
                        violation = true;
                        break;
                      }
                    }
                  }
                  if (violation) {
                    break; // this stem is overlapping with more than one other stem - do not consider at all
                  }
                  // 3 cases:
                  // case 1: longest stem: try shortening until half length is reached:
                  size_t digitHalf = ((subStems[digId].size()-1) / 2);
                  if (digitHalf > 0) {
                    if (verbose > VERBOSE_INFO) {
                      Rcpp::Rcout << "# Searching for shortened stem: " << digitHalf << endl;
                    }
                    EASSERT(subStems[digId].size() % 2 == 1); // odd number of helices: fully extended helix and two versions of shortened helices
                    if (digitRaw == 2) {
                      if (digitHalf % 2 == 1) {
                        ++digitHalf; // must be even
                      }
THROWIFNOT(digitHalf % 2 == 0,"Assertion violation in Strenum9.h :  L1365");
                    } else if (digitRaw == 3) {
                      if (digitHalf % 2 == 0) {
                        ++digitHalf; // must be odd
                      }
THROWIFNOT(digitHalf % 2 == 1,"Assertion violation in Strenum9.h :  L1370");
                    }
THROWIFNOT(digitHalf > 0,"Assertion violation in Strenum9.h :  L1372");
                    size_t kStart = digitHalf;
                    size_t kStep = 2;
                    size_t kStop = subStems[digId].size()+1;
                    if (digitRaw == 1) {
                      kStep = 1; // search from both sides starting from whole helix
                      kStart = 2;
                      kStop = digitHalf;
                    } 
                    bool found = false;
                    EASSERT(stemJ.hasCommonBases(subStems[digId][0])); // longest stem has overlap
                    if (kStart <= 1) {
#ifndef NDEBUG
                      Rcpp::Rcout << "# Warning: strange helix index: " << digId << " " << digitRaw << " " << subStems[digId].size() << " " << digitHalf << endl;
#endif
                      violation = true;
                      break;
                    }
THROWIFNOT(kStart > 1,"Assertion violation in Strenum9.h :  L1390");
                    for (size_t k = kStart; k < kStop; k+=kStep) {
THROWIFNOT(k > 0,"Assertion violation in Strenum9.h :  L1392");
THROWIFNOT(k-1 < subStems[digId].size(),"Assertion violation in Strenum9.h :  L1393");
                      const Stem& stemK = subStems[digId][k-1];
                      if (!stemJ.hasCommonBases(stemK)) {
                        found = true; 
                        if (verbose > VERBOSE_INFO) {
                          Rcpp::Rcout << "# Found shortened stem for position " << digId << " : " << k << endl;
                        }
                        digitPeel = k;
                        break;
                      } else {
                        if (verbose > VERBOSE_DETAILED) {
                          Rcpp::Rcout << "# Shortened stem is still colliding for position " << digId << " : " << k << endl;
                        }
                      }
                    }
                    if (!found) {
                      violation = true;
                      break;
                    }
                  } // else : stem cannot be shortened
                }
              }
              if (violation) {
                break;
              }
            }
            
            if (violation) {
              DEBUG_MSG("Stem (even if shortened) is unpleaceable.");
              continue;
            }
            
            if (digitPeel != digit) { // peel-back
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# Performing peel back: placing stem " << digitPeel << " at last position of " << digits;
              }
THROWIFNOT(digits[digits.size()-1] == digit,"Assertion violation in Strenum9.h :  L1429");
              digit = digitPeel;
              digits[digits.size()-1] = digitPeel;
            }
            
            size_t isolatedShortenedHelixCount = 0;
            // size_t crossingCount = 0; // number of pseudoknot crossings (not counting strand topology)
            
            // 	  for (size_t i = 0; i < digits.size(); ++i) { // TODO improve code by avoiding duplication of evaluation
            // 	    if (digits[i] > 0) { // try to find other helix with which it is overlapping:
            // 	      // bool found = false;
            // 	      size_t crossingCount = 0; // number of pseudoknot crossings (not counting strand topology)
            // 	      for (size_t j = i+1; j < digits.size(); ++j) {
            // 		if (digits[j] > 0) {
            // 		  crossingCount += stemPseudoknotMatrix[i][j];
            // 		  if (crossingCount > crossingMax) {
            // 		    if (verbose > VERBOSE_INFO) {
            // 		      Rcpp::Rcout << "# Ignoring digits because we have too high crossing counts: " << crossingMax << " : " << digits;
            // 		    }	    
            // 		    violation = true;
            // 		    break;
            // 		  }
            // 		  if (stemOverlapMatrix[i][j] > 0) {
            // 		    const Stem& stemI = subStems[i][digits[i]-1];
            // 		    const Stem& stemJ = subStems[j][digits[j]-1];
            // 		    if (stemI.hasCommonBases(stemJ)) {
            // 		      if (verbose >= VERBOSE_FINE) {
            // 			Rcpp::Rcout << "# Ignoring digits because two helices are not compaptible: " << digits;
            // 		      }	    
            // 		      // DERROR("Internal error: Strenum9cc-688"); // helices should at this stage not collide anymore
            //  		      violation = true; // helices are not compatible // VERIFY IF NECESSARY
            //  		      break;
            // 		    }
            // 		    // taken out: check for too many isolated shortened helices
            // // 		    else {
            // // 		      int maxBp = stems[i].getLength() + stems[j].getLength() - stemOverlapMatrix[i][j];
            // // 		      int pairBp = stemI.getLength() + stemJ.getLength();
            // // 		      if ((pairBp + branchSlop) < maxBp) {
            // // 			// violation = true; // branch migration is too "sloppy"; CURRENTLY DEACTIVATED
            // // 			if (verbose >= VERBOSE_INFO) {
            // // 			  Rcpp::Rcout << "# Violation due to pairing rule step: " << pairBp << " " << branchSlop << " " << maxBp << " : " << digits;
            // // 			}
            // // 			break;
            // // 		      }
            // // 		      found = true; // found a viable branch migration
            // // 		    }
            // 		  }
            // 		}
            // 	      }
            // 	      if (violation) {
            // 		break;
            // 	      }
            // // 	      if (!found) {
            // // 		++isolatedShortenedHelixCount;
            // // 	      }
            // 	    }
            // 	  }
            
            
            if (violation) {
              if (verbose > VERBOSE_DETAILED) {
                Rcpp::Rcout << "# skipped structure because of incompatible helices: ";
                for (size_t jj = 0; jj < digits.size(); ++jj) {
THROWIFNOT(jj < digits.size(),"Assertion violation in Strenum9.h :  L1492");
THROWIFNOT(jj < subStems.size(),"Assertion violation in Strenum9.h :  L1493");
                  if (digits[jj] > 0) {
THROWIFNOT(digits[jj]-1 < subStems[jj].size(),"Assertion violation in Strenum9.h :  L1495");
                    const Stem& stem = subStems[jj][digits[jj]-1];
                    Rcpp::Rcout << " " << stem;
                  }
                }
                Rcpp::Rcout << " digits: " << digits;
              }
              continue;
            }
            size_t isolatedShortenedHelixMax = 1000; // DEACTIVATED; // 4; // this many helices are allowed that are shortened but not part of branch migration
            if (isolatedShortenedHelixCount > isolatedShortenedHelixMax) {
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# skipped structure because too many isolated shortened helices: " << digits;
              }
              DERROR("Internal erro 529 - just for debugging.");
              continue;
            }
            ResiduePairing newStructureG = topSol.getStructureG();
            ResiduePairing newStructureH = topSol.getStructureH();
            string connectivityHash = newStructureG.getStrandConnectivityHash();
            if (digit > 0) {
THROWIFNOT((digit-1) < subStems[digId].size(),"Assertion violation in Strenum9.h :  L1516");
              Stem& stem = subStems[digId][digit-1];
              const ResiduePairing& testStructure = newStructureG;
              if (! (testStructure.isPlaceable(stem) && testStructure.isNewBasePairOK(stem.getStart(), stem.getStop() ) ) ) {
                //       if (verbose > VERBOSE_INFO) {
                //     	  Rcpp::Rcout << "# skipped structure because stem was not placeable on structural level: " << digits;
                // 	      Rcpp::Rcout << "# placeable: " << testStructure.isPlaceable(stem) << " OK: " << testStructure.isNewBasePairOK(stem.getStart(), stem.getStop()) << endl;
                // 	      Rcpp::Rcout << "# Tested stem: " << stem << endl;
                // 	      Rcpp::Rcout << "# Stems placed already: " << testStructure.getHelices();
                // 	    }
                // DERROR("Internal error Strenum9cc 943: new stem should be placeable");
                continue; // FIXIT more efficient : test shortest ones first, then extensions do not have to be tested
              }
              newStructureG.setStem(stem);
              newStructureH.setStem(stem);
              // Rcpp::Rcout << "# Stem " << stem << " was OK" << endl;
              // Rcpp::Rcout << "# Previous stems: " << digits;
              // for (size_t i = 0; i < digId; ++i) {
              //   if (digits[i] > 0) {
              // 	Rcpp::Rcout << subStems[i][digits[i]-1] << endl;
              //   }
              // }2
            }
            // everything OK:
            
            state_t state = digits;
            state.smallHelices = false;
            while (state.size() < stems.size()) {
              state.push_back(0); // add unplaced stem id "0"
            }
            state_t sofarState = sofarStems;	  
            while (sofarState.size() < stems.size()) {
              sofarState.push_back(0); // add unplaced stem id "0"
            }
            
            ResidueStructure structureG = ResidueStructureTools::buildStructure(newStructureG);
            
            // GEN_STRUCTURE: // pass through two times: with and without placed small helices
            
            double newStructureGEnergy = structureG.computeAbsoluteFreeEnergy();
            // double newStructureGEntropy = structureG.getEntropy();
            string connectivity = structureG.getStrandConnectivityHash();
            THROWIFNOT(structureSanityCheck(structureG), "Internal error in Strenum9: L1521");
            string transition, transition2;
            
            /////
            
            if (!placeSmallHelicesMode) {
	      Rcpp::warning("placing short helices has been deactivated");
              if (savedStatesSet.find(state) == savedStatesSet.end()) {
                savedStatesSet.insert(state);
                savedPairingSet.insert(structureG.convertFromResidueStructure());
                savedG.push_back(structureG.computeAbsoluteFreeEnergy());
                savedS.push_back(structureG.getEntropy());	  // savedH.push_back(newStructureH.computeAbsoluteFreeEnergy());
                savedBp.push_back(static_cast<double>(structureG.countBasePairs()));
                string currConn = structureG.getStrandConnectivityHash();
                savedConnectivities.push_back(currConn);
                savedStates.push_back(state);
                
                // string newConnectivity = structureG.getStrandConnectivityHash(); // newStructureG.getStrandConnectivityHash();
                if (currConn.compare(connectivityHash) != 0) {	    // topology has changed !
                  string transitionB = connectivityHash + " " + currConn; // forwards
                  string transitionB2 = currConn + " " + connectivityHash; // backwards
                  transitionStatesSet.insert(transitionB);
                  transitionStatesSet.insert(transitionB2);
                  size_t currId = savedG.size()-1; // id ot current transition states
                  transitionStatesFull[transitionB] = structureG;
                  if (transitionStatesIds.find(transitionB) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transitionB] = currId;
                  } else {
                    size_t prevTransId = transitionStatesIds[transitionB];
                    if (savedG[currId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transitionB] = currId;
                    }
                  }
                  if (transitionStatesIds.find(transitionB2) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transitionB2] = prevId; // id of parent structure - before stem was placed
                  } else {
                    size_t prevTransId = transitionStatesIds[transitionB2];
                    if (savedG[prevId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transitionB2] = prevId;
                    }
                  }
                } 
                
                if (newStructureGEnergy < bestStructureFreeEnergy) {
                  bestStructure = structureG;
                  bestStructureFreeEnergy = newStructureGEnergy;
                  THROWIFNOT(savedG.size()>0, "Internal error in Strenum9: L1567");
                  THROWIFNOT(savedG[minElement(savedG)] == bestStructureFreeEnergy, "Internal error in Strenum9: L1568");
                }
                if ((bestConnectivityScores.find(currConn) == bestConnectivityScores.end()) || (newStructureGEnergy < bestConnectivityScores[currConn])) {
                  THROWIFNOT(structureG.getStrandConnectivityHash().compare(currConn) == 0, "internal error in Strenum9: L1571");
                  bestConnectivityStructures[currConn] = structureG;
                  bestConnectivityScores[currConn] = newStructureGEnergy;
                  bestConnectivityIds[currConn] = savedG.size()-1; // not sure it id is OK
                }
              }
              //// 
              
            } else { // if (placeSmallHelicesMode) {
              DEBUG_MSG("Placing small helices.");
              size_t helixCount1 = structureG.getHelixCount();
              ResidueStructure structureGOrig = structureG;
              ResiduePairing pairingGOrig = structureGOrig.convertFromResidueStructure();
              structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff); // , helixLengthMin2, false);
              ResiduePairing pairingG = structureG.convertFromResidueStructure();
              if(!structureSanityCheck(structureG)) {
		Rcpp::warning("Best structure has validation problem.");
	      }
              double newStructureGEnergy2 =  structureG.computeAbsoluteFreeEnergy();
              size_t helixCount2 = structureG.getHelixCount();
              string connectivity2 = structureG.getStrandConnectivityHash();
              // 	    if (connectivity2.compare(connectivity) != 0) { // connectivity has changed due to placement of short helices	      
              if (savedStatesSet.find(state) == savedStatesSet.end() && savedPairingSet.find(pairingGOrig) == savedPairingSet.end()) {
                // save structure before placement of small helices:
                savedStatesSet.insert(state);
                savedPairingSet.insert(pairingGOrig);
                if(newStructureGEnergy != structureGOrig.computeAbsoluteFreeEnergy()) {
		  Rcpp::warning("The energy has changed from " + std::to_string(newStructureGEnergy) + " to " + std::to_string(structureGOrig.computeAbsoluteFreeEnergy()));
		}
                savedG.push_back(newStructureGEnergy);
                savedS.push_back(structureGOrig.getEntropy());	  // savedH.push_back(newStructureH.computeAbsoluteFreeEnergy());
                savedBp.push_back(static_cast<double>(structureGOrig.countBasePairs()));
                savedConnectivities.push_back(connectivity);
                savedStates.push_back(state);
                // string newConnectivity = structureG.getStrandConnectivityHash(); // newStructureG.getStrandConnectivityHash();
                if (connectivity.compare(connectivityHash) != 0) {	    // connectivity has change with respect to parent state
                  string transitionB = connectivityHash + " " + connectivity; // forwards
                  string transitionB2 = connectivity + " " + connectivityHash; // backwards
                  transitionStatesSet.insert(transitionB);
                  transitionStatesSet.insert(transitionB2);
                  size_t currId = savedG.size()-1; // id ot current transition states
                  transitionStatesFull[transitionB] = structureGOrig;
                  if (transitionStatesIds.find(transitionB) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transitionB] = currId;
                  } else {
                    size_t prevTransId = transitionStatesIds[transitionB];
                    if (savedG[currId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transitionB] = currId;
                    }
                  }
                  if (transitionStatesIds.find(transitionB2) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transitionB2] = prevId; // id of parent structure - before stem was placed
                  } else {
                    size_t prevTransId = transitionStatesIds[transitionB2];
                    if (savedG[prevId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transitionB2] = prevId;
                    }
                  }
                } 
                
                if (newStructureGEnergy < bestStructureFreeEnergy) {
                  bestStructure = structureGOrig;
                  bestStructureFreeEnergy = newStructureGEnergy;
                  bestStructureSmallHelices = false;
                  THROWIFNOT(savedG.size()>0, "Internal error in Strenum9: L1631");
                  THROWIFNOT(savedG[minElement(savedG)] == bestStructureFreeEnergy, "Internal error in Strenum9: L1632");
                }
                if ((bestConnectivityScores.find(connectivity) == bestConnectivityScores.end()) || (newStructureGEnergy < bestConnectivityScores[connectivity])) {
THROWIFNOT(structureGOrig.getStrandConnectivityHash().compare(connectivity) == 0,"Assertion violation in Strenum9.h :  L1676");
                  bestConnectivityStructures[connectivity] = structureGOrig;
                  bestConnectivityScores[connectivity] = newStructureGEnergy;
                  bestConnectivityIds[connectivity] = savedG.size()-1; // not sure it id is OK
                }
              } else {
                // Rcpp::Rcout << "Duplicate evaluation of state encountered." << state << endl;
              }
              
              // if (structureG.getBasePairCount() > bp1) { // newStructureGEnergy2 < newStructureGEnergy) {
              // if (true) { // always
              
              // now save structure with additional small helices placed:
              if (savedPairingSet.find(pairingG) == savedPairingSet.end()) {
                savedPairingSet.insert(pairingG);
                savedG.push_back(structureG.computeAbsoluteFreeEnergy());
                savedS.push_back(structureG.getEntropy());	  // savedH.push_back(newStructureH.computeAbsoluteFreeEnergy());
                savedBp.push_back(static_cast<double>(structureG.countBasePairs()));
                savedConnectivities.push_back(structureG.getStrandConnectivityHash()); // 	  savedConnectivities.push_back(newStructureG.getStrandConnectivityHash());
                state.smallHelices = true;
                savedStates.push_back(state);	      
                savedStatesSet.insert(state);
                
                if (newStructureGEnergy2 < bestStructureFreeEnergy) {
                  bestStructure = structureG;
                  bestStructureFreeEnergy = newStructureGEnergy2;
                  bestStructureSmallHelices = true;
THROWIFNOT(savedG.size()>0,"Assertion violation in Strenum9.h :  L1703");
THROWIFNOT(savedG[minElement(savedG)] == bestStructureFreeEnergy,"Assertion violation in Strenum9.h :  L1704");
                }
                if ((bestConnectivityScores.find(connectivity2) == bestConnectivityScores.end()) || (newStructureGEnergy2 < bestConnectivityScores[connectivity2])) {
THROWIFNOT(structureG.getStrandConnectivityHash().compare(connectivity2) == 0,"Assertion violation in Strenum9.h :  L1707");
                  bestConnectivityStructures[connectivity2] = structureG;
                  bestConnectivityScores[connectivity2] = newStructureGEnergy2;
                  bestConnectivityIds[connectivity2] = savedG.size(); // not sure it id is OK
                }
                
                if (connectivity.compare(connectivity2) != 0) {
                  transition = connectivity + " " + connectivity2; // forwards
                  transition2 = connectivity2 + " " + connectivity; // backwards
                  transitionStatesSet.insert(transition);
                  transitionStatesSet.insert(transition2);
                  transitionStatesFull[transition] = structureG;
                  size_t currId = savedG.size()-1; // id ot current transition staes
                  if (transitionStatesIds.find(transition) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transition] = currId;
                  } else {
                    size_t prevTransId = transitionStatesIds[transition];
                    if (savedG[currId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transition] = currId;
                    }
                  }
                  if (transitionStatesIds.find(transition2) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transition2] = prevId; // id of parent structure - before stem was placed
                  } else {
                    size_t prevTransId = transitionStatesIds[transition];
                    if (savedG[prevId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transition2] = prevId;
                    }
                  }
                }
                if (connectivityHash.compare(connectivity2) != 0) {
                  transition = connectivityHash + " " + connectivity2; // forwards
                  transition2 = connectivity2 + " " + connectivityHash; // backwards
                  transitionStatesSet.insert(transition);
                  transitionStatesSet.insert(transition2);
                  transitionStatesFull[transition] = structureG;
                  size_t currId = savedG.size()-1; // id ot current transition staes
                  if (transitionStatesIds.find(transition) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transition] = currId;
                  } else {
                    size_t prevTransId = transitionStatesIds[transition];
                    if (savedG[currId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transition] = currId;
                    }
                  }
                  if (transitionStatesIds.find(transition2) == transitionStatesIds.end()) { // new transition stae
                    transitionStatesIds[transition2] = prevId; // id of parent structure - before stem was placed
                  } else {
                    size_t prevTransId = transitionStatesIds[transition];
                    if (savedG[prevId] < savedG[prevTransId]) { // better transition state found!
                      transitionStatesIds[transition2] = prevId;
                    }
                  }
                }
                
              } // if (savedStateSet.find ...
              
              
              if (verbose > VERBOSE_DETAILED) {
                Rcpp::Rcout << "# Placed " << (helixCount2-helixCount1) << " helices leading to change in free energy from " 
                     << newStructureGEnergy << " to " << newStructureGEnergy2 << " ..." << endl;
              }
            }
            
            ResiduePairingStemSolution newSol(structureG.computeAbsoluteFreeEnergy(), savedG.size(), topSol.getId(), digits, newStructureG, newStructureH);
            if (verbose > VERBOSE_DETAILED) {
              Rcpp::Rcout << "# Storing stem ids in queue: " << newSol.getStemIds() << endl;
            }
            ERROR_IF(newSol.getStemIds().size() != (digId+1), "Internal error 578"); // this many stems must be defined (including unplaced stem)
            if (digId % 2 == 0) {
              queue2.push(newSol);
            } else {
              queue1.push(newSol);
            }
            
          } // for (size_t digit = 0; digit < subStemCounts[digId]; ++digit)
          if (digId % 2 == 0) {
            queue1.pop();
          } else {
            queue2.pop();
          }
        } // while (currQueue.size() > 0
        if (verbose > VERBOSE_FINE) {
          Rcpp::Rcout << "# Finished working on stem " << (digId+1) << " queue sizes: " << queue1.size() << " " << queue2.size() << endl;
        }
      } // for (size_t dig = 0. ... //  while (it.incIfPossible());
      DEBUG_MSG("Mark 584");
      queue1.clear(); // remove memory-intensive data
      queue2.clear();
    } // else (if strenumFile.size() == 0
    // DO_WHILE_LOOP_END:
    
    // if (distanceMode) { // swap, such that savedG contains distane-based energies
    //   auto tmp = savedG;
    //   EASSERT(savedG.size() == savedG2.size());
    //   savedG = savedG2;
    //   savedG2 = tmp;
    // } else {
    //   savedG2 = savedG;
    // }
    
    DEBUG_MSG("Mark 513");
    // if (saveMode) {
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Analyzing results" << endl;
    }
    // sort energies:
    if (sortStatesMode) {
      Vec<unsigned int> order = documentedSort(savedG);
      // EASSERT(savedH.size() == order.size());
      // savedH = sortByOrder(savedH, order);
THROWIFNOT(savedConnectivities.size() == order.size(),"Assertion violation in Strenum9.h :  L1818");
      savedConnectivities = sortByOrder(savedConnectivities, order);
THROWIFNOT(savedS.size() == order.size(),"Assertion violation in Strenum9.h :  L1820");
      // savedH = sortByOrder(savedH, order);
      savedS = sortByOrder(savedS, order);
      // EASSERT(savedG2.size() == order.size());
      // savedG2 = sortByOrder(savedG2, order);
THROWIFNOT(savedStates.size() == order.size(),"Assertion violation in Strenum9.h :  L1825");
      savedStates = sortByOrder(savedStates, order);
    }
    EASSERT(savedG.size() == savedS.size()); // numbe of stored entropies must match
THROWIFNOT(savedG.size() == savedBp.size(),"Assertion violation in Strenum9.h :  L1829");
    DEBUG_MSG("# Computing concentrations:");
    double partitionFunction = 0.0;
    // double logPartitionFunction = 0.0;
    double RT = modelG.getRT();
    // double R = RT / modelG.getTemperatureK();
    Vec<string> uniqConnectivities = uniqueSet(savedConnectivities);
    map<string, size_t> connectIdMap;
    set<string> complexSet;
    Vec<double> connectivityEntropies(uniqConnectivities.size(), 0.0); // compute entropy for each connectivity via S = -k Sum(p log p)
    for (size_t i = 0; i < uniqConnectivities.size(); ++i) {
      vector<string> complexes = getTokens(uniqConnectivities[i], "|");
      for (auto it = complexes.begin(); it != complexes.end(); it++) {
        complexSet.insert(*it);
      }
      connectIdMap.insert(make_pair(uniqConnectivities[i], i));
    }
#ifndef NDEBUG
    Rcpp::Rcout << "# Considered complexes:" << endl;
    for (auto it = complexSet.begin(); it != complexSet.end(); it++) {
      Rcpp::Rcout << (*it) << endl;
    }
#endif
    map<string, double> complexConcentrations;
    Vec<double> partialProbabilities(uniqConnectivities.size(), 0.0);
THROWIFNOT(savedG.size() > 0,"Assertion violation in Strenum9.h :  L1854");
    size_t lowestOverallId = minElement(savedG);
    // best structures for each topology:
    DEBUG_MSG("Mark L758");
    if (verbose > VERBOSE_INFO) {
      Rcpp::Rcout << "# Free energies of states: " << savedG;
    }
    if (verbose > VERBOSE_INFO) {
      Rcpp::Rcout << "# Id corresponding to minimum free energy: " << (minElement(savedG)+1) << " : " << savedG[minElement(savedG)] << endl;
    }
THROWIFNOT(RT == modelG.getRT(),"Assertion violation in Strenum9.h :  L1864");
    EASSERT(RT > modelH.getRT()); // Enthalpy model temperature has been set to zero Kelvin
    Vec<double> stateProbabilities = savedG;
THROWIFNOT(savedG.size() > 0,"Assertion violation in Strenum9.h :  L1867");
    double minG = savedG[minElement(savedG)];
    for (size_t i = 0; i < stateProbabilities.size(); ++i) {
      stateProbabilities[i] -= minG;
    }
    EASSERT(stateProbabilities[minElement(stateProbabilities)] == 0.0); // lowest energy is zero
THROWIFNOT(savedBp.size() == savedG.size(),"Assertion violation in Strenum9.h :  L1873");
THROWIFNOT(gCorrect >= 0.0,"Assertion violation in Strenum9.h :  L1874");
    for (size_t i = 0; i < stateProbabilities.size(); ++i) {
THROWIFNOT(isReasonable(stateProbabilities[i]),"Assertion violation in Strenum9.h :  L1876");
      double g = stateProbabilities[i];
      double term = exp(-g/RT);
THROWIFNOT(isReasonable(term),"Assertion violation in Strenum9.h :  L1879");
      // degeneracy?
      if (gCorrect != 0.0) {
        double bp = savedBp[i];
        if (bp > 1) {
          double deg = bp; // degeneracy
          double g2 = g + gCorrect; // energy is a bit higher due to one base pair less
          term += deg * exp(-g2/RT);
        }
      }
      stateProbabilities[i] = term; // go from log probabilities to regular probabilities
      partitionFunction += term;
THROWIFNOT(isReasonable(partitionFunction),"Assertion violation in Strenum9.h :  L1891");
THROWIFNOT(connectIdMap.find(savedConnectivities[i]) != connectIdMap.end(),"Assertion violation in Strenum9.h :  L1892");
      size_t id = (connectIdMap.find(savedConnectivities[i]))->second; // id of connectivity
      partialProbabilities[id] += term; 
    }
    double tmpSum = 0.0;
    for (size_t i = 0; i < partialProbabilities.size(); ++i) { // loop over configurations (not states)
      partialProbabilities[i] /= partitionFunction; // normalize by partition function
      if (partialProbabilities[i] < EPSILONISSIMO) {
	partialProbabilities[i] = EPSILONISSIMO; // numbers that are practically zero will be set to a value close to representation limit such as ~ 1e-307
      }
      tmpSum += partialProbabilities[i];
    }
    if (tmpSum != 1.0) { // despite workaround to avoid zero probabilities, we still want the sum of probabilities to be one
      for (size_t i = 0; i < partialProbabilities.size(); ++i) { // loop over configurations (not states)
	partialProbabilities[i] /= tmpSum;
      }
    }
    tmpSum = 0.0;
    for (size_t i = 0; i < stateProbabilities.size(); ++i) {
      stateProbabilities[i] /= partitionFunction;
      if (stateProbabilities[i] < EPSILONISSIMO) {
	stateProbabilities[i] = EPSILONISSIMO;
      }
      tmpSum += stateProbabilities[i];
    }
    if (tmpSum != 1.0) { // despite workaround to avoid zero probabilities, we still want the sum of probabilities to be one
      for (size_t i = 0; i < stateProbabilities.size(); ++i) { // loop over configurations (not states)
	stateProbabilities[i] /= tmpSum;
      }
    }
    Vec<double> featureProbs(features.size()); // probabilities of each feature
    Vec<Vec<double> > occMatrix0(concatSequence.size(), Vec<double>(concatSequence.size(), 0)); // overall occupancy matrix
    string suboptFileName = outFileBase + "_subopt.ct";
    if (!writeFileMode) {
      suboptFileName = GARBAGE_FILE_NAME; // do not actually write, like /dev/null on Unix
    }
    ofstream suboptFile(suboptFileName);
    ERROR_IF(!suboptFile, "Error opening sub-optimal structure file: " + suboptFileName);
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Writing sub-optimtal structures to " << suboptFileName << endl;
    }
    Vec<size_t> energyOrder = documentedOrder(savedG);
    double bpExpect = 0; // expected number of base pairs of complexes
    double lowestEnergy = savedG[energyOrder[0]];
    if (verbose > VERBOSE_INFO) {
      Rcpp::Rcout << "# lowestEnergy: " << lowestEnergy << " bestStructureFreeEnergy: " << bestStructureFreeEnergy << " small helices: " << bestStructureSmallHelices << endl;
    }
THROWIFNOT(lowestEnergy == bestStructureFreeEnergy,"Assertion violation in Strenum9.h :  L1919");
    if (verbose > VERBOSE_INFO) {
      Rcpp::Rcout << "# Found energies: ";
      for (size_t i = 0; i < savedG.size(); ++i) {
	Rcpp::Rcout << " " << savedG[energyOrder[i]];
      }
      Rcpp::Rcout << endl;
    }
THROWIFNOT(savedG.size() == energyOrder.size(),"Assertion violation in Strenum9.h :  L1927");
    
    for (size_t i0 = 0; i0 < savedG.size(); ++i0) {
THROWIFNOT((i0 == 0) || (savedG[energyOrder[i0-1]] <= savedG[energyOrder[i0]]),"Assertion violation in Strenum9.h :  L1930");
      size_t i = energyOrder[i0]; // evaluate in order of lowest free energy in order to write sorted suboptimal structures
      double prob = stateProbabilities[i];
      double g = savedG[i];
      if (verbose > VERBOSE_INFO) {
	Rcpp::Rcout << "# Working on supopt structure " << savedG[i] << " kcal/mol, probability " << prob << endl;
      }
      THROWIFNOT(prob >= 0.0,"Assertion violation in Strenum9.h :  L1937");
      THROWIFNOT(prob <= 1.0,"Assertion violation in Strenum9.h :  L1938");
      bpExpect += prob * savedBp[i];
      double termConc = conc * prob; 
      string connectivity = savedConnectivities[i];
      THROWIFNOT(connectIdMap.find(connectivity) != connectIdMap.end(),"Assertion violation in Strenum9.h :  L1942");
      size_t id = (connectIdMap.find(connectivity))->second; // id of connectivity
      if (partialProbabilities[id] > 0) {
	connectivityEntropies[id] += - prob * savedS[i] / partialProbabilities[id];
      }
      vector<string> complexes = getTokens(connectivity, "|");
      for (size_t j = 0; j < complexes.size(); ++j) {
        auto it = complexConcentrations.find(complexes[j]);
        if (it == complexConcentrations.end()) {
          complexConcentrations.insert(make_pair(complexes[j], termConc));
        } else {
          complexConcentrations[complexes[j]] = it->second + termConc;
        }
      }
      ResiduePairing pairing = createResiduePairing(&strands, &modelG, savedStates[i], subStems);
      ResidueStructure structureG = ResidueStructureTools::buildStructure(pairing);
      if (placeSmallHelicesMode && (pairing.countBasePairs() > 0) && savedStates[i].smallHelices) {	// add small stems; special case: no small stems are added to completely unfolded structure
        structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
        pairing = structureG.convertFromResidueStructure(&strands, &modelG);
      }
      
      size_t updateCount = 0;
      for (index_t ii = 0; ii < static_cast<index_t>(pairing.size()); ++ii) {
        if (structureG.isBasePaired(ii)) {
          index_t bp = structureG.getBasePair(ii);
THROWIFNOT(bp >= 0,"Assertion violation in Strenum9.h :  L1965");
THROWIFNOT(bp < static_cast<index_t>(occMatrix0[ii].size()),"Assertion violation in Strenum9.h :  L1966");
          if (bp > ii) {
            ++updateCount;
            occMatrix0[ii][bp] += stateProbabilities[i];
            occMatrix0[bp][ii] = occMatrix0[ii][bp];
	    // THROWIFNOT(stateProbabilities[i] > 0.0,"Assertion violation in Strenum9.h :  L1971");
          }
        }
      }
      
      if (updateCount != structureG.countBasePairs()) {
        if (verbose > VERBOSE_QUIET) {
	  Rcpp::Rcout << "Updated occupancy matrix for " << updateCount << " base pairs. Expected " << structureG.countBasePairs() << endl;
	}
        //    Rcpp::Rcout << occMatrix0 << endl;
      }
      if (i0 < suboptWriteMax && writeFileMode) {      
        suboptFile << "# Subopt_id=" << (i0+1) << " " << (i+1) << endl;
        suboptFile << "# Energy=" << savedG[i] << " " << g << endl;
        suboptFile << "# State=" << savedStates[i].smallHelices << " : " << savedStates[i];
        suboptFile << "# Connectivity=" << savedConnectivities[i] << endl;
        // structureG.writeCT(Rcpp::Rcout);
        structureG.writeCT(suboptFile);
      }
      if (verbose > VERBOSE_INFO) {
        Rcpp::Rcout << "State " << (i+1) << " :";
        if (bpOutMode > 0) {
          StrenumTools::writeLine(Rcpp::Rcout, pairing);
          Rcpp::Rcout << " :";
        }
        for (size_t j = 0; j < savedStates[i].size(); ++j) {
          Rcpp::Rcout << " " << savedStates[i][j];
        }
        Rcpp::Rcout << " : " << savedG[i] << " " << " : " << savedConnectivities[i] << " " << stateProbabilities[i];
        if (features.size() > 0) {
          Rcpp::Rcout << " Features:";
          for (size_t ii = 0; ii < features.size(); ++ii) {
            bool flag = features[i].hasFeature(savedStates[i]);
            Rcpp::Rcout << " " << flag;
            if (flag) {
              featureProbs[ii] += stateProbabilities[i];
            }
          }
        }
        Rcpp::Rcout << endl;
      }
    }
    suboptFile.close();
    double probabilitySum = 0.0;
    for (size_t i = 0; i < savedG.size(); ++i) {
      probabilitySum += stateProbabilities[i];
    }
    if (verbose > 2) {
      Rcpp::Rcout << "# The sum of state probabilties is " << probabilitySum << endl;
    }
    ERROR_IF(fabs(probabilitySum-1.0) > 0.01, "The probability sum is not 1.0");
    if (verbose > VERBOSE_QUIET) {
      Rcpp::Rcout << "# Structure with lowest free energy: " << bestStructureFreeEnergy << " kcal/mol" << endl;
      bestStructure.writeCT(Rcpp::Rcout);
    }
    resultList["best_structure"] = ResidueStructureRcppTools::residueStructureToList(bestStructure);
    THROWIFNOT(Rcpp::as<Rcpp::List>(resultList["best_structure"]).size() > 0, "Internal error Strenum9 L1960");    
    resultList["occupancy"] = RcppHelp::to_NumericMatrix(occMatrix0);
    //     if (outFileBase.size() > 0) {
    if (writeFileMode) {
      string bestStructureFileName = outFileBase + "_best.ct";
      ofstream bestStructureFile(bestStructureFileName.c_str());
      ERROR_IF(!bestStructureFile, "Error opening best structure file.");
      ERROR_IF(lowestOverallId >= savedStates.size(), "Internal error: undefined solution id.");
THROWIFNOT(structureSanityCheck(bestStructure),"Assertion violation in L2035");
      bestStructure.writeCT(bestStructureFile);
      if (verbose > VERBOSE_QUIET) {
        Rcpp::Rcout << "# Writing state " << (lowestOverallId+1) << " with free energy " << savedG[lowestOverallId] << " to " << bestStructureFileName << " : " << savedStates[lowestOverallId];
      }
      // if (temperatureC != temperatureC2){ 
      // Rcpp::Rcout << "# Free energy at temperature " << temperatureC2 << " C: " << bestStructure.computeAbsoluteFreeEnergy(temperatureC2 + CELSIUS_TO_KELVIN) << " kcal/mol " << endl;
      // }
      bestStructureFile.close();
    }
/*     if (writeFileMode) { */
/*       for (int jj = 1; jj <= 9; jj++) { */
/*         double cutoffProb = jj * 0.1; */
/*         string bestOccFileName = outFileBase + "_maxocc_" + dtos(cutoffProb) + ".ct"; */
/*         if (verbose > VERBOSE_QUIET) { */
/*           Rcpp::Rcout << "# Writing state " << (lowestOverallId+1) << " with free energy " << savedG[lowestOverallId] << " to " << bestOccFileName << " : " << savedStates[lowestOverallId]; */
/*         } */
/*         ofstream bestOccFile(bestOccFileName.c_str()); */
/*         ERROR_IF(!bestOccFile, "Error opening maximum occupance file: " + bestOccFileName); */
/*         ResiduePairing bestOccStructure(&strands, &modelG); */
/*         bestOccStructure.setBasePairsFromMatrix(occMatrix0, cutoffProb);  */
/*         ResiduePairingTools::writeCt(bestOccFile, bestOccStructure); */
/* 	if (verbose > VERBOSE_QUIET) { */
/*          Rcpp::Rcout << "# Writing state best occupancy structure to file: " << bestOccFileName << endl; */
/*         } */
/*         bestOccFile.close(); */
/*       } */
/*     } */
    if (optimizationMode != 0) {
      ResidueStructure bestStructureOpt = localOptimizationProtocoll(bestStructure, stemsTiny);
      resultList["opt_structure"] = ResidueStructureRcppTools::residueStructureToList(bestStructureOpt);
      if (writeFileMode) {
        string bestStructureFileNameOpt = outFileBase + "_best_opt.ct";
        ofstream bestStructureFileOpt(bestStructureFileNameOpt.c_str());
        ERROR_IF(!bestStructureFileOpt, "Error opening optimized best structure file.");
        ERROR_IF(lowestOverallId >= savedStates.size(), "Internal error: undefined solution id.");
        //     ResiduePairing bestStructure2 = createResiduePairing(&strands, &modelG, savedStates[lowestOverallId], subStems);
        //     ResidueStructure bestStructure = ResidueStructureTools::buildStructure(bestStructure2);
        //     if (placeSmallHelicesMode) {
        //       bestStructure.placeHelicesByProbability(stemsSmall);
        //     }
        bestStructureOpt.writeCT(bestStructureFileOpt);
        if (verbose > VERBOSE_QUIET) {
          Rcpp::Rcout << "# Writing state " << (lowestOverallId+1) << " with free energy " << bestStructureOpt.computeAbsoluteFreeEnergy() << " kcal/mol at " << temperatureC << "C to " << bestStructureFileNameOpt << endl;
        }
        if (temperatureC != temperatureC2){ 
          if (verbose > VERBOSE_QUIET) {
           Rcpp::Rcout << "# Free energy at temperature " << temperatureC2 << " C: " << bestStructureOpt.computeAbsoluteFreeEnergy(temperatureC2 + CELSIUS_TO_KELVIN) << " kcal/mol " << endl;
          }
        }
        bestStructureFileOpt.close();
      }
    }
    if (writeFileMode) {
      if (verbose > VERBOSE_QUIET) {
      string occMatrixFileName = outFileBase + "_occ.tab";
      Rcpp::Rcout << "# Writing occupancy matrix: to file " << occMatrixFileName << endl;
      ofstream occFile(occMatrixFileName.c_str());
      ERROR_IF(!occFile, "Error writing to occupancy file: " + occMatrixFileName);
      occFile << "\"Start\" \"Stop\" \"Probability\"" << endl;
      writePositiveMatrixElements(occFile, occMatrix0, true); // true: flag for symmetric matrices
      occFile.close();
      }
    }
    Rcpp::List connectivitiesList;
    for (size_t ii = 0; ii < uniqConnectivities.size(); ++ii) {
      string connectivity = uniqConnectivities[ii];
      // if (bestConnectivityIds.find(connectivity) != bestConnectivityIds.end()) {
      if (bestConnectivityStructures.find(connectivity) != bestConnectivityStructures.end()) {
        string bestStructureFileName = StrenumTools::sanitizeFileName(outFileBase + "_best_" + connectivity + ".ct");
        ResidueStructure structure = bestConnectivityStructures[connectivity];
        connectivitiesList[connectivity] = ResidueStructureRcppTools::residueStructureToList(structure);
        double energy = structure.computeAbsoluteFreeEnergy();
        if (verbose > VERBOSE_QUIET) {
          Rcpp::Rcout << "LOWEST_CONNECTIVITY " << connectivity << " " << energy << " ";
          StrenumTools::writeLine(Rcpp::Rcout, structure);
          Rcpp::Rcout << endl;
        }
THROWIFNOT(structure.getStrandConnectivityHash().compare(connectivity) == 0,"Assertion violation in Strenum9.h :  L2113");
        if (writeFileMode) {
          if (verbose > VERBOSE_QUIET) {
           Rcpp::Rcout << "# Saving lowest free energy structure with connectivity " << connectivity << " and free energy " << bestConnectivityScores[connectivity] << " to file " << bestStructureFileName << endl;
          }
          ofstream bestFile(bestStructureFileName.c_str());
          ERROR_IF(!bestFile, "Error opening best structure file.");
          structure.writeCT(bestFile);
          bestFile.close();
        }
        map<string, ResidueStructure> oneMoreHelixMap = ResidueStructureTools::placeHelicesForConnectivities(structure, stemsTiny, kineticStemExploreMax);
        if (verbose > VERBOSE_QUIET) {
	  Rcpp::Rcout << "# Found " << oneMoreHelixMap.size() << " neighboring connectivities " << endl;
	}
        for (auto it = oneMoreHelixMap.begin(); it != oneMoreHelixMap.end(); ++it) {
          const string& connectivity2 = it->first;
          ResidueStructure structure2 = it->second;
          double energy2 = structure2.computeAbsoluteFreeEnergy();
          double dEnergy = energy2 - energy;
          double energyFinal = 999.0;
          if (bestConnectivityStructures.find(connectivity2) != bestConnectivityStructures.end()) {
            energyFinal = bestConnectivityStructures[connectivity2].computeAbsoluteFreeEnergy();
          } else {
	    Rcpp::warning("Could not find structure with connectivity " + connectivity2 + " that emerges through additional base pairing from " + connectivity);
          }
          if (verbose > VERBOSE_QUIET) { // @TODO add to result list
	    Rcpp::Rcout << "TRANSITION " << connectivity << " " << connectivity2 << " " << energy << " " << energy2 << " " << dEnergy << " " << energyFinal << endl; // supercedes approach compared to "Transition"
          // StrenumTools::writeLine(Rcpp::Rcout, structure2);
	  }
        }
        string connectivity2 = structure.getStrandConnectivityHash();
        if (optimizationMode != 0) {
          ResidueStructure structureOpt = localOptimizationProtocoll(structure, stemsTiny);
          string structureFileNameOpt = StrenumTools::sanitizeFileName(outFileBase + "_best_" + connectivity2 + "_opt.ct");
          if (writeFileMode) {
           ofstream structureFileOpt(structureFileNameOpt.c_str());
           ERROR_IF(!structureFileOpt, "Error opening optimized best structure file: " + structureFileNameOpt );
           structureOpt.writeCT(structureFileOpt);
	   if (verbose > VERBOSE_QUIET){
	     Rcpp::Rcout << "# Saving lowest free energy (after local opimization) of structure with connectivity " << connectivity2 << " and free energy " << structureOpt.computeAbsoluteFreeEnergy() << " to file " << structureFileNameOpt << endl;
           }
           if (temperatureC != temperatureC2){ 
	     if (verbose > VERBOSE_QUIET) {
	       Rcpp::Rcout << "# Free energy at temperature " << temperatureC2 << " C: " << structureOpt.computeAbsoluteFreeEnergy(temperatureC2 + CELSIUS_TO_KELVIN) << " kcal/mol " << endl;
	     }
           }
           structureFileOpt.close();
          }
        }
      } else {
	Rcpp::warning("Could not find best structure corresponding to connectivity " + connectivity);
      }
    }
    resultList["connectivities"] = connectivitiesList;
    // } outFileBase.size() > 0
    //    Rcpp::Rcout << "# Complex concentrations (not normalized):" << endl;
    // for (auto it = complexConcentrations.begin(); it != complexConcentrations.end(); it++) {
    // Rcpp::Rcout << it->first << "\t" << it->second << endl;
    // }
    if (saveModeInt > 0) {
THROWIFNOT(complexSet.size() == complexConcentrations.size(),"Assertion violation in Strenum9.h :  L2173");
      // for (auto it = complexSet.begin(); it != complexSet.end(); it++) {
      //   complexConcentrations[*it] /= partitionFunction;
      // }
      // unique set of connectivities:
      if (verbose > VERBOSE_QUIET) {
	Rcpp::Rcout << "# Unique complexes:" << endl;
      }
      Vec<RankedSolution3<string> > rankedConcentrations;
      for (auto it = complexConcentrations.begin(); it != complexConcentrations.end(); it++) {
        rankedConcentrations.push_back(RankedSolution3<string>(it->second, it->first));
      }
      sort(rankedConcentrations.begin(), rankedConcentrations.end());
      reverse(rankedConcentrations.begin(), rankedConcentrations.end());
      Rcpp::CharacterVector rankedConcNameNV;
      Rcpp::NumericVector rankedConcNV;
      for (size_t i = 0;i < rankedConcentrations.size(); ++i) {
	if (verbose > VERBOSE_QUIET) {
	  Rcpp::Rcout << "Complex " << rankedConcentrations[i].second << "\t" << rankedConcentrations[i].first/1000000.0 << " mol/l" << endl; // careful with units: internal micromol/l, report mol/l
	}
        rankedConcNameNV.push_back(rankedConcentrations[i].second);
        rankedConcNV.push_back(rankedConcentrations[i].first/1000000.0);
      }
      resultList["complex_concentrations"] = Rcpp::DataFrame::create(Rcpp::Named("Complex")=rankedConcNameNV, Rcpp::Named("Concentration")=rankedConcNV);
      // normalize by completely dissociated state (intra-strand folding allowed):
      double rtlogp0 = 0.0;
      bool found = false;
      for (size_t i = 0; i < partialProbabilities.size(); ++i) {
        // Rcpp::Rcout << "Checking topology " << uniqConnectivities[i] << " for absence of _" << " " << partialProbabilities[i] << " " << log(partialProbabilities[i]) << endl;
        if (findPositions(uniqConnectivities[i], '_').size() == 0) { // connectivity without direct strand interactions
          rtlogp0 =  - RT * log(partialProbabilities[i]);
          found = true;
          // Rcpp::Rcout << "found!" << endl;
          break;
        }
      }
      if (!found) {
        Rcpp::Rcout << "# Warning: no completely dissociated state found." << endl;
      }
      if (verbose > VERBOSE_QUIET) {
	Rcpp::Rcout << "# Unique connectivities: " << uniqConnectivities;
	Rcpp::Rcout << "# Expected values and free energies w.r.t. dissociated state:" << endl;
	Rcpp::Rcout << "# -RT log p_diss = " << rtlogp0 << endl;
	Rcpp::Rcout << "# Expected number of unpaired bases: " << strands.getConcatSequence().size()-2*bpExpect << " fraction: " << ((strands.getConcatSequence().size()-2*bpExpect)/strands.getConcatSequence().size()) << endl;
	Rcpp::Rcout << "# Name Connectivity Probability -RT*log(p) dG -TdS" << endl;
      }
      Rcpp::CharacterVector configurationsCV;
      Rcpp::NumericVector partialProbabilitiesNV;
      for (size_t i = 0; i < partialProbabilities.size(); ++i) {
        // partialProbabilities[i] /= partitionFunction; // is already normalized earlier
        double G = std::numeric_limits<double>::max();
        if (partialProbabilities[i] > 0.0) {
          G = - RT * log(partialProbabilities[i]);
        }
        double G0 = G - rtlogp0;
        if (verbose > VERBOSE_QUIET) {
	  Rcpp::Rcout << "Configuration " << uniqConnectivities[i] << " " << partialProbabilities[i] << " " << G << " " << G0 << " " << (modelG.getTemperatureK() * connectivityEntropies[i]) << endl;
	}
        configurationsCV.push_back(uniqConnectivities[i]);
        partialProbabilitiesNV.push_back(partialProbabilities[i]);
      }
      resultList["ensemble_probabilities"] = Rcpp::DataFrame::create(Rcpp::Named("Configuration")=configurationsCV,
                                               Rcpp::Named("Probability")=partialProbabilitiesNV);
      if (kineticsMode == 0) {
        if (verbose > VERBOSE_QUIET) {
          Rcpp::Rcout << "# No kinetics analysis performed." << endl << "Good bye!" << endl;
        }
        if (resultList.size() == 0) {
          Rcpp::warning("Really strange: secondary structure prediction returns empty list.");
        }
        return resultList;
      }
      /***** Kinetics analysis *******/
      DEBUG_MSG("Starting kinetics analysis.");      

      // for (auto it = transitionStatesSet.begin(); it != transitionStatesSet.end(); it++) {
      for (auto it = transitionStatesIds.begin(); it != transitionStatesIds.end(); it++) {
        string transition = it->first;
        size_t i = it->second;
	if (verbose > VERBOSE_QUIET) { // @TODO add to result list
	  Rcpp::Rcout << "Transition " << transition << " " << (i+1) << " " << stateProbabilities[i] << " " << savedG[i] << " ";
	  if (bpOutMode > 0 && (transitionStatesFull.find(transition) != transitionStatesFull.end())) {
	    // ResiduePairing pairing = createResiduePairing(&strands, &modelG, savedStates[i], subStems);
	    ResidueStructure structureG = transitionStatesFull[transition]; // ResidueStructureTools::buildStructure(pairing);
	    // if (placeSmallHelicesMode && (pairing.countBasePairs() > 0) && savedStates[i].smallHelices) {	// add small stems; special case: no small stems are added to completely unfolded structure
	    //   structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
	    //   pairing = structureG.convertFromResidueStructure(&strands, &modelG);
	    
	    // }
	    StrenumTools::writeLine(Rcpp::Rcout, structureG);
	    Rcpp::Rcout << " :";
	  }
	  StrenumTools::writeLine(Rcpp::Rcout, savedStates[i]);
	  Rcpp::Rcout << endl;
	}
      }
      
      Vec<ResiduePairing> pairings(savedStates.size());
      for (size_t i = 0; i < savedStates.size(); ++i) {
        pairings[i] = createResiduePairing(&strands, &modelG, savedStates[i], subStems);
      }
      // #ifndef NDEBUG
      //     Rcpp::Rcout << "# List of all stored transition states:" << endl;
      //     for (auto it = transitionStates.begin(); it != transitionStates.end(); it++) {
      //       Rcpp::Rcout << it->first << "\t" << (it->second+1) << endl;
      //     }
      // #endif
      if (verbose > VERBOSE_INFO) {
        Rcpp::Rcout << "# State transition matrix: " << endl;
        for (size_t j = 0; j < savedStates.size(); ++j) {
          Rcpp::Rcout << (j+1)%10 << " ";
        }
        Rcpp::Rcout << endl;
        for (size_t i = 0; i < savedStates.size(); ++i) {
          Rcpp::Rcout << (i+1) << " ";
          for (size_t j = 0; j < savedStates.size(); ++j) {
            Rcpp::Rcout << StrenumTools::kineticHalfHelixStateChange(savedStates[i], savedStates[j], subStemCounts)[0] << " ";
          }
          Rcpp::Rcout << endl;
        }
      }
      
      for (size_t i = 0; i < uniqConnectivities.size(); ++i) {
        DEBUG_MSG("Mark 529");
        size_t lowestI = 0;
        double scoreI = 0.0;
        // serch for minimum free energy structure with that connectivity:
        bool found = false;
        for (size_t k = 0; k < savedConnectivities.size(); ++k) {
          if (savedConnectivities[k] == uniqConnectivities[i]) { //  && (isIsolated[k] == 0)) {
            lowestI = k;
            scoreI = savedG[k];
            found = true;
            break;
          }
        }
        if (!found) {
	  Rcpp::warning("Cannot find any state for connectivity " + uniqConnectivities[i]);
          continue;
        }
        for (size_t k = lowestI+1; k < savedConnectivities.size(); ++k) {
          if ((savedConnectivities[k] == uniqConnectivities[i])) { // && (isIsolated[k]==0)) {
            if (savedG[k] < scoreI) {
              lowestI = k;
              scoreI = savedG[k];
            }
          }
        }
        // occupancy matrix of lowest state:
        Vec<Vec<size_t> > occMatrix(concatSequence.size(), Vec<size_t>(concatSequence.size(), 0));
THROWIFNOT(savedConnectivities[lowestI] == uniqConnectivities[i],"Assertion violation in Strenum9.h :  L2322");
        const state_t lowIState = savedStates[lowestI];
        for (size_t ii = 0; ii < lowIState.size(); ++ii) {
          if (lowIState[ii] > 0) {
            const Stem& stem = subStems[ii][lowIState[ii]-1];
            for (int k = 0; k < stem.getLength(); ++k) {
              occMatrix[stem.getStart()+k][stem.getStop()-k] = 1;
            }
          }
        }
        for (size_t j = 0; j < uniqConnectivities.size(); ++j) {
          if (i == j) {
            continue;
          }
          size_t lowestJ = 0;
          double scoreJ = 0.0;
          bool found2 = false;
          for (size_t k = 0; k < savedConnectivities.size(); ++k) {
            if ((savedConnectivities[k] == uniqConnectivities[j])) { // && (isIsolated[k]==0)) {
              lowestJ = k;
              scoreJ = savedG[k];
              found2 = true;
              break;
            }
          }
          if (!found2) {
	    Rcpp::warning("Cannot find any state for connectivity " + uniqConnectivities[j]);
            continue;
          }
          for (size_t k = lowestJ+1; k < savedConnectivities.size(); ++k) {
            if ((savedConnectivities[k] == uniqConnectivities[j])) { // && (isIsolated[k] == 0)) {
              if (savedG[k] < scoreJ) {
                lowestJ = k;
                scoreJ = savedG[k];
              }
            }
          }
          // 	if (savedG[lowestI] < savedG[lowestJ]) {
          // 	  continue;
          // 	}
          // 	if (savedG[lowestI] == savedG[lowestJ]) {
          // 	  if (i > j) {
          // 	    continue; // do not run twise
          // 	  }
          // 	}
          Vec<int> path;
          // bool forward = true;
          Vec<size_t> compStateIds;
          compStateIds.push_back(lowestI);
          compStateIds.push_back(lowestJ);
          // Vec<size_t> sameHelixStateIds;
          if (verbose > VERBOSE_QUIET) {
            Rcpp::Rcout << "# Working on connectivity pair " << uniqConnectivities[i] << " " << uniqConnectivities[j] << endl;
          }
          // StrenumTools::findSameHelixStates(savedStates, compStateIds, sameHelixStateIds, newIds, subStems, strands.getConcatSequence().size()); // from new ids to original ids
          // only consider states that are compatible with considered start and end state
          // find transition states for those states
          // for each transition state, only consider those states that are compatible with start and transition state
          string connectivityPairHash = uniqConnectivities[i] + connDelim + uniqConnectivities[j];
          for (size_t ii = 0; ii < connectivityPairHash.size(); ++ii) { // change vertical bar to percent to make it easier to use that string as part of a filename
            if (connectivityPairHash[ii] == '|') {
              connectivityPairHash[ii] = '%';
            }
          }
          // auto itRange = transitionStates.equal_range(connectivityPairHash);
          // auto lowIt = std::lower_bound(transitionStates.begin(), transitionStates.end(), connectivityPairHash); // find all transition states
          // auto upIt  = std::upper_bound(transitionStates.begin(), transitionStates.end(), connectivityPairHash); // find all transition states
          // Vec<RankedSolution3<size_t> > bestTransitionStates;
          // Vec<size_t> bestTransitionStates = StrenumTools::findStrictHalfHelixStates(savedStates, savedConnectivities, savedG, compStateIds, sameHelixStateIds, newIds, subStems, strands.getConcatSequence().size(), bpSlop, expandedHelicesMax, helixCountSlop); // from new ids to original ids
          double totalRate = 0.0;
          
          // 	for (size_t bt = 0; (bt < bestTransitionStates.size()) && (bt < pathReportMax); bt++) {
          // 	  size_t bestTransitionState = bestTransitionStates[bt].second;
          // 	  if (bestTransitionState == lowestI) {
          // 	    continue; // this would be no path
          // 	  }
          // 	  EASSERT(savedConnectivities[bestTransitionState] == uniqConnectivities[j]); // transition state must have target connectivity
          // 	  size_t bestT = bestTransitionState; // shorter name
          // 	  if (verbose > VERBOSE_INFO) {
          // 	    Rcpp::Rcout << "# Best transition state as determined by findStrictHighBPSameHelixStates: " << (bestTransitionState+1) << endl;
          // 	  }
          Vec<Vec<size_t> > occMatrix2 = occMatrix;
          const state_t& tState = savedStates[lowestJ];
          for (size_t ii = 0; ii < tState.size(); ++ii) {
            if (tState[ii] > 0) {
              const Stem& stem = subStems[ii][tState[ii]-1];
              for (int k = 0; k < stem.getLength(); ++k) {
                occMatrix2[stem.getStart()+k][stem.getStop()-k] = 1;
              }
            }
          }
          Vec<size_t>  origIds; //    = sameHelixStateIds;
          // now find all states that are compatible with this state:
          Vec<size_t> newIds(savedStates.size(), savedStates.size());
          ERROR_IF(savedStates.size() != savedConnectivities.size(), "Internal error Strenum9cc-1224");
          for (size_t kk = 0; kk < savedStates.size(); ++kk) {
            // consider only orginal topology, later add transition state
            // if (((savedConnectivities[kk] == uniqConnectivities[i]) && isStateCompatible(occMatrix2, savedStates[kk], subStems)) {	      
            if (isStateCompatible(occMatrix2, savedStates[kk], subStems)) {	      
              bool bad = false;
              // one more check: it should never occur, that a base pair that is present in the final structure but not the transition state is set:
              // 	      for (size_t ii = 0; ii < savedStates[kk].size(); ++ii) {
              // 		if ((pairings[kk][ii] != pairings[lowestJ][ii]) && (pairings[kk][ii] == pairings[lowestJ][ii])) {
              // 		  bad = true;
              // 		  break;
              // 		}
              // 	      }
              if (!bad) {
                newIds[kk] = origIds.size();
                origIds.push_back(kk);
              }
            }
          }
          // newIds[lowestJ] = origIds.size();
          // origIds.push_back(lowestJ); // also save transition state as only state with target topology uniqConnectivities[j]
          Vec<size_t> sameHelixStateIds = origIds;
          if (sameHelixStateIds.size() == savedStates.size()) {
	    Rcpp::warning("No reduction of the number of states achieved.");
          }
          if (sameHelixStateIds.size() == 0) {
	    Rcpp::warning("Could not find direct path between start and end connectivity - skipping case.");
            continue;
          }
          Vec<state_t> compStates = getSubset(savedStates, sameHelixStateIds);
          Vec<double>  compG      = getSubset(savedG, sameHelixStateIds);
          Vec<string> compConnectivities = getSubset(savedConnectivities, sameHelixStateIds);
	  if (verbose > VERBOSE_QUIET) {
	    Rcpp::Rcout << "# Analyzing " << compStates.size() << " states and connectivities " << savedConnectivities[lowestI] << " and " << savedConnectivities[lowestJ] << endl;
	  }
          double rateScale = 1.0; 
          // 	  Vec<ScoredPath> compScoredStates;
          // 	  compScoredStates.reserve(compStates.size());
          // 	  for (size_t k = 0; k < compStates.size(); ++k) {
          // 	    compScoredStates.push_back(ScoredPath(compG[k], compStates[k]));
          // 	  }
          // 	  bool beyondDijkstra = false;
          // 	  if (beyondDijkstra) { // experimental graph algorithm - still needs improvement
          // 	    size_t fastPathMax = 10;
          // 	    size_t fastPathScoreLimit = 1000;
          // 	    Timer fastShortPathTimer;
          // 	    set<size_t> tabooIds;
          // 	    fastShortPathTimer.start();
          // 	    Rcpp::Rcout << "# Starting findScoredPaths ..." << endl;
          // 	    Vec<ScoredPath> bestPaths = findScoredPaths(compScoredStates, newIds[lowestI], newIds[bestTransitionState], fastPathMax, fastPathScoreLimit,
          // 							HelixCompatibilityChecker(),BasePairChangeScorer(rateScale, modelG.getRT()), tabooIds);
          // 	    Rcpp::Rcout << "# Finished findScoredPaths." << endl;
          // 	    fastShortPathTimer.stop();
          // 	    if (bestPaths.size() > 0) {
          // 	      Rcpp::Rcout << "Best path (fast algorithm):" << bestPaths[0];
          // 	    } else {
          // 	      Rcpp::Rcout << "Best path (fast algorithm):" << " No solution found." << endl;
          // 	    }
          // 	    Rcpp::Rcout << "# Timer for fast short path algorithm: " << fastShortPathTimer << endl;
          // 	  }
          Timer dijkstraTimer1, dijkstraTimer2;
          dijkstraTimer1.start();
          set<string> testConnectivities;
          for (size_t ii = 0; ii < compConnectivities.size(); ++ii) {
            testConnectivities.insert(compConnectivities[ii]);
          }
          size_t tcn = testConnectivities.size();
          if (tcn < 2) {
            if (tcn == 0) {
              Rcpp::Rcout << "# Warning: strange case there are no states with that connective." << endl;
            } else if (tcn == 1) {
              Rcpp::Rcout << "# Warning: strange case there is one connective instead of two." << endl;
            }
            continue;
          }
          Graph2 graph = StrenumTools::createHalfHelixGraphFromStates2(compStates, compG, subStemCounts, subStems, savedStates[lowestI], savedStates[lowestJ], &strands, &modelG, rateScale);
          dijkstraTimer1.stop();
          dijkstraTimer2.start();
          if (verbose >= VERBOSE_INFO) {
            Rcpp::Rcout << "# Created graph of for " << compStates.size() << " states with " << graph.size() << " vertices." << endl;
          }
#ifndef NDEBUG
          if (verbose > VERBOSE_DETAILED) {
            for (size_t ii = 0; ii < compStates.size(); ++ii) {
              Rcpp::Rcout << compConnectivities[ii] << " " << compG[ii] << " " << compStates[ii];
            }
          }
#endif
THROWIFNOT(origIds.size() == compStates.size(),"Assertion violation in Strenum9.h :  L2504");
          if (verbose > VERBOSE_INFO) {
            Rcpp::Rcout << "# Reduced list of states:" << endl;
            for (size_t k = 0; k < compStates.size(); ++k) {
              Rcpp::Rcout << (k+1) << " orig " << (sameHelixStateIds[k]+1) << " : " << compStates[k];
            }
            Rcpp::Rcout << "# Reduced graph:" << endl;
            Rcpp::Rcout << graph << endl;
            Rcpp::Rcout << "# New ids (internal counting): " << newIds << endl;
            Rcpp::Rcout << "# Start state: " << (lowestI+1) << " subgraph id: " << (newIds[lowestI]+1) << " state: " << savedStates[lowestI];
            Rcpp::Rcout << "# Final state: " << (lowestJ+1) << " subgraph id: " << (newIds[lowestJ]+1) << " state: " << savedStates[lowestJ];
            Rcpp::Rcout << "# Graph size: " << graph.size() << endl;
          }
          if (graph.size() != compStates.size()) {
            Rcpp::Rcout << "# Warning: Graph size: " << graph.size() << " is not equal to the number of compatible states: " << compStates.size() << endl;
          }
THROWIFNOT(graph.size() == compStates.size(),"Assertion violation in Strenum9.h :  L2520");
THROWIFNOT(sameHelixStateIds.size() == compStates.size(),"Assertion violation in Strenum9.h :  L2521");
THROWIFNOT(lowestI < newIds.size(),"Assertion violation in Strenum9.h :  L2522");
          // EASSERT(bestTransitionState < newIds.size());
          if (newIds[lowestI] >= graph.size()) {
            Rcpp::Rcout << "# Warning: illegal index of start structure enounctered." << endl;
            Rcpp::Rcout << "# Reduced list of states:" << endl;
            for (size_t k = 0; k < compStates.size(); ++k) {
              Rcpp::Rcout << (k+1) << " orig " << sameHelixStateIds[k] << " : " << compStates[k];
            }
            Rcpp::Rcout << "# Reduced graph:" << endl;
            Rcpp::Rcout << graph << endl;
            Rcpp::Rcout << "# New ids (internal counting): " << newIds << endl;
            Rcpp::Rcout << "# LowestI: " << (lowestI+1) << " subgraph id: " << (newIds[lowestI]+1) << " state: " << savedStates[lowestI];
            // Rcpp::Rcout << "# Best transition state: " << (bestTransitionState+1) << " subgraph id: " << (newIds[bestTransitionState]+1) << " state: " << savedStates[bestTransitionState];
            continue;
          }
THROWIFNOT(newIds[lowestI] < graph.size(),"Assertion violation in Strenum9.h :  L2537");
THROWIFNOT(newIds[lowestJ] < graph.size(),"Assertion violation in Strenum9.h :  L2538");
THROWIFNOT(lowestI != lowestJ,"Assertion violation in Strenum9.h :  L2539");
          if (graph.size() >= 2) {
            bool found = false;
            for (size_t kk = 0; kk < graph.size(); ++kk) {
              if (kk == newIds[lowestJ]) {
                continue;
              }
              if (graph[kk].find(newIds[lowestJ]) != graph[kk].end()) { // there is edge from kk to transition state
                found = true;
                break;
              }
            }
            if ((graph[newIds[lowestI]].size() > 0) && found) { // (graph[newIds[bestTransitionState]].size() > 0)) {
              DEBUG_MSG("Starting Dijkstra algorithm.");
              // dijkstra(graph, lowestI, lowestJ, path);
              // path = findDijkstraCormen(graph, newIds[lowestI], newIds[lowestJ]);
              std::vector<graph_weight_t> min_distance, min_distanceT;
              std::vector<vertex_t> previous;//, previousT;
              DEBUG_MSG("# Mark Strenum9-1021");
THROWIFNOT(lowestI != lowestJ,"Assertion violation in Strenum9.h :  L2558");
THROWIFNOT(lowestI < newIds.size(),"Assertion violation in Strenum9.h :  L2559");
THROWIFNOT(lowestJ < newIds.size(),"Assertion violation in Strenum9.h :  L2560");
THROWIFNOT(lowestJ < newIds.size(),"Assertion violation in Strenum9.h :  L2561");
              DijkstraComputePaths(newIds[lowestI], graph, min_distance, previous);
              // DijkstraComputePaths(newIds[bestTransitionState], graph, min_distanceT, previousT); // shortest paths from transition state
              // path = DijkstraGetShortestPathTo(newIds[lowestJ], previous);
              vertex_t vertex = newIds[lowestJ];
THROWIFNOT(vertex >= 0,"Assertion violation in Strenum9.h :  L2566");
              if ((vertex < static_cast<vertex_t>(previous.size())) ) {
THROWIFNOT((vertex == -1) || ((vertex >= 0) && (vertex < static_cast<vertex_t>(previous.size()))),"Assertion violation in Strenum9.h :  L2568");
                auto tmpPath = DijkstraGetShortestPathTo(newIds[lowestJ], previous);
                path.clear();
                DEBUG_MSG("# Mark Strenum9-1060");
                for (size_t ii = 0; ii < tmpPath.size(); ++ii) {
                  if (tmpPath[ii] < static_cast<int>(compStates.size())) {
                    path.push_back(tmpPath[ii]);
                    if (ii > 0) {
                      // check if path was even possible with graph:
                      ERROR_IF(graph[tmpPath[ii-1]].find(tmpPath[ii]) == graph[tmpPath[ii-1]].end(), "Internal error: computed path is according to path not possible.");
                    }
                    
                  } else {
                    DERROR("Debug stop Strenum9cc-1357");
                  }
                }
                if (path.size() > 1) {
                  if ((path[0] == static_cast<int>(newIds[lowestJ])) && (path[path.size()-1] == static_cast<int>(newIds[lowestI]))) {
                    reverse(path.begin(), path.end());
                    DEBUG_MSG("Reversing path direction");
                    DERROR("DEBUG STOP 1374");
                  }
                  // Rcpp::Rcout << "Path " << uniqConnectivities[i] << " " << uniqConnectivities[j] << " : " << (lowestI+1) << " " << scoreI << " " << (bestTransitionState+1) << " " << scoreJ;
                }
              }
              DEBUG_MSG("# Mark Strenum9c-1564");
            } else {
              Rcpp::Rcout << "# Warning: start or end state are isolated and cannot be reached by neighoring states." << endl;
              Rcpp::Rcout << "# Start and end id: " << (lowestI+1) << " " << (lowestJ+1) << " in ids of this graph: " << (newIds[lowestI]+1) << " " << (newIds[lowestJ]+1) << endl;
#ifndef NDEBUG
              Rcpp::Rcout << "# The used sub-graph is: " << endl;
              Rcpp::Rcout << graph;
#endif
              continue;
            }
          } else {
            Rcpp::Rcout << "# Warning: Strange graph with less than 2 vertices encountered! Ignoring case." << endl;
            continue;
          }
          DEBUG_MSG("# Mark Strenum9-1115");
          dijkstraTimer2.stop();
          if (path.size() > 0) {
            Vec<state_t> combiPath; // combined path
            Vec<double> combiG;
            double timer = 0.0;
            double timerDelta = 1.0;
	    Rcpp::Rcout.precision(5);
            size_t k = 1;
            // for (size_t k = 1; k < path.size(); ++k) {
            while (k < path.size()) {
              // find path segment from k-1 to k2
              // size_t k2 = StrenumTools::findRelevantStates(compStates, path, k, subStems); // overlapping helices lead to combined "branch migration" states
              // EASSERT(k2 < path.size());
              Vec<state_t> relevantStates;
              // for (size_t k3 = k-1; k3 <= k2; k3++) {
              size_t k3 = k-1;
              // do {
              // 	relevantStates.push_back(compStates[path[k3]]);
              // } while (++k3 < path.size() && (!StrenumTools::isBaseState(compStates[path[k3])));
              // for (size_t k3 = k-1; k3 <= k; k3++) {
THROWIFNOT((k == 1) || StrenumTools::isBaseState(compStates[path[k-1]]),"Assertion violation in Strenum9.h :  L2628");
              relevantStates.push_back(compStates[path[k-1]]);
              // first and last state of sub-path consist ony of fully extended helices
              size_t minBPCount = 99999;
              for (k3 = k; (k3 < path.size()) && (!StrenumTools::isBaseState(compStates[path[k3]])); k3++) {
THROWIFNOT(!StrenumTools::isBaseState(compStates[path[k3]]),"Assertion violation in Strenum9.h :  L2633");
                relevantStates.push_back(compStates[path[k3]]);
              }
              if (k3 < path.size()) {
THROWIFNOT(StrenumTools::isBaseState(compStates[path[k3]]),"Assertion violation in Strenum9.h :  L2637");
                relevantStates.push_back(compStates[path[k3]]);
                ++k3;
              }
              auto& subStart   = relevantStates[0];  // auto& subStart = compStates[path[k-1]];
              auto& subEnd   = relevantStates[relevantStates.size()-1]; // compStates[path[k]];
              
              for (size_t ii = 0; ii < relevantStates.size(); ++ii) {
                size_t bpCount = StrenumTools::countBasePairs(relevantStates[ii], subStems);
                if (bpCount < minBPCount) {
                  minBPCount = bpCount;
                }
              }
              
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# Considering " << relevantStates.size() << " relevant states for path segment." << endl;
                ERROR_IF(relevantStates.size() < 2, "Internal error Strenum9cc-1488");
                Rcpp::Rcout << "# segment start state: " << relevantStates[0];
                Rcpp::Rcout << "# segment end state: "   << relevantStates[relevantStates.size()-1];
                Rcpp::Rcout << "# Finding intermediate path between " << (k-1+1) << " and " << (k3) << " of path " << path << "between " << subStart << " and " << subEnd;
                Rcpp::Rcout << "# Relevant landmark states:" << endl;
                for (size_t ii = 0; ii < relevantStates.size(); ++ii) {
                  Rcpp::Rcout << relevantStates[ii];
                }
              }
              if (!StrenumTools::isBaseState(subStart)) {
                if (verbose > VERBOSE_INFO) {
                  Rcpp::Rcout << "# WARNING: Start state of path segment consist of partially folded helices." << endl;
                }
              }
              if (!StrenumTools::isBaseState(subEnd)) {
                if (verbose > VERBOSE_INFO) {
                  Rcpp::Rcout << "# WARNING: End state of path segment consist of partially folded helices." << endl;
                }
              }
THROWIFNOT((k == 1) || StrenumTools::isBaseState(subStart),"Assertion violation in Strenum9.h :  L2672");
THROWIFNOT((k3 == path.size()) || StrenumTools::isBaseState(subEnd),"Assertion violation in Strenum9.h :  L2673");
              // EASSERT(k2 + 1 > k); // must increase to avoid infinite loop
              // k = k2 + 1; // AVOID USING K FOR REST OF WHILE LOOP
              bool includeStatesMode = true; // original states are at beginning of subStates array
              Vec<state_t> subStatesRaw = StrenumTools::generateSubstates(relevantStates, subStems, includeStatesMode);
              if (subStatesRaw.size() > (10*subStateMax)) {
                //		Rcpp::Rcout << "# Warning: the number of initial sub-states is deemed too high for the shortest-path analysis: " << subStatesRaw << ". Skipping path!" << endl;
                goto END_OF_PATH;
              }
              // unfortunately, we have to compute energies:
              // 	      if (verbose > VERBOSE_INFO) {
              // 		      Rcpp::Rcout << "# The generated raw substates are:" << endl;
              // 		      Rcpp::Rcout << subStatesRaw;
              // // 		for(size_t m = 0; m < subStatesRaw.size(); ++m) {
              // // 		  Rcpp::Rcout << "# " << subStatesRaw[m];
              // // 		}
              // 	      }
              Vec<double> subStatesG;
              Vec<state_t> subStates;
              // DEBUG_MSG("# Mark Strenum9-1083");
              // Vec<double> subStateConnectivities(subStates.size(), "INFEASIBLE_STRUCTURE");
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << "# Evaluating sub-states ...";
              }
              for (size_t l = 0; l < subStatesRaw.size(); ++l) {
                
                size_t bpCount = StrenumTools::countBasePairs(subStatesRaw[l], subStems);
                if ((bpCount + bpLeeMax) < minBPCount) {
                  if (verbose > VERBOSE_INFO) {
                    Rcpp::Rcout << "# Skipping transition structure " << (l+1) << " because it had only " << bpCount << " base pair compared to the minimum of " << minBPCount << " : " << subStatesRaw[l];
                  }
                  continue;
                } else {
                  if (verbose > VERBOSE_DETAILED) {
                    Rcpp::Rcout << "# Not skipping structure with " << bpCount << " base pairs; " << subStatesRaw[l];
                  }
                }
                
                ResiduePairing newStructureG(&strands, &modelG);
                bool isOK = true;
                for (size_t m = 0; m < subStatesRaw[l].size(); ++m) {
                  size_t digit = subStatesRaw[l][m];
                  if (digit > 0) {
                    Stem& stem = subStems[m][digit-1];
                    if (newStructureG.isPlaceable(stem) && newStructureG.isNewBasePairOK(stem.getStart(), stem.getStop() ) ) {
                      newStructureG.setStem(stem);
                    } else {
                      // throw(1); // throw excaption
                      // this can happen, if - for example - the "no pseudoknot" mode is in effect
                      if (verbose > VERBOSE_INFO) {
                        Rcpp::Rcout << "# Skipping sub-structure: " << subStatesRaw[l];
                      }
                      isOK = false;
                      break;
                      // goto TRY_NEW_STRUCTURE; // this sub-structure was no good
                    }
                  }
                }
                if (isOK) {
                  try {
                    // DEBUG_MSG("# Mark Strenum9-1088");
                    ResidueStructure structureG = ResidueStructureTools::buildStructure(newStructureG);
                    if (placeSmallHelicesMode) {
                      double newStructureGEnergyInitial =  structureG.computeAbsoluteFreeEnergy();
                      size_t helixCount1 = structureG.getHelixCount();
                      structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
                      double newStructureGEnergy2 =  structureG.computeAbsoluteFreeEnergy();
                      size_t helixCount2 = structureG.getHelixCount();
                      if (verbose > VERBOSE_DETAILED) {
			Rcpp::Rcout << "# Placed " << (helixCount2-helixCount1) << " helices leading to change in free energy from " 
                             << newStructureGEnergyInitial << " to " << newStructureGEnergy2 << " !" << endl;
                      }
                      
                    }
                    double newStructureGEnergy = structureG.computeAbsoluteFreeEnergy();
                    if (newStructureGEnergy < bestStructureFreeEnergy) {
                      bestStructure = structureG;
                      bestStructureFreeEnergy = newStructureGEnergy;
THROWIFNOT(savedG.size()>0,"Assertion violation in Strenum9.h :  L2751");
THROWIFNOT(savedG[minElement(savedG)] == bestStructureFreeEnergy,"Assertion violation in Strenum9.h :  L2752");
                    }
                    string connectivity = newStructureG.getStrandConnectivityHash();
                    if (newStructureGEnergy < bestConnectivityScores[connectivity]) {
                      bestConnectivityStructures[connectivity] = structureG;
                      bestConnectivityScores[connectivity] = newStructureGEnergy;
                      bestConnectivityIds[connectivity] = savedG.size(); // not sure it id is OK
                    }
                    subStatesG.push_back(newStructureGEnergy);
                    subStates.push_back(subStatesRaw[l]);
                    // subStateConnectivities[l] = newStructureG.getStrandConnectivityHash();
                    // DEBUG_MSG("# Mark Strenum9-1103");
                  } catch (int e) {
                    if (verbose > VERBOSE_DETAILED) {
                      DEBUG_MSG("Caught distance constraint exception for substate!");
                    }
                    continue;
                  }
                }
              }
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << " done." << endl;
              }
              DEBUG_MSG("# Mark Strenum9-1111");
              if (verbose > VERBOSE_DETAILED) {
                Rcpp::Rcout << "# The generated substates are:" << endl;
                for(size_t m = 0; m < subStates.size(); ++m) {
                  Rcpp::Rcout << "# " << subStatesG[m] << " " << subStates[m];
                }
              }
              size_t fromId = subStates.size();
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << "# Searching for start state among substates ... ";
              }
              for (size_t ii = 0; ii < subStates.size(); ++ii) { // TODO linear search for something already known is not very interesting
                if (subStates[ii] == subStart) {
                  fromId = ii;
                  break;
                }
              }
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << "done." << endl;
              }
              DEBUG_MSG("# Mark Strenum9-1125");
              if (fromId >= subStates.size()) {
                DERROR("# Warning: Could not find start state in subgraph"); // TODO need to handle this case better
                // continue;
              }
              size_t toId = subStates.size();
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << "# Searching for end state among substates ... ";
              }
              for (size_t ii = 0; ii < subStates.size(); ++ii) {
                if (subStates[ii] == subEnd) {
                  toId = ii;
                  break;
                }
              }
              if (verbose >= VERBOSE_INFO) {
		Rcpp::Rcout << "done." << endl;
              }
              ERROR_IF(toId >= subStates.size(), "Internal error: Could not find target state in subgraph");
              // filter out energetically too unlikely substates:
              double goodEnergy = subStatesG[fromId];
              if (subStatesG[toId] > goodEnergy) { // take WORSE of start and end energy as baseline
                goodEnergy = subStatesG[toId];
              }
              Vec<size_t> goodSubstatesIds;
              Vec<size_t> goodSubstatesIds2(subStates.size(), subStates.size()); // ids for converting back
              goodSubstatesIds2[fromId] = goodSubstatesIds.size();
              goodSubstatesIds.push_back(fromId);
              goodSubstatesIds2[toId] = goodSubstatesIds.size();
              goodSubstatesIds.push_back(toId);
              for (size_t ii = 0; ii < subStates.size(); ++ii) {
                if ((ii != fromId) && (ii != toId) && (subStatesG[ii] <= (goodEnergy + energyBarrierMax))) {
                  goodSubstatesIds2[ii] = goodSubstatesIds.size();
                  goodSubstatesIds.push_back(ii);
                }
              }
              if (verbose > VERBOSE_INFO) {
                Rcpp::Rcout << "# Generating subset of " << goodSubstatesIds.size() << " energetically feasible from " << subStates.size() << " sub-states...";
              }
              subStates = getSubset(subStates, goodSubstatesIds);
              subStatesG = getSubset(subStatesG, goodSubstatesIds);
              Rcpp::Rcout << "done." << endl;
              ERROR_IF(subStates.size() != subStatesG.size(), "Internal error Strenum9cc-1832");
              fromId = 0;
              toId = 1;
              if (subStates.size() > subStateMax) {
                Rcpp::Rcout << "# Warning: the number of sub-states is deemed too high for the shortest-path analysis: " << subStates.size() << " Skipping path!" << endl;
                goto END_OF_PATH;
              }
              Rcpp::Rcout << "# Creating graph from " << subStates.size() << " (sub-)states ..." << endl;
              // Graph2 subGraph = StrenumTools::createGraphFromStates(subStates, subStatesG, subStemCounts, modelG.getRT(), rateScale);
              Graph2 subGraph = StrenumTools::createTargetedGraphFromStates(subStates, subStatesG, subStemCounts, subStems, subStart, subEnd, &strands, &modelG, rateScale);
              Rcpp::Rcout << "# finished creating graph from state subset." << endl;
              // start whole new structure prediction here with substates:
              std::vector<graph_weight_t> min_distance2;
              std::vector<vertex_t> previous2;
              if (verbose >= VERBOSE_INFO) {
                Rcpp::Rcout << "# Starting shortest-path algorithm on sub-graph with " << subGraph.size() << " vertices." << endl;
              }
THROWIFNOT(fromId != toId,"Assertion violation in Strenum9.h :  L2854");
              DijkstraComputePaths(fromId, subGraph, min_distance2, previous2);
              // path = DijkstraGetShortestPathTo(newIds[lowestJ], previous);
              vertex_t vertex = toId;
THROWIFNOT(vertex >= 0,"Assertion violation in Strenum9.h :  L2858");
THROWIFNOT(previous2.size() > 0,"Assertion violation in Strenum9.h :  L2859");
              if ((vertex >= 0) && (vertex < static_cast<vertex_t>(previous2.size()))) {
THROWIFNOT((vertex == -1) || ((vertex >= 0) && (vertex < static_cast<vertex_t>(previous2.size()))),"Assertion violation in Strenum9.h :  L2861");
                Vec<int> ipath = DijkstraGetShortestPathTo(toId, previous2);
                ERROR_IF(subStates.size() != subStatesG.size(), "Internal error 1159");
                if (ipath.size() > 0) {
                  Rcpp::Rcout << "Segment " << ipath.size() << " ";
THROWIFNOT(ipath.size() > 0,"Assertion violation in Strenum9.h :  L2866");
                  ERROR_IF(ipath[0] >= static_cast<int>(subStates.size()), "Internal error Strenum9-1874");
THROWIFNOT(ipath[0] < static_cast<int>(subStates.size()),"Assertion violation in Strenum9.h :  L2868");
THROWIFNOT(ipath[0] < static_cast<int>(subStatesG.size()),"Assertion violation in Strenum9.h :  L2869");
                  state_t fstate = subStates[ipath[0]];
                  for (size_t jj = 0; jj < fstate.size(); ++jj) {
                    Rcpp::Rcout << " " << fstate[jj];
                  }
                  Rcpp::Rcout << " ;";		
                  if (combiPath.size() == 0) {
                    combiPath.push_back(subStates[ipath[0]]);
                    combiG.push_back(subStatesG[ipath[0]]);
                  } 
                  for (size_t kk = 1; kk < ipath.size(); ++kk) {
THROWIFNOT(ipath[kk] < static_cast<int>(subStates.size()),"Assertion violation in Strenum9.h :  L2880");
THROWIFNOT(ipath[kk] < static_cast<int>(subStatesG.size()),"Assertion violation in Strenum9.h :  L2881");
                    combiPath.push_back(subStates[ipath[kk]]);
                    combiG.push_back(subStatesG[ipath[kk]]);
                    ERROR_IF(ipath[kk] >= static_cast<int>(subStates.size()), "Internal error Strenum9-1891");
                    state_t state = subStates[ipath[kk]];
                    for (size_t jj = 0; jj < state.size(); ++jj) {
                      Rcpp::Rcout << " " << state[jj];
                    }
                    Rcpp::Rcout << " ;";		
                  }
                  Rcpp::Rcout << endl;
                } else {
                  Rcpp::Rcout << "@ Warning: path length zero encountered : no shortest path found" << endl;
                }
              }
THROWIFNOT(k > 0,"Assertion violation in Strenum9.h :  L2896");
THROWIFNOT(k < path.size(),"Assertion violation in Strenum9.h :  L2897");
THROWIFNOT(path[k-1] < static_cast<int>(compG.size()),"Assertion violation in Strenum9.h :  L2898");
THROWIFNOT(path[k] < static_cast<int>(compG.size()),"Assertion violation in Strenum9.h :  L2899");
              if (compG[path[k]] > compG[path[k-1]]) {
                timer += exp((compG[path[k]]-compG[path[k-1]])/RT); // time proporiional to 1/rate
              } else {
                timer += timerDelta;
              }
              k = k3; // k = k + 1 // can be longer path segment
            } // while k ...
            END_OF_PATH:
              DEBUG_MSG("# Mark Strenum9-1164");
            // Vec<state_t> combiPathT; // combined path from transition state to final state
            // Vec<double> combiGT;
            // double timerT = 0.0;
            // double timerDeltaT = 1.0;
            Rcpp::Rcout.precision(5);
            k = 1; // variable used for second while loop - not good coding style
            // for (size_t k = 1; k < path.size(); ++k) {
            Rcpp::Rcout << "# Analyzing path: " << path.size() << " considered states from start to final state." << endl;
            Vec<int> pathAll = path;
            // ERROR_IF(pathT.size() == 0, "Internal error Strenum9cc-1274");
            // ERROR_IF(path.size() == 0, "Internal error Strenum9cc-1275");
            ERROR_IF(combiPath.size() != combiG.size(), "Internal error Strenum9-1963");
            Vec<state_t> combiPathAll = combiPath;
            Vec<double> combiGAll = combiG;
            // ERROR_IF(combiPathT.size() == combiGT.size(), "Internal error Strenum9cc-1316");
            foundPaths.push_back(combiPathAll); // save path for export and subsequence processing
            Rcpp::Rcout << "Landmarks: " << pathAll.size();
            for (size_t kk = 0; kk < pathAll.size(); ++kk) {
              Rcpp::Rcout << " " << (origIds[pathAll[kk]]+1) << " " << savedG[origIds[pathAll[kk]]] << " " << savedConnectivities[origIds[pathAll[kk]]]; // << "\t" << savedStates[path[k]];
THROWIFNOT(compG[pathAll[kk]] == savedG[origIds[pathAll[kk]]],"Assertion violation in Strenum9.h :  L2928");
            }
            Rcpp::Rcout << " Detailed: " << combiPathAll.size();
            double timer2 = 0;
            ERROR_IF(combiPathAll.size() != combiGAll.size(), "Internal error Strenum9-1975");
            Vec<string> outFileNames;
            for (size_t kk = 0; kk < combiPathAll.size(); ++kk) {
              const state_t& state = combiPathAll[kk];
              for (size_t l = 0; l < state.size(); ++l) {
                Rcpp::Rcout << " " << state[l];
              }
              Rcpp::Rcout << " " << combiGAll[kk] << " ; ";
              if (kk > 0) {
                if (combiGAll[kk] > combiGAll[kk-1]) {
                  timer2 += exp((combiGAll[kk]-combiGAll[kk-1])/RT); // time proporiional to 1/rate
                } else {
                  timer2 += timerDelta;
                }
              }
              // create structure and save to disk:
              if (outFileBase.size() > 0) {
                ResiduePairing newStructureG(&strands, &modelG);
                for (size_t jj = 0; jj < state.size(); ++jj) {
                  size_t digit = state[jj];
                  if (digit > 0) {
                    Stem& stem = subStems[jj][digit-1];
                    if (newStructureG.isPlaceable(stem) && newStructureG.isNewBasePairOK(stem.getStart(), stem.getStop() ) ) {
                      newStructureG.setStem(stem);
                    } else {
                      DERROR("Internal error Strenum9cc-1706: Cannot build structure.");
                    }
                  }
                }
                ResidueStructure structureG = ResidueStructureTools::buildStructure(newStructureG);
                if (placeSmallHelicesMode) {
                  double newStructureGEnergyInitial =  structureG.computeAbsoluteFreeEnergy();
                  size_t helixCount1 = structureG.getHelixCount();
                  structureG.placeHelicesByProbability(stemsSmall, smallStemDGCutoff);
                  double newStructureGEnergy2 =  structureG.computeAbsoluteFreeEnergy();
                  size_t helixCount2 = structureG.getHelixCount();
                  if (verbose > VERBOSE_DETAILED) {
                    Rcpp::Rcout << "# Placed " << (helixCount2-helixCount1) << " helices leading to change in free energy from " 
                         << newStructureGEnergyInitial << " to " << newStructureGEnergy2 << " ....." << endl;
                  }
                  
                }
                // write to disk:
                string outName = outFileBase + "." + connectivityPairHash + "." + uitos(kk+1) + ".ct";
                outFileNames.push_back(outName);
                ofstream outFile(outName.c_str());
                ERROR_IF(!outFile, "Error opening output file " + outName);
                structureG.writeCT(outFile);
                outFile.close();
              }
            }
            Rcpp::Rcout << " Rate2: " << (1/timer2);
            Rcpp::Rcout << endl;
            if (verbose > VERBOSE_QUIET) {
              Rcpp::Rcout << "# Wrote intermediate structures to disk: " << outFileNames.size();
              for (size_t ii = 0; ii < outFileNames.size(); ++ii) {
                Rcpp::Rcout << " " << outFileNames[ii];
              }
              Rcpp::Rcout << endl;
            }
            if (timer2 <= 0.0) {
              Rcpp::Rcout << "# Warning: zero transition time encountered." << endl;
            } else {
              totalRate += 1.0/timer2;
            }
          } else {
            Rcpp::Rcout << "# No path between start and end state could be identified: " << uniqConnectivities[i] << " " << uniqConnectivities[j] << endl;
          }
          Rcpp::Rcout << "# Time for Dijkstra algorithm " << dijkstraTimer1 << " for preprocessing and " << dijkstraTimer2 << endl;
          Rcpp::Rcout << "Totalrate " << uniqConnectivities[i] << " " << uniqConnectivities[j] << " " << totalRate << endl;
        }
      }
      if (outFileBase.size() > 0) {
        string bestStructureFileName = outFileBase + "_kin_best.ct";
        ofstream bestStructureFile(bestStructureFileName.c_str());
        ERROR_IF(!bestStructureFile, "Error opening best structure file.");
        ERROR_IF(lowestOverallId >= savedStates.size(), "Internal error: undefined solution id.");
        //     ResiduePairing bestStructure2 = createResiduePairing(&strands, &modelG, savedStates[lowestOverallId], subStems);
        //     ResidueStructure bestStructure = ResidueStructureTools::buildStructure(bestStructure2);
        //     if (placeSmallHelicesMode) {
        //       bestStructure.placeHelicesByProbability(stemsSmall);
        //     }
        bestStructure.writeCT(bestStructureFile);
	if (verbose > VERBOSE_QUIET) {
         Rcpp::Rcout << "# Writing state " << (lowestOverallId+1) << " with free energy " << savedG[lowestOverallId] << " to " << bestStructureFileName << " : " << savedStates[lowestOverallId];
	}
        bestStructureFile.close();
      }
    }
    THROWIFNOT(Rcpp::as<Rcpp::List>(resultList["best_structure"]).size() > 0, "Internal error Strenum9 L2891");    
    Rcpp::Rcout << "# Good bye!" << endl;
    return resultList;
  }

#endif
