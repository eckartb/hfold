// --*- C++ -*------x---------------------------------------------------------
//
// Class:           Matrix3D
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     3x3 Matrix.
//
// -----------------x-------------------x-------------------x-----------------

#ifndef __MATRIX3D_H__
#define __MATRIX3D_H__

#include <iostream>

#include "Vector3D.h"


/** 3D Matrix class.

    @see    @Gamma et al: "Design Patterns", Bridge-Pattern */
class Matrix3D {
  // friend class Matrix4D;
public:
  Matrix3D()
  {    
    x.x(0.0); x.y(0.0); x.z(0.0);
    y.x(0.0); y.y(0.0); y.z(0.0);
    z.x(0.0); z.y(0.0); z.z(0.0);
  }

  Matrix3D(double diagonal)  {
    x.x(diagonal); x.y(0.0); x.z(0.0);
    y.x(0.0); y.y(diagonal); y.z(0.0);
    z.x(0.0); z.y(0.0); z.z(diagonal);
 }

  Matrix3D(double d1, double d2, double d3) {
    x.x(d1); x.y(0.0); x.z(0.0);
    y.x(0.0); y.y(d2); y.z(0.0);
    z.x(0.0); z.y(0.0); z.z(d3);
  }

  Matrix3D(double d1, double d2, double d3,
	   double d4, double d5, double d6,
	   double d7, double d8, double d9) {
    x.x(d1); x.y(d2); x.z(d3);
    y.x(d4); y.y(d5); y.z(d6);
    z.x(d7); z.y(d8); z.z(d9);
  }

  Matrix3D(const Matrix3D& other) {
   copy(other);
  }

  Matrix3D& operator = (const Matrix3D& other) {
    if (&other != this) {
      copy(other);
    }
    return *this; 
  }

  bool operator == (const Matrix3D& other) const { 
    return (x == other.x) && (y == other.y) && (z == other.z);
  }

  Matrix3D operator * (double scale) const { 
    Matrix3D result;
    result.x = x*scale;
    result.y = y*scale;
    result.z = z*scale;
    return result;
  }

  Vector3D operator * (const Vector3D& v) const {
    Vector3D result;
    result.x(x.x() * v.x() + x.y() * v.y() + x.z() * v.z());
    result.y(y.x() * v.x() + y.y() * v.y() + y.z() * v.z());
    result.z(z.x() * v.x() + z.y() * v.y() + z.z() * v.z());
    return result;
  }

  Matrix3D operator * (const Matrix3D& other) const { 
    Matrix3D result;
    
    result.x.x(x.x()*other.x.x() + x.y()*other.y.x() + x.z()*other.z.x());
    result.x.y(x.x()*other.x.y() + x.y()*other.y.y() + x.z()*other.z.y());
    result.x.z(x.x()*other.x.z() + x.y()*other.y.z() + x.z()*other.z.z());
    
    result.y.x(y.x()*other.x.x() + y.y()*other.y.x() + y.z()*other.z.x());
    result.y.y(y.x()*other.x.y() + y.y()*other.y.y() + y.z()*other.z.y());
    result.y.z(y.x()*other.x.z() + y.y()*other.y.z() + y.z()*other.z.z());
    
    result.z.x(z.x()*other.x.x() + z.y()*other.y.x() + z.z()*other.z.x());
    result.z.y(z.x()*other.x.y() + z.y()*other.y.y() + z.z()*other.z.y());
    result.z.z(z.x()*other.x.z() + z.y()*other.y.z() + z.z()*other.z.z());
    
    return result;
}

  Matrix3D operator + (const Matrix3D& other) const {
    Matrix3D result;
    result.x = x+other.x;
    result.y = y+other.y;
    result.z = z+other.z;
    return result;
  }

  Matrix3D operator - (const Matrix3D& other) const {
    Matrix3D result;
    result.x = x-other.x;
    result.y = y-other.y;
    result.z = z-other.z;
    return result;
  }

  Matrix3D& operator *= (double scale) {
    x *= scale;
    y *= scale;
    z *= scale;
    return *this;
  }

  Matrix3D& operator *= (const Matrix3D& other) {
    Matrix3D result;
    result.x.x(x.x()*other.x.x() + x.y()*other.y.x() + x.z()*other.z.x());
    result.x.y(x.x()*other.x.y() + x.y()*other.y.y() + x.z()*other.z.y());
    result.x.z(x.x()*other.x.z() + x.y()*other.y.z() + x.z()*other.z.z());
    
    result.y.x(y.x()*other.x.x() + y.y()*other.y.x() + y.z()*other.z.x());
    result.y.y(y.x()*other.x.y() + y.y()*other.y.y() + y.z()*other.z.y());
    result.y.z(y.x()*other.x.z() + y.y()*other.y.z() + y.z()*other.z.z());
    
    result.z.x(z.x()*other.x.x() + z.y()*other.y.x() + z.z()*other.z.x());
    result.z.y(z.x()*other.x.y() + z.y()*other.y.y() + z.z()*other.z.y());
    result.z.z(z.x()*other.x.z() + z.y()*other.y.z() + z.z()*other.z.z());
    
    copy(result);

    return *this;
  }
  
  Matrix3D& operator += (const Matrix3D& other) {
    x = x+other.x;
    y = y+other.y;
    z = z+other.z;
    return *this;
  }
  
  Matrix3D& operator -= (const Matrix3D& other) {
    x = x-other.x;
    y = y-other.y;
    z = z-other.z;
    return *this;
  }
  
  Matrix3D operator - () const {  
    Matrix3D result;
    result.x = -x;
    result.y = -y;
    result.z = -z;
    return result;
  }

  const Vector3D& getX() const { return x; }

  const Vector3D& getY() const { return y; }

  const Vector3D& getZ() const { return z; }


  /* FRIENDS */

  // friend Matrix3D inverse(const Matrix3D& m);

  friend Matrix3D transpose(const Matrix3D& m);

  static Matrix3D rotationMatrix(Vector3D axis, double angle);

  friend Matrix3D scaleMatrix(const Vector3D& s);

  friend ostream& operator << (ostream& os, const Matrix3D& m);

  friend istream& operator >> (istream& is, Matrix3D& m);
  
  /* STATIC */

  /** Returns (new) rotated vector. rotate around center vector (not center location but direction) */
  static Vector3D rotate(const Vector3D& v, const Vector3D& center, double angle) 
  { 
    return (rotationMatrix(center, angle)) * v; 
  }


  /* PREDICATES */

  double xx() const { return x.x(); }
  double xy() const { return x.y(); }
  double xz() const { return x.z(); }
  double yx() const { return y.x(); }
  double yy() const { return y.y(); }
  double yz() const { return y.z(); }
  double zx() const { return z.x(); }
  double zy() const { return z.y(); }
  double zz() const { return z.z(); }

  /* MODIFIER */

  void copy(const Matrix3D& other) {
    x.copy(other.x);
    y.copy(other.y);
    z.copy(other.z);
  }

private:
  Vector3D x;
  Vector3D y;
  Vector3D z;
};

Matrix3D inverse(const Matrix3D& m);

inline
Matrix3D 
transpose(const Matrix3D& m) 
{
  Matrix3D result;
  result.x.x(m.x.x());
  result.x.y(m.y.x());
  result.x.z(m.z.x());

  result.y.x(m.x.y());
  result.y.y(m.y.y());
  result.y.z(m.z.y());

  result.z.x(m.x.z());
  result.z.y(m.y.z());
  result.z.z(m.z.z());
  
  return result;
}

inline
Matrix3D 
Matrix3D::rotationMatrix(Vector3D n, double alpha)
{

  Matrix3D R(1.0); // unit matrix
  Matrix3D S;
  Matrix3D U;

  if ((n.x() == 0) && (n.y() == 0) && (n.z() == 0)) {
    return R;
  }

  n.normalize();

  Vector3D nSin = n * sin(alpha);

  S.x.x(0.0);    S.x.y(-nSin.z()); S.x.z(nSin.y()); 
  S.y.x(nSin.z());  S.y.y(0.0);    S.y.z(-nSin.x());
  S.z.x(-nSin.y()); S.z.y(nSin.x());  S.z.z(0.0);      

  U.x.x(n.x()*n.x()); U.x.y(n.x()*n.y()); U.x.z(n.x()*n.z());
  U.y.x(n.y()*n.x()); U.y.y(n.y()*n.y()); U.y.z(n.y()*n.z());
  U.z.x(n.z()*n.x()); U.z.y(n.z()*n.y()); U.z.z(n.z()*n.z());

  R -= U;

  R *= cos(alpha); 
  R += U;
  R += S;

  return R;
}

inline
Matrix3D 
scaleMatrix(const Vector3D& s)
{
  return Matrix3D(s.x(), s.y(), s.z()); ;
}

inline
ostream& 
operator << (ostream& os, const Matrix3D& m)
{
  return os << "(Matrix3D " << endl 
	    << m.x.x() << ' ' << m.x.y() << ' ' << m.x.z() 
	    << endl 
	    << m.y.x() << ' ' << m.y.y() << ' ' << m.y.z() 
	    << endl 
	    << m.z.x() << ' ' << m.z.y() << ' ' << m.z.z() 
	    << " ) ";
}

inline
istream& 
operator >> (istream& is, Matrix3D& m)
{
  double x[9];
  string tag1, tag2;

  is >> tag1 
     >> x[0] >> x[1] >> x[2]
     >> x[3] >> x[4] >> x[5]
     >> x[6] >> x[7] >> x[8]
     >> tag2;

  if ((tag1 != "(Matrix3D") || (tag2 != ")")) 
    {
      DEBUG_MSG("bad input.");
      is.clear(ios::badbit);
    }
  else
    {
      m = Matrix3D(x[0], x[1], x[2],
		   x[3], x[4], x[5],
		   x[6], x[7], x[8]);
    }

  return is;
}

/* return "matrix product" of vectors */
inline
Matrix3D
matrixProduct(const Vector3D& a, const Vector3D& b)
{
  double xx = a.x() * b.x();
  double xy = a.x() * b.y();
  double xz = a.x() * b.z();
  double yx = a.y() * b.x();
  double yy = a.y() * b.y();
  double yz = a.y() * b.z();
  double zx = a.z() * b.x();
  double zy = a.z() * b.y();
  double zz = a.z() * b.z();
  return Matrix3D(xx,xy,xz,yx,yy, yz, zx,zy, zz);
}



#endif /* __MATRIX3D_H__ */



