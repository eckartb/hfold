#ifndef _RNA_INTERACTION_TYPE_
#define _RNA_INTERACTION_TYPE_

/** this class describes the interaction between different RNA nucleotides */
class RnaInteractionType {

 public:
   enum {NO_INTERACTION = -1, UNKNOWN_SUBTYPE = 0, WATSON_CRICK = 1, WOBBLE = 2,NON_STANDARD = 3, HOOGSTEEN = 4,TERTIARY = 5, BACKBONE = 6};
  
};

#endif
