// --*- C++ -*------x---------------------------------------------------------
// $Id: Stem.h,v 1.13 2011/06/17 14:21:06 bindewae Exp $
//
// Class:           Stem
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     concept of RNA secondary structure stem
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

#ifndef __STEM_H__
#define __STEM_H__

#include <iostream>
#include "debug.h"
#include <string>
#include <math.h>
#include "Vec.h"
#include "StringTools.h"

// Includes


/** contains info about RNA secondary structure stem
    
@author Eckart Bindewald
@see    GA software of Shapiro group
@review - */

class Stem {

public:

  typedef int index_type;
  typedef unsigned int size_type;
  
  /* CONSTRUCTORS */
  
  Stem() : start(0), stop(0), length(0), energy(0.0),formationEnergy(0.0) { }

  Stem(size_type _start, 
       size_type _stop, 
       size_type _length,
       double _energy, 
       const string& s1, 
       const string& s2) 
    : start(_start), 
      stop(_stop), length(_length),
      energy(_energy), sequence1(s1), sequence2(s2),formationEnergy(0.0) {
    // PRECOND(isRnaMatch(s1, s2, true), exception);
 }

  Stem(index_type _start, index_type _stop, index_type _length): start(_start), stop(_stop),
								 length(_length), energy(0.0),formationEnergy(0.0) { }
  
  Stem(const Stem& orig) { copy(orig); }

  virtual ~Stem() { }

  /* OPERATORS */

  /** Assigment operator. */
  Stem& operator = (const Stem& orig) {
    if (&orig != this) {
      copy(orig);
    }
    return *this;
  }

  friend ostream& operator << (ostream& os, const Stem& rval);

  friend istream& operator >> (istream& is, Stem& rval);

  /* PREDICATES */

  double getFormationEnergy() const { return formationEnergy; }

  /** Return maximum difference between stem helices in terms of length, start and stop position. Interesting: if zero, it corresponds to "==" operation .*/
  index_type computeMaxDifference(const Stem& other) const {
    index_type dl = abs(other.getLength()-getLength());
    index_type dstart = abs(other.getStart()-getStart());
    index_type dstop = abs(other.getStop()-getStop());
    index_type result = dl;
    if (dstart > result) {
      result = dstart;
    }
    if (dstop > result) {
      result = dstop;
    }
    return result;
  }

  /** Is current state valid? */
  virtual bool isValid() const {
    return (length > 0);
  }

  /** Returns true, iff both strands of the stem belong to the same sequence */
  virtual bool isIntraStrand(const Vec<size_type>& segmentIds) const {
    PRECOND(static_cast<size_type>(getStart()) < segmentIds.size());
    PRECOND(static_cast<size_type>(getStop()) < segmentIds.size());
    return segmentIds[getStart()] == segmentIds[getStop()];
  }
  /** Returns true, iff both strands of the stem belong to the same sequence */
  virtual bool isIntraStrand(const Vec<index_type>& segmentIds) const {
    PRECOND(static_cast<size_type>(getStart()) < segmentIds.size());
    PRECOND(static_cast<size_type>(getStop()) < segmentIds.size());
    return segmentIds[getStart()] == segmentIds[getStop()];
  }

  /** Returns pair of strand ids that are involved in the stem */
  virtual pair<size_type, size_type> involvedStrands(const Vec<size_type>& segmentIds) const {
    return pair<size_type, size_type>(segmentIds[getStart()] ,segmentIds[getStop()]);
  }

  /** Returns pair of strand ids that are involved in the stem */
  virtual pair<size_type, size_type> involvedStrands(const Vec<index_type>& segmentIds) const {
    return pair<size_type, size_type>(static_cast<size_type>(segmentIds[getStart()]) ,static_cast<size_type>(segmentIds[getStop()]));
  }

  /** Size is equal to number of base pairs of stem. */
  virtual size_type size() const {
    return length;
  }

  static index_type computeInvariant(int _start, int _stop, bool reverseMode=true) { 
    if (!reverseMode) {
      return _stop - _start; // for "forward-matching" stems (occurs when considering matches between + and - strands)
    }
    return _start + _stop; // regular case
  }

  virtual index_type getInvariant(bool reverseMode = true) const { 
    return computeInvariant(start,stop,reverseMode);
  }

  virtual index_type getStart() const { return start; }

  virtual index_type getStop() const { return stop; }

  virtual index_type getLength() const { return length; }

  /** returns loop length */
  virtual index_type getLoopLength() const { 
    index_type result = stop - (start + length); 
    POSTCOND( result >= 0); 
    return result; 
  }

  /** returns length of sequence spanned by stem */
  virtual index_type getMaxLength() const { 
    index_type result = stop + length - start;
    POSTCOND( result >= 0); 
    return result; 
  }

  virtual double getEnergy() const { return energy; }

  virtual const string& getSequence1() const { return sequence1; }

  virtual const string& getSequence2() const { return sequence2; }

  /** returns true, if base pair rStart:rStop is contained in stem - per area - does not garuantee that given base pair is part of stem */
  virtual bool contains(index_type rStart, index_type rStop) const {
    return ((rStart >= start) && (rStart < start + length)
	    && (rStop <= stop) && (rStop > stop - length));
  }

  /** returns true, if base pair rStart:rStop is contained in stem - garuantees that given base pair is part of stem */
  virtual bool uses(index_type rStart, index_type rStop, bool reverseMode) const {
    if (computeInvariant(rStart, rStop, reverseMode) == getInvariant(reverseMode)) {
      index_type di = rStart - start;
      if ((di >= 0) && (di < length)) {
	return true;
      }
    }
    return false;
  }

  /** returns true if set base pairs of this stem is super-set (or equal) to set of base pairs of other stem. */
  virtual bool uses(const Stem& other) const {
    for (index_type i = 0; i < other.getLength(); ++i) {
      if (!uses(other.getStart()+i, other.getStop()-i, true)) {
	return false;
      }
    }
    return true;
  }

  /** returns true if at least one base pair in common between stems */
  virtual bool isOverlapping(const Stem& other) const {
    for (index_type i = 0; i < other.getLength(); ++i) {
      if (contains(other.getStart()+i, other.getStop()-i)) {
	return true;
      }
    }
    return false;
  }

  /** returns true if at least one base (not one base pair!) in common between stems */
  virtual bool hasCommonBases(const Stem& other) const {
    for (index_type i = 0; i < getLength(); ++i) {
      if (other.usesBase(start + i) || other.usesBase(stop - i)) {
	return true;
      }
    }
    return false; // no common base found
  }

  /** returns true if at least one base (not one base pair!) in common between stems. Counts bases on both strands. */
  virtual size_t countCommonBases(const Stem& other) const {
    size_t count = 0;
    for (index_type i = 0; i < getLength(); ++i) {
      if (other.usesBase(start + i) || other.usesBase(stop - i)) {
	++count;
      }
    }
    return count;
  }

  /** Returns iff two base pairs or non-nested but are "crossing" */
  static bool isCrossing(index_type start1, index_type stop1, index_type start2, index_type stop2) {
    return ((start1 < start2) && (start2 < stop1) && (stop1 < stop2))
      || ((start2 < start1) && (start1 < stop2) && (stop2 < stop1));
  }

  bool isCrossing(const Stem& other) const {
    // PRECOND(!hasCommonBases(other));
    return isCrossing(getStart(), getStop(), other.getStart(), other.getStop());
  }

  virtual bool isSimilar(const Stem& other, index_type slack) const;

  /** orders start and stop position */
  virtual index_type orderInt() const {
    return orderInt(start, stop);
  }

  /** returns true if base with index n is used in this stem */
  virtual bool usesBase(index_type n) const {
    return ( ( (start <= n) && (n <= (start + length-1)))
	     || ((stop - (length - 1) <= n) && (n <= stop)) );
  }
  
  /* MODIFIERS */
  
  virtual void clear() {
    *this = Stem();
  }

  virtual void setFormationEnergy(double value) { formationEnergy = value; }
  
  virtual void setStart( index_type n)  {  start = n; }
  
  virtual void setStop( index_type n)  {  stop = n; }
  
  virtual void setLength( index_type n)  {  length = n; }
  
  virtual void setEnergy(double e)  {  energy = e; }
  
  virtual void setSequence1(const string& s) { sequence1 = s; }
  
  virtual void setSequence2(const string& s) { sequence2 = s; }
  
  /* STATIC */
  static index_type orderInt(index_type istart, index_type istop) {
    return istart * istart + istop + istop;
  }
   
  /** returns distance in matrix space between two 2d points */
  static double pointDistance(index_type x1, index_type y1,
			      index_type x2, index_type y2)
  {
    return sqrt(static_cast<double>((x1-x2) * (x1-x2) + (y1-y2) * (y1-y2)));
  }
  
  string toString(char delim=' ') const { return "" + itos(start+1) + delim + itos(stop+1) + delim + itos(length); }
 
protected:
  /* OPERATORS  */
  
  /* PREDICATES */
  
  /* MODIFIERS  */
  
  void copy(const Stem& other) {
    start = other.start;
    stop = other.stop;
    length = other.length;
    energy = other.energy;
    sequence1 = other.sequence1;
    sequence2 = other.sequence2;
    formationEnergy = other.formationEnergy;
  }

private:

  /* OPERATORS  */

  /* PREDICATES */

  /* MODIFIERS  */

private:

  /* PRIVATE ATTRIBUTES */

  index_type start;
  
  index_type stop;
  
  index_type length;
  
  double energy;
  
  string sequence1; // sequence starting at position start
  
  string sequence2; // sequence ending at position stop
  
  double formationEnergy; // can store entropic contribution -TdS for entropic penalty for forming this base pair

};

inline
ostream& operator << (ostream& os, const Stem& rval)
{
  os << (rval.start + 1) << " " << (rval.stop + 1) << " " 
     << rval.length << " " << rval.energy << " ";
  if (rval.sequence1.size() > 0) {
    os << rval.sequence1 << " ";
  }
  else {
    os << "- ";
  }
  if (rval.sequence2.size() > 0) {
    os << rval.sequence2 << " ";
  }
  else {
    os << "- ";
  }
  return os;
}

inline
istream& operator >> (istream& is, Stem& rval)
{
  rval.clear();
  string seq1, seq2;
  is >> rval.start >> rval.stop >> rval.length >> rval.energy 
     >> seq1 >> seq2;
  if (seq1.compare(string("-")) != 0) {
    rval.setSequence1(seq1);
  }
  if (seq2.compare(string("-")) != 0) {
    rval.setSequence2(seq2);
  }
  return is;
}

inline 
bool operator < (const Stem& stemA, const Stem& stemB)
{
  if (stemA.getStart() == stemB.getStart()) {
    if (stemA.getStop() == stemB.getStop()) {
      return (stemA.getLength() < stemB.getLength());
    }
    else {
      return (stemA.getStop() < stemB.getStop());
    }
  }
  return (stemA.getStart() < stemB.getStart());
}

inline 
bool operator >= (const Stem& stemA, const Stem& stemB)
{
  return !(stemA < stemB); 

}

inline 
bool operator == (const Stem& stemA, const Stem& stemB)
{
  return (! (stemA < stemB)) && (! (stemB < stemA));
}

inline 
bool operator != (const Stem& stemA, const Stem& stemB)
{
  return (! (stemA == stemB));
}

inline 
bool operator > (const Stem& stemA, const Stem& stemB)
{
  return (stemA != stemB) && (stemA >= stemB);

}

inline
bool 
Stem::isSimilar(const Stem& other, index_type slack) const
{
  return ((abs(getStart() - other.getStart()) <= slack)
	  && (abs(getStop() - other.getStop()) <= slack) );
}

/** useful for sorting by stem length. */
class StemLengthComparator {

public:
  bool operator () (const Stem& stem1, const Stem& stem2) const {
    return stem1.getLength() < stem2.getLength();
  }

};

class StemEnergyComparator {

 public:

  bool operator() (const Stem& stem1, const Stem& stem2) const {
    return stem1.getEnergy() < stem2.getEnergy();
  }

};

#endif /* __STEM_H__ */

