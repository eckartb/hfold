#ifndef RESIDUE_STRUCTURE_RCPP_TOOLS_H
#define RESIDUE_STRUCTURE_RCPP_TOOLS_H

#include <Rcpp.h>
#include "RcppHelp.h"
#include "ResidueStructure.h"

using namespace std;
using namespace Rcpp;

class ResidueStructureRcppTools {
  
public:
  
  typedef ResidueStructure::size_t size_t;
  typedef ResidueStructure::index_t index_t;
  
  /** Convert secondary structure to data frame. Does not store information about multiple sequences.
   * offset = 0: remain in zero-based counting; offset = 1: convert to 1-based-counting */
  static Rcpp::DataFrame residueStructureToDataFrame(const ResidueStructure& structure, const string& basepairName,
                                                     int offset) {
    NumericVector ids1, ids2;
    CharacterVector basepairNames;    
    for (index_t i = 0; i < static_cast<index_t>(structure.size()); ++i) {
      if (structure.isBasePaired(i)) {
        index_t bp = structure.getBasePair(i);
        if (i < bp) {
          ids1.push_back(i  + offset);
          ids2.push_back(bp + offset);
          basepairNames.push_back(basepairName);
        }
      }      
    }
    return  Rcpp::DataFrame::create(Rcpp::Named("id1")=ids1, Rcpp::Named("id2")=ids2, Rcpp::Named("bp")=basepairNames,_["stringsAsFactors"] = false);
  }
  
  /** Convert key information from multistrand secondary structure to list akin to a "beadmodel" sensu the R package rnameso */
  static Rcpp::List residueStructureToList(const ResidueStructure& structure) {
    // Rcpp::Rcout << "# starting residueStructureToList" << endl;
    auto pairs = residueStructureToDataFrame(structure, "cWW", 1);
    CharacterVector sequences = RcppHelp::to_CharacterVector(structure.getSequences());
    NumericVector concentrations = RcppHelp::to_NumericVector(structure.getConcentrations());
    NumericVector freeEnergy(1);
    freeEnergy(0) = structure.computeAbsoluteFreeEnergy();
    Rcpp::List result  = List::create(Rcpp::Named("sequences")=sequences,
                                      Rcpp::Named("pairs") = pairs,
                                      Rcpp::Named("concentrations")=concentrations,
                                      Rcpp::Named("absolute_free_energy")=freeEnergy(0));
    // Rcpp::Rcout << "# finished residueStructureToList" << endl;
    return result;
  }
  
};

#endif
