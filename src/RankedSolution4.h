// --*- C++ -*------x---------------------------------------------------------
// $Id: RankedSolution4.h,v 1.1.1.1 2006/07/03 14:43:21 bindewae Exp $
//
// Class:           RankedSolution4
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     Implements ranked solutions
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

#ifndef __RANKED_SOLUTION4_H__
#define __RANKED_SOLUTION4_H__

// Includes

#include <iostream>
#include "debug.h"

template<class S, class T>
class RankedSolution4 {
public:
  RankedSolution4<S,T>() { } 

  RankedSolution4<S,T>(const S& _first, const T& _second) : first(_first),
							    second(_second) {
  }

  RankedSolution4<S,T>(const RankedSolution4<S,T>& orig) { copy(orig); }

  virtual ~RankedSolution4<S,T>() {} 

  /* OPERATORS */

  /** Assigment operator. */
  RankedSolution4<S,T>& operator = (const RankedSolution4<S,T>& orig) {
    if (&orig != this) {
      copy(orig);
    }
    return *this;
  }

  /* PREDICATES */

  /* MODIFIERS */
  void copy(const RankedSolution4<S,T>& other) {
    first = other.first;
    second = other.second;
  }

  /* ATTRIBUTES */

  S first;
  T second;

protected:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* PRIVATE ATTRIBUTES */

};

/** comparison operator. */
template<class S, class T>
inline
bool 
operator < (const RankedSolution4<S,T>& lval,
	    const RankedSolution4<S,T>& rval) { 
  return lval.first < rval.first;
}

/** comparison operator. */
template<class S, class T>
inline
bool 
operator < (const S& lval, const RankedSolution4<S,T>& rval) {
  return lval < rval.first;
}

/** comparison operator. */
template<class S, class T>
inline
bool 
operator < (const RankedSolution4<S,T>& lval, const S& rval)   {
  return lval.first < rval;
}


#endif /* __ACLASS_H__ */

