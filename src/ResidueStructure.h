#ifndef _RESIDUE_STRUCTURE_H_
#define _RESIDUE_STRUCTURE_H_

#include <string>
#include "Vec.h"
#include "ResiduePairing.h"
#include "ResidueModel.h"
#include "debug.h"
#include "vectornumerics.h"
#include "debug.h"
#include "Stem.h"
#include "StemTools.h"
#include "ConnectivityTools.h"
#include "GraphTools.h"
#include <utility>
#include "verbose.h"
#include "basepair.h"
// #include "tbb/concurrent_vector.h"
// #include "tbb/concurrent_unordered_map.h"
#include "stemhelp.h"
#include <set>
#include "RnaFormats.h"
#include "ResiduePairingTools.h"

#include "BasePairTools.h"
#include "StringTools.h"

#define HELIX_ID_NONE -1
#define EPSILON 0.1
#define NO_PAIRING_INDEX -1

// if not defined, undo information will not be stored - speedup, but untested outcome
#define STRUCTURE_UNDO_MODE 1

#define COAXIAL_STACKING_FACTOR 1.0
// 0.5

// using namespace tbb;
using namespace std;

/** Central class that represents RNA and DNA multistrand secondary structures including distance constraints. */
class ResidueStructure {

 public:

  typedef string::size_type size_t;
  typedef int index_t;
  typedef float real_t; // easily switch between float and double
  typedef pair<index_t, index_t> basepair_t;
  typedef Vec<real_t> probrow_t;
  typedef Vec<Vec<real_t> >  pairingprob_t;
  typedef Vec<pairingprob_t > pairingprob_slices_t;
  typedef map<string, pairingprob_slices_t> pairingprob_slices_map_t; // for each strand connectivity, give base pair probability matrix
/*   typedef concurrent_vector<real_t> probrow_t; */
/*   typedef concurrent_vector<concurrent_vector<real_t> >  pairingprob_t; */
/*   typedef concurrent_vector<pairingprob_t > pairingprob_slices_t; */
/*   typedef concurrent_unordered_map<string, pairingprob_slices_t> pairingprob_slices_map_t; // for each strand connectivity, give base pair probability matrix */
  // enum { DEFAULT_DIVIDER = -1 };

  enum { NO_INITIATION_ADJUST_MODE = 0, STEM_INITIATION_ADJUST_MODE = 1, UNPAIRED_INITIATION_ADJUST_MODE = 2 };

  static const int STACKING_LOOP_LEN_MAX = 3; // maximum loop length for coaxial stacking energy interactions

  static const int DISTANCE_CONSTRAINT_EXCEPTION = 11; // if minimum allowed distance is higher maximum allowed distance for a residue



 public:

  bool activeTimer;   /** Folding time is only being tracked, if activeTimer is true. Used internally for unfolding and re-folding */
  int coaxialStackingMode;
  string concatSequence; // length of concatenated sequence
  Vec<double> concentrations; // RNA strand concentrations strands in units of micromol per liter //  per Angstroem cubed //  in milli mol per liter
  map<string, double> connectivityWaitingTimes; // for each strand connectivity, sum all average simulation times  
  Vec<int> dividers; // default: -1, if greater zero: two-pot assembly (strand Id less than divider in one group, strands with ids greater or equal divider in other group
  real_t effectiveVolume; // effective volume of ensemble being simulated
  real_t energy;
  real_t entropy;
  Vec<folding_event_t> foldingEvents;
  Vec<Stem> helices;
  Vec<Vec<int> > helixIds; // if greater or equal zero: index of helix; otherwise -1
  mutable Vec<int> helix3PrimeAdj; // for helix i, gives id of 3' downstream adjacent helix. -1 is no helix there. Length of 
  int initiationAdjustMode;
  real_t initiationAdjustMultiplier;  
  map<string, size_t> interConnectivityHops; // for each strand connectivity PAIR, count all events hoping from first to second connectivity
  map<string, double> interConnectivityWaitingTimes; // for each strand connectivity PAIR, sum all average simulation times spent in first configuration
  size_t lengthSum; // total number of residues
  Vec<Vec<real_t> > maxDists;
  Vec<Vec<real_t> > minDists;
  Vec<Vec<real_t> > occupancies; // average occupancies of each interaction
  size_t paCount; // total number of pseudo atoms:  lengthSum times number of pseudo-atom types
  Vec<int> pairv; // -1: no binding partner, otherwise index of binding partner for each residue
  size_t paTypes; // number of pseudo atom types
  Vec<Vec<real_t> > probabilities; // probability of interaction per time step
  string refAtom; // atom type name, like N1 or C4*
  ResidueModel * residueModel;
  Vec<real_t> residueProbabilities; // probability of interaction per time step
  Vec<string> sequences;
  StrandContainer strands; // (sequences, concentrations);
  real_t simulationTime;
  Vec<size_t> strandIds; // for each residue, specifies strand ID (zero-based). Must in size be equal to lengthSum. Max value is sequences.size()-1, min value is zero.
  Vec<int> starts; // positions of sequence starts; if only one sequence, contains a "zero" as first element
  Vec<Vec<set<string> > > strandConnectivity; // contains base pair hashes for each pair of strands
  Vec<Vec<real_t> > strandInteractionEnergies;
  bool trackEventMode;
  int updateDistanceMode; // = other.updateDistanceMode;a
  int verbose;

 private:
  
 public:

  /** Default constructure. Careful: will not validate. Prefer other constructors. */
 ResidueStructure() : activeTimer(true), coaxialStackingMode(1) {
  }

  /** Create secondary structure model based on sequences and concentrations (in units of strands per Angstroem cubed */
 ResidueStructure(const Vec<string>& origSequences,  const Vec<double>& _concentrations, ResidueModel * _residueModel) : coaxialStackingMode(1), strands(origSequences, _concentrations) {
    PRECOND(origSequences.size() > 0);
    PRECOND(origSequences[0].size() > 0);
    init(origSequences, _concentrations, _residueModel);
    // Rcpp::Rcout << "Strand concentrtions in ResidueStructure constructor: " << concentrations << "(1/A3)" << " " << getConcentrationsInMolPerLiter() << "(mol/l)" <<  endl;
    
THROWIFNOT(validateHelices(),"Assertion violation in L124");
THROWIFNOT(validateDistanceConstraintsMatrix(maxDists),"Assertion violation in L125");
THROWIFNOT(validateDistanceConstraintsMatrix(minDists),"Assertion violation in L126");
    // if(!validateBothConstraintMatrices()) {
    //   Rcpp::Rcout << "Sequences: " << origSequences << endl;
    //   Rcpp::Rcout << "# Minimum distances: " << endl;
    //   Rcpp::Rcout << minDists << endl;
    //   Rcpp::Rcout << "# Maximum distances: " << endl;
    //   Rcpp::Rcout << maxDists << endl;
    // }
THROWIFNOT(validateBothConstraintMatrices(),"Assertion violation in L134");
THROWIFNOT(validateStrandInteractionEnergies(),"Assertion violation in L135");
THROWIFNOT(validate(),"Assertion violation in L136");
  }

  ResidueStructure(const ResidueStructure& other) {
    copy(other);
  }

  virtual const ResidueStructure& operator = (const ResidueStructure& other) {
    if (&other != this) {
      copy(other);
    }
    return *this;
  }

  virtual ~ResidueStructure() {
    DEBUG_MSG("Calling destructor of ResidueStructure");
 }

  virtual real_t computeCoaxialStackingEnergy() const {
    DEBUG_MSG("Starting computeCoaxialStackingEnergy");
    double result = 0.0;
    if (helix3PrimeAdj.size() != helices.size()) {
      updateHelix3PrimeAdjacencies();
    }
    for (size_t i = 0; i < helices.size(); ++i) {
THROWIFNOT(i < helix3PrimeAdj.size(),"Assertion violation in ResidueStructure.h :  L161");
      index_t j = helix3PrimeAdj[i];
      if (j >= 0) {
THROWIFNOT(j < static_cast<index_t>(helices.size()),"Assertion violation in ResidueStructure.h :  L164");
THROWIFNOT(helices[i].getStart()+helices[i].getLength()-1 < static_cast<index_t>(concatSequence.size()),"Assertion violation in ResidueStructure.h :  L165");
THROWIFNOT(helices[i].getStop() +1 >= helices[i].getLength(),"Assertion violation in ResidueStructure.h :  L166");
THROWIFNOT(helices[j].getStart()+helices[j].getLength()-1 < static_cast<index_t>(concatSequence.size()),"Assertion violation in ResidueStructure.h :  L167");
THROWIFNOT(helices[j].getStop() +1 >= helices[j].getLength(),"Assertion violation in ResidueStructure.h :  L168");
	// Rcpp::Rcout << "working on helices " << helices[i] << " and " << helices[j] << endl;
	char a = concatSequence[helices[i].getStart()+helices[i].getLength()-1]; // last base pair: a:b
	char b = concatSequence[helices[i].getStop() -helices[i].getLength()+1];
	char x = concatSequence[helices[j].getStart()+helices[j].getLength()-1]; // first base pair of adjacent helix: x:y
	char y = concatSequence[helices[j].getStop() -helices[j].getLength()+1];
	// Rcpp::Rcout << "Getting stacking energy for " << (a+1) << " " << (b+1) << " " << (x+1) << " " << (y+1) << endl;
THROWIFNOT(ResidueModel::isWatsonCrick(a,b, true),"Assertion violation in ResidueStructure.h :  L175");
THROWIFNOT(ResidueModel::isWatsonCrick(x,y, true),"Assertion violation in ResidueStructure.h :  L176");
	result += residueModel->getStackingEnergyPermissive(a,b,x,y);
      }
    }
    // Rcpp::Rcout << "# COAXIAL STACKING ENERGY TERM: " << result << " for helices " << helices;
    return result;
  }

  /** Temperature must be given in Kelvin. Units for energy are kcal/mol. The more negative the correction term, the higher the melting temperature.
   */
  virtual real_t calcFreeEnergy() const {
    // Previously, the value of freeEnergyCorrectionis -4.17: FITTED such that the duplex
    // (AG)4 / (CU)4 gives the experimentsl melting temperature of 57.6C (according to Freier, PNAS, 1986)
    // real_t freeEnergyCorrection = 0.0; // -4.17; // -3.75; // -4.17; // (fit to (AG)4/(CU)4 // -3.67; 
    // real_t freeEnergyCorrection = -3.67; 
    double T = getTemperature();
    return calcFreeEnergy(T);
    /* double S = entropy; */
    /* double result = energy - T * S; */
    /* if (coaxialStackingMode) { */
    /*   result += COAXIAL_STACKING_FACTOR * computeCoaxialStackingEnergy(); */
    /* } */
    /* if (freeEnergyCorrection != 0.0) { */
    /*   result += (getBasePairCount()-getHelixCount()) * freeEnergyCorrection; */
    /* } */
    /* return result; */
  }

  /** Compute free energy of structure for given Temperature (in Kelvin). Units for energy are kcal/mol.
   */
  virtual real_t calcFreeEnergy(double T) const {
    // Previously, the value of freeEnergyCorrectionis -4.17: FITTED such that the duplex
    // (AG)4 / (CU)4 gives the experimentsl melting temperature of 57.6C (according to Freier, PNAS, 1986)
    // real_t freeEnergyCorrection = 0.0; // -4.17; // -3.75; // -4.17; // (fit to (AG)4/(CU)4 // -3.67; 
    // real_t freeEnergyCorrection = -3.67;  The more negative the correction term, the higher the melting temperature.
    double ew = residueModel->getEntropyWeight();
    double S =  ew * entropy;
    double result = energy - T * S;
    double coaxTerm = 0.0;
    if (coaxialStackingMode) {
      coaxTerm += COAXIAL_STACKING_FACTOR * computeCoaxialStackingEnergy();
    }
    result += coaxTerm;
    if (verbose > VERBOSE_DETAILED) {
      Rcpp::Rcout << "# ResidueStructure::calcFreeEnergy at " << T << " C: energy: " << energy << " entropy: " << entropy << " coaxial stacking: " << coaxTerm 
		  << " entropy weight: " << ew << " free energy: " << result << endl;
    }  
    /* if (freeEnergyCorrection != 0.0) { */
    /*   result += (getBasePairCount()-getHelixCount()) * freeEnergyCorrection; */
    /* } */
    return result;
  }

  /** Central method for obtaining free energy */
  virtual real_t computeAbsoluteFreeEnergy() const {
    return calcFreeEnergy(); // TODO: use cache to store result
  }

  /** Compute free energy of structure given a temperature in Kelvin */
  virtual real_t computeAbsoluteFreeEnergy(double temperatureK) const {
    return calcFreeEnergy(temperatureK); // TODO: use cache to store result
  }

  /** Returns set of all base pairs */
  virtual set<basepair_t> generateBasepairs() const {
    set<basepair_t> result;
    for (size_t i = 0; i < helices.size(); ++i) {
      for (index_t j = 0; j < helices[i].getLength(); ++j) {
	result.insert(basepair_t(helices[i].getStart() + j, helices[i].getStop()-j));
      } 
    }
    return result;
  }

  const string& getConcatSequence() const { return concatSequence; }

  Vec<folding_event_t> getFoldingEvents() const { return foldingEvents; }

  const Vec<Stem>& getHelices() const { return helices; }

  size_t getHelixCount() const { return helices.size(); }

  StrandContainer getStrands() const { return strands; }

  /** Returns sum of over- and under-predicted base pairs with respect to another structure. */
  virtual size_t countBasePairDifferences(const ResidueStructure& other) const {
    size_t result = 0;
    // overpredicted in this structure w.r.t. to other structure:
    for (size_t i = 0; i < helices.size(); ++i) {
      const Stem& stem = helices[i];
      for (index_t j = 0; j < stem.getLength(); ++j) {
	if (!other.isBasePaired(stem.getStart() + j, stem.getStop() - j)) {
	  ++result;
	}
      }
    }

    // overpredicted in other structure w.r.t. to this structure:
    for (size_t i = 0; i < other.helices.size(); ++i) {
      const Stem& stem = other.helices[i];
      for (index_t j = 0; j < stem.getLength(); ++j) {
	if (!isBasePaired(stem.getStart() + j, stem.getStop() - j)) {
	  ++result;
	}
      }
    }

    return result;
  }

  /** Given absolute position of residue, find position that would correspond to a structure consisting only of the specified strands. */
  virtual index_t findSubstrandIndex(index_t pos, const Vec<size_t>& strandSubsetIds) const {
    PRECOND(pos >= 0);
    PRECOND(pos < static_cast<index_t>(getResidueCount())); 
    // Rcpp::Rcout << "# Starting findSubstrandIndex " << (pos+1) << " " << strandSubsetIds << endl;
    size_t strandId = strandIds[pos];
    index_t counter = pos - starts[strandId];
    bool found = false;
    for (size_t i = 0; i < strandSubsetIds.size(); ++i) {
      if (strandSubsetIds[i] == strandId) {
	found = true;
	break;
      }
THROWIFNOT(strandSubsetIds[i] < sequences.size(),"Assertion violation in L292");
      counter += sequences[strandSubsetIds[i]].size();
    }
    if (!found) {
      counter = -1;
    }
    return counter;
  }

  virtual size_t getResidueCount() const { return lengthSum; }

  virtual size_t size() const { return getResidueCount(); }

  /** Returns temperature in Kelvin */
  virtual real_t getTemperature() const { return residueModel->getRT() / GAS_CONSTANT; }

  virtual size_t getBasePairCount() const {
    size_t result = 0;
    for (size_t i = 0; i < helices.size(); ++i) {
      result += static_cast<size_t>(helices[i].getLength());
    }
    return result;
  }

  virtual size_t countBasePairs() const {
    return getBasePairCount();
  }

  /** Returns strand unfolded strand concentrations in micromol per liter */ // was: strands per Angstroem cubed
  virtual const Vec<double>& getConcentrations() const { return concentrations; }

  /** Returns strand unfolded strand concentrations in strands per Angstroem cubed */
  virtual Vec<double> getConcentrationsInMolPerLiter() const {
    Vec<double> cv = concentrations; // unit is strands per Angstroem cubed
    for (size_t i = 0; i < cv.size(); ++i) {
      cv[i] /= 1000000.0;
      // cv[i] *= N_PER_ANG3_TO_MICROMOL / 1000000;
    }
    return cv;
  }

  virtual const Vec<int>& getDividers() const { return dividers; }

  virtual real_t getEntropy() const { return entropy; }

  virtual Vec<index_t> getStarts() const { return starts; }

  virtual void setEntropy(real_t val) { entropy = val; }

  set<basepair_t> generateBasePairSet() const {
    set<basepair_t> result;
    for (size_t i = 0; i < helices.size(); ++i) {
      for (index_t j = 0; j < helices[i].getLength(); ++j) {
	result.insert(generate_basepair(helices[i].getStart() + j, helices[i].getStop()-j));
      }
    }
    return result;
  }

  size_t countResidues(const Vec<size_t>& strandIds) const {
    size_t result = 0;
    for (size_t i = 0; i < strandIds.size(); ++i) {
THROWIFNOT(strandIds[i] < sequences.size(),"Assertion violation in L354");
      result += sequences[strandIds[i]].size();
    }
    return result;
  }

  bool isDivided() const {
    return (dividers.size() > 0);
  }

  virtual void setCoaxialStackingMode(int mode) { coaxialStackingMode = mode; }

  virtual void setDividers(const Vec<int>& dVec) {
THROWIFNOT(dVec.size() >= 1,"Assertion violation in L367");
    for (size_t i = 0; i < dVec.size(); ++i) {
THROWIFNOT(dVec[i] < static_cast<int>(sequences.size()),"Assertion violation in L369");
THROWIFNOT(dVec[i] >= 1,"Assertion violation in L370");
    }
    dividers = dVec;
  }

  virtual real_t updateHelixEnergy(size_t helixId) {
    PRECOND(helixId < helices.size());
    PRECOND(residueModel != NULL);
    real_t result = 0.0; // HELIX_PENALTY; // 0.0;
    Stem& helix = helices[helixId];
    index_t start = helix.getStart();
    index_t stop = helix.getStop();
    real_t energyOrig = helix.getEnergy();
    result = residueModel->calcStemStackingEnergy(concatSequence, start, stop, helix.getLength());
    /* for (index_t i = 1; i < helix.getLength(); ++i) { */
    /*   char a = getResidue(start+i-1); */
    /*   char b = getResidue(stop-i+1); */
    /*   char x = getResidue(start+i); */
    /*   char y = getResidue(stop-i); */
    /*   result += residueModel->getStackingEnergy(a,b,x,y); */
    /* } */
    helix.setEnergy(result);
    real_t energyDelta = (result - energyOrig); // update energy accordingly
    energy += energyDelta;
    size_t strandId1 = strandIds[start];
    size_t strandId2 = strandIds[stop];
THROWIFNOT(strandId1 < strandInteractionEnergies.size(),"Assertion violation in L396");
THROWIFNOT(strandId2 < strandInteractionEnergies.size(),"Assertion violation in L397");
THROWIFNOT(strandId2 < strandInteractionEnergies[strandId1].size(),"Assertion violation in L398");
THROWIFNOT(strandId1 < strandInteractionEnergies[strandId2].size(),"Assertion violation in L399");
    strandInteractionEnergies[strandId1][strandId2] += energyDelta;
    strandInteractionEnergies[strandId2][strandId1] = strandInteractionEnergies[strandId1][strandId2];
    
    updateHelix3PrimeAdjacencies();

    return result;
  }

  /* virtual real_t updateHelixEnergies() {   */
  /*   real_t result = 0.0; */
  /*   for (size_t i = 0; i < helices.size(); ++i) { */
  /*     result += updateHelixEnergy(i); */
  /*   } */
  /*   return result; */
  /* } */
  
  virtual real_t computeHelixEnergySum() const  {
    real_t result = 0.0;
    for (size_t i = 0; i < helices.size(); ++i) {
      // Rcpp::Rcout << "Helix " << (i+1) << " : " << helices[i].getEnergy() << " kcal/mol : " << helices[i] << endl;
      result += helices[i].getEnergy();
    }
    return result;
  }

  virtual real_t getEnergy() const { 
    /* if (energy != computeHelixEnergySum()) { */
    /*   Rcpp::Rcout << "# Warning: sum of base pair energies is not equal helix energies: " << energy << " " << computeHelixEnergySum() << endl; */
    /* } */
    /* ASSERT(energy == computeHelixEnergySum()); */
    return energy;
  }
  
  virtual const Vec<Vec<real_t> >& getOccupancies() const { return occupancies; }

  virtual const Vec<Vec<real_t> >& getMinDists() const { return minDists; }

  virtual const Vec<Vec<real_t> >& getMaxDists() const { return maxDists; }

  /** Returns matrix with "1" for each base pair and "0" for each unpaired pair of nucleotides */
  // virtual const Vec<Vec<int> >& getPairings() const { return pairings; }

  virtual Vec<Vec<int> > getPairings() const {
    Vec<Vec<int> > result(paCount, Vec<int>(paCount, 0));
    for (size_t i = 0; i < lengthSum; ++i) {
      for (size_t i2 = 0; i2 < paTypes; ++i2) {
	size_t ic = (i*paTypes) + i2;
THROWIFNOT(ic < pairv.size(),"Assertion violation in L447");
	// bool found = false;
	for (size_t j = 0; j < lengthSum; ++j) {
	  for (size_t j2 = 0; j2 < paTypes; ++j2) {
	    size_t jc = (j*paTypes) + j2;
	    // Rcpp::Rcout << i << " " << i2 << " " << ic << " " << j << " " << j2 << " " << jc << endl;
	    // ASSERT(jc < pairings[j].size());
	    if (isBasePaired(ic,jc)) {
	      // os << (i+1) << " " << concatSequence[i] << " : " << (j+1) << " " << concatSequence[j] << " " << (strandIds[i]+1) << " " << (strandIds[j]+1) << endl;
	      result[ic][jc] = 1;
	    } 
	  }
	}
      }
    }
    return result;
  }


  /** Returns vector with adjacency (base pairing) information */
  virtual size_t getPaCount() const {
    // ASSERT(paCount == pairv.size());
    return paCount;
  }

  /** Returns vector with index of partnering base of each base pair (zero-based). */
  virtual const Vec<int>& getPairv() const { return pairv; }

  virtual const Vec<Vec<real_t> >& getProbabilities() const { return probabilities; }

  virtual const Vec<string>& getSequences() const { return sequences; }

  virtual size_t getStrandId(unsigned int residue) const { return strandIds[residue]; }

  virtual const Vec<size_t>& getStrandIds() const { return strandIds; }

  virtual const Vec<Vec<real_t> >& getStrandInteractionEnergies() const { return strandInteractionEnergies; }

  /** Return one minus pairings matrix */
  virtual Vec<Vec<int> > getUnpairings() const { 
    Vec<Vec<int> > unpairings(paCount, Vec<int>(paCount, 1)); //  = pairings;
    for (size_t i = 0; i < unpairings.size(); ++i) {
      for (size_t j = 0; j < unpairings[i].size(); ++j) {
	if (isBasePaired(i,j)) {
	  unpairings[i][j] = 0;
	}
      }
    }
    return unpairings;
  }

  /** Removes all base pairs. Also removes all helix and strandConnectivity information, energy is zero */
  virtual void clearPairings() { 
    for (size_t i = 0; i < helixIds.size(); ++i) {
      pairv[i] = NO_PAIRING_INDEX;
      for (size_t j = 0; j < helixIds[i].size(); ++j) {
	// pairings[i][j] = 0;
	helixIds[i][j] = HELIX_ID_NONE;
      }
    }
    helices.clear();
THROWIFNOT(strandConnectivity.size() == strandInteractionEnergies.size(),"Assertion violation in L508");
THROWIFNOT(strandConnectivity.size() > 0,"Assertion violation in L509");
THROWIFNOT(strandConnectivity[0].size() == strandInteractionEnergies[0].size(),"Assertion violation in L510");
    for (size_t i = 0; i < strandConnectivity.size(); ++i) {
      for (size_t j = 0; j < strandConnectivity[i].size(); ++j) {
	strandConnectivity[i][j].clear();
	strandInteractionEnergies[i][j] = 0.0;
      }
    }
    energy = 0.0;
  }

  string getStrandConnectivityHash() const {
    // DEBUG_MSG("Improved slow implementation of getStrandConnecivityHash");
    Vec<Vec<int> > matrix = getStrandConnectivityMatrix();
    return ConnectivityTools::connectivityToCombinedHash(matrix);
  }

  /** Change from array of sets to strand adjacency matrix format. Same result data type as GraphTools::graph_adjacency_matrix_t */
  virtual Vec<Vec<int> > getStrandConnectivityMatrix() const {
    size_t n = strandConnectivity.size();// number of strands
    Vec<Vec<int> > result(n, Vec<int>(n, 0));
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = (i+1); j < n; ++j) {
	if ((strandConnectivity[i][j].size() > 0)
	    || (strandConnectivity[j][i].size() > 0)) {
	  result[i][j] = 1;
	  result[j][i] = 1;
	}
      }
    }
    return result;
  }

  /** Returns true, if and only if the structures have the same strand connectivity. Does not take indirect interactions into account. */
  virtual bool hasSameStrandConnectivity(const ResidueStructure& other) const {
    return getStrandConnectivityMatrix() == other.getStrandConnectivityMatrix();
  }

  /** Returns adjacency matrix of direct and indirect strand connectivities. */
  virtual Vec<Vec<int> > getStrandIndirectConnectivityMatrix() const {
    Vec<Vec<int> > connMatrix = getStrandConnectivityMatrix();
    return ConnectivityTools::connectivityToIndirectConnectivity(connMatrix);
  }

  virtual ResidueModel* getResidueModel() const { return residueModel; }

  /** Returns sequence character (zero-based) */
  virtual char getResidue(size_t pos) const { return concatSequence[pos]; }

  virtual char getMatrixResidue(size_t pos) const { return concatSequence[pos / paTypes]; }

  virtual const Vec<string>& getSequences() { return sequences; }
  
  /** Returns total number of nucleotides */
  virtual size_t getLengthSum() const { return lengthSum; }

  virtual size_t getStrandCount() const { return sequences.size(); }

  virtual bool isHelixFirstBP(int start, int stop) {
    if (helixIds[start][stop] >= 0) {
      const Stem& helix = helices[helixIds[start][stop]];
      return (start == helix.getStart()) && (stop == helix.getStop());
    }
    return false;
  }

  virtual bool isHelixLastBP(index_t start, index_t stop) {
THROWIFNOT(start >= 0,"Assertion violation in L576");
THROWIFNOT(stop >= 0,"Assertion violation in L577");
THROWIFNOT(start < static_cast<index_t>(lengthSum),"Assertion violation in L578");
THROWIFNOT(stop < static_cast<index_t>(lengthSum),"Assertion violation in L579");
    if (helixIds[start][stop] >= 0) {
      const Stem& helix = helices[helixIds[start][stop]];
      return (start == (helix.getStart() + helix.getLength()-1)) && (stop == (helix.getStop() - helix.getLength() + 1));
    }
    return false;
  }

  /** Returns true, iff two strands with ids strandId1 and strandId2 are directly interacting via at least one base pair */
  bool isStrandPairInteracting(size_t strandId1, size_t strandId2) const {
    PRECOND(strandId1 < getStrandCount());
    PRECOND(strandId2 < getStrandCount());
    return strandConnectivity[strandId1][strandId2].size() > 0;
  }

  /** Is there a "divider" betwen two strands? */ 
  bool isStrandPairInSamePot(size_t strandId1, size_t strandId2) const {
    PRECOND(strandId1 < getStrandCount());
    PRECOND(strandId2 < getStrandCount());
    if (!isDivided()) {
      return true;
    }
    if (strandId1 > strandId2) {
      return isStrandPairInSamePot(strandId2, strandId1);
    }
    for (size_t i = 0; i < dividers.size(); ++i) {
      if ((static_cast<int>(strandId1) < dividers[i]) && (static_cast<int>(strandId2) >= dividers[i]) ) {
	return false;
      }
    }
    return true;
  }


  /** Checks whether sum of strand-strand interaction energies is equal (up to epsilon) to the total energy of the structure */
  virtual bool validateStrandInteractionEnergies() const {
    // Rcpp::Rcout << "# Starting validateStrandInteractionEnergies" << endl;
    if (strandInteractionEnergies.size() != sequences.size()) {
      Rcpp::Rcout << "# Internal error 392: strand interaction energies do not validate." << endl;
      return false;
    }
    real_t result = 0.0;
    for (size_t i = 0; i < strandInteractionEnergies.size(); ++i) {
      if (strandInteractionEnergies[i].size() != sequences.size()) {
	Rcpp::Rcout << "# Internal error 398: strand interaction energies do not validate." << endl;
	return false;
      }
      for (size_t j = i; j < strandInteractionEnergies[i].size(); ++j) {
	result += strandInteractionEnergies[i][j];
      }
    }
    if (! (fabs(result - energy) < 0.001)) {
      Rcpp::Rcout << "# Internal error 406: strand interaction energies do not validate." << endl;
      return false;
    }
    return true;
  }

  /** Rebuild pairings-matrix from helices and check whether it gives a consistent result with current pairings matrix */
  virtual bool validateHelices() const {
    // Rcpp::Rcout << "# Starting validateHelices" << endl;
    // Vec<Vec<int> > tmpPairings = pairings;
    // size_t bpCount1 = 0;
    size_t bpCount2 = 0;
    size_t bpCount3 = 0;
    for (size_t i = 0; i < pairv.size(); ++i) {
      if ((pairv[i] > static_cast<index_t>(i)) && (pairv[i] < static_cast<index_t>(pairv.size()))) {
	if (pairv[pairv[i]] != NO_PAIRING_INDEX) {
	  bpCount3++;
	}
      }
      /* for (size_t j = 0; j < pairings[i].size(); ++j) { */
      /* 	if (tmpPairings[i][j] > 0) { */
      /* 	  bpCount1++; */
      /* 	} */
      /* 	tmpPairings[i][j] = 0; */

      /* } */
    }
    // bpCount1 /= 2;
    for (size_t i = 0; i < helices.size(); ++i) {
      bpCount2 += helices[i].getLength();
      /* for (index_t j = 0; j < helices[i].getLength(); ++j) { */
      /* 	tmpPairings[helices[i].getStart() +j][helices[i].getStop() -j ] = 1; */
      /* 	tmpPairings[helices[i].getStop() -j ][helices[i].getStart() +j] = 1; */
      /* } */
    }
    /* if (! (tmpPairings == pairings)) { */
    /*   if (verbose >= VERBOSE_QUIET) { */
    /* 	// if (verbose >= VERBOSE_DETAILED) { */
    /* 	Rcpp::Rcout << "# Internal error in basepair and helix representation: " << endl; */
    /* 	Rcpp::Rcout << pairings << endl << tmpPairings << endl; */
    /* 	Rcpp::Rcout << "# Helices: " << endl; */
    /* 	Rcpp::Rcout << helices << endl; */
    /* 	Rcpp::Rcout << "# Helix ids: " << endl; */
    /* 	Rcpp::Rcout << helixIds << endl; */
    /*   } */
    /* } */
    // ASSERT(bpCount1 == bpCount2);
    // if (bpCount2 != bpCount3) {
    //   Rcpp::Rcout << "Index vector and helices do not coincide: " << bpCount3 << " " << bpCount2 << endl;
    //   Rcpp::Rcout << pairv << endl;
    //   Rcpp::Rcout << helices << endl;
    // }
THROWIFNOT(bpCount2 == bpCount3,"Assertion violation in L683");
    // bool result = tmpPairings == pairings;
    // ASSERT(result);
    return true;
  }

  /** Rebuild pairings-matrix from helices and check whether it gives a consistent result with current pairings matrix */
  virtual bool validateDistanceConstraintsMatrix(const Vec<Vec<real_t> >& mtx) const {
    // Rcpp::Rcout << "# Starting validateDistanceContraintsMatrix" << endl;
    for (size_t i = 0; i < mtx.size(); ++i) {
      if (mtx[i][i] < 0.0) {
	DEBUG_MSG("Detected negative distance constraint.");
	return false;
      }
      for (size_t j = (i+1); j < mtx[i].size(); ++j) {
	if (mtx[i][j] != mtx[j][i]) {
	  if (verbose >= VERBOSE_INFO) {
	    Rcpp::Rcout << "validating position " << (i+1) << " " << (j+1) << " : not symmetric." << endl;
	  }
	  return false;
	}
	if (strandIds[i] != strandIds[j]) {
	  continue;
	}
	continue; // for debugging: ignore further checks
THROWIFNOT(i > 0,"Assertion violation in L708");
	if ((i > 0) && (strandIds[i] == strandIds[i-1]) && (fabs(mtx[i][j]-mtx[i-1][j]) > RESIDUE_ADJ_DIST_MAX + EPSILON)) {
	  if (verbose >= VERBOSE_INFO) {
	    Rcpp::Rcout << "validating position " << (i+1) << " " << (j+1) << " vs " << (i) << " " << (j+1) << " : too far away." << mtx[i][j] << " " << mtx[i-1][j] << ": " << fabs(mtx[i][j]-mtx[i-1][j]) << endl;
	  }
	  return false; // too large distance
	}
THROWIFNOT(j > 0,"Assertion violation in L715");
	if ((j > 0) && (strandIds[j] == strandIds[j-1]) && (fabs(mtx[i][j]-mtx[i][j-1]) > RESIDUE_ADJ_DIST_MAX + EPSILON)) {
	  if (verbose >= VERBOSE_INFO) {
	    Rcpp::Rcout << "validating position " << (i+1) << " " << (j+1) << " vs " << (i+1) << " " << (j) << " : too far away." << mtx[i][j] << " " << mtx[i][j-1] << ": " << fabs(mtx[i][j]-mtx[i][j-1]) << endl;
	  }
	  return false; // too large distance
	}
      }
    }
    return true;
  }

  /** It should never happen, that a minimum distance constraint is larger than a maximum distance contraint */
  virtual bool validateBothConstraintMatrices() const {
    // DEBUG_MSG("Starting validateBothContraintMatrices");
    if (minDists.size() == 0) {
      if (verbose >= VERBOSE_FINE) {
	Rcpp::Rcout << "Minimum distance matrix size is zero!" << endl;
      }
      return false;
    }
    if (minDists.size() != maxDists.size()) {
      if (verbose >= VERBOSE_FINE) {
	Rcpp::Rcout << "Minimum and maximum distance matrices have different dimensions: " << minDists.size() << " " << maxDists.size() << endl;
      }
      return false;
    }
    for (size_t i = 0; i < minDists.size(); ++i) {
      if (minDists[i].size() != maxDists[i].size()) {
	if (verbose >= VERBOSE_FINE) {
	  Rcpp::Rcout << "Minimum and maximum distance matrices have different dimensions in row " << (i+1) << " : " << minDists[i].size() << " " << maxDists[i].size() << endl;
	}
	return false;
      }
      if (minDists[i][i] != 0.0) {
	return false;
      }
      if (maxDists[i][i] != 0.0) {
	return false;
      }
      for (size_t j = i+1; j < minDists[i].size(); ++j) {
	if (minDists[i][j] > maxDists[i][j]) {
	  // if (verbose >= VERBOSE_FINE) {
	  //   Rcpp::Rcout << "Minimum is great maximum distance at element " << (i+1) << " " << (j+1) << endl << minDists << endl << maxDists << endl;
	  // }
	  return false;
	}
	if (minDists[i][j] < RESIDUE_DIST_MIN) {
	  DEBUG_MSG("Too small minimum  inter-residue distance encountered.");
	  return false;
	}
	if (maxDists[i][j] < RESIDUE_DIST_MIN) {
	  DEBUG_MSG("Too small maximum inter-residue distance encountered.");
	  return false;
	}
      }
    }  
    // DEBUG_MSG("Finished validateBothContraintMatrices");
    return true;
  }

  virtual bool validateProbabilityMatrix() const {
    for (size_t i = 0; i < probabilities.size(); ++i) {
      for (size_t j = i; j < probabilities.size(); ++j) {
	if (probabilities[i][j] > 0.0) {
	  if ((strandIds[i] == strandIds[j]) && ((j - i) <= LOOP_LEN_MIN)) {
      	    Rcpp::Rcerr << "Warning: probability matrix does not validate: loop shorter than 3nt: " 
		 << (i+1) << " " << (j+1) << " : " << concatSequence[i] << " " << concatSequence[j] << " " << probabilities[i][j] << endl;
	    return false; // minimum loop length is 3
	  }
	  if (!ResidueModel::isWatsonCrick(concatSequence[i], concatSequence[j], true)) {
	    Rcpp::Rcerr << "Warning: probability matrix does not validate: non-Watson-Crick pair found at position "
		 << (i+1) << " " << (j+1) << " : " << concatSequence[i] << " " << concatSequence[j] << " " << probabilities[i][j] << endl;
	    return false;
	  }
	}
      }
    }
    return true;
  }

  virtual bool validate() const {
    Vec<double> cv = getConcentrationsInMolPerLiter();
    if (elementSum(cv)/getStrandCount() > 1.0) { // sanity check: concentration must be less than one mol per liter
      DEBUG_MSG("Found unusually large strand concentration.");
      return false;
    }
    return validateHelices() && validateDistanceConstraintsMatrix(maxDists) && validateDistanceConstraintsMatrix(minDists) && validateBothConstraintMatrices() && validateStrandInteractionEnergies() && validateProbabilityMatrix();
  }

  virtual ostream& writeSequences(ostream& os) const {
    for (size_t i = 0; i < sequences.size(); ++i) {
      os << sequences[i] << endl;
    }
    return os;
  }

  /** Writes secondary structure in CT file format. */
  virtual void writeCT(ostream& os) const {
    // Rcpp::Rcout << "# Writing CT format structure of total length " << concatSequence.size() << " residues." << endl;
    // os << "# Energy: " << getEnergy() << " Entropy: " << getEntropy() << endl;
    writeCt(os, helices, concatSequence, starts);
    // os << endl;
  }

  /** Writes secondary structure in CT file format. */
  virtual void writeCT(ostream& os, const Vec<string>& keys, const Vec<string>& values) const {
    // Rcpp::Rcout << "# Writing CT format structure of total length " << concatSequence.size() << " residues." << endl;
    // os << "# Energy: " << getEnergy() << " Entropy: " << getEntropy() << endl;
    writeCt(os, helices, concatSequence, starts, keys, values);
    // os << endl;
  }

  virtual bool adjustDist(index_t start, index_t stop, real_t slop) {
    bool b1 = adjustMaxDist(start, stop, slop);
    bool b2 = adjustMinDist(start, stop, slop); // ensure both methods are called
    return (b1 || b2); 
  }

  /** Returns zero-based index of paired residue of a specified (zero-based) position. If unpaired, return NO_PAIRED_INDEX */
  virtual index_t getBasePair(int pos) const {
    PRECOND(pos >= 0);
    PRECOND(pos < static_cast<int>(size()));
    if (!isBasePaired(pos)) {
      return NO_PAIRING_INDEX;
    }
THROWIFNOT(pos < static_cast<int>(pairv.size()),"Assertion violation in L841");
    return pairv[pos];
  }

  virtual bool getTrackEventMode() const { return trackEventMode; }

  /** Returns true, if two residues are base paired. */
  virtual bool isBasePaired(index_t start, index_t stop) const {
    PRECOND(start >= 0);
    PRECOND(stop >= 0);
    PRECOND(start < static_cast<index_t>(paCount));
    PRECOND(stop < static_cast<index_t>(paCount));
    // return (pairings[start][stop] > 0);
    return pairv[start] == stop;
  }

  virtual bool isBasePaired(index_t pos) const {
    PRECOND(pos >= 0);
    PRECOND(pos < static_cast<index_t>(pairv.size()));
    return pairv[pos] != NO_PAIRING_INDEX;;
  }

  /** Sanity check for new base pair. Compare to method with same name for class ResiduePairing */
  virtual bool isNewBasePairOK(int start, int stop) const {
    if (stop < start) {
      return isNewBasePairOK(stop,start);
    }
    if (isBasePaired(start) || isBasePaired(stop)) {
      return false;
    }
    if (start == stop) {
      return false;
    }
    size_t s1 = strandIds[start];
    size_t s2 = strandIds[stop];
    if ((s1 == s2) && ((stop - start) <= LOOP_LEN_MIN)) {
      return false; // too short loop in hairpin
    }
    // no cheching for pseudoknots, unlike class ResiduePairing
    return true;
  }


  virtual bool hasSameConnectivity(int start, int stop) const {
    DEBUG_MSG("Placeholder implementation in isNewBasePairOk");
    return true;
  }

  /** Returns true, if helix can be placed because it is not base paired */
  virtual bool isPlaceable(const Stem& stem) const {
    for (int i = 0; i < stem.getLength(); ++i) {
      if (isBasePaired(stem.getStart() + i) || isBasePaired(stem.getStop()-i)) {
	return false;
      }
    }
    return true;
  }




  /** Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. Throws DISTANCE_CONSTRAINT_EXCEPTION if not placeable. */
  virtual bool setStem(const Stem& stem) throw (int) {
    DEBUG_MSG("Starting ResidueStructure.setStem");
    index_t start = stem.getStart();
    index_t stop  = stem.getStop();
    index_t len = stem.getLength();
    for (int i = 0; i < stem.getLength(); ++i) {
      if (probabilities[start + i][stop-i] <= 0.0) {
	DEBUG_MSG("# zero-probability encountered in setStem");
#ifndef NDEBUG
	if (verbose > VERBOSE_DETAILED) {
	  Rcpp::Rcout << "# Current base pairing probabilities:" << endl;
	  Rcpp::Rcout << probabilities << endl;
	}
#endif
	throw(DISTANCE_CONSTRAINT_EXCEPTION);
      }
    }
    bool changed = false;
    // determine which base pair was most like the first one to bind:
    // Rcpp::Rcout << "Stem at 836: " << stem << endl;
    if (probabilities[start][stop] >= probabilities[start+len-1][stop-(len-1)]) {
      real_t pAlt = 0.0;
      if (initiationAdjustMode == UNPAIRED_INITIATION_ADJUST_MODE) { //  STEM_INITIATION_ADJUST_MODE) {
	for (int i = 1; i < stem.getLength(); ++i) { // initiation probability is slightly higher through alternative pathways
	  pAlt += probabilities[start + i][stop-i];
	}
      }
      // Rcpp::Rcout << "Stem at 844: " << stem << endl;
      bool bpResult = setBasePair(start, stop, pAlt);
      DEBUG_MSG("# Mark ResidueStructureh-844");
      changed = changed || bpResult;
      DEBUG_MSG("# Mark ResidueStructureh-846");
      // Rcpp::Rcout << "Stem at 849: " << stem << endl;
      // Rcpp::Rcout << "Starting loop from 1 to " << stem.getLength() << " for stem " << stem << endl;
      for (int i = 1; i < stem.getLength(); ++i) {
	// Rcpp::Rcout << "i: " << i << endl;
THROWIFNOT(stem.getStart()+i < stem.getStop()-i,"Assertion violation in L939");
	DEBUG_MSG("# Mark ResidueStructureh-846");
	setBasePair(start +i, stop-i); // not necessary to keep track of "changed"
	DEBUG_MSG("# Mark ResidueStructureh-850");
      }
      DEBUG_MSG("# Mark ResidueStructureh-853");
    } else { // other direction
      DEBUG_MSG("# Mark ResidueStructureh-855");
      real_t pAlt = 0.0;
      for (int i = 0; (i+1) < stem.getLength(); ++i) {
THROWIFNOT(start+i < static_cast<int>(probabilities.size()),"Assertion violation in ResidueStructure.h :  L949");
THROWIFNOT(stop >= i,"Assertion violation in ResidueStructure.h :  L950");
THROWIFNOT(stop - i < static_cast<int>(probabilities[start + i].size()),"Assertion violation in ResidueStructure.h :  L951");
	pAlt += probabilities[start + i][stop-i];
      }
      bool bpResult = setBasePair(start + len -1, stop - (len-1), pAlt);
      DEBUG_MSG("# Mark ResidueStructureh-857");
      changed = changed || bpResult;
      for (int i = stem.getLength()-2; i >= 0; --i) { 
THROWIFNOT(start+i < stop-i,"Assertion violation in L958");
	bool bpResult = setBasePair(stem.getStart() +i, stem.getStop()-i);
	DEBUG_MSG("# Mark ResidueStructureh-862");
	changed = changed || bpResult;
      }
    }
    DEBUG_MSG("Finished ResidueStructure.setStem");
    return changed;
  }

  ResiduePairing convertFromResidueStructure() {
    return convertFromResidueStructure(&strands, residueModel);
  }

  ResiduePairing convertFromResidueStructure(StrandContainer * _strands, ResidueModel * _model) {
    DEBUG_MSG("# Starting ResidueStructure.convertFromResidueStructure...");
    // Rcpp::Rcout << "# strands: " << (*_strands) << endl;
    // Rcpp::Rcout << "# number of residues: " << _strands->getTotalLength() << endl;
    ResiduePairing pairing(_strands, _model);
    // Rcpp::Rcout << "# Trying to set property..." << endl;
    pairing.setProperty("negenergy", - getEnergy());
    // Rcpp::Rcout << "# Finished setting property!" << endl;
    for (size_t i = 0; i < size(); ++i) {
      // Rcpp::Rcout << "# working on position " << (i+1) << endl;
      if (isBasePaired(i)) {
	pairing.setBasePair(i, getBasePair(i));
      }
    }
    DEBUG_MSG("# Finished ResidueStructure.convertFromResidueStructure!");
    return pairing;
  }

  /** Build structure from light-weight ResiduePairing object. Can throw DISTANCE_CONSTRAINT_EXCEPTION */
  bool placeHelicesByProbability(const Vec<Stem>& stems, double dGCutoff) {
    return placeHelicesByProbability(stems, dGCutoff,1, false);
  }


  /** Places a single helices in order of probability */
  bool placeSingleHelixByProbability(const Vec<Stem>& stems, double dGCutoff, int crossingMax) {
    return placeHelicesByProbability(stems, dGCutoff, crossingMax, true);
  }

  /** Places helices in order of probability, until a helix is encountered, whose placement leads to an energy difference worse than dGCutoff */
  bool placeHelicesByProbability(const Vec<Stem>& stems, double dGCutoff, int crossingMax, bool singleHelixMode) {
    DEBUG_MSG("# Starting placeHelicesByProbability...");
    PRECOND(strands.validate());
    bool doBackup = false;
    size_t bpCount = getBasePairCount();
    ResiduePairing pairing = convertFromResidueStructure(&strands, residueModel);
    set<Stem> stemSet;
    set<Stem> placedStems;
    DEBUG_MSG("Saving current structure...");
    ResidueStructure backup;
    if (doBackup) {
      backup.copy(*this); // safety copy for reversing folding
    }
    DEBUG_MSG("done.");
    /* Rcpp::Rcout << "# Starting placeHlicesByProbability!" << endl; */
    /* Rcpp::Rcout << "# Already placed helices:" << endl; */
    /* Rcpp::Rcout << pairing.getHelices(); */
    /* Rcpp::Rcout << "# Short helices to be placed: " << stems; */
    for (size_t i = 0; i < stems.size(); ++i) {
      // result.setStem(stems[i]);
      bool ok = true;
      int crossCount = 0;
      if (crossingMax >= 0) {
	size_t s1 = strandIds[stems[i].getStart()];
	size_t s2 = strandIds[stems[i].getStop()];
	for (size_t j = 0; j < helices.size(); ++j) {
	  size_t s1b = strandIds[helices[j].getStart()];
	  size_t s2b = strandIds[helices[j].getStop()];
	  if ((s1 == s1b) && (s2 == s2b) && helices[j].isCrossing(stems[i])) {
	    ++crossCount;
	    if (crossCount > crossingMax) {
	      ok = false;
	      break;
	    }
	  }
	}
      }
      if (ok || (crossingMax < 0)) {
	stemSet.insert(stems[i]); // only save stems that are not crossing too often with existing structure
      }
    }
    DEBUG_MSG("# Mark ResidueStructureh 873");
    double T = residueModel->getTemperatureK();
    double g = computeAbsoluteFreeEnergy();
    size_t rounds = 0;
    // double RT = T * GAS_CONSTANT;
    while (stemSet.size() > 0) {
      ++rounds;
      // Rcpp::Rcout << "# Round " << rounds << " of placing highest probability helix" << endl;
      // find stem with highest initiation probability:
      set<Stem> incompatibleSet;
      auto bestIt = stemSet.end();
      double dGLowest = 999;
      bool first = true;
      for (auto it = stemSet.begin(); it != stemSet.end(); it++) {
	// Rcpp::Rcout << "# Checking stem " << (*it) << endl;
	// Rcpp::Rcout << "# Incompatible set size: " << incompatibleSet.size() << endl;
	const Stem& stem = *it;
	bool ok = true;
	if ((!isPlaceable(*it)) || (!pairing.isNewBasePairOK(it->getStart(), it->getStop()))) {
	  incompatibleSet.insert(stem);
	  ok = false;
	}
	// check if not crossing too often with already place helices
	if (ok && (crossingMax >= 0)) {
	  int crossCount = 0;
	  size_t s1 = strandIds[stem.getStart()];
	  size_t s2 = strandIds[stem.getStop()];
	  for (auto it2 = placedStems.begin(); it2 != placedStems.end(); it2++) {
	    const Stem& stem2 = *it2;
	    size_t s1b = strandIds[stem2.getStart()];
	    size_t s2b = strandIds[stem2.getStop()];
	    if ((s1 == s1b) && (s2 == s2b) && stem.isCrossing(stem2)) {
	      ++crossCount;
	      if (crossCount > crossingMax) {
		incompatibleSet.insert(stem);
		ok = false;
		break;
	      }
	    }
	  }
	}
	if (ok) {
	  double p = getStemInitiationProbability(*it);
	  double dS = GAS_CONSTANT * log(p); // negative quantity; each folding event leads to a decrease in entropy
	  double dG = it->getEnergy() - T * dS;
	  // Rcpp::Rcout << "# estimated free energy of folding for stem " << (*it) << " : " << dG << endl;
	  if ((dG < dGLowest) || first) {
	    dGLowest = dG;
	    bestIt = it;
	    first = false;
	  }
	}
      }
      if (dGLowest >= dGCutoff) {
	// Rcpp::Rcout << "# Lowest free energy of helices to place is greater or equal zero: " << dGLowest << endl;
	break; // no more folding!
      }
      if (bestIt != stemSet.end()) {
	try {
#ifndef NDEBUG
	  string hash1 = getStrandConnectivityHash();
#endif
	  setStem(*bestIt); // set highest-probability stem
#ifndef NDEBUG
	  string hash2 = getStrandConnectivityHash();
	  if (hash1 != hash2) {
	    Rcpp::Rcout << "# DEBUG NOTICE (TAKE OUT LATER): the connectivity of the molecular has changed due to placing stem " << (*bestIt) << endl;
	  }
#endif
	  pairing.setStem(*bestIt);
#ifndef NDEBUG
	  Rcpp::Rcout << "# Placing short helix " << (*bestIt) << endl;
#endif
	  if (singleHelixMode) {
	    return getBasePairCount() > bpCount;
	  }
	} catch (int e) {
	  
	}
	placedStems.insert(*bestIt);
	stemSet.erase(bestIt);
	// remove unplaceable stems from consideration
	for (auto it = incompatibleSet.begin(); it != incompatibleSet.end(); it++) {
	  stemSet.erase(*it);
	}
      } else { // no viable stem found - no reason to continue
	break;
      }
      double gNew = computeAbsoluteFreeEnergy();
      if (gNew >= g) {
	if (gNew > g) {
	  if (doBackup) {
	    Rcpp::Rcout << "Reverting to saved structure ..." << endl;
	    copy(backup);
	  }
#ifndef NDEBUG
	  Rcpp::Rcout << "Free energy increased from " << g << " to " << gNew << " kcal/mol for " << (*bestIt) << " reversing last step to " << computeAbsoluteFreeEnergy() << endl;
#endif
	}
	// DDERROR("New short helix did not improve free energy");
	break;
      } else {
	if (doBackup) {
	  try {
	    Rcpp::Rcout << "Applying stem to backup structure: ";
THROWIFNOT(bestIt != stemSet.end(),"Assertion violation in ResidueStructure.h :  L1148");
	    Rcpp::Rcout << (*bestIt) << endl;
	    backup.setStem(*bestIt);
	  } catch (int e) {
	    backup.writeCT(Rcpp::Rcout);
	  DERROR("Internal error: backup structure should not have collisions.");
	  }
	}
	g = gNew;
      }
    }
    // DEBUG_MSG("# Finished placeHelicesByProbability!");
    return getBasePairCount() > bpCount; // return true, iff at least one base pair has been set
  }

  /** Build structure from light-weight ResiduePairing object. Ensures that all helices have at least length helixLengthMin2 */
  bool placeHelicesByProbabilitySafe(const Vec<Stem>& stems, double dGCutoff, index_t helixLengthMin2, bool doBackup) {
    DEBUG_MSG("# Starting placeHelicesByProbabilitySafe...");
    PRECOND(strands.validate());
THROWIFNOT(StemTools::findMinimumHelixLength(stems) >= helixLengthMin2,"Assertion violation in ResidueStructure.h :  L1167");
    size_t bpCount = getBasePairCount();
    ResiduePairing pairing = convertFromResidueStructure(&strands, residueModel);
    set<Stem> stemSet;
    DEBUG_MSG("Saving current structure...");
    ResidueStructure backup;
    if (doBackup) {
      backup.copy(*this); // safety copy for reversing folding
    }
    DEBUG_MSG("done.");
    /* Rcpp::Rcout << "# Starting placeHlicesByProbability!" << endl; */
    /* Rcpp::Rcout << "# Already placed helices:" << endl; */
    /* Rcpp::Rcout << pairing.getHelices(); */
    /* Rcpp::Rcout << "# Short helices to be placed: " << stems; */
    for (size_t i = 0; i < stems.size(); ++i) {
      // result.setStem(stems[i]);
THROWIFNOT(stems[i].getLength() >= helixLengthMin2,"Assertion violation in ResidueStructure.h :  L1183");
      stemSet.insert(stems[i]);
    }
    DEBUG_MSG("# Mark ResidueStructureh 873");
    double T = residueModel->getTemperatureK();
    double g = computeAbsoluteFreeEnergy();
    size_t rounds = 0;
    // double RT = T * GAS_CONSTANT;
    while (stemSet.size() > 0) {
      ++rounds;
      // Rcpp::Rcout << "# Round " << rounds << " of placing highest probability helix" << endl;
      // find stem with highest initiation probability:
      set<Stem> incompatibleSet;
      auto bestIt = stemSet.end();
      double dGLowest = 999;
      bool first = true;
      for (auto it = stemSet.begin(); it != stemSet.end(); it++) {
	// Rcpp::Rcout << "# Checking stem " << (*it) << endl;
	// Rcpp::Rcout << "# Incompatible set size: " << incompatibleSet.size() << endl;
	if ((!isPlaceable(*it)) || (!pairing.isNewBasePairOK(it->getStart(), it->getStop()))) {
	  incompatibleSet.insert(*it);
	  // Rcpp::Rcout << "# Stem was not placeable!" << endl;
	} else {
	  double p = getStemInitiationProbability(*it);
	  double dS = GAS_CONSTANT * log(p); // negative quantity; each folding event leads to a decrease in entropy
	  double dG = it->getEnergy() - T * dS;
	  // Rcpp::Rcout << "# estimated free energy of folding for stem " << (*it) << " : " << dG << endl;
	  if ((dG < dGLowest) || first) {
	    dGLowest = dG;
	    bestIt = it;
	    first = false;
	  }
	}
      }
      if (dGLowest >= dGCutoff) {
	// Rcpp::Rcout << "# Lowest free energy of helices to place is greater or equal zero: " << dGLowest << endl;
	break; // no more folding!
      }
      if (bestIt != stemSet.end()) {
	try {
#ifndef NDEBUG
	  string hash1 = getStrandConnectivityHash();
#endif
	  setStem(*bestIt); // set highest-probability stem
#ifndef NDEBUG
	  string hash2 = getStrandConnectivityHash();
	  if (hash1 != hash2) {
	    Rcpp::Rcout << "# DEBUG NOTICE (TAKE OUT LATER): the connectivity of the molecular has changed due to placing stem " << (*bestIt) << endl;
	  }
#endif
	  pairing.setStem(*bestIt);
#ifndef NDEBUG
	  Rcpp::Rcout << "# Placing short helix " << (*bestIt) << endl;
#endif
	} catch (int e) {
	  
	}
	stemSet.erase(bestIt);
	// remove unplaceable stems from consideration
	for (auto it = incompatibleSet.begin(); it != incompatibleSet.end(); it++) {
	  stemSet.erase(*it);
	}
      } else { // no viable stem found - no reason to continue
	break;
      }
      double gNew = computeAbsoluteFreeEnergy();
      if (gNew >= g) {
	if (gNew > g) {
	  if (doBackup) {
	    Rcpp::Rcout << "Reverting to saved structure ..." << endl;
	    copy(backup);
	  }
#ifndef NDEBUG
	  Rcpp::Rcout << "Free energy increased from " << g << " to " << gNew << " kcal/mol for " << (*bestIt) << " reversing last step to " << computeAbsoluteFreeEnergy() << endl;
#endif
	}
	// DERROR("New short helix did not improve free energy");
	break;
      } else {
	if (doBackup) {
	  try {
	    Rcpp::Rcout << "Applying stem to backup structure: ";
THROWIFNOT(bestIt != stemSet.end(),"Assertion violation in ResidueStructure.h :  L1265");
	    Rcpp::Rcout << (*bestIt) << endl;
	    backup.setStem(*bestIt);
	  } catch (int e) {
	    backup.writeCT(Rcpp::Rcout);
	  DERROR("Internal error: backup structure should not have collisions.");
	  }
	}
	g = gNew;
      }
    }
    DEBUG_MSG("# Finished placeHelicesByProbability!");
THROWIFNOT(StemTools::findMinimumHelixLength(getHelices()) >= helixLengthMin2,"Assertion violation in ResidueStructure.h :  L1277");
    return getBasePairCount() > bpCount; // return true, iff at least one base pair has been set
  }


  /** Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. Throws DISTANCE_CONSTRAINT_EXCEPTION if not placeable. */
  virtual real_t getStemInitiationProbability(const Stem& stem) throw (int) {
    // DEBUG_MSG("Starting getStemInitiationProbability");
    real_t p = 0.0;
    index_t start = stem.getStart();
    index_t stop = stem.getStop();
    index_t len = stem.getLength();
    for (index_t i = 0; i < len; ++i) {
THROWIFNOT(strandIds[start] == strandIds[start+i],"Assertion violation in L1290");
THROWIFNOT(strandIds[stop] == strandIds[stop-i],"Assertion violation in L1291");
      p += probabilities[start+i][stop-i];
    }
    // DEBUG_MSG("Finished getStemInitiationProbability");
    return p;
  }

  /** Fold stems in greedy fashion. Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. Throws DISTANCE_CONSTRAINT_EXCEPTION if not placeable. */
  virtual bool setStems(const Vec<Stem>& stems) throw (int) {
    // DEBUG_MSG("Starting ResidueStructure.setStems");
    /* if (stems.size() == 0) { */
    /*   return false; */
    /* } */
    set<size_t> stemIds;
    for (size_t i = 0; i < stems.size(); ++i) {
      stemIds.insert(i);
    }
    bool changed = false;
    while (stemIds.size() > 0) {
      // find helix most likely to fold next:
      real_t bestProb = -1.0;
      size_t bestId = 0;
      for (auto it = stemIds.begin(); it != stemIds.end(); it++) {
	size_t id = *it;
THROWIFNOT(id < stems.size(),"Assertion violation in L1315");
	real_t p = getStemInitiationProbability(stems[id]);
	if (p > bestProb) {
	  bestProb = p;
	  bestId = id;
	}
      }
      // fold this stem:
      if (bestProb <= 0.0) {
	return changed; 
	//	Rcpp::Rcout << "# ERROR: could not place next best stem. Dumping stems:" << endl;
	//	for (auto it = stemIds.begin(); it != stemIds.end(); it++) {
	//	  size_t id = *it;
	// THROWIFNOT(id < stems.size(),"Assertion violation in L1327");
	//  real_t p = getStemInitiationProbability(stems[id]);
	//  Rcpp::Rcout << "Probability: " << p << " stem: " << stems[id] << endl;
	// }
      }
      ERROR_IF(bestProb <= 0.0, "Folding probability of stem should be greater zero.");
      bool setResult = setStem(stems[bestId]);
      changed = changed || setResult;
      stemIds.erase(bestId);
    }
    // DEBUG_MSG("Finished ResidueStructure.setStems");
    return changed;
  }

  /** For proper treatment of entropy, it is better to use setStems or setBasePairs. In some cases (like generating a movie of folding events)
   * one needs to force individual base pair folding, while proper treatment of entropy is not required).
   */
  virtual bool setBasePair_unsafe(index_t start, index_t stop, real_t pAlt=0.0) throw(int) {
    return setBasePair(start, stop, pAlt);
  }

  /** Fold base pairs in gready fashion. Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. Throws DISTANCE_CONSTRAINT_EXCEPTION if not placeable. */
  virtual bool setBasePairs(const Vec<basepair_t>& bpv) throw (int) {
    DEBUG_MSG("Starting ResidueStructure.setBasePairs");
    if (bpv.size() == 0) {
      return false;
    }
    StrandContainer strands(sequences, concentrations);
    ResiduePairing pairing(&strands, residueModel);
    set<size_t> bpIds;
    for (size_t i = 0; i < bpv.size(); ++i) {
//       if (verbose >= VERBOSE_FINEST) {
// 	Rcpp::Rcout << "Setting base pair " << bpv[i].first << " " << bpv[i].second << endl;
//       }
      pairing.setBasePair(bpv[i].first, bpv[i].second);
    }
    return setStems(pairing.getHelices());
    /* bool changed = false; */
    /* while (bpIds.size() > 0) { */
    /*   // find helix most likely to fold next: */
    /*   real_t bestProb = -1.0; */
    /*   size_t bestId = 0; */
    /*   for (auto it = bpIds.begin(); it != bpIds.end(); it++) { */
    /* 	size_t id = *it; */
    /* 	basepair_t bp = bpv[id]; */
    /* 	real_t p = probabilities[bp.first][bp.second]; */
    /* 	if (p > bestProb) { */
    /* 	  bestProb = p; */
    /* 	  bestId = id; */
    /* 	} */
    /*   } */
    /*   // fold this stem: */
    /*   ERROR_IF(bestProb <= 0.0, "Folding probability of stem should be greater zero."); */
    /*   bool bpResult = setBasePair(bpv[bestId].first, bpv[bestId].second); */
    /*   changed = changed || bpResult; */
    /*   bpIds.erase(bestId); */
    /* } */
    /* DEBUG_MSG("Finished ResidueStructure.setStems"); */
    /* return changed; */
  }

  /** Sets base pair and updates distances */
  virtual void setBasePairMatrix(const Vec<Vec<index_t> >& matrix) throw(int) {
    DEBUG_MSG("Deprecated - fix implementation of setBasePairMatrix");
THROWIFNOT(false,"Assertion violation in L1391");
THROWIFNOT(matrix.size() == paCount,"Assertion violation in L1392");
    size_t bpCount = 0;
    for (size_t i = 0; i < matrix.size(); ++i) {
      for (size_t j = i+1; j < matrix.size(); ++j) {
THROWIFNOT(matrix[i][j] == matrix[j][i],"Assertion violation in L1396");
	if (matrix[i][j] > 0) {
	  ++bpCount;
	  setBasePair(i,j);
	  if (verbose >= VERBOSE_DETAILED) {
	    Rcpp::Rcout << "Added base pair from matrix " << (i+1) << " " << (j+1) << " current energy: " << getEnergy() << endl;
	  }
	}
      }
    }
    // ASSERT(pairings == matrix); 
THROWIFNOT(getBasePairCount() == bpCount,"Assertion violation in L1407");
    if (verbose >= VERBOSE_DETAILED) {
      Rcpp::Rcout << "# Finished setBasePairMatrix." << endl;
    }
  }

  /** Sets base pair and updates distances */
  virtual void setBasePairMatrixAttempt(const Vec<Vec<index_t> >& matrix) {
THROWIFNOT(matrix.size() == paCount,"Assertion violation in L1415");
    size_t bpCount = 0;
    for (size_t i = 0; i < matrix.size(); ++i) {
      for (size_t j = i+1; j < matrix.size(); ++j) {
THROWIFNOT(matrix[i][j] == matrix[j][i],"Assertion violation in L1419");
	if (matrix[i][j] > 0) {
	  try {
	    setBasePair(i,j);
	    ++bpCount;
	  } catch (int e) {
	    // Rcpp::Rcerr << "# Warning: could not place base pair " << (i+1) << " " << (j+1) << endl;
            ERROR_IF(!validate(), "Structure does not validate.");
	  }
	  if (verbose >= VERBOSE_DETAILED) {
	    Rcpp::Rcout << "Added base pair from matrix " << (i+1) << " " << (j+1) << " current energy: " << getEnergy() << endl;
	  }
	}
      }
    }
    // ASSERT(pairings == matrix); 
    // ASSERT(getBasePairCount() == bpCount);
    if (verbose >= VERBOSE_DETAILED) {
      Rcpp::Rcout << "# Finished setBasePairMatrix." << endl;
    }
  }

  /** Sets base pair and updates distances */
  virtual void setBasePairVectorAttempt(const Vec<index_t>& matrix) {
THROWIFNOT(matrix.size() == paCount,"Assertion violation in L1443");
    size_t bpCount = 0;
    for (size_t ii = 0; ii < matrix.size(); ++ii) {
      index_t i = static_cast<index_t>(ii);
      index_t j = matrix[ii];
      if ((j > i) && (j < static_cast<index_t>(paCount))) {
	try {
	  setBasePair(i,j);
	  ++bpCount;
	} catch (int e) {
	  // Rcpp::Rcerr << "# Warning: could not place base pair " << (i+1) << " " << (j+1) << endl;
	  ERROR_IF(!validate(), "Structure does not validate.");
	}
	if (verbose >= VERBOSE_DETAILED) {
	  Rcpp::Rcout << "Added base pair from matrix " << (i+1) << " " << (j+1) << " current energy: " << getEnergy() << endl;
	}
      }
    }
    // ASSERT(pairings == matrix); 
    // ASSERT(getBasePairCount() == bpCount);
    if (verbose >= VERBOSE_DETAILED) {
      Rcpp::Rcout << "# Finished setBasePairMatrix." << endl;
    }
  }

  virtual void setEnergy(real_t x) { energy = x; }

  virtual void setTrackEventMode(bool mode) { trackEventMode = mode; }

  virtual void setVerbose(int v) { verbose = v; }

  virtual void writePairings(ostream& os) const {
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Starting writePairings: " << pairv.size() << endl;
      Rcpp::Rcout << "Connectivity: " <<  ConnectivityTools::connectivityToCombinedHash(getStrandConnectivityMatrix()) << endl;
    }
    for (size_t i = 0; i < lengthSum; ++i) {
      for (size_t i2 = 0; i2 < paTypes; ++i2) {
	size_t ic = (i*paTypes) + i2;
THROWIFNOT(ic < pairv.size(),"Assertion violation in L1482");
	bool found = false;
	for (size_t j = 0; j < lengthSum; ++j) {
	  for (size_t j2 = 0; j2 < paTypes; ++j2) {
	    size_t jc = (j*paTypes) + j2;
	    // Rcpp::Rcout << i << " " << i2 << " " << ic << " " << j << " " << j2 << " " << jc << endl;
	    if (isBasePaired(ic,jc)) {
	      os << (i+1) << " " << concatSequence[i] << " : " << (j+1) << " " << concatSequence[j] << " " << (strandIds[i]+1) << " " << (strandIds[j]+1) << endl;
	      found = true;
	    } 
	  }
	}
	if (!found) {
	  os << (i+1) << " " << concatSequence[i] << " : " << 0 << " " << '-' << " " << (strandIds[i]+1) << " " << 0 << endl;
	}
      }
    }
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Finished writePairings." << endl;
    }
  }

  virtual void removeDividers() {
    dividers.clear(); //  = DEFAULT_DIVIDER;
    // update distances
  }

  virtual void writePairings2(ostream& os) const {
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Starting writePairings: " << pairv.size() << endl;
    }
    for (size_t i = 0; i < lengthSum; ++i) {
      if ((i > 0) && (strandIds[i-1] != strandIds[i])) {
	os << " | "; // divider between strands
      }
      for (size_t i2 = 0; i2 < paTypes; ++i2) {
	size_t ic = (i*paTypes) + i2;
THROWIFNOT(ic < pairv.size(),"Assertion violation in L1519");
	os << (strandIds[i]+1) << concatSequence[i] << (i+1) ;
	for (size_t j = 0; j < lengthSum; ++j) {
	  for (size_t j2 = 0; j2 < paTypes; ++j2) {
	    size_t jc = (j*paTypes) + j2;
	    // Rcpp::Rcout << i << " " << i2 << " " << ic << " " << j << " " << j2 << " " << jc << endl;
	    if (isBasePaired(ic,jc) > 0) {
	      os << ":" << (strandIds[j]+1) << concatSequence[j] << (j+1);
	    }
	  }
	}
	os << " ";
      }
    }
    os << " | Energy: " << energy << endl;
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Finished writePairings2." << endl;
    }
  }

  /** Output of all data structures with exception of residue model */
  virtual void writeShort(ostream& os) {
    os << starts << endl 
       << minDists.size() << endl
       << maxDists.size() << endl
       << pairv.size() << endl
       << sequences.size() << endl
       << lengthSum << endl
       << paTypes << endl // lengthSum times number of pseudo-atom types
       << paCount; // lengthSum times number of pseudo-atom types
  }

  virtual void updateResidueProbabilities() {
    // if (verbose >=  VERBOSE_FINEST) {
    //   Rcpp::Rcout << "Starting updateResidueProbabilities" << endl;
    // }
    PRECOND(validate());
    for (size_t i = 0; i < paCount; ++i) {
      real_t sum = 0.0;
      for (size_t j = 0; j < paCount; ++j) {
	sum += probabilities[i][j];
      }
      residueProbabilities[i] = sum;
    }
    POSTCOND(validate());
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Finished updateResidueProbabilities" << endl;
    }
  }

  /** Output of all data structures with exception of residue model */
  virtual void writeAll(ostream& os) {
    os << "# Sequences:" << endl
       << sequences << endl
       << "# Energy: " << energy << endl
       << "# Start positions (zero-based)" << endl
       << starts << endl 
       << "# Dividers:" << externalCounting(dividers) << endl
       << "# Base pair connectivity vector: " << endl
       << pairv << endl
       << "# Minimum distances:" << endl
       << minDists << endl
       << "# Maximum distances:" << endl
       << maxDists << endl
       << "# Folding probabilities:" << endl
       << probabilities << endl
       << "# Occupancies:" << endl
       << occupancies << endl
//       << "# Helix ids:" << endl
//       << helixIds << endl
  //     << "# Helices: " << endl
  //     << helices << endl
       << "# Residue Probabilities:" << endl
       << residueProbabilities << endl
       << "# Total number of residues:" << endl
       << lengthSum << endl
       << "# Beads per residue:" << endl
       << paTypes << endl
       << "# Total number of beads:" << endl
       << paCount << endl // lengthSum times number of pseudo-atom types
       << "# Concentrations:" << endl
       << concentrations << endl
       << "# Effective volume:" << endl
       << effectiveVolume << endl
   //    << "# Strand Connectivity:" << endl
  //     << strandConnectivity << endl
       << "# Strand interaction energies:" << endl
       << strandInteractionEnergies << endl;
  }
  
 /*  ostream& operator << (ostream& os, const ResidueStructure& rval) { */
 /*    for (size_t = 0;; i < sequences.size(); ++i) { */
 /*      os << sequences[i]; */
 /*    } */
 /*  } */

 /* private: */

  /** Adjust maximum distances around position. Returns true, iff change was necessary. */
  bool adjustMaxDist(index_t start, index_t stop, real_t slop);

  /** Adjust minimum distances around position. Returns true, iff change was necessary. */
  bool adjustMinDist(index_t start, index_t stop, real_t slop);

  virtual void init(const Vec<string>& origSequences, const Vec<double>& _concentrations, ResidueModel * _residueModel) {
    PRECOND(_residueModel != NULL);
THROWIFNOT(_concentrations.size() == origSequences.size(),"Assertion violation in L1625");
    /* if (verbose >= VERBOSE_FINEST) { */
    /*   Rcpp::Rcout << "Starting ResidueStructure::init!" << endl; */
    /* } */
    dividers.clear(); // = DEFAULT_DIVIDER;
    initiationAdjustMode = NO_INITIATION_ADJUST_MODE; // STEM_INITIATION_ADJUST_MODE; // NO_INITIATION_ADJUST_MODE; // UNPAIRED_INITIATION_ADJUST_MODE;
    initiationAdjustMultiplier = 1.0;
    concentrations = _concentrations;
    residueModel = _residueModel;
    sequences = origSequences; // copy sequence info
    strandConnectivity = Vec<Vec<set<string> > >(sequences.size(), Vec<set<string> >(sequences.size()));
    lengthSum = 0;
    concatSequence = "";
    real_t concMin = concentrations[0];
    for (string::size_type i = 0; i < sequences.size(); ++i) {
      starts.push_back(lengthSum);
      lengthSum += sequences[i].size();
      concatSequence = concatSequence + sequences[i];
      if (concentrations[i] < concMin) {
	concMin = concentrations[i];
      }
    }
    strandIds = Vec<size_t>(lengthSum, 0);
    size_t pc = 0;
    for (string::size_type i = 0; i < sequences.size(); ++i) {
      for (size_t j = 0; j < sequences[i].size(); ++j) {
THROWIFNOT(pc <= strandIds.size(),"Assertion violation in L1651");
	strandIds[pc++] = i;
      }
    }
THROWIFNOT(strandIds.size() == pc,"Assertion violation in L1655");
    paTypes = residueModel->getAtomsPerResidue();
    paCount = lengthSum * residueModel->getAtomsPerResidue();
    probabilities = Vec<Vec<real_t> > (paCount, Vec<real_t>(paCount, 0.0));
    residueProbabilities = Vec<real_t> (paCount, 0.0);
    // pairings = Vec<Vec<index_t> > (paCount, Vec<index_t>(paCount, 0.0));
    pairv = Vec<index_t>(paCount, NO_PAIRING_INDEX);
    // ASSERT(pairv.size() == pairings.size());
    helixIds = Vec<Vec<index_t> > (paCount, Vec<index_t>(paCount, HELIX_ID_NONE));
    helices.clear();
    refAtom="C1'"; // careful: what about "*"?
THROWIFNOT(concMin > 0.0,"Assertion violation in L1666");
    effectiveVolume = STRANDS_PER_EFF_VOLUME / (MICROMOL_TO_N_PER_ANG3*concMin); // effective volume in angstroem cubed
    real_t maxDist = pow(effectiveVolume, 1/3.0); // third root of volumn is used as max distance 
    minDists = Vec<Vec<real_t> > (paCount, Vec<real_t>(paCount, RESIDUE_DIST_MIN)); // defined in ResidueModel.h
    maxDists = Vec<Vec<real_t> > (paCount, Vec<real_t>(paCount, maxDist));
    real_t p0Intra = getResidueModel()->getIntraCollisionProb0(); // (3.0/(4.0 * PI) ) * reactionVolume * intraCollisionEpsilon;
    real_t intraCollisionExponent = getResidueModel()->getIntraCollisionExponent();
    for (size_t i = 0; i < paCount; ++i) {
      minDists[i][i] = 0.0;
      maxDists[i][i] = 0.0;
      for (size_t j = i+1; j < paCount; ++j) {
	if (strandIds[i] == strandIds[j]) { // same strand
	  maxDists[i][j] = (j-i) * RESIDUE_ADJ_DIST_MAX;
	  maxDists[j][i] = maxDists[i][j];
	  if (((j - i) > LOOP_LEN_MIN) && ResidueModel::isWatsonCrick(concatSequence[i], concatSequence[j], true)) {
	    probabilities[i][j] = p0Intra * pow(maxDists[i][j], - intraCollisionExponent);
	  } else {
THROWIFNOT(probabilities[i][j] == 0.0,"Assertion violation in L1683");
	  }
	} else { // different strand
	  if (ResidueModel::isWatsonCrick(concatSequence[i], concatSequence[j], true)) {
	    probabilities[i][j] = p0Intra * pow(maxDists[i][j], -3.0); // free volume translation, no self-avoid walk correction
	  }
	}
	probabilities[j][i] = probabilities[i][j];
      }
    }
    energy = 0.0;
    entropy = 0.0;
    strandInteractionEnergies = Vec<Vec<real_t> >(sequences.size(), Vec<real_t>(sequences.size(), 0.0));
    simulationTime = 0.0;
    trackEventMode = false;
    updateDistanceMode = 1;
    verbose = VERBOSE_GLOBAL;
    //    if (verbose >= VERBOSE_FINEST) {
    // Rcpp::Rcout << "Finished ResidueStructure::init!" << endl;
    // }
  }

  virtual void copy(const ResidueStructure& other) {
    DEBUG_MSG("Starting ResidueStructure.copy");
    activeTimer = other.activeTimer;
    coaxialStackingMode = other.coaxialStackingMode;
    concatSequence = other.concatSequence;
    concentrations = other.concentrations;
    connectivityWaitingTimes = other.connectivityWaitingTimes; // for each strand connectivity, sum all average simulation times  
    dividers = other.dividers;
    effectiveVolume = other.effectiveVolume;
    energy = other.energy;
    entropy = other.entropy;
    foldingEvents = other.foldingEvents;
    helices = other.helices;
    helixIds = other.helixIds;
    initiationAdjustMode = other.initiationAdjustMode;
    initiationAdjustMultiplier = other.initiationAdjustMultiplier;
    interConnectivityHops = other.interConnectivityHops;
    interConnectivityWaitingTimes = other.interConnectivityWaitingTimes;
    lengthSum = other.lengthSum;
    maxDists = other.maxDists;
    minDists = other.minDists;
    occupancies = other.occupancies;
    paCount = other.paCount;
    pairv = other.pairv;
    paTypes = other.paTypes;
    probabilities = other.probabilities;
    refAtom = other.refAtom;
    residueModel = other.residueModel;
    residueProbabilities = other.residueProbabilities;
    sequences = other.sequences; // copy sequence info
    simulationTime = other.simulationTime;
    strandIds = other.strandIds;
    strands = other.strands;
    starts = other.starts;
    strandConnectivity = other.strandConnectivity;
    strandInteractionEnergies = other.strandInteractionEnergies;
    trackEventMode = other.trackEventMode;
    updateDistanceMode = other.updateDistanceMode;
    verbose = other.verbose;
    DEBUG_MSG("Finished ResidueStructure.copy");
  }

  real_t computeBasePairEnergyContribution(int start, int stop) const {
    PRECOND(residueModel != NULL);
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Starting computeBasePairEnergyContribution " << (start+1) << " " << (stop+1) << " " << getLengthSum() << endl;
    }
    char x = getResidue(start);
    char y = getResidue(stop);
    real_t result = 0.0;
    real_t tmp;
    int found = 0;
    int n = static_cast<int>(getLengthSum());
    real_t singleContrib = residueModel->getSingleBasePairEnergy(getResidue(start), getResidue(stop));
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "# Adding single base pairing energy contribution: " << singleContrib << endl;
    }
    result += singleContrib;
    if ((start > 0) && ((stop+1) < n)) {
      if ((isBasePaired(start-1, stop+1)) && (strandIds[start] == strandIds[start-1]) && (strandIds[stop] == strandIds[stop+1])) { // check if same strands
	char a = getResidue(start-1);
	char b = getResidue(stop+1);
	tmp = residueModel->getStackingEnergy(a,b,x,y);
	result += tmp;
	if (verbose >= VERBOSE_FINEST) {
	  Rcpp::Rcout << "Added first energy contribution of " << a << " " << b << " " << x << " " << y << " : " << tmp << endl;
	  if (tmp == 0.0) {
	    Rcpp::Rcout << "### WARNING : could not find energy parameter !!! ###" << endl;
	  }
	}
	++found;
      }
    }
    if ((stop > 0) && ((start+1) < n)) {
      if (isBasePaired(start+1, stop-1) && (strandIds[start] == strandIds[start+1]) && (strandIds[stop] == strandIds[stop-1])) { // check if same strands
	char a = getResidue(start+1);
	char b = getResidue(stop-1);
	tmp = residueModel->getStackingEnergy(x,y,a,b);
	result += tmp;
	++found;
	if (verbose >= VERBOSE_FINEST) {
	  Rcpp::Rcout << "Added second energy contribution of " << x << " " << y << " " << a << " " << b << " : " << tmp << endl;
	  if (tmp == 0.0) {
	    Rcpp::Rcout << "### WARNING : could not find energy parameter !!! ###" << endl;
	  }
	}
      }
    }

    if (found == 0) {
      /*      if (((x == 'A') && (y == 'U')) || ((x == 'U') && (y=='A')))  {#
	result = BP_ENERGY_AU_SINGLE;
      } else if (((x == 'G') && (y == 'C')) || ((x == 'C') && (y=='G')))  {
	  result = BP_ENERGY_GC_SINGLE;
	  } */
      result += HELIX_PENALTY; // new helix is started
    } else if (found == 2) {
      result -= HELIX_PENALTY; // new base pair effectively fuses two helices: avoid overcounting!
    }
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Finished computeBasePairEnergyContribution: " << result <<  endl;
    }
    return result;
  }

  /** Adjust maximum distances around position */
  bool setAndAdjustMaxDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop) throw(int);

  /** Adjust maximum distances around position */
  set<basepair_score_t> setAndAdjustDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop, bool minMode) throw(int);

  /** Adjust maximum distances around position */
  set<basepair_score_pair_t> setAndAdjustDistPair(index_t start, index_t stop, real_t valueMin, real_t valueMax, real_t slop, index_t fromStart, index_t fromStop) throw(int);

  /** Adjust maximum distances around position */
  set<basepair_score_t> setAndNotAdjustDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop, bool minMode) throw(int);

  static void merge(set<basepair_score_t>& undoSet, const set<basepair_score_t>& otherSet) {
    for (set<basepair_score_t>::const_iterator it = otherSet.begin(); it != otherSet.end(); it++) {
      undoSet.insert(*it);
    }
  }

  static void merge(set<basepair_score_pair_t>& undoSet, const set<basepair_score_pair_t>& otherSet) {
    for (auto it = otherSet.begin(); it != otherSet.end(); it++) {
      undoSet.insert(*it);
    }
  }

  /** Adds name-hashed probability matrices to result data struture */
  /* static void combineHashedMatrices(pairingprob_slices_map_t& result, const pairingprob_slices_map_t& addThis) { */
  /*   for (pairingprob_slices_map_t::const_iterator it = addThis.begin(); it != addThis.end(); it++) { */
  /*     string name = it->first; */
  /*     // check if exists: */
  /*     // Rcpp::Rcout << "# Working on combining name " << name << endl; */
  /*     pairingprob_slices_map_t::iterator resultIt = result.find(name); */
  /*     if (resultIt == result.end()) { // not found in result */
  /* 	// Rcpp::Rcout << "# Copying matrix" << endl; */
  /* 	result[name] = it->second; // copy */
  /*     } else { */
  /* 	pairingprob_slices_t resultMatrices = resultIt->second; */
  /* 	ASSERT(resultMatrices.size() > 0); */
  /* 	const pairingprob_slices_t& matrices = it->second; */
  /* 	ASSERT(matrices.size() == resultMatrices.size()); */
  /* 	for (size_t i = 0; i < matrices.size(); i++) { */
  /* 	  // Rcpp::Rcout << "# Adding matrix " << (i+1) << " " << endl; */
  /* 	  // writeMatrix(Rcpp::Rcout, resultMatrices[i]); */
  /* 	  // writeMatrix(Rcpp::Rcout, matrices[i]); */
  /* 	  ASSERT(matrices[i].size() == resultMatrices[i].size()); */
  /* 	  for (size_t j = 0; j < matrices[i].size(); j++) { */
  /* 	    ASSERT(matrices[i][j].size() == resultMatrices[i][j].size()); */
  /* 	    for (size_t k = 0; k < matrices[i][j].size(); k++) { */
  /* 	      resultMatrices[i][j][k] += matrices[i][j][k]; */
  /* 	    } */
  /* 	  } */
  /* 	} */
  /* 	result[name] = resultMatrices; // FIXIT avoid copying */
  /*     } */
  /*   } */
  /*   // Rcpp::Rcout << "# Finished combining matrices" << endl; */
  /* } */
  
  /** Generates a dummy RNA structure for testing purposes */
  static ResidueStructure generateDummy() {
    Vec<double> concentrations(2, 1.0);
    Vec<string> sequences(2);
    sequences[0] = "GACGGUAGC";
    sequences[1] = "GCUUCGUC";
    ResidueModel model; // default mode
    ResidueStructure rna(sequences, concentrations, &model);
    return rna;
  }

  void setUpdateDistanceMode(int _mode) { updateDistanceMode = _mode; }

 private:

  /** Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. If unfeasible sterically, throws exception.
   * pAlt; probability of additional alternative pathways (zero by default).
   */
  virtual bool setBasePair(index_t start, index_t stop, real_t pAlt=0.0) throw(int) {
    DEBUG_MSG("Starting ResidueStructure.setBasePair " + itos(start+1) + " " + itos(stop+1));
    PRECOND(!isBasePaired(start));
    PRECOND(!isBasePaired(stop));
    PRECOND(validateHelices());
    PRECOND(validate());
    // make safety copy 
    Vec<Stem> _helices = helices;
    Vec<Vec<int> > _helixIds = helixIds;
    // Vec<Vec<int> > _pairings = pairings;
    Vec<int> _pairv = pairv;
    Vec<Vec<real_t> > _minDists= minDists; // TODO inefficient
    Vec<Vec<real_t> > _maxDists= maxDists;
    Vec<Vec<real_t> > _probabilities = probabilities;
    bool result = false;
    // DEBUG_MSG("Mark ResidueStructure.h 746");
    try {
      // DEBUG_MSG("Mark ResidueStructure.h 748");
      // result = setBasePairRaw(start, stop);
      real_t p = probabilities[start][stop];
      size_t s1 = strandIds[start];
      size_t s2 = strandIds[stop];
      switch (initiationAdjustMode) {
      case NO_INITIATION_ADJUST_MODE:
	break; // do nothing
      case STEM_INITIATION_ADJUST_MODE:
	p += pAlt;
	break;
      case UNPAIRED_INITIATION_ADJUST_MODE: 
	if (initiationAdjustMultiplier != 0.0) {
	  for (int mul = -1; mul <= 1; mul+=2) { // search upstream and downstream
	    for (int k2 = 1; ; ++k2) {
	      int k = mul * k2;
	      int start2 = start + k;
	      int stop2 = stop - k;
	      if ((start2 < 0) || (start2 >= static_cast<int>(strandIds.size())) || (stop2 < 0) || (stop2 >= static_cast<int>(strandIds.size())) || (strandIds[start2] != s1) || (strandIds[stop2] != s2)) {
		break;
	      }
	      real_t p2 = initiationAdjustMultiplier * probabilities[start2][stop2];
	      if (p2 <= 0.0) {
		break;
	      }
	      p += p2;
	    }
	  }
	}
	break;
      default: DERROR("Unknown probability initation adjustment mode (option --init 1|2).");
      }
      ERROR_IF(p <= 0.0, "The probability of folding event is zero!.");
      ERROR_IF(p > 1.0, "Folding probability greater zero encountered.");
      result = setBasePairRaw2(start, stop);
      if (trackEventMode) {
	foldingEvents.push_back(folding_event_t(FOLDED_BASEPAIR, basepair_t(start, stop)));
      }
THROWIFNOT((foldingEvents.size() > 0) || (!trackEventMode),"Assertion violation in L1940");
      real_t dS = GAS_CONSTANT * log(p); // negative quantity; each folding event leads to a decrease in entropy
      ERROR_IF(dS >= 0.0, "Setting of base pair should lead to decrease in entropy");
      // real_t dS = 0.0; DEBUG_MSG("FOR DEBUGGING SETTTING ENTROPY CHANGE TO ZERO - MUST CHANGE THIS CODE FOR PRODUCTION!");
      entropy += dS;
    } catch (int e) {
      DEBUG_MSG("Mark ResidueStructure.h 755");
      // Rcpp::Rcout << "# Exception handling for base pair setting..." << endl;
      helices = _helices;
      helixIds = _helixIds;
      // pairings = _pairings;
      pairv    = _pairv;
      minDists = _minDists;
      maxDists = _maxDists;
      probabilities = _probabilities;
THROWIFNOT(validate(),"Assertion violation in L1955");
      // DEBUG_MSG("Mark ResidueStructure.h 765");
      // Rcpp::Rcout << "# Exception handling for base pair setting finished." << endl;
      throw (DISTANCE_CONSTRAINT_EXCEPTION);
      // DEBUG_MSG("Mark ResidueStructure.h 768");
    }
    POSTCOND(validateHelices());
    POSTCOND(validate());
    DEBUG_MSG("Finished setBasePair");
    return result;
  }

    /** Sets base pair and updates distances. Returns true if strand connectivity has changed, false otherwise. */
  virtual bool setBasePairRaw(index_t start, index_t stop) throw(int);

  virtual bool setBasePairRaw2(index_t start, index_t stop) throw(int);
 
  void updateHelix3PrimeAdjacencies() const {
    DEBUG_MSG("Starting updateHelix3PrimeAdjacencies");
    if (helix3PrimeAdj.size() != helices.size()) {
      if (helices.size() > 0) {
	helix3PrimeAdj = Vec<int>(helices.size(), -1);
      } else {
	helix3PrimeAdj.clear();
      }
      index_t n = static_cast<index_t>(size());
      Vec<index_t> residueHelices(n, -1);
      for (size_t i = 0; i < helices.size(); ++i) {
	for (index_t j = 0; j < helices[i].getLength(); ++j) {
THROWIFNOT(helices[i].getStart() +j < n,"Assertion violation in L1984");
THROWIFNOT(helices[i].getStop() >= j,"Assertion violation in L1985");
	  residueHelices[helices[i].getStart()+j] = i;
	  residueHelices[helices[i].getStop()-j] = i;
	}
      }
      for (size_t i = 0; i < helices.size(); ++i) {
	helix3PrimeAdj[i] = -1;
	index_t pos = helices[i].getStart() + helices[i].getLength()-1;
	size_t sid = strandIds[pos];
	for (index_t j = pos+1; j < n; ++j) {
	  if (strandIds[j] != sid) {
	    break;
	  }
	  index_t loopSize = j - pos - 1;
THROWIFNOT(loopSize >= 0,"Assertion violation in L1999");
	  if (loopSize > STACKING_LOOP_LEN_MAX) {
	    break;
	  }
	  index_t other = residueHelices[j];
	  if (other >= 0) { // helix found!
	    helix3PrimeAdj[i] = other;
THROWIFNOT(static_cast<index_t>(i) != other,"Assertion violation in L2006");
	  }
	}
      }
    }
    DEBUG_MSG("Finished updateHelix3PrimeAdjacencies");
  }

};

  /**
 comparison. return true, if == is true for all elements 
*/
inline
bool
operator == (const ResidueStructure& lval, const ResidueStructure& rval) {
  return (lval.getSequences() == rval.getSequences()) && (lval.getEnergy() == rval.getEnergy()) && (lval.getPairv() == rval.getPairv());
}


/** Adjust maximum distances around position */
bool
ResidueStructure::adjustMaxDist(index_t start, index_t stop, real_t slop) {
    // Rcpp::Rcout << "# starting adjustMaxDist " << (start+1) << " " << (stop+1) << " " << slop << endl;
    PRECOND(paCount == maxDists.size());
    PRECOND(validateDistanceConstraintsMatrix(maxDists));
    DERROR("Internal error: method ResidueStructure::adjustMaxDist is not supported anymore.");
    if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
      // Rcpp::Rcout << "Not adjusting!" << endl;
      return false; // no change
    }
    real_t md = maxDists[start][stop];
    real_t neighborMin = -1;
    bool adjusted = false;
    size_t strandId1 = strandIds[start];
    size_t strandId2 = strandIds[stop];
    if ((start > 0) && (strandId1 == strandIds[start-1])) {
      neighborMin = maxDists[start-1][stop];
    } else if (((start+1) < static_cast<index_t>(paCount)) && (strandId1 == strandIds[start+1])) {
      neighborMin = maxDists[start+1][stop];
    } else if ((stop > 0) && (strandId2 == strandIds[stop-1])) {
      neighborMin = maxDists[start][stop-1];
    } else if (((stop + 1) < static_cast<index_t>(paCount)) && (strandId2 == strandIds[stop+1])) {
      neighborMin = maxDists[start][stop+1];
    }
    if (neighborMin < 0) {
      if (verbose >= VERBOSE_FINE) {
	Rcpp::Rcout << "# Position not adjustable " << (start + 1) << " " << (stop + 1) << endl;
      }
      return false; // not adjustable
    }
    if ((start > 0) && (strandId1 == strandIds[start-1])) {
      neighborMin = minimum(maxDists[start-1][stop], neighborMin);
    }
    if (((start+1) < static_cast<index_t>(paCount)) && (strandId1 == strandIds[start+1])) {
      neighborMin = minimum(maxDists[start+1][stop], neighborMin);
    }
    if ((stop > 0) && (strandId2 == strandIds[stop-1])) {
      neighborMin = minimum(maxDists[start][stop-1], neighborMin);
    }
    if (((stop + 1) < static_cast<index_t>(paCount)) && (strandId2 == strandIds[stop+1])) {
      neighborMin = minimum(maxDists[start][stop+1], neighborMin);
    }
    if (md  > (neighborMin + slop)) {
      maxDists[start][stop] = neighborMin + slop;
      maxDists[stop][start] = maxDists[start][stop];
      adjusted = true;
    }
    // Rcpp::Rcout << "# Finished adjustMaxDist " << (start+1) << " " << (stop+1) << " " << slop << " " << adjusted << " " << maxDists[start][stop] << endl;
    return adjusted;
  }


/** Adjust maximum distances around position */
bool
ResidueStructure::adjustMinDist(index_t start, index_t stop, real_t slop) {
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# starting adjustMinDist " << (start+1) << " " << (stop+1) << " " << slop << endl;
  }
  DERROR("Internal error: method ResidueStructure::adjustMinDist is not supported anymore.");
    PRECOND(paCount == minDists.size());
    if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
      Rcpp::Rcout << "Not adjusting!" << endl;
      return false; // no change
    }
    real_t md = minDists[start][stop];
    real_t neighborMin = -1;
    bool adjusted = false;
    size_t strandId1 = strandIds[start];
    size_t strandId2 = strandIds[stop];
    if ((start > 0) && (strandId1 == strandIds[start-1])) {
      neighborMin = minDists[start-1][stop];
    } else if (((start+1) < static_cast<index_t>(paCount)) && (strandId1 == strandIds[start+1])) {
      neighborMin = minDists[start+1][stop];
    } else if ((stop > 0) && (strandId2 == strandIds[stop-1])) {
      neighborMin = minDists[start][stop-1];
    } else if (((stop + 1) < static_cast<index_t>(paCount)) && (strandId2 == strandIds[stop+1])) {
      neighborMin = minDists[start][stop+1];
    }
    if (neighborMin < 0) {
      if (verbose >= VERBOSE_FINE) {
	Rcpp::Rcout << "# Position not adjustable " << (start + 1) << " " << (stop + 1) << endl;
      }
      return false; // not adjustable
    }
    if ((start > 0) && (strandId1 == strandIds[start-1])) {
      neighborMin = maximum(minDists[start-1][stop], neighborMin);
    }
    if (((start+1) < static_cast<index_t>(paCount)) && (strandId1 == strandIds[start+1])) {
      neighborMin = maximum(minDists[start+1][stop], neighborMin);
    }
    if ((stop > 0) && (strandId2 == strandIds[stop-1])) {
      neighborMin = maximum(minDists[start][stop-1], neighborMin);
    }
    if (((stop + 1) < static_cast<index_t>(paCount)) && (strandId2 == strandIds[stop+1])) {
      neighborMin = maximum(minDists[start][stop+1], neighborMin);
    }
    if (md  < (neighborMin - slop)) {
      minDists[start][stop] = neighborMin - slop;
      minDists[stop][start] = minDists[start][stop];
      adjusted = true;
    }
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "# Finished adjustMinDist " << (start+1) << " " << (stop+1) << " " << slop << " Adjusted?: " << adjusted << " " << minDists[start][stop] << " from " << md << endl;
    }
THROWIFNOT((start == 0) || (fabs(minDists[start-1][stop] -minDists[start][stop]) <= RESIDUE_ADJ_DIST_MAX),"Assertion violation in L2131");
THROWIFNOT((start+1 >= static_cast<index_t>(minDists.size())) || (fabs(minDists[start+1][stop]-minDists[start][stop]) <= RESIDUE_ADJ_DIST_MAX),"Assertion violation in L2132");
THROWIFNOT((stop == 0) || (fabs(minDists[start][stop-1] -minDists[start][stop]) <= RESIDUE_ADJ_DIST_MAX),"Assertion violation in L2133");
THROWIFNOT((stop+1 >= static_cast<index_t>(minDists.size())) || (fabs(minDists[start][stop+1]-minDists[start][stop]) <= RESIDUE_ADJ_DIST_MAX),"Assertion violation in L2134");
    return adjusted;
  }


/** Adjust maximum or minimum distances around position. Returns undo information */
set<basepair_score_t>
ResidueStructure::setAndAdjustDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop, bool minMode) throw (int) {
  DEBUG_MSG("Starting ResidueStructure.setAndAdjustDist " + itos(start+1) + " " + itos(stop+1) + " min mode: " + itos((int)minMode) + " slop: " + dtos(slop));
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# starting setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << endl;
    Rcpp::Rcout << "# Initial minimum distances: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
  // Rcpp::Rcout << "# Initial validation before setAndAdjustDist starts..." << endl;
  // ASSERT(validate());
  // Rcpp::Rcout << "# Finished initial validation before setAndAdjustDist starts." << endl;

  PRECOND(paCount == minDists.size());
  set<basepair_score_t> undoSet;
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 133");
  // real_t reactionRadius = getResidueModel()->getReactionRadius();
  real_t intraCollisionExponent = getResidueModel()->getIntraCollisionExponent();
  // real_t reactionVolume = reactionRadius * reactionRadius * reactionRadius;
  real_t p0Intra = getResidueModel()->getIntraCollisionProb0(); // (3.0/(4.0 * PI) ) * reactionVolume * intraCollisionEpsilon;

  if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Not adjusting!" << endl;
    }
    return undoSet; // no change
  }    
THROWIFNOT(start < stop,"Assertion violation in L2166");
  if (!updateDistanceMode) {
    return setAndNotAdjustDist(start, stop, value, stop, fromStart, fromStop, minMode);
  }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 149");
  bool adjusted = false;
  size_t strandId1 = strandIds[start];
  size_t strandId2 = strandIds[stop];
  if (fromStart < 0 || fromStop < 0) {
    if (minMode) {
#ifdef STRUCTURE_UNDO_MODE
      basepair_score_t bpScore(minDists[start][stop], basepair_t(start, stop));
      undoSet.insert(bpScore);
#endif
      minDists[start][stop] = value;
      minDists[stop][start] = value;
      if ((probabilities[start][stop] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
	  probabilities[start][stop] = ULTRA_SMALL;
	  probabilities[stop][start] = probabilities[start][stop];
      }
    } else {
#ifdef STRUCTURE_UNDO_MODE
      basepair_score_t bpScore(maxDists[start][stop], basepair_t(start, stop));
      undoSet.insert(bpScore);
#endif
      maxDists[start][stop] = value;
      maxDists[stop][start] = value;
      if (probabilities[start][stop] > 0.0)  {
	probabilities[start][stop] = p0Intra * pow(maxDists[start][stop], - intraCollisionExponent);
	probabilities[stop][start] = probabilities[start][stop];
      }
    }
    adjusted = true;
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 174");
    if (minDists[start][stop] > maxDists[start][stop]) { // found error
      for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	// undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	real_t value = it->first;
	basepair_t bp = it->second;
	if (minMode) {
	  minDists[bp.first][bp.second] = value; 
	  minDists[bp.second][bp.first] = value; 
	  if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
	    probabilities[bp.first][bp.second] = ULTRA_SMALL;
	    probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	  }
	} else {
	  maxDists[bp.first][bp.second] = value; 
	  maxDists[bp.second][bp.first] = value; 
	  if (probabilities[bp.first][bp.second] > 0.0) {
	    probabilities[bp.first][bp.second] = p0Intra * pow(maxDists[bp.first][bp.second], - intraCollisionExponent);
	    probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	  }
	}
      }
      undoSet.clear();
      throw (DISTANCE_CONSTRAINT_EXCEPTION);
    }
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 198");
  } else {
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 199");
THROWIFNOT(fromStart < static_cast<int>(strandIds.size()),"Assertion violation in L2227");
THROWIFNOT(fromStop < static_cast<int>(strandIds.size()),"Assertion violation in L2228");
THROWIFNOT(strandIds[start] == strandIds[fromStart],"Assertion violation in L2229");
THROWIFNOT(strandIds[stop] == strandIds[fromStop],"Assertion violation in L2230");
    real_t x = 0.0;
    if (minMode) {
      x = minDists[fromStart][fromStop] - slop;
    } else {
      x = maxDists[fromStart][fromStop] + slop;
    }
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 212");
    Rcpp::Rcout << "Min mode: " << minMode << endl;
    if ((minMode && (x > minDists[start][stop])) || ((!minMode) && (x < maxDists[start][stop]))) {
      // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 214");
	if (minMode) {
#ifdef STRUCTURE_UNDO_MODE
	  undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop)));
#endif
	  minDists[start][stop] = x; // made more restrictive
	  minDists[stop][start] = x; // made more restrictive
	  if ((probabilities[start][stop] > 0.0) && (x > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
	    probabilities[start][stop] = ULTRA_SMALL;
	    probabilities[stop][start] = probabilities[start][stop];
	  }
	} else {
#ifdef STRUCTURE_UNDO_MODE
	  undoSet.insert(basepair_score_t(maxDists[start][stop], basepair_t(start, stop)));
#endif
	  maxDists[start][stop] = x; // made more restrictive
	  maxDists[stop][start] = x; // made more restrictive
	  if (probabilities[start][stop] > 0.0) {
	    probabilities[start][stop] = p0Intra * pow(maxDists[start][stop], - intraCollisionExponent);
	    probabilities[stop][start] = probabilities[start][stop];
	  }
	}
	adjusted = true;
	// DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 233");
	if (minDists[start][stop] > maxDists[start][stop]) {
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 235");
	  Rcpp::Rcout << "# Contradiction at " << (start+1) << " " << (stop+1) << " " << minDists[start][stop] << " " << maxDists[start][stop] << " difference " 
	       << (maxDists[start][stop]-minDists[start][stop]) << endl;
	  Rcpp::Rcout << "# Minimum distances: " << endl << minDists << endl;
	  Rcpp::Rcout << "# Maximum distances: " << endl << maxDists << endl;
	  for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	    // undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 238");
	    real_t value = it->first;
	    basepair_t bp = it->second;
	    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 241");
	    if (minMode) {
	      minDists[bp.first][bp.second] = value; 
	      minDists[bp.second][bp.first] = value; 
	      if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) { // will be overwrittein in setBasePair anyways
		probabilities[bp.first][bp.second] = ULTRA_SMALL;
		probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	      }
	    } else {
	      maxDists[bp.first][bp.second] = value; 
	      maxDists[bp.second][bp.first] = value; 
	      if (probabilities[bp.first][bp.second] > 0.0) {
		probabilities[bp.first][bp.second] = p0Intra * pow(maxDists[bp.first][bp.second], - intraCollisionExponent);
		probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	      }
	    }
	  }
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 258");
	  undoSet.clear();
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 260");
	  Rcpp::Rcout << "# Minimum distances: " << endl << minDists << endl;
	  Rcpp::Rcout << "# Maximum distances: " << endl << maxDists << endl;
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 265");
	  throw (DISTANCE_CONSTRAINT_EXCEPTION);
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 267");
	}
	// DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 269");
      }
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 271");
  }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 273");
  // ASSERT(validate());
  try {
    if (adjusted) {
      if ((start > 0) && (start-1 < stop) && ((start-1 != fromStart) || (stop != fromStop)) && (strandId1 == strandIds[start-1])) {
	// Rcpp::Rcout << "case 1 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "case 1b .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
	merge(undoSet, setAndAdjustDist(start-1, stop, value, slop, start, stop, minMode));
      }
      if (((start+1) < static_cast<index_t>(paCount)) && (start+1<stop) && ((start+1 != fromStart) || (stop != fromStop)) &&(strandId1 == strandIds[start+1])) {
	// Rcpp::Rcout << "case 2 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+2) << " " << (stop+1) << endl;
	merge(undoSet, setAndAdjustDist(start+1, stop, value, slop, start, stop, minMode));
      }
      if ((stop > 0) && (start < stop-1) && ((start != fromStart) || (stop-1 != fromStop)) && (strandId2 == strandIds[stop-1])) {
	// Rcpp::Rcout << "case 3 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop) << endl;
	merge(undoSet, setAndAdjustDist(start, stop-1, value, slop, start, stop, minMode));
      }
      if (((stop + 1) < static_cast<index_t>(paCount)) && (start < stop+1) && ((start != fromStart) || (stop+1 != fromStop)) &&(strandId2 == strandIds[stop+1])) {
	// Rcpp::Rcout << "case 4 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop+2) << endl;
	merge(undoSet, setAndAdjustDist(start, stop+1, value, slop, start, stop, minMode));
      }
    }
    // ASSERT((start == 0) || (fabs(minDists[start-1][stop] -minDists[start][stop]) <= slop));
    // ASSERT((start+1 >= minDists.size()) || (fabs(minDists[start+1][stop] -minDists[start][stop]) <= slop));
    // ASSERT((stop == 0) || (fabs(minDists[start][stop-1] -minDists[start][stop]) <= slop));
    // ASSERT((stop+1 >= minDists.size()) || (fabs(minDists[start][stop+1]-minDists[start][stop]) <= slop));
    // POSTCOND(validateDistanceConstraintsMatrix(minDists));
    } catch (int e) {
THROWIFNOT(e == DISTANCE_CONSTRAINT_EXCEPTION,"Assertion violation in L2334");
      for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	// undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	real_t value = it->first;
	basepair_t bp = it->second;
	if (minMode) {
	  minDists[bp.first][bp.second] = value; 
	  minDists[bp.second][bp.first] = value; 
	  if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) { // will be overwritten in setBasePair anyways
	    probabilities[bp.first][bp.second] = ULTRA_SMALL;
	    probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	  }
	} else {
	  maxDists[bp.first][bp.second] = value; 
	  maxDists[bp.second][bp.first] = value; 
	  if (probabilities[bp.first][bp.second] > 0.0) {
	    probabilities[bp.first][bp.second] = p0Intra * pow(maxDists[bp.first][bp.second], - intraCollisionExponent);
	    probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	  }
	}
      }
      undoSet.clear();
      throw (DISTANCE_CONSTRAINT_EXCEPTION);
    }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 388");
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "# Finished setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << " Adjusted?: " << adjusted << " " << minDists[start][stop] << endl;
      if (fromStart >= 0 && fromStop >= 0) {
	Rcpp::Rcout << "from :" << (fromStart+1) << " " << (fromStop+1) << " " << minDists[fromStart][fromStop] << endl;
      }
      if (minMode) {
	Rcpp::Rcout << "Final minimum distances: " << endl;
	Rcpp::Rcout << minDists << endl;
      } else {
	Rcpp::Rcout << "Final maximum distances: " << endl;
	Rcpp::Rcout << maxDists << endl;
      }
    }
    // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished..." << endl;
    // ASSERT(validate());
    // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished." << endl;
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 324");
    return undoSet;
}

/** Adjust maximum or minimum distances around position. Returns undo information */
set<basepair_score_pair_t>
ResidueStructure::setAndAdjustDistPair(index_t start, index_t stop, real_t valueMin, real_t valueMax, real_t slop, index_t fromStart, index_t fromStop) throw (int) {
  // DEBUG_MSG("Starting ResidueStructure.setAndAdjustDist " + itos(start+1) + " " + itos(stop+1) + " min mode: " + " slop: " + dtos(slop));
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# starting setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << endl;
    Rcpp::Rcout << "# Initial minimum distances: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
  // Rcpp::Rcout << "# Initial validation before setAndAdjustDist starts..." << endl;
  // ASSERT(validate());
  // Rcpp::Rcout << "# Finished initial validation before setAndAdjustDist starts." << endl;
  PRECOND(paCount == minDists.size());
  set<basepair_score_pair_t> undoSet;
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 133");
  // real_t reactionRadius = getResidueModel()->getReactionRadius();
  real_t intraCollisionExponent = getResidueModel()->getIntraCollisionExponent();
  // real_t reactionVolume = reactionRadius * reactionRadius * reactionRadius;
  real_t p0Intra = getResidueModel()->getIntraCollisionProb0(); // (3.0/(4.0 * PI) ) * reactionVolume * intraCollisionEpsilon;
  if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Not adjusting!" << endl;
    }
    return undoSet; // no change
  }    
THROWIFNOT(start < stop,"Assertion violation in L2404");
  if (!updateDistanceMode) {
    DERROR("Not yet implemented!");
    // return setAndNotAdjustDistPair(start, stop, valueMin, valueMax, stop, fromStart, fromStop);
  }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 149");
  bool adjusted = false;
  size_t strandId1 = strandIds[start];
  size_t strandId2 = strandIds[stop];
  if (fromStart < 0 || fromStop < 0) {
#ifdef STRUCTURE_UNDO_MODE
    basepair_score_pair_t bpScore(minDists[start][stop], maxDists[start][stop],start, stop);
    undoSet.insert(bpScore);
#endif
    minDists[start][stop] = valueMin;
    minDists[stop][start] = valueMin;
    maxDists[start][stop] = valueMax;
    maxDists[stop][start] = valueMax;
    if (probabilities[start][stop] > 0.0) { // && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
      // probabilities[start][stop] = ULTRA_SMALL;
      probabilities[start][stop] = p0Intra * pow(maxDists[start][stop], - intraCollisionExponent); // VERIFY
      probabilities[stop][start] = probabilities[start][stop];
    }
    adjusted = true;
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 174");
    if (minDists[start][stop] > maxDists[start][stop]) { // found error
      for (set<basepair_score_pair_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	// undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	real_t valueMin = it->first;
	real_t valueMax = it->second;
	int bpStart = it->third;
	int bpStop = it->fourth;
	minDists[bpStart][bpStop] = valueMin; 
	minDists[bpStop][bpStart] = valueMin; 
	maxDists[bpStart][bpStop] = valueMax; 
	maxDists[bpStop][bpStart] = valueMax; 
	// if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
	//   probabilities[bp.first][bp.second] = ULTRA_SMALL;
	//   probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	// }
	if (probabilities[bpStart][bpStop] > 0.0) {
	  probabilities[bpStart][bpStop] = p0Intra * pow(maxDists[bpStart][bpStop], - intraCollisionExponent);
	    probabilities[bpStop][bpStart] = probabilities[bpStart][bpStop];
	}
      }
      undoSet.clear();
      throw (DISTANCE_CONSTRAINT_EXCEPTION);
    }
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 198");
  } else {
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 199");
THROWIFNOT(fromStart < static_cast<int>(strandIds.size()),"Assertion violation in L2455");
THROWIFNOT(fromStop < static_cast<int>(strandIds.size()),"Assertion violation in L2456");
THROWIFNOT(strandIds[start] == strandIds[fromStart],"Assertion violation in L2457");
THROWIFNOT(strandIds[stop] == strandIds[fromStop],"Assertion violation in L2458");
    real_t xMin = minDists[fromStart][fromStop] - slop;
    if (xMin < RESIDUE_DIST_MIN) {
      xMin = RESIDUE_DIST_MIN;
    }
    real_t xMax = maxDists[fromStart][fromStop] + slop;
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 212");
    if ((xMin > minDists[start][stop]) || (xMax < maxDists[start][stop])) {
      // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 214");
#ifdef STRUCTURE_UNDO_MODE
	undoSet.insert(basepair_score_pair_t(minDists[start][stop], maxDists[start][stop],start, stop));
#endif
	minDists[start][stop] = xMin; // made more restrictive
	minDists[stop][start] = xMin; // made more restrictive
	maxDists[start][stop] = xMax; // made more restrictive
	maxDists[stop][start] = xMax; // made more restrictive
	//   if ((probabilities[start][stop] > 0.0) && (x > (HELIX_PAIRED_DIST + HELIX_SLOP))) {
	//     probabilities[start][stop] = ULTRA_SMALL;
	//     probabilities[stop][start] = probabilities[start][stop];
	//   }
	// } else {
	if (probabilities[start][stop] > 0.0) {
	  probabilities[start][stop] = p0Intra * pow(maxDists[start][stop], - intraCollisionExponent);
	  probabilities[stop][start] = probabilities[start][stop];
	}
	adjusted = true;
	// DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 233");
	if (minDists[start][stop] > maxDists[start][stop]) {
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 235");
	  Rcpp::Rcout << "# Contradiction at " << (start+1) << " " << (stop+1) << " " << minDists[start][stop] << " " << maxDists[start][stop] << " difference " 
	       << (maxDists[start][stop]-minDists[start][stop]) << endl;
	  Rcpp::Rcout << "# Minimum distances: " << endl << minDists << endl;
	  Rcpp::Rcout << "# Maximum distances: " << endl << maxDists << endl;
	  for (set<basepair_score_pair_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	    // undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 238");
	    real_t valueMin = it->first;
	    real_t valueMax = it->second;
	    int bpStart = it->third;
	    int bpStop = it->fourth;
	    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 241");
	    minDists[bpStart][bpStop] = valueMin; 
	    minDists[bpStop][bpStart] = valueMin; 
	    maxDists[bpStart][bpStop] = valueMax; 
	    maxDists[bpStop][bpStart] = valueMax; 
	    //   if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) { // will be overwrittein in setBasePair anyways
	    // 	probabilities[bp.first][bp.second] = ULTRA_SMALL;
	    // 	probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	    //   }
	    // } else {
	    if (probabilities[bpStart][bpStop] > 0.0) {
	      probabilities[bpStart][bpStop] = p0Intra * pow(maxDists[bpStart][bpStop], - intraCollisionExponent);
	      probabilities[bpStop][bpStart] = probabilities[bpStart][bpStop];
	    }
	  }
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 258");
	  undoSet.clear();
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 260");
	  Rcpp::Rcout << "# Minimum distances: " << endl << minDists << endl;
	  Rcpp::Rcout << "# Maximum distances: " << endl << maxDists << endl;
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 265");
	  throw (DISTANCE_CONSTRAINT_EXCEPTION);
	  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 267");
	}
	// DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 269");
      }
    // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 271");
  }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 273");
  // ASSERT(validate());
  try {
    if (adjusted) {
      if ((start > 0) && (start-1 < stop) && ((start-1 != fromStart) || (stop != fromStop)) && (strandId1 == strandIds[start-1])) {
	// Rcpp::Rcout << "case 1 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "case 1b .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
	merge(undoSet, setAndAdjustDistPair(start-1, stop, valueMin, valueMax, slop, start, stop));
      }
      if (((start+1) < static_cast<index_t>(paCount)) && (start+1<stop) && ((start+1 != fromStart) || (stop != fromStop)) &&(strandId1 == strandIds[start+1])) {
	// Rcpp::Rcout << "case 2 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+2) << " " << (stop+1) << endl;
	merge(undoSet, setAndAdjustDistPair(start+1, stop, valueMin, valueMax, slop, start, stop));
      }
      if ((stop > 0) && (start < stop-1) && ((start != fromStart) || (stop-1 != fromStop)) && (strandId2 == strandIds[stop-1])) {
	// Rcpp::Rcout << "case 3 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop) << endl;
	merge(undoSet, setAndAdjustDistPair(start, stop-1, valueMin, valueMax, slop, start, stop));
      }
      if (((stop + 1) < static_cast<index_t>(paCount)) && (start < stop+1) && ((start != fromStart) || (stop+1 != fromStop)) &&(strandId2 == strandIds[stop+1])) {
	// Rcpp::Rcout << "case 4 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop+2) << endl;
	merge(undoSet, setAndAdjustDistPair(start, stop+1, valueMin, valueMax, slop, start, stop));
      }
    }
    // ASSERT((start == 0) || (fabs(minDists[start-1][stop] -minDists[start][stop]) <= slop));
    // ASSERT((start+1 >= minDists.size()) || (fabs(minDists[start+1][stop] -minDists[start][stop]) <= slop));
    // ASSERT((stop == 0) || (fabs(minDists[start][stop-1] -minDists[start][stop]) <= slop));
    // ASSERT((stop+1 >= minDists.size()) || (fabs(minDists[start][stop+1]-minDists[start][stop]) <= slop));
    // POSTCOND(validateDistanceConstraintsMatrix(minDists));
    } catch (int e) {
THROWIFNOT(e == DISTANCE_CONSTRAINT_EXCEPTION,"Assertion violation in L2555");
      for (auto it = undoSet.begin(); it!= undoSet.end(); it++) {
	// undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	real_t vMin = it->first;
	real_t vMax = it->second;
	int bpStart = it->third;
	int bpStop = it->fourth;
	minDists[bpStart][bpStop] = vMin; 
	minDists[bpStop][bpStart] = vMin; 
	maxDists[bpStart][bpStop] = vMax; 
	maxDists[bpStop][bpStart] = vMax; 
	// if ((probabilities[bp.first][bp.second] > 0.0) && (value > (HELIX_PAIRED_DIST + HELIX_SLOP))) { // will be overwritten in setBasePair anyways
	//   probabilities[bp.first][bp.second] = ULTRA_SMALL;
	//   probabilities[bp.second][bp.first] = probabilities[bp.first][bp.second];
	// }
	// } else {
	if (probabilities[bpStart][bpStop] > 0.0) {
	  probabilities[bpStart][bpStop] = p0Intra * pow(maxDists[bpStart][bpStop], - intraCollisionExponent);
	  probabilities[bpStop][bpStart] = probabilities[bpStart][bpStop];
	}
	  // }
      }
      undoSet.clear();
      throw (DISTANCE_CONSTRAINT_EXCEPTION);
  }
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 388");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# Finished setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << " Adjusted?: " << adjusted << " " << minDists[start][stop] << endl;
    if (fromStart >= 0 && fromStop >= 0) {
      Rcpp::Rcout << "from :" << (fromStart+1) << " " << (fromStop+1) << " " << minDists[fromStart][fromStop] << endl;
    }
  }
  // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished..." << endl;
  // ASSERT(validate());
  // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished." << endl;
  // DEBUG_MSG("Mark ResidueStructure.setAndAdjustDist 324");
  return undoSet;
}

/** Sets base pair without adjusting distances. Used for folding without keeping track of distances. */
set<basepair_score_t>
ResidueStructure::setAndNotAdjustDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop, bool minMode) throw (int) {
  // DEBUG_MSG("Starting setAndNotAdjustDist");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# starting setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << endl;
    Rcpp::Rcout << "# Initial minimum distances: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
  // Rcpp::Rcout << "# Initial validation before setAndAdjustDist starts..." << endl;
  // ASSERT(validate());
  // Rcpp::Rcout << "# Finished initial validation before setAndAdjustDist starts." << endl;

  PRECOND(paCount == minDists.size());
  set<basepair_score_t> undoSet;
  if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "Not adjusting!" << endl;
    }
    return undoSet; // no change
  }    
THROWIFNOT(start < stop,"Assertion violation in L2615");
  bool adjusted = false;
  // size_t strandId1 = strandIds[start];
  // size_t strandId2 = strandIds[stop];
  if (fromStart < 0 || fromStop < 0) {
    if (minMode) {
#ifdef STRUCTURE_UNDO_MODE
      basepair_score_t bpScore(minDists[start][stop], basepair_t(start, stop));
      undoSet.insert(bpScore);
#endif
      // minDists[start][stop] = value;
      // minDists[stop][start] = value;
    } else {
#ifdef STRUCTURE_UNDO_MODE
      basepair_score_t bpScore(maxDists[start][stop], basepair_t(start, stop));
      undoSet.insert(bpScore);
#endif
      // maxDists[start][stop] = value;
      // maxDists[stop][start] = value;
    }
    adjusted = true;
      if (minDists[start][stop] > maxDists[start][stop]) { // found error
	for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	  // undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	  if (minMode) {
	    // minDists[bp.first][bp.second] = value; 
	    // minDists[bp.second][bp.first] = value; 
	  } else {
	    // maxDists[bp.first][bp.second] = value; 
	    // maxDists[bp.second][bp.first] = value; 
	  }
	}
	undoSet.clear();
	throw (DISTANCE_CONSTRAINT_EXCEPTION);
      }
    } else {
THROWIFNOT(strandIds[start] == strandIds[fromStart],"Assertion violation in L2651");
THROWIFNOT(strandIds[stop] == strandIds[fromStop],"Assertion violation in L2652");
      real_t x = 0.0;
      if (minMode) {
	x = minDists[fromStart][fromStop] - slop;
      } else {
	x = maxDists[fromStart][fromStop] + slop;
      }
      if ((minMode && (x > minDists[start][stop])) || ((!minMode) && (x < maxDists[start][stop]))) {
	if (minMode) {
#ifdef STRUCTURE_UNDO_MODE
	  undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop)));
#endif
	  // minDists[start][stop] = x; // made more restrictive
	  // minDists[stop][start] = x; // made more restrictive
	} else {
#ifdef STRUCTURE_UNDO_MODE
	  undoSet.insert(basepair_score_t(maxDists[start][stop], basepair_t(start, stop)));
#endif
	  // maxDists[start][stop] = x; // made more restrictive
	  // maxDists[stop][start] = x; // made more restrictive
	}
	adjusted = true;
	if (minDists[start][stop] > maxDists[start][stop]) {
	  // for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
	  //   // undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
	  //   real_t value = it->first;
	  //   basepair_t bp = it->second;
	  //   if (minMode) {
	  //     // minDists[bp.first][bp.second] = value; 
	  //     // minDists[bp.second][bp.first] = value; 
	  //   } else {
	  //     // maxDists[bp.first][bp.second] = value; 
	  //     // maxDists[bp.second][bp.first] = value; 
	  //   }
	  // }
	  undoSet.clear();
	  throw (DISTANCE_CONSTRAINT_EXCEPTION);
	}
      }
    }
    // ASSERT(validate());
    // try {
    // if (adjusted) {
    //   if ((start > 0) && (start-1 < stop) && ((start-1 != fromStart) || (stop != fromStop)) && (strandId1 == strandIds[start-1])) {
    // 	// Rcpp::Rcout << "case 1 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
    // 	// ASSERT(validate());
    // 	// Rcpp::Rcout << "case 1b .. current: " << (start+1) << " " << (stop+1) << " calling " << (start) << " " << (stop+1) << endl;
    // 	// merge(undoSet, setAndAdjustDist(start-1, stop, value, slop, start, stop, minMode));
    //   }
    //   if (((start+1) < static_cast<index_t>(paCount)) && (start+1<stop) && ((start+1 != fromStart) || (stop != fromStop)) &&(strandId1 == strandIds[start+1])) {
    // 	// Rcpp::Rcout << "case 2 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+2) << " " << (stop+1) << endl;
    // 	// merge(undoSet, setAndAdjustDist(start+1, stop, value, slop, start, stop, minMode));
    //   }
    //   if ((stop > 0) && (start < stop-1) && ((start != fromStart) || (stop-1 != fromStop)) && (strandId2 == strandIds[stop-1])) {
    // 	// Rcpp::Rcout << "case 3 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop) << endl;
    // 	// merge(undoSet, setAndAdjustDist(start, stop-1, value, slop, start, stop, minMode));
    //   }
    //   if (((stop + 1) < static_cast<index_t>(paCount)) && (start < stop+1) && ((start != fromStart) || (stop+1 != fromStop)) &&(strandId2 == strandIds[stop+1])) {
    // 	// Rcpp::Rcout << "case 4 .. current: " << (start+1) << " " << (stop+1) << " calling " << (start+1) << " " << (stop+2) << endl;
    // 	// merge(undoSet, setAndAdjustDist(start, stop+1, value, slop, start, stop, minMode));
    //   }
    // }
    // // ASSERT((start == 0) || (fabs(minDists[start-1][stop] -minDists[start][stop]) <= slop));
    // // ASSERT((start+1 >= minDists.size()) || (fabs(minDists[start+1][stop] -minDists[start][stop]) <= slop));
    // // ASSERT((stop == 0) || (fabs(minDists[start][stop-1] -minDists[start][stop]) <= slop));
    // // ASSERT((stop+1 >= minDists.size()) || (fabs(minDists[start][stop+1]-minDists[start][stop]) <= slop));
    // // POSTCOND(validateDistanceConstraintsMatrix(minDists));
    // } catch (int e) {
    //   ASSERT(e == DISTANCE_CONSTRAINT_EXCEPTION);
    //   for (set<basepair_score_t>::const_iterator it = undoSet.begin(); it!= undoSet.end(); it++) {
    // 	// undoSet.insert(basepair_score_t(minDists[start][stop], basepair_t(start, stop));
    // 	real_t value = it->first;
    // 	basepair_t bp = it->second;
    // 	if (minMode) {
    // 	  minDists[bp.first][bp.second] = value; 
    // 	  minDists[bp.second][bp.first] = value; 
    // 	} else {
    // 	  maxDists[bp.first][bp.second] = value; 
    // 	  maxDists[bp.second][bp.first] = value; 
    // 	}
    //   }
    //   undoSet.clear();
    //   throw (DISTANCE_CONSTRAINT_EXCEPTION);
    // }

    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "# Finished setAndAdjustDist " << (start+1) << " " << (stop+1) << " " << slop << " Adjusted?: " << adjusted << " " << minDists[start][stop] << endl;
      if (fromStart >= 0 && fromStop >= 0) {
	Rcpp::Rcout << "from :" << (fromStart+1) << " " << (fromStop+1) << " " << minDists[fromStart][fromStop] << endl;
      }
      if (minMode) {
	Rcpp::Rcout << "Final minimum distances: " << endl;
	Rcpp::Rcout << minDists << endl;
      } else {
	Rcpp::Rcout << "Final maximum distances: " << endl;
	Rcpp::Rcout << maxDists << endl;
      }
    }
    // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished..." << endl;
    // ASSERT(validate());
    // Rcpp::Rcout << "# Final validation before setAndAdjustDist is finished." << endl;
    return undoSet;
}


/** Adjust maximum distances around position */
bool
ResidueStructure::setAndAdjustMaxDist(index_t start, index_t stop, real_t value, real_t slop, index_t fromStart, index_t fromStop) throw (int) {
  // DEBUG_MSG("Starting setAndAdjustMaxDist");
  ASSERT(false); // Deprecated. use setAndAdjustDist instead
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# starting adjustMaxDist " << (start+1) << " " << (stop+1) << " " << slop << " from " << fromStart << " " << fromStop <<  endl;
  }
    PRECOND(paCount == maxDists.size());
    if ((start < 0) || (stop < 0) || (start >= static_cast<index_t>(paCount)) || (stop >= static_cast<index_t>(paCount))) {
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Out of bounds - Not adjusting!" << endl;
      }
      return false; // no change
    }
THROWIFNOT(start < stop,"Assertion violation in L2772");
    bool adjusted = false;
    size_t strandId1 = strandIds[start];
    size_t strandId2 = strandIds[stop];
    if (fromStart < 0 || fromStop < 0) {
      maxDists[start][stop] = value;
      maxDists[stop][start] = value;
      adjusted = true;
      if (minDists[start][stop] > maxDists[start][stop]) {
	if (verbose >= VERBOSE_FINEST) {
	  Rcpp::Rcout << "Throwing exception, because After adjustment the two values are: " << maxDists[start][stop] << " from orig " << maxDists[fromStart][fromStop] << " is smaller compared to min dist: " << minDists[start][stop] << endl;
	}
	throw (DISTANCE_CONSTRAINT_EXCEPTION);
      }
    } else {
THROWIFNOT(strandIds[start] == strandIds[fromStart],"Assertion violation in L2787");
THROWIFNOT(strandIds[stop] == strandIds[fromStop],"Assertion violation in L2788");
      real_t x = maxDists[fromStart][fromStop] + slop;
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Before adjustment the two values are: " << maxDists[start][stop] << " from orig " << maxDists[fromStart][fromStop] << " x " << x << endl;
      }
      if (x < maxDists[start][stop]) {
	maxDists[start][stop] = x; // made more restrictive
	maxDists[stop][start] = x; // made more restrictive
	adjusted = true;
	if (verbose >= VERBOSE_FINEST) {
	  Rcpp::Rcout << "After adjustment the two values are: " << maxDists[start][stop] << " from orig " << maxDists[fromStart][fromStop] << endl;
	}
	if (minDists[start][stop] > maxDists[start][stop]) {
	  if (verbose >= VERBOSE_FINEST) {
	    Rcpp::Rcout << "Throwing exception, because After adjustment the two values are: " << maxDists[start][stop] << " from orig " << maxDists[fromStart][fromStop] << " is smaller compared to min dist: " << minDists[start][stop] << endl;
	  }
	  throw (DISTANCE_CONSTRAINT_EXCEPTION);
	}
      } else {
	if (verbose >= VERBOSE_FINEST) {
	  Rcpp::Rcout << "No adjustment..." << endl;
	}
      }
      // ASSERT(fabs(maxDists[start][stop]-maxDists[fromStart][fromStop]) <= (slop+EPSILON));
    }
    if (adjusted) {
      if ((start > 0) && (start-1 < stop) && (strandId1 == strandIds[start-1]) && ((start-1 != fromStart) || (stop != fromStop)) ) {
	setAndAdjustMaxDist(start-1, stop, value, slop, start, stop);
      }
      if (((start+1) < static_cast<index_t>(paCount)) && (start+1 < stop) && (strandId1 == strandIds[start+1]) && ((start+1 != fromStart) || (stop != fromStop))  ) {
	setAndAdjustMaxDist(start+1, stop, value, slop, start, stop);
      }
      if ((stop > 0) && (start < stop-1) && (strandId2 == strandIds[stop-1]) && ((start != fromStart) || (stop-1 != fromStop))) {
	setAndAdjustMaxDist(start, stop-1, value, slop, start, stop);
      }
      if (((stop + 1) < static_cast<index_t>(paCount)) && (start < stop+1) && (strandId2 == strandIds[stop+1]) && ((start != fromStart) || (stop+1 != fromStop))) {
	setAndAdjustMaxDist(start, stop+1, value, slop, start, stop);
      }
    } 
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << "# Finished adjustMaxDist " << (start+1) << " " << (stop+1) << " " << slop << " Adjusted?: " << adjusted << " " << maxDists[start][stop] << " from " << fromStart << " " << fromStop << endl;
      if (fromStart >= 0 && fromStop >= 0) {
	Rcpp::Rcout << "From " << (fromStart+1) << " " << (fromStop+1) << " " << maxDists[fromStart][fromStop] << endl;
      }
    }
    // ASSERT((start == 0) || (fabs(maxDists[start-1][stop] -maxDists[start][stop]) <= slop));
    // ASSERT((start+1 >= maxDists.size()) || (fabs(maxDists[start+1][stop] -maxDists[start][stop]) <= slop));
    // ASSERT((stop == 0) || (fabs(maxDists[start][stop-1] -maxDists[start][stop]) <= slop));
    // ASSERT((stop+1 >= maxDists.size()) || (fabs(maxDists[start][stop+1]-maxDists[start][stop]) <= slop));
    // POSTCOND(validateDistanceConstraintsMatrix(maxDists));
    return adjusted;
  }

/** Sets a specified base pair. Also updates min and max distances and strand connectivity matrix */
bool
ResidueStructure::setBasePairRaw(int start, int stop) throw(int) {
  // DEBUG_MSG("Starting setBasePairRaw");
  if (start > stop) {
    return setBasePairRaw(stop, start);
  }
  if (isBasePaired(start, stop)) {
    return false;
  }
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# Starting ResidueStructure::setBasePair " << (start+1) << " " << (stop+1) << endl;
    writeAll(Rcpp::Rcout);
  }
  PRECOND(start < stop);  
  PRECOND(start >= 0);
  PRECOND(stop >= 0);
  PRECOND(start < static_cast<index_t>(paCount));
  PRECOND(stop < static_cast<index_t>(paCount));

THROWIFNOT(validate(),"Assertion violation in L2861");
THROWIFNOT(residueModel != NULL,"Assertion violation in L2862");
THROWIFNOT(start < static_cast<int>(concatSequence.size()),"Assertion violation in L2863");
THROWIFNOT(stop < static_cast<int>(concatSequence.size()),"Assertion violation in L2864");
  ASSERT(minDists[start][stop] <= residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom)); // otherwise base pairing is impossible
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 1: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 2: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // ASSERT(validate());
  // DEBUG_MSG("Mark ResidueStructure.cc 596");
  bool connectivityChanged = false;
THROWIFNOT((start >= 0) && (start < static_cast<int>(pairv.size())),"Assertion violation in L2879");
THROWIFNOT((stop >= 0) && (stop < static_cast<int>(pairv.size())),"Assertion violation in L2880");
THROWIFNOT(!isBasePaired(start, stop),"Assertion violation in L2881");
THROWIFNOT(helixIds[start][stop] < 0,"Assertion violation in L2882");
THROWIFNOT(start < static_cast<index_t>(minDists.size()),"Assertion violation in L2883");
THROWIFNOT(stop < static_cast<index_t>(minDists[start].size()),"Assertion violation in L2884");
THROWIFNOT(start < static_cast<index_t>(concatSequence.size()),"Assertion violation in L2885");
THROWIFNOT(stop < static_cast<index_t>(concatSequence.size()),"Assertion violation in L2886");
THROWIFNOT(minDists[start][stop] <= residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom),"Assertion violation in L2887");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << (minDists[start][stop]) << " " << residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom) << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  // DEBUG_MSG("Mark ResidueStructure.cc 611");

  try {

  for (size_t i = 0; i < paCount; ++i) {
    probabilities[start][i] = 0.0;
    probabilities[i][start] = 0.0;
    probabilities[i][stop] = 0.0;
    probabilities[stop][i] = 0.0;
  }

  Vec<Vec<int> > strandConn = getStrandConnectivityMatrix(); // adjacency matrix of direct and indirect strand interactions
  if (strandIds[start] != strandIds[stop]) {
    strandConn[strandIds[start]][strandIds[stop]] = 1; // take into account new base pair
    strandConn[strandIds[stop]][strandIds[start]] = 1;
  } // otherwise: intra-strand base pair, no change in connectivity
  Vec<Vec<int> > strandIndirectConn = ConnectivityTools::connectivityToIndirectConnectivity(strandConn);
  int n = static_cast<int>(maxDists.size());
THROWIFNOT(start < stop,"Assertion violation in L2910");
  // Rcpp::Rcout << "# Validation before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L2912");
  // Rcpp::Rcout << "# Finished alidation before adjustment of distancesin setBasePairRaw." << endl;
THROWIFNOT((strandIds[start] == strandIds[stop]) || (strandIndirectConn[strandIds[start]][strandIds[stop]] == 1),"Assertion violation in L2914");
  setAndAdjustDist(start,stop,residueModel->getHelixDistMax(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
  // DEBUG_MSG("Mark ResidueStructure.cc 632");
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate: 3" << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate: 4" << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // DEBUG_MSG("Mark ResidueStructure.cc 643");
  // maxDists[stop][start] = maxDists[start][stop]; // RESIDUE_PAIRED_DIST_MAX;
  // minDists[start][stop] = residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
THROWIFNOT(start < stop,"Assertion violation in L2930");

THROWIFNOT(start < stop,"Assertion violation in L2932");
  // Rcpp::Rcout << "# Validation 2 before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L2934");
  // Rcpp::Rcout << "# Finished validation 2 before adjustment of distancesin setBasePairRaw." << endl;
  // DEBUG_MSG("Mark ResidueStructure.cc 652");
  setAndAdjustDist(start,stop,residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
  // DEBUG_MSG("Mark ResidueStructure.cc 654");
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 5: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 6: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // DEBUG_MSG("Mark ResidueStructure.cc 665");
  // minDists[stop][start] = minDists[start][stop];
  if ((start > 0) && ((stop+1) < n) && (helixIds[start-1][stop+1] >= 0) && (strandIds[start-1] == strandIds[start]) && (strandIds[stop+1] == strandIds[stop])) { // extend existing upstream helix
    // Rcpp::Rcout << "# case 1..." << endl;
    // DEBUG_MSG("ResidueStructure.setBasePairRaw 693");
    helixIds[start][stop] = helixIds[start-1][stop+1];
    helixIds[stop][start] = helixIds[start][stop];
THROWIFNOT(start < static_cast<index_t>(helixIds.size()),"Assertion violation in L2956");
THROWIFNOT(stop < static_cast<index_t>(helixIds[start].size()),"Assertion violation in L2957");
THROWIFNOT(helixIds[start][stop] >= 0,"Assertion violation in L2958");
THROWIFNOT(helixIds[start][stop] < static_cast<index_t>(helices.size()),"Assertion violation in L2959");
    Stem& helix = helices[helixIds[start][stop]];
    for (int i = 1; i <= helix.getLength(); ++i) { // adjust helices
      if (start >= i) { // distances on same strand
	// maxDists[start][start-i] = residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 3 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 3 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[start-i]][strandIds[start]] == 1) {
	  setAndAdjustDist(start-i,start,residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	}
	// minDists[start][start-i] = residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 4 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 4 before adjustment of distancesin setBasePairRaw." << endl;

	setAndAdjustDist(start-i,start,residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	// maxDists[start-i][start] = maxDists[start][start-i];
	// minDists[start-i][start] = minDists[start][start-i];
#ifndef NDEBUG
	if (!validateDistanceConstraintsMatrix(maxDists)) {
	  Rcpp::Rcout << "Maximum distances do not validate 7: " << endl;
	  Rcpp::Rcout << maxDists << endl;
	}
	if (!validateDistanceConstraintsMatrix(minDists)) {
	  Rcpp::Rcout << "Minimum distances do not validate 8: " << endl;
	  Rcpp::Rcout << minDists << endl;
	}
#endif
      } else {
	ASSERT(false); // should never happen
      }
      if ((stop+i) <= n) { // opposing strand
	// maxDists[start][stop+i] = residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 5 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
  // Rcpp::Rcout << "# Finished validation 5 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[start]][strandIds[stop+i]] == 1) {
	  setAndAdjustDist(start,stop+i,residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	}
	// minDists[start][stop+i] = residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 6 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 6 before adjustment of distancesin setBasePairRaw." << endl;
	setAndAdjustDist(start,stop+i,residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	// maxDists[stop+i][start] = maxDists[start][stop+i];
	// minDists[stop+i][start] = minDists[start][stop+i];
#ifndef NDEBUG
	if (!validateDistanceConstraintsMatrix(maxDists)) {
	  Rcpp::Rcout << "Maximum distances do not validate 9: " << endl;
	  Rcpp::Rcout << maxDists << endl;
	}
	if (!validateDistanceConstraintsMatrix(minDists)) {
	  Rcpp::Rcout << "Minimum distances do not validate 10: " << endl;
	  Rcpp::Rcout << minDists << endl;
	}
      /* Rcpp::Rcout << "#  Adjusted helical distance " << (start+1) << " " << (stop+1) << " iter " << i << endl; */
      /* Rcpp::Rcout << "# max distances:" << endl; */
      /* Rcpp::Rcout << maxDists << endl; */
      /* Rcpp::Rcout << "# min distances:" << endl; */
      /* Rcpp::Rcout << minDists << endl; */
#endif
      }
    }
    // DEBUG_MSG("Mark ResidueStructure.cc 739");
    helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
    updateHelixEnergy(helixIds[start][stop]);
  }
  // DEBUG_MSG("Mark ResidueStructure.cc 743");
  if ((stop > 0) && ((start+1) < n) && (helixIds[start+1][stop-1] >= 0) && (strandIds[start+1] == strandIds[start]) && (strandIds[stop-1] == strandIds[stop])) { // case 2: extend to downstream helix?
    if (helixIds[start][stop] >= 0) { // case 2a: Fusing of helices !
      // DEBUG_MSG("ResidueStructure.setBasePairRaw 770");
      int helixId = helixIds[start][stop];
      Stem& helix = helices[helixIds[start][stop]];
      int helixId2 = helixIds[start+1][stop-1];
THROWIFNOT(helixId2 < static_cast<int>(helices.size()),"Assertion violation in L3034");
      Stem& helix2 = helices[helixId2];
      for (int i = 0; i < helix2.getLength(); ++i) {
	helixIds[helix2.getStart() + i][helix2.getStop()-i] = helixId;
      }
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Fusing helices " << helix << " and " << helix2 << endl;
      }
      helix.setLength(helix.getLength() + helix2.getLength()); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(helixId);
      helix2.setLength(0);
      helix2.setEnergy(0.0);
THROWIFNOT(helix.getStart() < helix.getStop(),"Assertion violation in L3046");
      for (int i = 0; i < helix.getLength(); ++i) {
	// direct base pair: should not be necessary
	// Rcpp::Rcout << "# Validation 7 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 7 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStop()-i]] == 1) {
	  setAndAdjustDist(helix.getStart() + i, helix.getStop() - i,residueModel->getHelixDistMax(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	}
	// Rcpp::Rcout << "# Validation 8 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 8 before adjustment of distancesin setBasePairRaw." << endl;

	setAndAdjustDist(helix.getStart() + i, helix.getStop() - i,residueModel->getHelixDistMin(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	for (int j = (i+1); j < helix.getLength(); ++j) {
	    // distances w.r.t. opposing strand:
	  if (verbose >= VERBOSE_FINEST) {
	    Rcpp::Rcout << "Fusing helices... position pair " << (helix.getStart()+i) << " " << (helix.getStop()-j) << endl;
	  }
THROWIFNOT(helix.getStart()+i < helix.getStop()-j,"Assertion violation in L3065");
	  // Rcpp::Rcout << "# Validation 9 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 9 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStop()-j]] == 1) {
	    setAndAdjustDist(helix.getStart() + i, helix.getStop() - j,residueModel->getHelixDistMax((j-i), false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-j], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1,false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // Rcpp::Rcout << "# Validation 10 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 10 before adjustment of distancesin setBasePairRaw." << endl;

	  setAndAdjustDist(helix.getStart() + i, helix.getStop() - j,residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-j], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	  
THROWIFNOT(helix.getStart()+j < helix.getStop()-i,"Assertion violation in L3078");
	  if (strandIndirectConn[strandIds[helix.getStart()+j]][strandIds[helix.getStop()-i]] == 1) {
	    setAndAdjustDist(helix.getStart() + j, helix.getStop() - i,residueModel->getHelixDistMax((j-i), false, concatSequence[helix.getStart()+j], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  setAndAdjustDist(helix.getStart() + j, helix.getStop() - i,residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+j], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	  
THROWIFNOT(helix.getStart()+i < helix.getStart()+j,"Assertion violation in L3084");
	  // distances w.r.t. same strand:
	  // Rcpp::Rcout << "# Validation 11 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 11 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStart()+j]] == 1) {
	    setAndAdjustDist(helix.getStart() + i, helix.getStart() + j,residueModel->getHelixDistMax((j-i), true, concatSequence[helix.getStart()+i], concatSequence[helix.getStart()+j], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  setAndAdjustDist(helix.getStart() + i, helix.getStart() + j,residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStart()+i], concatSequence[helix.getStart()+j], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;

THROWIFNOT(helix.getStop() - j < helix.getStop() - i,"Assertion violation in L3094");
	  if (strandIndirectConn[strandIds[helix.getStop()-j]][strandIds[helix.getStop()-i]] == 1) {
	    setAndAdjustDist(helix.getStop() - j, helix.getStop() - i,residueModel->getHelixDistMax((j-i), true, concatSequence[helix.getStop()-j], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  setAndAdjustDist(helix.getStop() - j, helix.getStop() - i,residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStop()-j], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	  // Rcpp::Rcout << "# Validation 12 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 12 before adjustment of distancesin setBasePairRaw." << endl;

#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 11: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 12: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
      }
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Finished fusing helices " << helix << " and " << helix2 << " as a result of placing " << (start+1) << ":" << (stop+1) << endl;
	// ASSERT(validate()); too early to validate
      }
    } else { // case 2b: extend helix start start + 1, stop - 1
      // DEBUG_MSG("ResidueStructure.setBasePairRaw 859");
      helixIds[start][stop] = helixIds[start+1][stop-1];
      helixIds[stop][start] = helixIds[start][stop];
THROWIFNOT(helixIds[start][stop] >= 0,"Assertion violation in L3123");
THROWIFNOT(helixIds[start][stop] < static_cast<index_t>(helices.size()),"Assertion violation in L3124");
      Stem& helix = helices[helixIds[start][stop]];
      for (int i = 1; i <= helix.getLength(); ++i) {
	if ((start + i) < n) { // distances on same strand
	  // maxDists[start][start+i] = residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	  // Rcpp::Rcout << "# Validation 13 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 13 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[start]][strandIds[start+i]] == 1) {	  
	    setAndAdjustDist(start,start+i,residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // minDists[start][start+i] = residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	  setAndAdjustDist(start,start+i,residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	  // maxDists[start+i][start] = maxDists[start][start+i];
	  // minDists[start+i][start] = minDists[start][start+i];
#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 13: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 14: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
	if ((stop-i) > 0) { // opposing strand
	  // maxDists[start][stop-i] = residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX; // bug with stop+i?
THROWIFNOT(start < stop-i,"Assertion violation in L3152");
	  // Rcpp::Rcout << "# Validation 14 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 14 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[start]][strandIds[stop-i]] == 1) {
	    setAndAdjustDist(start,stop-i,residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // minDists[start][stop-i] = residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	  setAndAdjustDist(start,stop-i,residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
	  // maxDists[stop-i][start] = maxDists[start][stop-i];
	  // minDists[stop-i][start] = minDists[start][stop-i];
#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 15: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 16: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
      }
      helix.setStart(helix.getStart()-1);
      helix.setStop(helix.getStop()+1);
      helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(helixIds[stop][start]);
    }
  }
  
  if (helixIds[start][stop] < 0) { // start new helix
      // Rcpp::Rcout << "# case 3..." << endl;
    helices.push_back(Stem(start, stop, 1));
    helixIds[start][stop] = (helices.size()-1);
    helixIds[stop][start] = helixIds[start][stop];
    updateHelixEnergy(helixIds[stop][start]);
  }
  
  // how for to go out?
  if (verbose >= VERBOSE_FINE) {
    Rcpp::Rcout << "Max distances after first setting of max dist of residues " << (start+1) << " " << (stop+1) << endl;    
    Rcpp::Rcout << maxDists << endl;
    Rcpp::Rcout << "Min distances after first setting of max dist of residues " << (start+1) << " " << (stop+1) << endl;    
    Rcpp::Rcout << minDists << endl;
    Rcpp::Rcout << "# Probabilities: " << endl;
    Rcpp::Rcout << probabilities << endl;
  }

  // finally set pair:
  // pairings[start][stop] = 1;
  // pairings[stop][start] = 1;
THROWIFNOT(start < static_cast<index_t>(pairv.size()),"Assertion violation in L3203");
THROWIFNOT(start >= 0,"Assertion violation in L3204");
THROWIFNOT(stop < static_cast<index_t>(pairv.size()),"Assertion violation in L3205");
THROWIFNOT(stop >= 0,"Assertion violation in L3206");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "Updating pairv: " << pairv;
  }
  pairv[start] = stop;
  pairv[stop] = start;
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << " to " << pairv << endl;
  }

  size_t strandId1 = strandIds[start];
  size_t strandId2 = strandIds[stop];
THROWIFNOT(strandId1 < strandConnectivity.size(),"Assertion violation in L3218");
THROWIFNOT(strandId2 < strandConnectivity[strandId1].size(),"Assertion violation in L3219");
  string hash = BasePairTools::generateBasePairHash(start, stop);
  if ((strandId1 != strandId2)
      && (strandConnectivity[strandId1][strandId2].size() == 0)) {
    connectivityChanged = true; // first base pair between two strands: connectivity changes.
    if ((trackEventMode && (verbose >= VERBOSE_INFO))
	|| (!trackEventMode && (verbose >= VERBOSE_DETAILED))) {
      if (!trackEventMode) {
	Rcpp::Rcout << "#### Untracked: ";
      }
      Rcpp::Rcout << "# The new base pair " << (start+1) << " " << (stop+1) 
	   << " introduces a new connectivity between strands " << (strandId1+1) << " and "
	   << (strandId2 + 1) << endl;
    }
  } else {
    connectivityChanged = false;
  }
  strandConnectivity[strandId1][strandId2].insert(hash);
  strandConnectivity[strandId2][strandId1].insert(hash);
  // energy += computeBasePairEnergyContribution(start, stop);
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 17: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 18: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  updateResidueProbabilities();
  // if (verbose >= VERBOSE_FINEST) {
  //   Rcpp::Rcout << "# Finished ResidueStructure::setBasePair " << (start+1) << " " << (stop+1) << endl;
  //   Rcpp::Rcout << "# Current strand connectivity: " << endl << strandConnectivity << endl;
  //   writeAll(Rcpp::Rcout);
  // }
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate Final: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate Final: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // Rcpp::Rcout << "Final validation before setBasePairRaw is finished..." << endl;
  if (!validate()) {
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Structure does not validate! Throwing exception..." << endl;
    }
    throw (DISTANCE_CONSTRAINT_EXCEPTION);
  }
THROWIFNOT(validate(),"Assertion violation in L3272");
  } catch (int e) {
    string msg = "Caught exception in setBasePairRaw (1010)" + itos(start+1) + " " + itos(stop+1);
    DEBUG_MSG(msg);
    throw (e);
  }
  // Rcpp::Rcout << "Final validation finished." << endl;
  // DEBUG_MSG("Mark ResidueStructure.cc 1014");
  return connectivityChanged;
}

/** Sets a specified base pair. Also updates min and max distances and strand connectivity matrix.
 * Uses setAndUpdateDistPair instead of setAndUpdateDist. 
 */
bool
ResidueStructure::setBasePairRaw2(int start, int stop) throw(int) {
  // DEBUG_MSG("Starting setBasePairRaw2");
  if (start > stop) {
    return setBasePairRaw2(stop, start);
  }
  if (isBasePaired(start, stop)) {
    return false;
  }
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "# Starting ResidueStructure::setBasePair " << (start+1) << " " << (stop+1) << endl;
    writeAll(Rcpp::Rcout);
  }
  PRECOND(start < stop);  
  PRECOND(start >= 0);
  PRECOND(stop >= 0);
  PRECOND(start < static_cast<index_t>(paCount));
  PRECOND(stop < static_cast<index_t>(paCount));

THROWIFNOT(validate(),"Assertion violation in L3305");
THROWIFNOT(residueModel != NULL,"Assertion violation in L3306");
THROWIFNOT(start < static_cast<int>(concatSequence.size()),"Assertion violation in L3307");
THROWIFNOT(stop < static_cast<int>(concatSequence.size()),"Assertion violation in L3308");
  ASSERT(minDists[start][stop] <= residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom)); // otherwise base pairing is impossible
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 20: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 21: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // ASSERT(validate());
  // DEBUG_MSG("Mark ResidueStructure.cc 1278");
  bool connectivityChanged = false;
THROWIFNOT((start >= 0) && (start < static_cast<int>(pairv.size())),"Assertion violation in L3323");
THROWIFNOT((stop >= 0) && (stop < static_cast<int>(pairv.size())),"Assertion violation in L3324");
THROWIFNOT(!isBasePaired(start, stop),"Assertion violation in L3325");
THROWIFNOT(helixIds[start][stop] < 0,"Assertion violation in L3326");
THROWIFNOT(start < static_cast<index_t>(minDists.size()),"Assertion violation in L3327");
THROWIFNOT(stop < static_cast<index_t>(minDists[start].size()),"Assertion violation in L3328");
THROWIFNOT(start < static_cast<index_t>(concatSequence.size()),"Assertion violation in L3329");
THROWIFNOT(stop < static_cast<index_t>(concatSequence.size()),"Assertion violation in L3330");
THROWIFNOT(minDists[start][stop] <= residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom),"Assertion violation in L3331");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << (minDists[start][stop]) << " " << residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom) << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  // DEBUG_MSG("Mark ResidueStructure.cc 1293");

  try {

  for (size_t i = 0; i < paCount; ++i) {
    probabilities[start][i] = 0.0;
    probabilities[i][start] = 0.0;
    probabilities[i][stop] = 0.0;
    probabilities[stop][i] = 0.0;
  }

  Vec<Vec<int> > strandConn = getStrandConnectivityMatrix(); // adjacency matrix of direct and indirect strand interactions
  if (strandIds[start] != strandIds[stop]) {
    strandConn[strandIds[start]][strandIds[stop]] = 1; // take into account new base pair
    strandConn[strandIds[stop]][strandIds[start]] = 1;
  } // otherwise: intra-strand base pair, no change in connectivity
  Vec<Vec<int> > strandIndirectConn = ConnectivityTools::connectivityToIndirectConnectivity(strandConn);
  int n = static_cast<int>(maxDists.size());
THROWIFNOT(start < stop,"Assertion violation in L3354");
  // Rcpp::Rcout << "# Validation before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L3356");
  // Rcpp::Rcout << "# Finished alidation before adjustment of distancesin setBasePairRaw." << endl;
THROWIFNOT((strandIds[start] == strandIds[stop]) || (strandIndirectConn[strandIds[start]][strandIds[stop]] == 1),"Assertion violation in L3358");
  setAndAdjustDistPair(start,stop,
		       residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom), 
		       residueModel->getHelixDistMax(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
  // DEBUG_MSG("Mark ResidueStructure.cc 1319");
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate: 22" << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate: 23" << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // DEBUG_MSG("Mark ResidueStructure.cc 1330");
  // maxDists[stop][start] = maxDists[start][stop]; // RESIDUE_PAIRED_DIST_MAX;
  // minDists[start][stop] = residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
THROWIFNOT(start < stop,"Assertion violation in L3376");

THROWIFNOT(start < stop,"Assertion violation in L3378");
  // Rcpp::Rcout << "# Validation 2 before adjustment of distances in setBasePairRaw ..." << endl;
THROWIFNOT(validate(),"Assertion violation in L3380");
  // Rcpp::Rcout << "# Finished validation 2 before adjustment of distancesin setBasePairRaw." << endl;
  // DEBUG_MSG("Mark ResidueStructure.cc 1339");
  // setAndAdjustDist(start,stop,residueModel->getHelixDistMin(0, false, concatSequence[start], concatSequence[stop], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, true); // RESIDUE_PAIRED_DIST_MAX;
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 24: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 25: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // DEBUG_MSG("Mark ResidueStructure.cc 1351");
  // minDists[stop][start] = minDists[start][stop];
  if ((start > 0) && ((stop+1) < n) && (helixIds[start-1][stop+1] >= 0) && (strandIds[start-1] == strandIds[start]) && (strandIds[stop+1] == strandIds[stop])) { // extend existing upstream helix
    // Rcpp::Rcout << "# case 1..." << endl;
    // DEBUG_MSG("ResidueStructure.setBasePairRaw 1355");
    helixIds[start][stop] = helixIds[start-1][stop+1];
    helixIds[stop][start] = helixIds[start][stop];
THROWIFNOT(start < static_cast<index_t>(helixIds.size()),"Assertion violation in L3401");
THROWIFNOT(stop < static_cast<index_t>(helixIds[start].size()),"Assertion violation in L3402");
THROWIFNOT(helixIds[start][stop] >= 0,"Assertion violation in L3403");
THROWIFNOT(helixIds[start][stop] < static_cast<index_t>(helices.size()),"Assertion violation in L3404");
    Stem& helix = helices[helixIds[start][stop]];
    for (int i = 1; i <= helix.getLength(); ++i) { // adjust helices
      if (start >= i) { // distances on same strand
	// maxDists[start][start-i] = residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 3 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 3 before adjustment of distancesin setBasePairRaw." << endl;

	// if (strandIndirectConn[strandIds[start-i]][strandIds[start]] == 1) {
	  // setAndAdjustDist(start-i,start,residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  // 	}
	// minDists[start][start-i] = residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 4 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 4 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[start-i]][strandIds[start]] == 1) {
	  setAndAdjustDistPair(start-i,start,
			       residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom),
			       residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom),
			       RESIDUE_ADJ_DIST_MAX, -1, -1); // verify
	} else {
	  setAndAdjustDistPair(start-i,start,
			       residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start-i], refAtom, refAtom),
			       maxDists[start-i][start],
			       RESIDUE_ADJ_DIST_MAX, -1, -1); // verify
	}
	// maxDists[start-i][start] = maxDists[start][start-i];
	// minDists[start-i][start] = minDists[start][start-i];
#ifndef NDEBUG
	if (!validateDistanceConstraintsMatrix(maxDists)) {
	  Rcpp::Rcout << "Maximum distances do not validate 26: " << endl;
	  Rcpp::Rcout << maxDists << endl;
	}
	if (!validateDistanceConstraintsMatrix(minDists)) {
	  Rcpp::Rcout << "Minimum distances do not validate 27: " << endl;
	  Rcpp::Rcout << minDists << endl;
	}
#endif
      } else {
	ASSERT(false); // should never happen
      }
      if ((stop+i) <= n) { // opposing strand
	// maxDists[start][stop+i] = residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 5 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
  // Rcpp::Rcout << "# Finished validation 5 before adjustment of distancesin setBasePairRaw." << endl;
	// if (strandIndirectConn[strandIds[start]][strandIds[stop+i]] == 1) {
	  // setAndAdjustDist(start,stop+i,residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	  // }
	// minDists[start][stop+i] = residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	// Rcpp::Rcout << "# Validation 6 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 6 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[start]][strandIds[stop+i]] == 1) {
	  setAndAdjustDistPair(start,stop+i,
			       residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom),
			       residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1); // VERIFY
	} else {
	  setAndAdjustDistPair(start,stop+i,
			       residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom),
			       maxDists[start][stop+i],
			       RESIDUE_ADJ_DIST_MAX, -1, -1); // VERIFY
	}
	// maxDists[stop+i][start] = maxDists[start][stop+i];
	// minDists[stop+i][start] = minDists[start][stop+i];
#ifndef NDEBUG
	if (!validateDistanceConstraintsMatrix(maxDists)) {
	  Rcpp::Rcout << "Maximum distances do not validate 28: " << endl;
	  Rcpp::Rcout << maxDists << endl;
	}
	if (!validateDistanceConstraintsMatrix(minDists)) {
	  Rcpp::Rcout << "Minimum distances do not validate 29: " << endl;
	  Rcpp::Rcout << minDists << endl;
	}
      /* Rcpp::Rcout << "#  Adjusted helical distance " << (start+1) << " " << (stop+1) << " iter " << i << endl; */
      /* Rcpp::Rcout << "# max distances:" << endl; */
      /* Rcpp::Rcout << maxDists << endl; */
      /* Rcpp::Rcout << "# min distances:" << endl; */
      /* Rcpp::Rcout << minDists << endl; */
#endif
      }
    }
    // DEBUG_MSG("Mark ResidueStructure.cc 1444");
    helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
    updateHelixEnergy(helixIds[start][stop]);
  }
  // DEBUG_MSG("Mark ResidueStructure.cc 1448");
  if ((stop > 0) && ((start+1) < n) && (helixIds[start+1][stop-1] >= 0) && (strandIds[start+1] == strandIds[start]) && (strandIds[stop-1] == strandIds[stop])) { // case 2: extend to downstream helix?
    if (helixIds[start][stop] >= 0) { // case 2a: Fusing of helices !
      // DEBUG_MSG("ResidueStructure.setBasePairRaw 1451");
      int helixId = helixIds[start][stop];
      Stem& helix = helices[helixIds[start][stop]];
      int helixId2 = helixIds[start+1][stop-1];
THROWIFNOT(helixId2 < static_cast<int>(helices.size()),"Assertion violation in L3498");
      Stem& helix2 = helices[helixId2];
      for (int i = 0; i < helix2.getLength(); ++i) {
	helixIds[helix2.getStart() + i][helix2.getStop()-i] = helixId;
      }
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Fusing helices " << helix << " and " << helix2 << endl;
      }
      helix.setLength(helix.getLength() + helix2.getLength()); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(helixId);
      helix2.setLength(0);
      helix2.setEnergy(0.0);
THROWIFNOT(helix.getStart() < helix.getStop(),"Assertion violation in L3510");
      for (int i = 0; i < helix.getLength(); ++i) {
	// direct base pair: should not be necessary
	// Rcpp::Rcout << "# Validation 7 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 7 before adjustment of distancesin setBasePairRaw." << endl;
	// if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStop()-i]] == 1) {
	//   setAndAdjustDist(helix.getStart() + i, helix.getStop() - i,residueModel->getHelixDistMax(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom), RESIDUE_ADJ_DIST_MAX, -1, -1, false); // RESIDUE_PAIRED_DIST_MAX;
	// }
	// Rcpp::Rcout << "# Validation 8 before adjustment of distances in setBasePairRaw ..." << endl;
	// ASSERT(validate());
	// Rcpp::Rcout << "# Finished validation 8 before adjustment of distancesin setBasePairRaw." << endl;
	if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStop()-i]] == 1) {
	  setAndAdjustDistPair(helix.getStart() + i, helix.getStop() - i,
			       residueModel->getHelixDistMin(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom),
			     residueModel->getHelixDistMax(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom),
			       RESIDUE_ADJ_DIST_MAX, -1, -1); // VERIFY
	} else {
	  setAndAdjustDistPair(helix.getStart() + i, helix.getStop() - i,
			       residueModel->getHelixDistMin(0, false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-i], refAtom, refAtom),
			       maxDists[helix.getStart() + i][helix.getStop() - i],
			       RESIDUE_ADJ_DIST_MAX, -1, -1); // VERIFY
	}
	for (int j = (i+1); j < helix.getLength(); ++j) {
	    // distances w.r.t. opposing strand:
	  if (verbose >= VERBOSE_FINEST) {
	    Rcpp::Rcout << "Fusing helices... position pair " << (helix.getStart()+i) << " " << (helix.getStop()-j) << endl;
	  }
THROWIFNOT(helix.getStart()+i < helix.getStop()-j,"Assertion violation in L3538");
	  // Rcpp::Rcout << "# Validation 9 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 9 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStop()-j]] == 1) {
	    setAndAdjustDistPair(helix.getStart() + i, helix.getStop() - j,
				 residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-j], refAtom, refAtom),
				 residueModel->getHelixDistMax((j-i), false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-j], refAtom, refAtom),
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(helix.getStart() + i, helix.getStop() - j,
				 residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+i], concatSequence[helix.getStop()-j], refAtom, refAtom),
				 maxDists[helix.getStart() + i][helix.getStop() - j], 
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
THROWIFNOT(helix.getStart()+j < helix.getStop()-i,"Assertion violation in L3553");
	  if (strandIndirectConn[strandIds[helix.getStart()+j]][strandIds[helix.getStop()-i]] == 1) {
	    setAndAdjustDistPair(helix.getStart() + j, helix.getStop() - i,
				 residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+j], concatSequence[helix.getStop()-i], refAtom, refAtom),
				 residueModel->getHelixDistMax((j-i), false, concatSequence[helix.getStart()+j], concatSequence[helix.getStop()-i], refAtom, refAtom),
			     RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(helix.getStart() + j, helix.getStop() - i,
				 residueModel->getHelixDistMin((j-i), false, concatSequence[helix.getStart()+j], concatSequence[helix.getStop()-i], refAtom, refAtom),
				 maxDists[helix.getStart() + j][helix.getStop() - i],
			     RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
THROWIFNOT(helix.getStart()+i < helix.getStart()+j,"Assertion violation in L3565");
	  // distances w.r.t. same strand:
	  // Rcpp::Rcout << "# Validation 11 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 11 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[helix.getStart()+i]][strandIds[helix.getStart()+j]] == 1) {
	    setAndAdjustDistPair(helix.getStart() + i, helix.getStart() + j,
				 residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStart()+i], concatSequence[helix.getStart()+j], refAtom, refAtom),
				 residueModel->getHelixDistMax((j-i), true, concatSequence[helix.getStart()+i], concatSequence[helix.getStart()+j], refAtom, refAtom),
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(helix.getStart() + i, helix.getStart() + j,
				 residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStart()+i], concatSequence[helix.getStart()+j], refAtom, refAtom),
				 maxDists[helix.getStart() + i][helix.getStart() + j],
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
THROWIFNOT(helix.getStop() - j < helix.getStop() - i,"Assertion violation in L3581");
	  if (strandIndirectConn[strandIds[helix.getStop()-j]][strandIds[helix.getStop()-i]] == 1) {
	    setAndAdjustDistPair(helix.getStop() - j, helix.getStop() - i,
				 residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStop()-j], concatSequence[helix.getStop()-i], refAtom, refAtom),
				 residueModel->getHelixDistMax((j-i), true, concatSequence[helix.getStop()-j], concatSequence[helix.getStop()-i], refAtom, refAtom),
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(helix.getStop() - j, helix.getStop() - i,
				 residueModel->getHelixDistMin((j-i), true, concatSequence[helix.getStop()-j], concatSequence[helix.getStop()-i], refAtom, refAtom),
				 maxDists[helix.getStop() - j][helix.getStop() - i],
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // Rcpp::Rcout << "# Validation 12 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 12 before adjustment of distancesin setBasePairRaw." << endl;

#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 30: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 31: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
      }
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Finished fusing helices " << helix << " and " << helix2 << " as a result of placing " << (start+1) << ":" << (stop+1) << endl;
	// ASSERT(validate()); too early to validate
      }
    } else { // case 2b: extend helix start start + 1, stop - 1
      // DEBUG_MSG("ResidueStructure.setBasePairRaw 1571");
      helixIds[start][stop] = helixIds[start+1][stop-1];
      helixIds[stop][start] = helixIds[start][stop];
THROWIFNOT(helixIds[start][stop] >= 0,"Assertion violation in L3617");
THROWIFNOT(helixIds[start][stop] < static_cast<index_t>(helices.size()),"Assertion violation in L3618");
      Stem& helix = helices[helixIds[start][stop]];
      for (int i = 1; i <= helix.getLength(); ++i) {
	if ((start + i) < n) { // distances on same strand
	  // maxDists[start][start+i] = residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX;
	  // Rcpp::Rcout << "# Validation 13 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 13 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[start]][strandIds[start+i]] == 1) {	  
	    setAndAdjustDistPair(start,start+i,
				 residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom), 
				 residueModel->getHelixDistMax(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom), 
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(start,start+i,
				 residueModel->getHelixDistMin(i, true, concatSequence[start], concatSequence[start+i], refAtom, refAtom), 
				 maxDists[start][start+i],
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // maxDists[start+i][start] = maxDists[start][start+i];
	  // minDists[start+i][start] = minDists[start][start+i];
#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 32: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 33: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
	if ((stop-i) > 0) { // opposing strand
	  // maxDists[start][stop-i] = residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop+i], refAtom, refAtom); // RESIDUE_PAIRED_DIST_MAX; // bug with stop+i?
THROWIFNOT(start < stop-i,"Assertion violation in L3652");
	  // Rcpp::Rcout << "# Validation 14 before adjustment of distances in setBasePairRaw ..." << endl;
	  // ASSERT(validate());
	  // Rcpp::Rcout << "# Finished validation 14 before adjustment of distancesin setBasePairRaw." << endl;
	  if (strandIndirectConn[strandIds[start]][strandIds[stop-i]] == 1) {
	    setAndAdjustDistPair(start,stop-i,
				 residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop-i], refAtom, refAtom),
				 residueModel->getHelixDistMax(i, false, concatSequence[start], concatSequence[stop-i], refAtom, refAtom),
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  } else {
	    setAndAdjustDistPair(start,stop-i,
				 residueModel->getHelixDistMin(i, false, concatSequence[start], concatSequence[stop-i], refAtom, refAtom),
				 maxDists[start][stop-i],
				 RESIDUE_ADJ_DIST_MAX, -1, -1); // RESIDUE_PAIRED_DIST_MAX;
	  }
	  // maxDists[stop-i][start] = maxDists[start][stop-i];
	  // minDists[stop-i][start] = minDists[start][stop-i];
#ifndef NDEBUG
	  if (!validateDistanceConstraintsMatrix(maxDists)) {
	    Rcpp::Rcout << "Maximum distances do not validate 34: " << endl;
	    Rcpp::Rcout << maxDists << endl;
	  }
	  if (!validateDistanceConstraintsMatrix(minDists)) {
	    Rcpp::Rcout << "Minimum distances do not validate 35: " << endl;
	    Rcpp::Rcout << minDists << endl;
	  }
#endif
	}
      }
      helix.setStart(helix.getStart()-1);
      helix.setStop(helix.getStop()+1);
      helix.setLength(helix.getLength() + 1); // extend length of helix. Careful: sequence internal to helix has not been adjusted      
      updateHelixEnergy(helixIds[stop][start]);
    }
  }
  
  if (helixIds[start][stop] < 0) { // start new helix
      // Rcpp::Rcout << "# case 3..." << endl;
    helices.push_back(Stem(start, stop, 1));
    helixIds[start][stop] = (helices.size()-1);
    helixIds[stop][start] = helixIds[start][stop];
    updateHelixEnergy(helixIds[stop][start]);
  }
  
  // how for to go out?
  if (verbose >= VERBOSE_FINE) {
    Rcpp::Rcout << "Max distances after first setting of max dist of residues " << (start+1) << " " << (stop+1) << endl;    
    Rcpp::Rcout << maxDists << endl;
    Rcpp::Rcout << "Min distances after first setting of max dist of residues " << (start+1) << " " << (stop+1) << endl;    
    Rcpp::Rcout << minDists << endl;
    Rcpp::Rcout << "# Probabilities: " << endl;
    Rcpp::Rcout << probabilities << endl;
  }

  // finally set pair:
  // pairings[start][stop] = 1;
  // pairings[stop][start] = 1;
THROWIFNOT(start < static_cast<index_t>(pairv.size()),"Assertion violation in L3709");
THROWIFNOT(start >= 0,"Assertion violation in L3710");
THROWIFNOT(stop < static_cast<index_t>(pairv.size()),"Assertion violation in L3711");
THROWIFNOT(stop >= 0,"Assertion violation in L3712");
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << "Updating pairv: " << pairv;
  }
  pairv[start] = stop;
  pairv[stop] = start;
  if (verbose >= VERBOSE_FINEST) {
    Rcpp::Rcout << " to " << pairv << endl;
  }

  size_t strandId1 = strandIds[start];
  size_t strandId2 = strandIds[stop];
THROWIFNOT(strandId1 < strandConnectivity.size(),"Assertion violation in L3724");
THROWIFNOT(strandId2 < strandConnectivity[strandId1].size(),"Assertion violation in L3725");
  string hash = BasePairTools::generateBasePairHash(start, stop);
  if ((strandId1 != strandId2)
      && (strandConnectivity[strandId1][strandId2].size() == 0)) {
    connectivityChanged = true; // first base pair between two strands: connectivity changes.
    if ((trackEventMode && (verbose >= VERBOSE_INFO))
	|| (!trackEventMode && (verbose >= VERBOSE_DETAILED))) {
      if (!trackEventMode) {
	Rcpp::Rcout << "#### Untracked: ";
      }
      Rcpp::Rcout << "# The new base pair " << (start+1) << " " << (stop+1) 
	   << " introduces a new connectivity between strands " << (strandId1+1) << " and "
	   << (strandId2 + 1) << endl;
    }
  } else {
    connectivityChanged = false;
  }
  strandConnectivity[strandId1][strandId2].insert(hash);
  strandConnectivity[strandId2][strandId1].insert(hash);
  // energy += computeBasePairEnergyContribution(start, stop);
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate 38: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate 39: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  updateResidueProbabilities();
  // if (verbose >= VERBOSE_FINEST) {
  //   Rcpp::Rcout << "# Finished ResidueStructure::setBasePair " << (start+1) << " " << (stop+1) << endl;
  //   Rcpp::Rcout << "# Current strand connectivity: " << endl << strandConnectivity << endl;
  //   writeAll(Rcpp::Rcout);
  // }
#ifndef NDEBUG
  if (!validateDistanceConstraintsMatrix(maxDists)) {
    Rcpp::Rcout << "Maximum distances do not validate Final: " << endl;
    Rcpp::Rcout << maxDists << endl;
  }
  if (!validateDistanceConstraintsMatrix(minDists)) {
    Rcpp::Rcout << "Minimum distances do not validate Final: " << endl;
    Rcpp::Rcout << minDists << endl;
  }
#endif
  // Rcpp::Rcout << "Final validation before setBasePairRaw is finished..." << endl;
  if (!validate()) {
    if (verbose >= VERBOSE_FINE) {
      Rcpp::Rcout << "Structure does not validate! Throwing exception..." << endl;
    }
    throw (DISTANCE_CONSTRAINT_EXCEPTION);
  }
THROWIFNOT(validate(),"Assertion violation in L3778");
  } catch (int e) {
    string msg = "Caught exception in setBasePairRaw (1010)" + itos(start+1) + " " + itos(stop+1);
    DEBUG_MSG(msg);
    throw (e);
  }
  // Rcpp::Rcout << "Final validation finished." << endl;
  // DEBUG_MSG("Mark ResidueStructure.cc 1744");
  return connectivityChanged;
}



#endif
