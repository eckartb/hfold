#ifndef BASE_PAIR_STATE_FEATURE
#define BASE_PAIR_STATE_FEATURE

#include "Vec.h"
#include "StateFeature.h"

class BasePairStateFeature : public StateFeature {

 public: 

  typedef string::size_type size_t;

  typedef pair<int , unsigned int> constraint_t;

 private:

  Vec<constraint_t> constraints;
  
 public:

  virtual void addConstraint(const constraint_t& constraint) {
    constraints.push_back(constraint);
  }

  virtual void clear() { constraints.clear(); }

  virtual bool hasFeature(const state_t& state) const {
    for (size_t i = 0; i < constraints.size(); ++i) {
      if (!hasFeature(state, constraints[i])) {
	return false;
      }
    }
    return true;
  }

  virtual bool hasFeature(const state_t& state, const constraint_t& constraint) const {
    return state[constraint.first] == constraint.second;
  }

};

#endif
