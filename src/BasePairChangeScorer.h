#ifndef BASEPAIR_CHANGE_SCORER_H
#define BASEPAIR_CHANGE_SCORER_H

class BasePairChangeScorer : public StateChangeScorer {


  double rateScale;
  double RT;
  double shortestTime;

 public:

  BasePairChangeScorer(double _rateScale, double _RT) : rateScale(_rateScale), RT(_RT), shortestTime(1.0)  { }

  /** Estimated first passage time to go from state1 to state2, assuming that their free energies are stored as score*/
  virtual double operator () (const ScoredPath& state1, const ScoredPath& state2) const {
    double energy1 = state1.first;
    double energy2 = state2.first;
    double time = shortestTime;
    if (energy2 > energy1) {
      time = exp((energy2-energy1)/RT);
    }
    return rateScale * time;
  }

};

#endif
