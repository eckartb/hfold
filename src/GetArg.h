// -*-C++-*-------------------------------------------------------------------
#ifndef GET_ARG_H 
#define GET_ARG_H 

#include <iostream>
#include <string.h>
#include <cstdarg>
#include <string>
#include <vector>
#include "debug.h"
#include "StringTools.h" // convert number to string and and vice versa
// #include <malloc.h>

// "value" has only one "word" as input, set default value (if
// wanted), set gotten path AND default value (if wanted):
void 
getArg( const char *option, string &value, 
	     int argc, char *argv[], string defaultValue = "",
	     bool justOption = false );

/** set flag to true, if option is present */
void 
getArg( const char *option, bool &flag, 
	     int argc, char *argv[]);

void 
getArg( const char *option, int &value, 
	     int argc, char *argv[], int defaultValue = 0 );

void 
getArg( const char *option, unsigned int &value, 
	     int argc, char *argv[], unsigned int defaultValue = 0 );

void 
getArg( const char *option, long &value, 
	     int argc, char *argv[], long defaultValue  = 0);

void 
getArg( const char *option, unsigned long &value, 
	     int argc, char *argv[], unsigned long defaultValue  = 0);

void 
getArg( const char *option, float &value, 
	     int argc, char *argv[], float defaultValue = 0 );

void 
getArg( const char *option, double &value, 
	     int argc, char *argv[], double defaultValue = 0 );

void 
getArg( const char *option, vector<int> &value, 
	     int argc, char *argv[],bool twiceSameNumber = true );

void 
getArg( const char *option, vector<unsigned int> &value, 
	     int argc, char *argv[]);

void 
getArg( const char *option, vector<double> &value, 
	     int argc, char *argv[]);

void 
getArg( const char *option, vector<string> &value, 
	     int argc, char *argv[],
	     bool twiceSameNumber = true );

/** checks commando line wether all options are correct. */
void 
checkArg( int argc, char *argv[], int optionNumber ... );

void 
checkArg( string text, int argc, char *argv[], 
	       int optionNumber ... );

/** converts input file to argc and argv */
// char **
// streamToCommands( istream& is, int& argc, const string& name = "" );

/** tests commando line, if all options are correct. */
bool 
testArg( int argc, char *argv[], int optionNumber ... );

/** returns true, if option is present in command line. */
bool
isPresent( const string& option , int argc, char* argv[]);

/** output of parameter. */
// template<class T>
// inline
// void 
// printArg( const char *option, const T& x )
// {
//   Rcpp::Rcout << "-" << option << " : " << x << endl;
// }

// /** output of parameter. */
// template<class T>
// inline
// void 
// printArg( ostream& os, const char *option, const T& x )
// {
//   os << "-" << option << " : " << x << endl;
// }


/** Evaluates an the command line options as specified in argv.
 * For given option, the found value is set in variable "input".
 */
void 
searchValueOfOption( char option[80], string &input, char *argv )
{
  // if there's an option, get this option:
  if( argv[0] == '-' ) 
    {
      // if the next character is a "-", you know, that the option
      // is a word, otherwise it is one character:
      if( argv[1] == '-' ) 
	{
	  int k = 0;
	  while( argv[k] != '\0' ) {
	    option[k] = argv[k+1]; 
	    ++k;
	  }
	  input = "";
	} // if( argv[1] == '-' )
      else
	{
	  // no option must start with a number
	  if ( ( argv[1] >= '0' ) && (argv[1] <= '9' ) )
	    { 
	      input = argv; 
	    }
	  else
	    { 
	      option[0] = argv[1]; 
	      option[1] = '\0';
	      // if the option contains only one character, check, if
	    // the value is appended directly:
	      if( argv[2] != '\0' )
		{
		  char tempName[80];
		  
		  int k = 1;
		  while( argv[k] != '\0' ) { 
		    tempName[k-1] = argv[k+1]; 
		    ++k;
		  }
		input = tempName;
		}
	      else
		{ input = ""; }
	    } // else (if(argv[1]=='-') ) 
	} // if( argv[0] == '-' )
    }  
  else
    { input = argv; }
}

/** interpret command line option:  value = string: */
void 
getArg( const char *givenOption, string &value, 
	int argc, char *argv[], string defaultValue,
	bool justOption )
{
  int countOption = 0, countValue = 0;
  string input;
  char option[80];

  option[0] = '\0';
  for( int i = 1; i < argc; i++ )
    {
      searchValueOfOption( option, input, argv[i] );
      // if there's an option, get this option:
      if ( argv[i][0] == '-' ) 
	{

	  // check if "option" is negative number
	  if ( (argv[i][1]<'0') || (argv[i][1]>'9') )
	    {
	      if( strcmp(givenOption, option) == 0 )
		{ 
		  countOption++; 
		}
	    }
 	} // if( argv[i][0] == '-' )
      // now interpret the input:
      if( input != "" )
	{
           if( strcmp(givenOption, option) == 0 )
		{ 
		  countValue++;
		  value = input;
		}
	} // if( input != "" )
    } // for( i < argc )

  // if an option is selected several times:
  if( countOption > 1 )
    {
      Rcpp::Rcerr << "You have given too often the option \"" 
	   << givenOption << "\"" << endl;
      DERROR( "Too many options" ); 
    }
  if( countValue > 1 )
    {
      Rcpp::Rcerr << "You have given too many input of the option \"" 
	   << givenOption << "\"" << endl;
      DERROR( "Too many options" ); 
    }
  if( justOption == false )
    { 
      if( (countOption==1) && (countValue==0) )
	{
	  Rcpp::Rcerr << "You have chosen the option \"" << givenOption 
	       << "\" but havn't specified the value!" << endl;
	  DERROR( "Missing value" ); 
	} // if( (countOption==1) && (countValue==0) &&)
      // check, if value is defined:
      if( value == "" )
	{ value = defaultValue; }
    } // if( justOption == false )
  else
    {
      if( (countOption==1) && (countValue==0) && (value=="") )
	{ value = defaultValue; }
      if( (countOption==0) && (countValue==0) )
	{ value = ""; }
    } // else (if(justOption==false))
}

/** interpret command line option:  value = integer: */
void 
getArg( const char *givenOption, int &value, 
	     int argc, char *argv[], int defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );

  if( tempInt != "" )
    { value = Stoi( tempInt ); }
}

/** 
  If givenOption is present, flag will be set to true, false otherwise
  */
void
getArg( const char *givenOption, bool& flag,
	int argc, char *argv[] )
{
  string value;
  string isThere = "OptionPresent";
  getArg(givenOption, value, argc, argv,isThere, true);
  if (value == isThere)
    {
      flag = true;
    }
  else
    {
      flag = false;
    }
}

/** interpret command line option:  value = unsigned integer: */
void 
getArg( const char *givenOption, unsigned int &value, 
	     int argc, char *argv[], unsigned int defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );
  if( tempInt != "" )
    { value = Stoui( tempInt ); }
}


/** interpret command line option:  value = long integer */
void 
getArg( const char *givenOption, long &value, 
	     int argc, char *argv[], long defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );

  if( tempInt != "" )
    { value = Stol( tempInt ); }
}

/** interpret command line option:  value = long integer */
void 
getArg( const char *givenOption, unsigned long &value, 
	     int argc, char *argv[], unsigned long defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );

  if( tempInt != "" )
    { value = Stoul( tempInt ); }
}

/** interpret command line option: value = float: */
void 
getArg( const char *givenOption, float &value, 
	     int argc, char *argv[], float defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );
  
  if( tempInt != "" )
    { value = Stof( tempInt ); }
}

/** interpret command line option:  value = double: */
void 
getArg( const char *givenOption, double &value, 
	     int argc, char *argv[], double defaultValue )
{
  string tempInt;
  
  value = defaultValue;
  getArg( givenOption, tempInt, argc, argv );
  if( tempInt != "" )
    { value = stod( tempInt ); }
}


/** interpret command line option:  value = vector of integer: */
void 
getArg( const char *givenOption, vector<int> &value, 
	     int argc, char *argv[], bool twiceSameNumber )
{
  int countOption = 0;
  string input;
  char option[80];

  option[0] = '\0';
  for( int i = 1; i < argc; i++ )
    {
      searchValueOfOption( option, input, argv[i] );
      // if there's an option, get this option:
      if( argv[i][0] == '-' ) 
	{
	  if( strcmp(givenOption, option) == 0 )
	    { countOption++; }
 	} // if( argv[i][0] == '-' )
      // now interpret the input:
      if( input != "" )
	{
	  if( twiceSameNumber == true )
	    {
	      // take each number:
	      if( strcmp(givenOption, option) == 0 )
		{ 
		   value.push_back( Stoi( input ) ); 
		}
	    } // if( twiceSameNumber == true )
	  else
	    {
	      // take each number, if not yet taken:
	      if( strcmp(givenOption, option) == 0 )
		{ 
		  bool alreadyAdded = false;
		  for( unsigned int i = 0; i < value.size(); i++ )
		    {
                      if( Stoi( input ) == value[i] )
			{
			  alreadyAdded = true;
			  break;
			}
		    }
		  if( alreadyAdded == false )
		    { 
		      value.push_back( Stoi( input ) ); 
		    }
		} // if( strcmp(givenOption, option) == 0 )
	    } // else ( if(twiceSameNumber==true) )
	} // if( input != "" )
    } // for( i < argc )
  // if an option is selected but not specified:
  if( (countOption>0) && (value.size()==0) )
    {
      Rcpp::Rcerr << "You have chosen the option \"" 
	   << givenOption << "\" but haven't specified the value!" << endl;
      DERROR( "Missing value(s)" ); 
    }
}

/** interpret command line option:  value = vector of unsigned integer: */
void 
getArg( const char *givenOption, vector<unsigned int> &value, 
	     int argc, char *argv[] )
{
  int countOption = 0;
  string input;
  char option[80];

  option[0] = '\0';
  for( int i = 1; i < argc; i++ )
    {
      searchValueOfOption( option, input, argv[i] );
      // if there's an option, get this option:
      if( argv[i][0] == '-' ) 
	{
	  if( strcmp(givenOption, option) == 0 )
	    { 
	      countOption++; 
	    }
 	} // if( argv[i][0] == '-' )
      // now interpret the input:
      if( input != "" )
	{
	  // take each number:
	  if( strcmp(givenOption, option) == 0 )
	    { 
	      value.push_back( Stoui( input ) ); 
	    }
	} // if( input != "" )
    } // for( i < argc )
  // if an option is selected but not specified:
  if( (countOption>0) && (value.size()==0) )
    {
      Rcpp::Rcerr << "You have chosen the option \"" 
	   << givenOption << "\" but haven't specified the value!" << endl;
      DERROR( "Missing value(s)" ); 
    }
}

/** interprets command line option:  value = vector of double: */
void 
getArg( const char *givenOption, vector<double> &value, 
	     int argc, char *argv[] )
{
  int countOption = 0;
  string input;
  char option[80];

  option[0] = '\0';
  for( int i = 1; i < argc; i++ )
    {
      searchValueOfOption( option, input, argv[i] );
      // if there's an option, get this option:
      if( argv[i][0] == '-' ) 
	{
	  if( strcmp(givenOption, option) == 0 )
	    { 
	      countOption++; 
	    }
 	} // if( argv[i][0] == '-' )
      // now interpret the input:
      if( input != "" )
	{
	  // take each number:
	  if( strcmp(givenOption, option) == 0 )
	    { 
	      value.push_back( stod( input ) ); 
	    }
	} // if( input != "" )
    } // for( i < argc )
  // if an option is selected but not specified:
  if( (countOption>0) && (value.size()==0) )
    {
      Rcpp::Rcerr << "You have chosen the option \"" 
	   << givenOption << "\" but haven't specified the value!" << endl;
      DERROR( "Missing value(s)" ); 
    }
}

/** interprets command line option:  value = vector of strings: */
void
getArg( const char *givenOption, vector<string> &value, 
	     int argc, char *argv[], bool twiceSameNumber )
{
  int countOption = 0;
  string input;
  char option[80];

  option[0] = '\0';
  for( int i = 1; i < argc; i++ )
    {
      searchValueOfOption( option, input, argv[i] );
      // if there's an option, get this option:
      if( argv[i][0] == '-' ) 
	{
	  if( strcmp(givenOption, option) == 0 )
	    { countOption++; }
	  else
	    { continue; }
 	} // if( argv[i][0] == '-' )
      // now interpret the input:
      if( input != "" )
	{
	  if( twiceSameNumber == true )
	    {
	      // take each number:
	      if( strcmp(givenOption, option) == 0 )
		{ value.push_back( input ); }
	      else
		{ continue; }
	    } // if( twiceSameNumber == true )
	  else
	    {
	      // take each number, if not yet taken:
	      if( strcmp(givenOption, option) == 0 )
		{ 
		  bool alreadyAdded = false;
		  for( unsigned int i = 0; i < value.size(); i++ )
		    {
		      if( input.compare( value[i] ) == 0 )
			{
			  alreadyAdded = true;
			  break;
			}
		    }
		  if( alreadyAdded == false )
		    { value.push_back( input ); }
		} // if( strcmp(givenOption, option) == 0 )
	      else
		{ continue; }
	    } // else ( if(twiceSameNumber==true) )
	} // if( input != "" )
    } // for( i < argc )
  // if an option is selected but not specified:
  if( (countOption>0) && (value.size()==0) )
    {
      Rcpp::Rcerr << "You have chosen the option \"" 
	   << givenOption << "\" but haven't specified the value!" << endl;
      DERROR( "Missing value(s)" ); 
    }
  return;
}

/**
* checks if there are chosen wrong options, if an
* unknown option is chosen, then error, if you give a text, then show
* this text.
*/
void 
checkArg( int argc, char *argv[], int optionNumber ... )
{
  bool rightOption = false;  
  char option[80];
  string str;
  va_list listOfOptions;

  option[0] = '\0';
  if( argc <= 1 )
    { return; }
  // check, if the first input is an option:
  if( argv[1][0] !=  '-' ) 
    {
      DERROR( "First input isn't an option" ); 
    }
  for( int i = 1; i < argc; i++ )
     {
       rightOption = false;
       searchValueOfOption( option, str, argv[i] );
       // define the listOfOptions:
       va_start( listOfOptions, optionNumber );
       // every given Options is a *char, get all givenOptions:
       for( int i = 0; i < optionNumber; i++ )
	 {
	   char *givenOption = va_arg( listOfOptions, char* );
	   if( givenOption == 0 )
	     { break; }
	   if( strcmp(givenOption, option) == 0 )
	     { 
	       rightOption = true;
	       break; // go to the next option
	     }	   
	 } // for( i < optionNumber )
       // end of the list:
       va_end( listOfOptions );
       if( rightOption == false )
	 {
	   Rcpp::Rcerr << "\"" << option << "\" is a wrong option" << endl;
	   DERROR( "Wrong option" ); 	   
	 } // if( rightOption == false )
     } // for( i < argc )
}

/** show text: */
void 
checkArg( string text, int argc, char *argv[], 
	       int optionNumber ... )
{
  bool rightOption = false;  
  char option[80];
  string str;
  va_list listOfOptions;

  option[0] = '\0';
  if( argc <= 1 )
    { return; }
  // check, if the first input is an option:
  if( argv[1][0] !=  '-' ) 
    {
      Rcpp::Rcerr << text << endl;
      DERROR( "First input isn't an option" ); 
    }
  for( int i = 1; i < argc; i++ )
     {
       rightOption = false;
       searchValueOfOption( option, str, argv[i] );
       // define the listOfOptions:
       va_start( listOfOptions, optionNumber );
       // every given Options is a *char, get all givenOptions:
       for( int i = 0; i < optionNumber; i++ )
	 {
	   char *givenOption = va_arg( listOfOptions, char* );
	   if( givenOption == 0 )
	     { break; }
	   if( strcmp(givenOption, option) == 0 )
	     { 
	       rightOption = true;
	       break; // go to the next option
	     }	   
	 } // for( i < optionNumber )
       // end of the list:
       va_end( listOfOptions );
       if( rightOption == false )
	 {
	   Rcpp::Rcerr << "\"" << option << "\" is a wrong option" << endl
		<< text << endl;
	   DERROR( "Wrong option" ); 	   
	 } // if( rightOption == false )
     } // for( i < argc )
}

/*
* checks if there are chosen wrong options, if an
* unknown option is chosen, then return false otherwise true
*
*/
bool 
testArg( int argc, char *argv[], int optionNumber ... )
{
  bool rightOption = false;  
  char option[80];
  string str;
  va_list listOfOptions;

  option[0] = '\0';
  if( argc <= 1 )
    { return true; }
  // check, if the first input is an option:
  if( argv[1][0] !=  '-' ) 
    {
      Rcpp::Rcout << "First parameter must begin with \"-\" " << endl;
      return false; 
    }
  for( int i = 1; i < argc; i++ )
     {
       rightOption = false;
       searchValueOfOption( option, str, argv[i] );
       // define the listOfOptions:
       va_start( listOfOptions, optionNumber );
       // every given Options is a *char, get all givenOptions:
       for( int i = 0; i < optionNumber; i++ )
	 {
	   char *givenOption = va_arg( listOfOptions, char* );
	   if( givenOption == 0 )
	     { break; }
	   if( strcmp(givenOption, option) == 0 )
	     { 
	       rightOption = true;
	       break; // go to the next option
	     }	   
	 } // for( i < optionNumber )
       // end of the list:
       va_end( listOfOptions );
       if( rightOption == false )
	 { 
	   Rcpp::Rcout << "\"" << option << "\" is a wrong option" << endl;
	   return false;
	 } // if( rightOption == false )
     } // for( i < argc )
  return true;
}

/** return true, if option is present in command line */
bool 
isPresent(const string& origOption, int argc, char** argv)
{
  ERROR_IF(origOption=="", "Empty string for option given.");
  string option = origOption;
  option = "-" + option;
  for (int i = 1; i < argc; i++)
    {
      if (option.compare(argv[i])==0)
	{
	  return true;
	}
    }

  return false; // option not found
}

/** read stream, return parameters like argc and argv */
/*
char **
streamToCommands( istream& is, int& argc, const string& startString)
{
  // get vector of words
  string line;
  vector<string> v;
  vector<string> words;
  if (startString.size() > 0) {
    v.push_back(startString);
  }
  while (is) {
    line = getLine(is);
    words = getTokens(line);
    for (unsigned int i = 0; i < words.size(); ++i) {
      if (words[i][0] == '#') {
	break;
      }
      v.push_back(words[i]);
    }
  }
  argc = v.size();
  char **argv = 0;
  if (argc == 0) {
    return argv; // return NULL if no commands found
  }
  argv = (char **) malloc(argc * sizeof(char *));
  for (unsigned int i = 0; i < v.size(); ++i) {
    argv[i] = (char *) malloc((v[i].size() + 1) * sizeof(char) );
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      argv[i][j] = v[i][j];
    }
    argv[i][v[i].size()] = '\0';
  }
  return argv;
}
*/

#endif // __GET_ARG_H__










