#ifndef _PARTITION_FUNCTION_
#define _PARTITION_FUNCTION_

#include <string>
#include "Vec.h"
#include "vectornumerics.h"


#define PARTITION_EXPONENT_MAX 20

class PartitionFunction {

 public:
  
  typedef string::size_type size_type;

  enum { PARTITION_FLAG_YES=1,  PARTITION_FLAG_NO=0,  PARTITION_FLAG_DONTCARE=-1 };
  
 private:

  bool baked;

  const ResiduePairingClassifier * classifier;

  Vec<int> classifierFlags;

  double classifierPropensity; // un-normalized probability (becomes normalized by dividing by partition function)

  Vec<double> energies; // energies of added states

  Vec<double> logTerms; // terms representing n(i) exp(-E(i)/RT) ( in other words product of large and small term

  Vec<string> flagNames;

  Vec<Vec<int> > flags;

  Vec<Vec<double> > probs;

  double partitionFunctionValue;

  double redox;

  double RT;

  int unfoldedId; // id of completely unfolded state

  ResiduePairing referenceStructure;

 public:

 PartitionFunction() : baked(false), classifier(NULL), classifierPropensity(0.0), partitionFunctionValue(0.0), redox(0.0), RT(RT_37C), unfoldedId(-1.0) {  }

 PartitionFunction(const Vec<string>& _flagNames, double _RT) : baked(false), classifier(NULL), classifierPropensity(0.0), flagNames(_flagNames), partitionFunctionValue(0.0), redox(0.0), RT(_RT), unfoldedId(-1) { }
  
  const PartitionFunction& operator=(const PartitionFunction& other) {
    if (this != &other) {
      copy(other);
    }
    return *this;
  }

  void add(double energy,  double logMultiplicity, const Vec<int>& currentFlags, const ResiduePairing& pairing) {
    PRECOND(!baked);
    // if (classifier != NULL) {
    // Rcpp::Rcout << "Starting PartitionFunction.add: " << classifierFlags.size() << " " << logTerms.size() << endl;
    // }
    PRECOND(classifier == NULL || classifierFlags.size() == logTerms.size());
    PRECOND(currentFlags.size() == flagNames.size());
THROWIFNOT(isReasonable(logMultiplicity),"Assertion violation in L69");
THROWIFNOT(isReasonable(energy),"Assertion violation in L70");
THROWIFNOT(isReasonable(RT),"Assertion violation in L71");
THROWIFNOT(RT > 0.0,"Assertion violation in L72");
    PRECOND(validate());
    double logTerm = logMultiplicity - (energy / RT); // log of n(i) exp(-E(i)/RT)
    // logMult = logTerm + energy/RT 
THROWIFNOT(isReasonable(logTerm),"Assertion violation in L76");
    if (pairing.countBasePairs() == 0) {
      unfoldedId = size(); // special case of completely unfolded state
    }
    logTerms.push_back(logTerm);
    energies.push_back(energy);
    flags.push_back(currentFlags);
    if (classifier != NULL) {
      classifierFlags.push_back(classifier->classify(pairing));
    }
THROWIFNOT(classifier == NULL || classifierFlags.size() == logTerms.size(),"Assertion violation in L86");
    ERROR_IF(redox < 0.0, "internal error L61");
    if ((logTerm - redox) > PARTITION_EXPONENT_MAX) {
      double redoxOld= redox;
      redox = logTerm; // 
      double redoxDiff = redox - redoxOld;
      // Rcpp::Rcout << "# Adjusting to new redox term: " << redox << " before: " << redoxOld << endl;
THROWIFNOT(redoxDiff > 0.0,"Assertion violation in L93");
      for (size_t i = 0; i < logTerms.size(); ++i) {
	// logTerms[i] -= redoxDiff;
	for (size_t j = 0; j < probs.size(); ++j) {
	  for (size_t k = 0; k < probs.size(); ++k) {
	    double a = log(probs[j][k]);
	    a -= redoxDiff;
	    probs[j][k] = exp(a);
	  }
	}
      }
    }
    double prob = exp(logTerm - redox); //  MAX_REASONABLE/10.0;
    //    if ((logTerm - redox) < log(MAX_REASONABLE)) {
    //   prob = exp(logTerm-redox);
    // } else {
#ifndef NDEBUG
    //  Rcpp::Rcout << "Warning: cutoff too large probabilities: log multiplicity: " << logMultiplicity << " -energy/RT " << (-energy/RT) << " log-term: " << logTerm << " " << (logTerm-redox) << " " << prob << endl;
#endif
    // }
    ERROR_IF(!isReasonable(prob), "Ill-defined probability value encountered.");
    for (size_t i = 0; i < pairing.size(); ++i) {
      if (pairing.isBasePaired(i)) {
	probs[i][pairing[i]] += prob;
      }
    }
  }

  void bake() {
    if (baked) {
      return;
    }
    // Rcpp::Rcout << "probs before bake: " << endl << probs << endl;
    size_t n = logTerms.size();
    // find largest term:
    size_t maxId = maxElement(logTerms);
    double maxVal = logTerms[maxId];
    double prob = exp(maxVal);
    classifierPropensity = 0.0;
    partitionFunctionValue = 0.0;
    // Rcpp::Rcout << "Best found structure found - renormalizing: " << maxId << " " << maxVal << " flags: " << flags[maxId] <<  endl;
    // Rcpp::Rcout << "probabilities before bake: " << endl << probs << endl;
    for (size_t i = 0; i < n; ++i) {
THROWIFNOT(i < logTerms.size(),"Assertion violation in L136");
      if (logTerms[i] > maxVal) {
	Rcpp::Rcout << "Strange: " << logTerms[i] << " " << maxVal << endl;
      }
      logTerms[i] -= maxVal; // equivalent of renormalizing energies
      if (logTerms[i] > 0) {
	Rcpp::Rcout << "Strange2: " << logTerms[i] << " " << maxVal << endl;
      }
      // ASSERT(logTerms[i] <= 0.0);
THROWIFNOT(i < logTerms.size(),"Assertion violation in L145");
      partitionFunctionValue += exp(logTerms[i]);
      // ERROR_IF(!isReasonable(prob), "Ill-defined probability value encountered (2).");
      if (classifier != NULL) {
THROWIFNOT(i < classifierFlags.size(),"Assertion violation in L149");
	if (classifierFlags[i]) {
	  classifierPropensity += exp(logTerms[i]);
	}
      }
    }
    if (partitionFunctionValue > 0.0) {
      classifierPropensity /= partitionFunctionValue;
    }
    // Rcpp::Rcout << "Partition function value: " << partitionFunctionValue << " max prob: " << prob << endl;
    for (size_t i = 0; i < probs.size(); ++i) {
      for (size_t j = 0; j < probs.size(); ++j) {
	probs[i][j] /= partitionFunctionValue;
	probs[i][j] *= exp(redox);
	probs[i][j] /= prob;
THROWIFNOT(probs[i][j] < 1.1,"Assertion violation in L164");
	// Rcpp::Rcout << (i+1) << " " << (j+1) << " " << probs[i][j] << endl;
	if (probs[i][j] > 1.0) {
	  probs[i][j] = 1.0;
	  probs[j][i] = 1.0;
	}

THROWIFNOT(probs[i][j] >= 0.0,"Assertion violation in L171");
      }
    }
    baked = true;
    // Rcpp::Rcout << "probs after  bake: " << endl << probs << endl;
    Rcpp::Rcout << "# Finished bake." << endl;
    // ASSERT(false);
  }

  /** Remove energy terms. The definitions (number of flags and temperature) is not changed, however. */
  void clear() {
    logTerms.clear();
    flags.clear();
    baked = false;
    partitionFunctionValue = 0.0;
    for (size_t i = 0; i < probs.size(); ++i) {
      for (size_t j = 0; j < probs.size(); ++j) {
	probs[i][j] = 0.0;
      }
    }
  }
  
  void copy(const PartitionFunction& other) {
    classifier = other.classifier;
    classifierFlags = other.classifierFlags;
    logTerms = other.logTerms;
    flagNames = other.flagNames;
    flags = other.flags;
    RT = other.RT;
    baked = other.baked;
    probs = other.probs;
    partitionFunctionValue = other.partitionFunctionValue;    
    redox = other.redox;
    unfoldedId = other.unfoldedId;
  }

  double getClassifierProbability() const {
    // Rcpp::Rcout << "# classifier flags: " << classifierFlags << endl;
    return classifierPropensity;
  }

  const Vec<string>& getFlagNames() const { return flagNames; }

  const Vec<Vec<double> > getProbs() const { return probs; }

  double getRT() const { return RT; }

  static Vec<string> createNames(size_t strandCount)  {
    Vec<string> result;
    for (size_t i = 0; i < strandCount; ++i) {
      for (size_t j = i; j < strandCount; ++j) {
	result.push_back(uitos(i+1) + "_" + uitos(j+1));
      }
    }
THROWIFNOT(result.size() > 0,"Assertion violation in L225");
    return result;
  }

  string createInteractionString(size_t strandCount, 
				 const Vec<int>& interactions,
				 map<size_t, string>& synonyms) {
    PRECOND(interactions.size() == ((strandCount+1)*(strandCount)/2));
    int pc = 0;
    // string result = "";
    // set<int> foundStrands;
    Vec<Vec<int> > strandConnectivity(strandCount, Vec<int>(strandCount, PARTITION_FLAG_NO));
    for (size_t i = 0; i < strandCount; ++i) {
      for (size_t j = i; j < strandCount; ++j) {
	if (interactions[pc++] == PARTITION_FLAG_YES) {
	  strandConnectivity[i][j] = PARTITION_FLAG_YES;
	  strandConnectivity[j][i] = PARTITION_FLAG_YES;
	  /* if (result.size() > 0) { */
	  /*   result = result + "."; */
	  /* } */
	  // foundStrands.insert(i);
	  // foundStrands.insert(j);
	  // result = result + uitos(i+1) + "_" + uitos(j+1);
	}
      }
    }
    // add isolated strands:
    /* for (size_t i = 0; i < strandCount; ++i) { */
    /*   if (foundStrands.find(i) == foundStrands.end()) { */
    /* 	result = result + "|" + uitos(i+1); */
    /*   } */
    /* } */
THROWIFNOT(pc == static_cast<int>(interactions.size()),"Assertion violation in L257");
    Vec<Vec<int> > strandIndirectConnectivity = ConnectivityTools::connectivityToIndirectConnectivity(strandConnectivity);
    Vec<string> componentHashes;
    if (synonyms.size() > 0) {
      componentHashes = ConnectivityTools::connectivityToHashes(strandIndirectConnectivity,synonyms); // TODO improve efficiency of implementation
    } else {
      componentHashes = ConnectivityTools::connectivityToHashes(strandIndirectConnectivity); // TODO improve efficiency of implementation
    }
THROWIFNOT(componentHashes.size() > 0,"Assertion violation in L265");
    string result2 = componentHashes[0];
    for (size_t i = 1; i < componentHashes.size(); ++i) {
      result2 += "|" + componentHashes[i];
    }
    return result2;
  }

  string createInteractionString(size_t strandCount, 
				 const Vec<int>& interactions) {
    map<size_t, string> emptySynonyms;
    return createInteractionString(strandCount, interactions, emptySynonyms);
  }
  /** a given set of strand ids corresponding to a complex, return flag pattern that can be used by method computeProbability */
  Vec<int> createComplexFlagPattern(size_t strandCount, const set<int>& strandIds) const {
    const Vec<string>& flagNames = getFlagNames();
    Vec<int> flags(flagNames.size(), PARTITION_FLAG_DONTCARE);
THROWIFNOT(flags.size() == flagNames.size(),"Assertion violation in L282");
    size_t pc = 0;
    for (size_t i = 0; i < strandCount; ++i) {
      bool found1 = (strandIds.find(i) != strandIds.end());
      for (size_t j = i; j < strandCount; ++j) {
	bool found2 = (strandIds.find(j) != strandIds.end());
	if (i == j) {
	  flags[pc] = PARTITION_FLAG_DONTCARE;
	} else if (found1 && found2) {
	  flags[pc] = PARTITION_FLAG_YES;
	} else if (found1 || found2) {
	  flags[pc] = PARTITION_FLAG_NO;
	} else {
THROWIFNOT(!found1,"Assertion violation in L295");
THROWIFNOT(!found2,"Assertion violation in L296");
	  flags[pc] = PARTITION_FLAG_DONTCARE;
	}
	pc++;
      }
    }
THROWIFNOT(pc == flagNames.size(),"Assertion violation in L302");
    return flags;
  }

  /** a given set of strand ids corresponding to a complex, return flag pattern that can be used by method computeProbability */
  string flagPatternToString(size_t strandCount, const Vec<int>& flags) const {
    const Vec<string>& flagNames = getFlagNames();
THROWIFNOT(flags.size() == flagNames.size(),"Assertion violation in L309");
    size_t pc = 0;
    string result;
    for (size_t i = 0; i < strandCount; ++i) {
      for (size_t j = i; j < strandCount; ++j) {
	if (result.size() > 0) {
	  result = result + ".";
	}
	result = result + flagNames[pc];
	switch (flags[pc]) {
	case PARTITION_FLAG_DONTCARE: 
	  result = result + "?";
	  break;
	case PARTITION_FLAG_YES:
	  result = result + "+";
	  break;
	case PARTITION_FLAG_NO:
	  result = result + "-";
	  break;
	default:
	  DERROR("Unknown flag id.");
	} 
	pc++;
      }
    }
//   ASSERT(pc == flagNames.size());
    return result;
  }


  double computeProbability(const Vec<int>& flagsPattern) const {
    ERROR_IF(!baked, "Internal error: partitition function not in correct status for computing probability.");
    PRECOND(flags.size() == logTerms.size());
    double result = 0.0;
    size_t n = size();
    for (size_t i = 0; i < n; ++i) {
THROWIFNOT(i < flags.size(),"Assertion violation in L345");
      if (flagsMatching(flags[i], flagsPattern)) {
	result += exp(logTerms[i]);
	ERROR_IF(!isReasonable(result), "Ill-defined probability value encountered (3).");
      }
    }
    return result / partitionFunctionValue;
  }

  /** Returns probability of special case of completely unfolded state */
  double computeUnfoldedProbability() const {
    if (unfoldedId < 0) {
      return NAN;
    }
    double logResult = logTerms[unfoldedId];
    logResult -= log(partitionFunctionValue);// divided by partition function
    double result = exp(logResult);
    ERROR_IF(!isReasonable(result), "Ill-defined probability value encountered (3).");
    return result;
  }


  Vec<size_t> findMatchingStates(const Vec<int>& flagsPattern) const {
    ERROR_IF(!baked, "Internal error: partitition function not in correct status for computing probability.");
    PRECOND(flags.size() == logTerms.size());
    size_t n = size();
    Vec<size_t> result;
    for (size_t i = 0; i < n; ++i) {
THROWIFNOT(i < flags.size(),"Assertion violation in L373");
      if (flagsMatching(flags[i], flagsPattern)) {
	result.push_back(i);
      }
    }
    return result;
  }

  bool flagsMatching(const Vec<int>& refFlags, const Vec<int>& searchFlags) const {
    PRECOND(refFlags.size() == searchFlags.size());
    size_t n = flagNames.size();
    for (size_t i = 0; i < n; ++i) {
      if ((searchFlags[i] != PARTITION_FLAG_DONTCARE) && (refFlags[i] != searchFlags[i])) {
	return false;
      }
    }
    return true;
  }

  void init(const Vec<string>& _flagNames, double _RT, size_t residueCount) {
    PRECOND(_flagNames.size() > 0);
    PRECOND(_RT > 0.0);
    flagNames = _flagNames;
    RT = _RT;
    baked = false;
    partitionFunctionValue = 0.0;
    probs = Vec<Vec<double> >(residueCount, Vec<double>(residueCount, 0.0));
THROWIFNOT(validate(),"Assertion violation in L400");
  }

  void merge(const PartitionFunction& other) {
    Rcpp::Rcout << "Starting merge!" << endl;
    ERROR_IF(flagNames != other.flagNames, "Flag names are not matching in merge operation!");
    ERROR_IF(baked || other.baked, "Merge operation is not possible if partition functions are finalized.");
    for (size_t i = 0; i < other.size(); ++i) {
      logTerms.push_back(other.logTerms[i]);
      flags.push_back(other.flags[i]);
    }
    partitionFunctionValue += other.partitionFunctionValue;
    Rcpp::Rcout << "Finished merge!" << endl;
  }
  
  void setClassifier(const ResiduePairingClassifier * _classifier) {
    classifier = _classifier;
  }

  void setRedox(double value) {
    redox = value;
  }

  size_t size() const {
    return logTerms.size();
  }

  bool validate() {
    return (RT > 0) && (flagNames.size() > 0.0) && ((classifier == NULL) || (classifierFlags.size() == logTerms.size()));;
  }

  void write(ostream& os) {
    os << "Flag names: " << flagNames << endl;
    os << "ID Energy logMultiplicity LogTerm Prob" << endl;
    for (size_t i = 0; i < logTerms.size(); ++i) {
      os << (i+1) << " " << energies[i] << " " << (logTerms[i] + energies[i]/RT) << " " << logTerms[i] << " " << exp(logTerms[i]) << " ";
      os << flags[i] << endl;
    }
  }

};

#endif
