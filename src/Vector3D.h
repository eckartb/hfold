// --*- C++ -*------x---------------------------------------------------------
//
// Class:           Vector3D
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald, Matthias Heiler
//
// Description:     Vector in 3D space.
// 
// -----------------x-------------------x-------------------x-----------------

#ifndef __VECTOR3D_H__
#define __VECTOR3D_H__

// Includes 

#include <math.h>

#include <libintl.h>
#include <iostream>
#include <string>

#include "debug.h"
#include "Limits.h"

using namespace std;

// workaround because of changed math.h headed : EB 02/99
#ifndef PI
#define PI 3.14159265358979323846	/* pi */
#endif

// 180.0/PI:
#ifndef RAD2DEG
#define RAD2DEG  57.29577951308233 
#endif

// PI/180.0:
#ifndef DEG2RAD
#define DEG2RAD  0.01745329251994
#endif

// maximum plausible component size
const double V3D_PLAUSIBLE = 100000.0;

// class Vector4D;
// class Matrix3D;
// class Vector3D;
// Matrix3D rotationMatrix(Vector3D axis, double angle);

/** Vector in 3D space.

  @see    @Gamma et al: "Design Patterns", Bridge-Pattern */
class Vector3D {
  // friend class Vector4D;
  // friend class Matrix3D;
  // friend Matrix3D rotationMatrix(Vector3D axis, double angle);
  // friend Matrix3D scaleMatrix(const Vector3D& s);

protected:
  /* COORDINATES */
  double _x;
  double _y;
  double _z;
  
public:

  Vector3D(double __x=0.0, double __y=0.0, double __z=0.0) {
    _x = __x;
    _y = __y;
    _z = __z;
  }

  Vector3D(const Vector3D& orig)  { copy(orig); }

  Vector3D& operator = (const Vector3D& orig) { 
    if (&orig != this) {
      copy(orig);
      }
    return *this; 
  }
  
  virtual ~Vector3D() { }
  
  /* Selectors */
  
  /** What is x-coordinate? */
   double x() const { return _x; }
  /** What is y-coordinate? */
   double y() const { return _y; }
  /** What is z-coordinate? */
   double z() const { return _z; }

  /** returns distance between two vectors */
  double distance(const Vector3D& other) const { 
    return ((*this)-other).length();
  }

  /** How long is vector? */
   double length() const { 
    return sqrt(lengthSquare());
  }

   double lengthSquare() const { 
    return ((_x*_x) + (_y*_y) + (_z*_z)); 
  }
  
   bool operator == (const Vector3D& other) const { 
    return ((_x==other._x) && (_y==other._y) && (_z==other._z));
  }

   bool operator != (const Vector3D& other) const { 
    return (!(*this == other)); 
  }

   bool operator <  (const Vector3D& other) const{ 
    return lengthSquare() <  other.lengthSquare(); 
  }

   bool operator <= (const Vector3D& other) const { 
    return lengthSquare() <= other.lengthSquare(); 
  }

   bool operator >  (const Vector3D& other) const { 
    return lengthSquare() >  other.lengthSquare(); 
  }

   bool operator >= (const Vector3D& other) const { 
    return lengthSquare() >= other.lengthSquare(); 
  }
  
   bool operator <  (double value) const { 
    return lengthSquare() <  (value*value); 
  }
  
   bool operator <= (double value) const { 
    return lengthSquare() <= (value*value); 
  }
  
   bool operator >  (double value) const { 
    return lengthSquare() >  (value*value); 
  }
  
   bool operator >= (double value) const { 
    return lengthSquare() >= (value*value); 
  }

  /* Modifiers */

  /** copy method, used by copy constructor and assignment operator */
   void copy(const Vector3D& other) {
    _x = other._x;
    _y = other._y;
    _z = other._z;
  }

  /** Set x-coordinate. */
   void x(double d) {
    PRECOND(fabs(d) < V3D_PLAUSIBLE);  _x = d; 
  }

  /** Set y-coordinate. */
   void y(double d) {
    PRECOND(fabs(d) < V3D_PLAUSIBLE); _y = d; 
  }

  /** Set z-coordinate. */
   void z(double d) {
    PRECOND(fabs(d) < V3D_PLAUSIBLE); _z = d; 
  }

   void set(double __x, double __y, double __z) { _x = __x; _y = __y; _z = __z; }

  /** normalize vector according to Euclidian norm */
   void normalize() { 
    double f = length();
    if (f > 0.0) {  
      f = 1.0 / f;
      _x *= f;
      _y *= f;
      _z *= f;
      // (*this) = (*this) * (1.0 / l); 
    }
THROWIFNOT(isSimilar(length(), 1.0),"Assertion violation in L187");
  }

   Vector3D operator + (const Vector3D& other) const { 
    return Vector3D(_x + other._x, _y + other._y, _z + other._z); 
  }
  
   Vector3D operator + (double other) const { 
    return Vector3D(_x + other, _y + other, _z + other); }

   Vector3D operator - (double other) const { 
    return Vector3D(_x - other, _y - other, _z - other); }

   Vector3D operator - (const Vector3D& other) const {
    return Vector3D(_x - other._x, _y - other._y, _z - other._z); 
  }

   Vector3D operator * (double other) const { 
    return Vector3D(_x * other, _y * other, _z * other); 
  }

   double   operator * (const Vector3D& other) const { 
    return _x*other._x + _y*other._y + _z*other._z;
  }

   Vector3D  operator / (double other) const {
    return Vector3D(_x / other, _y / other, _z / other); 
  }

   Vector3D& operator += (const Vector3D& other) {
    _x += other._x;
    _y += other._y;
    _z += other._z;
    return (*this);
  }

   Vector3D& operator -= (const Vector3D& other) {
    _x -= other._x;
    _y -= other._y;
    _z -= other._z;
    return (*this);
  }

   Vector3D& operator *= (double other) {
    _x *= other;
    _y *= other;
    _z *= other;
    return (*this);
  }

   Vector3D& operator /= (double other) {
    _x /= other;
    _y /= other;
    _z /= other;
    return (*this);
  }

   Vector3D operator - () const { 
    return Vector3D(-_x, -_y, -_z); 
  }

  /* Friends */

  friend Vector3D cross(const Vector3D& v1, const Vector3D& v2);
  // friend Vector3D rotate(const Vector3D& v, const Vector3D& center, 
  // double angle);
  friend ostream& operator << (ostream& os, const Vector3D& p);
  friend istream& operator >> (istream& is, Vector3D& p);

};

/** What is angle between Vector3D a and b? 

    @return angle from 0 to pi */
inline 
double
angle(const Vector3D& a, const Vector3D& b)
{
  PRECOND(a.length() * b.length() != 0.0);

  double d = (a * b) / (a.length() * b.length());

  // Cope with numeric inaccurracy.
  if (d > 1.0)
    {
      DEBUG_MSG("WARNING: Numeric inaccurracy.");
      d = 1.0;
    }
  else if (d < -1.0)
    {
      DEBUG_MSG("WARNING: Numeric inaccurracy.");
      d = -1.0;
    }

THROWIFNOT((-1.0 <= d) && (d <= 1.0),"Assertion violation in L281");

  return acos(d);
}

/** Cross product. */
inline 
Vector3D 
cross(const Vector3D& v1, const Vector3D& v2) 
{ 
  Vector3D result;
  result._x = v1._y * v2._z - v1._z * v2._y;
  result._y = v1._z * v2._x - v1._x * v2._z;
  result._z = v1._x * v2._y - v1._y * v2._x;
  return result;

}

/** Returns (new) rotated vector. rotate around center vector (not center location but direction) */
/* now moved to Matrix3D.h
  inline 
  Vector3D 
  rotate(const Vector3D& v, const Vector3D& center, double angle) 
  { 
  return (rotationMatrix(center, angle)) * v; 
  }
*/

/** Euclidian distance between two points. */
inline 
double
vecDistance(const Vector3D& a, const Vector3D& b)
{
  return (a-b).length();
}

/** Euclidian distance between two points. */
inline 
double
vecDistanceSquare(const Vector3D& a, const Vector3D& b)
{
  return (a-b).lengthSquare();
}

/** out of plane bend: what is the angle between the plane defined
    with v1 and v2 and the vector v3 ? 
*/
inline
double
outOfPlaneBend(const Vector3D& v1,const Vector3D& v2,const Vector3D& v3)
{
  Vector3D crossV = cross(v1,v2);
  return fabs( (0.5*PI) - angle(crossV,v3) );
}

inline 
ostream& 
operator << (ostream& os, const Vector3D& v)
{
  return os << "(Vector3D " << v.x() << " " << v.y() << " " << v.z() << " ) ";
}

inline 
istream& 
operator >> (istream& is, Vector3D& v)
{
  double x, y, z;
  string tag1, tag2;

  is >> tag1 >> x >> y >> z >> tag2;

  if ((tag1 != "(Vector3D") || (tag2 != ")")) 
    {
      DEBUG_MSG("bad input.");
      is.clear(ios::badbit);
    }
  else
    {
      v = Vector3D(x, y, z);
    }

  return is;
}

#endif /* __VECTOR3D_H__ */
