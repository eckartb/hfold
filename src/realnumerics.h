#ifndef __REAL_NUMERICS__
#define __REAL_NUMERICS__

#include "debug.h"
#include "Vector3D.h"

inline
double
convexComb(double frac, double a, double b)
{
  return (1.0 - frac) * a +  frac * b;
}

double
interpol(double x, double xmin, double xmax, double ymin, double ymax);

// trilinear interpolation, fancy version
double
interpol(const Vector3D& pos,
	 double dx,
	 double dy,
	 double dz,
	 double y0,
	 double y1, 
	 double y2,
	 double y3,
	 double y1N,
	 double y2N,
	 double y3N);

// trilinear interpolation!
inline
double
interpol(const Vector3D& pos,
	 double dx,
	 double dy,
	 double dz,
	 const Vector3D& pos0,
	 double y0,
	 double y1, 
	 double y2,
	 double y3)
{
  PRECOND((dx > 0.0) && (dy > 0.0) && (dz > 0.0));
  // pos = pos - pos0;
  //    double frac1 = pos.x() / dx; // projection to x axis
  //    double frac2 = pos.y() / dy; // projection to y axis
  //    double frac3 = pos.z() / dz;
  // double result = y0 + convexComb(frac1, 0, y1-y0) + convexComb(frac2, 0, y2-y0) + convexComb(frac3, 0, y3-y0);
  return (y0 + ((pos.x()-pos0.x()) * (y1-y0)/dx)  + ((pos.y()-pos0.y()) * (y2-y0)/dy) + ((pos.z()-pos0.z()) * (y3-y0)/dz));
}

// trilinear interpolation!
double
interpol(Vector3D pos,
	 double y,
	 Vector3D pos0,
	 double y0,
	 Vector3D pos1,
	 double y1, 
	 Vector3D pos2,
	 double y2,
	 Vector3D pos3,
	 double y3);

///////// from .cc file


double
interpol(double x, double xmin, double xmax, double ymin, double ymax)
{
  PRECOND(xmax > xmin);
  double frac = (x-xmin)/(xmax - xmin);
  return convexComb(frac, ymin, ymax);
  // return (1.0 - frac) * ymin + frac * ymax;
}

// trilinear interpolation!
double
interpol(Vector3D pos,
	 double y,
	 Vector3D pos0,
	 double y0,
	 Vector3D pos1,
	 double y1, 
	 Vector3D pos2,
	 double y2,
	 Vector3D pos3,
	 double y3)
{
  pos = pos - pos0;
  pos1 = pos1 - pos0;
  pos2 = pos2 - pos0;
  pos3 = pos3 - pos0;
  double d1 = pos1.length();
  double d2 = pos2.length();
  double d3 = pos3.length();
  double frac1 = pos * pos1; // projection to x axis
  double frac2 = pos * pos2; // projection to y axis
  double frac3 = pos * pos3;
  if (d1 > 0.0){
    frac1 /= (d1 * d1);
  }
  if (d2 > 0.0){
    frac2 /= (d2 * d2);
  }
  if (d3 > 0.0){
    frac3 /= (d3 * d3);
  }
  double result = convexComb(frac1, y0, y1) + convexComb(frac2, y0, y2) + convexComb(frac3, y0, y3);
  return result;
}

// trilinear interpolation!
double
interpol(const Vector3D& pos,
	 double dx,
	 double dy,
	 double dz,
	 double y0,
	 double y1, 
	 double y2,
	 double y3,
	 double y1N,
	 double y2N,
	 double y3N)
{
  PRECOND((dx > 0.0) && (dy > 0.0) && (dz > 0.0));
  // pos = pos - pos0;
  //    double frac1 = pos.x() / dx; // projection to x axis
  //    double frac2 = pos.y() / dy; // projection to y axis
  //    double frac3 = pos.z() / dz;
  // double result = y0 + convexComb(frac1, 0, y1-y0) + convexComb(frac2, 0, y2-y0) + convexComb(frac3, 0, y3-y0);
  // pos = pos - pos0;
  double result = y0;
  if (pos.x() > 0) {
    result += pos.x() * (y1 - y0) / dx;
  }
  else {
    result += pos.x() * (y0 - y1N) / dx;
  }
  if (pos.y() > 0) {
    result += pos.y() * (y2 - y0) / dx;
  }
  else {
    result += pos.y() * (y0 - y2N) / dx;
  }
  if (pos.z() > 0) {
    result += pos.z() * (y3 - y0) / dx;
  }
  else {
    result += pos.z() * (y0 - y3N) / dx;
  }
  return result;
}




#endif
