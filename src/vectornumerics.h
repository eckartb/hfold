// --*- C++ -*------x---------------------------------------------------------
// $Id:
//
// Author:          Eckart Bindewald
//
// Description:     simple numeric functions. Could be used for
//                  more sophisticated matrix or vector classes.
// 
// -----------------x-------------------x-------------------x-----------------

#ifndef __VECTOR_NUMERICS_H__
#define __VECTOR_NUMERICS_H__

#define __STL_NO_DRAND48 

// Includes

#include <math.h>
#include <limits.h>
#include <list>
#include <set>
#include "Vec.h"
// #include <nrutil.h>
#include <algorithm>
#include <math.h>

#include "debug.h"
#include "Matrix3D.h"
#include "Vector3D.h"
#include "RankedSolution4.h"
#include "Limits.h"
#include "StringTools.h"
#include "generalNumerics.h"

// #define NRANSI
// #include <nrutil.h>
// #define TINY 1.0e-20;

#include "Random.h"
#include "RankedSolution5.h"


// typedef Vec<Vector3D> Graph;

/** return false if not a number (NaN) */
bool isDefined(const Vector3D& v);

/** return true, if not obscure number */
template <class T>
inline
bool
isReasonable(const Vec<T>& v)
{
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (!isReasonable(v[i])) {
      return false;
    }
  }
  return true;
}


/** returns error function for positive values of z
    approximation according to Abramowitz and Stegun, 1964 
    (http://www.geo.vu.nl/users/hydro/students/hd537/error_function.pdf) */
double errorFunction(double z);

/** returns square of number */
template <class T>
T
square(const T& x)
{
  return x * x;
}

template <class T>
inline
void
writeHead(ostream& os, T& iterator, const T& iteratorEnd, const string& delim, size_t n = 10) {
  for (size_t i = 0; (i < n) && (iterator != iteratorEnd); iterator++) {
    if (i >0) {
      os << delim;
    }
    os << (*iterator);
  }
}

/* translate list into vector */
template<class T>
inline
Vec<T>
listToVector(const list<T>& lst)
{
  Vec<T> result(lst.size());
  unsigned int count = 0;
  for (typename list<T>::const_iterator i = lst.begin(); i != lst.end(); ++i) {
THROWIFNOT(count < result.size(),"Assertion violation in L97");
    result[count++] = *i;
  }
  return result;
}

/* translate list into vector */
/*
// template<class T>
inline
Vec<unsigned int>
listToVector(const list<unsigned int>& lst)
{
  Vec<unsigned int> result(lst.size());
  unsigned int count = 0;
  for (list<unsigned int>::const_iterator i = lst.begin(); i != lst.end(); i++) {
THROWIFNOT(count < result.size(),"Assertion violation in L113");
    result[count++] = *i;
  }
  return result;
}
*/

/** n trials with counts.size() possible outcomes are performed.
 * Given a vector of probabilities of observing each outcome,
 * it returns the probability of observing a particular patterns of counts of outcomes.
 * Corresponds to trials "with replacement"; use hypergeometric distribution if "without replacement"
 */
double
logMultinomial(const Vec<int>& counts, const Vec<double>& probs, int n);

/**
 * translates vector of unsigned integer to vector of integer
 */
Vec<int>
uivtoiv(const Vec<unsigned int>& v);

/**
 * translates vector of integer to vector of unsigned integer
 */
Vec<unsigned int>
ivtouiv(const Vec<int>& v);

/* return smaller entity. If equal,return a */
template <class T>
inline
T
minimum(const T& a, const T& b)
{
  if (b < a) {
    return b;
  }
  return a;
}

/** returns j'th column of matrix */
template<class T>
inline
Vec<T>
getColumn(const Vec<Vec<T> >& m, unsigned int j)
{
  Vec<T> result(m.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
THROWIFNOT(j < m[i].size(),"Assertion violation in L160");
    result[i] = m[i][j];
  }
  return result;
}

/** Multiplies all row and columns with vector entries */
void
modulateMatrix(Vec<Vec<double> >& matrix,
	       const Vec<double>& modulateValues);

/** returns j'th column of matrix */
template<class T>
inline
void
setColumn(Vec<Vec<T> >& m, const Vec<T>& v, unsigned int j)
{
  PRECOND(m.size()==v.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
THROWIFNOT(j < m[i].size(),"Assertion violation in L179");
    m[i][j] = v[i];
  }
}


/** returns j'th column of matrix */
template<class T>
inline
Vec<Vec<T> >
getYSlice(const Vec<Vec<Vec<T> > >& m, unsigned int y)
{
  Vec<Vec<T> >result(m.size(), Vec<T>(m[0][0].size()));
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m[0][0].size(); ++j) {
THROWIFNOT(i < m[i].size(),"Assertion violation in L194");
      result[i][j] = m[i][y][j];
    }
  }
  return result;
}

/** returns j'th column of matrix */
template<class T>
inline
void
setYSlice(Vec<Vec<Vec<T> > >& m, const Vec<Vec<T> >& v, unsigned int j)
{
  PRECOND(m.size()==v.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int k = 0; k < m[0][0].size(); ++k) {
      m[i][j][k] = v[i][k];
    }
  }
}


/** returns j'th column of matrix */
template<class T>
inline
Vec<Vec<T> >
getZSlice(const Vec<Vec<Vec<T> > >& m, unsigned int z)
{
  Vec<Vec<T> >result(m.size(), Vec<T>(m[0].size()));
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m[0].size(); ++j) {
      result[i][j] = m[i][j][z];
    }
  }
  return result;
}

/** returns j'th column of matrix */
template<class T>
inline
void
setZSlice(Vec<Vec<Vec<T> > >& m, const Vec<Vec<T> >& v, unsigned int z)
{
  PRECOND(m.size()==v.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m[0].size(); ++j) {
      m[i][j][z] = v[i][j];
    }
  }
}


/** returns j'th column of matrix */
template<class T>
inline
Vec<Vec<T> >
getColumns(const Vec<Vec<T> >& m, const Vec<unsigned int> cols)
{
  Vec<Vec<T> > result(m.size(), Vec<T>(cols.size()));
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < cols.size(); ++j) {
THROWIFNOT(cols[j] < m[i].size(),"Assertion violation in L255");
      result[i][j] = m[i][cols[j]];
    }
  }
  return result;
}

/** returns j'th column of matrix, ignore too short rows */
template<class T>
inline
Vec<T>
getColumnSave(const Vec<Vec<T> >& m, unsigned int j)
{
  Vec<T> result;
  for (unsigned int i = 0; i < m.size(); ++i) {
    if (m[i].size() > j) {
      result.push_back(m[i][j]);
    }
  }
  return result;
}

/** deletes one row of matrix */
template<class T>
inline
void
deleteMatrixRow(Vec<Vec<T> >& mtx,
		unsigned int n)
{
  PRECOND(n < mtx.size());
  mtx.erase(mtx.begin()+n);
}

/** deletes one column of matrix */
template<class T>
inline
void
deleteMatrixCol(Vec<Vec<T> >& mtx,
		unsigned int n)
{
  PRECOND((mtx.size() > 0) && (n < mtx[0].size()));
  for (unsigned int i = 0; i < mtx.size(); ++i) {
    mtx[i].erase(mtx[i].begin()+n);
  }
}

/* return larger entity. If equal return b */
template <class T>
inline
T
maximum(const T& a, const T& b)
{
  if (a < b) {
    return b;
  }
  return a;
}

/** 
 * returns vector in which each element i is the maximum of the i'th element of a and the i'th element of b
 */
template <class T>
inline
Vec<T>
maxVector(const Vec<T>& a, const Vec<T>& b) {
  PRECOND(a.size() == b.size());
  unsigned int n = a.size();
  Vec<T> c(n);
  for (unsigned int i = 0; i < n; ++i) {
    c[i] = maximum(a[i], b[i]);
  }
  return c;
}

/** 
 * returns vector in which each element i is the maximum of the i'th element of a and the i'th element of b
 */
template <class T>
inline
Vec<T>
minVector(const Vec<T>& a, const Vec<T>& b) {
  PRECOND(a.size() == b.size());
  unsigned int n = a.size();
  Vec<T> c(n);
  for (unsigned int i = 0; i < n; ++i) {
    c[i] = minimum(a[i], b[i]);
  }
  return c;
}

Vec<int> uiVecToIVec(const Vec<unsigned int>& v);

Vec<unsigned int> iVecToUiVec(const Vec<int>& v);


/** return true, if matrix is symmetrix */
bool
isSymmetric(const Vec<Vec<double> >& m);

/** symmetrizes matrix */
void
symmetrize(Vec<Vec<double> >& m);

/** my version of maximum function */
template <class T>
inline
T
maxFunc(const T& a, const T& b)
{
  if (a < b) {
    return b;
  }
  return a;
}

/** my version of minimum function */
template <class T>
inline
T
minFunc(const T& a, const T& b)
{
  if (b < a) {
    return b;
  }
  return a;
}

// generate array starting with 0 , incresing like a stair
Vec<unsigned int>
generateStair(unsigned int maxi, unsigned int mini = 0, unsigned int step = 1);

// return permutation of vector
template <class T>
Vec<T>
permutate(const Vec<T>& v, const Vec<unsigned int>& permut)
{
  PRECOND(permut.size() == v.size());
  Vec<T> result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
THROWIFNOT(permut[i] < v.size(),"Assertion violation in L394");
    result[i] = v[permut[i]];
  }
  return result;
}

// devides orig data into v1 (with size numPart) and v2 (size.orig -numpart)
// orderOrig: specifies order of v1 and v2
// may not contain dublicates!
template<class T>
inline
void
generatePartition(Vec<T>& orig,
		  Vec<T>& v1, 
		  Vec<T>& v2,
		  unsigned int numPart,
		  const Vec<unsigned int>& orderOrig)
{
  PRECOND(numPart <= orig.size()); 
  Vec<unsigned int> order;
  if (orderOrig.size() != orig.size()) {
    order = generateStair(orig.size());
  }
  else {
    order = orderOrig;
  }
  if (numPart == 0) {
    v1.clear();
    v2 = orig;
  }
  else if (numPart >= orig.size()) {
    numPart = orig.size();
    v1 = orig;
    v2.clear();
  }
  else {
    v1 = Vec<T>(numPart);
    v2 = Vec<T>(orig.size() - numPart);
  }
  for (unsigned int i = 0; i < numPart; ++i) {
THROWIFNOT(order[i] < orig.size(),"Assertion violation in L434");
    v1[i] = orig[order[i]]; 
  }
  for (unsigned int i = numPart; i < orig.size(); ++i) {
THROWIFNOT(order[i] < orig.size(),"Assertion violation in L438");
    v2[i-numPart] = orig[order[i]]; 
  }
  POSTCOND(v1.size() + v2.size() == orig.size());  
}

// generate 2 partitions from orig
// v2 has size 1
template<class T>
inline
void
generatePartitionLeaveOneOut(Vec<T>& orig,
			     Vec<T>& v1, 
			     Vec<T>& v2,
			     unsigned int numPart)
{
  PRECOND(numPart <= orig.size()); 
  ERROR_IF(orig.size() == 0, "Cannot generate partition of empty set!");
  if (orig.size() == 1) {
    v1 = Vec<T>();
    v2 = orig;
    return;
  }
  v1 = Vec<T>(orig.size() - 1);
  v2 = Vec<T>(1, orig[numPart]);
  for (unsigned int i = 0; i < numPart; ++i) {
    v1[i] = orig[i];
  }
  for (unsigned int i = numPart+1; i < orig.size(); ++i) {
    v1[i-1] = orig[i];
  }

  POSTCOND(v1.size() + v2.size() == orig.size());
}

/*
// devides orig data into v1 (with size numPart) and v2 (size.orig -numpart)
// orderOrig: specifies order of v1 and v2
// may not contain dublicates!
template<class T>
inline
void
generatePartition(Vec<T>& orig,
		  Vec<T>& v1, 
		  Vec<T>& v2,
		  unsigned int numPart,
		  const Vec<unsigned int>& orderOrig)
{
  PRECOND(numPart <= orig.size()); 
  Vec<unsigned int> order;
  if (orderOrig.size() != orig.size()) {
    order = generateStair(orig.size());
  }
  else {
    order = orderOrig;
  }
  if (numPart == 0) {
    v1.clear();
    v2 = orig;
  }
  else if (numPart >= orig.size()) {
    numPart = orig.size();
    v1 = orig;
    v2.clear();
  }
  else {
    v1 = Vec<T>(numPart);
    v2 = Vec<T>(orig.size() - numPart);
  }
  for (unsigned int i = 0; i < numPart; ++i) {
THROWIFNOT(order[i] < orig.size(),"Assertion violation in L508");
    v1[i] = orig[order[i]]; 
  }
  for (unsigned int i = numPart; i < orig.size(); ++i) {
THROWIFNOT(order[i] < orig.size(),"Assertion violation in L512");
    v2[i-numPart] = orig[order[i]]; 
  }
  POSTCOND(v1.size() + v2.size() == orig.size());  
}
*/

/* return vector with size numReturn, which has randomly assigned non-redundant bin values between min and max */
Vec<double>
rouletteWheel(double min, double max, unsigned int numBins, unsigned int numReturn);

/**
 * chooses an element indix from vector pVec with probability given in pVec
 */
unsigned int
chooseRouletteWheel(const Vec<double>& pVec); 

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel(const Vec<double>& pVec, const Vec<unsigned int>& tabooList);

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel2(const Vec<double>& pVec, const Vec<unsigned int>& tabooList);

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel3(const Vec<double>& pVec, const set<unsigned int>& tabooList);

/** Thread-safe version based on rand_r
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, const set<unsigned int>& tabooList, unsigned int * seed);

/** Thread-safe version based on rand_r
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, const Vec<unsigned int>& tabooList, unsigned int * seed);


/** Thread safe version of chooseRouletteWheel
 * chooses an element indix from vector pVec with probability given in pVec
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, unsigned int * seed); 

/**
 * generates non-redundant vecor of unsigned int with num entries between minId and maxId
 */
Vec<unsigned int>
generateRandomIndexSubset(unsigned int num,
                          unsigned int maxId, unsigned int minId);

/**
 * generates vecor of unsigned int with num entries between 0 and num
 * no indices can be double, WITH "replacement", use for bootstrap!
 */
Vec<unsigned int>
generateBootSubset(unsigned int num);

/** c = a + b */
template <class T>
inline
void
vectorAdd(const Vec<T>& a, const Vec<T>& b, Vec<T>& c)
{
  ERROR_IF((a.size() != b.size()),
	   "Adding vectors of different sizes!");
  if (a.size() == 0) {
    c.clear();
    return;
  }
  if (c.size() != a.size()) {
    c = Vec<T>(a.size());
  }
  for (unsigned int i = 0; i < a.size(); ++i) {
    c[i] = a[i] + b[i];
  }
}

/** c = a + b */
template <class T, class S>
inline
Vec<Vec<double> >
matrixAdd(const Vec<Vec<T> >& a, const Vec<Vec<S> >& b)
{
  PRECOND((a.size() == b.size()));
  Vec<Vec<T> > c = a;
  for (unsigned int i = 0; i < a.size(); ++i) {
    for (unsigned int j = 0; j < a[i].size(); ++j) {
      c[i][j] = a[i][j] + b[i][j];
    }
  }
  return c;
}

/** a <- a + b */
template <class T, class S>
inline
void
matrixAdd2(Vec<Vec<T> >& a, const Vec<Vec<S> >& b)
{
  PRECOND((a.size() == b.size()));
  for (unsigned int i = 0; i < a.size(); ++i) {
    for (unsigned int j = 0; j < a[i].size(); ++j) {
      a[i][j] += b[i][j];
    }
  }
}

/** a <- a - b */
template <class T, class S>
inline
void
matrixSub2(Vec<Vec<T> >& a, const Vec<Vec<S> >& b)
{
  PRECOND((a.size() == b.size()));
  for (unsigned int i = 0; i < a.size(); ++i) {
    for (unsigned int j = 0; j < a[i].size(); ++j) {
      a[i][j] -= b[i][j];
    }
  }
}

/** c = a + b */
template <class T>
inline
Vec<Vec<double> >
matrixSquareAdd(const Vec<Vec<T> >& a, const Vec<Vec<T> >& b)
{
  PRECOND((a.size() == b.size()));
  Vec<Vec<T> > c = a;
  for (unsigned int i = 0; i < a.size(); ++i) {
    for (unsigned int j = 0; j < a[i].size(); ++j) {
      c[i][j] = a[i][j] + (b[i][j] * b[i][j]);
    }
  }
  return c;
}

/** cij = max(aij, bij) for each element */
template <class T>
inline
Vec<Vec<double> >
matrixMax(const Vec<Vec<T> >& a, const Vec<Vec<T> >& b)
{
  PRECOND((a.size() == b.size()));
  Vec<Vec<T> > c = a;
  for (unsigned int i = 0; i < a.size(); ++i) {
    for (unsigned int j = 0; j < a[i].size(); ++j) {
      if (a[i][j] >= b[i][j]) {
	c[i][j] = a[i][j];
      }
      else {
	c[i][j] = b[i][j];
      }
    }
  }
  return c;
}

/** c = a + b */
template <class T>
inline
void
vectorAdd(const Vec<T>& a, const T& b, Vec<T>& c)
{
  PRECOND((a.size() == c.size()));
  for (unsigned int i = 0; i < a.size(); ++i) {
    c[i] = a[i] + b;
  }
}

/** c = a - b */
template <class T>
inline
void
vectorSubtract(const Vec<T>& a,
	       const Vec<T>& b, 
	       Vec<T>& c)
{
  ERROR_IF((a.size() != b.size()), 
	   "Internal error in line 471: subtracting vectors of different dimensions!");
  if (a.size() == 0) {
    c.clear();
    return;
  }
  if (c.size() != a.size()) {
    c = Vec<T>(a.size());
  }
  for (unsigned int i = 0; i < a.size(); ++i) {
    c[i] = a[i] - b[i];
  }
}

/** a <- b a  */
void
scalarMul(Vec<double>& a, double b);

/** c = a . b */
double
dotProduct(const Vec<double>& a,
	   const Vec<double>& b);

/** return sum of elements */
double
elementSum(const Vec<double>& v);

/** return sum of elements */
template<class T>
inline
T
elementSum(const Vec<T>& v) {
  T result = 0;
  for (Vec<int>::size_type i = 0; i < v.size(); ++i) {
    result += v[i];
  }
  return result;
}

/** return sum of elements */
template <class T>
inline
T
elementSum(const Vec<Vec<T> >& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      sum += v[i][j];
    }
  }
  return sum;
}

/** return sum of elements */
template <class T>
inline
T
elementSquareSum(const Vec<Vec<T> >& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      sum += v[i][j]*v[i][j];
    }
  }
  return sum;
}

/** return sum of elements */
template <class T>
inline
T
elementSum(const Vec<Vec<Vec<T> > >& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      for (unsigned int k = 0; k < v[i][j].size(); ++k) {
	sum += v[i][j][k];
      }
    }
  }
  return sum;
}

/** return sum of elements */
template <class T>
inline
T
elementSum(const Vec<Vec<Vec<Vec<T> > > >& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      for (unsigned int k = 0; k < v[i][j].size(); ++k) {
	for (unsigned int m = 0; m < v[i][j][k].size(); ++m) {
	  sum += v[i][j][k][m];
	}
      }
    }
  }
  return sum;
}

/** returns "1.0" for perfectly "sharp" predictions
    (each row with all zeros and one "1"),
    final result is smaller one otherwise
*/
double
contactMatrixQuality(const Vec<Vec<double> >& matrix, unsigned int diagLim);

/** return index of maximum value element */
template<class T>
inline
typename Vec<T>::size_type
maxElement(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  unsigned int bestIndex = 0;
  T bestVal = v[0];
  for (unsigned int i = 1; i < v.size(); ++i) {
    if (v[i] > bestVal) {
      bestVal = v[i];
      bestIndex = i;
    }
  }
  return bestIndex;
}

/** return index of minimum value element */
template<class T>
inline
typename Vec<T>::size_type
minElement(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  unsigned int bestIndex = 0;
  T bestVal = v[0];
  for (unsigned int i = 1; i < v.size(); ++i) {
    if (v[i] < bestVal) {
      bestVal = v[i];
      bestIndex = i;
    }
  }
  return bestIndex;
}

// transform such that sum of values is 1.0
void
probabilityNormalize(Vec<double>& v);

// transform such that sum of values is targetSum
void
probabilityNormalize(Vec<double>& v, double targetSum);

// transform such that sum of values is 1.0
void
probabilityNormalize(Vec<Vec<double> >& v);

// transform such that sum of values is 1.0
void
bayesianNormalize(Vec<double>& v);

// transform such that sum of values is 1.0
void
bayesianNormalize(Vec<Vec<double> >& v);

// compute euclidian norm of vector
template <class T>
inline
double
euclidianNorm(const Vec<T>& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += v[i] * v[i];
  }
  return sqrt(static_cast<double>(sum));
}

// compute euclidian norm of vector
template <class T>
inline
double
euclidianNormSquare(const Vec<T>& v)
{
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += v[i] * v[i];
  }
  return static_cast<double>(sum);
}


template <class T>
inline
double 
euclidianDistance(const Vec<T>& v1, const Vec<T>& v2)
{
  ERROR_IF(v1.size() != v2.size(), "Different dimensions encountered!");
  ERROR_IF(v1.size() == 0, "Zero dimension encountered!");
  Vec<T> v3(v1.size());
  vectorSubtract(v1, v2, v3);
  return euclidianNorm(v3);
}

template <class T>
inline
double 
euclidianDistanceSquare(const Vec<T>& v1, const Vec<T>& v2)
{
  ERROR_IF(v1.size() != v2.size(), "Different dimensions encountered!");
  ERROR_IF(v1.size() == 0, "Zero dimension encountered!");
  Vec<T> v3(v1.size());
  vectorSubtract(v1, v2, v3);
  return euclidianNormSquare(v3);
}

template <class T>
inline
double 
euclidianNorm(const Vec<Vec<T> >& v1)
{
  ERROR_IF(v1.size() == 0, "Zero dimension encountered!");
  Vec<Vec<T> > v2 = v1;
  return sqrt(elementSquareSum(v2));
}

template <class T>
inline
double 
euclidianDistance(const Vec<Vec<T> >& v1, const Vec<Vec<T> >& v2)
{
  ERROR_IF(v1.size() != v2.size(), "Different dimensions encountered!");
  ERROR_IF(v1.size() == 0, "Zero dimension encountered!");
  Vec<Vec<T> > v3 = v1;
  matrixSub2(v3, v2);
  return sqrt(elementSquareSum(v3));
}

/** square of euclidian norm, however with scaling of dimensions */
template <class T>
inline
double 
euclidianDistanceSquare(const Vec<T>& v1, const Vec<T>& v2, 
			const Vec<double>& scaling)
{
  ERROR_IF(v1.size() != v2.size(), "Different dimensions encountered!");
  ERROR_IF(v1.size() == 0, "Zero dimension encountered!");
  Vec<T> v3(v1.size());
  vectorSubtract(v1, v2, v3);
  for (unsigned int i = 0; i < v3.size(); ++i) {
    v3[i] *= scaling[i];
  }
  return euclidianNormSquare(v3);
}


// compute euclidian norm of vector
inline
double
absNorm(const Vec<double>& v) {
  double result = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    result += fabs(v[i]);
  }
  return result;
}

// transform such that sum of SQUARED values is 1.0
void
normalizeEuclidian(Vec<double>& v);

// transform such that sum of SQUARED values is 1.0
inline
void
normalizeAbs(Vec<double>& v)
{
  double norm = absNorm(v);
  if (norm <= 0.0) {
    return;
  }
  norm = 1.0 / norm;
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] *= norm;
  }
}

/** return mean value of elements */
template<class T>
inline
double
vecMean(const Vec<T>& v, unsigned int start, unsigned int end)
{
  PRECOND((v.size() > 0) && (start < end) && (end <= v.size()));
  double sum = 0.0;
  for (unsigned int i = start; i < end; ++i) {
    sum += static_cast<double>(v[i]);
  }
  return sum / (end - start);
}

/** return mean value of elements */
inline
Vec<double>
vecInverse(const Vec<double>& v)
{
  Vec<double> result(v.size(), 0.0);
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] != 0.0) {
      result[i] = 1.0 / v[i];
    }
  }
  return result;
}

/** return mean value of elements */
template<class T>
inline
double
vecMean(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  return vecMean(v, 0, v.size());
}

/* compute mean from sum and sum of squares of set of values */
double
varianceFromSum(double sum, double sumSquares, unsigned int n);

/** return variance (sigma squared) value of elements
  <x**2> - <x>**2 with correction factor */
template<class T>
inline
double
vecVariance(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  double sum = 0.0;
  double sumSquares = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sumSquares += static_cast<double>(v[i])
         * static_cast<double>(v[i]);
    sum += static_cast<double>(v[i]);
    
  }
  double result = varianceFromSum(sum, sumSquares, v.size());
  POSTCOND(result >= 0.0);
  return result;
}

/** return mean value of elements */
template<class T>
inline
double
vecMeanMinusDev(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  double mean = vecMean(v, 0, v.size());
  double stddev = sqrt(vecVariance(v)/v.size());
  if (fabs(mean) <= stddev) {
    mean = 0.0;
  }
  else if (mean > 0.0) {
    mean -= stddev;
  }
  else {
    mean += stddev;
  }
  return mean;
}


/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void transformToZScores(Vec<double>& v);

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void transformToZScores(Vec<Vec<double> >& m);

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void
transformToZScores(Vec<double>& v, double minSigma);

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
    use only subset indices for computing mean and std
*/
void
transformToZScores(Vec<double>& v,
		   const Vec<unsigned int>& subSet);

/** multiply all entries of matrix with this factor */
void
matrixScalarMul(Vec<Vec<double> >& matrix, double val);

/** b = m a */
void
matrixVectorMul(const Vec<Vec<double> >& m,
		const Vec<double>& a,
		Vec<double>& b);

/** b = a(T) b */
void
vectorMatrixMul(const Vec<Vec<double> >& m,
		const Vec<double>& a,
		Vec<double>& b);

/** standard matrix multiplication. c must already have correct dimensions */
void 
matrixMul(const Vec<Vec<double> >& a,
	  const Vec<Vec<double> >& b,
	  Vec<Vec<double> >& c);

/** non-standard element-wise multiplication. All matrices must have same dimensions. */
void 
matrixElementMul(const Vec<Vec<double> >& a,
		 const Vec<Vec<double> >& b,
		 Vec<Vec<double> >& c);

/** transpose matrix even if not square matrix */
template <class T>
inline
Vec<Vec<T> >
matrixTranspose2(const Vec<Vec<T> >& m1)
{
  unsigned int m = m1.size(); 
  ERROR_IF(m == 0, "Matrix with zero elements encountered.");
  unsigned int n = m1[0].size(); 
  Vec<T> newRow(m);
  Vec<Vec<T> > m2(n, newRow);
  for (unsigned int i = 0; i < m1.size(); ++i) {
    for (unsigned int j = 0; j < m1[i].size(); ++j) {
THROWIFNOT((j < m2.size())&& (i < m2[j].size()),"Assertion violation in L1154");
      m2[j][i] = m1[i][j];
    }
  }
  return m2;
}


/** transpose matrix itself. Works only for square matrices.  */
void
matrixTranspose(Vec<Vec<double> >& m);

/** set matrix elements to one, if i=j, zero otherwise
 */
void
unitMatrix(Vec<Vec<double> >& matrix);

/* return m x n matrix */
template<class T>
inline
Vec<Vec<T> > 
generate2DMatrix(unsigned int m, unsigned int n, const T& val)
{
  Vec<T> row(n, val);
  return Vec<Vec<T> > (m, row);
}

/* return m x n matrix */
template<class T>
inline
Vec<Vec<T> > 
generate3DMatrix(unsigned int n1, unsigned int n2, unsigned int n3, const T& val)
{
  Vec<T> row(n3, val);
  Vec<Vec<T> > field(n2, row);
  return Vec<Vec<T> > (n1, field);
}

/* generate matrix with distances of vectors */
inline
Vec<Vec<double> >
generateDistanceMatrix(const Vec<Vector3D>& v)
{
  Vec<Vec<double> > field = generate2DMatrix(v.size(), v.size(), 0.0);
  for (unsigned i = 0; i < v.size(); ++i) {
    field[i][i] = 0.0;
    for (unsigned j = 0; j < i; ++j) {
      field[i][j] = vecDistance(v[i],v[j]);
      field[j][i] = field[i][j];
    }
  }
  return field;
}

template<class T>
inline
unsigned int 
countElements(const Vec<Vec<T> >& m) {
  unsigned int result = 0;
  for (unsigned int i = 0; i < m.size(); ++i) {
    result += m[i].size();
  }
  return result;
}

/** translates matrix into 1D vector */
template<class T>
inline
Vec<T>
matrix2Vec(const Vec<Vec<T> >& m) {
  unsigned int numElements = countElements(m);
  Vec<T> v(numElements);
  unsigned int pc = 0;
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m[i].size(); ++j) {
      v[pc++] = m[i][j];
    }
  }
  return v;
}

/** return bin of histogram */
int getBin(double val, double min, double delta);

/** return histogram: vector of bins filled with counts */
Vec<unsigned int> computeHistogram(const Vec<double>& data,
	          double min, double delta, unsigned int numBins);

/** return historgramm: vector of bins filled with y values belonging
 to this bin */
Vec<Vec<double> > computeScatterHistogram(const Vec<double>& datax,
	  const Vec<double>& datay, double min, double delta, 
					  unsigned int numBins);

/** return histogram: vector of bins filled with counts */
Vec<double> computeDoubleHistogram(const Vec<double>& data,
	          double min, double delta, unsigned int numBins);

/** return histogram: vector of bins filled with counts */
Vec<double> 
computeFrequency(const Vec<double>& data,
		 double min, double delta, unsigned int numBins);

/**
 * perform sum (integral) over histogram data
 */
template <class T>
inline
Vec<T>
accumulation(const Vec<T>& v) {
  Vec<T> result(v.size());
  T sum = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum = sum + v[i];
    result[i] = sum;
  }
  return result;
}

/** reads matrix in format that is also used by program "R" */
inline
Vec<Vec<double> >
readPlainMatrix(istream& is)
{
  Vec<Vec<double > > result;
  while (is) {
    string line = getLine(is);
    Vec<string> words = getTokens(line);
    if (words.size() > 0) {
      Vec<double> row(words.size());
      for (unsigned int i = 0; i < words.size(); ++i) {
	row[i] = stod(words[i]);
      }
      result.push_back(row);
    }
  }
  return result;
}

/** compute uniqe set from original set
    result is also sorted */
template <class T>
inline
Vec<T>
uniqueSet(const Vec<T>& vOrig)
{
  Vec<T> result;
  if (vOrig.size() == 0) {
    return result;
  }
  Vec<T> v = vOrig;
  sort(v.begin(), v.end());
  result.push_back(v[0]);
  for (unsigned int i = 1; i < v.size(); ++i) {
    if (!(v[i] == v[i-1])) {
      result.push_back(v[i]);
    }
  }
  // POSTCOND(!containsDuplicates(result));
  return result;
}

/** compute uniqe set from original set */
bool
containsDuplicates(const Vec<unsigned int>& vOrig);

/** get subset of set. Vector m contains the indices of vector v that are being returned. The result vector has length m and is in the same order as the vector m */
/* template <class T, class S>
inline
Vec<T>
getSubset(const Vec<T>& v, const Vec<S>& m)
{
  PRECOND(!containsDuplicates(m));
  Vec<T> result(m.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
THROWIFNOT(m[i] < v.size(),"Assertion violation in L1329");
    result[i] = v[m[i]];
  }
  return result;
  }
*/

/** get subset of set */
template <class T>
inline
Vec<T>
getSubset(const Vec<T>& v, const Vec<int>& m)
{
  Vec<T> result(m.size());
  for (size_t i = 0; i < m.size(); ++i) {
THROWIFNOT(m[i] >= 0,"Assertion violation in vectornumerics.h :  L1344");
THROWIFNOT(static_cast<size_t>(m[i]) < v.size(),"Assertion violation in vectornumerics.h :  L1345");
    result[i] = v[m[i]];
  }
  return result;
}

/** get subset of set */
template <class T>
inline
Vec<T>
getSubset(const Vec<T>& v, const Vec<unsigned int>& m)
{
  Vec<T> result(m.size());
  for (size_t i = 0; i < m.size(); ++i) {
THROWIFNOT(static_cast<size_t>(m[i]) < v.size(),"Assertion violation in L1359");
    result[i] = v[m[i]];
  }
  return result;
}

/** get subset of set */
template <class T>
inline
Vec<T>
getSubset(const Vec<T>& v, const Vec<size_t>& m)
{
  Vec<T> result(m.size());
  for (size_t i = 0; i < m.size(); ++i) {
THROWIFNOT(static_cast<size_t>(m[i]) < v.size(),"Assertion violation in L1373");
    result[i] = v[m[i]];
  }
  return result;
}

/** get subset of set, ignore invalid indices of m */
template <class T>
inline
Vec<T>
getValidSubset(const Vec<T>& v, const Vec<int>& m)
{
  Vec<T> result;
  for (unsigned int i = 0; i < m.size(); ++i) {
    if ((m[i] >= 0) && (m[i] < static_cast<int>(v.size()))) {
      result.push_back(v[m[i]]);
    }
  }
  return result;
}

/** return all emelents between 0 and maxSize, which are not part of m */
template <class T>
inline
Vec<unsigned int>
getAntiSet(unsigned int maxSize, const Vec<unsigned int>& m)
{
  Vec<T> result;
  for (unsigned int i = 0; i < maxSize; ++i) {
    bool found = false;
    for (unsigned int j = 0; j < m.size(); ++j) {
      if (i == m[j]) {
	found = true;
	break;
      }
    }
    if (!found) {
      result.push_back(i);
    }
  }
  return result;
}

/* deprecated, used removeFromSet instead */
template <class T>
inline
Vec<T>
getRemainingSet(const Vec<T>& m, const Vec<T>& subset)
{
  Vec<T> result;
  for (unsigned int i = 0; i < m.size(); ++i) {
    bool found = false;
    for (unsigned int j = 0; j < subset.size(); ++j) {
      if (m[i] == subset[j]) {
	found = true;
	break;
      }
    }
    if (!found) {
      result.push_back(m[i]);
    }
  }
  return result;
}

/** get subset of matrix (subset 2,5,13 will get you 3x3 matrix consisting of those columns and rows of the original matrix) */
template <class T>
inline
Vec<Vec<T> >
getMatrixSubset(const Vec<Vec<T> >& v, const Vec<unsigned int>& m)
{
  Vec<Vec<T> > result = getSubset(v, m);
  for (unsigned int i = 0; i < result.size(); ++i) {
    result[i] = getSubset(result[i], m);
  }
  return result;
}

/** get subset of matrix (subset 2,5,13 will get you 3x3 matrix consisting of those columns and rows of the original matrix) */
template <class T>
inline
Vec<Vec<T> >
getMatrixSubset(const Vec<Vec<T> >& v, const Vec<int>& m)
{
  Vec<Vec<T> > result = getSubset(v, m);
  for (unsigned int i = 0; i < result.size(); ++i) {
    result[i] = getSubset(result[i], m);
  }
  return result;
}

/** get subset of set of v, with all element removed which correspond to the indices of m 
 * @todo  : very slow implementation!
 */
template <class T>
inline
Vec<T>
removeFromSet(const Vec<T>& v, const Vec<unsigned int>& m)
{
  Vec<T> result;
  for (unsigned int i = 0; i < v.size(); ++i) {
    bool found = false;
    for (unsigned int j = 0; j < m.size(); ++j) {
      if (i == m[j]) {
	found = true;
	break;
      }
    }
    if (!found) {
      result.push_back(v[i]);
    }
  }
  return result;
}

/** get subset of set of v, with all element removed which correspond to the indices of m 
 * @todo  : very slow implementation!
 */
template <class T>
inline
Vec<T>
removeElement(const Vec<T>& v, unsigned int m)
{
  return removeFromSet(v, Vec<unsigned int>(1, m));
}

/** get subset of set v, which is NOT part of m*/
template <class T>
inline
Vec<T>
getExcludingSubset(const Vec<T>& v, const Vec<T>& m)
{
  Vec<T> result;
  for (unsigned int i = 0; i < v.size(); ++i) {
    bool found = false;
    for (unsigned int j = 0; j < m.size(); ++j) {
      // static_cast<int>(i)) { // BUGFIX November 2002!
      if (m[j] == v[i]) { 
	found = true;
	break;
      }
    }
    if (!found) {
      result.push_back(v[i]);
    }
  }
  return result;
}

/* compute common subset of two sets.
   not unique list, if input lists are not unique! */
Vec<unsigned int>
commonSubset(const Vec<unsigned int>& lOrig1,
	     const Vec<unsigned int>& lOrig2 );

/* compute unique union of two sets */
Vec<unsigned int>
uniqueUnion(const Vec<unsigned int>& l1,
	    const Vec<unsigned int>& l2);

/** vector with probabilities of different entities.
    values must be between 0 and 1
*/
double
conservation(const Vec<double>& v);

/* return 3D matrix from Euler angles
   rotation around x-axis: w
   rotation around y-axis: a
   rotation around z-axis: k
   taken from: Ronald Azuma 1991 (collinear.pdf). Check again from other source!!!
*/
Matrix3D
computeEulerRotationMatrix(double w, double a, double k);

/* find minimum x, y, z coordinates */
Vector3D
findMinCoordinates(const Vec<Vector3D>& vv);

/* find maximum x, y, z coordinates */
Vector3D
findMaxCoordinates(const Vec<Vector3D>& vv);

/* return index of v, which is furthest to pos */
unsigned int
findMaxDistanceIndex(const Vector3D& pos,
		     const Vec<Vector3D>& v);

/* return index of v, which is closest to pos */
unsigned int
findMinDistanceIndex(const Vector3D& pos,
		     const Vec<Vector3D>& v);

/** return coordinates which are in the middle of
    min and max values for each dimension */
Vector3D
computeCenterOfCoordinates(const Vec<Vector3D>& v);

/** return center of mass. Do not take weights into account! */
Vector3D
computeCenterOfMass(const Vec<Vector3D>& v);

/** return center of mass. */
Vector3D
computeCenterOfMass(const Vec<Vector3D>& v,
		    const Vec<double> weights);

/** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
    moment of inertia */
Matrix3D
computeGyrationMatrix(const Vec<Vector3D>& v,
		      const Vec<double> weights);

/** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
    moment of inertia */
void
computeGyrationRadiusGeometry(const Vec<Vector3D>& v,
			      const Vec<double> weights,
			      Vector3D& eigenValues,
			      Matrix3D& eigenVectors);

/** compute root mean square deviation of two sets of coordinate vectors */
double
rms(const Vec<Vector3D>& v1, const Vec<Vector3D>& v2);

/** 
    Move centroid of Vec <Vector3D> to ( 0, 0, 0)
  */
Vector3D moveCenter( Vec <Vector3D> &v1 );

/** compute optimal superposition of coordinates of
    molecules m1 and m2
    m2' = (m2-center(m2)) * rot + v
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1Orig, Vec<Vector3D>& v2, 
		Matrix3D& rot, Vector3D& v);

/** compute optimal superposition of coordinates of
    molecules m1 and m2
    m2' = (m2-oldCenter) * rot + v
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1Orig, Vec<Vector3D>& v2, 
		Matrix3D& rot, Vector3D& c1, Vector3D& c2);

/** compute optimal superposition of coordinates of
    molecules m1 and m2
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1, Vec<Vector3D>& v2);

/** compute optimal superposition of coordinates of
    molecules m1 and m2
*/
inline
double 
minRms(const Vec<Vector3D>& v1, const Vec<Vector3D>& v2){
  Vec<Vector3D> vHelp = v2;
  minRmsSuperpose(v1, vHelp);
  return rms(v1, vHelp);
}


/** return connected patch. Dist is maximum distance between
    adjacent vectors */
Vec<unsigned int>
getConnected(const Vec<Vector3D> v, double dist, unsigned int start);


/** sort vector of elements
    but also return the new rank order as Vec<unsigned int> 
*/
/*
  template <class T>
  inline
  Vec<unsigned int>
  documentedSort(Vec<T>& v)
  {
  return documentedSort2(v); // faster version!!!
  //   Vec<unsigned int> order(v.size());
//   for (unsigned int i = 0; i < order.size(); ++i) {
//     order[i] = i;
//   }
//   bool sorted = false;
//   while (!sorted) {
//     sorted = true;
//     for (unsigned int i = 1; i < v.size(); ++i) {
//       if (v[i] < v[i-1]) {
// 	swap(v[i], v[i-1]);
// 	swap(order[i], order[i-1]);
// 	sorted = false;
//       }
//     }
//   }
//   return order;
}
*/

/**
 * generates loopup table: i'th element of result is position of value of i in v
 */
Vec<unsigned int>
getIndexSet(const Vec<unsigned int>& v);

/**
 * exchanges content of elements n and m of vector 
 */
template <class T>
inline
void
swapElements(Vec<T>& v, unsigned int n, unsigned int m)
{
  PRECOND((n < v.size()) && (m < v.size()));
  T help = v[n];
  v[n] = v[m];
  v[m] = help;
} 

template <class T>
bool
isRectangle(const Vec<Vec<T> >& v) 
{
  if (v.size() == 0) {
    return false;
  }
  unsigned int sOrig = v[0].size();
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i].size() != sOrig) {
      return false;
    }
  }
  return true;
}

/** sort vector of elements of v (sorry, only bubble sort)
    but also return the new rank order as Vec<unsigned int>
    also sorts element of w ACCORDING TO ORDER OF v !
*/
/*
template <class T, class S>
inline
Vec<unsigned int>
parallelSort(Vec<T>& v, Vec<S>& w)
{
  return parallelSort2(v, w); // overwrite with faster version!
    Vec<unsigned int> order(v.size());
    for (unsigned int i = 0; i < order.size(); ++i) {
    order[i] = i;
    }
    bool sorted = false;
    while (!sorted) {
    sorted = true;
    for (unsigned int i = 1; i < v.size(); ++i) {
    if (v[i] < v[i-1]) {
    swap(v[i], v[i-1]);
    swap(order[i], order[i-1]);
    swap(w[i], w[i - 1]);
    sorted = false;
    }
    }
    }
    return order;
}
*/

/** sort vector of elements of v (sorry, only bubble sort)
    but also return the new rank order as Vec<unsigned int>
    also sorts element of w ACCORDING TO ORDER OF v !
*/
/*
template <class T, class S>
inline
Vec<unsigned int>
parallelUnaffectedSort(const Vec<T>& vOrig, Vec<S>& w)
{
  return parallelUnaffectedSort2(vOrig, w); // overwrite with faster version!
  Vec<T> v = vOrig;
  Vec<unsigned int> order(v.size());
  for (unsigned int i = 0; i < order.size(); ++i) {
    order[i] = i;
  }
  bool sorted = false;
  while (!sorted) {
    sorted = true;
    for (unsigned int i = 1; i < v.size(); ++i) {
      if (v[i] < v[i-1]) {
	swap(v[i], v[i-1]);
	swap(order[i], order[i-1]);
	swap(w[i], w[i - 1]);
	sorted = false;
      }
    }
  }
  return order;
}
*/

/** sort vector of elements (fast!)
    but also return the old rank order as Vec<unsigned int> 
*/
template <class T>
inline
Vec<unsigned int>
documentedSort(Vec<T>& v)
{
  PRECOND(v.size() > 0);
  // RankedSolution4<T,unsigned int> t;
  Vec<RankedSolution4<T, unsigned int> > order(v.size());
  for (unsigned int i = 0; i < order.size(); ++i) {
    order[i] = RankedSolution4<T,unsigned int>(v[i], i);
  }
  sort(order.begin(), order.end());
  Vec<unsigned int> resultOrder(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    resultOrder[i] = order[i].second;
    v[i] = order[i].first;
  }
  return resultOrder;
}

/** determines order of vector of elements (fast!) without actually sorting it
*/
template <class T>
inline
Vec<size_t>
documentedOrder(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  // RankedSolution4<T,unsigned int> t;
  Vec<RankedSolution4<T, unsigned int> > order(v.size());
  for (size_t i = 0; i < order.size(); ++i) {
    order[i] = RankedSolution4<T,unsigned int>(v[i], i);
  }
  sort(order.begin(), order.end());
  Vec<size_t> resultOrder(v.size());
  for (size_t i = 0; i < v.size(); ++i) {
    resultOrder[i] = order[i].second;
    // v[i] = order[i].first;
  }
  return resultOrder;
}

/** sort vector of elements of v 
    but also return the new rank order as Vec<unsigned int>
    also sorts element of w ACCORDING TO ORDER OF v !
*/
template <class T, class S>
inline
Vec<unsigned int>
parallelSort(Vec<T>& v, Vec<S>& w)
{
  PRECOND(v.size() == w.size());
  Vec<unsigned int> order = documentedSort(v);
  // Rcpp::Rcout << "The input list is: " << v << " and " << w << endl << "order: " << order << endl;
  Vec<S> wNew(w.size());
  for (unsigned int i = 0; i < order.size(); ++i) {
    wNew[i] = w[order[i]];
  }
  for (unsigned int i = 0; i < order.size(); ++i) {
    w[i] = wNew[i]; // FIXIT inefficient
  }
  // Rcpp::Rcout << "Sorted list: " << v << " parallel: " << w << endl;
  return order;
}

/** Returns vector, sorted by given order */
template <class T, class S>
inline
Vec<T>
sortByOrder(const Vec<T>& v, const Vec<S>& order) {
  PRECOND(v.size() == order.size());
  Vec<T> result = v;
  for (size_t i = 0; i < v.size(); ++i) {
    result[i] = v[order[i]];
  }
  return result;
}

/** Sorts vector of something with a sizes by size. Smallest first */
template<class S>
Vec<unsigned int>
sortBySize(Vec<S>& w) {
  Vec<unsigned int> sizes(w.size());
  for (size_t i = 0; i < w.size(); ++i) {
    sizes[i] = w[i].size();
  }
  return parallelSort(sizes, w);
}


/** sort vector of elements of v (sorry, only bubble sort)
    but also return the new rank order as Vec<unsigned int>
    also sorts element of w ACCORDING TO ORDER OF v !
*/
template <class T, class S>
inline
Vec<unsigned int>
parallelUnaffectedSort(const Vec<T>& vOrig, Vec<S>& w)
{
  PRECOND(vOrig.size() == w.size());
  Vec<T> v = vOrig;
  return parallelSort(v, w);
}

unsigned int findLargestDistanceIndex(const Vector3D& v, 
				      const Vec<Vector3D>& data);

unsigned  int findSmallestDistanceIndex(const Vector3D& v, 
					const Vec<Vector3D>& data);

/* find smallest distance point not belonging to forbidden set */
unsigned int findSmallestDistanceIndex(const Vector3D& v, 
				       const Vec<Vector3D>& data,
				       const Vec<unsigned int>& forbidden);

/* find n closest points */
Vec<unsigned int>
findSmallestDistanceIndices(const Vector3D& v, 
			    const Vec<Vector3D>& data,
			    unsigned int n);

/* find n closest points */
Vec<unsigned int>
findSmallestDistanceIndices(const Vector3D& v, 
			    const Vec<Vector3D>& data,
			    unsigned int n,
			    double minRadius,
			    double& maxRadius);

/* return distance in indices of pair of vectors with shortest distance */
double
findSmallestDistance(const Vec<Vector3D>& v1,
		     const Vec<Vector3D>& v2,
		     unsigned int& n1,
		     unsigned int& n2);

/** return point of m, which are closer than limitDist 
    to subset sub, but do not belong to sub 
*/
Vec<unsigned int>
getClosePoints(const Vec<unsigned int>& sub, const Vec<Vector3D>& m,
	       double limitDist);

/** return point of m2, which are closer than limitDist to m1
*/
Vec<unsigned int>
getClosePoints(const Vec<Vector3D>& m1, const Vec<Vector3D>& m2,
	       double limitDist);

unsigned int
centralPoint(const Vec<Vector3D>& choose, const Vec<Vector3D>& data,
	     double& radius);


/** return minimum element of vector */
template <class T>
inline
T findMin(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  T best = v[0];
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] < best) {
      best = v[i];
    }
  }
  return best;
}

/** return index of minimum element of vector */
template <class T>
inline
unsigned int
findMinIndex(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  unsigned int iBest = 0;
  T best = v[iBest];
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] < best) {
      best = v[i];
      iBest = i;
    }
  }
  POSTCOND(iBest < v.size());
  return iBest;
}

/** return index of maximum element of vector */
template <class T>
inline
unsigned int
findMaxIndex(const Vec<T>& v)
{
  PRECOND(v.size() > 0);
  unsigned int iBest = 0;
  T best = v[iBest];
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] > best) {
      best = v[i];
      iBest = i;
    }
  }
  POSTCOND(iBest < v.size());
  return iBest;
}

/** return index of maximum element of vector */
template <class T>
inline
void
findMaxIndices(const Vec<Vec<T> >& v, unsigned int& i1, unsigned int& i2)
{
  PRECOND((v.size() > 0)&&(v[0].size()>0));
  i1 = 0;
  i2 = 0;
  T best = v[i1][i2];
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      if (v[i][j] > best) {
	best = v[i][j];
	i1 = i;
	i2 = j;
      }
    }
  }
}

/** return indeces of elements equal to elem */
template <class T>
inline
Vec<unsigned int>
findEqualIndexSet(const Vec<T>& v, const T& elem)
{
  Vec<unsigned int> result;
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] == elem) {
      result.push_back(i);
    }
  }
  return result;
}

/** return first index of elements equal to elem */
template <class T>
inline
unsigned int
findFirstIndex(const Vec<T>& v, const T& elem)
{
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] == elem) {
      return i;
    }
  }
  return v.size();
}

// find index, with value closes to x
inline
unsigned int
findClosestIndex(const Vec<double>& v, double x)
{
  PRECOND(v.size() > 0);
  unsigned int idxBest = 0;
  double bestVal = fabs(v[0] - x);
  for (unsigned int i = 1; i < v.size(); ++i) {
    double d = fabs(v[i] - x); 
    if (d < bestVal) {
      idxBest = i;
      bestVal = d; 
    }
  }
  return idxBest;
}
 
/** 
 * add element to vector if it is not yet part of the vector
 */
template<class T>
inline
void
push_back_ifuniq(Vec<T>& v, const T& elem) {
  unsigned int n = findFirstIndex(v, elem);
  if (n >= v.size()) {
    v.push_back(elem);
  }
}

/** 
 * add element to vector if it is not yet part of the vector
 */
template<class T>
inline
void
push_front(Vec<T>& v, const T& elem) {
  v.push_back(elem); // just for allocating space for new element
  if (v.size() == 1) {
    return;
  }
  // shift all elements to one higher index:
  for (unsigned int i = v.size() - 1; i > 0; --i) {
    v[i] = v[i - 1];
  } 
  // now set first element to "elem":
  v[0] = elem;
}

/** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
    moment of inertia */
/*
void
computeGyrationGeometry(const Vec<Vector3D>& v,
			const Vec<double> weights,
			Vec<double>& eigenValues,
			Vec<Vec<double> >& eigenVectors);
*/

/* return radius of gyration of set of vectors */
double
computeRadiusOfGyration(const Vec<Vector3D>& v);

Vec<Vec<double> >
translateMatrix3DToVec(const Matrix3D& m);

/** internal amino acid counting starts from zero, external from ONE */
template <class T>
void
convert2InternalCounting(Vec<T>& v)
{
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v[i] > 0) {
      v[i] = v[i] - 1;
    }
    else {
      v[i] = 9999;
    }
  }
}

/** internal amino acid counting starts from zero, external from ONE */
template <class T>
void
convert2ExternalCounting(Vec<T>& v)
{
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = v[i] + 1;
  }
}

// convert internal residues counting (starting from zero)
// to external counting (starting from one)
template <class T>
Vec<T>
externalCounting(const Vec<T>& residues)
{
  Vec<T> result(residues.size());
  for (unsigned int i = 0; i < residues.size(); ++i) {
    result[i] = residues[i] + 1;
  }
  return result;
}

/* generate set of n points with radius r around origin vector, on a ring defined by theta */
Vec<Vector3D>
generateRingPoints(unsigned int n, double r, double theta, const Vector3D& origin);

/* generate set of n points with radius r around origin vector on a ring defined by theta */
Vec<Vector3D>
generateRingPoints(unsigned int n, double r, double theta, const Vector3D& origin, 
		   Vector3D direction);

/** generate set of n points with radius r around origin vector */
Vec<Vector3D>
generateSpherePoints(unsigned int n, double r, const Vector3D& origin);

/* generate surface points of point set */
void
generateSurfacePoints(Vec<Vector3D>& mol, const Vec<Vector3D>& molProt, 
		      double r, unsigned int tessel, bool rawMode);

/* generate surface points of point set
   result: vector of size molProt, each subvector
   containing surface vectors belonging to that atom */
Vec<Vec<Vector3D> >
generateSurfacePoints(const Vec<Vector3D>& molProt, 
		      double r, unsigned int tessel, bool rawMode);

/* generate surface points of point set */
void
generateSurfacePoints(Vec<Vector3D>& mol, Vec<int>& newIdVec,
      const Vec<Vector3D>& molProt, const Vec<int>& idVec, 
      double r, unsigned int tessel, bool rawMode);

/* generate surface points of point set */
void
generateSurfacePoints(Vec<Vector3D>& mol,
     Vec<int>& newIdVec, Vec<Vector3D>& directions,
     const Vec<Vector3D>& molProt, 
     const Vec<int>& idVec,
		      double r, unsigned int tessel, bool rawMode);

/* generate surface points of point set */
void
generateSurfacePointsAccessibleSurface(Vec<Vector3D>& mol,
     Vec<int>& newIdVec, Vec<Vector3D>& directions,
     Vec<double>& accessSurface,				       
     const Vec<Vector3D>& molProt, 
     const Vec<int>& idVec,
     double r, unsigned int tessel, bool rawMode);

/* generate compute accessible surface for point set */
void
computeAccessibleSurface(const Vec<Vector3D>& molProt, 
			  Vec<double>& accessSurface,				       
			 double r, unsigned int tessel);

/* generate simplex of n+1 dimensions */
Vec<Vec<double> > generateSimplex(const Vec<double>& v, double step);

/* generate simplex of n+1 dimensions */
Vec<Vec<double> > generateSimplex(const Vec<double>& v, const Vec<double>& stepSizes);

inline
bool
isInBox(const Vector3D& p, const Vector3D& minVec, const Vector3D& maxVec)
{
  return ( (p.x() >= minVec.x()) && (p.x() <= maxVec.x()) 
	   && (p.y() >= minVec.y()) && (p.y() <= maxVec.y()) 
	   && (p.z() >= minVec.z()) && (p.z() <= maxVec.z()) );
}



// get k nearest neighours of query vector closer than maxDist 
Vec<unsigned int> getKNearestNeighbours(const Vec<Vector3D>& v, 
		unsigned int query, unsigned int maxN, double maxDist);

// get k nearest neighours of query vector closer than maxDist 
Vec<Vec<unsigned int> >
getKNearestNeighbours(const Vec<Vector3D>& v, unsigned int maxN,
		      double maxDist);

/* return non-linear weighting of value x */
double nonLinearWeighting(double x, double maxY, double x0, double scale, 
			  double skew = 0.0);

/* return 2 step non-linear weighting of value x */
double
nonLinearWeightingDouble(double x, double maxY1, double x01, double scale1, 
			 double maxY2, double x02, double scale2);

/* expects N SORTED x-values and N+1 y values defining a step function. */
double
stepFunction(double x, const Vec<double>& xVec, const Vec<double>& yVec);

/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double computeMathews(const Vec<double>& scores, 
    const Vec<unsigned int>& trueClass, unsigned int classNum, double cutOff, 
    unsigned int& truePos, unsigned int& falsePos, 
    unsigned int& trueNeg, unsigned int& falseNeg);

/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double
computeMathews(	unsigned int truePos, unsigned int falsePos, 
		unsigned int trueNeg, unsigned int falseNeg);


/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double
computeMathews(const Vec<Vec<double> > & scores, 
	       const Vec<Vec<unsigned int> > & trueClass, 
	       unsigned int classNum, 
	       double cutOff,
	       unsigned int& truePos, 
	       unsigned int& falsePos, 
	       unsigned int& trueNeg, 
	       unsigned int& falseNeg);

/** write info about distribution */
template<class T>
inline
void
distributionInfo(ostream& os, const Vec<T>& v)
{
  if (v.size() == 0) {
    Rcpp::Rcout << "Vector is empty." << endl;
  }
  else {
    Rcpp::Rcout << "Elements: " << v.size() << " mean: " 
	 << vecMean(v) << " std dev: " << sqrt(vecVariance(v))
	 << " std dev(mean): " << sqrt(vecVariance(v)/v.size())
	 << " min: " << *min_element(v.begin(), v.end())
	 << " max: " << *max_element(v.begin(), v.end()) << endl;
  }
}

/** return lengths of 2D array */
template<class T>
inline
Vec<unsigned int>
getLengths(const Vec<Vec<T> >& field)
{
  Vec<unsigned int> result(field.size());
  for (unsigned int i = 0; i < field.size(); ++i) {
    result[i] = field[i].size();
  }
  return result;
}

/** return 1D array from 2D array */
template<class T>
inline
Vec<T>
flatten(const Vec<Vec<T> >& field)
{
  // compute total length:
  unsigned int n = 0;
  for (unsigned int i = 0; i < field.size(); ++i) {
    n += field[i].size();
  }
  Vec<T> result(n);
  unsigned int count = 0;
  for (unsigned int i = 0; i < field.size(); ++i) {
    for (unsigned int j = 0; j < field[i].size(); ++j) {
THROWIFNOT(count < result.size(),"Assertion violation in L2301");
      result[count++] = field[i][j];
    }
  }
  return result;
}

/** return 1D array from 2D array 
 * @param idVec       : i'th element: stores from which row of the original structure the i'th element in result came
 * @param flatIndices : stores new flat indices or original structures
 * */
template<class T>
inline
Vec<T>
flatten(const Vec<Vec<T> >& field, Vec<unsigned int>& idVec,
        Vec<Vec<unsigned int> >& flatIndices)
{
  // compute total length:
  unsigned int n = 0;
  for (unsigned int i = 0; i < field.size(); ++i) {
    n += field[i].size();
  }
  Vec<T> result(n);
  idVec = Vec<unsigned int>(n, 0);
  flatIndices = Vec<Vec<unsigned int> >(field.size());
  unsigned int count = 0;
  for (unsigned int i = 0; i < field.size(); ++i) {
    flatIndices[i] = Vec<unsigned int>(field[i].size(), 0U);
    for (unsigned int j = 0; j < field[i].size(); ++j) {
THROWIFNOT(count < result.size(),"Assertion violation in L2330");
      idVec[count] = i; // store id from which this element originated
      flatIndices[i][j] = count; // store new index
      result[count++] = field[i][j];
    }
  }
  POSTCOND(idVec.size() == result.size());
  POSTCOND(flatIndices.size() == field.size());
  return result;
}

/** return cut array of size n */
template<class T>
inline
Vec<T>
vectorCut(const Vec<T>& v,  unsigned int n)
{
  // Rcpp::Rcout << "Starting vectorCut!" << endl;
  if (n >= v.size()) {
    return v;
  }
  if (n == 0) {
    return Vec<T>();
  }
  Vec<T> result(n);
  for (unsigned int i = 0; i < n; ++i) {
    result[i] = v[i];
  }
  // Rcpp::Rcout <<  " ending vector cut!" <<endl;
  return result;
}

/** return number of elements */
template <class T>
inline
unsigned int
vecCount(const Vec<T>& v, const T& x, unsigned int a, unsigned int b)
{
  PRECOND(b <= v.size());
  unsigned int count = 0;
  for (unsigned int i = a; i < b; ++i) {
    if (v[i] == x) {
      ++count;
    }
  }
  return count;
}

/** return number of elements */
template <class T>
inline
unsigned int
matrixCount(const Vec<Vec<T> >& v, const T& x) {

  unsigned int count = 0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      if (v[i][j] == x) {
	++count;
      }
    }
  }
  return count;
}

/* my version of "winner takes most" ranking */
double
computeWinnerTakesMostScore(const Vec<double>& vOrig, double maxVal);

/** output of histogram x axis */
void
outBins(ostream& os, double min, double delta, unsigned int numBins);

/** Writes matrix to output stream as table of row, column, value for positive elements */
template<class T>
inline
void
writePositiveMatrixElements(ostream& os, const Vec<Vec<T> > & matrix, bool isSymmetric, double cutoff=0.0) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix.size(); ++i) {
    Vec<Vec<double> >::size_type jStart = 0;
    if (isSymmetric) {
      jStart = i;
    }
    for (Vec<Vec<double> >::size_type j = jStart; j < matrix[i].size(); ++j) {
      if (matrix[i][j] > cutoff) {
	os << (i+1) << " " << (j+1) << " " << matrix[i][j] << endl;
      }
    }
  }
}

/** Writes matrix to output stream as rectangular space-separated block */
template<class T>
inline
void
writeMatrix(ostream& os, const Vec<Vec<T> > & matrix) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix.size(); ++i) {
    for (Vec<Vec<double> >::size_type j = 0; j < matrix[i].size(); ++j) {
      os << matrix[i][j];
      if ((j + 1) < matrix[i].size()) {
	os << " ";
      } else {
	os << endl;
      }
    }
  }
}

/** Writes matrix to output stream as rectangular space-separated block */
template<class T>
inline
void
writeMatrix(ostream& os, const Vec<Vec<T> > & matrix, const string& prefix) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix.size(); ++i) {
    os << prefix << " ";
    for (Vec<Vec<double> >::size_type j = 0; j < matrix[i].size(); ++j) {
      os << matrix[i][j];
      if ((j + 1) < matrix[i].size()) {
	os << " ";
      } else {
	os << endl;
      }
    }
  }
}

// /** Writes matrix to output stream as rectangular space-separated block */
// template<class T>
// inline
// void
// writeMatrix(ostream& os, const concurrent_vector<concurrent_vector<T> > & matrix) {
//   for (concurrent_vector<concurrent_vector<double> >::size_type i = 0; i < matrix.size(); ++i) {
//     for (concurrent_vector<concurrent_vector<double> >::size_type j = 0; j < matrix[i].size(); ++j) {
//       os << matrix[i][j];
//       if ((j + 1) < matrix[i].size()) {
// 	os << " ";
//       } else {
// 	os << endl;
//       }
//     }
//   }
// }


/* after computation the values between the two indices are
   interpolated */
void
vecInterpolate(Vec<double>& v, unsigned int startidx, unsigned int endidx);

/* fill in missing values if borders by defined values.
   undefined is every v[i] with fabs(v[i] - empty) <= diff.
*/
void vecInterpolateMissing(Vec<double>& v, double empty, double diff);

template<class T>
inline
T
power(const T& a, const T& b)
{
  PRECOND(b >= 0);
  T result = 1;
  for (T i = 0; i < b; ++i) {
    result *= a;
  }
  return result;
}

// compute convolution of vector v with gauss Kernel exp(-x*x/d*d)
Vec<double>
smoothGaussian(const Vec<double>& v, double d);

Vec<double>
smoothGaussian(const Vec<double>& v, double d, unsigned int nMax);

void
smoothGaussian(Vec<Vec<Vec<double> > >& cube, double d, unsigned int nMax);

// increase number to a certain basis.
void
incBaseNumber(unsigned int& n, unsigned int base, bool& overflow);

// increase number to a certain basis. v[0] corresponds to lowest digit!
void
incBaseNumber(Vec<unsigned int>& v, unsigned int base);

// return maximum number which can be expressed by number with so many digits 
// to a certain base
unsigned int
maxBaseNumber(unsigned int digits, unsigned int base);

template <class T>
inline
bool
isSorted(const Vec<T>& v, bool descending=false) {
  for (size_t i = 1; i < v.size(); ++i) {
    if (!descending) {
      if (v[i] > v[i-1]) {
	Rcpp::Rcout << "Error at element: " << (i-1) << " and " << i << " : " << v[i-1] << " is larger than " << v[i] << endl;
	return false;
      }
    } else {
      if (v[i] < v[i-1]) {
	Rcpp::Rcout << "Error at element: " << (i-1) << " and " << i << " : " << v[i-1] << " is smaller than " << v[i] << endl;
	return false;
      }
    }
  }
  return true;
}

// sorts such that least populated columns are in front
template<class T>
void
sortBySize(Vec<Vec<T> >& m) {
  Vec<unsigned int> sizes(m.size());
  for (unsigned int i = 0; i < m.size(); ++i) {
    sizes[i] = m[i].size();
  }
  parallelSort(sizes, m);
#ifndef NDEBUG
  for (unsigned int i = 1; i < m.size(); ++i) {
    if (m[i].size() < m[i-1].size()) {
      Rcpp::Rcout << "Internal error: " << endl << sizes << endl << m << endl;
      DERROR("Internal error in sortBySize!");
    }
  }
#endif
}

/** makes matrix more similar to distance matrix: positive definite,
    and zero in diagonals */
void
distify(Vec<Vec<double> >& m);

inline
int
computeAntiDiagonalId(int nx, int ny, int sz) 
{
  int szm = sz - 1;
  while ((nx > 0) && (ny < szm)) {
    --nx;
    ++ny;
  }
  if (nx == 0) {
    return ny;
  }
  return sz + nx - 1;
}

/** n: id of antidiagonal, nx, ny: left hand start points */
inline
void
computeStartFromAntiDiagonalId(int n, int& nx, int& ny, int sz) 
{
  if (n < sz) {
    nx = 0;
    ny =  n;
  }
  else {
    nx = n + 1 - sz;
    ny =  sz - 1;
  }
}


/** get anti diagonal slice including element
  n < 2*size - 1
  n counts left left hand index if smaller of than size or
  the upper bar if larger than size
*/
template <class T>
inline
void
setAntiDiagonal(Vec<Vec<T> >& mat, const Vec<T>& diag, int n)
{
  int sz = mat.size(); 
  int x, y;
  ERROR_IF((n < 0) || (n >= ((2*sz)-1)),
	   "Illegal diagonal index!");
  computeStartFromAntiDiagonalId(n, x, y, sz);
  int len = diag.size();
  for (int i = 0; i < len; ++i) {
    ASSERT((x < static_cast<int>(mat.size())) 
	   && (y < static_cast<int>(mat[x].size())));
//     Rcpp::Rcout << "Setting " << x << " " << y << " from " 
// 	 << mat[x][y] << " to " << diag[i] << endl;
    mat[x++][y--] = diag[i];

  }
}

/** get anti diagonal slice including element
  n < 2*size - 1
  n counts left left hand index if smaller of than size or
  the upper bar if larger than size
*/
template <class T>
inline
Vec<T>
getAntiDiagonal(const Vec<Vec<T> >& mat, int n)
{
  int sz = mat.size(); 
  if ((n < 0) || (n >= ((2*sz)-1))) {
    return Vec<T>();
  }
  int len = sz - abs(static_cast<int>(sz - 1 - n));
  if (len == 0) {
    return Vec<T>();
  }
  Vec<T> result(len, 0.0);
  int srow, scol;
  if (n < sz) {
    srow =  n;
    scol = 0;
  }
  else {
    srow =  sz - 1;
    scol = n + 1 - sz;
  }
  int row = srow;
  int col = scol;
  for (int i = 0; i < len; ++i) {
THROWIFNOT((row < static_cast<int>(mat.size())) && (col < static_cast<int>(mat[row].size())),"Assertion violation in L2652");
    result[i] = mat[row--][col++];
  }
  return result;
}

/** get anti diagonal slice including element nx,ny
  n < 2*size - 1
*/
template <class T>
inline
Vec<T>
getAntiDiagonal(const Vec<Vec<T> >& mat, int nx, int ny)
{
  return getAntiDiagonal(mat, computeAntiDiagonalId(nx, ny, mat.size()));
}

/** get anti diagonal slice including element n,n
    counting: n varies between -size+1 ... to size -1
    0 corresponds to main diagonal. 
    n < 0 corresponds to row 0, col (-n),
    n > 0 corresponds to row n, col (0),
*/
template <class T>
inline
Vec<T>
getDiagonal(const Vec<Vec<T> >& mat, int n)
{
  int sz = mat.size(); 
  if ((n <= -sz) || (n >= sz)) {
    return Vec<T>();
  }
  int srow, scol;
  if (n < 0) {
    srow =  0;
    scol = -n;
  }
  else {
    srow =  n;
    scol = 0;
  }
  int len = sz - (scol + srow);
  if (len == 0) {
    return Vec<T>();
  }
  Vec<T> result(len, 0.0);
  int row = srow;
  int col = scol;
  for (int i = 0; i < len; ++i) {
    ASSERT((row < static_cast<int>(mat.size())) 
	   && (col < static_cast<int>(mat[row].size())));
    result[i] = mat[row++][col++];
  }
  return result;
}

/** returns index n,n from point m,n */
double
diagonalProjection(int row, int col, int size);

/** returns row index of projection to antidiagonal */
double
antiDiagonalProjection(int row, int col, int size);

Vec<double>
getAntiDiagonalAverages(const Vec<Vec<double> >& mat);

Vec<double>
getDiagonalAverages(const Vec<Vec<double> >& mat);

double
correlationCoefficient(const Vec<double>& x, const Vec<double>& y);

/** in each row, leave only the highest scoring element,
 also delete diagonal */
void
winnerTakesAll(Vec<Vec<double> >& matrix, int diagBorder, double cutoff);

/** sets all elements i,i to i,i+diagBorder */
void
fillDiagonal(Vec<Vec<double> >& matrix, int diagBorder, double value);


/** n trials with counts.size() possible outcomes are performed.
 * Given a vector of probabilities of observing each outcome,
 * it returns the probability of observing a particular patterns of counts of outcomes.
 * Corresponds to trials "with replacement"; use hypergeometric distribution if "without replacement"
 */
double
logMultinomial(const Vec<int>& counts, const Vec<double>& probs, int n) {
  double result = logFactorial(n);
  for (Vec<double>::size_type i = 0; i < probs.size(); ++i) {
    result += log(pow(probs[i], counts[i])) - logFactorial(counts[i]);
  }
  return result;
}

// return error function for positive values of z
// approximation according to Abramowitz and Stegun, 1964 
// (http://www.geo.vu.nl/users/hydro/students/hd537/error_function.pdf)
/*
double
errorFunction(double z)
{
  const double a1 = 0.254829592;
  const double a2 = -0.284496736;
  const double a3 = 1.421413741;
  const double a4 = -1.453152027;
  const double a5 = 1.061405429;
  const double a6 = 0.3275911;
  double b = 1.0 / (1.0 + a6 * z);
  double result = 1.0 - (a1 * b + a2*b*b + a3*b*b*b + a4 *b*b*b*b + a5 * pow(b, 5.0)) * exp(-b*b);
  POSTCOND((result >= 0.0) && (result <= 1.0));
  return result;
}
*/

/** error function.
    from: http://dehesa.freeshell.org/NCSE/12/error_f.f
*/
double
errorFunction(double x)
{
  double t = 1.0/(1.0 + 0.3275911*fabs(x));    
  double erfun_c = t * exp(-(x*x))*(0.254829592 + t*(-0.284496736 +
	     + t *( 1.421413741 + t * (-1.453152027 + t*1.061405429 ) ) ) );  
  if(x < 0.0) {
    erfun_c = 2.0-erfun_c;
  }
  return 1.0 - erfun_c;
}

Vec<int>
uivtoiv(const Vec<unsigned int>& v) 
{
  Vec<int> result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    result[i] = static_cast<int>(v[i]);
  }
  return result;
}

Vec<unsigned int>
ivtouiv(const Vec<int>& v) 
{
  Vec<unsigned int> result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    result[i] = static_cast<unsigned int>(v[i]);
  }
  return result;
}

double
varianceFromSum(double sum, double sumSquares, unsigned int n)
{
  if (n < 2) {
    return 0.0;
  }
  double nDub = static_cast<double>(n);
  double result = (nDub/(nDub-1.0)) * ((sumSquares/n) - ((sum * sum) / (n * n)));
  if (result < 0.0) {
    result = 0.0;
  }
  return result;
} 


/* my version of "winner takes most" ranking */
double
computeWinnerTakesMostScore(const Vec<double>& vOrig, double maxVal)
{
  PRECOND(maxVal > 0.0);
  Vec<double> v = vOrig;
  sort(v.begin(), v.end());
  reverse(v.begin(), v.end());
  double result = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (result >= maxVal) {
      break;
    }
    double restFrac = (maxVal - result) / maxVal;
    result += v[i] * restFrac;
  }
  return result;
}

/** output of histogram x axis */
void
outBins(ostream& os, double min, double delta, unsigned int numBins)
{
  for (unsigned int i = 0; i < numBins; ++i) {
    os << i << "\t";
  }
  os << endl;
  for (unsigned int i = 0; i < numBins; ++i) {
    os << min + i * delta << "\t";
  }
  os << endl;
}

// generate array starting with 0 , incresing like a stair
Vec<unsigned int>
generateStair(unsigned int maxi, unsigned int mini, unsigned int step)
{
  PRECOND(step > 0);
  if (maxi <= mini) {
    return Vec<unsigned int>(); // return "empty" stair
  }
  unsigned int d = (maxi -mini)/step;
  Vec<unsigned int> result(d);
  for (unsigned int i = 0; i < d; ++i) {
    result[i] = (i*step) + mini;
THROWIFNOT(result[i] < maxi,"Assertion violation in L2864");
  }
  return result;
}

/**
 * generates loopup table: i'th element of result is position of value of i in v
 */
Vec<unsigned int>
getIndexSet(const Vec<unsigned int>& v) {
  // find maximum index:
  unsigned int maxSize = v[findMaxIndex(v)] + 1;
  Vec<unsigned int> result(maxSize, 0U);
  for (unsigned int i = 0; i < v.size(); ++i) {
    result[v[i]] = i;
  }
  return result;
}

/* return vector with size numReturn, which has randomly assigned non-redundant bin values between min and max */
Vec<double>
rouletteWheel(double min, double max, unsigned int numBins, unsigned int numReturn)
{
  PRECOND(numBins > 0);
  Vec<double> result(numReturn);
  Vec<double> stair(numBins);
  Random& rnd = Random::getInstance();
  double step = (max - min) / numBins;
  for (unsigned int i = 0; i < numBins; ++i) {
    stair[i] = min + step * i;
  }
  random_shuffle(stair.begin(), stair.end(), rnd);
  for (unsigned int i = 0; i < numReturn; ++i) {
    result[i] = stair[i];
  }
  POSTCOND(result.size() == numReturn);
  return result;
}

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 */
unsigned int
chooseRouletteWheel(const Vec<double>& pVec)
{
  PRECOND(pVec.size() > 0);
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = elementSum(pVec);
  Random& rnd = Random::getInstance();
  double y = rnd.getRandf() * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L2921");
    if ((y >= sum) && (y <= (sum + pVec[i]))) {
      result = i;
      found = true;
      break;
    }
    sum += pVec[i];
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  return result; // dummy result
}

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel(const Vec<double>& pVec, const Vec<unsigned int>& tabooList)
{
  PRECOND(pVec.size() > 0);
  PRECOND(tabooList.size() < pVec.size());
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = 0.0;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    bool found = false; // check taboo list:
    if (findFirstIndex(tabooList, i) < tabooList.size()) {
	found = true;
    }
    if (!found) {
      totalSum += pVec[i];
    }
  }
  Random& rnd = Random::getInstance();
  double y = rnd.getRandf() * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L2963");
    found = false; // check taboo list:
    if (findFirstIndex(tabooList, i) < tabooList.size()) {
      found = true;
THROWIFNOT(tabooList.size() > 0,"Assertion violation in L2967");
    }
    if (! found) {
      if ((y >= sum) && (y <= (sum + pVec[i]))) {
	result = i;
	found = true;
	break;
      }
      sum += pVec[i];
    }
  }
  if (!found) {
    Rcpp::Rcout << "chooseRouletteWheel called with: " << y << " " << pVec << endl << tabooList << endl;
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result; // dummy result
}

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 */
unsigned int
chooseRouletteWheel2(const Vec<double>& pVec, const Vec<unsigned int>& tabooList)
{
  PRECOND(pVec.size() > 0);
  PRECOND(tabooList.size() < pVec.size());
  if (pVec.size() == 1) {
    return 0;
  }
  if (tabooList.size() == 0) {
    return chooseRouletteWheel(pVec);
  }
  double totalSum = 0.0;
  Vec<unsigned int> indexList;
  Vec<double> newP;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    bool found = false; // check taboo list:
    if (findFirstIndex(tabooList, i) < tabooList.size()) {
	found = true;
    }
    if (!found) {
      totalSum += pVec[i];
      indexList.push_back(i);
      newP.push_back(pVec[i]); // new pie without taboo elements
    }
  }
THROWIFNOT(newP.size() > 0,"Assertion violation in L3017");
  unsigned int result = chooseRouletteWheel(newP);
THROWIFNOT(result < indexList.size(),"Assertion violation in L3019");
  result = indexList[result]; // translate to old indices
  POSTCOND(result < pVec.size());
  POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result; // dummy result
}

/**
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 * Fast implementation based on set
 */
unsigned int
chooseRouletteWheel3(const Vec<double>& pVec, const set<unsigned int>& tabooList)
{
  PRECOND(pVec.size() > 0);
  PRECOND(tabooList.size() < pVec.size());
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = 0.0;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    bool found = false; // check taboo list:
    if (tabooList.find(i) != tabooList.end()) {
      found = true;
    }
    if (!found) {
      totalSum += pVec[i];
    }
  }
  Random& rnd = Random::getInstance();
  double y = rnd.getRandf() * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L3056");
    found = false; // check taboo list:
    if (tabooList.find(i) != tabooList.end()) {
      found = true;
    }
    if (! found) {
      if ((y >= sum) && (y <= (sum + pVec[i]))) {
	result = i;
	found = true;
	break;
      }
      sum += pVec[i];
    }
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  // POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result;
}

/**
 * Uses thread-safe version of rand_r for random number generation
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 * Fast implementation based on set
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, const set<unsigned int>& tabooList, unsigned int * seed)
{
  PRECOND(pVec.size() > 0);
  PRECOND(tabooList.size() < pVec.size());
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = 0.0;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    bool found = false; // check taboo list:
    if (tabooList.find(i) != tabooList.end()) {
      found = true;
    }
    if (!found) {
      totalSum += pVec[i];
    }
  }
  // Random& rnd = Random::getInstance();
  // double y = rnd.getRandf() * totalSum;
  double rf = rand_r(seed) / static_cast<double>(RAND_MAX);
THROWIFNOT(rf >= 0.0,"Assertion violation in L3104");
THROWIFNOT(rf <= 1.0,"Assertion violation in L3105");
  double y = rf * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L3111");
    found = false; // check taboo list:
    if (tabooList.find(i) != tabooList.end()) {
      found = true;
    }
    if (! found) {
      if ((y >= sum) && (y <= (sum + pVec[i]))) {
	result = i;
	found = true;
	break;
      }
      sum += pVec[i];
    }
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  // POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result;
}

/**
 * Uses thread-safe version of rand_r for random number generation
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 * Fast implementation based on set
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, const Vec<unsigned int>& tabooList, unsigned int * seed)
{
  PRECOND(pVec.size() > 0);
  PRECOND(tabooList.size() < pVec.size());
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = 0.0;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    bool found = false; // check taboo list:
    if (findFirstIndex(tabooList, i) < tabooList.size()) {
      found = true;
    }
    if (!found) {
      totalSum += pVec[i];
    }
  }
  // Random& rnd = Random::getInstance();
  // double y = rnd.getRandf() * totalSum;
  double rf = rand_r(seed) / static_cast<double>(RAND_MAX);
THROWIFNOT(rf >= 0.0,"Assertion violation in L3159");
THROWIFNOT(rf <= 1.0,"Assertion violation in L3160");
  double y = rf * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L3166");
    found = false; // check taboo list:

    if (findFirstIndex(tabooList, i) < tabooList.size()) {
      found = true;
    }
    if (! found) {
      if ((y >= sum) && (y <= (sum + pVec[i]))) {
	result = i;
	found = true;
	break;
      }
      sum += pVec[i];
    }
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  // POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result;
}


/**
 * Uses thread-safe version of rand_r for random number generation
 * chooses an element indix from vector pVec with probability given in pVec
 * imagine probabilities like pieces of "pie" 
 * given is also a list of taboo indices.
 * Fast implementation based on set
 */
unsigned int
chooseRouletteWheel4(const Vec<double>& pVec, unsigned int * seed)
{
  PRECOND(pVec.size() > 0);
  if (pVec.size() == 1) {
    return 0;
  }
  double totalSum = 0.0;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
    totalSum += pVec[i];
  }
  // Random& rnd = Random::getInstance();
  // double y = rnd.getRandf() * totalSum;
  double rf = rand_r(seed) / static_cast<double>(RAND_MAX);
THROWIFNOT(rf >= 0.0,"Assertion violation in L3209");
THROWIFNOT(rf <= 1.0,"Assertion violation in L3210");
  double y = rf * totalSum;
  double sum = 0.0;
  unsigned int result = 0;
  bool found = false;
  for (unsigned int i = 0; i < pVec.size(); ++i) {
THROWIFNOT(pVec[i] >= 0.0,"Assertion violation in L3216");
    if ((y >= sum) && (y <= (sum + pVec[i]))) {
      result = i;
      found = true;
      break;
    }
    sum += pVec[i];
  }
  ERROR_IF(!found, "Internal error in chooseRouletteWheel!"); // should never be here!
  POSTCOND(result < pVec.size());
  // POSTCOND(findFirstIndex(tabooList, result) >= tabooList.size());
  return result;
}



/**
 * generates vecor of unsigned int with num entries between minId and maxId
 * no indices can be double, so no "replacement" (see also: generateBootSubset)
 */
Vec<unsigned int>
generateRandomIndexSubset(unsigned int num,
                          unsigned int maxId, unsigned int minId) {
  Vec<unsigned int> stair = generateStair(maxId, minId);
  Random& rnd = Random::getInstance();
  random_shuffle(stair.begin(), stair.end(), rnd);
  Vec<unsigned int> result(num);
  for (unsigned int i = 0; i < num; ++i ) {
    result[i] = stair[i];
  }
  return result;
}

/**
 * generates vecor of unsigned int with num entries between 0 and num
 * no indices can be double, WITH "replacement", use for bootstrap!
 */
Vec<unsigned int>
generateBootSubset(unsigned int num) {
  if (num == 0) {
    return Vec<unsigned int>();
  }
  Random& rnd = Random::getInstance();
  Vec<unsigned int> result(num);
  for (unsigned int i = 0; i < num; ++i ) {
    result[i] = rnd.getRand(static_cast<long>(num));
  }
  return result;
}

/** return false if not a number (NaN) */
bool isDefined(const Vector3D& v) {
  return isDefined(v.x()) && isDefined(v.y()) && isDefined(v.z());
}

Vec<int>
uiVecToIVec(const Vec<unsigned int>& v)
{
  Vec<int> result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    result[i] = static_cast<int>(v[i]);
  }
  return result;
}

Vec<unsigned int>
iVecToUiVec(const Vec<int>& v)
{
  Vec<unsigned int> result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    ERROR_IF(v[i] < 0, 
      "Error converting unsigned int vector to int!");
    result[i] = static_cast<unsigned int>(v[i]);
  }
  return result;
}

void
unitMatrix(Vec<Vec<double> >& matrix)
{
  for (unsigned int i = 0; i < matrix.size(); ++i) {
    for (unsigned int j = 0; j < matrix[i].size(); ++j) {
      if (i != j) {
	matrix[i][j] = 0.0;
      }
      else {
	matrix[i][j] = 1.0;
      }
    }
  }
  POSTCOND(isReasonable(matrix));
}

/** a <- b a  */
void
scalarMul(Vec<double>& a, double b)
{
  for (unsigned int i = 0; i < a.size(); ++i) {
    a[i] = a[i] *b;
  }
}

/** c = a . b */
double
dotProduct(const Vec<double>& a,
	   const Vec<double>& b)
{
  PRECOND((a.size() == b.size()));
  double result = 0.0;
  for (unsigned int i = 0; i < a.size(); ++i) {
    result += a[i]*b[i];
  }
  return result;
}

/** return sum of elements */
double
elementSum(const Vec<double>& v) {
  double sum = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += v[i];
  }
  return sum;
}

// transform such that sum of values is 1.0
void
probabilityNormalize(Vec<double>& v) {
  double sum = elementSum(v);
  if (v.size() == 0) {
    return;
  }
  if (sum == 0.0) {
    double tmp = 1.0 / static_cast<double>(v.size());
    for (unsigned int i = 0; i < v.size(); ++i) {
      v[i] = tmp;
    }
    return;  // do nothing
  }
  ERROR_IF(sum < 0.0, "Vector not normalizable, sum negative.");
//   ERROR_IF(!isReasonable(sum), "Vector not normalizable, weird members.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] /= sum;
  }
}

// transform such that sum of values is targetSum
void
probabilityNormalize(Vec<double>& v, double targetSum) {
  PRECOND(targetSum > 0.0);
  double sum = elementSum(v);
  if (sum == 0.0) {
    return;  // do nothing
  }
  sum /= targetSum;
  //  ERROR_IF(sum < 0.0, "Vector not normalizable.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] /= sum;
  }
}

// transform such that sum of values is 1.0, must start from raw counts, add apriori probability
void
bayesianNormalize(Vec<double>& v) {
  double sum = elementSum(v) + v.size();
  //  ERROR_IF(sum < 0.0, "Vector not normalizable.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = (v[i] + 1.0)/sum;
  }
}


// transform such that sum of values is 1.0
void
probabilityNormalize(Vec<Vec<double> >& v) {
  double sum = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += elementSum(v[i]);
  }
  if (sum == 0.0) {
    return;  // do nothing
  }
  //  ERROR_IF(sum < 0.0, "Vector not normalizable.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      v[i][j] /= sum;
    }
  }
}

// transform such that sum of values is 1.0. Must start from raw counts (instead of x/n we use x+1/(n+c), c being number of classes)
void
bayesianNormalize(Vec<Vec<double> >& v) {
  double sum = v.size()*v.size();
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += elementSum(v[i]);
  }
  //  ERROR_IF(sum < 0.0, "Vector not normalizable.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    for (unsigned int j = 0; j < v[i].size(); ++j) {
      v[i][j] = (v[i][j] + 1.0) / sum;
    }
  }
}

// compute euclidian norm of vector
/*
double
euclidianNorm(const Vec<double>& v) {
  double sum = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += v[i] * v[i];
  }
  if (sum <= 0.0) {
    return 0.0;  // do nothing
  }
  sum = sqrt(sum);
  POSTCOND(sum >= 0.0);
  return sum;
}
*/

// transform such that sum of SQUARED values is 1.0
void
normalizeEuclidian(Vec<double>& v) {
  double sum = euclidianNorm(v);
  //  ERROR_IF(sum < 0.0, "Vector not normalizable.");
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] /= sum;
  }
}

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void
transformToZScores(Vec<double>& v)
{
  double mean = vecMean(v);
  double sigma = vecVariance(v); // variance
  if (sigma <= 0.0) {
    for (unsigned int i = 0; i < v.size(); ++i) {
      v[i] = 0.0;
    }
    return; // all components are identical
  }
  sigma = sqrt(sigma); // deviation
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = (v[i] - mean) / sigma;
  }
}

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void
transformToZScores(Vec<Vec<double> >& m)
{
  Vec<double> v = flatten(m);
  double mean = vecMean(v);
  double sigma = vecVariance(v); // variance
  if (sigma <= 0.0) {
    for (unsigned int i = 0; i < m.size(); ++i) {
      for (unsigned int j = 0; j < m[i].size(); ++j) {
	m[i][j] = 0.0;
      }
    }
    return; // all components are identical
  }
  sigma = sqrt(sigma); // deviation
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < m[i].size(); ++j) {
      m[i][j] = (m[i][j] - mean) / sigma;
    }
  }
}


/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
    use only subset indices for computing mean and std
*/
void
transformToZScores(Vec<double>& v,
		   const Vec<unsigned int>& subSet)
{
  PRECOND(subSet.size() > 0);
  double mean = vecMean(getSubset(v, subSet));
  double sigma = vecVariance(getSubset(v, subSet)); // variance
  if (sigma <= 0.0) {
    for (unsigned int i = 0; i < v.size(); ++i) {
      v[i] = 0.0;
    }
    return; // all components are identical
  }
  sigma = sqrt(sigma); // deviation
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = (v[i] - mean) / sigma;
  }
}


/** moves centroid of Vec <Vector3D> to ( 0, 0, 0)
  */
Vector3D moveCenter( Vec <Vector3D> &v1 )
{
  double xmid   = 0.0;
  double ymid   = 0.0;
  double zmid   = 0.0;
  double weight = 1.0;
  double norm   = 0.0;
  
  for (unsigned int i=0; i < v1.size(); i++ )
    { 
      xmid = xmid + v1[i].x()*weight;
      ymid = ymid + v1[i].y()*weight;
      zmid = zmid + v1[i].z()*weight;
      norm = norm + weight;
    };
  
  xmid = xmid / norm;
  ymid = ymid / norm;
  zmid = zmid / norm;
  
  for (unsigned int i=0; i < v1.size(); i++ )
    {
      v1[i].x( v1[i].x() - xmid );
      v1[i].y( v1[i].y() - ymid );
      v1[i].z( v1[i].z() - zmid );
    };
  return Vector3D(xmid, ymid, zmid);
}

/** compute optimal superposition of coordinates of
    molecules m1 and m2
    m2' = (m2-center(m2)) * rot + v
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1Orig, Vec<Vector3D>& v2, 
		Matrix3D& rot, Vector3D& v)
{
  PRECOND(v1Orig.size() == v2.size());
  Vec<Vector3D> v1 = v1Orig; // safety copy
  Vector3D c1 = moveCenter(v1);
  Vector3D c2 = moveCenter(v2);
  DERROR("Sorry, quatfit not supported anymore!");
  // rot = quatfit(v1,v2); 
  // shiftAtoms(v2, c1); // move to center of m1!
  vectorAdd(v2, c1, v2);
  v = c1 - c2; // translation which is performed on m2 // NOT SURE IF CORRECT!??
}


/** compute optimal superposition of coordinates of
    molecules m1 and m2
    m2' = rot * (m2-c2)  + c1
    @deprecated
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1Orig, Vec<Vector3D>& v2, 
		Matrix3D& rot, Vector3D& c1, Vector3D& c2)
{
  PRECOND(v1Orig.size() == v2.size());
  Vec<Vector3D> v1 = v1Orig; // safety copy
  c1 = moveCenter(v1);
  c2 = moveCenter(v2);
  DERROR("Sorry, quatfit not supported anymore!");
  // rot = quatfit(v1,v2); 
  // shiftAtoms(v2, c1); // move to center of m1!
  vectorAdd(v2, c1, v2);
  //    v = c1 - c2; // translation which is performed on m2
  //    oldCenter = c2;
}


/** compute optimal superposition of coordinates of
    molecules m1 and m2
*/
void
minRmsSuperpose(const Vec<Vector3D>& v1, Vec<Vector3D>& v2)
{
  PRECOND(v1.size() == v2.size());
  Vector3D v(0.0,0.0,0.0);
  Matrix3D m(1.0,1.0,1.0);
  minRmsSuperpose(v1, v2, m, v);
}



double
getMinDist(const Vector3D& v, const Vec<Vector3D>& c)
{
  double result = vecDistance(v,c[0]);
  for (unsigned int i = 1; i < c.size(); ++i) {
    double dist = vecDistance(c[i],v);
    if ( dist < result) {
      result = dist;
    }
  }
  return result;
}

Vec<unsigned int>
getConnected(const Vec<Vector3D> v, double dist, unsigned int start)
{
  PRECOND(start < v.size());
  Vec<unsigned int> result;
  Vec<unsigned int> touched(v.size(), 0U);
  result.push_back(start);
  unsigned int clustered = 1;
  touched[start] = 1;
  while (clustered < v.size()) {
    // get next best vector to existing cluster
    double bestDist = dist;
    unsigned int bestIndex = v.size();
    Vec<Vector3D> subs = getSubset(v,result);
    for (unsigned int i = 1; i < v.size(); ++i) {
      if (touched[i] == 0) {
	double d = getMinDist(v[i], subs);
	if (d < bestDist) {
	  bestDist = d;
	  bestIndex = i;
	}
      }
    }
    if (bestIndex >= v.size()) {
      return result; // nothing further found
    }
    result.push_back(bestIndex);
    touched[bestIndex] = 1;
    ++clustered;
  }
  return result; // all connected!
}


unsigned int findLargestDistanceIndex(const Vector3D& v, 
				       const Vec<Vector3D>& data)
{
  double bestDist = -1.0;
  unsigned int bestIndex = 0;
  for (unsigned int i = 0; i < data.size(); ++i) {
    double dist = vecDistance(v,data[i]);
    if (dist > bestDist) {
      bestDist = dist;
      bestIndex = i;
    }
  }
  return bestIndex;
}

unsigned int findSmallestDistanceIndex(const Vector3D& v, 
				       const Vec<Vector3D>& data)
{
  double bestDist = -1.0;
  unsigned int bestIndex = 0;
  for (unsigned int i = 0; i < data.size(); ++i) {
    double dist = vecDistance(v,data[i]);
    if ((bestDist < 0) || (dist < bestDist)) {
      bestDist = dist;
      bestIndex = i;
    }
  }
  return bestIndex;
}

/* find smallest distance point not belonging to forbidden set */
unsigned int findSmallestDistanceIndex(const Vector3D& v, 
       const Vec<Vector3D>& data, const Vec<unsigned int>& forbidden)
{
  double bestDist = -1.0;
  unsigned int bestIndex = 0;
  for (unsigned int i = 0; i < data.size(); ++i) {
    bool found = false;
    for (unsigned int j = 0; j < forbidden.size(); ++j) {
      if (i == forbidden[j]) {
	found = true;
	break;
      }
    }
    if (found) {
      continue; // ignore this value because in forbidden set
    }
    double dist = vecDistance(v,data[i]);
    if ((bestDist < 0) || (dist < bestDist)) {
      bestDist = dist;
      bestIndex = i;
    }
  }
  return bestIndex;
}

/* find n closes points */
Vec<unsigned int>
findSmallestDistanceIndices(const Vector3D& v, 
			    const Vec<Vector3D>& data,
			    unsigned int n)
{
  PRECOND(n <= data.size());
  Vec<unsigned int> result;
  while (result.size() < n) {
    result.push_back(findSmallestDistanceIndex(v, data, result));
  }
  return result;
}

/* find n closes points */
Vec<unsigned int>
findSmallestDistanceIndices(const Vector3D& v, 
			    const Vec<Vector3D>& data,
			    unsigned int n,
			    double minRadius,
			    double& maxRadius)
{
  PRECOND(n <= data.size());
  Vec<unsigned int> result;
  maxRadius = 0.0;
  while (result.size() < n) {
    unsigned int ind = findSmallestDistanceIndex(v, data, result);
    double d = vecDistance(v, data[ind]);
    if (d > minRadius) {
      continue;
    }
    if (d > maxRadius) {
      maxRadius = d;
    }
    result.push_back(ind);
  }
  return result;
}

void
modulateColumn(Vec<Vec<double> >& matrix,
	       const Vec<double>& modulateValues,
	       Vec<Vec<double> >::size_type colId) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix.size(); ++i) {
    matrix[i][colId] *= modulateValues[i];
  }

}

void
modulateRow(Vec<Vec<double> >& matrix,
	    const Vec<double>& modulateValues,
	    Vec<Vec<double> >::size_type rowId) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix[0].size(); ++i) {
    matrix[rowId][i] *= modulateValues[i];
  }

}

void
modulateColumns(Vec<Vec<double> >& matrix,
		const Vec<double>& modulateValues) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix[0].size(); ++i) {
    modulateColumn(matrix, modulateValues, i);
  }
}

void
modulateRows(Vec<Vec<double> >& matrix,
		const Vec<double>& modulateValues) {
  for (Vec<Vec<double> >::size_type i = 0; i < matrix.size(); ++i) {
    modulateRow(matrix, modulateValues, i);
  }
}

/** Multiplies all row and columns with vector entries */
void
modulateMatrix(Vec<Vec<double> >& matrix,
	       const Vec<double>& modulateValues) {
  modulateColumns(matrix, modulateValues);
  modulateRows(matrix, modulateValues);
}

unsigned int
centralPoint(const Vec<Vector3D>& choose, const Vec<Vector3D>& data,
	     double& radius)
{
  double bestDist = -1.0;
  unsigned int bestIndex = 0;
  for (unsigned int i = 0; i < choose.size(); ++i) {
    unsigned int largestGuy = findLargestDistanceIndex(choose[i], data);
THROWIFNOT(largestGuy < data.size(),"Assertion violation in L3802");
    double dist = vecDistance(choose[i], data[largestGuy]);
    if ((bestDist < 0) || (dist < bestDist)) {
      bestDist = dist;
      bestIndex = i;
    }
  }
  radius = bestDist;
  return bestIndex;
}

/** return point of m, which are closer than limitDist 
    to subset sub, but do not belong to sub 
*/
Vec<unsigned int>
getClosePoints(const Vec<unsigned int>& sub,
	       const Vec<Vector3D>& m,
	       double limitDist)
{
  Vec<unsigned int> result;
  for (unsigned int i = 0; i < sub.size(); ++i) {
THROWIFNOT(sub[i] < m.size(),"Assertion violation in L3823");
    for (unsigned int j = 0; j < m.size(); ++j) {
      if (sub[i] == j) {
	continue;
      }
      if (vecDistance(m[sub[i]], m[j]) <= limitDist) {
	// check if not member of sub:
	bool found = false;
	for (unsigned int k = 0; k < sub.size(); ++k) {
	  if (j == sub[k]) {
	    found = true;
	    break;
	  }
	}
	if (!found) {
	  result.push_back(j);
	}
      }
    }    
  }
  result = uniqueSet(result);
  POSTCOND(result.size() <= m.size());
  return result;
}

/** return point of m, which are closer than limitDist 
    to subset sub, but do not belong to sub 
*/
Vec<unsigned int>
getClosePoints(const Vec<Vector3D>& m1, const Vec<Vector3D>& m2,
	       double limitDist)
{
  Vec<unsigned int> result;
  for (unsigned int i = 0; i < m1.size(); ++i) {
    for (unsigned int j = 0; j < m2.size(); ++j) {
      if (vecDistance(m1[i], m2[j]) <= limitDist) {
	result.push_back(j);
      }
    }    
  }
  return result;
}

/** return true, if matrix is symmetrix */
bool
isSymmetric(const Vec<Vec<double> >& m)
{
  unsigned int mdim = m.size();
  for (unsigned int i = 0; i < m.size(); ++i) {
    if (m[i].size() != mdim) {
      return false;
    }
    for (unsigned int j = 0; j < i; ++j) {
      if (fabs(m[i][j] - m[j][i]) > 0.1) {
	return false;
      }
    }
  }
  return true;
}

/** symmetrizes matrix */
void
symmetrize(Vec<Vec<double> >& m)
{
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < i; ++j) {
      m[i][j] = 0.5 * (m[i][j] + m[j][i]);
      m[j][i] = m[i][j];
    }
  }
}

/** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
    moment of inertia */
Matrix3D
computeInertiaTensor(const Vec<Vector3D>& v,
		     const Vec<double> weights) 
{
  PRECOND(weights.size() == v.size());
  Matrix3D m;
  Matrix3D unitMatrix(1.0,1.0,1.0);
THROWIFNOT(isSymmetric(translateMatrix3DToVec(unitMatrix)),"Assertion violation in L3905");
  for (unsigned int i = 0; i < v.size(); ++i) {
    m += ( (unitMatrix * (v[i]*v[i])) -matrixProduct(v[i],v[i]) ) * weights[i];
THROWIFNOT(isSymmetric(translateMatrix3DToVec(m)),"Assertion violation in L3908");
  }
  POSTCOND( isReasonable(translateMatrix3DToVec(m))
	 && isSymmetric(translateMatrix3DToVec(m)));
  return m;
}


Vec<Vec<double> >
translateMatrix3DToVec(const Matrix3D& m)
{
  Vec<double> row(3,0.0);
  Vec<Vec<double> > matrix(3,row);
  matrix[0][0] = m.xx();
  matrix[0][1] = m.xy();
  matrix[0][2] = m.xz();
  matrix[1][0] = m.yx();
  matrix[1][1] = m.yy();
  matrix[1][2] = m.yz();
  matrix[2][0] = m.zx();
  matrix[2][1] = m.zy();
  matrix[2][2] = m.zz();
  return matrix;
}

/** see Goldstein, Classical Mechanics Ch. "The inertia tensor and the
    moment of inertia */
/*
void
computeGyrationGeometry(const Vec<Vector3D>& v,
			const Vec<double> weights,
			Vec<double>& eigenValuesOrig,
			Vec<Vec<double> >& eigenVectorsOrig)
{
  PRECOND(v.size() == weights.size());
  Matrix3D inertiaTensor = computeInertiaTensor(v,weights);
  Vec<double> eigenValues(3, 0.0);
  Vec<Vec<double> > eigenVectors(3, eigenValues);
  jacobi(translateMatrix3DToVec(inertiaTensor), eigenValues,
	 eigenVectors);
  eigenValuesOrig = eigenValues;
  eigenVectorsOrig = eigenVectors;
}
*/

/** 
    transform scores to z-scores: measure is 
    deviation from mean in terms of its standard deviation
*/
void
transformToZScores(Vec<double>& v, double minSigma)
{
  double mean = vecMean(v);
  double sigma = vecVariance(v);
  if (sigma <= 0.0) {
    for (unsigned int i = 0; i < v.size(); ++i) {
      v[i] = 0.0;
    }
    return; // all components are identical
  }
  sigma = max(minSigma,sqrt(sigma));
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = (v[i] - mean) / sigma;
  }
}

/** multiply all entries of matrix with this factor */
void
matrixScalarMul(Vec<Vec<double> >& matrix, double val) 
{
  for (unsigned int i = 0; i < matrix.size(); ++i) {
    for (unsigned int j = 0; j < matrix[i].size(); ++j) {
      matrix[i][j] *= val;
    }
  }
}

/** b = m a */
void
matrixVectorMul(const Vec<Vec<double> >& m,
		const Vec<double>& a,
		Vec<double>& b)
{
  PRECOND((a.size() == b.size()));
  for (unsigned int i = 0; i < m.size(); ++i) {
    b[i] = 0.0;
    for (unsigned int j = 0; j < m[i].size(); ++j) {
      b[i] += m[i][j]*a[j];
    }
  }
}

/** b = a(T) b */
void
vectorMatrixMul(const Vec<Vec<double> >& m,
		const Vec<double>& a,
		Vec<double>& b)
{
  PRECOND((m.size() > 0) 
	  && (a.size() == m.size()) && (b.size() == m[0].size()));
  // number of columns
  for (unsigned int i = 0; i < m[0].size(); ++i) {
      b[i] = 0.0;
      // loop over rows
      for (unsigned int j = 0; j < m.size(); ++j) {
	b[i] += m[j][i]*a[j];
      }
  }
}

void 
matrixMul(const Vec<Vec<double> >& a,
	  const Vec<Vec<double> >& b,
		Vec<Vec<double> >& c)
{
  PRECOND( (a.size() > 0) && (b.size() > 0) && (c.size() > 0)
	   && (a.size() == c.size()) 
	   && (b[0].size() == c[0].size()) 
	   && (a[0].size() == b.size()));
  for (unsigned int i = 0; i < c.size(); ++i) {
    for (unsigned int j = 0; j < c[i].size(); ++j) {
      c[i][j] = 0.0;
      for (unsigned int k = 0; k < a[i].size(); ++k) {
	c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

/** simple element-wise (non-standard!) multiplication */
void 
matrixElementMul(const Vec<Vec<double> >& a,
		 const Vec<Vec<double> >& b,
		 Vec<Vec<double> >& c)
{
  PRECOND( (a.size() > 0) && (b.size() > 0) && (c.size() > 0)
	   && (a.size() == c.size()) 
	   && (b[0].size() == c[0].size()) 
	   && (a[0].size() == b.size()));
  for (unsigned int i = 0; i < c.size(); ++i) {
    for (unsigned int j = 0; j < c[i].size(); ++j) {
      c[i][j] = a[i][j] * b[i][j];
    }
  }
}

/** transpose matrix */
void
matrixTranspose(const Vec<Vec<double> >& m1,
		Vec<Vec<double> >& m2 )
{
  unsigned int m = m1.size(); 
  ERROR_IF(m == 0, "Matrix with zero elements encountered.");
  unsigned int n = m1[0].size(); 
  Vec<double> newRow(m,0.0);
  Vec<Vec<double> > helpField(n, newRow);
  m2 = helpField;
  for (unsigned int i = 0; i < m1.size(); ++i) {
    for (unsigned int j = 0; j < m1[i].size(); ++j) {
THROWIFNOT((j < m2.size())&& (i < m2[j].size()),"Assertion violation in L4067");
      m2[j][i] = m1[i][j];
    }
  }
}

/** transpose matrix itself. Works only for square matrices.  */
void
matrixTranspose(Vec<Vec<double> >& m)
{
  PRECOND(m.size() == m[0].size()); // only for square matrices!
  Rcpp::Rcout << "inside matrixTranspose!" << endl;
  double help = 0.0;
  for (unsigned int i = 0; i < m.size(); ++i) {
    for (unsigned int j = 0; j < i; ++j) {
THROWIFNOT((j < m.size())&& (i < m[j].size()),"Assertion violation in L4082");
      help = m[j][i]; // swap content
      m[j][i] = m[i][j];
      m[i][j] = help;
    }
  }
}

/** set matrix elements to one, if i=j, zero otherwise
 */
void
unitMatrix(Vec<Vec<double> >& matrix);



/** compute matrix inverse. See numerical recepies */
void
matrixInverse(const Vec<Vec<double> >& m, Vec<Vec<double> >& y);

/** compute matrix inverse and determinante. See numerical recepies */
void
matrixInverseAndDet(const Vec<Vec<double> >& m, Vec<Vec<double> >& y,
		    double& d);

/** compute matrix inverse and log of determinante . See numerical recepies */
void
matrixInverseAndLogDet(const Vec<Vec<double> >& mOrig,
		    Vec<Vec<double> >& y, double& logDet);

/** reverse matrix triangular decomposition (stored in lum) as
    computed by ludcmp
*/
void
matrixReverseDecomposition(const Vec<Vec<double> >& lum, 
			   Vec<Vec<double> >& m);

/** return bin of histogram */
int getBin(double val, double min, double delta)
{
  PRECOND(delta > 0.0);
  return static_cast<int>((val - min) / delta);
}

/** return historgramm: vector of bins filled with counts */
Vec<unsigned int> computeHistogram(const Vec<double>& data,
			      double min,
			      double delta, 
			      unsigned int numBins)
{
  PRECOND(delta > 0.0);
  Vec<unsigned int> result(numBins, 0);
  int bin = 0; 
  for (unsigned int i = 0; i < data.size(); ++i) {
    bin = static_cast<int>((data[i] - min) / delta);
    if ((bin >= 0) && (bin < static_cast<int>(result.size()) ) ) {
      ++result[bin];
    }
  }
  return result;
}

/** return historgramm: vector of bins filled with y values belonging
 to this bin */
Vec<Vec<double> > computeScatterHistogram(const Vec<double>& datax,
					  const Vec<double>& datay,
					  double min,
					  double delta, 
					  unsigned int numBins)
{
  PRECOND(delta > 0.0);
  PRECOND(datax.size() == datay.size());
  Vec<Vec<double> > result(numBins);
//    Rcpp::Rcout << "datax: " << datax << endl;
//    Rcpp::Rcout << "datay: " << datay << endl;
  int bin = 0; 
  for (unsigned int i = 0; i < datax.size(); ++i) {
    bin = static_cast<int>((datax[i] - min) / delta);
    if ((bin >= 0) && (bin < static_cast<int>(result.size()) ) ) {
      result[bin].push_back(datay[i]);
    }
  }
  return result;
}

/** return historgramm: vector of bins filled with counts */
Vec<double> computeDoubleHistogram(const Vec<double>& data,
				   double min,
				   double delta, 
				   unsigned int numBins)
{
  PRECOND(delta > 0.0);
  Vec<double> result(numBins, 0.0);
  int bin = 0; 
  for (unsigned int i = 0; i < data.size(); ++i) {
    bin = static_cast<int>((data[i] - min) / delta);
    if ((bin >= 0) && (bin < static_cast<int>(result.size()) ) ) {
      result[bin] = result[bin] + 1.0;
    }
  }
  return result;
}

/** return historgramm: vector of bins filled with counts */
Vec<double> computeFrequency(const Vec<double>& data,
			     double min,
			     double delta, 
			     unsigned int numBins)
{
  PRECOND(delta > 0.0);
  Vec<double> result(numBins, 0.0);
  int bin = 0;
  double count = 0;
  for (unsigned int i = 0; i < data.size(); ++i) {
    bin = static_cast<int>((data[i] - min) / delta);
    if ((bin >= 0) && (bin < static_cast<int>(result.size()) ) ) {
      ++result[bin];
      ++count;
    }
  }
  if (count > 0.0) {
    for (unsigned int i = 0; i < result.size(); ++i) {
      result[i] = result[i] / count;
    }
  }
  return result;
}


/** compute uniqe set from original set */
bool
containsDuplicates(const Vec<unsigned int>& vOrig)
{
  if (vOrig.size() == 0) {
    return false;
  }
  Vec<unsigned int> v = vOrig; // safety copy
  sort(v.begin(), v.end());
  for (unsigned int i = 1; i < v.size(); ++i) {
    if (v[i] == v[i-1]) {
      return true;
    }
  }
  return false;
}


/* compute common subset of two sets.
   not unique list, if input lists are not unique! */
Vec<unsigned int>
commonSubset(const Vec<unsigned int>& lOrig1,
	     const Vec<unsigned int>& lOrig2 )
{
  Vec<unsigned int> l1 = lOrig1;
  Vec<unsigned int> l2 = lOrig2;
  Vec<unsigned int> result;
  sort(l1.begin(), l1.end());
  sort(l2.begin(), l2.end());
  for (unsigned int i = 0; i < l1.size(); ++i) {
    for (unsigned int j = 0; j < l2.size(); ++j) {
      if (l1[i] == l2[j]) {
	result.push_back(l1[i]);
	break;
      }
    }
  }
  return result;
}

/* compute unique union of two sets */
Vec<unsigned int>
uniqueUnion(const Vec<unsigned int>& l1,
	    const Vec<unsigned int>& l2)
{
  Vec<unsigned int> result = l1;
  for (unsigned int i = 0; i < l2.size(); ++i) {
    bool found = false;
    for (unsigned int j = 0; j < result.size(); ++j) {
      if (result[j] == l2[i]) {
	found = true;
	break;
      }
    }
    if (!found) {
      unsigned int tmp = l2[i];
      result.push_back(tmp);
    }
  }
  return result;
}

/** vector with probabilities of different entities.
    values must be between 0 and 1
*/
double
conservation(const Vec<double>& v)
{
  double sum = elementSum(v);
  if (sum <= 0.0) {
    return 0.0;
  }
  double x = *(max_element(v.begin(), v.end()));
  return x / sum;
}

/* return 3D matrix from Euler angles
   rotation around x-axis: w
   rotation around y-axis: a
   rotation around z-axis: k
   taken from: Ronald Azuma 1991 (collinear.pdf). Check again from other source!!!
*/
Matrix3D
computeEulerRotationMatrix(double w, double a, double k)
{
  Matrix3D m(
  cos(k)*cos(a),-sin(k)*cos(w)+cos(k)*sin(a)*sin(w),sin(k)*sin(w)+cos(w)*cos(k)*sin(a),
  sin(k)*cos(a),cos(k)*cos(w)+sin(w)*sin(k)*sin(a),-sin(w)*cos(k)+cos(w)*sin(k)*sin(a),
  -sin(a),sin(w)*cos(a),cos(w)*cos(a) );
  return m;
}

/* find minimum x, y, z coordinates */
Vector3D
findMinCoordinates(const Vec<Vector3D>& vv)
{
  PRECOND(vv.size() > 0);
  double xmin = vv[0].x();
  double ymin = vv[0].y();
  double zmin = vv[0].z();
  for (unsigned int i = 1; i < vv.size(); ++i) {
    if (vv[i].x() < xmin) {
      xmin = vv[i].x();
    }
    if (vv[i].y() < ymin) {
      ymin = vv[i].y();
    }
    if (vv[i].z() < zmin) {
      zmin = vv[i].z();
    }
  }
  Vector3D result(xmin, ymin, zmin);
  return result;
}

/* find maximum x, y, z coordinates */
Vector3D
findMaxCoordinates(const Vec<Vector3D>& vv)
{
  PRECOND(vv.size());
  double xmax = vv[0].x();
  double ymax = vv[0].y();
  double zmax = vv[0].z();
  for (unsigned int i = 1; i < vv.size(); ++i) {
    if (vv[i].x() > xmax) {
      xmax = vv[i].x();
    }
    if (vv[i].y() > ymax) {
      ymax = vv[i].y();
    }
    if (vv[i].z() > zmax) {
      zmax = vv[i].z();
    }
  }
  Vector3D result(xmax, ymax, zmax);
  return result;
}

unsigned int
findMaxDistanceIndex(const Vector3D& pos, const Vec<Vector3D>& v) {
  PRECOND(v.size() > 0);
  unsigned int bestIndex = 0;
  double dBest = vecDistanceSquare(pos, v[0]);
  for (unsigned int i = 1; i < v.size(); ++i) {
    double d = vecDistanceSquare(pos, v[i]);
    if (d > dBest) {
      dBest = d;
      bestIndex = i;
    }
  }
  return bestIndex;
}

unsigned int
findMinDistanceIndex(const Vector3D& pos,
		     const Vec<Vector3D>& v) {
  PRECOND(v.size() > 0);
  unsigned int bestIndex = 0;
  double dBest = vecDistance(pos, v[0]);
  for (unsigned int i = 1; i < v.size(); ++i) {
    double d = vecDistance(pos, v[i]);
    if (d < dBest) {
      dBest = d;
      bestIndex = i;
    }
  }
  POSTCOND(bestIndex < v.size());
  return bestIndex;
}

/* return distance in indices of pair of vectors with shortest distance */
double
findSmallestDistance(const Vec<Vector3D>& v1,
		    const Vec<Vector3D>& v2,
		    unsigned int& n1,
		    unsigned int& n2)
{
  PRECOND((v1.size() > 0) && (v2.size() > 0));
  unsigned int v1Size = v1.size();
  unsigned int v2Size = v2.size();
  double d;
  double dBest = vecDistanceSquare(v1[0], v2[0]);
  n1 = 0;
  n2 = 0;
  for (unsigned int i = 0; i < v1Size; ++i) {
    for (unsigned int j = 0; j < v2Size; ++j) {
      d = vecDistanceSquare(v1[i], v2[j]);
      if (d < dBest) {
	dBest = d;
	n1 = i;
	n2 = j;
      }
    }
  }
  return sqrt(dBest);
}

/** return coordinates which are in the middle of
    min and max values for each dimension */
Vector3D
computeCenterOfCoordinates(const Vec<Vector3D>& v)
{
  return ( findMaxCoordinates(v)
	 + findMinCoordinates(v) ) * 0.5;
}

/** return center of mass. Do not take weights into account! */
Vector3D
computeCenterOfMass(const Vec<Vector3D>& v)
{
  PRECOND(v.size() > 0);
  Vector3D sum(0.0,0.0,0.0);
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += v[i];
  }
  sum *= (1.0/static_cast<double>(v.size()));
  return sum;
}

/** return center of mass. */
Vector3D
computeCenterOfMass(const Vec<Vector3D>& v,
		    const Vec<double> weights)
{
  PRECOND((v.size() > 0) && (v.size() == weights.size()));
  Vector3D sum(0.0,0.0,0.0);
  double weightSum = 0.0;
  for (unsigned int i = 0; i < v.size(); ++i) {
    sum += (v[i] * weights[i]);
    weightSum += weights[i];
  }
THROWIFNOT(weightSum > 0.0,"Assertion violation in L4441");
  return sum * (1.0/weightSum);
}

/* return radius of gyration of set of vectors */
double
computeRadiusOfGyration(const Vec<Vector3D>& v)
{
  double result = 0;
  Vector3D center = computeCenterOfMass(v);
  for (unsigned int i = 0; i < v.size(); ++i) {
    double r = vecDistance(v[i],center);
    result += r * r;
  }
  result = sqrt(result);
  return result;
}

/** compute root mean square deviation of two sets of coordinate vectors */
double
rms(const Vec<Vector3D>& v1, const Vec<Vector3D>& v2)
{
  PRECOND((v1.size() > 0) && (v1.size() == v2.size()));
  double sum = 0.0;
  for (unsigned int i = 0; i < v1.size(); ++i) {
    sum += vecDistanceSquare(v1[i],v2[i]);
  }
  return sqrt(sum/v1.size());
}

/* generate set of n points with radius r around origin vector on a ring defined by theta */
Vec<Vector3D>
generateRingPoints(unsigned int n, double r, double theta, const Vector3D& origin)
{
  if (n == 0) {
    return Vec<Vector3D>();
  }
  Vec<Vector3D> ray(n);
  double phi;
  double phiDelta = (PI * 2) / static_cast<double>(n);
  for (unsigned int i = 0; i < n; ++i) {
    phi = i * phiDelta;
    ray[i].x(cos(phi) * sin(theta));
    ray[i].y(sin(phi)*sin(theta));
    ray[i].z(cos(theta));
  }
  for (unsigned int i = 0; i < ray.size(); ++i) {
    ray[i] = (ray[i] * r) + origin;
  }
  POSTCOND(ray.size() == n);
  return ray;
}

/* generate set of n points with radius r around origin vector on a ring defined by theta */
Vec<Vector3D>
generateRingPoints(unsigned int n, double r, double theta, const Vector3D& origin, 
		   Vector3D direction)
{
  if (n == 0) {
    return Vec<Vector3D>();
  }
  Vector3D zeroVec(0.0, 0.0, 0.0);
  Vector3D zAxis(0.0, 0.0, 1.0);
  Vec<Vector3D> ray = generateRingPoints(n, r, theta, zeroVec);
  if (direction.length() > 0.0) {
    direction.normalize();
    if (direction != zAxis) {
      double ang = angle(direction, zAxis);
      Vector3D rotAxis = cross(zAxis, direction);
      rotAxis.normalize();
      for (unsigned int i = 0; i < ray.size(); ++i) {
	ray[i] = Matrix3D::rotate(ray[i], rotAxis, ang);
      }
    }
  }
  for (unsigned int i = 0; i < ray.size(); ++i) {
    ray[i] = ray[i] + origin;
  }
  POSTCOND(ray.size() == n);
  return ray;
}


/* generate set of n points with radius r around origin vector */
Vec<Vector3D>
generateSpherePoints(unsigned int n, double r, const Vector3D& origin)
{
  if (n == 0) {
    return Vec<Vector3D>();
  }
  Vec<Vector3D> ray(n);
  double h,theta,phi;
  double oldPhi = 0.0;
  for (unsigned int i = 1; i < n-1; ++i)
    {
      h = -1.0 + 2.0 * i / (n-1.0);
THROWIFNOT((h>=-1.0)&&(h<=1.0),"Assertion violation in L4537");
      theta = acos(h);
      phi = (oldPhi + 3.6/sqrt(n*(1-h*h)));
      ray[i].x(cos(phi) * sin(theta));
      ray[i].y(sin(phi)*sin(theta));
      ray[i].z(cos(theta));
      oldPhi = phi;
    }
  ray[0] = Vector3D(0.0,0.0,-1.0);
  ray[n-1] = Vector3D(0.0,0.0,1.0);
  for (unsigned int i = 0; i < ray.size(); ++i) {
    ray[i] = (ray[i] * r) + origin;
  }
  return ray;
}

/* generate surface points of point set */
void
generateSurfacePoints(Vec<Vector3D>& mol, const Vec<Vector3D>& molProt, 
		      double r, unsigned int tessel, bool rawMode)
{
  mol.clear();
  Vec<Vec<Vector3D> > allv(molProt.size());
  for (unsigned int i = 0; i < molProt.size(); ++i) {
    Vec<Vector3D> sset = generateSpherePoints(tessel, r, molProt[i]);
    // add points to set of all generated points:
    allv[i] = sset;
  }
  // only keep those, that are not closer to point from other set:
  for (unsigned int i = 0; i < allv.size(); ++i) {
    for (unsigned int j = 0; j < allv[i].size(); ++j) {
      bool found = false;
      for (unsigned int k = 0; k < molProt.size(); ++k) {
	if (k == i) {
	  continue; // skip if same sphere
	}
	double d = vecDistance(allv[i][j], molProt[k]);
	if (d < r) {
	  found = true;
	  break;
	}
      }
      if (rawMode || (!found)) {
	mol.push_back(allv[i][j]);
      }
    }
  }
}

/* generate surface points of point set
   result: vector of size molProt, each subvector
   containing surface vectors belonging to that atom */
Vec<Vec<Vector3D> >
generateSurfacePoints(const Vec<Vector3D>& molProt, 
		      double r, unsigned int tessel, bool rawMode)
{
  PRECOND(molProt.size() > 0);
  Vec<Vec<Vector3D> > allv(molProt.size()), mol(molProt.size()) ;
  for (unsigned int i = 0; i < molProt.size(); ++i) {
    Vec<Vector3D> sset = generateSpherePoints(tessel, r, molProt[i]);
    // add points to set of all generated points:
    allv[i] = sset;
  }
  // only keep those, that are not closer to point from other set:
  for (unsigned int i = 0; i < allv.size(); ++i) {
    for (unsigned int j = 0; j < allv[i].size(); ++j) {
      bool found = false;
      for (unsigned int k = 0; k < molProt.size(); ++k) {
	if (k == i) {
	  continue; // skip if same sphere
	}
	double d = vecDistance(allv[i][j], molProt[k]);
	if (d < r) {
	  found = true;
	  break;
	}
      }
      if (rawMode || (!found)) {
	mol[i].push_back(allv[i][j]);
      }
    }
  }
  return mol;
}


/* generate surface points of point set */
/*
void
generateSurfacePoints(Vec<Vector3D>& mol,
     Vec<int>& newIdVec, const Vec<Vector3D>& molProt, 
     const Vec<int>& idVec, double r, unsigned int tessel, bool rawMode)
{
  PRECOND(molProt.size() == idVec.size());
  mol.clear();
  newIdVec.clear();
  Vec<Vec<Vector3D> > allv(molProt.size());
  for (unsigned int i = 0; i < molProt.size(); ++i) {
    Vec<Vector3D> sset = generateSpherePoints(tessel, r, molProt[i]);
    // add points to set of all generated points:
    allv[i] = sset;
  }
  // only keep those, that are not closer to point from other set:
  for (unsigned int i = 0; i < allv.size(); ++i) {
    for (unsigned int j = 0; j < allv[i].size(); ++j) {
      bool found = false;
      for (unsigned int k = 0; k < molProt.size(); ++k) {
	if (k == i) {
	  continue; // skip if same sphere
	}
	double d = vecDistance(allv[i][j], molProt[k]);
	if (d < r) {
	  found = true;
	  break;
	}
      }
      if (rawMode || (!found)) {
	mol.push_back(allv[i][j]);
	newIdVec.push_back(idVec[i]);	
      }
    }
  }
}
*/

/* generate surface points of point set */
void
generateSurfacePoints(Vec<Vector3D>& mol,
     Vec<int>& newIdVec, Vec<Vector3D>& directions,
     const Vec<Vector3D>& molProt, 
     const Vec<int>& idVec,
     double r, unsigned int tessel, bool rawMode)
{
  mol.clear();
  directions.clear();
  newIdVec.clear();
  Vec<Vec<Vector3D> > allv(molProt.size());
  for (unsigned int i = 0; i < molProt.size(); ++i) {
    Vec<Vector3D> sset = generateSpherePoints(tessel, r, molProt[i]);
    // add points to set of all generated points:
    allv[i] = sset;
  }
  // only keep those, that are not closer to point from other set:
  for (unsigned int i = 0; i < allv.size(); ++i) {
    for (unsigned int j = 0; j < allv[i].size(); ++j) {
      bool found = false;
      for (unsigned int k = 0; k < molProt.size(); ++k) {
	if (k == i) {
	  continue; // skip if same sphere
	}
	double d = vecDistance(allv[i][j], molProt[k]);
	if (d < r) {
	  found = true;
	  break;
	}
      }
      if (rawMode || (!found)) {
	mol.push_back(allv[i][j]);
	if (i < idVec.size()) {
	  newIdVec.push_back(idVec[i]);	
	}
	else {
	  newIdVec.push_back(i);
	}
	// vector from surf point to its nearest atom center is surface normal
	directions.push_back((allv[i][j] - molProt[i]));
      }
    }
  }
  POSTCOND((mol.size() == newIdVec.size())
	   && (mol.size() == directions.size()));
}

/* generate surface points of point set */
void
generateSurfacePointsAccessibleSurface(Vec<Vector3D>& mol,
     Vec<int>& newIdVec, Vec<Vector3D>& directions,
     Vec<double>& accessSurface,				       
     const Vec<Vector3D>& molProt, 
     const Vec<int>& idVec,
     double r, unsigned int tessel, bool rawMode)
{
  mol.clear();
  directions.clear();
  newIdVec.clear();
  accessSurface.clear();
  accessSurface = Vec<double>(molProt.size(), 0.0);
  Vec<Vec<Vector3D> > allv(molProt.size());
  for (unsigned int i = 0; i < molProt.size(); ++i) {
    Vec<Vector3D> sset = generateSpherePoints(tessel, r, molProt[i]);
    // add points to set of all generated points:
    allv[i] = sset;
  }
  // only keep those, that are not closer to point from other set:
  for (unsigned int i = 0; i < allv.size(); ++i) {
    for (unsigned int j = 0; j < allv[i].size(); ++j) {
      bool found = false;
      for (unsigned int k = 0; k < molProt.size(); ++k) {
	if (k == i) {
	  continue; // skip if same sphere
	}
	double d = vecDistance(allv[i][j], molProt[k]);
	if (d < r) {
	  found = true;
	  break;
	}
      }
      if (rawMode || (!found)) {
	mol.push_back(allv[i][j]);
	if (i < idVec.size()) {
	  newIdVec.push_back(idVec[i]);	
	}
	else {
	  newIdVec.push_back(i);
	}
	accessSurface[i] = accessSurface[i] + 1.0;
	// vector from surf point to its nearest atom center is surface normal
	directions.push_back((allv[i][j] - molProt[i]));
      }
    }
  }
  double totAtomSurface = 4.0 * PI * r * r;
  for (unsigned int i = 0; i < accessSurface.size(); ++i) {
    accessSurface[i] = accessSurface[i] / static_cast<double>(tessel) 
      * totAtomSurface;
THROWIFNOT((accessSurface[i] <= totAtomSurface),"Assertion violation in L4762");
  }
  POSTCOND((mol.size() == newIdVec.size())
	   && (mol.size() == directions.size())
	   && (accessSurface.size() == molProt.size()));
}

/* generate compute accessible surface for point set */
void
computeAccessibleSurface(const Vec<Vector3D>& molProt, 
			  Vec<double>& accessSurface,				       
			  double r, unsigned int tessel)
{
  Vec<Vector3D> surfaceMol;
  Vec<Vector3D> directions;
  Vec<int> newIdVec;
  Vec<int> idVec(molProt.size(), 0);
  generateSurfacePointsAccessibleSurface(surfaceMol,
     newIdVec, directions,
     accessSurface, molProt, idVec, r, tessel, false);
  POSTCOND(molProt.size() == accessSurface.size());
}

/* generate simplex of n+1 dimensions */
Vec<Vec<double> > generateSimplex(const Vec<double>& v, double step)
{
  Vec<Vec<double> > result(v.size() +1, v);
  // first vector is start vector, others are one step away
  for (unsigned int i = 1; i < result.size(); ++i) {
    result[i][i-1] += step;
  }
  POSTCOND(result.size() == v.size() + 1);
  return result;
}

/* generate simplex of n+1 dimensions */
Vec<Vec<double> > generateSimplex(const Vec<double>& v, const Vec<double>& stepSizes)
{
  PRECOND(stepSizes.size() == v.size());
  Vec<Vec<double> > result(v.size() +1, v);
  // first vector is start vector, others are one step away
  for (unsigned int i = 1; i < result.size(); ++i) {
    result[i][i-1] += stepSizes[i-1];
  }
  POSTCOND(result.size() == v.size() + 1);
  return result;
}


/*
template <class T>
Vec<unsigned int>
annotatedSort(Vec<T>& v)
{
  bool sorted = false;
  Vec<unsigned int> annotation(v.size(), 0);
  for (unsigned int i = 0; i < annotation.size(); ++i) {
    annotation[i] = i;
  }
  while (!sorted) {
    sorted = true;
    for (unsigned int i = 1; i < v.size(); ++i) {
      if (v[i] < v[i-1]) {
	swap(v[i-i], v[i]);
	swap(annotation[i-1], annotation[i]);
	sorted = false;
      }
    }
  }
  return annotation;
}
*/

// get k nearest neighours of query vector closer than maxDist 
Vec<unsigned int>
getKNearestNeighbours(const Vec<Vector3D>& v, 
		      unsigned int query,
		      unsigned int maxN,
		      double maxDist)
{
  PRECOND(query < v.size());
  Vec<unsigned int> bestIndices;
  Vec<double> distances;  
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (i == query) {
      continue;
    }
    double d = vecDistance(v[i], v[query]);      
    if (d <= maxDist) {
      distances.push_back(d);
      bestIndices.push_back(i);
    }
  }
  // sort:
  Vec<unsigned int> formerOrder = documentedSort(distances);
  unsigned int maxI = formerOrder.size();
  if (maxI > maxN) {
    maxI = maxN;
  }
  Vec<unsigned int> result;
  for (unsigned int i = 0; i < maxI; ++i) {
    result.push_back(bestIndices[formerOrder[i]]);
  }
  return result;
}

// get k nearest neighours of query vector closer than maxDist 
Vec<Vec<unsigned int> >
getKNearestNeighbours(const Vec<Vector3D>& v, 
		      unsigned int maxN,
		      double maxDist)
{
  Vec<Vec<unsigned int> > result(v.size());
  for (unsigned int i = 0; i < v.size(); ++i) {
    result[i] = getKNearestNeighbours(v,i,maxN,maxDist);
  }
  return result;
}

/* return non-linear weighting of value x */
double
nonLinearWeighting(double x, double maxY, double x0, double scale, double skew)
{
  PRECOND(scale > 0.0);
  return ( 0.5 * maxY * (tanh(scale*(x-x0)) + 1) ) + skew;
}

/* return non-linear weighting of value x */
double
nonLinearWeightingDouble(double x, double maxY1, double x01, double scale1, 
			 double maxY2, double x02, double scale2)
{
  PRECOND((scale1 > 0.0) && (scale2 > 0.0));
  return nonLinearWeighting(x, fabs(maxY1), x01, scale1, - fabs(maxY1)) 
       + nonLinearWeighting(x, fabs(maxY2), x02, scale2);
}

/* expects N SORTED x-values and N+1 y values defining a step function. */
double
stepFunction(double x, const Vec<double>& xVec, const Vec<double>& yVec)
{
  PRECOND(xVec.size() + 1 == yVec.size());
  double y = 0.0;
  if ((xVec.size() == 0) || (x < xVec[0])) {
    y = yVec[0];
  }
  else if (x > xVec[xVec.size()-1]) {
    y = yVec[xVec.size()];
  }
  else  {
    for (unsigned int i = 0; i < xVec.size(); ++i) {
      if (x <= xVec[i]) {
	y = yVec[i];
	break;
      }
    }
  }
  return y;
}

/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double
computeMathews(const Vec<double>& scores, 
		const Vec<unsigned int>& trueClass, 
		unsigned int classNum, 
		double cutOff,
		unsigned int& truePos, 
		unsigned int& falsePos, 
		unsigned int& trueNeg, 
		unsigned int& falseNeg)
{
  PRECOND(scores.size() == trueClass.size());
  for (unsigned int i = 0; i < scores.size(); ++i) {
    if (scores[i] >= cutOff) {
      if (trueClass[i] == classNum) {
	++truePos;
      }
      else {
	++falsePos;
      }
    }
    else {
      if (trueClass[i] == classNum) {
	++falseNeg;
      }
      else {
	++trueNeg;
      }
    }
  }
THROWIFNOT(truePos + falsePos + falseNeg + trueNeg == scores.size(),"Assertion violation in L4953");
  return computeMathews(truePos, falsePos, trueNeg, falseNeg);
}


/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double
computeMathews(const Vec<Vec<double> > & scores, 
	       const Vec<Vec<unsigned int> > & trueClass, 
	       unsigned int classNum, 
	       double cutOff,
	       unsigned int& truePos, 
	       unsigned int& falsePos, 
	       unsigned int& trueNeg, 
	       unsigned int& falseNeg)
{
  PRECOND(scores.size() == trueClass.size());
  truePos = 0;
  falsePos = 0;
  trueNeg = 0;
  falseNeg = 0;
  for (unsigned int i = 0; i < scores.size(); ++i) {
    for (unsigned int j = 0; j < scores[i].size(); ++j) {
      if (scores[i][j] >= cutOff) {
	if (trueClass[i][j] == classNum) {
	  ++truePos;
	}
	else {
	  ++falsePos;
	}
      }
      else {
	if (trueClass[i][j] == classNum) {
	  ++falseNeg;
	}
	else {
	  ++trueNeg;
	}
      }
    }
  }
THROWIFNOT(truePos + falsePos + falseNeg + trueNeg == (scores.size()*scores[0].size()),"Assertion violation in L4995");
  return computeMathews(truePos, falsePos, trueNeg, falseNeg);
}


/* compute true/false positives/negatives of scores, which if greater
   than cutOff, are predicted to belong to class classNum */
double
computeMathews(	unsigned int truePos, unsigned int falsePos, 
		unsigned int trueNeg, unsigned int falseNeg)
{
  double head = static_cast<double>((truePos * trueNeg)) 
              - static_cast<double>((falsePos * falseNeg));
  double denom = static_cast<double>(trueNeg + falseNeg) 
               * static_cast<double>(trueNeg + falsePos)
               * static_cast<double>(truePos + falseNeg) 
               * static_cast<double>(truePos + falsePos);
  if (denom <= 0.0) {
    return -1000.0;
  }
  return head / sqrt(denom);
}

/* after computation the values between the two indices are
   interpolated */
void
vecInterpolate(Vec<double>& v, unsigned int startidx, unsigned int endidx)
{
  PRECOND((endidx > startidx) && (endidx < v.size()));
  unsigned int deltax = endidx - startidx;
  double dy = (v[endidx] - v[startidx]) / deltax;
  for (unsigned int i = startidx + 1; i < endidx; ++i) {
    v[i] = ( (i - startidx) * dy ) + v[startidx];
  }
}

/* fill in missing values if borders by defined values.
   undefined is every v[i] with fabs(v[i] - empty) <= diff.
*/
void
vecInterpolateMissing(Vec<double>& v, double empty, double diff)
{
  unsigned int i = 0;
  unsigned status = 0;
  unsigned int lastidx = 0;
  while (i < v.size()) {
    bool isdef = (fabs(v[i] - empty) > diff);
    switch (status)
      {
      case 0: // no possible start point defined yet. 
	if (isdef) {
	  status = 1; // possible startpoint
	  lastidx = i;
	}
	break;
      case 1: // possible start point defined, no undefined region found
	if (isdef) {
	  status = 1; // keep status unchanged move starting point
	  lastidx = i;
	}
	else if ( (i + 1) == v.size()) { // close to end
	  for (unsigned int j = lastidx + 1; j < v.size(); ++j) {
	    v[j] = v[lastidx]; // set right side to last seen value
	  }
	}
	else {
	  status = 2; // region suddenly undefined
	}
	break;
      case 2: // region was undefined
	if (isdef) { // suddenly defined again
	  vecInterpolate(v, lastidx, i);
	  status = 1; // possible startpoint
	  lastidx = i;
	}
	else if ( (i + 1) == v.size()) {
	  for (unsigned int j = lastidx + 1; j < v.size(); ++j) {
	    v[j] = v[lastidx]; // set right side to last seen value
	  }
	}
	break;
      default:
	DERROR("Internal error in line 2233!");
      }
    ++i;
  }

}

// compute gaussian at point n for vector v, with relaxation constant d,
// and maximum width nMax. If nMax == 0, ignore nMax
double
gaussValue(int n, const Vec<double>& v, double d, unsigned int nMax)
{
  //  const double INV_SQRT_2PI = 1.0 / sqrt((3.0/2.0) * PI);
  int nv = static_cast<int>(v.size() / 2);
  if ((nMax > 0) && (static_cast<int>(nMax) < nv)) {
    nv = nMax;
  }
  int vsize = v.size();
  double result = 0;
  double dsq = d * d;
  double val = 0;
  for (int i = -nv; i <= nv; ++i) {
    int j = n + i;
    // Rcpp::Rcout << "j: " << j << endl;
    if (j < 0) {
      val = v[0];
    }
    else if (j >= vsize) {
      val = v[vsize - 1];
    }
    else {
      val = v[j];
    }
    result += exp(- (i * i) / dsq) * val; 
  }
  //   result *= INV_SQRT_2PI; // NO NORMALIZATION ! DONE LATER!
  return result;
}

double
gaussValue(int n, const Vec<double>& v, double d)
{
  //  const double INV_SQRT_2PI = 1.0 / sqrt((3.0/2.0) * PI);
  int nv = static_cast<int>(v.size() / 2);
  int vsize = v.size();
  double result = 0;
  double dsq = d * d;
  double val = 0;
  for (int i = -nv; i <= nv; ++i) {
    int j = n - i;
    if (j < 0) {
      val = v[0];
    }
    else if (j >= vsize) {
      val = v[vsize - 1];
    }
    else {
      val = v[j];
    }
    result += exp(- (i * i) / dsq) * val; 
  }
  //   result *= INV_SQRT_2PI; // NO NORMALIZATION ! DONE LATER!
  return result;
}


Vec<double>
smoothGaussian(const Vec<double>& v, double d)
{
  Vec<double> w(v);
  if (d <= 0.0) {
    return w;
  }
  double sum = elementSum(v);
  for (unsigned int i = 0; i < v.size(); ++i) {
    w[i] = gaussValue(i, v, d);
  }
  double newSum = elementSum(w);
  if (newSum > 0.0) {
    scalarMul(w, sum / newSum); // normalize to old sum
  }
  return w;
}

Vec<double>
smoothGaussian(const Vec<double>& v, double d, unsigned int nMax)
{
  Vec<double> w(v);
  if (d <= 0.0) {
    return w;
  }
  double sum = elementSum(v);
  for (unsigned int i = 0; i < v.size(); ++i) {
    // Rcpp::Rcout << "Working on " << i << " " << v[i] << endl;
    w[i] = gaussValue(static_cast<int>(i), v, d, nMax );
  }
  double newSum = elementSum(w);
  if (newSum > 0.0) {
    scalarMul(w, sum / newSum); // normalize to old sum
  }
  return w;
}

// smoothing of 3D cube. Can be done as subsequent 1d smoothings
// see: http://www.mrc-cbu.cam.ac.uk/Imaging/smoothing.html
void
smoothGaussian(Vec<Vec<Vec<double> > >& cube, double d, unsigned int nMax)
{
  unsigned int dimX = cube.size();
  unsigned int dimY = cube[0].size();
  unsigned int dimZ = cube[0][0].size();
  Vec<double> xRow(dimX, 0.0);
  Vec<double> yRow(dimY, 0.0);
  Vec<double> zRow(dimZ, 0.0);
  // first smooth in x - direction:  
  for (unsigned int j = 0; j < dimY; ++j) {
    for (unsigned int k = 0; k < dimZ; ++k) {
      for (unsigned int i = 0; i < dimX; ++i) {
	xRow[i] = cube[i][j][k];
      }
      xRow = smoothGaussian(xRow, d, nMax);
      for (unsigned int i = 0; i < dimX; ++i) {
	cube[i][j][k] = xRow[i];
      }
    }
  }  
  // smooth in y - direction:  
  for (unsigned int i = 0; i < dimX; ++i) {
    for (unsigned int k = 0; k < dimZ; ++k) {
      for (unsigned int j = 0; j < dimY; ++j) {
	yRow[j] = cube[i][j][k];
      }
      yRow = smoothGaussian(yRow, d, nMax);
      for (unsigned int j = 0; j < dimY; ++j) {
	cube[i][j][k] = yRow[j];
      }
    }
  }  
  // smooth in z - direction:  
  for (unsigned int i = 0; i < dimX; ++i) {
    for (unsigned int j = 0; j < dimY; ++j) {
      for (unsigned int k = 0; k < dimZ; ++k) {
	zRow[k] = cube[i][j][k];
      }
      zRow = smoothGaussian(zRow, d, nMax);
      for (unsigned int k = 0; k < dimZ; ++k) {
	cube[i][j][k] = zRow[k];
      }
    }
  }  
}

// increase number to a certain basis.
void
incBaseNumber(unsigned int& n, unsigned int base, bool& overflow)
{
THROWIFNOT(n < base,"Assertion violation in L5233");
  ++n;
  if (n >= base) {
    n = 0;
    overflow = true;
  }
  else {
    overflow = false;
  }
}

// increase number to a certain basis. v[0] corresponds to lowest digit!
void
incBaseNumber(Vec<unsigned int>& v, unsigned int base)
{
  unsigned int i = 0;
  bool overflow = false;
  do {
    incBaseNumber(v[i++], base, overflow);
  }
  while (overflow && (i < v.size() ) );
}

// return maximum number which can be expressed by number with so many digits 
// to a certain base
unsigned int
maxBaseNumber(unsigned int digits, unsigned int base)
{
  double maxNum = pow(static_cast<double>(base), static_cast<int>(digits));
  if (maxNum > UINT_MAX){
    return UINT_MAX;
  }
  return static_cast<unsigned int>(maxNum);
}

/** makes matrix more similar to distance matrix: positive definite,
    and zero in diagonals */
void
distify(Vec<Vec<double> >& m)
{
  for (unsigned int i = 0; i < m.size(); ++i) {
    double ddi = m[i][i];
    for (unsigned int j = 0; j < i; ++j) {
      double ddj = m[j][j];
      // first symmetrize:
      double mavg = 0.5 * (m[i][j] + m[j][i]);
      m[i][j] = 0.5 * (fabs(ddi - mavg) + fabs(ddj - mavg));
      m[j][i] = m[i][j];
    }
  }
  for (unsigned int i = 0; i < m.size(); ++i) {
    m[i][i] = 0.0;
  }
}

/** returns index n,n from point m,n */
double
diagonalProjection(int row, int col, int size)
{
  if (col > row) {
    int help = row;
    row = col;
    col = help;
  }
  double x = static_cast<double>(row - col)/2.0;
  return row - x;
}

/** returns row index of projection to antidiagonal */
double
antiDiagonalProjection(int row, int col, int size)
{
  double x = static_cast<double>(row - col)/2.0;
  double cd = 0.5 * size - x; // anti-diagonal column
THROWIFNOT(cd >= 0.0,"Assertion violation in L5307");
  //   if (cd < 0.0) {
  //     x = static_cast<double>(col - row)/2.0;
  //     cd = 0.5 * size - x;
  //   }
  double rd = 0.5 * static_cast<double>(size) + x; // diagonal column
  return rd;
}

Vec<double>
getAntiDiagonalAverages(const Vec<Vec<double> >& mat)
{
  unsigned int maxDiagSize = 2 * mat.size() - 1;
  Vec<double> result(maxDiagSize);
  for (unsigned int i = 0; i < maxDiagSize; ++i) {
    result[i] = vecMean(getAntiDiagonal(mat, i));
  }
  return result;
}

Vec<double>
getDiagonalAverages(const Vec<Vec<double> >& mat)
{
  int maxDiagSize = 2 * static_cast<int>(mat.size()) - 1;
  Vec<double> result(maxDiagSize);
  int istart = - static_cast<int>(mat.size()) + 1;
  int ioffs = - istart;
  for (int i = istart; i < static_cast<int>(mat.size()); ++i) {
    Vec<double> diag = getDiagonal(mat, i);
    // Rcpp::Rcout << "Getting diagonal " << i << "  " << diag << endl;
THROWIFNOT(i + ioffs < static_cast<int>(result.size()),"Assertion violation in L5337");
    result[i + ioffs] = vecMean(diag);
  }
  return result;
}

/** returns correlation coefficient (Pearson correlation).
    @see Peter Dalgaard, Introductory Statistics with R
*/
double
correlationCoefficient(const Vec<double>& x, const Vec<double>& y)
{
  PRECOND(x.size() == y.size());
  double xm = vecMean(x);
  double ym = vecMean(y);
//   double xvar = vecVariance(x);
//   double yvar = vecVariance(y);
  double sum = 0.0;
  double denom1 = 0.0;
  double denom2 = 0.0;
  for (unsigned int i = 0; i < x.size(); ++i) {
    sum += (x[i] - xm) * (y[i] - ym);
    denom1 += (x[i] - xm) * (x[i] - xm);
    denom2 += (y[i] - ym) * (y[i] - ym);
  }
  double denom = sqrt(denom1 * denom2);  
  if (denom == 0.0) {
    return 0.0;
  }
  return sum / denom;
}

/** what would be that z-score of a perfectly predicted row
    in a contact matrix? Assume vector with all zeros and one "1" */
double
contactMatrixRowMaxScore(unsigned int n)
{
  PRECOND(n > 0);
  double mean = 1.0 / static_cast<double>(n);
  double sum = 1.0 * 1.0;
  double std = sqrt((sum/n) - (mean * mean)); 
  if (std == 0.0) {
    Rcpp::warning("Warning: zero standard deviation in contactMatrixRowMaxScore detected");
    return 0.0;
  }
  // ASSERT((std > 0.0));
  double result = (1.0 - mean) / std;
  POSTCOND(result > 0.0);
  return result;
}

double
contactMatrixRowScore(const Vec<double>& v, 
		      unsigned int pos, unsigned int diagLim)
{
  double maxVal = v[0];
  for (unsigned int i = 0; i + diagLim < pos + 1; ++i) {
    if (v[i] > maxVal) {
      maxVal = v[i];
    }
  }
  for (unsigned int i = pos + diagLim; i < v.size(); ++i) {
    if (v[i] > maxVal) {
      maxVal = v[i];
    }
  }
  double mean = vecMean(v);
  double maxStd = sqrt(vecVariance(v));
  if (maxStd < 1e-10) {
    return 0.0;
  }
  double zScore = (maxVal-mean) / maxStd;
  return zScore;
}

/** returns "1.0" for perfectly "sharp" predictions
    (each row with all zeros and one "1"),
    final result is smaller one otherwise
*/
double
contactMatrixQuality(const Vec<Vec<double> >& matrix, unsigned int diagLim)
{
  if (matrix.size() == 0) {
    return 0.0;
  }
  double maxZScore = contactMatrixRowMaxScore(matrix.size());
  double sum = 0.0;
  for (unsigned int i = 0; i < matrix.size(); ++i) {
    sum += contactMatrixRowScore(matrix[i], i, diagLim);
  }
  sum /= maxZScore; // renormalize each row to a maximum score of one
  return (sum / matrix.size()); // renormalize to total score maximal "1"
}

/** in each row, leave only the highest scoring element,
 also delete diagonal */
void
winnerTakesAll(Vec<Vec<double> >& matrix, int diagBorder, double cutoff)
{
  // store all promising candidates:
  Vec<RankedSolution5<int, int> > positions;
  int nn = matrix.size();
  for (int i = 0; i < nn; ++i) {
    for (int j = 0; j + diagBorder <= i; ++j) {
      if (matrix[i][j] >= cutoff) {
	positions.push_back(RankedSolution5<int, int>(matrix[i][j], i, j));
      }
    }
  }
  // sort:
  sort(positions.begin(), positions.end());
  reverse(positions.begin(), positions.end()); // highest score first:
  int n, m;
  while (positions.size() > 0) {
    n = positions[0].second;
    m = positions[0].third;
    // delete all competing interactions:
    for (int i = 0; i < nn; ++i) {
      if (i == n) {
	continue;
      }
      matrix[i][m] = 0.0;
      matrix[m][i] = 0.0;
      // delete competing positions:
      for (int k = static_cast<int>(positions.size())-1; k >= 0; --k) {
	if (((positions[k].second == i) && (positions[k].third == m)) 
	    || ((positions[k].second == m) && (positions[k].third == i))) {
	  positions.erase(positions.begin() + k);
	  break; // list is unique, we can quite loop here
	}
      }
    }
    for (int i = 0; i < nn; ++i) {
      if (i == m) {
	continue;
      }
      matrix[n][i] = 0.0;
      matrix[i][n] = 0.0;
      // delete competing positions:
      for (int k = static_cast<int>(positions.size())-1; k >= 0; --k) {
	if (((positions[k].second == i) && (positions[k].third == n)) 
	    || ((positions[k].second == n) && (positions[k].third == i))) {
	  positions.erase(positions.begin() + k);
	  break; // list is unique, we can quite loop here
	}
      }
    }
    // delete first element
    if (positions.size() > 0) {
      positions.erase(positions.begin());
    }
  }
}




/** sets all elements i,i-diagBorder to i,i+diagBorder to specified value. */
void
fillDiagonal(Vec<Vec<double> >& matrix, int diagBorder, double value) {
  for (int i = 0; i <  static_cast<int>(matrix.size()); ++i) {
    for (int j = i-diagBorder; j <= i+diagBorder; ++j) {
      if ((j < 0) || (j >= static_cast<int>(matrix.size()))) {
	continue;
      }
      matrix[i][j] = value;
      matrix[j][i] = value;
    }
  }
}







#endif /* __A_CLASS_H__ */


