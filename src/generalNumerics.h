#ifndef __GENERAL_NUMERICS__
#define __GENERAL_NUMERICS__

#include <libintl.h>
#include <math.h>
#include "debug.h"
#include "realnumerics.h"

/** Returns log odds, typically the logarithm of a likelihood ratio */
inline
double logOdds(double p) {
THROWIFNOT((p > 0.0 ) && (p < 1.0),"Assertion violation in L12");
  return log(p / (1-p));
}

template <class T>
int 
signum(T x)
{
  if (x > 0) {
    return 1;
  }
  else if (x < 0) {
    return -1;
  }
  return 0;
}

double factorial(int n);

double logFactorial(int n);

double nOverK(int n, int k); 

double logNOverK(int n, int k);

double lg2(double x);

/** returns logistic function */
double logistic(double x);

/** delta function */
template <class T>
int 
deltaFunction(const T& x, const T& y)
{
  if (x == y) {
    return 1;
  }
  return 0;
}

/** returns poisson function value */
double poisson(int k, double expected);

/** returns poisson function value */
double poissonCdf(int k, double expected);

/** how likely is it to observe this many or more counts?
 * same as probability as NOT observing less counts */
double poissonCdfP(int k, double expected);


double
factorial(int n)
{
  double result = 1.0;
  for (int i = 2; i <= n; ++i) {
    result *= i;
  }
  POSTCOND(isDefined(result));
  return result;
}

double
logFactorial(int n)
{
  double result = 0.0;
  for (int i = 2; i <= n; ++i) {
    result += log(static_cast<double>(i));
  }
  POSTCOND(isDefined(result));
  return result;
}


/** returns poisson function value of observing k rare independent events if "expected" events are expected */
double poisson(int k, double expected) {
  return pow(expected, k) * exp(-expected) / factorial(k);
}

/** returns poisson function value of observing k rare independent events if "expected" events are expected */
double logPoisson(int k, double expected) {
  return (k*log(expected)) - expected - logFactorial(k);
}

/** returns poisson function value of observing k or less rare independent events if "expected" events are expected */
double poissonCdf(int k, double expected) {
  double sum = 0.0;
  for (int i = 0; i <= k; ++i) {
    sum += exp((i* log(expected)) - logFactorial(i));
  }
  double result = exp(-expected) * sum;
  if (result > 1.0) {
    result = 1.0;
  } else if (result < 0.0) {
    result = 0.0;
  }
  POSTCOND((result <= 1.0) && (result >= 0.0));
  return result;
}

/** how likely is it to observe this many or more counts?
 * same as probability as NOT observing less counts */
double poissonCdfP(int k, double expected) {
  double result = 1.0;
  if (k > 0) {
    result = 1.0 - poissonCdf(k - 1, expected);
  }
  ERROR_IF(result < -0.1 , "Internal error computing P value based on Poisson distribution. Obtained value less than -0.1");
  ERROR_IF(result > 1.1 , "Internal error computing P value based on Poisson distribution. Obtained value greater than 1.1");
  if (result < -0.1) {
    result = 0.0;
  }
  if (result > 1.1) {
    result = 1.0;
  }
  POSTCOND((result >= 0.0) || (result <= 1.0));
  return result;
}

double
nOverK(int n, int k) 
{
THROWIFNOT(n > 0,"Assertion violation in L135");
THROWIFNOT(k > 0,"Assertion violation in L136");
THROWIFNOT(n >= k,"Assertion violation in L137");
//   if (k > n) {
//     return 0.0;
//   }
//   if (k == 1) {
//     return static_cast<double>(n);
//   }
//   if ((k == n) || (k == 0)) {
//     return 0.0;
//   }
  double result = (factorial(n) / factorial(k)) / factorial(n - k);
  POSTCOND(isDefined(result)); // avoid not-a-number (overflow) case
  return result;
}

double
logNOverK(int n, int k) 
{
THROWIFNOT(n > 0,"Assertion violation in L155");
THROWIFNOT(k > 0,"Assertion violation in L156");
THROWIFNOT(n >= k,"Assertion violation in L157");
//   if (k > n) {
//     ASSERT(false);
//     return 0.0;
//   }
//   if (k == 1) {
//     return log(static_cast<double>(n));
//   }
//   if ((k == n) || (k == 0)) {
//     ASSERT(false);
//     return 0.0;
//   }
  double result = (logFactorial(n) - logFactorial(k)) - logFactorial(n - k);
  POSTCOND(isDefined(result)); // avoid not-a-number (overflow) case
  return result;
}

double
lg2(double x)
{
  const double LOG2 = log(2.0);
  if (x <= 0.0) {
    return 0.0;
  }
  return log(x) / LOG2;
}

/** returns logistic function */
double
logistic(double x)
{
  double result = (exp(x) / (1 + exp(x)));
  // ASSERT((result >= 0.0) && (result <= 1.0), exception);
  return result;
}




#endif
