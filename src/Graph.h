#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <algorithm>
#include <limits>
#include <queue>
#include <utility>
#include <set>
#include "Vec.h"

using namespace std;

// http://codeclassics.blogspot.com/2012/12/dijkstras-algorithm-in-c-using-stl.html

typedef int graph_node_t;
typedef graph_node_t vertex_t; // synonym
typedef double graph_weight_t;

typedef pair<graph_node_t, graph_weight_t> neighbor;

/** Graph representation is weighted potentially directed adjacency list */
typedef Vec<Vec<pair<graph_node_t,graph_weight_t> > > Graph;

typedef Vec<map<graph_node_t,graph_weight_t> >  Graph2; // faster version of graph using setso

const graph_weight_t max_weight = std::numeric_limits<double>::infinity();

class NodeWeightComparator {
 public:
  int operator() ( const pair<graph_node_t,graph_weight_t>& p1, const pair<graph_node_t,graph_weight_t>& p2)
  {
    return p1.second>p2.second;
  }
};

inline
void 
dijkstra(const Graph  &G,const int &source,const int &destination,Vec<int> &path)
{
  DEBUG_MSG("Starting dijkstra");
  ERROR_IF(source == destination, "Source and destination must be distinct vertices!");
  Vec<double> d(G.size());
  Vec<int> parent(G.size());
  for(unsigned int i = 0 ;i < G.size(); i++) {
    d[i] = std::numeric_limits<double>::max();
    parent[i] = -1;
  }
  priority_queue<pair<int,double>, Vec<pair<int,double> >, NodeWeightComparator> Q;
THROWIFNOT(source < static_cast<int>(d.size()),"Assertion violation in L50");
  d[source] = 0.0f;
  Q.push(make_pair(source,d[source]));
  while(!Q.empty())   {
      int u = Q.top().first;
THROWIFNOT(u < static_cast<int>(G.size()),"Assertion violation in L55");
      if(u==destination) break;
      Q.pop();
      for(size_t i=0; i < G[u].size(); i++) {
THROWIFNOT(i < G[u].size(),"Assertion violation in L59");
	int v= G[u][i].first;
	double w = G[u][i].second;
THROWIFNOT(v < static_cast<int>(d.size()),"Assertion violation in L62");
THROWIFNOT(u < static_cast<int>(d.size()),"Assertion violation in L63");
	if(d[v] > d[u]+w)
	  {
	    d[v] = d[u]+w;
THROWIFNOT(v < static_cast<int>(parent.size()),"Assertion violation in L67");
	    parent[v] = u;
	    Q.push(make_pair(v,d[v]));
	  }
      }
  }
  path.clear();
  int p = destination;
  while(p!=source) {
    if (p >= static_cast<int>(parent.size())) {
     Rcpp::Rcout << "# Warning: no shortest path between graph vertices could be identified!" << endl;
     path.clear();
     return;
    }
   path.push_back(destination);

THROWIFNOT(p < static_cast<int>(parent.size()),"Assertion violation in L83");
    p = parent[p];
    path.push_back(p);
  }
  set<int> nodeSet;
  for (size_t i = 0; i < path.size(); ++i) {
    ERROR_IF(nodeSet.find(path[i]) != nodeSet.end(), "Path contains the same node twice!");
    nodeSet.insert(path[i]);
  }
  DEBUG_MSG("Finished dijkstra");
}

inline
bool 
graphIsConnected(const Graph& g, graph_node_t nodeIdFrom, graph_node_t nodeIdTo) {
  for (size_t i = 0; i < g[nodeIdFrom].size(); ++i) {
    if (g[nodeIdFrom][i].first == nodeIdTo) {
      return true;
    }
  }
  return false;
}

inline
bool 
graphIsConnected(const Graph2& g, graph_node_t nodeIdFrom, graph_node_t nodeIdTo) {
  return (g[nodeIdFrom].find(nodeIdTo)) != g[nodeIdFrom].end();
}


inline
void
graphAddEdge(Graph& g, graph_node_t nodeIdFrom, graph_node_t nodeIdTo, double weight) {
  PRECOND(static_cast<size_t>(nodeIdFrom) < g.size());
  PRECOND(!graphIsConnected(g, nodeIdFrom, nodeIdTo));
  g[nodeIdFrom].push_back(make_pair(nodeIdTo, weight));
}

inline
void
graphAddEdge(Graph2& g, graph_node_t nodeIdFrom, graph_node_t nodeIdTo, double weight) {
  PRECOND(static_cast<size_t>(nodeIdFrom) < g.size());
  PRECOND(!graphIsConnected(g, nodeIdFrom, nodeIdTo));
  g[nodeIdFrom].insert(make_pair(nodeIdTo, weight));
}

inline
ostream& operator << (ostream& os, const Graph& g) {
  for (size_t i = 0; i < g.size(); ++i) {
    os << (i+1) << " :";
    for (size_t j = 0; j < g[i].size(); ++j) {
      ERROR_IF(g[i][j].first < 0, "internal error: negative graph node id encountered.");
      os << ' ' << (g[i][j].first+1) << ";" << g[i][j].second;
    }
    os << endl;
  }
  return os;
}

inline
ostream& operator << (ostream& os, const Graph2& g) {
  for (size_t i = 0; i < g.size(); ++i) {
    os << (i+1) << " :";
    for (auto jt = g[i].begin(); jt != g[i].end(); ++jt) {
      os << ' ' << (jt->first+1) << ";" << jt->second;
    }
#ifndef NDEBUG
    os << "# ( " << g[i].size() << " outgoing edges )";
#endif
    os << endl;
  }
  return os;
}

/* according to Cormen et al: Algorithms.
*/
inline
Vec<int> 
findDijkstraCormen(const Graph& g, graph_node_t source, graph_node_t destination) {
  DEBUG_MSG("Starting findDijstraCormen");
  PRECOND(source >= 0);
  PRECOND(source < static_cast<int>(g.size()));
#ifndef NDEBUG
  Rcpp::Rcout << "Attempting to find path from node " << (source+1) << " to " << (destination+1) << endl;
  Rcpp::Rcout << "For graph:" <<endl;
  Rcpp::Rcout << g;
#endif
  // map destination paths
  // initialize-single-source:
  size_t n = g.size();
  priority_queue<pair<int,double>, Vec<pair<int,double> >, NodeWeightComparator> Q;
  Vec<graph_node_t> pred(g.size(), -1); // predecessor vertices (negative one corresponds to NIL)
  Vec<graph_weight_t> d(g.size(), std::numeric_limits<graph_weight_t>::max()); // set to maximum allowed value
  d[source] = 0; // no distance from source to itself
  set<graph_node_t> S;
  for (size_t i = 0; i < n; ++i) {
    Q.push(make_pair(i, d[i]));
  }
  size_t iteration = 0;
  while (Q.size() > 0) {
    Rcpp::Rcout << "Shortest path Iteration " << ++iteration << " pred : " << pred << " d: " << d;
    graph_node_t u = Q.top().first; // next best id with shortest distance to source. 
    Q.pop();
    // the very first node should be source:
    ERROR_IF((S.size() == 0) && (u != source), "Error in Dijkstra algorithm (implementation according to Cormen - Algorithms: very first node should be equal to source");
    S.insert(u);
    for (size_t i = 0; i < g[u].size(); ++i) {
      graph_node_t v = g[u][i].first; // adjacent node id
      // Relax(u,v, w):
      graph_weight_t w_uv = g[u][i].second;
      if (d[v] > (d[u] + w_uv)) {
	d[v] = d[u] + w_uv; // go through node u to reach node v?
	pred[v] = u;
      }
    }
  }
  Vec<int> result;
  graph_node_t curr = destination;
  result.push_back(curr);
  while (pred[curr] >= 0) {
    result.push_back(pred[curr]);
    curr = pred[curr];
  }
  reverse(result.begin(), result.end());
  // ERROR_IF(!((result[0] == source) && (result[result.size()-1] == destination)), "Could not find path between source and destination.");
  if(!((result[0] == source) && (result[result.size()-1] == destination)) ) {  // , "Could not find path between source and destination.");
    DEBUG_MSG("# Warning : clearing shortest path.");
    Rcpp::Rcout << result;
    result.clear();
  }
  DEBUG_MSG("Finished findDijstraCormen");
  return result;
}

// code according to RosettaCode:
inline
void
DijkstraComputePaths(vertex_t source,
		     const Graph &adjacency_list,
		     std::vector<graph_weight_t> &min_distance,
		     std::vector<vertex_t> &previous) {
#ifndef NDEBUG
  Rcpp::Rcout << "Starting DijkstraComputePaths with " << source << " adjacency list size " << adjacency_list.size() << " and previous path: ";
  for (size_t i = 0; i < previous.size(); ++i) {
    Rcpp::Rcout << previous[i] << " ";
  }
#endif
  int n = adjacency_list.size();
  min_distance.clear();
  min_distance.resize(n, max_weight);
THROWIFNOT(source < static_cast<vertex_t>(min_distance.size()),"Assertion violation in Graph.h :  L233");
  min_distance[source] = 0;
  previous.clear();
  previous.resize(n, -1);
  std::set<std::pair<graph_weight_t, vertex_t> > vertex_queue;
  vertex_queue.insert(std::make_pair(min_distance[source], source));
  DEBUG_MSG("Mark Graphh-238");
  while (!vertex_queue.empty()) {
    graph_weight_t dist = vertex_queue.begin()->first;
    vertex_t u = vertex_queue.begin()->second;
    vertex_queue.erase(vertex_queue.begin());
    // Visit each edge exiting u
    const std::vector<neighbor> &neighbors = adjacency_list[u];
    DEBUG_MSG("Mark Graphh-245");
    for (auto neighbor_iter = neighbors.begin(); neighbor_iter != neighbors.end(); neighbor_iter++) {
      DEBUG_MSG("Mark Graphh-247");
      vertex_t v = neighbor_iter->first; // target;
      graph_weight_t weight = neighbor_iter->second; // weight;
      graph_weight_t distance_through_u = dist + weight;
      if (distance_through_u < min_distance[v]) {
	vertex_queue.erase(std::make_pair(min_distance[v], v));
	min_distance[v] = distance_through_u;
	previous[v] = u;
	vertex_queue.insert(std::make_pair(min_distance[v], v));
      }
    }
  }
#ifndef NDEBUG
  Rcpp::Rcout << "# Result of Dijkstra algorithm: ";
  for (size_t i = 0; i < previous.size(); ++i) {
    Rcpp::Rcout << previous[i];
  }
#endif
}
 

// code according to RosettaCode:
inline
void
DijkstraComputePaths(vertex_t source,
		     const Graph2 &adjacency_list,
		     std::vector<graph_weight_t> &min_distance,
		     std::vector<vertex_t> &previous) {
#ifndef NDEBUG
  Rcpp::Rcout << "Starting DijkstraComputePaths with source id " << (source+1) << " min distances " << min_distance.size() << " and adjacency list size " << adjacency_list.size() << " and previous path: ";
  for (size_t i = 0; i < previous.size(); ++i) {
    Rcpp::Rcout << previous[i] << " ";
  }
#endif
  PRECOND(source < static_cast<vertex_t>(adjacency_list.size()));
  int n = adjacency_list.size();
  min_distance.clear();
  min_distance.resize(n, max_weight);  
THROWIFNOT(source < static_cast<vertex_t>(min_distance.size()),"Assertion violation in Graph.h :  L286");
THROWIFNOT(static_cast<int>(min_distance.size()) == n,"Assertion violation in Graph.h :  L287");
  min_distance[source] = 0;
  previous.clear();
  previous.resize(n, -1);
THROWIFNOT(static_cast<int>(previous.size()) == n,"Assertion violation in Graph.h :  L291");
  std::set<std::pair<graph_weight_t, vertex_t> > vertex_queue;
  vertex_queue.insert(std::make_pair(min_distance[source], source));
  while (!vertex_queue.empty()) {
    DEBUG_MSG("Graphh-295");
      graph_weight_t dist = vertex_queue.begin()->first;
      vertex_t u = vertex_queue.begin()->second;
      vertex_queue.erase(vertex_queue.begin());
      // Visit each edge exiting u
THROWIFNOT(u < static_cast<vertex_t>(adjacency_list.size()),"Assertion violation in Graph.h :  L300");
      const std::map<graph_node_t, graph_weight_t> &neighbors = adjacency_list[u];
      for (auto neighbor_iter = neighbors.begin(); neighbor_iter != neighbors.end(); neighbor_iter++) {
	  vertex_t v = neighbor_iter->first; // target;
	  graph_weight_t weight = neighbor_iter->second; // weight;
	  graph_weight_t distance_through_u = dist + weight;
THROWIFNOT(v < static_cast<vertex_t>(min_distance.size()),"Assertion violation in Graph.h :  L306");
	  if (distance_through_u < min_distance[v]) {
	    vertex_queue.erase(std::make_pair(min_distance[v], v));
	    min_distance[v] = distance_through_u;
THROWIFNOT(v < static_cast<vertex_t>(previous.size()),"Assertion violation in Graph.h :  L310");
	    previous[v] = u;
	    vertex_queue.insert(std::make_pair(min_distance[v], v));
	  }
        }
    }
#ifndef NDEBUG
  Rcpp::Rcout << "Finished DijkstraComputePaths with " << source << " adjacency list size " << adjacency_list.size() << " and previous path: ";
  for (size_t i = 0; i < previous.size(); ++i) {
    Rcpp::Rcout << previous[i] << " ";
  }
#endif
}

inline
Vec<vertex_t>
DijkstraGetShortestPathTo(vertex_t vertex, const std::vector<vertex_t> &previous)
{
  Vec<vertex_t> path;
THROWIFNOT((vertex == -1) || ((vertex >= 0) && (vertex < static_cast<vertex_t>(previous.size()))),"Assertion violation in L329");
  for ( ; vertex != -1; vertex = previous[vertex]) {
    path.push_back(vertex);
THROWIFNOT((vertex == -1) || ((vertex >= 0) && (vertex < static_cast<vertex_t>(previous.size()))),"Assertion violation in L332");
  }
  reverse(path.begin(), path.end());
  return path;
}
 
inline
void
testDijkstraRosetta() {
  // remember to insert edges both ways for an undirected graph
  Graph adjacency_list(6);
  // 0 = a
  adjacency_list[0].push_back(neighbor(1, 7));
  adjacency_list[0].push_back(neighbor(2, 9));
  adjacency_list[0].push_back(neighbor(5, 14));
  // 1 = b
  adjacency_list[1].push_back(neighbor(0, 7));
  adjacency_list[1].push_back(neighbor(2, 10));
  adjacency_list[1].push_back(neighbor(3, 15));
  // 2 = c
  adjacency_list[2].push_back(neighbor(0, 9));
  adjacency_list[2].push_back(neighbor(1, 10));
  adjacency_list[2].push_back(neighbor(3, 11));
  adjacency_list[2].push_back(neighbor(5, 2));
  // 3 = d
  adjacency_list[3].push_back(neighbor(1, 15));
  adjacency_list[3].push_back(neighbor(2, 11));
  adjacency_list[3].push_back(neighbor(4, 6));
  // 4 = e
  adjacency_list[4].push_back(neighbor(3, 6));
  adjacency_list[4].push_back(neighbor(5, 9));
  // 5 = f
  adjacency_list[5].push_back(neighbor(0, 14));
  adjacency_list[5].push_back(neighbor(2, 2));
  adjacency_list[5].push_back(neighbor(4, 9));
 
  std::vector<graph_weight_t> min_distance;
  std::vector<vertex_t> previous;
  DijkstraComputePaths(0, adjacency_list, min_distance, previous);
  Rcpp::Rcout << "Distance from 0 to 4: " << min_distance[4] << std::endl;
  Vec<vertex_t> path = DijkstraGetShortestPathTo(4, previous);
  Rcpp::Rcout << "Path : ";
  std::copy(path.begin(), path.end(), std::ostream_iterator<vertex_t>(Rcpp::Rcout, " "));
  Rcpp::Rcout << std::endl;
 
}

inline
void
testDijkstraRosetta2() {
  // remember to insert edges both ways for an undirected graph
  Graph2 adjacency_list(6);
  // 0 = a
  adjacency_list[0].insert(neighbor(1, 7));
  adjacency_list[0].insert(neighbor(2, 9));
  adjacency_list[0].insert(neighbor(5, 14));
  // 1 = b
  adjacency_list[1].insert(neighbor(0, 7));
  adjacency_list[1].insert(neighbor(2, 10));
  adjacency_list[1].insert(neighbor(3, 15));
  // 2 = c
  adjacency_list[2].insert(neighbor(0, 9));
  adjacency_list[2].insert(neighbor(1, 10));
  adjacency_list[2].insert(neighbor(3, 11));
  adjacency_list[2].insert(neighbor(5, 2));
  // 3 = d
  adjacency_list[3].insert(neighbor(1, 15));
  adjacency_list[3].insert(neighbor(2, 11));
  adjacency_list[3].insert(neighbor(4, 6));
  // 4 = e
  adjacency_list[4].insert(neighbor(3, 6));
  adjacency_list[4].insert(neighbor(5, 9));
  // 5 = f
  adjacency_list[5].insert(neighbor(0, 14));
  adjacency_list[5].insert(neighbor(2, 2));
  adjacency_list[5].insert(neighbor(4, 9));
 
  std::vector<graph_weight_t> min_distance;
  std::vector<vertex_t> previous;
  DijkstraComputePaths(0, adjacency_list, min_distance, previous);
  Rcpp::Rcout << "Distance from 0 to 4: " << min_distance[4] << std::endl;
  Vec<vertex_t> path = DijkstraGetShortestPathTo(4, previous);
  Rcpp::Rcout << "Path : ";
  std::copy(path.begin(), path.end(), std::ostream_iterator<vertex_t>(Rcpp::Rcout, " "));
  Rcpp::Rcout << std::endl;
 
}



/*
int main()
{
/// Graph
    GRAPH TYPE = UNDIRECTED
    NUMBER OF VERTICES = 6 indexed from 0 to 5
    NUMBER OF EDGES = 9
    edge 0->1 weight = 7
    edge 0->2 weight = 9
    edge 0->5 weight = 14
    edge 1->2 weight = 10
    edge 1->3 weight = 15
    edge 2->5 weight = 2
    edge 2->3 weight = 11
    edge 3->4 weight = 6
    edge 4->5 weight = 9
//
Graph g;
g.resize(6);
g[0].push_back(make_pair(1,7));
g[1].push_back(make_pair(0,7));

g[0].push_back(make_pair(2,9));
g[2].push_back(make_pair(0,9));

g[0].push_back(make_pair(5,14));
g[5].push_back(make_pair(0,14));

g[1].push_back(make_pair(2,10));
g[2].push_back(make_pair(1,10));

g[1].push_back(make_pair(3,15));
g[3].push_back(make_pair(1,15));

g[2].push_back(make_pair(5,2));
g[5].push_back(make_pair(2,2));

g[2].push_back(make_pair(3,11));
g[3].push_back(make_pair(2,11));

g[3].push_back(make_pair(4,6));
g[4].push_back(make_pair(3,6));

g[4].push_back(make_pair(5,9));
g[5].push_back(make_pair(4,9));
Vec<int> path;
dijkstra(g,0,4,path);
for(int i=path.size()-1;i>=0;i--)
  Rcpp::Rcout<<path[i]<<"->";
return 0;
}

*/

#endif
