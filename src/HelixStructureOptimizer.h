#ifndef HELIX_STRUCTURE_OPTIMIZER
#define HELIX_STRUCTURE_OPTIMIZER

#include "Stem.h"
#include "StemTools.h"
#include "ResidueStructure.h"
#include "ResidueStructureOptimizer.h"
#include "GraphTools.h"
#include "GraphTools.h"
#include "MultiIterator.h"
// #include "IndyFold.h"

class HelixStructureOptimizer : ResidueStructureOptimizer {

 private:

  bool allowMelting; // not only explore branch migration, allow melting away of one helix
  bool guAllowed;
  int lenDiffMax; 
  /** Minimum length of added unfolded helices */
  int tinyHelixLengthMin;
  /** Maximum number of unfolded helices for each component. Reasonable values are 0 or 1 */
  size_t unfoldedHelicesMax;
  int verbose;

 public:

 HelixStructureOptimizer() : allowMelting(true), guAllowed(true), lenDiffMax(4), tinyHelixLengthMin(4), unfoldedHelicesMax(0), verbose(1) {  }
  
  virtual ~HelixStructureOptimizer() { }

  virtual ResidueStructure optimize(const ResidueStructure& structure) const {
    int lenMin = 2;
    StrandContainer strands = structure.getStrands();
    const Vec<size_t>& seqIds = structure.getStrandIds();
    string seq = strands.getConcatSequence();
    Vec<Stem> stemsTinyOrig = StemTools::generateAllStems(&strands, strands.getConcatSequence(), strands.getStrandIds(), guAllowed, verbose);
    Vec<Stem> stemsTiny;
    for (size_t i = 0; i < stemsTinyOrig.size(); ++i) {
      if ((stemsTinyOrig[i].getLength() >= tinyHelixLengthMin) && structure.isPlaceable(stemsTinyOrig[i])) {
	stemsTiny.push_back(stemsTinyOrig[i]);
      }
    }
    set<Stem> longestTinyStemSet;
    for (size_t i = 0; i < stemsTiny.size(); ++i) {
      longestTinyStemSet.insert(StemTools::elongateStem(stemsTiny[i], seq, seqIds, guAllowed));
    }
    /* Vec<Vec<Stem> > longestUnfoldedHelixSubstems(helices.size()); // substems corresponding to unfolded helices */
    /* for (auto it = longestTinyStemSet.begin(); it!= longestTinyStemSet.end(); it++) { */
    /*   longestUnfoldedHelixSubstems.push_back(StemTools::generateSubStemsWithSelf(*it, tinyHelixLengthMin)); */
    /* }	 */
    const Vec<Stem>& helices = structure.getHelices();
    // Rcpp::Rcout << "# Called HelixStructureOptimizer with helices " << structure.getHelices() << endl;
    Vec<Stem> longest;
    // const Vec<string>& sequences = structure.getSequences();
				// string seq;

    /* for (size_t i = 0; i < sequences.size(); ++i) { */
    /*   seq.append(sequences[i]); */
    /* } */
    for (size_t i = 0; i < helices.size(); ++i) {
      longest.push_back(StemTools::elongateStem(helices[i], seq, seqIds, guAllowed));
    }
    Vec<Vec<Stem> > longestSubstems(helices.size());
    for (size_t i = 0; i < helices.size(); ++i) {
      longestSubstems[i] = StemTools::generateSubStemsWithSelf(longest[i], lenMin);
    }
    // if (verbose > VERBOSE_INFO) {
    //   Rcpp::Rcout << "# Current helices of structure: " << helices << endl;
    //   Rcpp::Rcout << "# Elongated helices of structure: " << longest << endl;
    // }
    // Rcpp::Rcout << "Substems of elongated helices: " << endl << longestSubstems << endl;
    adjacency_list_t adj = stemCommonBasesToAdjacency(longest);
    graph_id_sets_t components = adjacency_to_components(adj);
    // if (verbose >= VERBOSE_INFO) {
    //   Rcpp::Rcout << "# Found " << components.size() << "  components: " << components << endl;
    // }
//     if (verbose > VERBOSE_INFO) {
//       Rcpp::Rcout << "# All helices of all components:" << endl;
//       for (auto it = components.begin(); it != components.end(); it++) {
// 	      graph_id_set_t s = *it;
// 	      Rcpp::Rcout << " Component: " << endl;
// 	      for (auto it2 = s.begin(); it2 != s.end(); it2++) {
// 	        Rcpp::Rcout << longestSubstems[*it2] << endl;
// 	      }
//       }
//     }
    for (auto it = components.begin(); it != components.end(); it++) {
      graph_id_set_t s = *it;
      for (auto it2 = it; it2 != components.end(); it2++) {
	if (it2 != it) {
	  graph_id_set_t s2 = *it2;
	  for (auto it3 = s.begin(); it3 != s.end(); it3++) {
	    const Stem& stem1 = longestSubstems[*it3][0];
	    for (auto it4 = s2.begin(); it4 != s2.end(); it4++) {
	      const Stem& stem2 = longestSubstems[*it4][0];
THROWIFNOT(!stem1.hasCommonBases(stem2),"Assertion violation in HelixStructureOptimizer.h :  L97");
	    }
	  }
	}
      }
    }
    Vec<Stem> allNewStems;
    for (auto it = components.begin(); it != components.end(); ++it) {
      graph_id_set_t component = *it;
  //    if (verbose >= VERBOSE_INFO) {
//	Rcpp::Rcout << "# Optimizing  component " << (++pc) << " : "; 
//	for (auto it3 = component.begin(); it3 != component.end(); it3++) {
//	  Stem stem = helices[*it3];
	//  Rcpp::Rcout << stem << " ; "; 
//	  }
//	Rcpp::Rcout << endl; 
//      }
      Vec<Stem> addedUnfoldedStems;
      if (unfoldedHelicesMax > 0) { // add unfolded helices?	
	for (auto it2 = longestTinyStemSet.begin(); it2 != longestTinyStemSet.end(); it2++) {
	  bool found = false;
	  Stem unplacedStem = *it2;
	  for (auto it3 = component.begin(); it3 != component.end(); it3++) {
	    Stem stem = helices[*it3];
	    if (unplacedStem.hasCommonBases(stem)) {
	      found = true;
	      break;
	    }
	  }
	  if (found) {
	    addedUnfoldedStems.push_back(unplacedStem);
	  }
	}
      }
//       if (verbose > VERBOSE_INFO) {
// 	Rcpp::Rcout << "# Added the following unfolded helices: " << addedUnfoldedStems;
//       }
      size_t unfoldedStemIdStart = longestSubstems.size(); // stems with this or higher ids correspond to unfolded stems
      Vec<Vec<Stem> > longestSubstemsWork = longestSubstems;
      for (size_t i = 0; i < addedUnfoldedStems.size(); ++i) {
	longestSubstemsWork.push_back(StemTools::generateSubStemsWithSelf(longest[i], tinyHelixLengthMin));
    }
      Vec<Stem> newStems = optimizeComponent(structure, *it, longestSubstems, unfoldedStemIdStart);
      // EASSERT(newStems.size() > 0);
//       if (verbose >= VERBOSE_INFO) {
// 	Rcpp::Rcout << "# Optimized result of component " << pc << " : " << newStems;
// 	Rcpp::Rcout << "# Previously placed stems of components: " << allNewStems;
//       }
      for (size_t i = 0; i < newStems.size(); ++i) {
	for (size_t j = 0; j < allNewStems.size(); ++j) {
THROWIFNOT(!newStems[i].hasCommonBases(allNewStems[j]),"Assertion violation in HelixStructureOptimizer.h :  L147");
	}
      }
      allNewStems.insert(allNewStems.end(), newStems.begin(), newStems.end());
THROWIFNOT(allNewStems.size() >= newStems.size(),"Assertion violation in HelixStructureOptimizer.h :  L151");
    }
    ResidueStructure result(structure.getSequences(), structure.getConcentrations(), structure.getResidueModel());
    try {
//       if (verbose > VERBOSE_INFO) {
// 	Rcpp::Rcout << "# FINALLY trying to set best combination of helices: " << allNewStems;
//       }
      result.setStems(allNewStems);
    } catch(int e) {
     // Rcpp::Rcout << allNewStems << endl;
      DERROR("Internal error in HelixStructureOptimizer: inconsiten structure encountered.");
    }
    // if (verbose > VERBOSE_QUIET) {
    //   Rcpp::Rcout << "# Result of opimitzation" << result.computeAbsoluteFreeEnergy() << " from " << structure.computeAbsoluteFreeEnergy() << " changed: " << changed << endl;
    //   Rcpp::Rcout << "# Tried to set best combination of stems: " << allNewStems;
    // }
THROWIFNOT(structure.countBasePairs() == 0 || result.countBasePairs() > 0,"Assertion violation in HelixStructureOptimizer.h :  L167");
    return result;
  }

  void setAllowMelting(bool mode) { allowMelting = mode; }

  void setVerbose(int _verbose) {
    verbose = _verbose;
  }

 private:

  Vec<Stem> optimizeComponent(const ResidueStructure& structure, const graph_id_set_t& component, 
			      const Vec<Vec<Stem> >& longestSubstems, size_t unfoldedStemIdStart) const {
    // if (verbose > VERBOSE_INFO) {
    //   Rcpp::Rcout << "# Opimizing component " << component << endl;
    // }
    Vec<Stem> result;
    ResidueStructure emptyStructure(structure.getSequences(), structure.getConcentrations(), structure.getResidueModel());
    const Vec<Stem>& helices = structure.getHelices();
    EASSERT(!StemTools::hasCommonBases(helices)); // helices must not use same nucleotides
    unsigned int long combos = 1;
    MultiIterator::container_t compVec(component.size());
    MultiIterator::container_t compVecSizes(component.size());
    // Rcpp::Rcout << "Optimization for component " << component << endl;
    int pc = 0;
    Vec<Stem> remainingHelices;
    Vec<Stem> changeHelicesOrig;
    for (size_t i = 0; i < helices.size(); ++i) {
      if (component.find(i) == component.end()) {
	remainingHelices.push_back(helices[i]);
      } else {
	changeHelicesOrig.push_back(helices[i]);
      }
    }
THROWIFNOT(changeHelicesOrig.size() == component.size(),"Assertion violation in HelixStructureOptimizer.h :  L202");
THROWIFNOT(remainingHelices.size() + component.size() == helices.size(),"Assertion violation in HelixStructureOptimizer.h :  L203");
    for (auto it = component.begin(); it != component.end(); it++) {
      graph_base_t id = *it;
      combos *= longestSubstems[id].size();
      // result.push_back(helices[id]);
      compVec[pc] = id; // index zero: not placed at all; index one: full-length helix is placed
      compVecSizes[pc] = longestSubstems[id].size()+1; // index zero: not placed at all; index one: full-length helix is placed (already included)
THROWIFNOT(compVecSizes[pc] >= 1,"Assertion violation in HelixStructureOptimizer.h :  L210");
      ++pc;
    }
    // if (verbose > 1) {
    //   Rcpp::Rcout << "# The combination vector is " << compVecSizes;
    // }
    MultiIterator multi(compVecSizes);
    double gBest = structure.computeAbsoluteFreeEnergy();
    Vec<Stem> helicesChangedBest = changeHelicesOrig; // oringal helices  - to be changed
    Vec<Stem> helicesAllBest = helices; // original helices
    Vec<Stem> trial;
    Vec<Stem> changedStems;
THROWIFNOT(!StemTools::hasCommonBases(helicesChangedBest),"Assertion violation in HelixStructureOptimizer.h :  L222");
THROWIFNOT(!StemTools::hasCommonBases(helicesAllBest),"Assertion violation in HelixStructureOptimizer.h :  L223");
    bool everChanged = false;
    // bool everValid = false; // at least one of the many tried structures needs to validate (see POSTCOND)
    do {
      trial = remainingHelices;
      changedStems.clear();
THROWIFNOT(changedStems.size() == 0,"Assertion violation in HelixStructureOptimizer.h :  L229");
      MultiIterator::container_t digits = multi.getDigits();
      bool valid = true;      
//       if (verbose > 1) {
// 	Rcpp::Rcout << "# trying digit combo " << digits;
//       }
      int unfoldedPlacedCount = 0;
      for (size_t i = 0; i < unfoldedStemIdStart; ++i) {
	if (digits[i] == 0) {
	  if (!allowMelting) { // zeros are not allowed if all original helices of a locus have to be present
	    valid = false;
	    goto LOOP_LAST;
	  }
	}
      }

      for (size_t i = unfoldedStemIdStart; i < digits.size(); ++i) {
	if (digits[i] == 0) {
	  ++unfoldedPlacedCount;
	  if (unfoldedPlacedCount > 1) { // at most one unfolded helix can be used
	    valid = false;
	    goto LOOP_LAST;
	  }
	}
      }
      if (valid) {
	for (size_t i = 0; i < digits.size(); ++i) {
	  if (digits[i] > 0) {
THROWIFNOT(i < compVec.size(),"Assertion violation in HelixStructureOptimizer.h :  L257");
THROWIFNOT(digits[i] >= 1,"Assertion violation in HelixStructureOptimizer.h :  L258");
THROWIFNOT(digits[i] - 1 < longestSubstems[compVec[i]].size(),"Assertion violation in HelixStructureOptimizer.h :  L259");
	    const Stem& stem = longestSubstems[compVec[i]][digits[i]-1];
THROWIFNOT(emptyStructure.isPlaceable(stem),"Assertion violation in HelixStructureOptimizer.h :  L261");
	    const Stem& origStem = helices[compVec[i]];
THROWIFNOT(stem.getInvariant() == origStem.getInvariant(),"Assertion violation in HelixStructureOptimizer.h :  L263");
THROWIFNOT(emptyStructure.isPlaceable(origStem),"Assertion violation in HelixStructureOptimizer.h :  L264");
	    int lenDiff = stem.computeMaxDifference(origStem);
	    if (lenDiff > lenDiffMax) {
	      valid = false;
	      // Rcpp::Rcout << "# Original stem and trial stem had too large difference in terms of length, start or stop position: " << stem << " " << origStem << endl;
	      goto LOOP_LAST;
	    }
	    for (size_t j = 0; j < changedStems.size(); ++j) {
	      if (changedStems[j].hasCommonBases(stem)) {
		valid = false;
		// if (verbose >1) {
		//   Rcpp::Rcout << "# Stem " << stem << " is overlapping with already placed stem " << (j+1) << " " << changedStems[j] << endl;
		// }
		goto LOOP_LAST;
	      }
	    }
	    for (size_t j = 0; j < remainingHelices.size(); ++j) {
	      if (stem.hasCommonBases(remainingHelices[j])) {
		valid = false;
		goto LOOP_LAST;
	      }
	    }
	    changedStems.push_back(stem);
	    trial.push_back(stem);
	  }
	}
      }
      if (valid) {
	if (changedStems.size() > 0) {
	  // everValid = true;
	}
	// Rcpp::Rcout << "Trying combo " << digits << " stems: " << changedStems;
	ResidueStructure trialStructure(structure.getSequences(), structure.getConcentrations(), structure.getResidueModel());
	try {
	  for (size_t i = 0; i < changedStems.size(); ++i) {
THROWIFNOT(trialStructure.isPlaceable(changedStems[i]),"Assertion violation in HelixStructureOptimizer.h :  L299");
	  }
	  for (size_t i = 0; i < trial.size(); ++i) {
	    if (!trialStructure.isPlaceable(trial[i])) {
	      Rcpp::Rcout << "# Warning: helix not placeable: " << trial[i] << endl;
	     // Rcpp::Rcout << "# The whole trial structure us: " << trial;
	     //  Rcpp::Rcout << "# The emptry structure is:" << endl;
	      trialStructure.writeCT(Rcpp::Rcout);
	    }
THROWIFNOT(trialStructure.isPlaceable(trial[i]),"Assertion violation in HelixStructureOptimizer.h :  L308");
	  }
THROWIFNOT(!StemTools::hasCommonBases(changedStems),"Assertion violation in HelixStructureOptimizer.h :  L310");
	  for (size_t i = 0; i < changedStems.size(); ++i) {
	    for (size_t j = 0; j < remainingHelices.size(); ++j) {
THROWIFNOT(!changedStems[i].hasCommonBases(remainingHelices[j]),"Assertion violation in HelixStructureOptimizer.h :  L313");
	    }
	  }
	  
	  trialStructure.setStems(trial); // build structure

	  double g = trialStructure.computeAbsoluteFreeEnergy();

	  // if (verbose > VERBOSE_DETAILED) {
	  //   Rcpp::Rcout << "# Tried optimizing structure " << digits << " with energy " << g << " stems: " << changedStems << " : " << trial;
	  // }

	  if (g < gBest) {
	    // if (verbose > VERBOSE_INFO) {
	    //   Rcpp::Rcout << "# Found new best free energy: " << g << " " << trial << " instead of " << helices;
	    // }
	    gBest = g;
	    helicesChangedBest = changedStems; 
	    helicesAllBest = trial;
	    everChanged = true;
	  } else {
	    // if (verbose > VERBOSE_INFO) {
	    //   Rcpp::Rcout << "# Sorry, energy higher than " << gBest << " : " << g << " " << trial;
	    // }
	  }
	} catch (int e) {
	  // do nothing
	  // if (verbose > VERBOSE_INFO) {
	  //   Rcpp::Rcout << "# Could not place trial helices: " << trial;
	  // }
	}
      }
    LOOP_LAST:
      DEBUG_MSG("Last line of loop over helix combinations.");
    } while (multi.incIfPossible());
    if (!everChanged) {
      if (verbose > VERBOSE_DETAILED) {
	Rcpp::Rcout << "# No combination led to better results comopared to original structure." << endl;
      }
      helicesChangedBest = changeHelicesOrig;
    }
    // if (verbose > VERBOSE_INFO) {
    //   Rcpp::Rcout << "# finishing analysis of component " << component << endl;
    //   Rcpp::Rcout << "# best combination for this component: " << helicesAllBest;
    //   Rcpp::Rcout << "# overall best combination for this component: " << helicesChangedBest;
    // }
    // EASSERT(everValid); // strange if no combination was valid
    EASSERT(helicesChangedBest.size() <= compVecSizes.size()); // only return stems corresponding to this component
THROWIFNOT(!StemTools::hasCommonBases(helicesAllBest),"Assertion violation in HelixStructureOptimizer.h :  L361");
THROWIFNOT(!StemTools::hasCommonBases(helicesChangedBest),"Assertion violation in HelixStructureOptimizer.h :  L362");
    return helicesChangedBest;
  }
  
  adjacency_list_t stemCommonBasesToAdjacency(const Vec<Stem>& stems) const {
    adjacency_list_t adj(stems.size());
    for (size_t i = 0; i < stems.size(); ++i) {
      for (size_t j = i+1; j < stems.size(); ++j) {
	if (stems[i].hasCommonBases(stems[j])) {
	  adj[i].insert(j);
	  adj[j].insert(i);
	}
      }
    }
    return adj;
  }

};

#endif
