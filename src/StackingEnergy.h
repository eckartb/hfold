#ifndef _STACKING_ENERGY_H_
#define _STACKING_ENERGY_H_

#include <string>
#include <map>
#include <algorithm>
#include "debug.h"
#include "StringTools.h"

using namespace std;
class StackingEnergy {

 public:
  typedef string::size_type size_type;
  typedef int index_type;

 StackingEnergy() : toupperMode(true) { }

  StackingEnergy(const StackingEnergy& other) { copy(other); }

  virtual ~StackingEnergy() { }

  StackingEnergy& operator = (const StackingEnergy& other) {
    if (&other != this) {
      copy(other);
    }
    return (*this);
  }

 public:
  
  /** Adjust temperature of energy table using temperature (in Kelvin) and enthalpies */
  void adjustTemperatureK(double tempK, const StackingEnergy& enthalpies) {
    double T37 = 273.15 + 37.0; // 37 C in Kelvin
    for (auto it = energyTable.begin(); it != energyTable.end(); it++) {
      string hash = it->first;
      double g37 = it->second;
      auto it2 = enthalpies.energyTable.find(hash);
      if (it2 != enthalpies.energyTable.end()) {
	double h = it2->second;
	// dG = dH - T dS ->  dG-dH = - T dS -> dS = (dH-dG)/T
	double s = (h - g37)/T37;  // find entropy difference
	double g = h - tempK * s; // compute dG for new temperature
	energyTable[hash] = g; // set to new value
      } else {
	Rcpp::Rcerr << "# Warning: could not find enthalpy values for energy table entry " << hash << endl;
THROWIFNOT(false,"Assertion violation in StackingEnergy.h :  L47");
       }
    }
  }

  void copy(const StackingEnergy& other) {
    energyTable = other.energyTable;
    toupperMode = other.toupperMode; 
  }

  /** For determining whether to apply symmetry penalty. This term is 0.43 in the Matthews 1999 model.
   */
  bool isSymmetric(const string& sequence,
		   index_type start,
		   index_type stop,
		   index_type length) const {
    for (index_type i = 0; i < length; ++i) {
      if (sequence[start +i] != sequence[stop-length+i+1]) {
	return false;
      }
    }
    return true;
  }

  /** Compute stem energy
   */
  double calcStemStackingEnergy(const string& sequence,
				index_type start,
				index_type stop,
				index_type length) const;

  void read(istream& is) {
    vector<string> lines = getLines(is);
    energyTable.clear();
    for (size_type i = 0; i < lines.size(); ++i) {
      vector<string> words = getTokens(lines[i]);
      if ((words.size() == 3) && (words[0][0] != '#')) {
	if (words[0] == "!TERMINAL") {
	  string hash = words[1];
	  double value = stod(words[2]);
	  energyTable[hash] = value;
	  string hash2 = hash;
	  std::reverse(hash2.begin(), hash2.end());
	  energyTable[hash2] = value; // also add the reverse: corresponds to reading pairing in opposite by equivalent order
	  // Rcpp::Rcout << "Added terminal energy term for " << hash << " : " << value << " and " << hash2 << " " << value << " to stacking energy model." << endl;
	} else {
	  string hash = words[0] + words[1];
	  double value = stod(words[2]);
	  energyTable[hash] = value;
	  string hash2 = hash;
	  std::reverse(hash2.begin(), hash2.end());
	  energyTable[hash2] = value; // also add the reverse: corresponds to reading pairing in opposite by equivalent order
	  // Rcpp::Rcout << "Added " << hash << " " << value << " and " << hash2 << " " << value << " to stacking energy model." << endl;
	}
      }
    }
  }

  void setToupperMode(bool b) { toupperMode = b; }

  bool validate() const { return energyTable.size() > 0; }

  /** Writes to output stream */
  void write(ostream & os) const {
    for (map<string, double>::const_iterator it = energyTable.begin(); it != energyTable.end(); it++) {
      //     os << it->first << ":" << it->second << endl; 
      os << it->first << " : " << (it->second) << endl; // ;->first << ":" << it->second << endl; 
    }
  } 

    /** Given a base pair A-B followed by base pair X-Y, return stacking energy 
   * values for a,b,x,y: A|C|G|U
   */
  double getStackingEnergy(char a, char b, char x, char y) const;

    /** Given a base pair A-B followed by base pair X-Y, return stacking energy 
   * values for a,b,x,y: A|C|G|U
   * If not entry in data table is found, try different order.
   */
  double getStackingEnergyPermissive(char a, char b, char x, char y) const;

  /** Energy term for terminal ends. Result does not depend on order (G,C same as C,G) */
  double getTerminalEnergy(char x, char y) const {
    string hash = "xx";
    if (toupperMode) {
      hash[0] = toupper(x);
      hash[1] = toupper(y);
    } else {
      hash[0] = x;
      hash[1] = y;
    }
    map<string,double>::const_iterator it = energyTable.find(hash);
    if (it != energyTable.end()) {
      return it -> second;
    } 
    // search other way around:
    string hash2 = hash;
    hash2[1] = hash[0];
    hash2[0] = hash[1];
    it = energyTable.find(hash2);
    if (it != energyTable.end()) {
      return it -> second;
    } 

    return 0.0;
  }

  double getSymmetryTerm() const {
    return 0.43; // currently hard-coded according to Matthews/Turner 1999 model
  }  

  /** Private attributes */
 private:
  map<string, double> energyTable; // stores in hash codes like GCGU the stacking energy of an G-C base pair followed by a GU base pair
  bool toupperMode;

};


/** Compute stem energy
 */
double
StackingEnergy::calcStemStackingEnergy(const string& sequence,
				       index_type start,
				       index_type stop,
				       index_type length) const {
    PRECOND(validate());
    double energy = 0;
    if (length <= 1) {
      return 0.0;
    }
    // Rcpp::Rcout << "Computing stacking energy of helix " << (start+1) << " " << (stop+1) << " " << length << " : " << sequence.size() << " " 
    // << sequence.substr(start, length) << " " << sequence.substr(stop-length+1, length) << endl;
      for (index_type i = 0; (i + 1) < length; i++) {
THROWIFNOT(stop >= (i + 1),"Assertion violation in StackingEnergy.h :  L181");
THROWIFNOT(start + i + 1 < static_cast<index_type>(sequence.size()),"Assertion violation in StackingEnergy.h :  L182");
      double term = getStackingEnergy(sequence[start+i], sequence[stop-i], sequence[start+i+1], sequence[stop-i-1]);
      energy += term;
      // Rcpp::Rcout << "Added term " << term << " for " << sequence[start+i] << sequence[stop-i] << sequence[start+i+1] << sequence[stop-i-1] << endl;
    }
    energy += getTerminalEnergy(sequence[start], sequence[stop]);
    energy += getTerminalEnergy(sequence[start+length-1], sequence[stop-length+1]);
    // Rcpp::Rcout << "Added terminal terms: " << getTerminalEnergy(sequence[start], sequence[stop]) << " and " << getTerminalEnergy(sequence[start+length-1], sequence[stop-length+1]) << endl;
    if (isSymmetric(sequence, start, stop, length)) {
      // Rcpp::Rcout << "Added symmetry term: " << energy << endl;
      energy += getSymmetryTerm();
    }
    // Rcpp::Rcout << "Stacking energy of helix " << (start+1) << " " << (stop+1) << " " << length << " : " << energy << " "
    //	 << sequence.substr(start, length) << " " << sequence.substr(stop-length+1, length) << endl;
    return energy;
  }

/** Given a base pair A-B followed by base pair X-Y, return stacking energy 
 * values for a,b,x,y: A|C|G|U
 */
double
StackingEnergy::getStackingEnergy(char a, char b, char x, char y) const {
    string hash = "xxxx";
    string hash2 = "xxxx";
    if (toupperMode) {
      a = toupper(a);
      b = toupper(b);
      x = toupper(x);
      y = toupper(y);
    }
    hash[0] = a;
    hash[1] = b;
    hash[2] = x;
    hash[3] = y;

    hash2[0] = y;
    hash2[1] = x;
    hash2[2] = b;
    hash2[3] = a;

    map<string,double>::const_iterator it = energyTable.find(hash);
    if (it == energyTable.end()) {
      it = energyTable.find(hash2);
      // Rcpp::Rcout << "# Note: used reverse order for stacking parameters for " << a << b << " followed by " << x << y << endl;
      if (it == energyTable.end()) {
	// Rcpp::Rcout << "# Warning, could not find stacking parameters for " << a << b << " followed by " << x << y << endl;
	// EASSERT(false);
	return 0.0;
      }
    }
    return it->second;
  }

/** Given a base pair A-B followed by base pair X-Y, return stacking energy 
 * values for a,b,x,y: A|C|G|U
 */
double
StackingEnergy::getStackingEnergyPermissive(char a, char b, char x, char y) const {
  double result = getStackingEnergy(a,b,x,y);
  if (result == 0.0) { // can only be if not entry in table has been found
    result = getStackingEnergy(a,b,y,x); // flip order: take care of special case of stacking of 2 RNA/DNA hybrids
  }
  if (result == 0.0) {
    result = getStackingEnergy(b,a,x,y); // flip order: take care of special case of stacking of 2 RNA/DNA hybrids
  }
  if (result == 0.0) {
    result = getStackingEnergy(b,a,y,x); // flip order: take care of special case of stacking of 2 RNA/DNA hybrids
  }
  return result;
}

#endif
