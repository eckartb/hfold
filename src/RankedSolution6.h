// --*- C++ -*------x---------------------------------------------------------
// $Id: RankedSolution6.h,v 1.1.1.1 2006/07/03 14:43:21 bindewae Exp $
//
// Class:           RankedSolution6
// 
// Base class:      -
//
// Derived classes: - 
//
// Author:          Eckart Bindewald
//
// Description:     Implements ranked solutions
// 
// Reviewed by:     -
// -----------------x-------------------x-------------------x-----------------

#ifndef __RANKED_SOLUTION6_H__
#define __RANKED_SOLUTION6_H__

// Includes

#include <iostream>
#include "debug.h"

template<class T, class S = int, class R = int>
class RankedSolution6 {
public:
  RankedSolution6<T, S, R>() : first(0.0) { } 

  RankedSolution6<T, S, R>(double _first, const T& _second, const S& _third, const R& _fourth) : 
    first(_first), second(_second), third(_third), fourth(_fourth) { }

  RankedSolution6<T, S, R>(const RankedSolution6<T, S, R>& orig) { copy(orig); }

  virtual ~RankedSolution6<T, S, R>() {} 

  /* OPERATORS */

  /** Assigment operator. */
  RankedSolution6<T, S, R>& operator = (const RankedSolution6<T, S, R>& orig) {
    if (&orig != this) {
      copy(orig);
    }
    return *this;
  }

  friend ostream& operator << (ostream& os, const RankedSolution6<T, S, R>& orig) {
    os << orig.first << " " << orig.second << " " << orig.third << " " << orig.fourth; 
    return os;
  }

  friend istream& operator >> (istream& is, RankedSolution6<T, S, R>& orig) {
    is >> orig.first >> orig.second >> orig.third >> orig.fourth; 
    return is;
  }

  /* PREDICATES */

  double getFirst() const { return first; }

  const T& getSecond() const { return second; }

  const S& getThird() const { return third; }

  const R& getFourth() const { return fourth; }

  /* MODIFIERS */
  void copy(const RankedSolution6<T, S, R>& other) {
    first = other.first;
    second = other.second;
    third = other.third;
    fourth = other.fourth;
  }

  /* ATTRIBUTES */

  double first;
  T second;
  S third;
  R fourth;

protected:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* OPERATORS  */
  /* PREDICATES */
  /* MODIFIERS  */

private:
  /* PRIVATE ATTRIBUTES */

};

/** comparison operator. */
template<class T, class S, class R>
inline
bool operator < (const RankedSolution6<T, S, R>& lval,
		 const RankedSolution6<T, S, R>& rval) { 
  return lval.first < rval.first;
}

/** comparison operator. */
template<class T, class S, class R>
inline
bool operator < (double lval, const RankedSolution6<T, S, R>& rval) {
  return lval < rval.first;
}

/** comparison operator. */
template<class T, class S, class R>
inline
bool operator < (const RankedSolution6<T, S, R>& lval, double rval)   {
  return lval.first < rval;
}


#endif /* __ACLASS_H__ */

