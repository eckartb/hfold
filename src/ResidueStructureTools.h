#ifndef _RESIDUE_STRUCTURE_TOOLS_
#define _RESIDUE_STRUCTURE_TOOLS_

#include <iterator>
#include <utility>
#include "ResidueStructure.h"
#include "ResidueModel.h"
#include "BasePairTools.h"
#include "debug.h"
#include "basepair.h"
#include "RnaFormats.h"
#include "stemhelp.h"
#include "StrenumTools.h"

class ResidueStructureTools {

 public:
  typedef ResidueStructure::basepair_t basepair_t;

 public:

  /** Build structure from light-weight ResiduePairing object. Can throw DISTANCE_CONSTRAINT_EXCEPTION */
  static ResidueStructure buildStructure(const ResiduePairing& pairing) throw (int) {
    StrandContainer * strands = pairing.getSequences();
    ResidueStructure result(*strands, strands->getConcentrations(), pairing.getResidueModel());
    const Vec<Stem>& stems = pairing.getHelices();
    set<Stem> stemSet;
    for (size_t i = 0; i < stems.size(); ++i) {
      // result.setStem(stems[i]);
      stemSet.insert(stems[i]);
    }
    while (stemSet.size() > 0) {
      // find stem with highest initiation probability:
      auto bestIt = stemSet.begin();
      double highestProb = result.getStemInitiationProbability(*bestIt);
      for (auto it = stemSet.begin(); it != stemSet.end(); it++) {
	double prob = result.getStemInitiationProbability(*bestIt);
	if ((prob > highestProb) || ((prob == highestProb) || ((*it) > (*bestIt)))) {
	  bestIt = it;
	}
      }
      result.setStem(*bestIt); // set highest-probability stem
      stemSet.erase(bestIt);
    }
    return result;
  }

  /** Build structure from light-weight ResiduePairing object. Can throw DISTANCE_CONSTRAINT_EXCEPTION */
/*   static ResiduePairing structureToPairing(const ResidueStructure& structure) throw (int) { */
/*     StrandContainer * strands = structure.getSequences(); */
/*     ResiduePairing result(*strands, strands->getConcentrations(), pairing.getResidueModel()); */
/*     const Vec<Stem>& stems = structure.getHelices(); */
/*     for (size_t i = 0; i < stems.size(); ++i) { */
/*       result.setStem(stems[i]); */
/*     } */
/*     return result; */
/*   } */

  static void applyFoldingEvent(ResidueStructure& structure, const folding_event_t& event, int helixLengthMin, int verbose) throw (int) {
    if (event.first == FOLDED_BASEPAIR) {
      structure.setBasePair_unsafe(event.second.first, event.second.second);
    } else if (event.first == UNFOLDED_BASEPAIR) {
      set<basepair_t> basepairs;
      basepairs.insert(event.second);
      unsetBasePairs(structure, basepairs, helixLengthMin, verbose);
    } else {
      DERROR("Unknown folding event code: " + std::to_string(event.first));
    }
  }

  /** Places a single helices and returns map of connectivities that leads to an improved structure. */
  static map<string, ResidueStructure> placeHelicesForConnectivities(const ResidueStructure& structure, Vec<Stem>& stems, size_t exploreMax) {
    map<string, ResidueStructure> result;
    string connectivity = structure.getStrandConnectivityHash();
    // double energyOrig = structure.computeAbsoluteFreeEnergy();
    size_t strandCount = structure.getStrandCount();
    string totSequence = structure.getConcatSequence();
    Vec<Vec<Vec<Stem> > > stemSets(strandCount, Vec<Vec<Stem> >(strandCount, Vec<Stem>()));
    for (size_t i = 0; i < stems.size(); ++i) {
      const Stem& stem = stems[i];
      if (structure.isPlaceable(stems[i])) {
	size_t s1 = structure.getStrandId(stem.getStart());
	size_t s2 = structure.getStrandId(stem.getStop());
	stemSets[s1][s2].push_back(stem);
	stemSets[s2][s1].push_back(stem);
      }
    }
    for (size_t i = 0; i< strandCount; ++i) {
      for (size_t j = 0; j < strandCount; ++j) {
	sort(stemSets[i][j].begin(), stemSets[i][j].end(), StemEnergyComparator());
	sort(stemSets[j][i].begin(), stemSets[j][i].end(), StemEnergyComparator());
      }
    }
    for (size_t i = 0; i < strandCount; ++i) {
      for (size_t j = i+1; j < strandCount; ++j) { // intra-strand helices not interesting because they do not change connectivity
	size_t countMax = stemSets[i][j].size();
	if (countMax > exploreMax) {
	  countMax = exploreMax;
	}
	for (size_t k = 0; k < countMax; ++k) {
	  const Stem& stem = stemSets[i][j][k];
	  ResidueStructure structure2 = structure;
	  if (structure2.isPlaceable(stem)) {
	    structure2.setStem(stem);
	    string connectivity2 = structure2.getStrandConnectivityHash();
	    if (connectivity.compare(connectivity2) != 0) {
	      double energy2 = structure2.computeAbsoluteFreeEnergy();
	      // double dE = energy2 - energyOrig;
#ifndef NDEBUG
	      Rcpp::Rcout << "# Examining path " << connectivity << " " << connectivity2 << " " << energyOrig << " " << energy2 << " " << dE << " stem : ";
	      Rcpp::Rcout << (stem.getStart()+1) << " " << (stem.getStop()+1) << " " << (stem.getLength()) << " " << stem.getEnergy() << " " << StemTools::getSequence1(stem, totSequence) << " " << StemTools::getSequence2(stem, totSequence) << " structure : ";
	      StrenumTools::writeLine(Rcpp::Rcout, structure2);
	      Rcpp::Rcout << endl;
#endif
	      if (result.find(connectivity2) == result.end()) { // new connectivity!
		result[connectivity2] = structure2;
	      } else {
		double energy = result[connectivity2].computeAbsoluteFreeEnergy();
		if (energy2 < energy) { // better energy encountered
		  result[connectivity2] = structure2;
		}
	      }
	    }
	  }
	}
      }
    }
    return result;
  }

  /** Use secondary structure format ids as defined in the file RnaFormats.h. 
   * Value of stride should be one, unless 
   */
  static void writeFoldingEvents(ostream& os, const ResidueStructure& rna, int formatId, int helixLengthMin, int stride, int verbose) {
    const Vec<folding_event_t>& foldingEvents = rna.getFoldingEvents();
    switch (formatId) {
    case RnaFormats::REGION_FORMAT: {
      ResidueStructure tmpStruct = rna; // copy of this structure
      tmpStruct.clearPairings();
      for (size_t i = 0; i < foldingEvents.size(); ++i) {
	try {
	  applyFoldingEvent(tmpStruct, foldingEvents[i], helixLengthMin, verbose);	
	//  Rcpp::Rcout << "Applied folding event " << foldingEvents[i] << " set base pairs: " << tmpStruct.getBasePairCount() << endl;
	  if ((i % stride) == 0) {
	    writeStems(os, tmpStruct.getHelices());
	  }
	} catch (int foldingException) {
	  Rcpp::Rcerr << "# Warning: folding error encountered during re-constitution of folding pathway." << endl;
	}
      }      
    }
      break;
    case RnaFormats::FOLDING_EVENT_FORMAT:
      os << foldingEvents.size() << endl;
      for (size_t i = 0; i < foldingEvents.size(); ++i) {
	if ((i % stride) == 0) {
	  os << foldingEvents[i] << endl;
	}
      }
      break;
    default:
      DERROR("Unknown output format id.");
    }
  }

  /** For a given matrix, randomly chooses a greater-zero matrix element. */
  static basepair_t computeRandomBasePair(const Vec<Vec<double> >& probabilities, unsigned int * seedP) {
    set<unsigned int> tabooSet;
    unsigned int counter = 0;
    for (size_t i = 0; i < probabilities.size(); ++i) {
      for (size_t j = i; j < probabilities[i].size(); ++j) {
	if (probabilities[i][j] > 0.0) {
	  ++counter;
	}
      }
    }
    Rcpp::Rcout << "# Found " << counter << " non-zero probability matrix elements." << endl;
    double rf = rand_r(seedP) / static_cast<double>(RAND_MAX); // number between 0 and 1
    Rcpp::Rcout << "# Random number is: " << rf << endl;
    unsigned int rid = static_cast<unsigned int>(counter * rf);
THROWIFNOT(rid < counter,"Assertion violation in L181");
    unsigned int counter2 = 0;
    for (size_t i = 0; i < probabilities.size(); ++i) {
      for (size_t j = i; j < probabilities[i].size(); ++j) {
	if (probabilities[i][j] > 0.0) {
	  ++counter2;
	}
	if (counter2 == rid) {
	  basepair_t bp(i,j);
	  Rcpp::Rcout << "# Found id match at " << bp << endl;
	  return bp;
	}
      }
    }    
    POSTCOND(false); // should never be here
    return basepair_t(-1,-1);
  }

  /** For a given matrix, randomly chooses a greater-zero matrix element. Ineffient - check if needed. */
  static basepair_t computeRandomBasePair(const Vec<Vec<float> >& probabilities, unsigned int * seedP) {
    set<unsigned int> tabooSet;
    unsigned int counter = 0;
    for (size_t i = 0; i < probabilities.size(); ++i) {
      for (size_t j = i; j < probabilities[i].size(); ++j) {
	if (probabilities[i][j] > 0.0) {
	  ++counter;
	}
      }
    }
    Rcpp::Rcout << "# Found " << counter << " non-zero probability matrix elements." << endl;
    double rf = rand_r(seedP) / static_cast<double>(RAND_MAX); // number between 0 and 1
    Rcpp::Rcout << "# Random number is: " << rf << endl;
    unsigned int rid = static_cast<unsigned int>(counter * rf);

THROWIFNOT(rid < counter,"Assertion violation in L215");
    unsigned int counter2 = 0;
    for (size_t i = 0; i < probabilities.size(); ++i) {
      for (size_t j = i; j < probabilities[i].size(); ++j) {
	if (probabilities[i][j] > 0.0) {
	  ++counter2;
	}
	if (counter2 == rid) {
	  basepair_t bp(i,j);
	  Rcpp::Rcout << "# Found id match at " << bp << endl;
	  return bp;
	}
      }
    }    
    POSTCOND(false); // should never be here
    return basepair_t(-1,-1);
  }

  /** For a given matrix, randomly chooses a greater-zero matrix element. */
  static Vec<basepair_t> computeRandomHelix(const Vec<Vec<double> >& probabilities, unsigned int * seedP) {
    basepair_t bp = computeRandomBasePair(probabilities, seedP);
    int start = bp.first;
    int stop = bp.second;
    Vec<basepair_t> result;
    while ((start >= 0) && (stop < static_cast<int>(probabilities.size()))) {
      result.push_back(basepair_t(start,stop));
      --start;
      ++stop;
    }
    start = bp.first + 1;
    stop = bp.second - 1;
    while ((start >= 0) && (stop < static_cast<int>(probabilities.size())) && ((stop - start) > LOOP_LEN_MIN) && (probabilities[start][stop] > 0)) {
      result.push_back(basepair_t(start,stop));
      ++start;
      --stop;
    }
    return result;
  }

  /** For a given matrix, randomly chooses a greater-zero matrix element. */
  static Vec<basepair_t> computeRandomHelix(const Vec<Vec<float> >& probabilities, unsigned int * seedP) {
    basepair_t bp = computeRandomBasePair(probabilities, seedP);
    int start = bp.first;
    int stop = bp.second;
    Vec<basepair_t> result;
    while ((start >= 0) && (stop < static_cast<int>(probabilities.size()))) {
      result.push_back(basepair_t(start,stop));
      --start;
      ++stop;
    }
    start = bp.first + 1;
    stop = bp.second - 1;
    while ((start >= 0) && (stop < static_cast<int>(probabilities.size())) && ((stop - start) > LOOP_LEN_MIN) && (probabilities[start][stop] > 0)) {
      result.push_back(basepair_t(start,stop));
      ++start;
      --stop;
    }
    return result;
  }

 // for each strand connectivity, sum all average simulation times 
  static map<string, double> addMapValues(const map<string, double> map1,
				      const map<string, double> map2) {
    map<string, double> result;
    for (map<string,double>::const_iterator it = map1.begin(); it != map1.end(); it++) {
      double value = it->second;
      map<string, double>::const_iterator it2 = map2.find(it->first);
      if (it2 != map2.end()) {
	value += it2->second;
      }
      result[it->first] = value;
    }
    // now add all values that are unique to map 2:
    for (map<string,double>::const_iterator it = map2.begin(); it != map2.end(); it++) {
      double value = it->second;
      map<string, double>::const_iterator it2 = map1.find(it->first);
      if (it2 == map1.end()) {
	result[it->first] = value;
      }
    }
    return result;
  }

  /** Initialize max and min distance constraints and folding probabilities. Keep synchronized with removeDivider method! */
  static void initializeDistances(ResidueStructure & structure, int startRes, int stopRes,
				  int helixLengthMin, int verbose) {
  if (verbose >= VERBOSE_FINE) {
    Rcpp::Rcout << "Starting initializeDistances..." << endl;
  }
  PRECOND(structure.validate());
  double reactionRadius = structure.getResidueModel()->getReactionRadius();
  double intraCollisionExponent = structure.getResidueModel()->getIntraCollisionExponent();
  double reactionVolume = reactionRadius * reactionRadius * reactionRadius;
  double p0Intra = structure.getResidueModel()->getIntraCollisionProb0(); // (3.0/(4.0 * PI) ) * reactionVolume * intraCollisionEpsilon;
  
  // int divider = structure.divider;

  for (size_t i = 0; i < structure.lengthSum; ++i) {
    size_t strandId1 = structure.strandIds[i];
    for (size_t i2 = 0; i2 < structure.paTypes; ++i2) {
      size_t ic = (i*structure.paTypes) + i2; // each residue can have several pseudoatoms or "beads"
      if (ic >= structure.getPaCount()) {
	Rcpp::Rcout << "# Warning: index is greater than number of pseudoatoms: " << (i+1) << " " << structure.paTypes << " " << (i2+1) << " " << (i*structure.paTypes + i2) << " " << structure.getPaCount() << endl;
      }
THROWIFNOT(ic < structure.getPaCount(),"Assertion violation in L319");
      for (size_t j = i; j < structure.lengthSum; ++j) {
	if ((startRes >= 0) && (stopRes >= 0) && ((static_cast<int>(i) != startRes) && (static_cast<int>(j) != stopRes))) {
	  continue;
	}
	size_t strandId2 = structure.strandIds[j];
	size_t di = j - i;
	for (size_t j2 = 0; j2 < structure.paTypes; ++j2) {
	  size_t jc = (j*structure.paTypes) + j2;
THROWIFNOT(jc < structure.pairv.size(),"Assertion violation in L328");
	  ASSERT(structure.paTypes == 1); // only 1-bead model at the moment, or below code will be wrong - FIXIT
	  int foundLength = ResidueModel::findWatsonCrickHelixLength(structure.concatSequence, ic,jc, true, structure.strandIds);
	  if (foundLength < helixLengthMin) {
	    structure.probabilities[ic][jc] = 0.0; // TODO
	    structure.probabilities[jc][ic] = 0.0;
	    if (verbose > VERBOSE_FINE) {
	      Rcpp::Rcout << "Insufficient Helix starting from positions " << (ic+1) << " " << (jc+1) << " : " << foundLength << endl;
	    }
	  } else {
	    structure.probabilities[ic][jc] = 1.0; // will be multiplied with other probabilities
	    structure.probabilities[jc][ic] = 1.0;
	    // ASSERT((j - i) > LOOP_LEN_MIN);
	    if (verbose > VERBOSE_FINE) {
	      Rcpp::Rcout << "Sufficient Helix starting from positions " << (ic+1) << " " << (jc+1) << " : " << foundLength << endl;
	    }
	  }

	  if ((i == j) && (i2 == j2) ) { // same residue and same pseudo-atom
	    structure.minDists[ic][jc] = 0.0;
	    structure.minDists[jc][ic] = 0.0;
	    structure.maxDists[ic][jc] = 0.0;
	    structure.maxDists[jc][ic] = 0.0;
	    structure.probabilities[ic][jc] = 0.0;
	    structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	  } else if (!structure.isStrandPairInSamePot(strandId1, strandId2)) {  // case: strand pair cannot interact due to divider
	    // (divider>=0) && (static_cast<int>(strandId1) < divider) && (static_cast<int>(strandId2) >= divider)) { // multipot assembly: strands 1...(<_)divider s firce, then the curret one, the strand(n+1) and higer
	    structure.minDists[ic][jc] = DIFFERENT_POT_DISTANCE;
	    structure.minDists[jc][ic] = DIFFERENT_POT_DISTANCE;
	    structure.maxDists[ic][jc] = DIFFERENT_POT_DISTANCE;
	    structure.maxDists[jc][ic] = DIFFERENT_POT_DISTANCE;
	    structure.probabilities[ic][jc] = 0.0;
	    structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	  } else { // interesting case : in same reaction "pot"
THROWIFNOT(di > 0.0,"Assertion violation in L362");
	    structure.minDists[ic][jc] = RESIDUE_DIST_MIN;
	    structure.minDists[jc][ic] = RESIDUE_DIST_MIN;
	    if (strandId1 == strandId2) { // same-strand interactions:
	      structure.maxDists[ic][jc] = RESIDUE_ADJ_DIST_MAX * di;
	      structure.maxDists[jc][ic] = structure.maxDists[ic][jc];
	      if (di > LOOP_LEN_MIN) { // observe minimum loop length
		structure.probabilities[ic][jc] = structure.probabilities[ic][jc] * p0Intra * pow(structure.maxDists[ic][jc], - intraCollisionExponent); // pow(reactionRadius/structure.maxDists[jc][ic], 3);
		// structure.probabilities[ic][jc] = pow(REACTION_RADIUS/structure.maxDists[jc][ic], 3);
		structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	      }
	    } else { // same pot, different strands
	      // probability of two reacting nucleotides can be found in same reaction volume, multiplied by number of reaction volumes
	      structure.probabilities[ic][jc] = structure.probabilities[ic][jc] * (reactionVolume * structure.concentrations[strandId1]); //  * (reactionVolume * structure.concentrations[strandId1]); // (structure.effectiveVolume/REACTION_VOLUME) * (structure.concentrations[strandId1]/REACTION_VOLUME) * (structure.concentrations[strandId2]/REACTION_VOLUME);
	     //  assert(structure.concentrations[strandId1] == structure.concentrations[strandId2]);
	      // structure.probabilities[ic][jc] = structure.probabilities[ic][jc]* (structure.effectiveVolume * structure.concentrations[strandId1]) * (reactionVolume * structure.concentrations[strandId1]); // (structure.effectiveVolume/REACTION_VOLUME) * (structure.concentrations[strandId1]/REACTION_VOLUME) * (structure.concentrations[strandId2]/REACTION_VOLUME);
	      // structure.probabilities[ic][jc] = (structure.effectiveVolume * structure.concentrations[strandId1]) * (REACTION_VOLUME * structure.concentrations[strandId1]); // (structure.effectiveVolume/REACTION_VOLUME) * (structure.concentrations[strandId1]/REACTION_VOLUME) * (structure.concentrations[strandId2]/REACTION_VOLUME);
	      if (structure.probabilities[ic][jc] > 1.0) {
		structure.probabilities[ic][jc] = 1.0;
	      }
	      structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	      // structure.maxDists[ic][jc] = reactionRadius / pow(structure.probabilities[ic][jc], 1.0/3.0); // solve previous equation for distance // this distance will not be needed
	      structure.maxDists[ic][jc] = pow(structure.effectiveVolume, 1.0/3.0);
	      structure.maxDists[jc][ic] = structure.maxDists[ic][jc];
	    }
	  }
	}
      }
    }
  }
THROWIFNOT(structure.validate(),"Assertion violation in L392");
  structure.updateResidueProbabilities();
  POSTCOND(structure.validate());
  if (verbose >= VERBOSE_DETAILED) {
    Rcpp::Rcout << "Result of initializeDistances:" << endl;
    Rcpp::Rcout << "Min distances:" << endl;
    Rcpp::Rcout << structure.minDists << endl;
    Rcpp::Rcout << "Max distances:" << endl;
    Rcpp::Rcout << structure.maxDists << endl;
    Rcpp::Rcout << "Probabilities:" << endl;
    Rcpp::Rcout << structure.probabilities << endl;
    Rcpp::Rcout << "Residue Probabilities:" << endl;
    Rcpp::Rcout << structure.residueProbabilities << endl;
    Rcpp::Rcout << "Pairings:" << endl;
    Rcpp::Rcout << structure.pairv << endl;
    Rcpp::Rcout << "Finished intializeDistances" << endl;
  }

  }

  /** Remove divider. Keep synchronized with initializeDistances method! */
  static void removeDividers(ResidueStructure & structure,
			    int helixLengthMin, int verbose) {
  if (verbose >= VERBOSE_FINE) {
    Rcpp::Rcout << "Starting removeDivider..." << endl;
  }
  double reactionRadius = structure.getResidueModel()->getReactionRadius();
  double reactionVolume = reactionRadius * reactionRadius * reactionRadius;
  double p0Intra = structure.getResidueModel()->getIntraCollisionProb0(); // (3.0/(4.0 * PI) ) * reactionVolume * intraCollisionEpsilon;
  double intraCollisionExponent = structure.getResidueModel()->getIntraCollisionExponent();
  structure.removeDividers();
  for (size_t i = 0; i < structure.lengthSum; ++i) {
    size_t strandId1 = structure.strandIds[i];
    for (size_t i2 = 0; i2 < structure.paTypes; ++i2) {
      size_t ic = (i*structure.paTypes) + i2; // each residue can have several pseudoatoms or "beads"
THROWIFNOT(ic < structure.pairv.size(),"Assertion violation in L427");
      for (size_t j = i; j < structure.lengthSum; ++j) {
	size_t strandId2 = structure.strandIds[j];
	size_t di = j - i;
	for (size_t j2 = 0; j2 < structure.paTypes; ++j2) {
	  size_t jc = (j*structure.paTypes) + j2;
THROWIFNOT(jc < structure.pairv.size(),"Assertion violation in L433");
	  if ((i == j) && (i2 == j2) ) {
	    structure.minDists[ic][jc] = 0.0;
	    structure.minDists[jc][ic] = 0.0;
	    structure.maxDists[ic][jc] = 0.0;
	    structure.maxDists[jc][ic] = 0.0;
	    structure.probabilities[ic][jc] = 0.0;
	    structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	  /* } else if ((divider>=0) && (strandId1 < divider) && (strandId2 >= divider)) { // multipot assembly: strands 1...(<_)divider s firce, then the curret one, the strand(n+1) and higer */
	  /*   structure.minDists[ic][jc] = DIFFERENT_POT_DISTANCE; */
	  /*   structure.minDists[jc][ic] = DIFFERENT_POT_DISTANCE; */
	  /*   structure.maxDists[ic][jc] = DIFFERENT_POT_DISTANCE; */
	  /*   structure.maxDists[jc][ic] = DIFFERENT_POT_DISTANCE; */
	  /*   structure.probabilities[ic][jc] = 0.0; */
	  /*   structure.probabilities[jc][ic] = structure.probabilities[ic][jc]; */
	  } else if ((structure.minDists[ic][jc] == DIFFERENT_POT_DISTANCE) && (structure.maxDists[ic][jc] == DIFFERENT_POT_DISTANCE)) { // interesting case
THROWIFNOT(di > 0.0,"Assertion violation in L449");
	    structure.minDists[ic][jc] = RESIDUE_DIST_MIN;
	    structure.minDists[jc][ic] = RESIDUE_DIST_MIN;
	    if (strandId1 == strandId2) { // same strand
	      structure.maxDists[ic][jc] = RESIDUE_ADJ_DIST_MAX * di;
	      structure.maxDists[jc][ic] = structure.maxDists[ic][jc];
	      if (di > LOOP_LEN_MIN) { // observe minimum loop length
		structure.probabilities[ic][jc] = structure.probabilities[ic][jc] * p0Intra * pow(structure.maxDists[ic][jc], - intraCollisionExponent);  // pow(reactionRadius/structure.maxDists[jc][ic], 3);
		structure.probabilities[jc][ic] = structure.probabilities[ic][jc]; // TODO
	      }
	    } else { // not same strand
	      // probability of two reacting nucleotides can be found in same reaction volume, multiplied by number of reaction volumes
	      structure.probabilities[ic][jc] = structure.probabilities[ic][jc] * (reactionVolume * structure.concentrations[strandId1]); //  * (reactionVolume * structure.concentrations[strandId1]); // (structure.effectiveVolume/REACTION_VOLUME) * (structure.concentrations[strandId1]/REACTION_VOLUME) * (structure.concentrations[strandId2]/REACTION_VOLUME)
	      // structure.probabilities[ic][jc] = (structure.effectiveVolume * structure.concentrations[strandId1]) * (reactionVolume * structure.concentrations[strandId1]); // (structure.effectiveVolume/REACTION_VOLUME) * (structure.concentrations[strandId1]/REACTION_VOLUME) * (structure.concentrations[strandId2]/REACTION_VOLUME);
	      if (structure.probabilities[ic][jc] > 1.0) {
		structure.probabilities[ic][jc] = 1.0;
	      }
	      structure.probabilities[jc][ic] = structure.probabilities[ic][jc];
	      structure.maxDists[ic][jc] = pow(structure.effectiveVolume, 1.0/3.0); // solve previous equation for distance // this distance will not be needed
	      // structure.maxDists[ic][jc] = reactionRadius / pow(structure.probabilities[ic][jc], 1.0/3.0); // solve previous equation for distance // this distance will not be needed
	      structure.maxDists[jc][ic] = structure.maxDists[ic][jc];
	    }
	    // if (! ResidueModel::isWatsonCrick(c1,c2, true)) 
	    int foundLength = ResidueModel::findWatsonCrickHelixLength(structure.concatSequence, ic,jc, true, structure.strandIds);
	    if (foundLength < helixLengthMin) {
	      structure.probabilities[ic][jc] = 0.0; // TODO
	      structure.probabilities[jc][ic] = 0.0;
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "Insufficient Helix starting from positions " << (ic+1) << " " << (jc+1) << " : " << foundLength << endl;
	      }
	    } else {
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "Sufficient Helix starting from positions " << (ic+1) << " " << (jc+1) << " : " << foundLength << endl;
	      }
	    }
	  }
	}
      }
    }
  }
  structure.updateResidueProbabilities();
  if (verbose >= VERBOSE_DETAILED) {
    Rcpp::Rcout << "Result of initializeDistances:" << endl;
    Rcpp::Rcout << "Min distances:" << endl;
    Rcpp::Rcout << structure.minDists << endl;
    Rcpp::Rcout << "Max distances:" << endl;
    Rcpp::Rcout << structure.maxDists << endl;
    Rcpp::Rcout << "Probabilities:" << endl;
    Rcpp::Rcout << structure.probabilities << endl;
    Rcpp::Rcout << "Residue Probabilities:" << endl;
    Rcpp::Rcout << structure.residueProbabilities << endl;
    /* Rcpp::Rcout << "Pairings Matrix:" << endl; */
    /* Rcpp::Rcout << structure.pairings << endl; */
    Rcpp::Rcout << "Pairings:" << endl;
    Rcpp::Rcout << structure.pairv << endl;
    // ASSERT(structure.pairv.size() == structure.pairings.size());
    Rcpp::Rcout << "Finished intializeDistances" << endl;
  }
  }

  static void rebuildDistances(ResidueStructure & structure, 
			       int helixLengthMin, int verbose) throw (int) {
    // size_t initialBps = structure.getBasePairCount();
    // Vec<Vec<int> > pairingsOrig = structure.pairings;
    Vec<int> pairVOrig = structure.pairv;
    /* Rcpp::Rcout << "# Starting ResidueStructureTools::Starting rebuildDistances..." << endl; */
    /* Rcpp::Rcout << "# Rebuilding this base pair matrix: " << endl << pairingsOrig << endl; */
    /* Rcpp::Rcout << "# Initial number of base pairs: " << initialBps << endl; */
    // structure.writeAll(Rcpp::Rcout);
    bool eventMode = structure.getTrackEventMode();
    structure.setTrackEventMode(false);
    structure.clearPairings();   
    // initializeDistances(structure, -1, -1, helixLengthMin, verbose);
    initializeDistances(structure, -1, -1, helixLengthMin, verbose);
    // Rcpp::Rcout << "# Structure after initializing distances: " << endl;
    // structure.writeAll(Rcpp::Rcout);
    // Rcpp::Rcout << "# Setting base pairs of structure with matrix : " << endl << pairingsOrig << endl;
    structure.setBasePairVectorAttempt(pairVOrig);
    structure.setTrackEventMode(eventMode);
    // Rcpp::Rcout << "# Structure after rebuilding: " << endl;
    // structure.writeAll(Rcpp::Rcout);
    // Rcpp::Rcout << "# Number of base pairs after rebuilding: " << structure.getBasePairCount() << endl;
    // Rcpp::Rcout << "# Helices: " << structure.helices.size() << endl;
    // for (size_t i = 0; i < structure.helices.size(); ++i) {                                                                                                                          // Rcpp::Rcout << structure.helices[i] << " length: " << structure.helices[i].getLength() << endl;
    //     }       
    // ASSERT(initialBps == structure.getBasePairCount()); // had to take out: initial structure has correct pairing matrix, but incorrect helix information 
    // Rcpp::Rcout << "# Finished ResidueStructureTools::Starting rebuildDistances..." << endl;
  }

  /** Unsets a specified base pair. Also updates min and max distances and strand connectivity matrix. Returns true, if and only if strand connectivity has changed. */
  static bool unsetBasePairs(ResidueStructure& structure, const set<basepair_t>& basepairs, int helixLengthMin, int verbose) throw (int) {
    // ASSERT(structure.validate());
   // size_t initialBps = structure.getBasePairCount();
    // if (verbose >= VERBOSE_FINEST) {
    //   Rcpp::Rcout << "# Removing basepairs from structure: " << basepairs << " with this many original base pairs: " << initialBps << endl;
    //   structure.writeAll(Rcpp::Rcout);
    // }
    bool strandConnectivityChanged = false;
    if (basepairs.size() == 0) {
      return strandConnectivityChanged;
    }
    // if (verbose > VERBOSE_INFO) {
    //   Rcpp::Rcout << "Unsetting base pairs " << basepairs << endl;
    // }
    Vec<Vec<int> > origConnectivity = structure.getStrandConnectivityMatrix();
    for (set<basepair_t>::const_iterator bp = basepairs.begin(); bp != basepairs.end(); ++bp) {
THROWIFNOT(validate_basepair(*bp),"Assertion violation in L555");
      if (structure.isBasePaired(bp->first, bp->second)) {
	// connecticvities are now updated withing rebuildDistances method
	/* string hash = BasePairTools::generateBasePairHash(bp->first, bp->second); */
	/* int seqId1 = structure.strandIds[bp->first]; */
	/* int seqId2 = structure.strandIds[bp->second]; */
	/* ASSERT (structure.strandConnectivity[seqId1][seqId2].find(hash) != structure.strandConnectivity[seqId1][seqId2].end()); */
	/* // Rcpp::Rcout << "Removing base pair " << (*bp) << endl; */
	/* structure.strandConnectivity[seqId1][seqId2].erase(hash); */
	/* if (structure.strandConnectivity[seqId1][seqId2].size() == 0) { */
	/* 	strandConnectivityChanged = true; */
	/* } */
	// structure.pairings[bp->first][bp->second] = 0;
	// structure.pairings[bp->second][bp->first] = 0;
THROWIFNOT(bp->first < static_cast<int>(structure.pairv.size()),"Assertion violation in L569");
THROWIFNOT(bp->second < static_cast<int>(structure.pairv.size()),"Assertion violation in L570");
	structure.pairv[bp->first] = NO_PAIRING_INDEX;
	structure.pairv[bp->second] = NO_PAIRING_INDEX;
      }
    }
    rebuildDistances(structure, helixLengthMin, verbose);
    if (structure.trackEventMode) {
      set<basepair_t> actualBasepairs = structure.generateBasepairs();
      // find out which are missing:
      for (set<basepair_t>::const_iterator it = basepairs.begin(); it != basepairs.end(); it++) {
	if (actualBasepairs.find(*it) == actualBasepairs.end()) {
	  // not found
	  structure.foldingEvents.push_back(folding_event_t(UNFOLDED_BASEPAIR, *it));
	}
      }
    }
    Vec<Vec<int> > finalConnectivity = structure.getStrandConnectivityMatrix();
    strandConnectivityChanged = (finalConnectivity != origConnectivity); // check if connectivity has changed.
    // if ((verbose >= VERBOSE_INFO) && strandConnectivityChanged) {
    //   size_t resultBps = structure.getBasePairCount();
    //   Rcpp::Rcout << "# Strand connectivity has changed after unsetting of base pairs. Structure after removing basepairs from structure: " << basepairs << " Starting from " << initialBps << " now to " << resultBps << endl;
    //   // structure.writeAll(Rcpp::Rcout); 
    // } 
    // ASSERT((initialBps - resultBps) == basepairs.size()); // it can be that evem more base pairs where removed!
    // ASSERT(structure.validate());
    return strandConnectivityChanged;
  }  

  /** Unsets a specified base pair. Also updates min and max distances and strand connectivity matrix */
  static void unsetBasePair_incomplete(ResidueStructure& structure, int start, int stop, int helixLengthMin, int verbose) {
    ResidueModel * residueModel = structure.residueModel;
    double reactionRadius = structure.getResidueModel()->getReactionRadius();
    // double reactionVolume = reactionRadius * reactionRadius * reactionRadius;
    const string& refAtom = structure.refAtom;
    if (verbose >= VERBOSE_FINEST) {
      Rcpp::Rcout << (structure.minDists[start][stop]) << " " << residueModel->getHelixDistMin(0, false, structure.concatSequence[start], structure.concatSequence[stop], refAtom, refAtom) << endl;
      Rcpp::Rcout << "Starting ResidueStructure::unsetBasePair " << (start+1) << " " << (stop+1) << endl;
      Rcpp::Rcout << structure.maxDists << endl;
    }
    if (start > stop) {
      unsetBasePair_incomplete(structure, stop, start, helixLengthMin, verbose);
      return;
    }


THROWIFNOT((start >= 0) && (start < static_cast<int>(structure.pairv.size())),"Assertion violation in L615");
    // ASSERT((start >= 0) && (start < static_cast<int>(structure.pairings.size())));
THROWIFNOT((start >= 0) && (start < static_cast<int>(structure.pairv.size())),"Assertion violation in L617");
    // ASSERT((stop >= 0) && (stop < static_cast<int>(structure.pairings.size())));
THROWIFNOT((stop >= 0) && (stop < static_cast<int>(structure.pairv.size())),"Assertion violation in L619");
THROWIFNOT(start <= stop,"Assertion violation in L620");
THROWIFNOT(structure.isBasePaired(start, stop),"Assertion violation in L621");
    /* ASSERT(structure.minDists[start][stop] <= residueModel->getHelixDistMin(0, false, */
    /* 									  structure.concatSequence[start], */
    /* 									  structure.concatSequence[stop], */
    /* 									  structure.refAtom, structure.refAtom)); */
    // structure.pairings[start][stop] = 0;
    // structure.pairings[stop][start] = 0;

THROWIFNOT(start < static_cast<int>(structure.pairv.size()),"Assertion violation in L629");
THROWIFNOT(stop < static_cast<int>(structure.pairv.size()),"Assertion violation in L630");

    structure.pairv[start] = NO_PAIRING_INDEX;
    structure.pairv[stop] = NO_PAIRING_INDEX;

    // find helix
    int helixId = structure.helixIds[start][stop];
THROWIFNOT(helixId >= 0,"Assertion violation in L637");
THROWIFNOT(helixId < static_cast<int>(structure.helices.size()),"Assertion violation in L638");
    Stem& helix = structure.helices[helixId];
    if ((helix.getStart() == start) && (helix.getStop() == stop)) {
      structure.helixIds[start][stop] = HELIX_ID_NONE; // remove reference to this helix
      structure.helixIds[stop][start] = HELIX_ID_NONE; // remove reference to this helix
      if (helix.getLength() == 1) {
	helix.setLength(0);
      } else {
	helix.setStart(helix.getStart() + 1);
	helix.setStop(helix.getStop() - 1);
	helix.setLength(helix.getLength() - 1);
THROWIFNOT(helix.getLength() > 0,"Assertion violation in L649");
      }
    } else if (((helix.getStart()+helix.getLength()-1) == start) && ((helix.getStop()-helix.getLength()+1) == stop)) { // other end
      structure.helixIds[start][stop] = HELIX_ID_NONE; // remove reference to this helix
      structure.helixIds[stop][start] = HELIX_ID_NONE;
      helix.setLength(helix.getLength() - 1);
    }
    
    initializeDistances(structure, start, stop, helixLengthMin, verbose);
    structure.adjustMaxDist(start, stop, RESIDUE_ADJ_DIST_MAX);
  
  // how for to go out?
  if (verbose >= VERBOSE_FINE) {
    Rcpp::Rcout << "Max distances after first unsetting of max dist of residues " << (start+1) << " " << (stop+1) << endl;    
    Rcpp::Rcout << structure.maxDists << endl;
    Rcpp::Rcout << structure.probabilities << endl;
  }
  int paCount = structure.paCount;
  int n = structure.lengthSum;
  for (int i = 1; i < static_cast<int>(paCount); ++i) {
    bool changed = false;
    for (int jj = 0; jj <= i; ++jj) {
      for (int jk = -1; jk <= 1; jk+=2) {
	int j = jj * jk;
	if ((jj == 0) && (jk == -1)) {
	  continue;
	}
	// Rcpp::Rcout << "# Testing cases for adjusting distances around position " << (start + 1) << " " << (stop+1) << " " << i << " " << j << endl;
	if (structure.adjustMaxDist(start + i, stop + j, RESIDUE_ADJ_DIST_MAX)) {
	  changed = true;
THROWIFNOT((start + i ) >= 0,"Assertion violation in L679");
THROWIFNOT((start + i ) < n,"Assertion violation in L680");
THROWIFNOT((stop + j ) >= 0,"Assertion violation in L681");
THROWIFNOT((stop + j ) < n,"Assertion violation in L682");
	  if (structure.probabilities[start + i][stop + j] > 0.0) {
	    if (structure.minDists[start+i][stop+j] > residueModel->getHelixDistMin(0, false, structure.concatSequence[start+i], structure.concatSequence[stop+j], refAtom, refAtom)) {
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "# Cannot fold distance (2) " << (start+i+1) << " " << (stop+j+1) << " " << structure.minDists[start+i][stop+j] << " " << residueModel->getHelixDistMin(0, false, structure.concatSequence[start+i], structure.concatSequence[stop+j], refAtom, refAtom) << endl;
	      }
	      structure.probabilities[start + i][stop + j] = 0.0; // cannot fold, min dist higher than bp dist
	    } else {
	      structure.probabilities[start + i][ stop + j] = pow(reactionRadius/structure.maxDists[start + i][stop + j], 3); // updated probability; FIXIT: needs updating
	      ASSERT(false); // TODO : update above formula
	    }
	    structure.probabilities[stop + j][start + i] = structure.probabilities[start + i][ stop + j];
	  }
	}
	if (structure.adjustMaxDist(start - i, stop + j, RESIDUE_ADJ_DIST_MAX)) {
	  changed = true;
THROWIFNOT((start - i ) >= 0,"Assertion violation in L698");
THROWIFNOT((start - i ) < static_cast<int>(structure.probabilities.size()),"Assertion violation in L699");
THROWIFNOT((stop + j ) >= 0,"Assertion violation in L700");
THROWIFNOT((stop + j ) < static_cast<int>(structure.probabilities.size()),"Assertion violation in L701");
	  if (structure.probabilities[start - i][stop + j] > 0.0) {
	    if (structure.minDists[start-i][stop+j] > residueModel->getHelixDistMin(0, false, structure.concatSequence[start-i], structure.concatSequence[stop+j], refAtom, refAtom)) {
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "# Cannot fold distance (2) " << (start-i+1) << " " << (stop+j+1) << " " << structure.minDists[start-i][stop+j] << " " << residueModel->getHelixDistMin(0, false, structure.concatSequence[start-i], structure.concatSequence[stop+j], refAtom, refAtom) << endl;
	      }
	      structure.probabilities[start - i][stop + j] = 0.0; // cannot fold, min dist higher than bp dist
	    } else {
	      structure.probabilities[start - i][ stop + j] = pow(reactionRadius/structure.maxDists[start - i][stop + j], 3); // updated probability
	      ASSERT(false); // FIXIT
	    }
	    structure.probabilities[stop + j][start - i] = structure.probabilities[start - i][ stop + j];
	  }
	} 
	if (structure.adjustMaxDist(start + j, stop + i, RESIDUE_ADJ_DIST_MAX)) {
	  changed = true;
THROWIFNOT((start + j ) >= 0,"Assertion violation in L717");
THROWIFNOT((start + j ) < n,"Assertion violation in L718");
THROWIFNOT((stop + i ) >= 0,"Assertion violation in L719");
THROWIFNOT((stop + i ) <  n,"Assertion violation in L720");
	  if (structure.probabilities[start + j][stop + i] > 0.0) {
	    if (structure.minDists[start+j][stop+i] > residueModel->getHelixDistMin(0, false, structure.concatSequence[start+j], structure.concatSequence[stop+i], refAtom, refAtom)) {
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "# Cannot fold distance (3) " << (start+j+1) << " " << (stop+i+1) << " " << structure.minDists[start+j][stop+i] << " " << residueModel->getHelixDistMin(0, false, structure.concatSequence[start+j], structure.concatSequence[stop+i], refAtom, refAtom) << endl;
	      }
	      structure.probabilities[start + j][stop + i] = 0.0; // cannot fold, min dist higher than bp dist
	    } else {
	      structure.probabilities[start + j][ stop + i] = pow(reactionRadius/structure.maxDists[start + j][stop + i], 3); // updated probability
	      ASSERT(false); // FIXIT
	    }
	    structure.probabilities[stop + i][start + j] = structure.probabilities[start + j][ stop + i];
	  }
	}
	if (structure.adjustMaxDist(start + j, stop - i, RESIDUE_ADJ_DIST_MAX)) {
	  changed = true;
THROWIFNOT((start + j ) >= 0,"Assertion violation in L736");
THROWIFNOT((start + j ) < static_cast<int>(structure.probabilities.size()),"Assertion violation in L737");
THROWIFNOT((stop - i ) >= 0,"Assertion violation in L738");
THROWIFNOT((stop - i ) < static_cast<int>(structure.probabilities.size()),"Assertion violation in L739");
	  if (structure.probabilities[start + j][stop - i] > 0.0) {
	    if (structure.minDists[start+j][stop-i] > residueModel->getHelixDistMin(0, false, structure.concatSequence[start+j], structure.concatSequence[stop-i], refAtom, refAtom)) {
	      if (verbose >= VERBOSE_FINEST) {
		Rcpp::Rcout << "# Cannot fold distance (4) " << (start+j+1) << " " << (stop-i+1) << " " << structure.minDists[start+j][stop-i] << " " << residueModel->getHelixDistMin(0, false, structure.concatSequence[start+j], structure.concatSequence[stop-i], refAtom, refAtom) << endl;
	      }
	      structure.probabilities[start + j][stop - i] = 0.0; // cannot fold, min dist higher than bp dist
	    } else {
	      structure.probabilities[start + j][ stop - i] = pow(reactionRadius/structure.maxDists[start + j][stop - i], 3); // updated probability
	      ASSERT(false); // FIXIT
	    }
	    structure.probabilities[stop - i][start + j] = structure.probabilities[start + j][ stop - i];
	  }
	}
	// Rcpp::Rcout << "# Testing cases for adjusting distances around position " << (start + 1) << " " << (stop+1) << " " << i << " " << j << endl;
      }
      // Rcpp::Rcout << "# Finished testing cases for adjusting distances..." << endl;
    }
    // Rcpp::Rcout << "Max distance matrix after iteration " << i << endl;
    //      Rcpp::Rcout << maxDists << endl;
    // Rcpp::Rcout << structure.probabilities << endl;
    if (!changed) {
      if (verbose >= VERBOSE_FINEST) {
	Rcpp::Rcout << "Quitting loop because no further updates where necessary" << endl;
      }
      break;
    }
  }
  size_t strandId1 = structure.strandIds[start];
  size_t strandId2 = structure.strandIds[stop];
THROWIFNOT(strandId1 < structure.strandConnectivity.size(),"Assertion violation in L769");
THROWIFNOT(strandId2 < structure.strandConnectivity[strandId1].size(),"Assertion violation in L770");
  string hash = BasePairTools::generateBasePairHash(start, stop);
  structure.strandConnectivity[strandId1][strandId2].erase(hash); 
  structure.strandConnectivity[strandId2][strandId1].erase(hash);
  structure.updateResidueProbabilities();
  // if (verbose >= VERBOSE_FINEST) {
  //   Rcpp::Rcout << "# Finished ResidueStructure::setBasePair " << (start+1) << " " << (stop+1) << endl;
  //   Rcpp::Rcout << "# Current strand connectivity: " << endl << structure.strandConnectivity << endl;
  // }
}


};

#endif
