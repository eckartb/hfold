#ifndef __RNA_SECONDARY_STRUCTURE__
#define __RNA_SECONDARY_STRUCTURE__

#include <iostream>
#include <set>
#include <string>
#include <ctype.h>
#include "debug.h"
#include "Vec.h"
#include "Stem.h"
#include "AbstractSecondaryStructure.h"
#include "RnaInteractionType.h"


using namespace std;

class RnaSecondaryStructure : public AbstractSecondaryStructure {

 public:

  RnaSecondaryStructure() : energy(0.0), starts(Vec<int>(1,0)) { initSegmentIds(); }

  RnaSecondaryStructure(const basepair_set_type& _basePairs) : basePairs(_basePairs), energy(0.0), starts(Vec<int>(1,0)) { 
  }

  RnaSecondaryStructure(const Vec<Stem>& stems,
			const sequence_type& _sequence) : energy(0.0), sequence(_sequence), starts(Vec<int>(1,0)) {
    
    basePairs = basepair_set_type(_sequence.size(), UNPAIRED_ID);
    for (size_type i = 0; i < stems.size(); ++i) {
      for (Stem::index_type j = 0; j < stems[i].getLength(); ++j) {
	setPaired(stems[i].getStart() + j,
		  stems[i].getStop() - j);
      }
    }
    initSegmentIds();
  }

  /** Effectively setting several sequences */
  RnaSecondaryStructure(const Vec<Stem>& stems,
			const sequence_type& _sequence, 
			const Vec<int>& _starts) : energy(0.0), sequence(_sequence), starts(Vec<int>(1,0)) {
    basePairs = basepair_set_type(_sequence.size(), UNPAIRED_ID);
    for (size_type i = 0; i < stems.size(); ++i) {
      for (Stem::index_type j = 0; j < stems[i].getLength(); ++j) {
	setPaired(stems[i].getStart() + j,
		  stems[i].getStop() - j);
      }
    }
    starts = _starts;
    initSegmentIds();
  }

  /** Effectively setting several sequences */
  RnaSecondaryStructure(const sequence_type& _sequence, 
			const Vec<int>& _starts) : energy(0.0), sequence(_sequence), starts(Vec<int>(1,0)) {
    basePairs = basepair_set_type(_sequence.size(), UNPAIRED_ID);
    starts = _starts;
    initSegmentIds();
  }

  RnaSecondaryStructure(size_type numRes) : energy(0.0), starts(Vec<int>(1,0)) { 
    basePairs = basepair_set_type(numRes, UNPAIRED_ID);
    initSegmentIds();
  }

  RnaSecondaryStructure(const sequence_type& _sequence) : energy(0.0), sequence(_sequence), starts(Vec<int>(1, 0)) { 
    size_type numRes = _sequence.size();
    basePairs = basepair_set_type(numRes, UNPAIRED_ID);
    initSegmentIds();
  }

  RnaSecondaryStructure(const RnaSecondaryStructure& other) { copy(other); }

  virtual ~RnaSecondaryStructure() { }

  const RnaSecondaryStructure& operator = (const RnaSecondaryStructure& other) {
    if (this != &other) {
      copy(other);
    }
    return *this;
  }

  void addContacts(Vec<Vec<double> >& matrix) const {
    PRECOND(matrix.size() == size());
    PRECOND(matrix[0].size() == size());
    for (size_type i = 0; i < size(); ++i) {
      index_type bp = getBasePair(i);
      if ((bp >= 0) && (bp < static_cast<index_type>(size())) && (bp != UNPAIRED_ID)) {
	matrix[i][bp] += 1.0;
	// matrix[bp][i] += 1.0; // not necessary, because base pair is already represented twice; would be overcounting
      }
    }
  }

  void addContacts(Vec<Vec<int> >& matrix) const {
    PRECOND(matrix.size() == size());
    PRECOND(matrix[0].size() == size());
    for (size_type i = 0; i < size(); ++i) {
      index_type bp = getBasePair(i);
      if ((bp >= 0) && (bp < static_cast<index_type>(size())) && (bp != UNPAIRED_ID)) {
	matrix[i][bp] += 1;
	// matrix[bp][i] += 1;
      }
    }
  }

  void addContacts(Vec<Vec<unsigned int> >& matrix) const {
    PRECOND(matrix.size() == size());
    PRECOND(matrix[0].size() == size());
    for (size_type i = 0; i < size(); ++i) {
      index_type bp = getBasePair(i);
      if ((bp >= 0) && (bp < static_cast<index_type>(size())) && (bp != UNPAIRED_ID)) {
	matrix[i][bp] += 1;
	// matrix[bp][i] += 1;
      }
    }
  }

  index_type getSegmentStart(size_type id) const {
    return starts[id];
  }

  index_type getSegmentEnd(size_type id) const {
    if (id < starts.size()) {
      return starts[id];
    } 
    return getLength();
  }

  friend ostream & operator << (ostream& os, const RnaSecondaryStructure& rval); 

  virtual const basepair_set_type& getBasePairs() const { return basePairs; }

  /** returns base-paired partner, -1 if otherwise */
  virtual index_type getBasePair(index_type n) const { 
    PRECOND(n < static_cast<index_type>(size()));
    return basePairs[n]; 
  }

  /** Counts number of base pairs */
  virtual size_type getBasePairCount() const {
    int count = 0;
    for (size_type i = 0; i < size(); ++i) {
      if (isPaired(i)) {
	++count;
      }
    }
    return count/2; // otherwise every base pair would be counted twice
  }

  virtual double getEnergy() const { return energy; }

  virtual void setEnergy(double x) { energy = x; }

  /** returns sequence length, but in type "index_type" (typically int instead of unsigned int) */
  virtual index_type getLength() const { return static_cast<index_type>(size()); }
  
  virtual const sequence_type& getSequence() const { return sequence; }

  virtual const Vec<int>& getStarts() const { return starts; }

  virtual size_type getSegmentCount() const { return starts.size(); }

  virtual string getSegmentSequence(size_type n) const {
THROWIFNOT(n < getSegmentCount(),"Assertion violation in L166");
    size_type firstIndex = starts[n];
    size_type lastIndex = sequence.size();
    if ((n + 1) < starts.size()) {
      lastIndex = starts[n + 1]; // points to one element higher than last valid index of segment n
    }
THROWIFNOT(lastIndex > firstIndex,"Assertion violation in L172");
    return sequence.substr(firstIndex, lastIndex - firstIndex);
  }

  /** returns true if nt n is involved in a base pair */
  virtual bool isBasePaired(index_type n) const { 
    PRECOND(n < static_cast<index_type>(basePairs.size()));
    return basePairs[n] >= 0; 
  }

  virtual bool isConsistent() {
    for (size_type i = 0; i < basePairs.size(); ++i) {
      if (basePairs[i] >= 0) {
	if (basePairs[basePairs[i]] != static_cast<index_type>(i)) {
	  
	  return false;
	}
      }
    }
    return true;
  }

  virtual bool isPaired(index_type i, index_type j) const { return basePairs[i] == j; }

  virtual bool isPaired(index_type i) const { return basePairs[i] >= 0; }

  virtual void setBasePairs(const basepair_set_type& bp) { basePairs = bp; }

  virtual void setPaired(index_type i, index_type j) { 
    PRECOND(i < getLength());
    PRECOND(j < getLength());
    PRECOND(i != j);
    basePairs[i] = j; 
    basePairs[j] = i;
    POSTCOND(isPaired(i, j));
    if (! isConsistent()) {
      Rcpp::Rcout << "Internal error after setting: " << (i+1) << " " << (j+1) << " " 
	   << basePairs[i] << " " << basePairs[j] << " size: " << size() << endl;
      Rcpp::Rcout << "Dump: " << *this << endl;
    }
THROWIFNOT(isConsistent(),"Assertion violation in L212");
  }

  virtual void setUnpaired(index_type i, index_type j) { 
    PRECOND(i < getLength());
    PRECOND(j < getLength());
    basePairs[i] = UNPAIRED_ID; 
    basePairs[j] = UNPAIRED_ID;
  }

  virtual void setSequence(const sequence_type& seq) { 
    sequence = seq;
    if (basePairs.size() == 0) {
      basePairs = basepair_set_type(seq.size(), UNPAIRED_ID);
    }
  }

  virtual void setStarts(const Vec<int>& _starts) {
    ASSERT(_starts.size() > 0); // even if only one sequences, there has to be a leading "0"
THROWIFNOT(_starts[0] == 0,"Assertion violation in L231");
    starts = _starts;
    initSegmentIds();
  }
  
  /** returns sequence length */
  virtual size_type size() const { return basePairs.size(); }

  virtual void copy(const RnaSecondaryStructure& other) {
    basePairs = other.basePairs;
    designCosts = other.designCosts;
    energy = other.energy;
    rnaFlags = other.rnaFlags;
    sequence = other.sequence;
    starts = other.starts;
    segmentIds = other.segmentIds;
  }

  /** Two RnaSecondaryStructures are equal if their base pairs
    * and sequences are equal. */
  virtual bool operator==(const RnaSecondaryStructure& other) const {
    return sequence == other.sequence && basePairs == other.basePairs; 
  }

  /** Two RnaSecondaryStructures are unequal if their sequences or
    * base pairs differ */
  virtual bool operator!=(const RnaSecondaryStructure& other) const {
    return ! operator==(other);
  }

  /** returns true, if two base pairs form a pseudoknot */
  static bool isPseudoKnotted(index_type startA, index_type stopA,
			      index_type startB, index_type stopB) {
    PRECOND(startA < stopA);
    PRECOND(startB < stopB);
    PRECOND(startA != startB);
    PRECOND(stopA != stopB);
    return ( ((startA < startB) && (startB < stopA) && (stopB > stopA))
	     || ((startB < startA) && (startA < stopB) && (stopA > stopB)));
  }

  void initSegmentIds() {
    PRECOND(starts.size() > 0);
    segmentIds = Vec<size_type>(size(), (starts.size()-1));
    size_type count = 0;
    rnaFlags = Vec<int>(starts.size(), 1); // all sequences are thought to be RNA
    for (size_type i = 0; i < (starts.size()-1); ++i) {
      if (islower(sequence[starts[i]]) ) {
	rnaFlags[i] = false; // must be DNA, indicated by lower case
      }
      for (index_type j = starts[i]; j < starts[i+1]; ++j) {
THROWIFNOT(j < static_cast<index_type>(segmentIds.size()),"Assertion violation in L282");
	segmentIds[j] = count;
      }
      ++count;
    }
    // Rcpp::Rcout << "# RNA/DNA flags: " << rnaFlags << endl;
    // Rcpp::Rcout << "Finished initSegmentIds: " << starts << endl << segmentIds << endl;
  }

  string getClassName() const { return "RnaSecondaryStructure"; }

  Vec<double> getDesignCosts() const {
    return designCosts;
  }

  /** Converts absolute position into segment position */
  size_type getSegmentFromPosition(index_type pos) {
    return segmentIds[pos];
  }

  const Vec<size_type> getSegmentIds() const { return segmentIds; }

  /** Converts absolute position into segment position */
  index_type getPositionInSegment(index_type pos) {
    size_type segmentId = getSegmentFromPosition(pos);
THROWIFNOT(segmentId < starts.size(),"Assertion violation in L307");
THROWIFNOT(pos >= starts[segmentId],"Assertion violation in L308");
    return pos - starts[segmentId];
  }

  /** Generates interaction matrix between sequences (segments) id1 and id2 */
  Vec<Vec<int> > generateSegmentInteractionMatrix(size_type id1, size_type id2) const {
    string seq1 = getSegmentSequence(id1);
    string seq2 = getSegmentSequence(id2);
    Vec<Vec<int> > result(seq1.size(), Vec<int>(seq2.size(), RnaInteractionType::NO_INTERACTION));
    /*       for (int i = 0; i < result.size(); ++i) { */
    /* 	for (int j = 0; j < result[i].size(); ++j) { */
    /* 	  result[i][j] = RnaInteractionType.NO_INTERACTION; */
    /* 	} */
    /*       } */
    index_type seg1Start = getSegmentStart(id1);
    index_type seg1End = getSegmentEnd(id1);
    index_type seg2Start = getSegmentStart(id2);
    index_type seg2End = getSegmentEnd(id2);
    for (int pos1 = seg1Start; pos1 < seg1End; ++pos1) {
      // int pos1 = interaction.getResidue1().getPos();
      int pos2 = getBasePair(pos1);
      if ((pos2 < seg2Start) || (pos2 >= seg2End)) {
	continue;
      }
      index_type lpos1 = pos1 - seg1Start;
      index_type lpos2 = pos2 - seg2Start;
      result[lpos1][lpos2] = RnaInteractionType::WATSON_CRICK;
      if (id1 == id2) {
	result[lpos2][lpos1] = result[lpos1][lpos2]; // if same sequence: symmetric
      }
    }
/*     // sets rows and columns to UNKNOWN_INTERACTION, if property seqstatus is "ignore" */
/*     for (int i = 0; i < result.size(); ++i) { */
/*       if (("ignore".equals(seq1.getResidue(i).getProperty("seqstatus"))) */
/* 	  || (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq1.getResidue(i).getProperty(SequenceStatus.name)))) { */
/* 	for (int j = 0; j < result[i].size(); ++j) { */
/* 	  result[i][j] = RnaInteractionType.UNKNOWN_SUBTYPE; */
/* 	} */
/*       } */
/*     } */
/*     for (int i = 0; i < result[0].size(); ++i) { */
/*       if (("ignore".equals(seq2.getResidue(i).getProperty("seqstatus")))  */
/* 	  || (ignoreNotConstantMode && SequenceStatus.fragment.equals(seq2.getResidue(i).getProperty(SequenceStatus.name)))) { */
/* 		for (int j = 0; j < result.size(); ++j) { */
/* 		    result[j][i] = RnaInteractionType.UNKNOWN_SUBTYPE; */
/* 		} */
/* 	    } */
/* 	} */
/* 	return result; */
/*     } */
    return result;
  }
    
    Vec<Vec<Vec<Vec<int> > > > generateSegmentInteractionMatrices() const {
      size_type n = getSegmentCount();
      Vec<Vec<Vec<Vec<int> > > > matrices(n, Vec<Vec<Vec<int> > >(n));
      for (size_type i = 0; i < n; ++i) {
	for (size_type j = i; j < n; ++j) {
	  matrices[i][j] = generateSegmentInteractionMatrix(i, j);
	}
      }
      return matrices;
    }

    void setDesignCosts(const Vec<double>& _designCosts) {
      designCosts = _designCosts;
    }
    
    bool validateBasePairs() const {
      PRECOND(size() > 0);
      // check that no base is paired twice
      set<index_type> pairedBases;
      for (index_type i = 0; i < static_cast<index_type>(size()); ++i) {
	if (basePairs[i] != UNPAIRED_ID) {
	  if (basePairs[i] < 0 || basePairs[i] >= static_cast<index_type>(size())) {
	    return false;
	  }
	  if (basePairs[basePairs[i]] != i) {
	    return false; // should be stored twice
	  }
	  // try to find:
	  if (pairedBases.find(basePairs[i]) != pairedBases.end()) {
	    return false; // this base is already paired!
	  } else {
	    pairedBases.insert(basePairs[i]);
	  }
	}
      }
      return true;
    }

    bool validate() const {
      return size() > 0 && validateBasePairs() && (segmentIds.size() == size());
    }

 private:

  basepair_set_type basePairs; // shows binding partner of each base (-1 if unpaired)
  Vec<double> designCosts;
  double energy;
  Vec<int> rnaFlags;
  sequence_type sequence;
  Vec<int> starts; // for multiple sequences
  Vec<size_type> segmentIds; // array of sequence indices
};

inline
ostream & operator << (ostream& os, const RnaSecondaryStructure& rval)
{
  // PRECOND(rval.sequence.size() == rval.size());
  if (rval.sequence.size() == rval.size()) {
    for (RnaSecondaryStructure::size_type i = 0; i < rval.size(); ++i) {
      os << (i+1) << "\t" << (rval.basePairs[i]+1) << "\t" << rval.sequence[i] << endl;
    }
  } else {
    for (RnaSecondaryStructure::size_type i = 0; i < rval.size(); ++i) {
      os << (i+1) << "\t" << (rval.basePairs[i]+1) << endl;
    }
  }
  return os;
}


#endif
