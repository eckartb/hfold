#ifndef _STEM_TOOLS_H_
#define _STEM_TOOLS_H_

#include <iostream>
#include <string>
#include "Vec.h"
#include "Stem.h"
#include "stemhelp.h"
#include "StringTools.h"
#include <vector>
#include <math.h>
#include "RnaSecondaryStructure.h"
#include "vectornumerics.h" // only needed for minimum
#include "StrandContainer.h"
#include <algorithm>

class StemTools {

 public:

  typedef Stem::index_type index_type;
  typedef unsigned int size_type;
  typedef Stem stem_type;
  typedef Vec<Stem> stem_set;
  typedef Vec<Vec<double> > matrix_type;
  typedef Vec<double> matrix_row_type;
  typedef pair<double, double> point_type;

  /** Length of shortest helix. Returns -1 if not helices are given. */
  static index_type findMinimumHelixLength(const Vec<Stem>& helices) {
    if (helices.size() == 0) {
      return -1;
    }
    index_type len = helices[0].getLength(); 
    for (size_t i = 1; i < helices.size(); ++i) {
      if (helices[i].getLength() < len) {
	len = helices[i].getLength();
      }
    }
    return len;
  }

  /** Both startPos and stopPos are zero-based inclusive indices of minimum and maximum positions. For the output, "1" is added. */
  static void printStemsByPosition(ostream& os, const Vec<Stem>& stems, index_type startPos, index_type stopPos) {
    index_type offset = 1;
    for (index_type pos = startPos; pos <= stopPos; ++pos) {
      os << (pos+offset) << " :";
      for (size_t i = 0; i < stems.size(); ++i) {
	if (stems[i].usesBase(pos)) {
	  os << " " << stems[i];
	}
      }
      os << endl;
    }
  }

  static void printStemsByPosition(ostream& os, const Vec<Stem>& stems, 
				   const Vec<string>& sequences) {
    index_type offset = 1;
    index_type pos = 0;
    for (size_t s = 0; s < sequences.size(); ++s) {
      string seq = sequences[s];
      for (size_t j = 0; j < seq.size(); ++j) {
	os << (s+offset) << " " << (j+offset) << " " << (pos+offset) << " " << seq.size() << " " << seq[j] << " :";
	for (size_t i = 0; i < stems.size(); ++i) {
	  if (stems[i].usesBase(pos)) {
	    os << " " << stems[i];
	  }
	}
	os << endl;
	++pos;
      }
    }
  }

  /** Elongate stem to maximal extent */
  static Stem elongateStem(const Stem& stem, const string& seq, const Vec<size_t>& seqIds,  bool guAllowed) {
    int incLen = 0;
    int start  = stem.getStart();
    int stop = stem.getStop();
    int len = stem.getLength();
    int decStart = 0;
    int n = static_cast<int>(seq.size());
    for (int i = 1; i < n; ++i) {
      if ((start-i < 0) || (stop+i >= n) || (seqIds[start-i] != seqIds[start]) || (seqIds[stop+i] != seqIds[stop]) || (!ResidueModel::isWatsonCrick(seq[start-i], seq[stop+i], guAllowed))) {
	break;
      }
      decStart = i; // increase length this much
    }
    start -= decStart;
    stop += decStart;
    len += decStart;
    for (int i = 1; i < n; ++i) {
      if ((start+len-1+i >= n) || (stop-len+1-i < 0) || (seqIds[start+len-1+i] != seqIds[start]) || (seqIds[stop-len+1-i] != seqIds[stop]) ||  (!ResidueModel::isWatsonCrick(seq[start+len-1+i], seq[stop-len+1-i], guAllowed))) {
	break;
      }
      incLen = i; // increase length this much
    }
    len += incLen;
    return Stem(start, stop, len);
  }

  /** Generates stem table for one sequence that is "cut" at certain points. 
   * Even for a single sequence, the first element of the "starts" array has to be alwasy zero. 
   * guallowed: if true, allow GU or UG base pairs in stems
   * guEndAllowed: if true, allow GU or UG base pairs at end of stems
   */
  static Vec<Stem> generateLongestStems(const StrandContainer& strands, size_t minExtendability, bool guAllowed, bool guEndAllowed, const Vec<int>& dividers) {
    // Rcpp::Rcout << "Starting StemTools::generateoLongestStems" << endl;
    bool longestOnly=true;
    int lengthModulo = 1;
    ERROR_IF(!longestOnly, "Currently StemTools::generateLongestStems needs to be run with only computing longest helices.");
    string s = strands.getConcatSequence();
    Vec<int> seqId = strands.getStrandIds();
  
    // Rcpp::Rcout << "######## Creating stems with minimum length " << minExtendability << endl;
    Vec<Stem> result;
    set<pair<int, int> > forbiddenStrandCombos;
    if (dividers.size() > 0) {
      DERROR("StemTools::generateLongestStems: Dividers are currently not implemented.");
    }
    int n = static_cast<int>(s.size());
    for (int i = 0; i < n; ++i) { // start positions
      for (int j = i + 1; j < n; ++j) { // stop positions
	int s1 = seqId[i];
	int s2 = seqId[j];
THROWIFNOT(s1 <= s2,"Assertion violation in L125");
	if ((s1 < s2) && (forbiddenStrandCombos.find(pair<int, int>(s1,s2)) != forbiddenStrandCombos.end())) {
	  continue; // forbidden strand combinations, skip
	}
	bool interStrand = (s1 != s2);
	bool isWob = strands.isWobble(i,j); // true if not wobble GU or UG base pair
	// Rcpp::Rcout << "Position pair " << (i+1) << " " << (j+1) << " " << seqId[i] << " " << seqId[j] << " interstrand: " << interStrand << " " << s[i] << " " << s[j] << endl;	
	if (strands.isWatsonCrick(i, j, guAllowed) && (guEndAllowed || (!isWob))) {  // must be complementary
	  if ((i == 0) || ((j+1) >= static_cast<int>(s.size())) || (seqId[i] != seqId[i-1]) || (seqId[j+1] != seqId[j]) || 
	      (!strands.isWatsonCrick(i-1, j+1, guEndAllowed) ) ) {  // must be beginning of stem
	    // if (((i == 0) || (seqId[i] != seqId[i-1]) ||  ((j+1) >= static_cast<int>(s.size()))
	    if (interStrand || ((j - i) > LOOP_LEN_MIN)) {
	      // int k = 0;
	      int lastGoodK = -1;
	      Vec<Stem> tmpStems;
	      for (int k = 0; ((i+k) < n) && (j >= k) && (i+k < j -k) && strands.isWatsonCrick(i+k,j-k, guAllowed) && (seqId[i] == seqId[i+k]) && (seqId[j] == seqId[j-k])
		     && (interStrand || ((j-i-k-k) > LOOP_LEN_MIN)); ++k) {
		lastGoodK = k;
		if ((k+1) >= static_cast<int>(minExtendability)) {
		  Stem stem(i,j,k+1);
		  
		  THROWIFNOT(strands.isWatsonCrick(stem, guAllowed),"Assertion violation in StemTools.h :  L143");
		  // stem.setEnergy(computeViennaStemEnergy(stem, s));
		  // if (stem.getEnergy() < HELIX_ENERGY_MAX) {
		  THROWIFNOT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1,"Assertion violation in L146");
		  THROWIFNOT(stem.getStart() + stem.getLength() - 1 < s.size(), "Internal error StemTools L148");
		  THROWIFNOT(stem.getStop() + 1 >= stem.getLength(), "Internal error StemTools L149");
		  // Rcpp::Rcout << "Created stem: " << stem << endl;
		  if ((stem.getLength() >= static_cast<int>(minExtendability)) ) {
		    // && startStructure.isPlaceable(stem)) {
		    if ((stem.getLength() % lengthModulo) == 0 && strands.isWatsonCrick(i+k,j-k,guEndAllowed)) {
		      result.push_back(stem);
		    }
		    // Rcpp::Rcout << "Added stem to stem list: " << stem << " : " << minExtendability << endl;
		  } else {
		    // Rcpp::Rcout << "Not added stem to stem list: " << stem << " : " << minExtendability << " because it is not placeable" << endl;
		  }		    
		}
	      } // for (int k = 0; ...
	      if (tmpStems.size() > 0) {
		if (longestOnly) {
		  result.push_back(tmpStems[tmpStems.size()-1]);
		} else {
		  for (size_t i = 0; i < tmpStems.size(); ++i) {
		    result.push_back(tmpStems[i]);
		  }
		}
	      }
	      /* if (longestOnly) { */
	      /* 	if ((lastGoodK+1) >= static_cast<int>(minExtendability)) { */
	      /* 	  Stem stem(i,j,lastGoodK+1); */
	      /* 	  EASSERT(strands.isWatsonCrick(stem, guAllowed)); */
	      /* 	  // stem.setEnergy(computeViennaStemEnergy(stem, s)); */
	      /* 	  // if (stem.getEnergy() < HELIX_ENERGY_MAX) { */
	      /* 	  ASSERT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1); */
	      /* 	  // Rcpp::Rcout << "Created stem: " << stem << endl; */
	      /* 	  if ((stem.getLength() >= static_cast<int>(minExtendability))) { */
	      /* 	    // && startStructure.isPlaceable(stem)) { */
	      /* 	    result.push_back(stem); */
	      /* 	    // Rcpp::Rcout << "Added stem to stem list: " << stem << " : " << minExtendability << endl; */
	      /* 	  } else { */
	      /* 	    // Rcpp::Rcout << "Not added stem to stem list: " << stem << " : " << minExtendability << " because it is not placeable" << endl; */
	      /* 	  }		     */
	      /* 	} */
	      /* }  */
	      
	    }
	  }
	}
      }
    }
    // Rcpp::Rcout << "Finished StemTools::generateoLongestStems" << endl;
    return result;
}

  /** Generates stem table for one sequence that is "cut" at certain points. Originates from class "IndyFold"
   */
  static Vec<Stem> generateAllStems(StrandContainer * sequences, const string& s, const Vec<int>& seqId, bool guAllowed, int verbose) {
    Vec<Stem> result;
    int n = static_cast<int>(s.size());
    if (verbose > 3) {
      Rcpp::Rcout << "# Searching sequence of length " << s.size() << endl;
      Rcpp::Rcout << "# Generated seqIds: " << endl << seqId << endl;
    }
    for (int i = 0; i < n; ++i) {
      for (int j = i + 1; j < n; ++j) {
	int s1 = seqId[i];
	int s2 = seqId[j];
	bool interStrand = (s1 != s2);
	// Rcpp::Rcout << "Position pair " << (i+1) << " " << (j+1) << " " << seqId[i] << " " << seqId[j] << " interstrand: " << interStrand << " " << s[i] << " " << s[j] << endl;	
	if (sequences->isWatsonCrick(i, j, guAllowed)) {  // must be complementary
	  // if (((i == 0) || (seqId[i] != seqId[i-1]) ||  ((j+1) >= static_cast<int>(s.size()))
	  // || (!sequences->isWatsonCrick(s[i-1], s[j+1], guAllowed)))  // must be beginning of stem
	  if (interStrand || ((j - i) > LOOP_LEN_MIN)) {
	    // Rcpp::Rcout << "line 189" << endl;
	    int k = 0;
	    // int lastGoodK = -1;
	    for (; ((i+k) < n) && (j >= k) && (i+k < j -k) && sequences->isWatsonCrick(i+k,j-k, guAllowed) && (seqId[i] == seqId[i+k]) && (seqId[j] == seqId[j-k])
		   && (interStrand || ((j-i-k-k) > LOOP_LEN_MIN)); ++k) {
	      if (k > 0) { // stem length will be k+1; this corresponds to a minimum stem length of 2
		Stem stem(i,j,k+1);
		// stem.setEnergy(computeViennaStemEnergy(stem, s));
THROWIFNOT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1,"Assertion violation in L222");
		// Rcpp::Rcout << "Created stem: " << stem << endl;
THROWIFNOT(sequences->isWatsonCrick(stem, guAllowed),"Assertion violation in L224");
		result.push_back(stem);
	      }
	    }
	  }
	}
      }
    }
    return result;
  }



  /** New version for stem generation. Generates stem table for one sequence that is "cut" at certain points. 
   * Even for a single sequence, the first element of the "starts" array has to be alwasy zero. 
   * If constraintsOnParentStem is true, then the longest parent stem has to fullfill length restrictions.
   * lenMin, lenMax: inclusive indices on length of helices
   */
  static Vec<Stem> generateAllStemsDEPRECATED(const StrandContainer& strands, int lenMin, bool guAllowed, const Vec<int>& dividers, int lengthModulo, int lenMax, bool constraintsOnParentStem=false) {
    DERROR("Implementation not supported.");
    // Rcpp::Rcout << "Starting generateAllStems" << endl;
    Vec<Stem> stems = generateLongestStems(strands, 2, guAllowed,guAllowed, dividers);
    Vec<Stem> stems2;
    for (size_t i = 0; i < stems.size(); ++i) {
      if (stems[i].getLength() >= lenMin) {
	if (constraintsOnParentStem && (stems[i].getLength() > lenMax)) {
	  continue;
	}
	Vec<Stem> stems3 = generateSubStems(stems[i], lenMin, lengthModulo);
	if ((lenMax <= 0) || (stems[i].getLength() <= lenMax)) {
	  stems2.push_back(stems[i]);
	}
	for (size_t j = 0; j < stems3.size(); ++j) {
	  if ((lenMax <= 0) || (stems3[j].getLength() <= lenMax)) {
	    stems2.push_back(stems3[j]);
	  }
	}
      }
    }
#ifndef NDEBUG
    if (lenMax > 0) {
      for (size_t j = 0; j < stems2.size(); ++j) {
THROWIFNOT(stems2[j].getLength() <= lenMax,"Assertion violation in L266");
      }
    }
#endif
    // Rcpp::Rcout << "Finished generateAllStems" << endl;
    return stems2;
  }

  /** New version for stem generation. Generates stem table for one sequence that is "cut" at certain points. 
   * Even for a single sequence, the first element of the "starts" array has to be alwasy zero. 
   */
  static Vec<Stem> generateAllFlankedStems(const StrandContainer& strands, size_t minExtendability, bool guAllowed, bool guEndAllowed, const Vec<int>& dividers, bool longestOnly, int lengthModulo=1) {
    Vec<Stem> stems = generateLongestStems(strands, minExtendability, guAllowed, guEndAllowed, dividers);
    if (!longestOnly) {
      Vec<Stem> stems2;
      for (size_t i = 0; i < stems.size(); ++i) {
	Vec<Stem> stems3 = generateFlankedStems(stems[i], minExtendability, lengthModulo);
	for (size_t j = 0; j < stems3.size(); ++j) {
	  stems2.push_back(stems3[j]);
	}
      }
      for (size_t j = 0; j < stems2.size(); ++j) {
	stems.push_back(stems2[j]);
      }
    }
    return stems;
}

  static bool hasCommonBases(const Vec<Stem>& stems) {
    size_t n = stems.size();
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = i+1; j < n; ++j) {
	if (stems[i].hasCommonBases(stems[j])) {
	  return true;
	}
      }
    }
    return false;
  }
  
  /** Returns true, iff for each helix there is at least one other helix with which it has residues in common.
   * This leaves the possibility, that the set consist of several non-interacting subsets. */
 static bool isMutuallyOverlappingHelixSet(const Vec<size_t>& chosenToStemIds, const Vec<Stem>& stems) {
   size_t n = chosenToStemIds.size();
   for (size_t i = 0; i < n; ++i) {
     bool found = false;
     for (size_t j = i+1; j < n; ++j) {
       if (stems[i].hasCommonBases(stems[j])) {
	 found = true;
	 break;
       }
     }
     if (!found) {
       return false;
     }
   }
   return true;
 }
 
 static string getSequence1(const Stem& stem, const string seq) {
   string result(stem.getLength(), 'x');
THROWIFNOT(result.size() == static_cast<size_t>(stem.getLength()),"Assertion violation in StemTools.h :  L327");
   for (Stem::index_type i = 0; i < stem.getLength(); ++i) {
     result[i] = seq[stem.getStart()+i];
   }
   return result;
 }

 static string getSequence2(const Stem& stem, const string seq) {
   string result(stem.getLength(), 'x');
THROWIFNOT(result.size() == static_cast<size_t>(stem.getLength()),"Assertion violation in StemTools.h :  L336");
   for (Stem::index_type i = 0; i < stem.getLength(); ++i) {
     result[i] = seq[stem.getStop()-i];
   }
   return result;
 }

  /** Converts a 2 stems into a distance. Each stems is treated like two 2D points. This leads to 4 possible distances, the smallest ist used  */
  static double stemEndDist(const stem_type& stem1, const stem_type& stem2) {
    point_type s1b(static_cast<double>(stem1.getStart()),
		   static_cast<double>(stem1.getStop()));  // point corresponding to first base pair of stem1
    point_type s1e(static_cast<double>(stem1.getStart() + stem1.getLength() - 1),
		   static_cast<double>(stem1.getStop()  - stem1.getLength() + 1));  // point corresponding to first base pair of stem1
    point_type s2b(static_cast<double>(stem2.getStart()),
		   static_cast<double>(stem2.getStop()));  // point corresponding to first base pair of stem1
    point_type s2e(static_cast<double>(stem2.getStart() + stem2.getLength() - 1),
		   static_cast<double>(stem2.getStop()  - stem2.getLength() + 1));  // point corresponding to first base pair of stem1
    double d1b2b = sqrt((s1b.first-s2b.first)*(s1b.first-s2b.first) + (s1b.second-s2b.second)*(s1b.second-s2b.second));
    double d1b2e = sqrt((s1b.first-s2e.first)*(s1b.first-s2e.first) + (s1b.second-s2e.second)*(s1b.second-s2e.second));
    double d1e2b = sqrt((s1e.first-s2b.first)*(s1e.first-s2b.first) + (s1e.second-s2b.second)*(s1e.second-s2b.second));    
    double d1e2e = sqrt((s1e.first-s2e.first)*(s1e.first-s2e.first) + (s1e.second-s2e.second)*(s1e.second-s2e.second));    
    return minimum(minimum(d1b2b,d1b2e), minimum(d1e2b,d1e2e)); // return overall minimum
  }

  /** Converts a 2 stems into a distance. Each stems is treated like a 2D point */
  static double stemStartDist(const stem_type& stem1, const stem_type& stem2) {
    double d1 = stem2.getStart() - stem1.getStart();
    double d2 = stem2.getStop() - stem1.getStop();
    return sqrt(d1*d1 + d2*d2);
  }

  /** Converts a 2 stems into a distance. Each stems is treated like two 2D points. This leads to 4 possible distances, the smallest ist used  */
  static bool stemDistTest() {
    Stem stem1(1000,1000,99);
    Stem stem2(1100,900,50);
    double de = stemEndDist(stem1, stem2);
    double ds = stemStartDist(stem1, stem2);
    Rcpp::Rcout << "Distance between stem 1: " << stem1 << " and stem 2: " << stem2 << " : " << de << " start distance: " << ds << endl;
    return (de < 3.0) && (ds > 90);
  }

  /** Converts a list of stems into a distance matrix. Each stems is treated like a 2D point */
  static matrix_type convertStemStartsToDistanceMatrix(const stem_set& stems) {
THROWIFNOT(stems.size() > 0,"Assertion violation in L379");
    matrix_type result(stems.size(), matrix_row_type(stems.size(), 0));
THROWIFNOT(result.size() == stems.size(),"Assertion violation in L381");
THROWIFNOT(result[0].size() == stems.size(),"Assertion violation in L382");
    for (stem_set::size_type i = 0; i < stems.size(); ++i) {
      for (stem_set::size_type j = i+1; j < stems.size(); ++j) {
	result[i][j] = stemStartDist(stems[i], stems[j]);
	result[j][i] = result[i][j];
      }
    }
    return result;
  }

  /** Converts a list of stems into a distance matrix. Each stems is treated like a 2D point */
  static matrix_type convertStemEndsToDistanceMatrix(const stem_set& stems) {
THROWIFNOT(stems.size() > 0,"Assertion violation in L394");
    matrix_type result(stems.size(), matrix_row_type(stems.size(), 0));
THROWIFNOT(result.size() == stems.size(),"Assertion violation in L396");
THROWIFNOT(result[0].size() == stems.size(),"Assertion violation in L397");
    for (stem_set::size_type i = 0; i < stems.size(); ++i) {
      for (stem_set::size_type j = i+1; j < stems.size(); ++j) {
	result[i][j] = stemEndDist(stems[i], stems[j]);
	result[j][i] = result[i][j];
      }
    }
    return result;
  }

  /** Expand stems of length n into n stems of length 1. Careful: currently only supports reverse-complementary stems. */
  static Vec<Stem> expandStem(const Stem& stem, index_type stemLength, bool reverseMode) {
    Vec<Stem> result;
    index_type totLen = 0;
    int numStems = stem.getLength() / stemLength;
THROWIFNOT(numStems > 0,"Assertion violation in L412");
    // Rcpp::Rcout << "Expanding stem " << stem << " of length " << stem.getLength() << " using minimum stem length " << stemLength << " into " << numStems << " substems " << endl;
    for (int i = 0; i < (numStems-1); ++i) {
      index_type offset = i * stemLength;
      if (reverseMode) { // reguar reverse-complementary stems
	Stem newStem(stem.getStart() + offset, stem.getStop() - offset, stemLength);
THROWIFNOT(newStem.getLength() >= stemLength,"Assertion violation in L418");
	result.push_back(newStem);
	totLen += newStem.getLength();
      } else { // case of "forward-matching" stems as used by covarna in case of opposing strand directionalities
	Stem newStem(stem.getStart() + offset, stem.getStop() + offset, stemLength);
THROWIFNOT(newStem.getLength() >= stemLength,"Assertion violation in L423");
	result.push_back(newStem);
	totLen += newStem.getLength();
      }
      // Rcpp::Rcout << "Adding substem: " << newStem << endl;
    }
    index_type missing = stem.getLength() - totLen;
    // Rcpp::Rcout << "Still missing: " << missing << endl;
THROWIFNOT(missing >= stemLength,"Assertion violation in L431");
    index_type offset = (numStems - 1) * stemLength;
    if (reverseMode) { // regular case of reverse compl stem
      Stem newStem(stem.getStart() + offset, stem.getStop() - offset, missing);
      result.push_back(newStem);
      totLen += newStem.getLength();
    } else {
      Stem newStem(stem.getStart() + offset, stem.getStop() + offset, missing);
      result.push_back(newStem);
      totLen += newStem.getLength();
    }
THROWIFNOT(totLen = stem.getLength(),"Assertion violation in L442");
    return result;
  }

  /** Expand stems of length n into n stems of length 1. Careful: currently only supports reverse-complementary stems. */
  static Vec<Stem> generateSubStems(const Stem& stem, index_type stemLengthMin, index_type lengthStep=1) {
    Vec<Stem> result;
    Stem::index_type len = stem.getLength();
    // Rcpp::Rcout << "Expanding stem " << stem << " of length " << stem.getLength() << " using minimum stem length " << stemLength << " into " << numStems << " substems " << endl;
    for (int i = 0; i < stem.getLength(); ++i) {
      for (int j = len - i; j >= stemLengthMin; j -= lengthStep) {
	if ((i > 0) || (j != (len-i))) {
	  Stem newStem(stem.getStart() + i, stem.getStop() - i, j);
	  ERROR_IF(newStem.getInvariant(true) != stem.getInvariant(true), "generateSubStems: Internal error in stem generation.");
	  result.push_back(newStem);
	}
      }
    }
    return result;
  }

  /** Expand stems of length n into n stems of length 1. Careful: currently only supports reverse-complementary stems. */
  static Vec<Stem> generateSubStemsWithSelf(const Stem& stem, index_type stemLengthMin) {
    Vec<Stem> result;
    result.push_back(stem);
    Vec<Stem> tmpResult = generateSubStems(stem, stemLengthMin);
    result.insert(result.end(), tmpResult.begin(), tmpResult.end());
    return result;
  }

  /** Expand stems of length n into n stems of length 1. Careful: currently only supports reverse-complementary stems. */
  static Vec<Stem> generateFlankedStems(const Stem& stem, index_type stemLengthMin, index_type lengthStep=1) {
    Vec<Stem> result;
    Stem::index_type len = stem.getLength();
    // Rcpp::Rcout << "Expanding stem " << stem << " of length " << stem.getLength() << " using minimum stem length " << stemLength << " into " << numStems << " substems " << endl;
    for (int i = stemLengthMin; i < len; i += lengthStep) { // longest stems are generated first
      Stem newStem(stem.getStart() , stem.getStop() , i);
      ERROR_IF(newStem.getInvariant(true) != stem.getInvariant(true), "generateSubStems: Internal error in stem generation.");
      result.push_back(newStem);
      Stem newStem2(stem.getStart() + len - i - 1, stem.getStop() - len + i + 1, i);
      ERROR_IF(newStem2.getInvariant(true) != stem.getInvariant(true), "generateSubStems: Internal error in stem generation.");
      result.push_back(newStem2);
    }
    reverse(result.begin(), result.end());
    return result;
  }

  static bool expandStemTest() {
    Stem stem(100, 200, 5);
    Vec<Stem> stems = expandStem(stem, 2, true);
   // Rcpp::Rcout << "Expanded stem " << stem << " to " << stems << " of stem lengths 2 to 3" << endl;
THROWIFNOT(stems.size() == 2,"Assertion violation in L493");
THROWIFNOT(stems[0].getLength() == 2,"Assertion violation in L494");
THROWIFNOT(stems[1].getLength() == 3,"Assertion violation in L495");

    Vec<Stem> stems2 = expandStem(stem, 2, false);
    // Rcpp::Rcout << "Expanded stem " << stem << " to " << stems2 << " of stem lengths 2 to 3" << endl;
THROWIFNOT(stems2.size() == 2,"Assertion violation in L499");
THROWIFNOT(stems2[0].getLength() == 2,"Assertion violation in L500");
THROWIFNOT(stems2[1].getLength() == 3,"Assertion violation in L501");
    return true;
  }

  static stem_set readRNAfold(istream& is, string& sequence) {
    string line = getLine(is);
    ERROR_IF(line.size() == 0, "readRNAfoldUnexcected empty first line encountered.");
    if (line[0] == '>') {
      line = getLine(is); // skip header line
    }
    sequence.clear();
    sequence.append(line);
    string bracket = getLine(is);
    unsigned int pkCounter = 0;
    Vec<Vec<double> > matrix = secToMatrix(bracket, 1.0, pkCounter);
    Vec<Stem> stems = generateStemsFromMatrix(matrix, 1, 0.5, sequence);
    return stems;
  }

/*   static RnaSecondaryStructure readRNAfold2(istream& is) { */
/* /\*     string line = getLine(is); *\/ */
/* /\*     ERROR_IF(line.size() == 0, "readRNAfoldUnexcected empty first line encountered."); *\/ */
/* /\*     if (line[0] == '>') { *\/ */
/* /\*       line = getLine(is); // skip header line *\/ */
/* /\*     } *\/ */
/*     string sequence; //  = line; */
/*     string bracket; // = getLine(is); */
/*     is >> sequence; */
/*     is >> bracket; */
/*     unsigned int pkCounter = 0; */
/*     Vec<Vec<double> > matrix = secToMatrix(bracket, 1.0, pkCounter); */
/* /\*     Rcpp::Rcout << "Sequence: " << sequence << endl; *\/ */
/* /\*     Rcpp::Rcout << "Bracket: " << bracket << endl; *\/ */
/* /\*     Rcpp::Rcout << "Matrix size: " << matrix.size() << " Sequnece size: " << sequence.size() << " " *\/ */
/* /\* 	 << " Bracket size: " << bracket.size() << endl; *\/ */
/*     ASSERT(matrix.size() == sequence.size()); */

/*     Vec<Stem> stems = generateStemsFromMatrix(matrix, 1, 0.5, sequence); */
/*     return RnaSecondaryStructure(stems, sequence); */
/*   } */

  static RnaSecondaryStructure readRNAfold2(istream& is) {
     string line = getLine(is);
     ERROR_IF(line.size() == 0, "readRNAfoldUnexcected empty first line encountered.");
     if (line[0] == '>') {
       line = getLine(is); // skip header line */
     } 
    string sequence = line;
    vector<string> tokens = getTokens(getLine(is));
    Rcpp::warning("Warning: Bracket notation and energy expected in last line of RNAfold format.");
    string bracket = tokens[0];
    unsigned int pkCounter = 0;
    Vec<Vec<double> > matrix = secToMatrix(bracket, 1.0, pkCounter);
/*     Rcpp::Rcout << "Sequence: " << sequence << endl; */
/*     Rcpp::Rcout << "Bracket: " << bracket << endl; */
/*     Rcpp::Rcout << "Matrix size: " << matrix.size() << " Sequnece size: " << sequence.size() << " " */
/* 	 << " Bracket size: " << bracket.size() << endl; */
THROWIFNOT(matrix.size() == sequence.size(),"Assertion violation in L558");

    Vec<Stem> stems = generateStemsFromMatrix(matrix, 1, 0.5, sequence);
    return RnaSecondaryStructure(stems, sequence);
  }

  /** reads data of the format used by RADAR as well as RNAevel
   * >sequence-name
   * ACUGUUAACCUUUUCCGAU
   * (((--)))----((--))-
   */
  static RnaSecondaryStructure readRNAeval(istream& is) {
     string line = getLine(is);
     ERROR_IF(line.size() == 0, "readRNAfoldUnexcected empty first line encountered.");
     if (line[0] == '>') {
       line = getLine(is); // skip header line */
     } 
    string sequence = line;
    string bracket = getLine(is);
    unsigned int pkCounter = 0;
    Vec<Vec<double> > matrix = secToMatrix(bracket, 1.0, pkCounter);
/*     Rcpp::Rcout << "Sequence: " << sequence << endl; */
/*     Rcpp::Rcout << "Bracket: " << bracket << endl; */
/*     Rcpp::Rcout << "Matrix size: " << matrix.size() << " Sequnece size: " << sequence.size() << " " */
/* 	 << " Bracket size: " << bracket.size() << endl; */
THROWIFNOT(matrix.size() == sequence.size(),"Assertion violation in L583");
    Vec<Stem> stems = generateStemsFromMatrix(matrix, 1, 0.5, sequence);
    return RnaSecondaryStructure(stems, sequence);
  }
  
  static matrix_type readWMatch(istream& is, size_type n) {
    matrix_type matrix(n, matrix_row_type(n, 0.0));
    size_type id1, id2;
    for (size_type i = 0; i < n; ++i) {
      is >> id1 >> id2;
      ERROR_IF(id1 < 1, "First id has to be greater zero!");
      --id1;
      if (id2 > 0) {
	--id2;
	ERROR_IF(id1 >= n, "First index too large!");
	ERROR_IF(id2 >= n, "Second index too large!");
	matrix[id1][id2] = 1.0;
	matrix[id2][id1] = 1.0;
      }
    }
    return matrix;
  }

  
};

#endif
