// -*- C++ -*- -------------------------------------------------------------
//
// Author: Eckart Bindewald
//
// Class: BranchAndBoundNodeContainer<T>
//
// Base class: priority_queue (STL-template)
//
// Derived classes: -
//
// description:
// container class for storing node elements. 
// Behaves like a priority queue, but with the additional property
// that no two elements "dominate" each other.
// You can add an element with push().
// With top() or pop() you get the minimum element of the priority_queue.
// pop() also deletes the top element.
// Elements, which are "dominated" by others in the priority_queue are discarded
// automatically.
// The type T must be derived from BranchAndBoundNode
// -------------------------------------------------------------------------

#ifndef _PURGE_PRIORITY_QUEUE_H_
#define _PURGE_PRIORITY_QUEUE_H_

#include <iostream>
#include <queue> // for container
#include <functional>
#include <algorithm> // for remove_if
#include <functional>
#include "debug.h"
#include "Vec.h"

using namespace std;

// #define DEBUG_VERBOSE

template <class T, class U = vector<T>,class V = less<typename U::value_type> >
class PurgePriorityQueue : public priority_queue<T,U,V> 
{
public:

  typedef typename priority_queue<T,U,V>::size_type size_type;

  /** default maximum size of container */
  enum { MAXSIZE_DEFAULT = 1000000 };

  /// default constructor
  /// maximum size max (max = 0: arbitrary size)
  PurgePriorityQueue() : 
    priority_queue<T,U, V>(), leak(false), wasAccepted(false), maxSize(MAXSIZE_DEFAULT) {}
  PurgePriorityQueue<T,U,V>(const PurgePriorityQueue<T,U,V>& orig) {
    copy(orig); 
  }

  /* OPERATORS */
  /// extremely inefficient, please avoid:
  PurgePriorityQueue<T,U,V>& operator = (const PurgePriorityQueue<T,U,V>& other) { 
    if (&other!=this) { 
      copy(other); 
    } 
    return *this; 
  }

  /* SELECTORS */

  /** returns true if no elements stored. */
  bool empty() const { return size() == 0; }

   /// return front element
  const T& top() const { return priority_queue<T,U,V>::top(); }
  /// return maximum size of container
  size_type getMaxSize() { return maxSize; }

  bool hasLeaked() const { return leak; }

  // return best best purged element
  const T& getBestPurged() const {
    ERROR_IF(!hasLeaked(), 
	"getBestPurged can only be called if container was purged at least once.");
    return bestPurged;
  }

  /** returns number of elements. */
  size_type size() const { return priority_queue<T,U,V>::size(); }

  // translate to vector. Very inefficient, please avoid 
  Vec<T> toVector() const {
    if (size() == 0) {
      return Vec<T>();
    }
    PurgePriorityQueue<T,U,V> qTemp = (*this); 
    Vec<T> result(qTemp.size());
    for (unsigned int i = 0; i < result.size(); ++i) {
      result[i] = qTemp.top(); 
      qTemp.pop();  
    }  
    return result;
  }
  // translate to vector. Very inefficient, please avoid 
  Vec<T> toVector(unsigned int n) const {
    if (size() == 0) {
      return Vec<T>();
    }
    PurgePriorityQueue<T,U,V> qTemp = (*this); 
    if (n > qTemp.size()) {
      n = qTemp.size();
    }
    Vec<T> result(n);
    for (unsigned int i = 0; i < n; ++i) {
THROWIFNOT((i < result.size()) && (qTemp.size() > 0),"Assertion violation in L111");
      result[i] = qTemp.top(); 
      qTemp.pop();  
    }  
    return result;
  }

  bool isAccepted() const { return wasAccepted; }

  /* MODIFIER */

  /// copy object
  void copy(const PurgePriorityQueue<T,U,V>&); 

  /// push element on priority_queue: make copy! 
  void push(const T& element); 
  /// delete and return first element
  void pop() { priority_queue<T,U,V>::pop(); } 
  /* empty container */
  void clear(); 
  // set maximum size;
  void setMaxSize(size_type max) { maxSize = max; }

private:

  /* MODIFIER */
  // reduce queue to half of max-size elements!
  void purge(); 

  /* ATTRIBUTES */
  
  bool leak;

  bool wasAccepted;

  size_type maxSize;

  T bestPurged; // best element which was purged

};

/**
 * copy method. extremely inefficient workaround. Please avoid!
*/
// template <class T>
template <class T, class U,class V>
inline 
void 
PurgePriorityQueue<T,U,V>::copy(const PurgePriorityQueue<T,U,V>& orig) 
{
   clear();
   maxSize = orig.maxSize;
   PurgePriorityQueue *qptr = const_cast<PurgePriorityQueue *>(&orig);
   Vec<T> helpVec(orig.size());
   unsigned int ii = 0;
   while ( ! (qptr->empty() ) )
     {
       push(qptr->top());
THROWIFNOT(ii < helpVec.size(),"Assertion violation in L169");
       helpVec[ii++] = qptr->top();
       qptr->pop();
     }
   // now copy back:
   for (unsigned int i = 0; i < helpVec.size(); ++i) {
     qptr->push(helpVec[i]);
   }
   leak    = orig.leak;
   wasAccepted = orig.wasAccepted;
   bestPurged = orig.bestPurged;
   
}


// remove all container elements
template <class T, class U, class V>
inline 
void 
PurgePriorityQueue<T,U,V>::clear()
{
  while (!empty())
    {
      pop();
    }
  leak = false;
}

// purge elements of priority_queue
template <class T, class U, class V>
inline 
void 
PurgePriorityQueue<T,U,V>::purge()
{
  if (maxSize < 10)
    {
      return;
    }
  priority_queue<T,U,V> pq;
  size_type halfSize = static_cast<size_type>(size()*0.666);
  for (size_type i = 0; i < halfSize; i++)
    {
THROWIFNOT((pq.size()-i)==0,"Assertion violation in L211");
      pq.push(top());
      pop();
    }
  if ( (!leak) || (bestPurged < top() ) ) {
    bestPurged = top(); // save best element which will be purged
  }
  clear();
  while (!pq.empty())
    {
      push(pq.top());
      pq.pop();
    }
  leak = true;
THROWIFNOT(size() == halfSize,"Assertion violation in L225");
} 


// push element on priority_queue
template <class T, class U, class V>
inline 
void 
PurgePriorityQueue<T,U,V>::push(const T& element) 
{
  // if queue was once purged, push element only not worse then best purge element
  wasAccepted = false;
  if (hasLeaked() && (element < bestPurged)) {
    return;
  }
  priority_queue<T,U,V>::push(element);
  wasAccepted = true;
  // check, if priority_queue is still smaller than maxSize:
  if ( (maxSize > 0) && (size() > maxSize) ) 
    {
      DEBUG_MSG("Container max size reached. Purging last elements...");
      purge(); // delete worst elements
      leak = true; // container has leaked because it was too big
THROWIFNOT(hasLeaked(),"Assertion violation in L248");
    }
}

// output of elements of container
template <class T, class U, class V>
inline 
ostream&
operator<<(ostream& os, const PurgePriorityQueue<T,U,V>& nc) 
{
  os << nc.size() << " entries." << endl; 
  typename PurgePriorityQueue<T,U,V>::const_iterator i = nc.begin();
  while (i != nc.end()) 
    {
    os << *i << endl; 
    i++;
    }
  return os;
}

#endif // #ifndef _PURGE_PRIORITY_QUEUE_H_







