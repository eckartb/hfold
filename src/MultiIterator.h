#ifndef _MULTI_ITERATOR_
#define _MULTI_ITERATOR_

#include "Vec.h"

class MultiIterator {

 public:

  typedef Vec<int>::size_type size_t;

  typedef unsigned int digit_t;

  typedef Vec<digit_t> container_t;

 private:

  container_t digits;

  container_t bases;

  // DigitChecker checker;

  bool startedOver;


  container_t minDigits;

  container_t maxDigits;

 public:

  MultiIterator(container_t _bases) {
    //    PRECOND(_bases.size() > 0);
    bases = _bases;
    digits = container_t(bases.size(), 0);
    minDigits = digits;
    maxDigits = bases;
    startedOver = false;
  }

  static double computeCombinations(container_t _bases) {
    double mul = 1.0;
    for (size_t i = 0; i < _bases.size(); ++i) {
      mul *= _bases[i];
    }
    return mul;    
  }

  double computeCombinations() const {
    return computeCombinations(bases);
  }

  const container_t& getBases() const { return bases; }
    
  const container_t& getDigits() const { return digits; }

  bool hasNext() const {
    for (size_t i = 0; i < digits.size(); ++i) {
      if (digits[i]+1 < bases[i]) {
	return true;
      }
    }
    return false;
  }

  void inc() {
    startedOver = false;
    if (digits.size() == 0) {
      startedOver = true;
      return;
    }
    do {
      // bool result = true;
      for (size_t i = 0; i < digits.size(); ++i) {
	digits[i] = digits[i] + 1;
	if (digits[i] >= bases[i]) {
	  digits[i] = 0;
	  if (i+1 == digits.size()) {
	    startedOver = true;
	    // result = false;
	    break;
	  }
	} else {
	  break;
	}
      }
    } while (false); // ((checker != null) && (!checker.isValid(digits)));
  }

  void inc(const container_t& bases2) {
    startedOver = false;
    if (digits.size() == 0) {
      startedOver = true;
      return;
    }
    do {
      // bool result = true;
      for (size_t i = 0; i < digits.size(); ++i) {
	digits[i] = digits[i] + 1;
	if (digits[i] >= bases2[i]) {
	  digits[i] = 0;
	  if (i+1 == digits.size()) {
	    startedOver = true;
	    // result = false;
	    break;
	  }
	} else {
	  break;
	}
      }
    } while (false); // ((checker != null) && (!checker.isValid(digits)));
    // return true;
  }

  void incToBelowThreshold(int threshold) {
    startedOver = false;
    if (digits.size() == 0) {
      startedOver = true;
      return;
    }
    ERROR_IF(digits.size() <= 0, "Internal error MultiIteraotr 92");
    // inc();
    bool changed = false;
    bool changeNeccessary = false;
    size_t m = digits.size()-1;
    do {
      changed = false;
      for (size_t i = 0; i < digits.size(); ++i) {
THROWIFNOT(digits[m-i] < bases[i],"Assertion violation in L130");
	if (static_cast<int>(digits[m-i]) > threshold) {
	  changeNeccessary = true;
	  if ((m-i+1) >= digits.size()) {
	    startedOver=true;
	    return;
	  }
	  /* size_t pc = 0; */
	  /* bool ok2 = false; */
	  /* do { */
	  /*   ok2 = true; */
	  /*   if (m-i+1+pc >= digits.size()) { */
	  /*     startedOver=true; */
	  /*     break; */
	  /*   } */
	    ++digits[m-i+1];
	  /*   if (digits[m-i+1+pc] >= threshold) { */
	  /*     digits[m-i+1+pc] = 0; */
	  /*     ++pc; */
	  /*     ok2 = false; */
	  /*   } */
	  /* } while (!ok2); */
	  for (size_t j = 0; j < (m-i+1); ++j) {
	    digits[j] = 0;
	  }
	}
      }
    } while (changed);
    if (!changeNeccessary) {
      inc(container_t(digits.size(), (threshold+1))); // increase with base 2 if threshold 1 is given
    }
  }

  /** Fast-forward lowest digit */
  bool incSkipIfPossible() {
    if (digits.size() == 0) {
      startedOver = true;
      return false;
    }
    digits[0] = bases[0]-1;
    return incIfPossible();
  }

  /** Fast-forward lowest digit until a new non-zero digit is found */
  bool incSkipUntilNewIfPossible() {
    if (digits.size() == 0) {
      startedOver = true;
      return false;
    }
    container_t digits0 = digits;
    size_t n = digits.size();
    while (true) {
      bool result = incIfPossible();
      if (!result) {
	return false;
      }
      for (size_t i = 0; i < n; ++i) {
	if ((digits0[i] == 0) && (digits[i] > 0)) {
	  return true;
	}
      }
    }
    return false;
  }

  double computeScore(const Vec<Vec<double> >& scores) const {
    // Rcpp::Rcout << "# Starting MultiIterator.computeScore" << endl;
    double score = 0.0;
    for (size_t i = 0; i < digits.size(); ++i) {
THROWIFNOT(digits[i] < scores[i].size(),"Assertion violation in L199");
      score += scores[i][digits[i]];
    }
    // Rcpp::Rcout << "# Finished MultiIterator.computeScore" << endl;
    return score;
  }

  /** Fast-forward lowest digit until a new non-zero digit is found */
  bool incSkipUntilHigherScoreIfPossible(const Vec<Vec<double> >& scores) {
    // Rcpp::Rcout << "# Starting MultiIterator.incSkipUntilHigherScoreIfPossible..." << endl;
    if (digits.size() == 0) {
      startedOver = true;
      return false;
    }
    double score0 = computeScore(scores);
    while (incIfPossible()) {
      double score = computeScore(scores);
      if (score > score0) {
	return true;
      }
    }
    // Rcpp::Rcout << "# Finished MultiIterator.incSkipUntilHigherScoreIfPossible!" << endl;
    return false;
  }

  bool incToBelowThresholdIfPossible(int threshold) {
    if (!hasNext()) {
      return false;
    }
    incToBelowThreshold(threshold);
    return !startedOver;
  }

  bool incIfPossible() {
    // Rcpp::Rcout << "# Starting MultiIterator.incIfPossible" << endl;
    if (!hasNext()) {
      // Rcpp::Rcout << "# Cannot increase digits" << endl;
      return false;
    }
    startedOver = false;
    do {
      // bool result = true;
      for (size_t i = 0; i < digits.size(); ++i) {
	digits[i] = digits[i] + 1;
	if (digits[i] >= bases[i]) {
	  digits[i] = 0;
	  if (i+1 == digits.size()) {
	    startedOver = true;
	    // result = false;
	    break;
	  }
	} else {
	  break;
	}
      }
    } while (false); // ((checker != null) && (!checker.isValid(digits)));
    // Rcpp::Rcout << "# Finished MultiIterator.incIfPossible" << endl;
    return true;
  }


  bool isStartedOver() { return startedOver; }

  void reset() {
    for (size_t i = 0; i < digits.size(); ++i) {
      digits[i] = 0;
    }
  }

  size_t size() const { return bases.size(); }

  // void setChecker(DigitChecker _checker) { checker = _checker; }
    
  static void test1() {
    container_t bases(3,3);
    Rcpp::Rcout << "Testing MultiIterator with 3 3 3:" << endl;
    for (MultiIterator it = MultiIterator(bases); !it.isStartedOver(); it.inc()) {
      Rcpp::Rcout << it.getDigits() << endl;
    }
    Rcpp::Rcout << "Should be 27 cases from 0 0 0 to 2 2 2!" << endl;
  }

  static void test2Base2() {
    container_t bases(2,2);
    // System.out.println("Testing MultiIterator with 2 2:");
    for (MultiIterator it = MultiIterator(bases); !it.isStartedOver(); it.inc()) {
      Rcpp::Rcout << it.getDigits() << endl;
    }
    // System.out.println("Should be 4 cases from 0 0 to 1 1!");
  }

};


#endif
