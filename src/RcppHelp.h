#ifndef RCPP_HELP_H
#define RCPP_HELP_H

#include <Rcpp.h>
#include <vector>
#include <string>

using namespace Rcpp;
using namespace std;

class RcppHelp {

public:
  typedef string::size_type size_t;

  static CharacterVector to_CharacterVector(const vector<string>& cv) {
    CharacterVector result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result(i) = cv[i];
    }
    return result;
  }

  static NumericVector to_NumericVector(const vector<size_t>& cv) {
    NumericVector result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result(i) = cv[i];
    }
    return result;
  }

  static NumericVector to_NumericVector(const vector<float>& cv) {
    NumericVector result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result(i) = cv[i];
    }
    return result;
  }

  static NumericVector to_NumericVector(const vector<double>& cv) {
    NumericVector result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result(i) = cv[i];
    }
    return result;
  }

  static vector<string> to_vector_string(const CharacterVector& cv) {
    vector<string> result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result[i] = cv(i);
    }
    return result;
  }

  static vector<double> to_vector_double(const NumericVector& cv) {
    vector<double> result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result[i] = cv(i);
    }
    return result;
  }

  static vector<size_t> to_vector_size_t(const NumericVector& cv) {
    vector<size_t> result(cv.size());
    for (size_t i = 0; i < cv.size(); ++i) {
      result[i] = cv(i);
    }
    return result;
  }

  static vector<vector<float> > to_vector_vector(const NumericMatrix& cv) {
    size_t m = cv.nrow();
    size_t n = cv.ncol();
    vector<vector<float> > result(m, vector<float>(n));
    for (size_t i = 0; i < m; ++i) {
      for (size_t j = 0; j < n; ++j) {
        result[i][j] = cv(i,j);
      }
    }
    return result;
  }

  static NumericMatrix to_NumericMatrix(const vector<vector<float> >& cv) {
    size_t m = cv.size();
    size_t n = 0;
    if (cv.size() > 0) {
      n = cv[0].size();
    }
    NumericMatrix result(m,n);
    for (size_t i = 0; i < m; ++i) {
      for (size_t j = 0; j < n; ++j) {
        result(i,j) = cv[i][j];
      }
    }
    return result;
  }

  static NumericMatrix to_NumericMatrix(const vector<vector<double> >& cv) {
    size_t m = cv.size();
    size_t n = 0;
    if (cv.size() > 0) {
      n = cv[0].size();
    }
    NumericMatrix result(m,n);
    for (size_t i = 0; i < m; ++i) {
      for (size_t j = 0; j < n; ++j) {
        result(i,j) = cv[i][j];
      }
    }
    return result;
  }
  
};

#endif
