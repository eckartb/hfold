// -*- C++ -*-

#ifndef __TIMER__
#define __TIMER__

extern "C" {
#include <time.h>
}
#include <iostream>

using namespace std;

class Timer {
public:
  Timer() : startTime(0), stopTime(0) {}

  inline void start() { time(&startTime); }
  inline void stop() { time(&stopTime); }
  inline void reset() { 
    startTime = 0; 
    stopTime = 0; 
  }

  int seconds() const;
  int minutes() const;
  int hours() const;
  int totalSeconds() const { 
    return static_cast<int>((stopTime - startTime));
  }

  friend ostream& operator << (ostream& os, const Timer& time);
  friend bool operator == (const Timer& left, const Timer& right);

protected:
  time_t startTime;
  time_t stopTime;  
};  

ostream& operator << (ostream& os, const Timer& time);


int 
Timer::seconds() const
{
  if (stopTime != 0) {
    return stopTime-startTime;
  } 
  else {
    time_t now = time(0);
    return now-startTime;
  }
}

int 
Timer::minutes() const
{
  if (stopTime != 0) {
    return (stopTime-startTime)/60;
  } 
  else {
    time_t now = time(0);
    return (now-startTime)/60;
  }
}

int 
Timer::hours() const
{
  if (stopTime != 0) {
      return (stopTime-startTime)/3600;
  } 
  else {
    time_t now = time(0);
    return (now-startTime)/3600;
  }
}

ostream& 
operator << (ostream& os, const Timer& time) 
{
  time_t delta = time.stopTime - time.startTime;
  int hours   = delta / 3600;
  int minutes = (delta-hours*3600) / 60;
  int seconds = (delta-hours*3600-minutes*60);
  return os << hours << ":" << minutes << ":" << seconds
	    << " total seconds: " << delta;
}

bool 
operator == (const Timer& left, const Timer& right)
{
  return ( (left.startTime == right.startTime)
	  &&(left.stopTime == right.stopTime) );
}




#endif


