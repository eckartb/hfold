#ifndef STRAND_CONTAINER
#define STRAND_CONTAINER
#include <string>
#include "Stem.h"
#include "Vec.h"
// #include <ResidueModel.h>
// #include <ResiduePairing.h>

class StrandContainer : public Vec<string> {

 public:

  typedef string::size_type size_t;

  typedef Stem::index_type index_t;

 private:
  
  Vec<double> concentrations;

  string concatSequence;

  Vec<int> strandIds;

  Vec<Vec<size_t> > extendability;
  
 public:

  StrandContainer() { }

 StrandContainer(const Vec<string>& seqs, const Vec<double>& _concentrations) : Vec<string>(seqs), concentrations(_concentrations) {
    // DEBUG_MSG("Starting StrandContainer constructor");
    updateSequences();
    // DEBUG_MSG("Finished StrandContainer constructor");
  }

  StrandContainer(const StrandContainer& other) {
    copy(other);
  }

  virtual ~StrandContainer() { }
  
  StrandContainer& operator = (const StrandContainer& other) {
    if (this != &other) {
      copy(other);
    }
    return (*this);
  }

  void clear() {
    Vec<string>::clear();
    // this->clear(); // @TODO verify
    concentrations.clear();
    concatSequence.clear();
    strandIds.clear();
    extendability.clear();
  }

  const string& getConcatSequence() const { return concatSequence; }
  
  Vec<double> getConcentrations() const { return concentrations; }

  /** Lazy evaluation of extendability matrix */
  size_t getExtendability(size_t start, size_t stop) {
    if (extendability.size() == 0) {
      bool guAllowed = true;
      extendability = createExtendabilityMatrix(concatSequence, strandIds, guAllowed);
    }
    return extendability[start][stop];
  }

  /** Lazy evaluation of extendability matrix */
  Vec<Vec<size_t> > getExtendability() {
    if (extendability.size() == 0) {
      bool guAllowed = true;
      extendability = createExtendabilityMatrix(concatSequence, strandIds, guAllowed);
    }
    return extendability;
  }

  const Vec<int>& getStrandIds() const { return strandIds; }

  const Vec<int> getStarts() const {
    Vec<int> result;
    result.push_back(0);
    for (size_t i = 1; i < concatSequence.size(); ++i) {
      if (strandIds[i] != strandIds[i-1]) {
	result.push_back(i);
      }
    }
    return result;
  }

  char getResidue (int pos) const {
    return concatSequence[pos];
  }

  /** Returns the strand id of a given residue */
  const int getStrandId(int residue) const { return strandIds[residue]; }
  
  size_t getTotalLength() const {
    return concatSequence.size();
  }

 bool isWatsonCrick(int start, int stop, bool guAllowed) const { 
   PRECOND(start < static_cast<int>(concatSequence.size())); 
   PRECOND(stop < static_cast<int>(concatSequence.size())); 
   return ResidueModel::isWatsonCrick(concatSequence[start], concatSequence[stop], guAllowed);
 }   

 /** Return true iff GU or UG */
 bool isWobble(int start, int stop) const { 
   return ResidueModel::isWobble(concatSequence[start], concatSequence[stop]);
 }

  bool isIntraStrand(const Stem& stem) const {
    return strandIds[stem.getStart()] == strandIds[stem.getStop()];
  }

  bool isWatsonCrick(const Stem& stem, bool guAllowed) const;
  
  void copy(const StrandContainer& other) {
    Vec<string>::operator=(other);
    concentrations = other.concentrations;
    concatSequence = other.concatSequence;
    strandIds = other.strandIds;
    extendability = other.extendability;
    POSTCOND(getConcatSequence() == other.getConcatSequence());
  }

  void set(const Vec<string>& seqs, const Vec<double>& _concentrations) {
    Rcpp::Rcout << "Starting StrandContainer::setSequences " << seqs << endl;
    clear();
    for (size_t i = 0; i < seqs.size(); ++i) {
      (*this).push_back(seqs[i]);
    }
    updateSequences();
    concentrations = _concentrations;
    POSTCOND(validate());
  }

  bool validateCharacters() {
    // Rcpp::Rcout << "Starting validateCharacters" << endl;
    for (size_t i = 0; i < size(); ++i) {
      if ((*this)[i].size() == 0) {
	Rcpp::Rcout << "# Sequence of length zero encountered: " << (i+1) << endl;
	return false;
      }
      for (size_t j = 0; j < (*this)[i].size(); ++j) {
	if (!(isalpha((*this)[i][j]))) {
	  Rcpp::Rcout << "# Non-alphabetical character encountered in sequence " << (i+1) << " position " << (j+1) << endl;
	  return false;
	}
      }
    }
    // Rcpp::Rcout << "Finished validateCharacters" << endl;
    return true;
  }

  bool validate() { 
THROWIFNOT(size() == concentrations.size(),"Assertion violation in L156");
THROWIFNOT(concatSequence.size() == strandIds.size(),"Assertion violation in L157");
THROWIFNOT(validateCharacters(),"Assertion violation in L158");
    return true;
    /* return (size() == concentrations.size()) && (concatSequence.size() == strandIds.size()) && validateCharacters(); */
  }

  void write(ostream& os) {
    os << "# Defined strands: ";
    operator<<(os,static_cast<Vec<string> >(*this)) << endl;
    os << "# Concentrations: " << concentrations << endl;
    os << "# Concatenated sequence: " << concatSequence << endl;
    os << "# Strand ids: " <<  strandIds << endl;
  }

  /** Generates stem table for one sequence that is "cut" at certain points. 
   * Even for a single sequence, the first element of the "starts" array has to be alwasy zero. 
   */
  static Vec<Stem> generateLongestStemsFromSequence(const string& s, unsigned int minExtendability, bool guAllowed, const Vec<int>& seqId, bool allowSuperstems = false, int verbose = 0, const Vec<int> * forbiddenPairing = NULL)  {
    Rcpp::Rcout << "Starting generateLongestStemsFromSequence with minimum stem length " << minExtendability << endl;
    if (verbose > 1) {
      Rcpp::Rcout << "Creating stems with minimum length " << minExtendability << endl;
    }
    if (forbiddenPairing != NULL) {
      Rcpp::Rcout << "Forbidden pairs: " << endl;
      Rcpp::Rcout << (*forbiddenPairing) << endl;
    }
    Vec<Stem> result;
    int n = static_cast<int>(s.size());
    if (verbose > 3) {
      Rcpp::Rcout << "# Searching sequence of length " << s.size() << endl;
      Rcpp::Rcout << "# Generated seqIds: " << endl << seqId << endl;
    }
    for (int i = 0; i < n; ++i) {
      for (int j = i + 1; j < n; ++j) {
	int s1 = seqId[i];
	int s2 = seqId[j];
	bool interStrand = (s1 != s2);
	// Rcpp::Rcout << "Position pair " << (i+1) << " " << (j+1) << " " << seqId[i] << " " << seqId[j] << " interstrand: " << interStrand << " " << s[i] << " " << s[j] << endl;	
	if (ResidueModel::isWatsonCrick(s[i], s[j], guAllowed)) {  // must be complementary
	  if ((i == 0) || ((j+1) >= static_cast<int>(s.size())) || (seqId[i] != seqId[i-1]) || (seqId[j+1] != seqId[j]) || (!ResidueModel::isWatsonCrick(s[i-1], s[j+1], guAllowed))) {  // must be beginning of stem
	    // if (((i == 0) || (seqId[i] != seqId[i-1]) ||  ((j+1) >= static_cast<int>(s.size()))
	    // || (!sequences->isWatsonCrick(s[i-1], s[j+1], guAllowed)))  // must be beginning of stem
	    if (interStrand || ((j - i) > LOOP_LEN_MIN)) {
	      int k = 0;
	      int lastGoodK = -1;
	      for (; ((i+k) < n) && (j-k >= 0) && (i+k < j -k) && ResidueModel::isWatsonCrick(s[i+k],s[j-k], guAllowed) && (seqId[i] == seqId[i+k]) && (seqId[j] == seqId[j-k])
		     && (interStrand || ((j-i-k-k) > LOOP_LEN_MIN)); ++k) {
		lastGoodK = k;
		if (forbiddenPairing != NULL && (((*forbiddenPairing)[i+k] >= 0) || ((*forbiddenPairing)[j-k] >= 0))) {
		  // found violation with already place base pair; do not save stem
		  lastGoodK = -1;
		  break;
		}
	      }
	      if (lastGoodK >= 0) {
		k = lastGoodK;
		if ((k+1) >= static_cast<int>(minExtendability)) {
		  Stem stem(i,j,k+1);
		  // stem.setEnergy(computeViennaStemEnergy(stem, s));
		  // if (stem.getEnergy() < HELIX_ENERGY_MAX) {
THROWIFNOT(stem.getStart() + stem.getLength() - 1 < stem.getStop() - stem.getLength() + 1,"Assertion violation in L217");
		  // Rcpp::Rcout << "Created stem: " << stem << endl;
		  // ASSERT(sequences->isWatsonCrick(stem, guAllowed));
		  if (stem.getLength() >= static_cast<index_t>(minExtendability)) {
		    // // && startStructure.isPlaceable(stem)) {
		    bool okToAdd = true;
		    if (!allowSuperstems) {
		      int replaceCount = 0;
		      for (size_t m = 0; m < result.size(); ++m) {
			if (result[m].uses(stem)) {
			  okToAdd = false;
			  break;
			} else if (stem.uses(result[m])) {
			  result[m] = stem; // replace
			  ++replaceCount;
			  ERROR_IF(replaceCount > 1, "Internal error: several stems found that contain current stem.");
			}
		      }
		    }
		    if (okToAdd) {
		      result.push_back(stem);
		    }
		    // Rcpp::Rcout << "Added stem to stem list: " << stem << " : " << minExtendability << endl;
		  } else {
		    // Rcpp::Rcout << "Not added stem to stem list: " << stem << " : " << minExtendability << " because it is not placeable" << endl;
		  }		    
		}
	      }
	    }
	  }
	}
      }
    }
    Rcpp::Rcout << "Finished generateLongestStemsFromSequence with minimum stem length " << minExtendability << " : " << result.size() << endl;
    return result;
  }

  /** generates stem list from sequence. Original from stemhelp.cc */
  static Vec<Stem> generateLongestStemsFromSequence_old(const string& sequence,
						    unsigned int minStemLength,
						    bool guAllowed,
							const Vec<int>& sequenceIds) {
    PRECOND(sequenceIds.size() == sequence.size());
    unsigned int stemStart = 0;
    unsigned int stemStop = 0;
    unsigned int stemLength = 0;
    double stemEnergy = 0.0;
    Vec<Stem> stems;
    for (unsigned int i = 0; i < sequence.size(); ++i) {
      for (unsigned int j = i + 1; j < sequence.size(); ++j) {
	if (ResidueModel::isWatsonCrick(sequence[i], sequence[j], guAllowed)) {
	  if ((i > 0) && ((j+1) < sequence.size()) && (sequenceIds[i] == sequenceIds[i-1]) && (sequenceIds[j] == sequenceIds[j+1]) 
	      && (ResidueModel::isWatsonCrick(sequence[i-1], sequence[j+1], guAllowed))) {
	    continue; // this is not the start of the stem, so skip
	  }
	  stemLength = 1;
	  stemStart = i;
	  stemStop = j;
	  stemEnergy = 1.0; // dummy energy
	  // ++stemLength;
	  unsigned int k = 0;
	  for (k = 1; ((i+k) < sequence.size()) && (k <= j)
		 && ((i+k) < (j-k)); ++k) {
	    if ((!ResidueModel::isWatsonCrick(sequence[i+k], sequence[j-k], guAllowed))
		|| (sequenceIds[i+k] != sequenceIds[i]) || (sequenceIds[j-k] != sequenceIds[j])) { // test for sequence boundaries
	      break;
	    }
	    if ((sequenceIds[i+k] == sequenceIds[j-k]) && ((j-k) - (i+k) -1 ) <= LOOP_LEN_MIN ) { // same strand
	      break;
	    }
	  } 
	  stemLength = k;
	  // 	if (stemLength >= histogram.size()) {
	  // 	  stemLength = histogram.size() - 1;
	  // 	}
	  string seq1= sequence.substr(stemStart, stemLength);
	  ERROR_IF(stemLength > stemStop, "Internal error in line 321!");
	  string seq2 = sequence.substr(stemStop - stemLength + 1, stemLength);
	  // ++histogram[stemLength];
	  string seq2Rev = reverseString(seq2);
	  // reverse(seq2Rev.begin(), seq2Rev.end());
	  /* if(!ResidueModel::isWatsonCrick(seq1, seq2Rev, guAllowed)) { */
	  /*   Rcpp::Rcout << seq1 << endl << seq2 << endl << seq2Rev << endl; */
	  /*   ERROR("Internal error in line 328!"); */
	  /* } */
	  if (stemLength >= minStemLength) {     
	    stems.push_back(Stem(stemStart, stemStop, stemLength, 
				 stemEnergy, seq1, seq2));
	  }
	}
      }
    }
    return stems;
  }
  
  static Vec<Vec<size_t> > createExtendabilityMatrix(const string& concatSequence, const Vec<int>& sequenceIds, bool guAllowed) {
    DEBUG_MSG("Starting StrandContainer.createExtendabilityMatrix");
    size_t len = concatSequence.size();
    Vec<Vec<size_t> > result = Vec<Vec<size_t> >(len, Vec<size_t>(len, 0));
    unsigned int minStemLength = 1;
    Vec<Stem> stems = generateLongestStemsFromSequence(concatSequence, minStemLength, guAllowed, sequenceIds);
    for (size_t i = 0;i < stems.size(); ++i) {
      size_t slen = static_cast<size_t>(stems[i].getLength());
      for (size_t j = 0; j < slen; ++j) {
	index_t px = stems[i].getStart() + j;
THROWIFNOT(px < static_cast<index_t>(len),"Assertion violation in L322");
THROWIFNOT(static_cast<index_t>(j) <= stems[i].getStop(),"Assertion violation in L323");
	index_t py = stems[i].getStop() - j;
	if (slen > result[px][py]) {
	  result[px][py] = slen;
	  result[py][px] = slen;
	}
      }
    }
    DEBUG_MSG("Finished StrandContainer.createExtendabilityMatrix");
    return result;
  }

 private:

  void updateSequences() {
    // DEBUG_MSG("Starting StrandContainer.updateSequences");
    if (size() == 0) {
      clear();
      return;
    }
THROWIFNOT((*this).size() > 0,"Assertion violation in L343");
    concatSequence = (*this)[0];
    for (size_t i = 1; i < size(); ++i) {
      concatSequence = concatSequence + (*this)[i];
    }
    strandIds = Vec<int>(concatSequence.size(), 0);
    int pc = 0;
    for (size_t i = 0; i < size(); ++i) {
      for (size_t j = 0; j < (*this)[i].size(); ++j) {
	ERROR_IF(pc >= static_cast<int>(strandIds.size()), "Interal error while initializing strand ids.");
	strandIds[pc++] = i;
      }
    }
    // bool guAllowed = true;
    // extendability = createExtendabilityMatrix(concatSequence, strandIds, guAllowed); // no use lazy evaluation
    // DEBUG_MSG("Finished StrandContainer.updateSequences");
  }

};

bool
StrandContainer::isWatsonCrick(const Stem& stem, bool guAllowed) const {
THROWIFNOT(ResidueModel::isWatsonCrickDna('g','c'),"Assertion violation in StrandContainer.h :  L365");
THROWIFNOT(ResidueModel::isWatsonCrick('g','c', true),"Assertion violation in StrandContainer.h :  L366");
THROWIFNOT(ResidueModel::isWatsonCrick('g','c', false),"Assertion violation in StrandContainer.h :  L367");
  for (int i = 0; i < stem.getLength(); ++i) {
THROWIFNOT(stem.getStart() + i < stem.getStop() - i,"Assertion violation in L369");
    if (!isWatsonCrick(stem.getStart() + i, stem.getStop() - i, guAllowed)) {
      Rcpp::Rcout << "Inconsistent stem found: " << concatSequence.substr(stem.getStart(), stem.getLength()) << " " 
	   << concatSequence.substr(stem.getStop()-stem.getLength()+1, stem.getLength()) << " " << (i+1) << " " << concatSequence[stem.getStart()+1] << " " << concatSequence[stem.getStop()-i] << " " << ResidueModel::isWatsonCrick(stem.getStart() + i, stem.getStop() - i, guAllowed) << endl;
      return false;
    }
  }    
  return true;
}


#endif
