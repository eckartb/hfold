# Base pair *enthalpies* (delta H values, not delta G values) compiled by Brian Lindsay on Jan 22, 2013 based on 2 papers:
#  Xia et al (1998) Biochemistry
# this file is based on a different notation: AU AU stands for an AU base pair followed by another AU base pair. The standard notation for the same two base pairs would be AA/UU
# (1999), Mathews D.H. et al. J. Mol. Biol. - GU base pairing (Table 4)
# AU AU -6.82 dH
# standard notation: 5-AA-3/3-UU-5:
AU AU -0.93
# AU UA -9.38 dH
# standard notation: 5-AU-3/3-UA-5:
AU UA -1.1
# UA AU -7.79 dH
# standard notation: 5-UA-3/3-AU-5:
UA AU -1.33
# CG UA -10.48 dH
# standard notation: 5-CU-3/3-GA-5:
CG UA -2.08
# CG AU -10.44 dH
# standard notation: 5-CA-3/3-GU-5:
CG AU -2.11
# GC UA -11.40 dH
# standard notation: 5-GU-3/3-CA-5:
GC UA -2.24
# GC AU -12.44
# standard notation: 5-GA-3/3-CU-5:
GC AU -2.35
# CG GC -10.64
# standard notation: 5-CG-3/3-GC-5:
CG GC -2.36
# GC GC -13.39
# standard notation: 5-GG-3/3-CC-5:
GC GC -3.26
# GC CG -14.88
# standard notation: 5-GC-3/3-CG-5:
GC CG -3.42
# AU GU -3.21
# standard notation: 5-AG-3/3-UU-5:
AU GU -0.55
# AU UG -8.81
# standard notation: 5-AU-3/3-UG-5:
AU UG -1.36
# CG GU -5.61
# standard notation: 5-CG-3/3-GU-5:
CG GU -1.41
# CG UG -12.11
# standard notation: 5-CU-3/3-GG-5:
CG UG -2.11
# GC GU -8.33
# standard notation: 5-GG-3/3-CU-5:
GC GU -1.53
# GC UG -12.59
# standard notation: 5-GU-3/3-CG-5:
GC UG -2.51
# GU AU -12.83
# standard notation: 5-GA-3/3-UU-5:
GU AU -1.27
# GU GU -13.47
# GU GU 0.47 : according to Mathews: large error, experimental value overwritten by value -0.5 to improve structure prediction accuracy
# standard notation: 5-GG-3/3-UU-5:
GU GU -0.5
# GU UG -14.59
# standard notation: 5-GU-3/3-UG-5:
GU UG 1.29
# UA GU -6.99
# standard notation: 5-UG-3/3-AU-5:
UA GU -1.0
# UG GU -9.26
# standard notation: 5-UG-3/3-GU-5:
UG GU 0.30
# !TERMINAL AU 3.72
!TERMINAL AU 0.45
# !TERMINAL GU 3.72
!TERMINAL GU 0.45
# DNA/DNA: A unified view of polymer, dumbbell, and oligonucleotide DNA nearest-neighbor thermodynamics, John SantaLucia, Jr. 1998
# at at -7.9
# standard notation: 5-aa-3/3-tt-5:
at at -1.015
# at ta -7.2
# standard notation: 5-at-3/3-ta-5:
at ta -0.873
# ta at -7.2
# standard notation: 5-ta-3/3-at-5:
ta at -0.594
# cg at -8.5
# standard notation: 5-ca-3/3-gt-5:
cg at -1.450
# gc ta -8.4
# standard notation: 5-gt-3/3-ca-5:
gc ta -1.452
# cg ta -7.8
# standard notation: 5-ct-3/3-ga-5:
cg ta -1.287
# gc at -8.2
# standard notation: 5-ga-3/3-ct-5:
gc at -1.315
# cg gc -10.6
# standard notation: 5-cg-3/3-gc-5:
cg gc -2.164
# gc cg -9.8
# standard notation: 5-gc-3/3-cg-5:
gc cg -2.232
# gc gc -8.0
# standard notation: 5-gg-3/3-cc-5:
gc gc -1.828

!TERMINAL gc 0.98
!TERMINAL at 1.03
# RNA/DNA Thermodynamic Parameters To Predict Stability of RNA/DNA Hybrid Duplexes, Sugimoto et al. 1995 Biochemistry
# standard notation: 5-AA-3/3-tt-5:
At At -1.0
# standard notation: 5-AC-3/3-tg-5:
At Cg -2.1
# standard notation: 5-AG-3/3-tc-5:
At Gc -1.8
# standard notation: 5-AU-3/3-ta-5:
At Ua -0.9
# standard notation: 5-CA-3/3-gt-5:
Cg At -0.9
# standard notation: 5-CC-3/3-gg-5:
Cg Cg -2.1
# standard notation: 5-CG-3/3-gc-5:
Cg Gc -1.7
# standard notation: 5-CU-3/3-ga-5:
Cg Ua -0.9
# standard notation: 5-GA-3/3-ct-5:
Gc At -1.3
# standard notation: 5-GC-3/3-cg-5:
Gc Cg -2.7
# standard notation: 5-GG-3/3-cc-5:
Gc Gc -2.9
# standard notation: 5-GU-3/3-ca-5:
Gc Ua -1.1
# standard notation: 5-GU-3/3-cg-5:
# data not available. Added zero:
Gc Ug 0.0
# standard notation: 5-UA-3/3-at-5:
Ua At -0.6
# standard notation: 5-UC-3/3-ag-5:
Ua Cg -1.5
# standard notation: 5-UG-3/3-ac-5:
Ua Gc -1.6
# standard notation: 5-UU-3/3-aa-5:
Ua Ua -0.2
# data not available, added zero:
# standard notation: 5-UA-3/3-gt-5:
Ug At 0.0
# standard notation: 5-UU-3/3-gg-5:
# data not available, added zero:
Ug Ug 0.0
# Initiation of RNA/DNA: 3.1
!TERMINAL At 1.55
!TERMINAL Cg 1.55
!TERMINAL Gc 1.55
!TERMINAL Ua 1.55
