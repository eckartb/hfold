#!/usr/bin/env perl

use strict;

my $debugMode = 0;

if ($debugMode) {
 for (my $i = 0; $i < scalar(@ARGV); $i++) {
    print "Parameter $i: $ARGV[$i]\n";
    if ($ARGV[$i] eq "-h") {
	print "found -h\n";
    }
    if ($ARGV[$i] eq "help") {
	print "found help\n";
    }
    if ($ARGV[$i] eq "--help") {
	print "found --help\n";
    }
    if ($ARGV[$i] eq "-help") {
	print "found -help\n";
    }
 }
}

if (scalar(@ARGV) > 0) {
#    if (($ARGV[0] eq "h") or ($ARGV[0] eq "-h") or ($ARGV[0] eq "help") or ($ARGV[0] eq "--help") or ($ARGV[0] eq "-help"))  {
    if ($ARGV[0] eq "help")  {
	print "Multistrand nucleic acid secondary structure prediction with hyperfold. Usage: hyperfold OUTPUTFILE < INPUT\nThe format of the input file are newline separated character strings corresponding to the interacting sequences. The format of the output file is JSON\n";
    exit(0);
    }
}

for (my $i = 0; $i < scalar(@ARGV); $i++) {
    my $ii = $i+1;
    if ($ARGV[$i] =~ /^[a-zA-Z0-9_=\-\.]+$/) { # allow a-zA-Z=-. 
	# everything is fine    
    } else {
	die "Error in command line parameter $ii. Command line parameters must consist of only alpha-numeric characters (a-z,A-Z,0-9,_)\n";
    }
}

my $concentration = "";
my $outfile = "";
my $verbose = 0;

for (my $i = 0; $i < scalar(@ARGV); $i++) {
    if (substr($ARGV[$i],0,2) eq "o=") {
	$outfile = substr($ARGV[$i],2); # special case for o=OUTPUTFILE
    } elsif (substr($ARGV[$i],0,2) eq "c=") {
	$concentration = substr($ARGV[$i],2); # special case for c=CONCENTRATION [ in mol/l ]
    } elsif (substr($ARGV[$i],0,8) eq "verbose=") {
	$verbose=substr($ARGV[$i],8);
    }

}

if (length($outfile) == 0) {
    $outfile = "hyperfold_out";
}

# chomp(my @sequences = <STDIN>);
my @sequences;
while (<STDIN>) {
    chomp;
    my $s = $_;
    $s =~ s/\s+//g;
    if (length($s) > 0) {
	if (substr($s,0,1) ne ">") {
  	   push(@sequences, $s);   
	}
    }
}

if (scalar(@sequences) == 0) {
    die "No sequences were specified in the input file\n";
}

my $command = "Rscript -e 'library(hfold); Hyperfold(c(";
my $tseq = "";
for (my $i = 0; $i < scalar(@sequences); $i++) {
 my $sequence = $sequences[$i];
 $sequence =~ s/\s+//g; # remove any whitespace
 if (substr($sequence,0,1) eq ">") {
     next; # was a header not a sequence
 }
 if (!($sequence =~ /^[ACGTUacgtu]+$/)) {
     die ("Found sequence data containing non-nucleotide characters. Allowed are A,C,G,T,U,a,c,g,t,u\n");
 }
 if ($sequence =~ /u/) {
     if (!($sequence =~ /t/)) { # must be RNA, convert to upper case
	 print STDERR "# Warning: the convention of this program is that RNA and DNA nucleotides are denoted in upper-case and lower-case respectively. Found lower case \"u\", converting sequence to uppercase. It is highly recommended to not rely on this conversion but to provide the correct upper-case and lower-case notation of RNA and DNA nucleotides.\n";
	 $sequence = uc $sequence;
     } else {
	 die("# Warning: the convention of this program is that RNA and DNA nucleotides are denoted in upper-case and lower-case respectively. Found lower case \"u\" as well as \"t\", not able to convert sequence to uppercase. Please adjust your sequence data.\n");
     }
 }
 if (length($tseq) > 0) {
   $tseq = $tseq . ",\"" . $sequence . "\"";
 } else {
   $tseq = "\"" . $sequence . "\"";
 }
}

$command = $command . $tseq . "),o=\"$outfile\", verbose=$verbose, params=list(";

my $paramString = "";

for (my $i = 0; $i < scalar(@ARGV); $i++) {
    if (substr($ARGV[$i],0,2) eq "o=") {

    } elsif (substr($ARGV[$i],0,2) eq "c=") {

    } elsif (substr($ARGV[$i],0,8) eq "verbose=") {
      
    } elsif ($ARGV[$i] eq "debug") {
	$debugMode = 1;
    } else {
	if ($i > 0) {
	    $paramString = $paramString . ",";
	}
	$paramString = $paramString . $ARGV[$i];
    }
}

$command = $command . $paramString . " )";
 if (length($concentration) > 0) { # special case for c=CONCENTRATION
   $command = $command . ", concentration=$concentration";
 }

$command = $command . " )'";

if ($debugMode) {
 print "$command\n";
}

exec ($command ); # hand over control to hyperfold binary


