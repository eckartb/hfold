import os
import sys

# This Python 3 script extracts the first secondary structure from a multi-structure CT output file

argv = sys.argv

f = sys.stdin

if len(argv) > 1:
    filename = argv[1]
    f = open(filename,"r")

lines = f.readlines()

motif = "# Connectivity"
motifcount = 0

for line in lines:
    if line.startswith(motif):
        motifcount += 1
    if motifcount >= 2:
        break
    if not line.startswith('#'):
        print(line.rstrip())



