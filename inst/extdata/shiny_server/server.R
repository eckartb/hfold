## minimal example server.R
library(shiny)
library(hyperfold)
library(ggplot2)
library(reshape2)
library(XML)
library(rnameso)

shinyServer(function(input, output, session) {


  output$prediction <- renderUI(

    list(renderTable({

    # ommiting switch(input$dataset) ...
    sequenceText <- input$comment
    sequenceText <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText, asText = TRUE), "//p")[[1]]))
    sequenceText2 <- input$comment2
    sequenceText2 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText2, asText = TRUE), "//p")[[1]]))
    sequenceText3 <- input$comment3
    sequenceText3 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText3, asText = TRUE), "//p")[[1]]))

   rnadna1 = switch(input$rnadna1,
                    "rna"="rna",
		    "dna"="dna")
   rnadna2 = switch(input$rnadna2,
                    "rna"="rna",
		    "dna"="dna")
   rnadna3 = switch(input$rnadna3,
                    "rna"="rna",
		    "dna"="dna")
    if (rnadna1 == "rna") {
       sequenceText <- gsub("T","U", toupper(sequenceText))
    } else {
       sequenceText <- gsub("u", "t", tolower(sequenceText))
    }		    
    if (rnadna2 == "rna") {
       sequenceText2 <- gsub("T","U", toupper(sequenceText2))
    } else {
       sequenceText2 <- gsub("u", "t", tolower(sequenceText2))
    }		    
    if (rnadna3 == "rna") {
       sequenceText3 <- gsub("T","U", toupper(sequenceText3))
    } else {
       sequenceText3 <- gsub("u", "t", tolower(sequenceText3))
    }		    

    cat("Raw text:\n")
    print(sequenceText)
    print(sequenceText2)
    print(sequenceText3)
    sequences <- sequenceText # rep("", length(seqs))
    if (nchar(sequenceText2) > 0) {
     sequences <- append(sequences, sequenceText2)
    }
    if (nchar(sequenceText3) > 0) {
     sequences <- append(sequences, sequenceText3)
    }
    cat("Passing this sequence to the backend:\n")
    print(sequences)
    prediction_all <- Hyperfold(sequences=sequences)
    bestStructure <- prediction_all[["best_structure"]]

#    updateTextInput(session, inputId = "newenergy", value = bestStructure[["absolute_free_energy"]])

    bpt <- bestStructure[["pairs"]] # predicted base pair table
    bpt[,"id1"] <- as.integer(bpt[, "id1"])
    bpt[,"id2"] <- as.integer(bpt[, "id2"])
    cat("The structure prediction was:\n")
    print(bpt)
    bpt}),
  # additional elements
    hr()
    ) 
    ) # renderUI

    output$concentrations <- renderTable({

    sequenceText <- input$comment
    sequenceText <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText, asText = TRUE), "//p")[[1]]))
    sequenceText2 <- input$comment2
    sequenceText2 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText2, asText = TRUE), "//p")[[1]]))
    sequenceText3 <- input$comment3
    sequenceText3 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText3, asText = TRUE), "//p")[[1]]))

   rnadna1 = switch(input$rnadna1,
                    "rna"="rna",
		    "dna"="dna")
   rnadna2 = switch(input$rnadna2,
                    "rna"="rna",
		    "dna"="dna")
   rnadna3 = switch(input$rnadna3,
                    "rna"="rna",
		    "dna"="dna")
    if (rnadna1 == "rna") {
       sequenceText <- gsub("T","U", toupper(sequenceText))
    } else {
       sequenceText <- gsub("u", "t", tolower(sequenceText))
    }		    
    if (rnadna2 == "rna") {
       sequenceText2 <- gsub("T","U", toupper(sequenceText2))
    } else {
       sequenceText2 <- gsub("u", "t", tolower(sequenceText2))
    }		    
    if (rnadna3 == "rna") {
       sequenceText3 <- gsub("T","U", toupper(sequenceText3))
    } else {
       sequenceText3 <- gsub("u", "t", tolower(sequenceText3))
    }		    

    cat("Raw text:\n")
    print(sequenceText)
    print(sequenceText2)
    sequences <- sequenceText # rep("", length(seqs))
    if (nchar(sequenceText2) > 0) {
     sequences <- append(sequences, sequenceText2)
    }
    if (nchar(sequenceText3) > 0) {
     sequences <- append(sequences, sequenceText3)
    }
    cat("Passing this sequence to the backend:\n")
    print(sequences)
    prediction_all <- Hyperfold(sequences=sequences)
    concentrations=prediction_all[["complex_concentrations"]]
#    concentrations$Concentration <- concentrations$Concentration * 1E6 # express in micromol/l
    concentrations
  }, digits=-10)

 #  output$energy <- renderText({"hi })

   output$datatext <- renderText({

    sequenceText <- input$comment
    sequenceText <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText, asText = TRUE), "//p")[[1]]))
    sequenceText2 <- input$comment2
    sequenceText2 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText2, asText = TRUE), "//p")[[1]]))
    sequenceText3 <- input$comment3
    sequenceText3 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText3, asText = TRUE), "//p")[[1]]))

   rnadna1 = switch(input$rnadna1,
                    "rna"="rna",
		    "dna"="dna")
   rnadna2 = switch(input$rnadna2,
                    "rna"="rna",
		    "dna"="dna")
   rnadna3 = switch(input$rnadna3,
                    "rna"="rna",
		    "dna"="dna")
    if (rnadna1 == "rna") {
       sequenceText <- gsub("T","U", toupper(sequenceText))
    } else {
       sequenceText <- gsub("u", "t", tolower(sequenceText))
    }		    
    if (rnadna2 == "rna") {
       sequenceText2 <- gsub("T","U", toupper(sequenceText2))
    } else {
       sequenceText2 <- gsub("u", "t", tolower(sequenceText2))
    }		    
    if (rnadna3 == "rna") {
       sequenceText3 <- gsub("T","U", toupper(sequenceText3))
    } else {
       sequenceText3 <- gsub("u", "t", tolower(sequenceText3))
    }		    

    cat("Raw text:\n")
    print(sequenceText)
    print(sequenceText2)
    sequences <- sequenceText # rep("", length(seqs))
    if (nchar(sequenceText2) > 0) {
     sequences <- append(sequences, sequenceText2)
    }
    if (nchar(sequenceText3) > 0) {
     sequences <- append(sequences, sequenceText3)
    }
    cat("Passing this sequence to the backend:\n")
    print(sequences)


    prediction_all <- Hyperfold(sequences=sequences)
    bestStructure <- prediction_all[["best_structure"]]
    paste0("Free energy: ", round(bestStructure[["absolute_free_energy"]], 3))
   })

   output$plotDisplay <- renderPlot({

    sequenceText <- input$comment
    sequenceText <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText, asText = TRUE), "//p")[[1]]))
    sequenceText2 <- input$comment2
    sequenceText2 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText2, asText = TRUE), "//p")[[1]]))
    sequenceText3 <- input$comment3
    sequenceText3 <- gsub("\\s+", "", xmlValue(getNodeSet(htmlParse(sequenceText3, asText = TRUE), "//p")[[1]]))

   rnadna1 = switch(input$rnadna1,
                    "rna"="rna",
		    "dna"="dna")
   rnadna2 = switch(input$rnadna2,
                    "rna"="rna",
		    "dna"="dna")
   rnadna3 = switch(input$rnadna3,
                    "rna"="rna",
		    "dna"="dna")
    if (rnadna1 == "rna") {
       sequenceText <- gsub("T","U", toupper(sequenceText))
    } else {
       sequenceText <- gsub("u", "t", tolower(sequenceText))
    }		    
    if (rnadna2 == "rna") {
       sequenceText2 <- gsub("T","U", toupper(sequenceText2))
    } else {
       sequenceText2 <- gsub("u", "t", tolower(sequenceText2))
    }		    
    if (rnadna3 == "rna") {
       sequenceText3 <- gsub("T","U", toupper(sequenceText3))
    } else {
       sequenceText3 <- gsub("u", "t", tolower(sequenceText3))
    }		    

    sequences <- sequenceText # rep("", length(seqs))
    if (nchar(sequenceText2) > 0) {
     sequences <- append(sequences, sequenceText2)
    }
    if (nchar(sequenceText3) > 0) {
     sequences <- append(sequences, sequenceText3)
    }

    prediction_all <- Hyperfold(sequences=sequences)
    occupancy <- prediction_all[["occupancy"]]
#    print(head(occupancy))
#    occm <- reshape2::melt(occupancy)
#    print(head(occm))
#    colnames(occm) <- c("Var1", "Var2", "Occupancy")
    totlen=sum(nchar(sequences))
    ggplot2::ggplot(occupancy, aes(x=id1, y=id2)) + geom_tile(aes(fill=occ)) + xlab("Position") + ylab("Position") + coord_fixed() + theme_minimal(base_size = 18)  + scale_fill_gradient(low = "white", high = "black") + xlim(0,totlen) + ylim(0,totlen)
   })
	      
})
